package com.suresh.innapp_purches

import java.util.*

/**
 * @since  15/4/16.
 */
object InAppConstants {
    val base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtUOPbg4sp9ttS0jWdOo5RNIQRb6JpurkC7sMqqn+gxruPlyf9Eb+6+BA9rdXQRShHIwD7XSF9Iez/6/U9ep9ayaaRBEzXzSJmIUpMwuFU7yVFW4YdTMfgNP1YBkbLYL78G0Z6+qXxwHRuXsew/08hz9V8IjZYl1m2ASu8hcb8qZ69aOXLRFTVWk0KhSq5/NMJHY46IjbmQRTpNqJCZu8L3/bFOT9ImtbGol3ThOwUQdPBR+u3fuscvyJtGIBsham9S3EKJ7zFbKB2qP/9fX9zjrgBbT+zAtYncmnd5fZQQjCtLpBa7qeB6tdriJnsE9Th1r2iAkJTNsUACX3wMGjCQIDAQAB".trim { it <= ' ' }

    /*
     *purchase item details. */
    enum class Purchase_item(val key: String, val id: String) {
        LOWER_PRICE("times_more_buyers_100", "1503151128429"), MEDIUM_PRICE("times_more_buyers_200", "1503153105155"), HIGH_PRICE("times_more_buyers_300", "1504019528923"), HIGEST_PRICE("times_more_buyers_500", "1499918013413"), TEST_CASES("android.test.com", "android.test.purchased");

        companion object {
            //com.yelo.lowest
            fun getPurchaseItem(id: String): Purchase_item {
                var purchase_item: Purchase_item? = null
                val purchase_items = ArrayList<Purchase_item>()
                Collections.addAll(purchase_items, *values())
                for (item in purchase_items) {
                    if (item.key == id) {
                        purchase_item = item
                        break
                    }
                }
                if (purchase_item == null) {
                    purchase_item = TEST_CASES
                }
                return purchase_item
            }

            val purchaseItemList: ArrayList<String>
                get() {
                    val temp_list = ArrayList<String>()
                    val purchase_item: Purchase_item? = null
                    val purchase_items = ArrayList<Purchase_item>()
                    Collections.addAll(purchase_items, *values())
                    for (item in purchase_items) {
                        temp_list.add(item.key)
                    }
                    return temp_list
                }

            /*
         *Getting the plan Id */
            fun getPlanId(playKey: String): Purchase_item? {
                var purchase_item: Purchase_item? = null
                val purchase_items = ArrayList<Purchase_item>()
                Collections.addAll(purchase_items, *values())
                for (item in purchase_items) {
                    if (item.key == playKey) {
                        purchase_item = item
                        break
                    }
                }
                return purchase_item
            }
        }

    }
}