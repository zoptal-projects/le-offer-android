package com.suresh.innapp_purches.Inn_App_billing

import android.os.Parcel
import android.os.Parcelable
import android.util.Log
import org.json.JSONException
import org.json.JSONObject
import java.util.*

/**
 * With this PurchaseInfo a developer is able verify
 * a purchase from the google play store on his own
 * server. An example implementation of how to verify
 * a purchase you can find [here](https://github.com/mgoldsborough/google-play-in-app-billing-verification/blob/master/library/GooglePlay/InAppBilling/GooglePlayResponseValidator.php#L64)
 */
class PurchaseInfo : Parcelable {
    enum class PurchaseState {
        PurchasedSuccessfully, Canceled, Refunded, SubscriptionExpired
    }

    val responseData: String
    val signature: String?

    constructor(responseData: String, signature: String?) {
        this.responseData = responseData
        this.signature = signature
    }

    class ResponseData : Parcelable {
        var orderId: String? = null
        var packageName: String? = null
        var productId: String? = null
        var purchaseTime: Date? = null
        var purchaseState: PurchaseState? = null
        var developerPayload: String? = null
        var purchaseToken: String? = null
        var autoRenewing = false
        override fun describeContents(): Int {
            return 0
        }

        override fun writeToParcel(dest: Parcel, flags: Int) {
            dest.writeString(orderId)
            dest.writeString(packageName)
            dest.writeString(productId)
            dest.writeLong(if (purchaseTime != null) purchaseTime!!.time else -1)
            dest.writeInt(if (purchaseState == null) -1 else purchaseState!!.ordinal)
            dest.writeString(developerPayload)
            dest.writeString(purchaseToken)
            dest.writeByte(if (autoRenewing) 1.toByte() else 0.toByte())
        }

        constructor() {}
        protected constructor(`in`: Parcel) {
            orderId = `in`.readString()
            packageName = `in`.readString()
            productId = `in`.readString()
            val tmpPurchaseTime = `in`.readLong()
            purchaseTime = if (tmpPurchaseTime == -1L) null else Date(tmpPurchaseTime)
            val tmpPurchaseState = `in`.readInt()
            purchaseState = if (tmpPurchaseState == -1) null else PurchaseState.values()[tmpPurchaseState]
            developerPayload = `in`.readString()
            purchaseToken = `in`.readString()
            autoRenewing = `in`.readByte().toInt() != 0
        }

        companion object {
            val CREATOR: Parcelable.Creator<ResponseData?> = object : Parcelable.Creator<ResponseData?> {
                override fun createFromParcel(source: Parcel): ResponseData? {
                    return ResponseData(source)
                }

                override fun newArray(size: Int): Array<ResponseData?> {
                    return arrayOfNulls(size)
                }
            }
        }
    }

    fun parseResponseData(): ResponseData? {
        return try {
            val json = JSONObject(responseData)
            val data = ResponseData()
            data.orderId = json.optString("orderId")
            data.packageName = json.optString("packageName")
            data.productId = json.optString("productId")
            val purchaseTimeMillis = json.optLong("purchaseTime", 0)
            data.purchaseTime = if (purchaseTimeMillis != 0L) Date(purchaseTimeMillis) else null
            data.purchaseState = getPurchaseState(json.optInt("purchaseState", 1))
            data.developerPayload = json.optString("developerPayload")
            data.purchaseToken = json.getString("purchaseToken")
            data.autoRenewing = json.optBoolean("autoRenewing")
            data
        } catch (e: JSONException) {
            Log.e(LOG_TAG, "Failed to parse response data", e)
            null
        }
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeString(responseData)
        dest.writeString(signature)
    }

    protected constructor(`in`: Parcel) {
        responseData = `in`.readString()
        signature = `in`.readString()
    }

    companion object {
        private const val LOG_TAG = "iabv3.purchaseInfo"
        fun getPurchaseState(state: Int): PurchaseState {
            return when (state) {
                0 -> PurchaseState.PurchasedSuccessfully
                1 -> PurchaseState.Canceled
                2 -> PurchaseState.Refunded
                3 -> PurchaseState.SubscriptionExpired
                else -> PurchaseState.Canceled
            }
        }

        val CREATOR: Parcelable.Creator<PurchaseInfo?> = object : Parcelable.Creator<PurchaseInfo?> {
            override fun createFromParcel(source: Parcel): PurchaseInfo? {
                return PurchaseInfo(source)
            }

            override fun newArray(size: Int): Array<PurchaseInfo?> {
                return arrayOfNulls(size)
            }
        }
    }
}