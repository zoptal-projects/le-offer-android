package com.suresh.innapp_purches.Inn_App_billing

import android.os.Parcel
import android.os.Parcelable
import org.json.JSONObject

class SkuDetails : Parcelable {
    val productId: String?
    val title: String
    val description: String
    val isSubscription: Boolean
    val currency: String
    val priceValue: Double

    /**
     * Use this value to return the raw price from the product.
     * This allows math to be performed without needing to worry about errors
     * caused by floating point representations of the product's price.
     *
     * This is in micros from the Play Store.
     */
    val priceLong: Long
    val priceText: String

    constructor(source: JSONObject) {
        var responseType = source.optString(Constants.RESPONSE_TYPE)
        if (responseType == null) responseType = Constants.PRODUCT_TYPE_MANAGED
        productId = source.optString(Constants.RESPONSE_PRODUCT_ID)
        title = source.optString(Constants.RESPONSE_TITLE)
        description = source.optString(Constants.RESPONSE_DESCRIPTION)
        isSubscription = responseType.equals(Constants.PRODUCT_TYPE_SUBSCRIPTION, ignoreCase = true)
        currency = source.optString(Constants.RESPONSE_PRICE_CURRENCY)
        priceLong = source.optLong(Constants.RESPONSE_PRICE_MICROS)
        priceValue = priceLong / 1000000.0
        priceText = source.optString(Constants.RESPONSE_PRICE)
    }

    override fun toString(): String {
        return String.format("%s: %s(%s) %f in %s (%s)", productId, title, description, priceValue, currency, priceText)
    }

    override fun equals(o: Any?): Boolean {
        if (this === o) return true
        if (o == null || javaClass != o.javaClass) return false
        val that = o as SkuDetails
        return if (isSubscription != that.isSubscription) false else !if (productId != null) productId != that.productId else that.productId != null
    }

    override fun hashCode(): Int {
        var result = productId?.hashCode() ?: 0
        result = 31 * result + if (isSubscription) 1 else 0
        return result
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeString(productId)
        dest.writeString(title)
        dest.writeString(description)
        dest.writeByte(if (isSubscription) 1.toByte() else 0.toByte())
        dest.writeString(currency)
        dest.writeDouble(priceValue)
        dest.writeLong(priceLong)
        dest.writeString(priceText)
    }

    protected constructor(`in`: Parcel) {
        productId = `in`.readString()
        title = `in`.readString()
        description = `in`.readString()
        isSubscription = `in`.readByte().toInt() != 0
        currency = `in`.readString()
        priceValue = `in`.readDouble()
        priceLong = `in`.readLong()
        priceText = `in`.readString()
    }

    companion object {
        val CREATOR: Parcelable.Creator<SkuDetails?> = object : Parcelable.Creator<SkuDetails?> {
            override fun createFromParcel(source: Parcel): SkuDetails? {
                return SkuDetails(source)
            }

            override fun newArray(size: Int): Array<SkuDetails?> {
                return arrayOfNulls(size)
            }
        }
    }
}