package com.suresh.innapp_purches.Inn_App_billing

import android.content.Context
import android.text.TextUtils
import java.util.*
import java.util.regex.Pattern

internal class BillingCache(context: Context?, key: String) : BillingBase(context) {
    private val data: HashMap<String?, PurchaseInfo>
    private val cacheKey: String
    private var version: String? = null
    private val preferencesCacheKey: String
        private get() = preferencesBaseKey.toString() + cacheKey

    private val preferencesVersionKey: String
        private get() = preferencesCacheKey + VERSION_KEY

    private fun load() {
        for (entry in loadString(preferencesCacheKey, "")!!.split(Pattern.quote(ENTRY_DELIMITER))) {
            if (!TextUtils.isEmpty(entry)) {
                val parts = entry.split(Pattern.quote(LINE_DELIMITER)).toTypedArray()
                if (parts.size > 2) {
                    data[parts.get(0)] = PurchaseInfo(parts.get(1), parts.get(2))
                } else if (parts.size > 1) {
                    data[parts.get(0)] = PurchaseInfo(parts.get(1), null)
                }
            }
        }
        version = currentVersion
    }

    private fun flush() {
        val output = ArrayList<String?>()
        for (productId in data.keys) {
            val info: PurchaseInfo? = data[productId]
            output.add(productId + LINE_DELIMITER + info!!.responseData + LINE_DELIMITER + info!!.signature)
        }
        saveString(preferencesCacheKey, TextUtils.join(ENTRY_DELIMITER, output))
        version = java.lang.Long.toString(Date().time)
        saveString(preferencesVersionKey, version)
    }

    fun includesProduct(productId: String?): Boolean {
        reloadDataIfNeeded()
        return data.containsKey(productId)
    }

    fun getDetails(productId: String?): PurchaseInfo? {
        reloadDataIfNeeded()
        return if (data.containsKey(productId)) data[productId] else null
    }

    fun put(productId: String?, details: String, signature: String?) {
        reloadDataIfNeeded()
        if (!data.containsKey(productId)) {
            data[productId] = PurchaseInfo(details, signature)
            flush()
        }
    }

    fun remove(productId: String?) {
        reloadDataIfNeeded()
        if (data.containsKey(productId)) {
            data.remove(productId)
            flush()
        }
    }

    fun clear() {
        reloadDataIfNeeded()
        data.clear()
        flush()
    }

    private val currentVersion: String
        private get() = loadString(preferencesVersionKey, "0")!!

    private fun reloadDataIfNeeded() {
        if (!version.equals(currentVersion, ignoreCase = true)) {
            data.clear()
            load()
        }
    }

    val contents: List<String?>
        get() = ArrayList(data.keys)

    override fun toString(): String {
        return TextUtils.join(", ", data.keys)
    }

    companion object {
        private const val ENTRY_DELIMITER = "#####"
        private const val LINE_DELIMITER = ">>>>>"
        private const val VERSION_KEY = ".version"
    }

    init {
        data = HashMap<String?, PurchaseInfo>()
        cacheKey = key
        load()
    }
}