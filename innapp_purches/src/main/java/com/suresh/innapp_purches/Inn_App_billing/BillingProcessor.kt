package com.suresh.innapp_purches.Inn_App_billing

import android.app.Activity
import android.app.PendingIntent
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.os.RemoteException
import android.text.TextUtils
import android.util.Log
import com.android.vending.billing.IInAppBillingService
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class BillingProcessor(context: Context, licenseKey: String?, merchantId: String?, handler: IBillingHandler?) : BillingBase(context) {
    /**
     * Callback methods where billing events are reported.
     * Apps must implement one of these to construct a BillingProcessor.
     */
    interface IBillingHandler {
        fun onProductPurchased(productId: String?, details: TransactionDetails?)
        fun onPurchaseHistoryRestored()
        fun onBillingError(errorCode: Int, error: Throwable?)
        fun onBillingInitialized()
    }

    private var billingService: IInAppBillingService? = null
    private val contextPackageName: String
    private val signatureBase64: String?
    private val cachedProducts: BillingCache
    private val cachedSubscriptions: BillingCache
    private var eventHandler: IBillingHandler?=null
    private val developerMerchantId: String?
    private var isSubsUpdateSupported = false
    private var mcontext: Context? = null
    private val serviceConnection: ServiceConnection? = object : ServiceConnection {
        override fun onServiceDisconnected(name: ComponentName) {
            billingService = null
        }

        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            billingService = IInAppBillingService.Stub.asInterface(service)
            loadOwnedPurchasesFromGoogle()
            if (!isPurchaseHistoryRestored && loadOwnedPurchasesFromGoogle()) {
                setPurchaseHistoryRestored()
                eventHandler?.onPurchaseHistoryRestored()
            }
            eventHandler?.onBillingInitialized()
        }
    }

    constructor(context: Context, licenseKey: String?, handler: IBillingHandler?) : this(context, licenseKey, null, handler) {}

    private fun bindPlayServices() {
        try {
            val iapIntent = Intent("com.android.vending.billing.InAppBillingService.BIND")
            iapIntent.setPackage("com.android.vending")
            mcontext!!.bindService(iapIntent, serviceConnection, Context.BIND_AUTO_CREATE)
        } catch (e: Exception) {
            Log.e(LOG_TAG, e.toString())
        }
    }

    override fun release() {
        if (serviceConnection != null && context != null) {
            try {
                context!!.unbindService(serviceConnection)
            } catch (e: Exception) {
                Log.e(LOG_TAG, e.toString())
            }
            billingService = null
        }
        cachedProducts.release()
        super.release()
    }

    val isInitialized: Boolean
        get() = billingService != null

    fun isPurchased(productId: String?): Boolean {
        return cachedProducts.includesProduct(productId)
    }

    fun isSubscribed(productId: String?): Boolean {
        return cachedSubscriptions.includesProduct(productId)
    }

    fun listOwnedProducts(): List<String?> {
        return cachedProducts.contents
    }

    fun listOwnedSubscriptions(): List<String?> {
        return cachedSubscriptions.contents
    }

    private fun loadPurchasesByType(type: String, cacheStorage: BillingCache): Boolean {
        if (!isInitialized) return false
        try {
            val bundle: Bundle = billingService!!.getPurchases(Constants.GOOGLE_API_VERSION, contextPackageName, type, null)
            if (bundle.getInt(Constants.RESPONSE_CODE) == Constants.BILLING_RESPONSE_RESULT_OK) {
                cacheStorage.clear()
                val purchaseList = bundle.getStringArrayList(Constants.INAPP_PURCHASE_DATA_LIST)
                val signatureList = bundle.getStringArrayList(Constants.INAPP_DATA_SIGNATURE_LIST)
                if (purchaseList != null) for (i in purchaseList.indices) {
                    val jsonData = purchaseList[i]
                    if (!TextUtils.isEmpty(jsonData)) {
                        val purchase = JSONObject(jsonData)
                        val signature = if (signatureList != null && signatureList.size > i) signatureList[i] else null
                        cacheStorage.put(purchase.getString(Constants.RESPONSE_PRODUCT_ID), jsonData, signature)
                    }
                }
            }
            return true
        } catch (e: Exception) {
            eventHandler?.onBillingError(Constants.BILLING_ERROR_FAILED_LOAD_PURCHASES, e)
            Log.e(LOG_TAG, e.toString())
        }
        return false
    }

    fun loadOwnedPurchasesFromGoogle_preview() {
        loadPurchasesByType(Constants.PRODUCT_TYPE_MANAGED, cachedProducts)
        loadPurchasesByType(Constants.PRODUCT_TYPE_SUBSCRIPTION, cachedSubscriptions)
    }

    fun loadOwnedPurchasesFromGoogle(): Boolean {
        return isInitialized &&
                loadPurchasesByType(Constants.PRODUCT_TYPE_MANAGED, cachedProducts) &&
                loadPurchasesByType(Constants.PRODUCT_TYPE_SUBSCRIPTION, cachedSubscriptions)
    }

    fun purchase(activity: Activity, productId: String): Boolean {
        return purchase(activity, productId, Constants.PRODUCT_TYPE_MANAGED, null)
    }

    fun subscribe(activity: Activity, productId: String): Boolean {
        return purchase(activity, productId, Constants.PRODUCT_TYPE_SUBSCRIPTION, null)
    }

    fun purchase(activity: Activity, productId: String, developerPayload: String?): Boolean {
        return purchase(activity, productId, Constants.PRODUCT_TYPE_MANAGED, developerPayload)
    }

    fun subscribe(activity: Activity, productId: String, developerPayload: String?): Boolean {
        return purchase(activity, productId, Constants.PRODUCT_TYPE_SUBSCRIPTION, developerPayload)
    }

    // Avoid calling the service again if this value is true
    val isSubscriptionUpdateSupported: Boolean
        get() {
            // Avoid calling the service again if this value is true
            if (isSubsUpdateSupported) return true
            try {
                val response: Int = billingService!!.isBillingSupported(Constants.GOOGLE_API_SUBSCRIPTION_CHANGE_VERSION, contextPackageName, Constants.PRODUCT_TYPE_SUBSCRIPTION)
                isSubsUpdateSupported = response == Constants.BILLING_RESPONSE_RESULT_OK
            } catch (e: RemoteException) {
                e.printStackTrace()
            }
            return isSubsUpdateSupported
        }

    /**
     * Change subscription i.e. upgrade or downgrade
     *
     * @param activity the activity calling this method
     * @param oldProductId passing null or empty string will act the same as [.subscribe]
     * @param productId the new subscription id
     * @return `false` if `oldProductId` is not `null` AND change subscription
     * is not supported.
     */
    fun updateSubscription(activity: Activity?, oldProductId: String?, productId: String): Boolean {
        var oldProductIds: MutableList<String?>? = null
        if (!TextUtils.isEmpty(oldProductId)) {
            oldProductIds = ArrayList(1)
            oldProductIds.add(oldProductId)
        }
        return updateSubscription(activity, oldProductIds, productId)
    }

    /**
     * Change subscription i.e. upgrade or downgrade
     *
     * @param activity the activity calling this method
     * @param oldProductIds passing null will act the same as [.subscribe]
     * @param productId the new subscription id
     * @return `false` if `oldProductIds` is not `null` AND change subscription
     * is not supported.
     */
    fun updateSubscription(activity: Activity?, oldProductIds: List<String?>?, productId: String): Boolean {
        return if (oldProductIds != null && !isSubscriptionUpdateSupported) false else purchase(activity, oldProductIds, productId, Constants.PRODUCT_TYPE_SUBSCRIPTION, null)
    }

    private fun purchase(activity: Activity, productId: String, purchaseType: String,
                         developerPayload: String?): Boolean {
        return purchase(activity, null, productId, purchaseType, developerPayload)
    }

    private fun purchase(activity: Activity?, oldProductIds: List<String?>?, productId: String, purchaseType: String, developerPayload: String?): Boolean {
        if (!isInitialized || TextUtils.isEmpty(productId) || TextUtils.isEmpty(purchaseType)) return false
        try {
            var purchasePayload = "$purchaseType:$productId"
            if (purchaseType != Constants.PRODUCT_TYPE_SUBSCRIPTION) {
                purchasePayload += ":" + UUID.randomUUID().toString()
            }
            if (developerPayload != null) {
                purchasePayload += ":$developerPayload"
            }
            savePurchasePayload(purchasePayload)
            val bundle: Bundle
            bundle = if (oldProductIds != null && purchaseType == Constants.PRODUCT_TYPE_SUBSCRIPTION) billingService!!.getBuyIntentToReplaceSkus(Constants.GOOGLE_API_SUBSCRIPTION_CHANGE_VERSION, contextPackageName, oldProductIds, productId, purchaseType, purchasePayload) else billingService!!.getBuyIntent(Constants.GOOGLE_API_VERSION, contextPackageName, productId, purchaseType, purchasePayload)
            if (bundle != null) {
                val response = bundle.getInt(Constants.RESPONSE_CODE)
                if (response == Constants.BILLING_RESPONSE_RESULT_OK) {
                    val pendingIntent = bundle.getParcelable<PendingIntent>(Constants.BUY_INTENT)
                    if (activity != null && pendingIntent != null) activity.startIntentSenderForResult(pendingIntent.intentSender, PURCHASE_FLOW_REQUEST_CODE, Intent(), 0, 0, 0) else eventHandler?.onBillingError(Constants.BILLING_ERROR_LOST_CONTEXT, null)
                } else if (response == Constants.BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED) {
                    if (!isPurchased(productId) && !isSubscribed(productId)) loadOwnedPurchasesFromGoogle()
                    var details: TransactionDetails? = getPurchaseTransactionDetails(productId)
                    if (!checkMerchant(details)) {
                        Log.i(LOG_TAG, "Invalid or tampered merchant id!")
                        eventHandler?.onBillingError(Constants.BILLING_ERROR_INVALID_MERCHANT_ID, null)
                        return false
                    }
                    if (eventHandler != null) {
                        if (details == null) details = getSubscriptionTransactionDetails(productId)
                        eventHandler?.onProductPurchased(productId, details)
                    }
                } else eventHandler?.onBillingError(Constants.BILLING_ERROR_FAILED_TO_INITIALIZE_PURCHASE, null)
            }
            return true
        } catch (e: Exception) {
            Log.e(LOG_TAG, e.toString())
            eventHandler?.onBillingError(Constants.BILLING_ERROR_OTHER_ERROR, e)
        }
        return false
    }

    /**
     * Checks merchant's id validity. If purchase was generated by Freedom alike program it doesn't know
     * real merchant id, unless publisher GoogleId was hacked
     * If merchantId was not supplied function checks nothing
     *
     * @param details TransactionDetails
     * @return boolean
     */
    private fun checkMerchant(details: TransactionDetails?): Boolean {
        if (developerMerchantId == null) //omit merchant id checking
            return true
        if (details!!.purchaseTime!!.before(DATE_MERCHANT_LIMIT_1)) //new format [merchantId].[orderId] applied or not?
            return true
        if (details!!.purchaseTime!!.after(DATE_MERCHANT_LIMIT_2)) //newest format applied
            return true
        if (details!!.orderId == null || details!!.orderId!!.trim().length === 0) return false
        val index: Int = details!!.orderId!!.indexOf('.')
        if (index <= 0) return false //protect on missing merchant id
        //extract merchant id
        val merchantId: String = details!!.orderId!!.substring(0, index)
        return merchantId.compareTo(developerMerchantId) == 0
    }

    private fun getPurchaseTransactionDetails(productId: String, cache: BillingCache): TransactionDetails? {
        val details: PurchaseInfo = cache.getDetails(productId)!!
        if (details != null && !TextUtils.isEmpty(details.responseData)) {
            try {
                return TransactionDetails(details)
            } catch (e: JSONException) {
                Log.e(LOG_TAG, "Failed to load saved purchase details for $productId")
            }
        }
        return null
    }

    fun consumePurchase(productId: String): Boolean {
        if (!isInitialized) return false
        try {
            val transactionDetails: TransactionDetails? = getPurchaseTransactionDetails(productId, cachedProducts)
            if (transactionDetails != null && !TextUtils.isEmpty(transactionDetails.purchaseToken)) {
                val response: Int = billingService!!.consumePurchase(Constants.GOOGLE_API_VERSION, contextPackageName, transactionDetails.purchaseToken)
                if (response == Constants.BILLING_RESPONSE_RESULT_OK) {
                    cachedProducts.remove(productId)
                    Log.d(LOG_TAG, "Successfully consumed $productId purchase.")
                    return true
                } else {
                    eventHandler?.onBillingError(response, null)
                    Log.e(LOG_TAG, String.format("Failed to consume %s: error %d", productId, response))
                }
            }
        } catch (e: Exception) {
            Log.e(LOG_TAG, e.toString())
            eventHandler?.onBillingError(Constants.BILLING_ERROR_CONSUME_FAILED, e)
        }
        return false
    }

    private fun getSkuDetails(productId: String, purchaseType: String): SkuDetails? {
        val productIdList = ArrayList<String>()
        productIdList.add(productId)
        val skuDetailsList: List<SkuDetails>? = getSkuDetails(productIdList, purchaseType)
        return if (skuDetailsList != null && skuDetailsList.size > 0) skuDetailsList[0] else null
    }

    private fun getSkuDetails(productIdList: ArrayList<String>?, purchaseType: String): List<SkuDetails>? {
        if (billingService != null && productIdList != null && productIdList.size > 0) {
            try {
                val products = Bundle()
                products.putStringArrayList(Constants.PRODUCTS_LIST, productIdList)
                val skuDetails: Bundle = billingService!!.getSkuDetails(Constants.GOOGLE_API_VERSION, contextPackageName, purchaseType, products)
                val response = skuDetails.getInt(Constants.RESPONSE_CODE)
                if (response == Constants.BILLING_RESPONSE_RESULT_OK) {
                    val productDetails: ArrayList<SkuDetails> = ArrayList<SkuDetails>()
                    val detailsList: List<String>? = skuDetails.getStringArrayList(Constants.DETAILS_LIST)
                    if (detailsList != null) for (responseLine in detailsList) {
                        val `object` = JSONObject(responseLine)
                        val product = SkuDetails(`object`)
                        productDetails.add(product)
                    }
                    return productDetails
                } else {
                    eventHandler?.onBillingError(response, null)
                    Log.e(LOG_TAG, String.format("Failed to retrieve info for %d products, %d", productIdList.size, response))
                }
            } catch (e: Exception) {
                Log.e(LOG_TAG, String.format("Failed to call getSkuDetails %s", e.toString()))
                eventHandler?.onBillingError(Constants.BILLING_ERROR_SKUDETAILS_FAILED, e)
            }
        }
        return null
    }

    fun getPurchaseListingDetails(productId: String): SkuDetails? {
        return getSkuDetails(productId, Constants.PRODUCT_TYPE_MANAGED)
    }

    fun getSubscriptionListingDetails(productId: String): SkuDetails? {
        return getSkuDetails(productId, Constants.PRODUCT_TYPE_SUBSCRIPTION)
    }

    fun getPurchaseListingDetails(productIdList: ArrayList<String>?): List<SkuDetails>? {
        return getSkuDetails(productIdList, Constants.PRODUCT_TYPE_MANAGED)
    }

    fun getSubscriptionListingDetails(productIdList: ArrayList<String>?): List<SkuDetails>? {
        return getSkuDetails(productIdList, Constants.PRODUCT_TYPE_SUBSCRIPTION)
    }

    fun getPurchaseTransactionDetails(productId: String): TransactionDetails? {
        return getPurchaseTransactionDetails(productId, cachedProducts)
    }

    fun getSubscriptionTransactionDetails(productId: String): TransactionDetails? {
        return getPurchaseTransactionDetails(productId, cachedSubscriptions)
    }

    fun handleActivityResult(requestCode: Int, resultCode: Int, data: Intent?): Boolean {
        if (requestCode != PURCHASE_FLOW_REQUEST_CODE) return false
        if (data == null) {
            Log.e(LOG_TAG, "handleActivityResult: data is null!")
            return false
        }
        val responseCode = data.getIntExtra(Constants.RESPONSE_CODE, Constants.BILLING_RESPONSE_RESULT_OK)
        Log.d(LOG_TAG, String.format("resultCode = %d, responseCode = %d", resultCode, responseCode))
        val purchasePayload = purchasePayload
        if (resultCode == Activity.RESULT_OK && responseCode == Constants.BILLING_RESPONSE_RESULT_OK && !TextUtils.isEmpty(purchasePayload)) {
            val purchaseData = data.getStringExtra(Constants.INAPP_PURCHASE_DATA)
            val dataSignature = data.getStringExtra(Constants.RESPONSE_INAPP_SIGNATURE)
            try {
                val purchase = JSONObject(purchaseData)
                val productId = purchase.getString(Constants.RESPONSE_PRODUCT_ID)
                var developerPayload = purchase.getString(Constants.RESPONSE_PAYLOAD)
                if (developerPayload == null) developerPayload = ""
                val purchasedSubscription: Boolean = purchasePayload.startsWith(Constants.PRODUCT_TYPE_SUBSCRIPTION)
                if (purchasePayload == developerPayload) {
                    if (verifyPurchaseSignature(productId, purchaseData, dataSignature)) {
                        val cache: BillingCache = if (purchasedSubscription) cachedSubscriptions else cachedProducts
                        cache.put(productId, purchaseData, dataSignature)
                        eventHandler?.onProductPurchased(productId, TransactionDetails(PurchaseInfo(purchaseData, dataSignature)))
                    } else {
                        Log.e(LOG_TAG, "Public key signature doesn't match!")
                        eventHandler?.onBillingError(Constants.BILLING_ERROR_INVALID_SIGNATURE, null)
                    }
                } else {
                    Log.e(LOG_TAG, String.format("Payload mismatch: %s != %s", purchasePayload, developerPayload))
                    eventHandler?.onBillingError(Constants.BILLING_ERROR_INVALID_SIGNATURE, null)
                }
            } catch (e: Exception) {
                Log.e(LOG_TAG, e.toString())
                eventHandler?.onBillingError(Constants.BILLING_ERROR_OTHER_ERROR, e)
            }
        } else {
            eventHandler?.onBillingError(responseCode, null)
        }
        return true
    }

    private fun verifyPurchaseSignature(productId: String, purchaseData: String, dataSignature: String?): Boolean {
        return try {
            /*
             * Skip the signature check if the provided License Key is NULL and return true in order to
             * continue the purchase flow
             */
            TextUtils.isEmpty(signatureBase64) || Security.verifyPurchase(productId, signatureBase64, purchaseData, dataSignature)
        } catch (e: Exception) {
            false
        }
    }

    fun isValidTransactionDetails(transactionDetails: TransactionDetails): Boolean {
        return verifyPurchaseSignature(transactionDetails.productId,
                transactionDetails.purchaseInfo.responseData,
                transactionDetails.purchaseInfo.signature) &&
                checkMerchant(transactionDetails)
    }

    private val isPurchaseHistoryRestored: Boolean
        private get() = loadBoolean(preferencesBaseKey.toString() + RESTORE_KEY, false)

    fun setPurchaseHistoryRestored() {
        saveBoolean(preferencesBaseKey.toString() + RESTORE_KEY, true)
    }

    private fun savePurchasePayload(value: String) {
        saveString(preferencesBaseKey.toString() + PURCHASE_PAYLOAD_CACHE_KEY, value)
    }

    private val purchasePayload: String
        private get() = loadString(preferencesBaseKey.toString() + PURCHASE_PAYLOAD_CACHE_KEY, null)!!

    companion object {
        private val DATE_MERCHANT_LIMIT_1 = Date(2012, 12, 5) //5th December 2012
        private val DATE_MERCHANT_LIMIT_2 = Date(2015, 7, 20) //21st July 2015
        private const val PURCHASE_FLOW_REQUEST_CODE = 20162
        private const val LOG_TAG = "iabv3"
        private const val SETTINGS_VERSION = ".v2_6"
        private const val RESTORE_KEY = ".products.restored$SETTINGS_VERSION"
        private const val MANAGED_PRODUCTS_CACHE_KEY = ".products.cache$SETTINGS_VERSION"
        private const val SUBSCRIPTIONS_CACHE_KEY = ".subscriptions.cache$SETTINGS_VERSION"
        private const val PURCHASE_PAYLOAD_CACHE_KEY = ".purchase.last$SETTINGS_VERSION"
        fun isIabServiceAvailable(context: Context): Boolean {
            val packageManager = context.packageManager
            val intent = Intent("com.android.vending.billing.InAppBillingService.BIND")
            val list = packageManager.queryIntentServices(intent, 0)
            return list.size > 0
        }
    }

    init {
        mcontext = context
        signatureBase64 = licenseKey
        eventHandler = handler
        contextPackageName = context.applicationContext.packageName
        cachedProducts = BillingCache(context, MANAGED_PRODUCTS_CACHE_KEY)
        cachedSubscriptions = BillingCache(context, SUBSCRIPTIONS_CACHE_KEY)
        developerMerchantId = merchantId
        bindPlayServices()
    }
}