package com.suresh.innapp_purches.Inn_App_billing

import android.os.Parcel
import android.os.Parcelable
import org.json.JSONObject
import java.util.*

class TransactionDetails : Parcelable {
    val productId: String
    val orderId: String?
    val purchaseToken: String
    val purchaseTime: Date?
    val purchaseInfo: PurchaseInfo

    constructor(info: PurchaseInfo) {
        val source = JSONObject(info.responseData)
        purchaseInfo = info
        productId = source.getString(Constants.RESPONSE_PRODUCT_ID)
        orderId = source.optString(Constants.RESPONSE_ORDER_ID)
        purchaseToken = source.getString(Constants.RESPONSE_PURCHASE_TOKEN)
        purchaseTime = Date(source.getLong(Constants.RESPONSE_PURCHASE_TIME))
    }

    override fun toString(): String {
        return java.lang.String.format("%s purchased at %s(%s). Token: %s, Signature: %s", productId, purchaseTime, orderId, purchaseToken, purchaseInfo.signature)
    }

    override fun equals(o: Any?): Boolean {
        if (this === o) return true
        if (o == null || javaClass != o.javaClass) return false
        val details = o as TransactionDetails
        return !if (orderId != null) orderId != details.orderId else details.orderId != null
    }

    override fun hashCode(): Int {
        return orderId?.hashCode() ?: 0
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeString(productId)
        dest.writeString(orderId)
        dest.writeString(purchaseToken)
        dest.writeLong(purchaseTime?.time ?: -1)
        dest.writeParcelable(purchaseInfo, flags)
    }

    protected constructor(`in`: Parcel) {
        productId = `in`.readString()
        orderId = `in`.readString()
        purchaseToken = `in`.readString()
        val tmpPurchaseTime = `in`.readLong()
        purchaseTime = if (tmpPurchaseTime == -1L) null else Date(tmpPurchaseTime)
        purchaseInfo = `in`.readParcelable(PurchaseInfo::class.java.getClassLoader())
    }

    companion object {
        val CREATOR: Parcelable.Creator<TransactionDetails?> = object : Parcelable.Creator<TransactionDetails?> {
            override fun createFromParcel(source: Parcel): TransactionDetails? {
                return TransactionDetails(source)
            }

            override fun newArray(size: Int): Array<TransactionDetails?> {
                return arrayOfNulls(size)
            }
        }
    }
}