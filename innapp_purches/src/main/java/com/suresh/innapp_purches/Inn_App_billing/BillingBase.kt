package com.suresh.innapp_purches.Inn_App_billing

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import java.lang.ref.WeakReference

public open class BillingBase(context: Context?) {
    private val contextReference: WeakReference<Context?>?
    val context: Context?
        get() = contextReference!!.get()

    protected val preferencesBaseKey: String
        protected get() = contextReference!!.get()!!.packageName + "_preferences"

    private val preferences: SharedPreferences?
        private get() = if (contextReference!!.get() != null) PreferenceManager.getDefaultSharedPreferences(contextReference.get()) else null

    public open fun release() {
        contextReference?.clear()
    }

    protected fun saveString(key: String?, value: String?): Boolean {
        val sp = preferences
        if (sp != null) {
            val spe = sp.edit()
            spe.putString(key, value)
            spe.commit()
            return true
        }
        return false
    }

    protected fun loadString(key: String?, defValue: String?): String? {
        val sp = preferences
        return if (sp != null) sp.getString(key, defValue) else defValue
    }

    protected fun saveBoolean(key: String?, value: Boolean?): Boolean {
        val sp = preferences
        if (sp != null) {
            val spe = sp.edit()
            spe.putBoolean(key, value!!)
            spe.commit()
            return true
        }
        return false
    }

    protected fun loadBoolean(key: String?, defValue: Boolean): Boolean {
        val sp = preferences
        return sp?.getBoolean(key, defValue) ?: defValue
    }

    init {
        contextReference = WeakReference(context)
    }
}