package com.zoptal.cellableapp.device_camera

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.graphics.*
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import androidx.core.content.FileProvider
import co.simplecrop.android.simplecropimage.CropImage
import com.zoptal.cellableapp.utility.VariableConstants
import java.io.File
import java.io.IOException
import java.io.InputStream
import java.io.OutputStream

/**
 * <h>HandleCameraEvent</h>
 *
 *
 * In this class we used to do all operation of cameras like capture
 * image, select image from gallery. we have one method chooseImage()
 * from that we open a dialog popup to select image from camera or
 * gallery.
 *
 * @since 18-Aug-17
 * @author 3Embed
 * @version 1.0
 */
class HandleCameraEvents(private val mActivity: Activity, private val mFile: File) {

    /**
     * <h>TakePicFromCamera</h>
     *
     *
     * This method is called from selectImage method.
     * This method is helps us to capture image.afetr
     * capturing image we save that image as a file
     * in newFile variable.
     *
     */
    fun takePicture() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        try {
            val mImageCaptureUri: Uri
            val state = Environment.getExternalStorageState()
            mImageCaptureUri = if (Environment.MEDIA_MOUNTED == state) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    FileProvider.getUriForFile(mActivity, mActivity.packageName + ".provider", mFile)
                } else {
                    Uri.fromFile(mFile)
                }
            } else {
                /*
	        	 * The solution is taken from here: http://stackoverflow.com/questions/10042695/how-to-get-camera-result-as-a-uri-in-data-folder
	        	 */
                InternalStorageContentProvider.Companion.CONTENT_URI
            }
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri)
            intent.putExtra("return-data", true)
            mActivity.startActivityForResult(intent, VariableConstants.CAMERA_CAPTURE)
        } catch (e: ActivityNotFoundException) {
            Log.d(TAG, "cannot take picture", e)
        }
    }

    fun openGallery() {
        val photoPickerIntent = Intent(Intent.ACTION_PICK)
        photoPickerIntent.type = "image/*"
        mActivity.startActivityForResult(photoPickerIntent, VariableConstants.SELECT_GALLERY_IMG_REQ_CODE)
    }

    fun startCropImage() {
        val intent = Intent(mActivity, CropImage::class.java)
        intent.putExtra(CropImage.IMAGE_PATH, mFile.path)
        intent.putExtra(CropImage.SCALE, true)
        intent.putExtra(CropImage.ASPECT_X, 3)
        intent.putExtra(CropImage.ASPECT_Y, 2)
        mActivity.startActivityForResult(intent, VariableConstants.PIC_CROP)
    }

    @Throws(IOException::class)
    fun copyStream(input: InputStream, output: OutputStream) {
        val buffer = ByteArray(1024)
        var bytesRead: Int
        while (input.read(buffer).also { bytesRead = it } != -1) {
            output.write(buffer, 0, bytesRead)
        }
    }

    fun getCircleBitmap(bitmap: Bitmap): Bitmap {
        val output = Bitmap.createBitmap(bitmap.width,
                bitmap.height, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(output)
        val color = Color.RED
        val paint = Paint()
        val rect = Rect(0, 0, bitmap.width, bitmap.height)
        val rectF = RectF(rect)
        paint.isAntiAlias = true
        canvas.drawARGB(0, 0, 0, 0)
        paint.color = color
        canvas.drawOval(rectF, paint)
        paint.xfermode = PorterDuffXfermode(PorterDuff.Mode.SRC_IN)
        canvas.drawBitmap(bitmap, rect, rect, paint)
        bitmap.recycle()
        return output
    }

    companion object {
        private val TAG = HandleCameraEvents::class.java.simpleName
    }

}