package com.zoptal.cellableapp.county_code_picker

import android.app.Dialog
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.utility.ClickListener
import java.util.*

/**
 * <h>CountryPickerRvAdap</h>
 *
 *
 * In this recyclerview adpter class. We inflate single_row_select_country_code to show
 * country list.
 *
 * @since 4/12/2017
 */
internal class CountryPickerRvAdap(private var arrayListCountry: ArrayList<Country>, private val listener: SetCountryCodeListener, private val countryPickerDialog: Dialog) : RecyclerView.Adapter<CountryPickerRvAdap.MyViewHolder?>(), Filterable {
    private var myFilter: MyFilter? = null
   override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.single_row_select_country_code, parent, false)
        return MyViewHolder(view)
    }

  override  fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.tV_country_name.text = arrayListCountry[position].name
        holder.tV_country_code.text = arrayListCountry[position].code
        holder.setClickListener(object : ClickListener {
            override fun onItemClick(view: View?, position: Int) {
                println(TAG + " " + "set code=" + arrayListCountry[position].code + " " + "set name=" + arrayListCountry[position].name)
                listener.getCode(arrayListCountry[position].code, arrayListCountry[position].name)
                countryPickerDialog.dismiss()
            }
        })
    }

    override fun getItemCount(): Int {
      return  arrayListCountry.size
    }


    override fun getFilter(): Filter {
        if (myFilter == null) myFilter = MyFilter()
        return myFilter!!
    }

    private inner class MyFilter : Filter() {
        private val arrayListFilter = arrayListCountry
        override fun performFiltering(constraint: CharSequence): FilterResults {
            var constraint: CharSequence? = constraint
            val results = FilterResults()

            //CHECK CONSTRAINT VALIDITY
            if (constraint != null && constraint.length > 0) {
                //CHANGE TO UPPER
                constraint = constraint.toString().toUpperCase()
                //STORE OUR FILTERED PLAYERS
                val filteredPlayers = ArrayList<Country>()
                for (i in arrayListFilter.indices) {
                    //CHECK
                    if (arrayListFilter[i].name.toUpperCase().contains(constraint)) {
                        //ADD PLAYER TO FILTERED PLAYERS
                        filteredPlayers.add(arrayListFilter[i])
                    }
                }
                results.count = filteredPlayers.size
                results.values = filteredPlayers
            } else {
                results.count = arrayListFilter.size
                results.values = arrayListFilter
            }
            return results
        }

        override fun publishResults(constraint: CharSequence, results: FilterResults) {
            arrayListCountry = results.values as ArrayList<Country>
            notifyDataSetChanged()
        }
    }

    internal inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        val tV_country_name: TextView
        val tV_country_code: TextView
        private var clickListener: ClickListener? = null
        fun setClickListener(listener: ClickListener?) {
            clickListener = listener
        }

        override fun onClick(v: View) {
            clickListener!!.onItemClick(v, getAdapterPosition())
        }

        init {
            itemView.setOnClickListener(this)
            tV_country_name = itemView.findViewById<View>(R.id.tV_country_name) as TextView
            tV_country_code = itemView.findViewById<View>(R.id.tV_country_code) as TextView
        }
    }

    companion object {
        private val TAG = CountryPickerRvAdap::class.java.simpleName
    }

}