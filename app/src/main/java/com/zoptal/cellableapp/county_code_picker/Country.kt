package com.zoptal.cellableapp.county_code_picker

/**
 * <h>Country</h>
 *
 *
 * The pojo class for metioning country iso code and number.
 *
 * @since 04/12/2017
 */
class Country {
    var code = ""
    var name = ""

}