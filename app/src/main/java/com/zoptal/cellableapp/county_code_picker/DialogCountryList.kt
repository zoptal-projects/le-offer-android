package com.zoptal.cellableapp.county_code_picker

import android.app.Activity
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.recyclerview.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.Window
import android.widget.EditText
import androidx.recyclerview.widget.LinearLayoutManager
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.utility.CommonClass
import java.util.*

/**
 * <h>DialogCountryList</h>
 *
 *
 * In this class we used to open simple dialog pop-up to select
 * your own country code.
 *
 * @since 4/12/2017
 */
class DialogCountryList(private val mActivity: Activity, private val arrayListCountry: ArrayList<Country>) {
    private val arrayListFilter: ArrayList<Country>
    private var countryPickerDialog: Dialog? = null
    private var rV_countryList: RecyclerView? = null
    private var countryPickerRvAdap: CountryPickerRvAdap? = null

    /**
     * <h>showCountryCodePicker</h>
     *
     *
     * In this method we used to set all country iso code and number on dialog pop-up.
     *
     * @param listener The callback variable of the SetCountryCodeListener interface
     */
    fun showCountryCodePicker(listener: SetCountryCodeListener) {
        countryPickerDialog = Dialog(mActivity)
        countryPickerDialog!!.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        countryPickerDialog!!.setContentView(R.layout.dialog_show_country_list)
        countryPickerDialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        countryPickerDialog!!.window!!.setLayout((CommonClass.getDeviceWidth(mActivity) * 0.9).toInt(), (CommonClass.getDeviceHeight(mActivity) * 0.9).toInt())
        rV_countryList = countryPickerDialog!!.findViewById<View>(R.id.rV_countryList) as RecyclerView
        val layoutManager = LinearLayoutManager(mActivity)
        rV_countryList!!.setLayoutManager(layoutManager)

        //setRvAdapter(arrayListCountry,listener);
        countryPickerRvAdap = CountryPickerRvAdap(arrayListCountry, listener, countryPickerDialog!!)
        rV_countryList!!.setAdapter(countryPickerRvAdap)
        countryPickerRvAdap!!.notifyDataSetChanged()

        // Search
        val eT_searchCode = countryPickerDialog!!.findViewById<View>(R.id.eT_searchCode) as EditText
        eT_searchCode.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                val searchedText = eT_searchCode.text.toString()
                countryPickerRvAdap!!.filter.filter(searchedText)
            }

            override fun afterTextChanged(s: Editable) {}
        })
        countryPickerDialog!!.show()
    }

    companion object {
        private val TAG = DialogCountryList::class.java.simpleName
    }

    init {
        arrayListFilter = ArrayList()
    }
}