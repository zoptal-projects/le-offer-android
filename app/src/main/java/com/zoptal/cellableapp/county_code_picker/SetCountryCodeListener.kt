package com.zoptal.cellableapp.county_code_picker

/**
 * <h>ClickListener</h>
 *
 *
 * In this interface we used to define getCode method. to get code and name
 * whenever we click on recyclerview any particular row.
 *
 * @since 4/12/2017.
 */
interface SetCountryCodeListener {
    fun getCode(code: String?, name: String?)
}