package com.zoptal.cellableapp.Twiter_manager

import android.content.Context
import android.content.SharedPreferences

/**
 * @since  8/16/2017.
 * @author 3Embed.
 * @version 1.0.
 */
internal class TweeterSession(context: Context?) {
    private val sharedPreferences: SharedPreferences
    private val editor: SharedPreferences.Editor
    fun clear() {
        editor.clear()
        editor.commit()
    }

    var tweeterLoginStatus: Boolean
        get() = sharedPreferences.getBoolean(TweeterConfig.PREFERENCE_TWITTER_IS_LOGGED_IN, false)
        set(login_status) {
            editor.putBoolean(TweeterConfig.PREFERENCE_TWITTER_IS_LOGGED_IN, login_status)
            editor.commit()
        }

    var tweeterLoginOAUTH_Token: String?
        get() = sharedPreferences.getString(TweeterConfig.PREFERENCE_TWITTER_OAUTH_TOKEN, "")
        set(token) {
            editor.putString(TweeterConfig.PREFERENCE_TWITTER_OAUTH_TOKEN, token)
            editor.commit()
        }

    var tweeterLoginOAUTH_Token_Secret: String?
        get() = sharedPreferences.getString(TweeterConfig.PREFERENCE_TWITTER_OAUTH_TOKEN_SECRET, "")
        set(token) {
            editor.putString(TweeterConfig.PREFERENCE_TWITTER_OAUTH_TOKEN_SECRET, token)
            editor.commit()
        }

    var authenticatedAppName: String?
        get() = sharedPreferences.getString("APPNAME", "")
        set(app_name) {
            editor.putString("APPNAME", app_name)
            editor.commit()
        }

    init {
        val PICOGRAM_TITLE = "Tweeter"
        /*
          Mod is private.*/
        val PREFERENCE_MODE = 0
        sharedPreferences = context!!.getSharedPreferences(PICOGRAM_TITLE, PREFERENCE_MODE)
        editor = sharedPreferences.edit()
        editor.commit()
    }
}