package com.zoptal.cellableapp.Twiter_manager

import twitter4j.Twitter
import twitter4j.TwitterException
import twitter4j.TwitterFactory
import twitter4j.auth.AccessToken
import twitter4j.auth.RequestToken
import twitter4j.conf.ConfigurationBuilder

/**
 * @since  8/16/2017.
 */
internal class TwitterUtil private constructor() {
    var requestToken: RequestToken? = null
        get() {
            if (field == null) {
                try {
                    field = twitterFactory!!.instance.getOAuthRequestToken(TweeterConfig.TWITTER_CALLBACK_URL)
                } catch (e: TwitterException) {
                    e.printStackTrace()
                }
            }
            return field
        }
        private set
    var twitterFactory: TwitterFactory? = null
    var twitter: Twitter
        private set

    fun setTwitterFactory(accessToken: AccessToken?) {
        twitter = twitterFactory!!.getInstance(accessToken)
    }

    fun logout() {
        twitter.oAuthAccessToken = null
        reset()
    }

    fun reset() {
        instance = TwitterUtil()
    }

    companion object {
        var instance = TwitterUtil()
    }

    init {
        val configurationBuilder = ConfigurationBuilder()
        configurationBuilder.setOAuthConsumerKey(TweeterConfig.TWITTER_CONSUMER_KEY)
        configurationBuilder.setOAuthConsumerSecret(TweeterConfig.TWITTER_CONSUMER_SECRET)
        val configuration = configurationBuilder.build()
        twitterFactory = TwitterFactory(configuration)
        twitter = twitterFactory?.instance!!
    }
}