package com.zoptal.cellableapp.Twiter_manager

/**
 * @since  8/16/2017.
 * @author 3Embed.
 * @version 1.o.
 */
object StringUtil {
    fun isNullOrWhitespace(string: String?): Boolean {
        return string == null || string.isEmpty() || string.trim { it <= ' ' }.isEmpty()
    }
}