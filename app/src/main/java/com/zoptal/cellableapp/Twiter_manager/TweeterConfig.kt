package com.zoptal.cellableapp.Twiter_manager

/**
 * @since  8/16/2017.
 */
internal object TweeterConfig {
    var TWITTER_CONSUMER_KEY = ""
    var TWITTER_CONSUMER_SECRET = ""
    var TWITTER_CALLBACK_URL = "http://www.flontapp.oauth.com"
    var URL_PARAMETER_TWITTER_OAUTH_VERIFIER = "oauth_verifier"
    var PREFERENCE_TWITTER_OAUTH_TOKEN = "TWITTER_OAUTH_TOKEN"
    var PREFERENCE_TWITTER_OAUTH_TOKEN_SECRET = "TWITTER_OAUTH_TOKEN_SECRET"
    var PREFERENCE_TWITTER_IS_LOGGED_IN = "TWITTER_IS_LOGGED_IN"
    var STRING_EXTRA_AUTHENCATION_URL = "AuthencationUrl"
}