package com.zoptal.cellableapp.Twiter_manager


import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentTransaction
import com.zoptal.cellableapp.R

/**
 * <h2>OAuthActivity</h2>
 * <P>
 *
</P> *
 * @since  8/16/2017.
 */
class OAuthActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        val authenticationUrl: String = getIntent().getStringExtra(TweeterConfig.STRING_EXTRA_AUTHENCATION_URL)
        val fragmentTransaction: FragmentTransaction = getSupportFragmentManager().beginTransaction()
        val oAuthWebViewFragment: OAuthWebViewFragment = OAuthWebViewFragment.Companion.getInstance(authenticationUrl)
        fragmentTransaction.add(R.id.content, oAuthWebViewFragment)
        fragmentTransaction.commit()
    }
}