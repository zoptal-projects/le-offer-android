package com.zoptal.cellableapp.Twiter_manager

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.annotation.Nullable
import androidx.fragment.app.Fragment

import com.zoptal.cellableapp.R
import twitter4j.Twitter
import twitter4j.TwitterException
import twitter4j.auth.AccessToken
import twitter4j.auth.RequestToken

/**
 * <h2>OAuthWebViewFragment</h2>
 *
 *
 *
 *
 * @since  8/16/2017.
 */
class OAuthWebViewFragment : Fragment() {
    private var webView: WebView? = null
    private var authenticationUrl: String? = null
    private var mactivity: Activity? = null
    private var tweeterSession: TweeterSession? = null
    override fun onCreate(@Nullable savedInstanceState: Bundle?) {
        mactivity = getActivity()
        tweeterSession = TweeterSession(mactivity)
         super.onCreate(savedInstanceState?: Bundle())
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view = inflater.inflate(R.layout.oauth_webview, container, false)
        webView = view.findViewById<View>(R.id.webViewOAuth) as WebView
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        webView!!.loadUrl(authenticationUrl)
        webView!!.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                if (url.contains("oauth_verifier=")) {
                    initControl(Uri.parse(url))
                } else if (url.contains("denied=")) {
                    if (mactivity != null) mactivity!!.finish()
                } else {
                    view.loadUrl(url)
                }
                return true
            }
        }
        val webSettings = webView!!.settings
        webSettings.javaScriptEnabled = true
    }

    /*
     * */
    private fun initControl(uri: Uri?) {
        if (uri != null && uri.toString().startsWith(TweeterConfig.TWITTER_CALLBACK_URL)) {
            val verifier = uri.getQueryParameter(TweeterConfig.URL_PARAMETER_TWITTER_OAUTH_VERIFIER)
            TwitterGetAccessTokenTask().execute(verifier)
        } else TwitterGetAccessTokenTask().execute("")
    }

    /**
     */
    private inner class TwitterGetAccessTokenTask : AsyncTask<String?, String?, String?>() {
        override fun onPostExecute(userName: String?) {
            tweeterSession!!.authenticatedAppName=(userName)
            val intent = Intent()
            val data = Bundle()
            data.putBoolean("ISLOGIN", true)
            intent.putExtras(data)
            if (mactivity != null) {
                mactivity!!.setResult(Activity.RESULT_OK, intent)
                mactivity!!.finish()
            }
        }

        protected override fun doInBackground(vararg params: String?): String? {
            val twitter: Twitter = TwitterUtil.Companion.instance.twitter
            val requestToken: RequestToken = TwitterUtil.Companion.instance.requestToken!!
            if (!StringUtil.isNullOrWhitespace(params[0])) {
                try {
                    val accessToken = twitter.getOAuthAccessToken(requestToken, params[0])
                    tweeterSession!!.tweeterLoginOAUTH_Token=(accessToken.token)
                    tweeterSession!!.tweeterLoginOAUTH_Token_Secret=(accessToken.tokenSecret)
                    tweeterSession!!.tweeterLoginStatus=(true)
                    return twitter.showUser(accessToken.userId).name
                } catch (e: TwitterException) {
                    e.printStackTrace()
                }
            } else {
                val accessTokenString = tweeterSession!!.tweeterLoginOAUTH_Token
                val accessTokenSecret = tweeterSession!!.tweeterLoginOAUTH_Token_Secret
                val accessToken = AccessToken(accessTokenString, accessTokenSecret)
                try {
                    TwitterUtil.Companion.instance.setTwitterFactory(accessToken)
                    return TwitterUtil.Companion.instance.twitter.showUser(accessToken.userId).getName()
                } catch (e: TwitterException) {
                    e.printStackTrace()
                }
            }
            return null
        }
    }

    companion object {
        fun getInstance(authenticationUrl: String?): OAuthWebViewFragment {
            val temp = OAuthWebViewFragment()
            temp.authenticationUrl = authenticationUrl
            return temp
        }
    }
}