package com.zoptal.cellableapp.Twiter_manager

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.util.Log
import androidx.fragment.app.Fragment

import twitter4j.Status
import twitter4j.auth.AccessToken
import twitter4j.auth.RequestToken
import java.io.BufferedInputStream
import java.io.BufferedReader
import java.io.InputStream
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL

/**
 * <h2>TweetManger</h2>
 * @author 3Embed.
 * @since  8/16/2017.
 * @version 1.0.
 */
class TweetManger private constructor(context: Context, tweeterkey: String, tweeterSecrest: String) {
    private val tweeterSession: TweeterSession
    fun loggedOut() {
        TwitterUtil.Companion.instance.logout()
        tweeterSession.clear()
    }

    val isUserLoggedIn: Boolean
        get() = tweeterSession.tweeterLoginStatus

    fun doLogin(requestCode: Int, activity: Activity, fragment: Fragment?) {
        TwitterUtil.Companion.instance.reset()
        object : AsyncTask<String?, String?, RequestToken?>() {
            override fun onPostExecute(requestToken: RequestToken?) {
                val intent = Intent(activity.applicationContext, OAuthActivity::class.java)
                intent.putExtra(TweeterConfig.STRING_EXTRA_AUTHENCATION_URL, requestToken!!.authenticationURL)
                if (fragment != null) {
                    fragment.startActivityForResult(intent, requestCode)
                } else {
                    activity.startActivityForResult(intent, requestCode)
                }
            }

            protected override fun doInBackground(vararg params: String?): RequestToken? {
                return TwitterUtil.Companion.instance.requestToken
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
    }

    fun updateStatus(caption: String?, status: String?, callback: TweetSuccess?) {
        object : AsyncTask<String?, String?, Boolean>() {
            protected override fun doInBackground(vararg params: String?): Boolean {
                try {
                    val tiny_url = getTinnyUrl(params[0]!!)
                    val caption = params[1]
                    /*
                     * got the tinny url and the*/
                    val accessTokenString = tweeterSession.tweeterLoginOAUTH_Token
                    val accessTokenSecret = tweeterSession.tweeterLoginOAUTH_Token_Secret
                    if (!StringUtil.isNullOrWhitespace(accessTokenString) && !StringUtil.isNullOrWhitespace(accessTokenSecret)) {
                        val accessToken = AccessToken(accessTokenString, accessTokenSecret)
                        val status: twitter4j.Status = TwitterUtil.Companion.instance.twitterFactory?.getInstance(accessToken)!!.updateStatus("$caption $tiny_url")
                        Log.d("dfs", "" + status.retweetedStatus)
                        return true
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                return false
            }

            override fun onPostExecute(result: Boolean) {
                if (callback != null) {
                    if (result) callback.onSuccess() else callback.onFailed()
                }
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, status, caption)
    }

    @Throws(Exception::class)
    private fun getTinnyUrl(url_string: String): String? {
        var result: String? = null
        val sb = StringBuilder()
        val tinny_url = "http://tinyurl.com/api-create.php?url=$url_string"
        val url = URL(tinny_url)
        val urlConnection = url.openConnection() as HttpURLConnection
        try {
            val `in`: InputStream = BufferedInputStream(urlConnection.inputStream)
            val br = BufferedReader(InputStreamReader(`in`))
            var inputLine: String?
            while (br.readLine().also { inputLine = it } != null) {
                sb.append(inputLine)
            }
            br.close()
            `in`.close()
            result = sb.toString()
        } finally {
            urlConnection.disconnect()
        }
        return result
    }

    /**
     * Tweeter tweet success. */
    interface TweetSuccess {
        fun onSuccess()
        fun onFailed()
    }

    companion object {
        private var manger: TweetManger? = null
        @JvmStatic
        fun initialization(context: Context, key: String, secret: String) {
            manger = TweetManger(context, key, secret)
        }

        val instance: TweetManger?
            get() {
                if (manger == null) {
                }
                return manger
            }
    }

    init {
        tweeterSession = TweeterSession(context)
        TweeterConfig.TWITTER_CONSUMER_KEY = tweeterkey
        TweeterConfig.TWITTER_CONSUMER_SECRET = tweeterSecrest
    }
}