package com.zoptal.cellableapp.pojo_class.product_category

import java.io.Serializable
import java.util.*

/**
 * Created by hello on 2017-05-04.
 */
class ProductCategoryResDatas : Serializable {
    var categoryNodeId = ""
    var name = ""
    var deactiveimage = ""
    var activeimage = ""
    var isSelected = false
    var subCategoryCount = 0
    var filterCount = 0
    var filter: ArrayList<FilterData>? = null

}