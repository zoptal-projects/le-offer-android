package com.zoptal.cellableapp.pojo_class.facebook_friends_pojo

/**
 * Created by hello on 04-Jul-17.
 */
class FacebookFriendsPosts {
    /*"hashTags":null,
            "thumbnailImageUrl":null,
            "postedOn":null,
            "likes":null,
            "postCaption":null,
            "postId":null,
            "taggedUserCoordinates":null,
            "place":null,
            "type":null,
            "usersTagged":null,
            "mainUrl":null,
            "postLikedBy":null,
            "hasAudio":null,
            "comments":null*/
    var hashTags = ""
    var thumbnailImageUrl = ""
    var postedOn = ""
    var likes = ""
    var postCaption = ""
    var postId = ""
    var taggedUserCoordinates = ""
    var place = ""
    var type = ""
    var usersTagged = ""
    var mainUrl = ""
    var postLikedBy = ""
    var hasAudio = ""
    var comments = ""

}