package com.zoptal.cellableapp.pojo_class.discovery_people_pojo

import com.zoptal.cellableapp.pojo_class.UserPostDataPojo
import java.util.*

/**
 * Created by hello on 4/27/2017.
 */
class DiscoverPeopleResponse {
    /*"followsFlag":0,
"postData":[],
"postedByUserName":"zhaoxc",
"postedByUserFullName":"Yueyue Zhao",
"profilePicUrl":"https://lh3.googleusercontent.com/-HN0bPUJMl3A/AAAAAAAAAAI/AAAAAAAAAAA/AMp5VUoDgphZBY9nYzDOGnZI8BdUalqxTg/s80/photo.jpg",
"privateProfile":null,
"postedByUserEmail":"zhaoxc1115@gmail.com"*/
    var followsFlag = ""
    var postedByUserName = ""
    var postedByUserFullName = ""
    var profilePicUrl = ""
    var privateProfile = ""
    var postedByUserEmail = ""
    var postData: ArrayList<UserPostDataPojo>? = null

}