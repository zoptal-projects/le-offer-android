package com.zoptal.cellableapp.pojo_class.likedPosts

import com.zoptal.cellableapp.pojo_class.home_explore_pojo.ExploreLikedByUsersDatas
import com.zoptal.cellableapp.pojo_class.product_details_pojo.SwapPost
import java.util.*

/**
 * Created by hello on 4/10/2017.
 */
class LikedPostResponseDatas {
    /*"likeStatus":2,
"postNodeId":1461,
"label":[],
"likes":0,
"mainUrl":"https://res.cloudinary.com/dxaxmyifi/image/upload/fl_progressive:steep/v1499777772/i0ivsbmazzfp90dksy3h.jpg",
"place":"10th Cross Road, RT Nagar, Bengaluru, Karnataka 560024, India",
"thumbnailImageUrl":"https://res.cloudinary.com/dxaxmyifi/image/upload/w_150,h_150/v1499777772/i0ivsbmazzfp90dksy3h.jpg",
"postId":1499777774486,
"hashTags":"null",
"postCaption":null,
"imageCount":2,
"postedByUserName":"smita",
"postedByUserprofilePicUrl":"",
"postedByUserFullName":"smita",
"postsType":0,
"postedOn":1499777877415,
"latitude":13.028737,
"longitude":77.588929,
"city":"bengaluru",
"countrySname":"in",
"productsTaggedCoordinates":null,
"productsTagged":null,
"hasAudio":0,
"containerHeight":1280,
"containerWidth":720,
"thumbnailUrl1":null,
"imageUrl1":"https://res.cloudinary.com/dxaxmyifi/image/upload/fl_progressive:steep/v1499777773/vl9bdrlgav6qoqh7udzn.jpg",
"containerHeight1":null,
"containerWidth1":null,
"imageUrl2":null,
"thumbnailUrl2":null,
"containerHeight2":null,
"containerWidth2":null,
"thumbnailUrl3":null,
"imageUrl3":null,
"containerHeight3":null,
"containerWidth3":null,
"thumbnailUrl4":null,
"imageUrl4":null,
"containerHeight4":null,
"containerWidth4":null,
"productUrl":null,
"description":"Ipod",
"negotiable":1,
"condition":"For Parts",
"price":25000,
"currency":"INR",
"productName":"iPod",
"sold":0,
"totalComments":0,
"commentData":[],
"categoryData":[],
"likedByUsers":[]*/
    var likeStatus = ""
    var postNodeId = ""
    var likes = ""
    var mainUrl = ""
    var place = ""
    var thumbnailImageUrl = ""
    var postId = ""
    var hashTags = ""
    var postCaption = ""
    var imageCount = ""
    var postedByUserName = ""
    var postedByUserprofilePicUrl = ""
    var postedByUserFullName = ""
    var postsType = ""
    var postedOn = ""
    var latitude = ""
    var longitude = ""
    var city = ""
    var countrySname = ""
    var productsTaggedCoordinates = ""
    var productsTagged = ""
    var hasAudio = ""
    var imageUrl1 = ""
    var containerHeight = ""
    var containerWidth = ""
    var containerWidth1 = ""
    var imageUrl2 = ""
    var thumbnailUrl2 = ""
    var containerHeight2 = ""
    var containerWidth2 = ""
    var thumbnailUrl3 = ""
    var imageUrl3 = ""
    var containerHeight3 = ""
    var thumbnailUrl4 = ""
    var imageUrl4 = ""
    var containerHeight4 = ""
    var containerWidth4 = ""
    var productUrl = ""
    var title = ""
    var description = ""
    var negotiable = ""
    var condition = ""
    var price = ""
    var currency = ""
    var productName = ""
    var totalComments = ""
    var likedByUsers: ArrayList<ExploreLikedByUsersDatas>? = null
    var categoryData: ArrayList<LikedPostCategoryDatas>? = null
    var isToAddLikedItem = false
    var swapPost: ArrayList<SwapPost>? = null
    var isSwap = 0

}