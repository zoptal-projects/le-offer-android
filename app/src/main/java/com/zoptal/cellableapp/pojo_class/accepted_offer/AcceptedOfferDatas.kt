package com.zoptal.cellableapp.pojo_class.accepted_offer

import java.io.Serializable

/**
 * <h>AcceptedOfferDatas</h>
 *
 *
 * This is main pojo class for accepted offer
 *
 * @since 13-Jul-17
 */
class AcceptedOfferDatas : Serializable {
    /*"postId":1499777774486,
            "mainuUrl":"https://res.cloudinary.com/dxaxmyifi/image/upload/fl_progressive:steep/v1499777772/i0ivsbmazzfp90dksy3h.jpg",
            "thumbnailImageUrl":"https://res.cloudinary.com/dxaxmyifi/image/upload/w_150,h_150/v1499777772/i0ivsbmazzfp90dksy3h.jpg",
            "price":25000,
            "currency":"INR",
            "category":"electronics",
            "categoryImageurl":"http://138.197.65.222/public/appAssets/1496832987097.png",
            "activeImageUrl":"http://138.197.65.222/public/appAssets/1496832987098.png",
            "offerPrice":25000,
            "buyername":"jayz",
            "buyerFullName":"jayz",
            "buyerProfilePicUrl":"https://res.cloudinary.com/dxaxmyifi/image/upload/v1499932565/rrwzn2a1pdbfmyslbwyq.png",
            "buyerId":1376,
            "username":"smita",
            "offerCreatedOn":1499928180225*/
    var postId = ""
    var mainuUrl = ""
    var thumbnailImageUrl = ""
    var price = ""
    var currency = ""
    var category = ""
    var categoryImageurl = ""
    var activeImageUrl = ""
    var offerPrice = ""
    var buyername = ""
    var buyerFullName = ""
    var buyerProfilePicUrl = ""
    var buyerId = ""
    var username = ""
    var offerCreatedOn = ""

}