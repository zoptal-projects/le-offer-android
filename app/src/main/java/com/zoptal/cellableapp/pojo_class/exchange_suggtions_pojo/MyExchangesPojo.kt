package com.zoptal.cellableapp.pojo_class.exchange_suggtions_pojo

import java.util.*

/**
 * Created by ${3embed} on ${27-10-2017}.
 * Banglore
 */
class MyExchangesPojo {
    var message: String? = null
    var data: ArrayList<MyExchangesData>? = null
    var code: String? = null

    override fun toString(): String {
        return "ClassPojo [message = $message, data = $data, code = $code]"
    }
}