package com.zoptal.cellableapp.pojo_class

/**
 * Created by hello on 4/4/2017.
 */
class LoginResponsePojo {
    /*"code":200,
            "message":"Success!!",
            "token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJuYW1lIjoicmF2aSIsImVtYWlsIjoiaGlAZ21haWwuY29tIiwiaWF0IjoxNDkxMzE2Njc0LCJleHAiOjE0OTY1MDA2NzR9.t1J0lUPV9dq-qHIkdh3q8_Et_qFb8L1gFc5_0JPI5-4",
            "userId":201,
            "pushToken":"1234",
            "createdOn":1490955709710,
            "username":"ravi",
            "followers":0,
            "following":0,
            "posts":0,
            "deviceId":null,
            "isPrivate":null,
            "businessProfile":1,
            "profilePicUrl":"http://52.89.162.162//apps/Redbags/CustomerImages/i8vapldoo0j1489223562461.jpg",
            "website":null,
            "phoneNumber":"12345",
            "email":"hi@gmail.com",
            "aboutBusiness":null,
            "businessName":null,
            "place":null,
            "latitude":null,
            "longitude":null*/
    var code = ""
    var message = ""
    var token = ""
    var userId = ""
    var pushToken = ""
    var createdOn = ""
    var username = ""
    var followers = ""
    var posts = ""
    var deviceId = ""
    var isPrivate = ""
    var businessProfile = ""
    var profilePicUrl = ""
    var website = ""
    var phoneNumber = ""
    var email = ""
    var aboutBusiness = ""
    var businessName = ""
    var place = ""
    var latitude = ""
    var longitude = ""

    var mqttId: String? = null

}