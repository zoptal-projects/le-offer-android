package com.zoptal.cellableapp.pojo_class.sold_somewhere_else_pojo

/**
 * Created by hello on 24-Jul-17.
 */
class SoldSomeWhereMainPojo {
    /*"code":200,
            "message":"success, product marked as sold",
            "data":{}*/
    var code = ""
    var message = ""
    var data: SoldSomeWhereData? = null

}