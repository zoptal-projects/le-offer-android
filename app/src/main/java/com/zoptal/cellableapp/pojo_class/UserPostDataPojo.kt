package com.zoptal.cellableapp.pojo_class

import java.util.*

/**
 * Created by hello on 21-Nov-17.
 */
class UserPostDataPojo {
    /*"commenTs":null,
            "latitude":13.02863,
            "price":1000,
            "hashTags":"null",
            "containerHeight":384,
            "thumbnailImageUrl":"https://res.cloudinary.com/dxaxmyifi/image/upload/w_150,h_150/v1511259853/trsut7ufjcz1utqdkdz1.jpg",
            "postedOn":null,
            "postedByUserNodeId":2360,
            "longitude":77.589489,
            "likes":null,
            "postCaption":null,
            "containerWidth":384,
            "postId":1511259858724,
            "postNodeId":null,
            "productName":"bag",
            "taggedUserCoordinates":null,
            "currency":"INR",
            "place":"10th Cross Road, RT Nagar, Bengaluru, Karnataka 560024, India",
            "type":null,
            "usersTagged":null,
            "postLikedBy":null,
            "postLikedBy":0*/
    /*"latitude":5.340946132297079,
"likeStatus":1,
"price":8900,
"hashTags":"null",
"containerHeight":2220,
"thumbnailImageUrl":"http://res.cloudinary.com/dxaxmyifi/image/upload/q_60,w_150,h_150,c_thumb/v1511247463/u2ltopotyfjhtyanmt6c.jpg",
"postedOn":1511247464361,
"postedByUserNodeId":2348,
"longitude":-3.974937535822391,
"likes":null,
"postsType":0,
"postCaption":null,
"clickCount":0,
"containerWidth":1080,
"likedByUser":null,
"postId":1511247464932,
"postNodeId":null,
"usersTaggedInPosts":null,
"productName":"torches",
"taggedUserCoordinates":null,
"currency":"XOF",
"category":[],
"place":"D 33, Abidjan, Côte d'Ivoire",
"hasAudio":0,
"comments":null*/
    var commenTs = ""
    var latitude = ""
    var price = ""
    var hashTags = ""
    var containerHeight = ""
    var thumbnailImageUrl = ""
    var postedOn = ""
    var postedByUserNodeId = ""
    var longitude = ""
    var likes = ""
    var postCaption = ""
    var containerWidth = ""
    var postId = ""
    var postNodeId = ""
    var productName = ""
    var taggedUserCoordinates = ""
    var currency = ""
    var place = ""
    var type = ""
    var usersTagged = ""
    var postLikedBy = ""
    var likeStatus = ""
    var postsType = ""
    var clickCount = ""
    var likedByUser = ""
    var usersTaggedInPosts = ""
    var hasAudio = ""
    var mainUrl = ""
    var category: ArrayList<ProductCategoryDatas>? = null

}