package com.zoptal.cellableapp.pojo_class.home_explore_pojo

import com.facebook.ads.Ad
import com.google.android.gms.ads.formats.UnifiedNativeAd
import com.zoptal.cellableapp.pojo_class.ProductCategoryDatas
import com.zoptal.cellableapp.pojo_class.product_details_pojo.SwapPost
import java.io.Serializable
import java.util.*

/**
 * Created by hello on 4/5/2017.
 */
class ExploreResponseDatas : Serializable {
    /*"postNodeId":246,
"mainUrl":"http://res.cloudinary.com/dr49tdlw2/image/upload/v1491316748/xsoasth0slbsoed9kyfx.jpg",
"usersTaggedInPosts":null,
"place":"NH21, Kullu, Kullu and Manali, Himachal Pradesh 175101, India",
"latitude":31.957851,
"longitude":77.109459,
"thumbnailImageUrl":"http://res.cloudinary.com/dr49tdlw2/image/upload/w_150,h_150/v1491316748/xsoasth0slbsoed9kyfx.jpg",
"postId":1491316749735,
"hashTags":"null",
"postCaption":null,
"postedByUserNodeId":46,
"postedByUserName":"jayz",
"profilePicUrl":"defaultUrl",
"postsType":0,
"postedOn":1491316749289,
"postedByUserEmail":"jayz@gmail.com",
"containerWidth":640,
"containerHeight":638,
"postedByUserFullName":"Jay",
"businessProfile":1,
"hasAudio":0,
"condition":"New(never used)",
"negotiable":1,
"likeStatus":1,
"taggedUserCoordinates":null,
"category":"fashion and accessories",
"subCategory":"null",
"productUrl":null,
"price":0,
"currency":"INR",
"productName":"test",
"likes":7,
"likedByUsers":[],
"totalComments":0,
"commentData":[]*/
    var postNodeId = ""
    var mainUrl = ""
    var usersTaggedInPosts = ""
    var place = ""
    var latitude = ""
    var longitude = ""
    var thumbnailImageUrl = ""
    var postId = ""
    var membername = ""
    var isPromoted: String? = null
    var hashTags = ""
    var postCaption = ""
    var postedByUserNodeId = ""
    var postedByUserName = ""
    var profilePicUrl: String? = null
     var postsType = ""
    var postedOn = ""
    var postedByUserEmail = ""
    var containerWidth = ""
    var containerHeight = ""
    var postedByUserFullName = ""
    var businessProfile = ""
    var hasAudio = ""
    var condition = ""
    var negotiable = ""
    var likeStatus = ""
    var taggedUserCoordinates = ""
    var category = ""
    var subCategory = ""
    var productUrl = ""
    var price = ""
    var minInstallationPrice = ""
    var maxInstallationPrice = ""
    var minWigPrice = ""
    var maxWigPrice = ""
    var salon = ""
    var currency = ""
    var productName = ""
    var productUsed = ""
    var likes = ""
    var totalComments = ""
    var clickCount = ""
    var description = ""
    var followRequestStatus = ""
    var memberProfilePicUrl: String? = null
    var imageUrl1 = ""
    var thumbnailUrl1 = ""
    var containerHeight1 = ""
    var containerWidth1 = ""
    var imageUrl2 = ""
    var thumbnailUrl2 = ""
    var containerHeight2 = ""
    var containerWidth2 = ""
    var imageUrl3 = ""
    var thumbnailUrl3 = ""
    var containerHeight3 = ""
    var containerWidth3 = ""
    var thumbnailUrl4 = ""
    var imageUrl4 = ""
    var containerHeight4 = ""
    var containerWidth4 = ""
    var type = ""
    var likedByUsers: ArrayList<ExploreLikedByUsersDatas>? = null
    var categoryData: ArrayList<ProductCategoryDatas>? = null
    var isToRemoveHomeItem = false
    var swapPost: ArrayList<SwapPost>? = null
    var isSwap = 0
    var isAdd = false

    // native ads getter setter and constructor
    var unifiedNativeAd: UnifiedNativeAd? = null

    // facebook native add
    var ad: Ad? = null



}