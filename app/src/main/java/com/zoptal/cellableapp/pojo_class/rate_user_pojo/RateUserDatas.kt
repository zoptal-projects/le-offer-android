package com.zoptal.cellableapp.pojo_class.rate_user_pojo

import com.zoptal.cellableapp.pojo_class.ProductCategoryDatas
import java.util.*

/**
 * Created by hello on 11-Oct-17.
 */
class RateUserDatas {
    /*"username":"shubham",
"profilePicUrl":"https://lh3.googleusercontent.com/-6StF6_hkNFA/AAAAAAAAAAI/AAAAAAAAAAs/NwPWTiObXHc/photo.jpg",
"fullName":"Shubham Santuwala",
"pushToken":"fATfTFbIezg:APA91bFgBJfPF2WV0IWFEdfVAUuzGAYyjOgi3Sx2V3fd9Mz2uH_cws5k5oKKZnKm3JqZNCwYgtuK4mwuHYd3jzkgjS7bk2MaZ8j4Q4pYtINxJlbzK6eZsv5XYXAQDhkPdkswqA74fjTY",
"postedOn":1511273727974,
"type":0,
"postNodeId":2370,
"postId":1511271513025,
"productsTagged":null,
"place":"19, 1st Main Road, RBI Colony, Hebbal, Bengaluru, Karnataka 560024, India",
"latitude":13.0285988,
"longitude":77.5894102,
"imageCount":2,
"mainUrl":"http://res.cloudinary.com/dxaxmyifi/image/upload/v1511271511/yllv6igmmkre2jjj9osy.jpg",
"thumbnailImageUrl":"http://res.cloudinary.com/dxaxmyifi/image/upload/q_60,w_150,h_150,c_thumb/v1511271511/yllv6igmmkre2jjj9osy.jpg",
"postCaption":null,
"hashtags":"null",
"tagProduct":null,
"tagProductCoordinates":null,
"containerHeight":"415",
"containerWidth":"355",
"thumbnailUrl1":null,
"imageUrl1":"http://res.cloudinary.com/dxaxmyifi/image/upload/q_60,w_150,h_150,c_thumb/v1511271511/leincswoivnrzvjcrs7k.jpg",
"containerHeight1":null,
"containerWidth1":null,
"imageUrl2":"http://res.cloudinary.com/dxaxmyifi/image/upload/v1511271511/ox79g2vsaecxqvmodguf.jpg",
"thumbnailUrl2":null,
"containerHeight2":null,
"containerWidth2":null,
"imageUrl3":null,
"thumbnailUrl3":null,
"containerHeight3":null,
"containerWidth3":null,
"imageUrl4":null,
"thumbnailUrl4":null,
"containerHeight4":null,
"containerWidth4":null,
"productUrl":null,
"productName":"Tiffin",
"description":"tiffinhhh",
"rating":4,
"price":300,
"currency":"INR",
"sold":1,
"condition":"Used(normal wear)",
"buyername":"andlive01",
"categoryData":[],
"buyerPushToken":"fDKybGc_HL4:APA91bHZWx7zXiRrWCbgqthIJ41uUAp1BheK-RIlSzIVrECGBFGNEPk49MUwqYWJd3X4Ty2Bod-binJIZuoszDyVWQeeAVTQYy6fT5bV2U-1h2os5niP9KMnjql0Y-gp7nh8aV4hijPX",
"buyerProfilePicUrl":""*/
    var username = ""
    var profilePicUrl = ""
    var fullName = ""
    var pushToken = ""
    var postedOn = ""
    var type = ""
    var postNodeId = ""
    var postId = ""
    var productsTagged = ""
    var place = ""
    var latitude = ""
    var longitude = ""
    var imageCount = ""
    var mainUrl = ""
    var thumbnailImageUrl = ""
    var postCaption = ""
    var hashtags = ""
    var tagProduct = ""
    var tagProductCoordinates = ""
    var containerHeight = ""
    var containerWidth = ""
    var thumbnailUrl1 = ""
    var imageUrl1 = ""
    var containerHeight1 = ""
    var containerWidth1 = ""
    var imageUrl2 = ""
    var thumbnailUrl2 = ""
    var containerHeight2 = ""
    var containerWidth2 = ""
    var imageUrl3 = ""
    var thumbnailUrl3 = ""
    var containerHeight3 = ""
    var containerWidth3 = ""
    var imageUrl4 = ""
    var thumbnailUrl4 = ""
    var containerHeight4 = ""
    var containerWidth4 = ""
    var productUrl = ""
    var productName = ""
    var description = ""
    var rating = ""
    var price = ""
    var currency = ""
    var sold = ""
    var condition = ""
    var buyername = ""
    var buyerPushToken = ""
    var buyerProfilePicUrl = ""
    var categoryData: ArrayList<ProductCategoryDatas>? = null

}