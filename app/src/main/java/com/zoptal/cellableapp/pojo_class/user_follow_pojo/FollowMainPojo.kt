package com.zoptal.cellableapp.pojo_class.user_follow_pojo

import java.util.*

/**
 * Created by hello on 4/21/2017.
 */
class FollowMainPojo {
    /*"code":200,
            "message":"success",
            "result":[]*/
    var code = ""
    var message = ""
    var result: ArrayList<FollowResponseDatas>? = null
    var followers: ArrayList<FollowResponseDatas>? = null
    var memberFollowers: ArrayList<FollowResponseDatas>? = null
    var following: ArrayList<FollowResponseDatas>? = null

}