package com.zoptal.cellableapp.pojo_class.user_likes_pojo

import com.zoptal.cellableapp.pojo_class.UserPostDataPojo
import java.util.*

/**
 * Created by hello on 4/28/2017.
 */
class UserLikesResponseDatas {
    /*"username":"jayz",
            "followStatus":1,
            "memberPrivateFlag":null,
            "userFollowRequestStatus":1,
            "userStartedFollowingOn":1487592149367,
            "fullname":"jay Rathor",
            "profilePicUrl":"https://res.cloudinary.com/yelo/image/upload/v1491806167/zdmagazzoinsalkstfxa.png",
            "likedOn":1493200524664*/
    var username = ""
    var followStatus = ""
    var memberPrivateFlag = ""
    var userFollowRequestStatus = ""
    var userStartedFollowingOn = ""
    var fullname = ""
    var profilePicUrl = ""
    var likedOn = ""
    var postData: ArrayList<UserPostDataPojo>? = null

}