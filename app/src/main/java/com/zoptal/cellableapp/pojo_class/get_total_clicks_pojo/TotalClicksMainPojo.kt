package com.zoptal.cellableapp.pojo_class.get_total_clicks_pojo

import java.util.*

/**
 * Created by hello on 22-Aug-17.
 */
class TotalClicksMainPojo {
    var message: String? = null
    var data: ArrayList<TotalClicksDatas>? = null
    var code: String? = null

    override fun toString(): String {
        return "ClassPojo [message = $message, data = $data, code = $code]"
    }
}