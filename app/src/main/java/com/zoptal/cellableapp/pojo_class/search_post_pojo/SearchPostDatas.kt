package com.zoptal.cellableapp.pojo_class.search_post_pojo

import com.zoptal.cellableapp.pojo_class.home_explore_pojo.ExploreLikedByUsersDatas
import java.util.*

/**
 * Created by hello on 29-Jun-17.
 */
class SearchPostDatas {
    /*"username":"jony",
            "fullName":"Jon",
            "profilePicUrl":"https://graph.facebook.com/123975791528857/picture?type=large&return_ssl_resources=1",
            "postedOn":1498643097208,
            "postsType":0,
            "postId":1498643097663,
            "productsTagged":null,
            "place":"10th Cross Road, RT Nagar, Bengaluru, Karnataka 560024, India",
            "latitude":13.028614,
            "longitude":77.589491,
            "city":"bengaluru",
            "countrySname":"in",
            "mainUrl":"https://res.cloudinary.com/dxaxmyifi/image/upload/fl_progressive:steep/v1498643096/jgb4nhtwz3luyfnlac87.jpg",
            "thumbnailImageUrl":"https://res.cloudinary.com/dxaxmyifi/image/upload/w_150,h_150/v1498643096/jgb4nhtwz3luyfnlac87.jpg",
            "postCaption":null,
            "hashtags":"null",
            "imageCount":1,
            "containerHeight":1000,
            "containerWidth":1000,
            "productsTaggedCoordinates":null,
            "hasAudio":0,
            "category":"fashion and accessories",
            "categoryMainUrl":"http://138.197.65.222/public/appAssets/1496833004327.png",
            "cateoryActiveImageUrl":"http://138.197.65.222/public/appAssets/1496833004331.png",
            "price":100,
            "currency":"INR",
            "productName":"band",
            "thumbnailUrl1":null,
            "imageUrl1":null,
            "containerHeight1":null,
            "containerWidth1":null,
            "imageUrl2":null,
            "thumbnailUrl2":null,
            "containerHeight2":null,
            "containerWidth2":null,
            "thumbnailUrl3":null,
            "imageUrl3":null,
            "containerHeight3":null,
            "containerWidth3":null,
            "thumbnailUrl4":null,
            "imageUrl4":null,
            "containerHeight4":null,
            "containerWidth4":null,
            "commenData":[],
            "likes":1,
            "likedByUsers":[]*/
    var username = ""
    var fullName = ""
    var profilePicUrl = ""
    var postedOn = ""
    var postsType = ""
    var postId = ""
    var productsTagged = ""
    var place = ""
    var latitude = ""
    var longitude = ""
    var city = ""
    var countrySname = ""
    var mainUrl = ""
    var thumbnailImageUrl = ""
    var postCaption = ""
    var hashtags = ""
    var imageCount = ""
    var containerHeight = ""
    var containerWidth = ""
    var productsTaggedCoordinates = ""
    var hasAudio = ""
    var category = ""
    var price = ""
    var currency = ""
    var productName = ""
    var thumbnailUrl1 = ""
    var imageUrl1 = ""
    var containerHeight1 = ""
    var containerWidth1 = ""
    var imageUrl2 = ""
    var thumbnailUrl2 = ""
    var containerHeight2 = ""
    var containerWidth2 = ""
    var thumbnailUrl3 = ""
    var imageUrl3 = ""
    var containerHeight3 = ""
    var containerWidth3 = ""
    var thumbnailUrl4 = ""
    var imageUrl4 = ""
    var containerHeight4 = ""
    var containerWidth4 = ""
    var likes = ""
    var likedByUsers: ArrayList<ExploreLikedByUsersDatas>? = null

}