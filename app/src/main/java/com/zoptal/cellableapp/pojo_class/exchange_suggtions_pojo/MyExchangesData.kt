package com.zoptal.cellableapp.pojo_class.exchange_suggtions_pojo

/**
 * Created by ${3embed} on ${27-10-2017}.
 * Banglore
 */
class MyExchangesData {
    var description: String? = null
    var mainUrl: String? = null
    var postedOn: String? = null
    var productName: String? = null
    var postId: String? = null

    override fun toString(): String {
        return "ClassPojo [description = $description, mainUrl = $mainUrl, postedOn = $postedOn, productName = $productName, postId = $postId]"
    }
}