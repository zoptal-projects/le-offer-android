package com.zoptal.cellableapp.pojo_class.social_frag_pojo

import com.zoptal.cellableapp.pojo_class.ProductCategoryDatas
import com.zoptal.cellableapp.pojo_class.home_explore_pojo.ExploreLikedByUsersDatas
import java.util.*

/**
 * Created by hello on 26-Oct-17.
 */
class SocialDatas {
    /*"postNodeId":1926,  "mainUrl":"http://res.cloudinary.com/dxaxmyifi/image/upload/v1509006358/lassdnhso0c5wcv3kw46.jpg",
            "imageUrl1":null,  "imageUrl2":null,  "imageUrl3":null,  "imageUrl4":null,  "usersTaggedInPosts":null,
            "place":"19, 1st Main Road, RBI Colony, Hebbal, Bengaluru, Karnataka 560024, India",
            "latitude":13.0285975,  "longitude":77.5894218,
            "thumbnailImageUrl":"http://res.cloudinary.com/dxaxmyifi/image/upload/q_60,w_150,h_150,c_thumb/v1509006358/lassdnhso0c5wcv3kw46.jpg",
            "postId":1509006363328,  "hashTags":"null",  "postCaption":null,  "postedByUserNodeId":1582,
            "membername":"sachinchat01", "profilePicUrl":"https://lh5.googleusercontent.com/-yGleOfUh0O0/AAAAAAAAAAI/AAAAAAAAU9Y/xkE3iS6DVOw/photo.jpg",
            "memberProfilePicUrl":"http://res.cloudinary.com/dxaxmyifi/image/upload/v1506092959/n14zjdcenayeuit8cvh0.jpg",
            "postsType":0,  "postedOn":1509006362185,  "postedByUserEmail":"chhc@fyyf.guug",  "containerWidth":456,
            "containerHeight":322,  "memberFullName":"Sachinchat01",  "businessProfile":null,  "hasAudio":0, "condition":"New(never used)",
            "negotiable":1, "likeStatus":0,  "taggedUserCoordinates":null,  "categoryData":[],  "productUrl":null,
            "price":999,  "currency":"INR",  "productName":"Sling bag",  "likes":0,  "likedByUsers":[],  "clickCount":11,
            "totalComments":0,  "commentData":[]*/
    var postNodeId = ""
    var mainUrl = ""
    var imageUrl1 = ""
    var imageUrl2 = ""
    var imageUrl3 = ""
    var imageUrl4 = ""
    var usersTaggedInPosts = ""
    var place = ""
    var latitude = ""
    var longitude = ""
    var thumbnailImageUrl = ""
    var postId = ""
    var hashTags = ""
    var postCaption = ""
    var postedByUserNodeId = ""
    var membername = ""
    var profilePicUrl = ""
    var memberProfilePicUrl = ""
    var postsType = ""
    var postedOn = ""
    var postedByUserEmail = ""
    var containerWidth = ""
    var containerHeight = ""
    var memberFullName = ""
    var businessProfile = ""
    var hasAudio = ""
    var condition = ""
    var negotiable = ""
    var likeStatus = ""
    var taggedUserCoordinates = ""
    var productUrl = ""
    var price = ""
    var currency = ""
    var productName = ""
    var likes = ""
    var clickCount = ""
    var totalComments = ""
    var category = ""
    var subCategory = ""
    var categoryData: ArrayList<ProductCategoryDatas>? = null
    var likedByUsers: ArrayList<ExploreLikedByUsersDatas>? = null
    var isToAddSocialData = false

}