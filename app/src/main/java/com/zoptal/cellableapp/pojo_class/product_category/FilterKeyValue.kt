package com.zoptal.cellableapp.pojo_class.product_category

import java.io.Serializable

/**
 * Created by embed on 18/5/18.
 */
class FilterKeyValue : Serializable {
    //type use for identify number or text data in edittext.
    var type: String? = null
    var key: String
    var value: String

    constructor(key: String, value: String) {
        this.key = key
        this.value = value
    }

    constructor(type: String?, key: String, value: String) {
        this.type = type
        this.key = key
        this.value = value
    }

}