package com.zoptal.cellableapp.pojo_class.mark_selling

/**
 * Created by hello on 17-Jul-17.
 */
class MarkSellingResData {
    /*"username":"shobhitkc",
            "profilePicUrl":"http://res.cloudinary.com/dxaxmyifi/image/upload/v1499666551/xyfjs5larjawuw7neqrx.jpg",
            "fullName":"Shobhit Kumar",
            "pushToken":"dVuEIrvH1qM:APA91bEUakLVCTgQVEmCZqwyafJ7IuXzPhN6ZyMME64_HJeyH-ZoiapGZ6oLi1L32Nqnw70PukD2fqdwQfOgLRgtx_BKtnC5tbh88mh7SlILED89pPaBtVChQdp-DV3OOyqcqFL0opiI",
            "postedOn":1499759926383,
            "type":0,
            "postNodeId":1391,
            "postId":1499670720068,
            "productsTagged":null,
            "place":"University of Agricultural Sciences - UAS, RT Nagar, Bengaluru, Karnataka 560024, India",
            "latitude":13.029044,
            "longitude":77.587523,
            "imageCount":5,
            "mainUrl":"https://res.cloudinary.com/dxaxmyifi/image/upload/fl_progressive:steep/v1499670711/bwbktxqvfjpzzeoljqar.jpg",
            "thumbnailImageUrl":"https://res.cloudinary.com/dxaxmyifi/image/upload/w_150,h_150/v1499670711/bwbktxqvfjpzzeoljqar.jpg",
            "postCaption":null,
            "hashtags":"null",
            "tagProduct":null,
            "tagProductCoordinates":null,
            "containerHeight":1920,
            "containerWidth":1080,
            "thumbnailUrl1":null,
            "imageUrl1":"https://res.cloudinary.com/dxaxmyifi/image/upload/fl_progressive:steep/v1499670712/rabgcuif1dwvruvyufd0.jpg",
            "containerHeight1":null,
            "containerWidth1":null,
            "imageUrl2":"https://res.cloudinary.com/dxaxmyifi/image/upload/fl_progressive:steep/v1499670713/uz2mwx6mk80tzz5xhqfw.jpg",
            "thumbnailUrl2":null,
            "containerHeight2":null,
            "containerWidth2":null,
            "imageUrl3":"https://res.cloudinary.com/dxaxmyifi/image/upload/fl_progressive:steep/v1499670716/xahwnj8mcqvmpedxmdg2.jpg",
            "thumbnailUrl3":null,
            "containerHeight3":null,
            "containerWidth3":null,
            "imageUrl4":"https://res.cloudinary.com/dxaxmyifi/image/upload/fl_progressive:steep/v1499670717/qia2c2gqzzdzfg8siaar.jpg",
            "thumbnailUrl4":null,
            "containerHeight4":null,
            "containerWidth4":null,
            "hasAudio":0,
            "productUrl":null,
            "description":"Heyo",
            "categoryData":[],
            "price":85555,
            "currency":"INR",
            "productName":"bcmc",
            "sold":0,
            "condition":"New(Never Used)"*/
    var username = ""
    var profilePicUrl = ""
    var fullName = ""
    var pushToken = ""
    var postedOn = ""
    var type = ""
    var postNodeId = ""
    var postId = ""
    var productsTagged = ""
    var place = ""
    var latitude = ""
    var longitude = ""
    var imageCount = ""
    var mainUrl = ""
    var thumbnailImageUrl = ""
    var postCaption = ""
    var hashtags = ""
    var tagProduct = ""
    var tagProductCoordinates = ""
    var containerHeight = ""
    var containerWidth = ""
    var containerHeight1 = ""
    var containerWidth1 = ""
    var imageUrl2 = ""
    var thumbnailUrl2 = ""
    var containerHeight2 = ""
    var containerWidth2 = ""
    var imageUrl3 = ""
    var thumbnailUrl3 = ""
    var containerHeight3 = ""
    var imageUrl4 = ""
    var thumbnailUrl4 = ""
    var containerHeight4 = ""

}