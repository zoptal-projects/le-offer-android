package com.zoptal.cellableapp.pojo_class.update_product_pojo

/**
 * Created by hello on 08-Sep-17.
 */
class UpdateProductData {
    /*"username":"shobhitkc",
"profilePicUrl":"http://res.cloudinary.com/dxaxmyifi/image/upload/v1501250331/pjjvx38jrqqoiesdndzn.jpg",
"fullName":"Shobhit Kumar",
"pushToken":"eKBZesbKrkY:APA91bHhSICPEICZlcZZMZbQYz8nuqaYCarOvz_w6v3YYrXrNUAog9LHaLJGcF6g3UVYhV77_Jzu6erAklGFFczRfKP3UVMME8bJriMA-CO1w0y9Y4m9qnpiwY7kKlHmnpEWIF2HLj3Y",
"postedOn":1504873186578,
"type":0,
"condition":"Used(normal wear)",
"negotiable":1,
"postNodeId":1146,
"postId":1504609460149,
"place":"19, 1st Main Road, RBI Colony, Hebbal, Bengaluru, Karnataka 560024",
"imageCount":1,
"latitude":13.0285986,
"longitude":77.5894442,
"mainUrl":"http://res.cloudinary.com/dxaxmyifi/image/upload/v1504609458/etwebqty5af6mcgrpqyd.jpg",
"thumbnailImageUrl":"http://res.cloudinary.com/dxaxmyifi/image/upload/q_60,w_150,h_150,c_thumb/v1504609458/etwebqty5af6mcgrpqyd.jpg",
"postCaption":null,
"hashtags":"null",
"tagProduct":null,
"tagProductCoordinates":null,
"containerHeight":"532",
"containerWidth":"400",
"thumbnailUrl1":null,
"imageUrl1":null,
"containerHeight1":null,
"containerWidth1":null,
"imageUrl2":null,
"thumbnailUrl2":null,
"containerHeight2":null,
"containerWidth2":null,
"imageUrl3":null,
"thumbnailUrl3":null,
"containerHeight3":null,
"containerWidth3":null,
"imageUrl4":null,
"thumbnailUrl4":null,
"containerHeight4":null,
"containerWidth4":null,
"hasAudio":0,
"category":"electronics",
"productUrl":"http://res.cloudinary.com/dxaxmyifi/image/upload/v1504609458/etwebqty5af6mcgrpqyd.jpg",
"description":"Black color t-shirtt",
"price":2000,
"priceInUSD":31.03662225212221,
"currency":"INR",
"productName":"Coat",
"sold":0*/
    var username = ""
    var profilePicUrl = ""
    var fullName = ""
    var pushToken = ""
    var postedOn = ""
    var type = ""
    var condition = ""
    var negotiable = ""
    var postNodeId = ""
    var postId = ""
    var place = ""
    var imageCount = ""
    var latitude = ""
    var longitude = ""
    var mainUrl = ""
    var thumbnailImageUrl = ""
    var postCaption = ""
    var hashtags = ""
    var tagProduct = ""
    var tagProductCoordinates = ""
    var containerHeight = ""
    var containerWidth = ""
    var thumbnailUrl1 = ""
    var imageUrl1 = ""
    var containerHeight1 = ""
    var containerWidth1 = ""
    var imageUrl2 = ""
    var thumbnailUrl2 = ""
    var containerWidth2 = ""
    var containerHeight2 = ""
    var imageUrl3 = ""
    var thumbnailUrl3 = ""
    var containerHeight3 = ""
    var containerWidth3 = ""
    var imageUrl4 = ""
    var thumbnailUrl4 = ""
    var containerHeight4 = ""
    var containerWidth4 = ""
    var hasAudio = ""
    var category = ""
    var productUrl = ""
    var description = ""
    var price = ""
    var priceInUSD = ""
    var currency = ""
    var productName = ""
    var sold = ""

}