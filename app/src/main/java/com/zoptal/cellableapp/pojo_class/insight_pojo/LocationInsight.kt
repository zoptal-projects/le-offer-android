package com.zoptal.cellableapp.pojo_class.insight_pojo

import java.util.*

/**
 * Created by hello on 22-Aug-17.
 */
class LocationInsight {
    /*"code":200,
            "message":"success",
            "type":3,
            "typeMessage":"location insights",
            "data":[]*/
    var code = ""
    var message = ""
    var type = ""
    var typeMessage = ""
    var data: ArrayList<LocationInsightData>? = null

}