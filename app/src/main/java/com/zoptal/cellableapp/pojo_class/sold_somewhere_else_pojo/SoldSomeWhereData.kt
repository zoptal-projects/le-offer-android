package com.zoptal.cellableapp.pojo_class.sold_somewhere_else_pojo

import com.zoptal.cellableapp.pojo_class.ProductCategoryDatas
import java.util.*

/**
 * Created by hello on 24-Jul-17.
 */
class SoldSomeWhereData {
    /*""postedOn":1510754194938,
"type":0,
"postNodeId":1981,
"postId":1509349482629,
"productsTagged":null,
"place":"19, 1st Main Road, RBI Colony, Hebbal, Bengaluru, Karnataka 560024, India",
"latitude":13.0285865,
"longitude":77.5894218,
"imageCount":1,
"mainUrl":"http://res.cloudinary.com/dxaxmyifi/image/upload/v1509349480/rjf5l9tn88cdib9pn97j.jpg",
"thumbnailImageUrl":"http://res.cloudinary.com/dxaxmyifi/image/upload/q_60,w_150,h_150,c_thumb/v1509349480/rjf5l9tn88cdib9pn97j.jpg",
"postCaption":null,
"hashtags":"null",
"tagProduct":null,
"tagProductCoordinates":null,
"containerHeight":960,
"containerWidth":720,
"thumbnailUrl1":null,
"imageUrl1":null,
"containerHeight1":null,
"containerWidth1":null,
"imageUrl2":null,
"thumbnailUrl2":null,
"containerHeight2":null,
"containerWidth2":null,
"imageUrl3":null,
"thumbnailUrl3":null,
"containerHeight3":null,
"containerWidth3":null,
"imageUrl4":null,
"thumbnailUrl4":null,
"containerHeight4":null,
"containerWidth4":null,
"hasAudio":0,
"category":null,
"subCategory":null,
"productUrl":null,
"productName":"Flower",
"description":"Nice roses",
"condition":"Used(normal wear)",
"categoryData":[],
"price":200,
"currency":"INR",
"sold":2*/
    var postedOn = ""
    var type = ""
    var postNodeId = ""
    var postId = ""
    var productsTagged = ""
    var place = ""
    var latitude = ""
    var longitude = ""
    var imageCount = ""
    var mainUrl = ""
    var thumbnailImageUrl = ""
    var postCaption = ""
    var hashtags = ""
    var tagProduct = ""
    var tagProductCoordinates = ""
    var containerHeight = ""
    var containerWidth = ""
    var thumbnailUrl1 = ""
    var imageUrl1 = ""
    var containerWidth1 = ""
    var containerHeight1 = ""
    var imageUrl2 = ""
    var thumbnailUrl2 = ""
    var containerHeight2 = ""
    var containerWidth2 = ""
    var imageUrl3 = ""
    var thumbnailUrl3 = ""
    var containerHeight3 = ""
    var containerWidth3 = ""
    var imageUrl4 = ""
    var thumbnailUrl4 = ""
    var containerHeight4 = ""
    var containerWidth4 = ""
    var hasAudio = ""
    var category = ""
    var subCategory = ""
    var productUrl = ""
    var productName = ""
    var description = ""
    var condition = ""
    var price = ""
    var currency = ""
    var sold = ""
    var categoryData: ArrayList<ProductCategoryDatas>? = null

}