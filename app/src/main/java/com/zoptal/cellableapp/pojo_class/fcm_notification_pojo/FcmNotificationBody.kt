package com.zoptal.cellableapp.pojo_class.fcm_notification_pojo

/**
 * Created by hello on 24-Aug-17.
 */
class FcmNotificationBody {
    /*"campaignId":1503568131483,
            "imageUrl":"",
            "title":"Push Title",
            "message":"Push message",
            "type":73,
            "userId":1519,
            "url":"",
            "username":"ravi"*/
    var campaignId = ""
    var imageUrl = ""
    var title = ""
    var message = ""
    var type = ""
    var userId = ""
    var url = ""
    var username = ""

}