package com.zoptal.cellableapp.pojo_class.insight_pojo

/**
 * Created by hello on 22-Aug-17.
 */
class InsightData {
    /*"code":200,
            "basicInsight":{}*/
    // timeInsight{}, locationInsight{}
    var code = ""
    var basicInsight: BasicInsight? = null
    var timeInsight: TimeInsight? = null
    var locationInsight: LocationInsight? = null

}