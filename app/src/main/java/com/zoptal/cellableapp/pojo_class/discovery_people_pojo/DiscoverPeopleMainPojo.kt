package com.zoptal.cellableapp.pojo_class.discovery_people_pojo

import java.util.*

/**
 * Created by hello on 4/27/2017.
 */
class DiscoverPeopleMainPojo {
    /*"code":200,
"message":"Success",
"discoverData":[]*/
    var code = ""
    var message = ""
    var discoverData: ArrayList<DiscoverPeopleResponse>? = null

}