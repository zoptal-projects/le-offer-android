package com.zoptal.cellableapp.pojo_class.product_details_pojo

import java.io.Serializable

/**
 * Created by ${3embed} on ${27-10-2017}.
 * Banglore
 */
class SwapPost : Serializable {
    var swapPostId: String? = null
    var swapTitle: String? = null
    var itemType = false

    override fun toString(): String {
        return "ClassPojo [swapPostId = " + swapPostId + ", swapTitle = " + swapTitle + "itemType" + itemType + "]"
    }
}