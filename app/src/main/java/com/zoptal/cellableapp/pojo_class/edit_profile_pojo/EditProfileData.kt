package com.zoptal.cellableapp.pojo_class.edit_profile_pojo

/**
 * Created by hello on 10-Jul-17.
 */
class EditProfileData {
    var fullName = ""
    var profilePicUrl = ""
    var username = ""
    var websiteUrl = ""
    var bio = ""
    var email = ""
    var phoneNumber = ""
    var gender = ""
    var businessProfile = ""
    var aboutBusiness = ""
    var mainBannerImageUrl = ""
    var thumbnailImageUrl = ""
    var place = ""
    var latitude = ""
    var longitude = ""
    var googleVerified = ""
    var facebookVerified = ""
    var emailVerified = ""
    var paypalUrl = ""

}