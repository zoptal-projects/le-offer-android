package com.zoptal.cellableapp.pojo_class.profile_pojo

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by hello on 4/10/2017.
 */
class ProfilePostsData private constructor(`in`: Parcel) : Parcelable {
    /*"postNodeId":362,
            "label":[],
            "likes":0,
            "mainUrl":"http://res.cloudinary.com/yelo/image/upload/v1491805387/h0jbmanynxnah71rxwlf.jpg",
            "postLikedBy":null,
            "place":"3Embed Software Technologies",
            "thumbnailImageUrl":"http://res.cloudinary.com/yelo/image/upload/w_150,h_150/v1491805387/h0jbmanynxnah71rxwlf.jpg",
            "postId":1491805388368,
            "usersTaggedInPosts":null,
            "taggedUserCoordinates":null,
            "hasAudio":0,
            "containerHeight":960,
            "containerWidth":960,
            "hashTags":"null",
            "postCaption":null,
            "latitude":13.028318,
            "longitude":77.58929,
            "postsType":0,
            "postedOn":1491805387921,
            "likeStatus":0,
            "category":"electronics",
            "subCategory":"laptops",
            "productUrl":null,
            "price":25000,
            "currency":"INR",
            "productName":"mac mini",
            "totalComments":0,
            "commentData":[]*/
    var postNodeId: String? = ""
    var likes: String? = ""
    var mainUrl: String? = ""
    var postLikedBy: String? = ""
    var place: String? = ""
    var thumbnailImageUrl: String? = ""
    var postId: String? = ""
    var usersTaggedInPosts: String? = ""
    var taggedUserCoordinates: String? = ""
    var hasAudio: String? = ""
    var containerHeight: String? = ""
    var containerWidth: String? = ""
    var hashTags: String? = ""
    var postCaption: String? = ""
    var latitude: String? = ""
    var longitude: String? = ""
    var postsType: String? = ""
    var postedOn: String? = ""
    var likeStatus: String? = ""
    var category: String? = ""
    var subCategory: String? = ""
    var productUrl: String? = ""
    var price: String? = ""
    var currency: String? = ""
    var productName: String? = ""
    var totalComments: String? = ""
    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeString(postNodeId)
        dest.writeString(likes)
        dest.writeString(mainUrl)
        dest.writeString(postLikedBy)
        dest.writeString(place)
        dest.writeString(thumbnailImageUrl)
        dest.writeString(postId)
        dest.writeString(usersTaggedInPosts)
        dest.writeString(taggedUserCoordinates)
        dest.writeString(hasAudio)
        dest.writeString(containerHeight)
        dest.writeString(containerWidth)
        dest.writeString(hashTags)
        dest.writeString(postCaption)
        dest.writeString(latitude)
        dest.writeString(longitude)
        dest.writeString(postsType)
        dest.writeString(postedOn)
        dest.writeString(likeStatus)
        dest.writeString(category)
        dest.writeString(subCategory)
        dest.writeString(productUrl)
        dest.writeString(price)
        dest.writeString(currency)
        dest.writeString(productName)
        dest.writeString(totalComments)
    }

    companion object {
        val CREATOR: Parcelable.Creator<ProfilePostsData> = object : Parcelable.Creator<ProfilePostsData> {
            override fun createFromParcel(`in`: Parcel): ProfilePostsData? {
                return ProfilePostsData(`in`)
            }

            override fun newArray(size: Int): Array<ProfilePostsData?> {
                return arrayOfNulls(size)
            }
        }
    }

    init {
        postNodeId = `in`.readString()
        likes = `in`.readString()
        mainUrl = `in`.readString()
        postLikedBy = `in`.readString()
        place = `in`.readString()
        thumbnailImageUrl = `in`.readString()
        postId = `in`.readString()
        usersTaggedInPosts = `in`.readString()
        taggedUserCoordinates = `in`.readString()
        hasAudio = `in`.readString()
        containerHeight = `in`.readString()
        containerWidth = `in`.readString()
        hashTags = `in`.readString()
        postCaption = `in`.readString()
        latitude = `in`.readString()
        longitude = `in`.readString()
        postsType = `in`.readString()
        postedOn = `in`.readString()
        likeStatus = `in`.readString()
        category = `in`.readString()
        subCategory = `in`.readString()
        productUrl = `in`.readString()
        price = `in`.readString()
        currency = `in`.readString()
        productName = `in`.readString()
        totalComments = `in`.readString()
    }
}