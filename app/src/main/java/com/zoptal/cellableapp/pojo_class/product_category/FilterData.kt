package com.zoptal.cellableapp.pojo_class.product_category

import java.io.Serializable

/**
 * Created by embed on 17/5/18.
 */
class FilterData : Serializable {
    var fieldName: String? = null
    var values: String? = null
    var type = 0
    var isMandatory = false
    var isSelected = false
    var selectedValues = ""

}