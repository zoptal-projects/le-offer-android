package com.zoptal.cellableapp.pojo_class.explore_for_you_pojo

import com.zoptal.cellableapp.pojo_class.UserPostDataPojo
import java.util.*

/**
 * Created by hello on 4/17/2017.
 */
class ForYouResposeDatas {
    /*"username":null,
            "membername":null,
            "notificationId":null,
            "notificationMessage":null,
            "memberIsPrivate":null,
            "createdOn":null,
            "notificationType":null,
            "seenStatus":null,
            "postId":null,
            "thumbnailImageUrl":null,
            "followRequestStatus":null,
            "memberProfilePicUrl":null*/
    var username = ""
    var membername = ""
    var notificationId = ""
    var notificationMessage = ""
    var memberIsPrivate = ""
    var createdOn = ""
    var notificationType = ""
    var seenStatus = ""
    var postId = ""
    var thumbnailImageUrl = ""
    var followRequestStatus = ""
    var memberProfilePicUrl = ""
    var productName = ""
    var postData: ArrayList<UserPostDataPojo>? = null

}