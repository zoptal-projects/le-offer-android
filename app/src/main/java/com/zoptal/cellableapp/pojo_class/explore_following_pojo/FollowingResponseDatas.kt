package com.zoptal.cellableapp.pojo_class.explore_following_pojo

/**
 * Created by hello on 4/17/2017.
 */
class FollowingResponseDatas {
    /*"user1_username":"carl",
            "user1_fullName":"Reed",
            "user1_profilePicUrl":"https://graph.facebook.com/1893859380903000/picture?type=large&return_ssl_resources=1",
            "user2_username":"tran",
            "user2_fullname":"Joe",
            "user2_profilePicUrl":"https://res.cloudinary.com/dr49tdlw2/image/upload/v1490602701/mdqqgljc47smp1qm6tmv.png",
            "notificationId":1480,
            "notificationMessage":"startedFollowing",
            "createdOn":1492091404466,
            "notificationType":3,
            "seenStatus":0,
            "postsType":null,
            "postedOn":null,
            "thumbnailImageUrl":null,
            "mainUrl":null,
            "postId":null*/
    var user1_username = ""
    var user1_fullName = ""
    var user1_profilePicUrl = ""
    var user2_username = ""
    var user2_fullname = ""
    var user2_profilePicUrl = ""
    var notificationId = ""
    var notificationMessage = ""
    var createdOn = ""
    var notificationType = ""
    var seenStatus = ""
    var postsType = ""
    var postedOn = ""
    var thumbnailImageUrl = ""
    var mainUrl = ""
    var postId = ""

}