package com.zoptal.cellableapp.pojo_class.phone_contact_pojo

import java.util.*

/**
 * Created by hello on 04-Jul-17.
 */
class PhoneContactData {
    /*"Following":0,
            "membername":"jef",
            "userId":1276,
            "profilePicUrl":"https://graph.facebook.com/1893859380903000/picture?type=large&return_ssl_resources=1",
            "fullName":"Jeffrey",
            "phoneNumber":"+919582429675",
            "memberPrivate":null,
            "followRequestStatus":null,
            "userPrivate":null,
            "postData":[]*/
    var following = ""
    var membername = ""
    var userId = ""
    var profilePicUrl = ""
    var fullName = ""
    var phoneNumber = ""
    var memberPrivate = ""
    var followRequestStatus = ""
    var userPrivate = ""
    var postData: ArrayList<PhoneContactPostData>? = null

}