package com.zoptal.cellableapp.pojo_class.facebook_friends_pojo

import java.util.*

/**
 * Created by hello on 04-Jul-17.
 */
class FacebookFriendsData {
    /*"membername":"moni",
            "memberPrivate":null,
            "followRequestStatus":null,
            "pushToken":null,
            "phoneNumber":"+91985698569856",
            "deviceType":"1",
            "fullname":"Monika",
            "profilePicUrl":"https://graph.facebook.com/1849809298607268/picture?type=large&return_ssl_resources=1",
            "facebookId":"1849809298607268",
            "memberPosts":[]*/
    var membername = ""
    var memberPrivate = ""
    var followRequestStatus = ""
    var pushToken = ""
    var phoneNumber = ""
    var deviceType = ""
    var fullname = ""
    var profilePicUrl = ""
    var facebookId = ""
    var memberPosts: ArrayList<FacebookFriendsPosts>? = null

}