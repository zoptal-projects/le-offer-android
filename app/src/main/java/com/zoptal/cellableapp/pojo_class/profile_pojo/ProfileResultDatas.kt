package com.zoptal.cellableapp.pojo_class.profile_pojo

/**
 * Created by hello on 27-May-17.
 */
class ProfileResultDatas {
    /*"phoneNumber":"+919035617542",
"profilePicUrl":"http://res.cloudinary.com/dxaxmyifi/image/upload/v1500878731/z19ejraqhh8zyy7mclru.jpg",
"fullName":"Shobhit Kumar",
"email":"shobhit@mobifyi.com",
"bio":null,
"followers":0,
"following":0,
"posts":0,
"website":null,
"username":"shobhitkc",
"followStatus":1,
"paypalUrl":"https://www.paypal.me/appscrip",
"googleVerified":0,
"facebookVerified":0,
"emailVerified":0*/
    var phoneNumber = ""
    var profilePicUrl = ""
    var fullName = ""
    var email = ""
    var bio = ""
    var followers = ""
    var avgRating = ""
    var ratedBy = ""
    var following = ""
    var posts = ""
    var website = ""
    var username = ""
    var followStatus = ""
    var paypalUrl = ""
    var googleVerified = ""
    var facebookVerified = ""
    var emailVerified = ""
    var accountId = ""
    var weburl = ""
    var is_strip_connected = 0

}