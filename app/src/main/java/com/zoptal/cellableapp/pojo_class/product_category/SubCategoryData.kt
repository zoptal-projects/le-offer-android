package com.zoptal.cellableapp.pojo_class.product_category

import java.util.*

/**
 * Created by embed on 18/5/18.
 */
class SubCategoryData {
    var subCategoryName: String? = null
    var fieldCount = 0
    var imageUrl: String? = null
    var filter: ArrayList<FilterData>? = null
    var isSelectd = false

}