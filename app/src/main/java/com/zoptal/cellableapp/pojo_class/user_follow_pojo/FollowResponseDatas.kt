package com.zoptal.cellableapp.pojo_class.user_follow_pojo

import com.zoptal.cellableapp.pojo_class.UserPostDataPojo
import java.util.*

/**
 * Created by hello on 4/21/2017.
 */
class FollowResponseDatas {
    /*"username":"kim",
"fullName":"Alice",
"profilePicUrl":"https://res.cloudinary.com/yelo/image/upload/v1492147455/m4zvf6nvbdbcv6f9nwun.png",
"memberPrivateFlag":null,
"userFollowRequestStatus":null,
"userStartedFollowingOn":null,
"FollowedBack":0*/
    var username = ""
    var membername = ""
    var fullName = ""
    var fullname = ""
    var profilePicUrl = ""
    var followsFlag = ""
    var memberPrivateFlag = ""
    var userFollowRequestStatus = ""
    var userStartedFollowingOn = ""
    var followedBack: String? = null
    var postData: ArrayList<UserPostDataPojo>? = null

}