package com.zoptal.cellableapp.pojo_class.product_details_pojo

import com.zoptal.cellableapp.pojo_class.ProductCategoryDatas
import com.zoptal.cellableapp.pojo_class.home_explore_pojo.ExploreLikedByUsersDatas
import java.util.*

/**
 * Created by hello on 4/20/2017.
 */
class ProductResponseDatas {
    /*"userId":46,
            "memberId":57,
            "username":"jayz",
            "profilePicUrl":"https://res.cloudinary.com/yelo/image/upload/v1491806167/zdmagazzoinsalkstfxa.png",
            "userFullName":"jay Rathor",
            "membername":"mahadev",
            "memberProfilePicUrl":"https://res.cloudinary.com/yelo/image/upload/v1492582549/apcnk3jajudzibcss38u.png",
            "memberFullName":"mahadev",
            "followRequestStatus":null,
            "postType":0,
            "postedOn":1492683468941,
            "containerWidth":960,
            "containerHeight":960,
            "mainUrl":"http://res.cloudinary.com/yelo/image/upload/v1492683468/dcifxyf4f5sdozrexhsv.jpg",
            "postId":1492683469668,
            "productsTaggedCoordinates":null,
            "hasAudio":0,
            "productsTagged":null,
            "longitude":77.58929,
            "latitude":13.028318,
            "place":"3Embed Software Technologies",
            "thumbnailImageUrl":"http://res.cloudinary.com/yelo/image/upload/w_150,h_150/v1492683468/dcifxyf4f5sdozrexhsv.jpg",
            "hashTags":"null",
            "title":null,
            "description":null,
            "negotiable":1,
            "condition":"Used(normal wear)",
            "productUrl":null,
            "price":8000,
            "currency":"INR",
            "productName":"iphone 4s",
            "likes":0,
            "thumbnailUrl1":null,
            "imageUrl1":null,
            "containerHeight1":null,
            "containerWidth1":null,
            "imageUrl2":null,
            "thumbnailUrl2":null,
            "containerHeight2":null,
            "containerWidth2":null,
            "thumbnailUrl3":null,
            "imageUrl3":null,
            "containerHeight3":null,
            "containerWidth3":null,
            "thumbnailUrl4":null,
            "imageUrl4":null,
            "containerHeight4":null,
            "containerWidth4":null,
            "imageCount":1,
            "commentData":[],
            "categoryData":[],
            "likedByUsers":[],
            "likeStatus":0*/
    var userId = ""
    var memberId = ""
    var username = ""
    var profilePicUrl = ""
    var userFullName = ""
    var membername = ""
    var memberProfilePicUrl = ""
    var memberFullName: String? = null
    var followRequestStatus = ""
    var postType = ""
    var postedOn = ""
    var containerWidth = ""
    var containerHeight = ""
    var mainUrl = ""
    var postId = ""
    var productsTaggedCoordinates = ""
    var hasAudio = ""
    var productsTagged = ""
    var longitude = ""
    var latitude = ""
    var place = ""
    var thumbnailImageUrl = ""
    var hashTags = ""
    var title = ""
    var description = ""
    var negotiable = ""
    var condition = ""
    var price = ""
    var currency = ""
    var productName = ""
    var likes = ""
    var thumbnailUrl1 = ""
    var imageUrl1 = ""
    var containerHeight1 = ""
    var containerWidth1 = ""
    var imageUrl2 = ""
    var thumbnailUrl2 = ""
    var containerHeight2 = ""
    var containerWidth2 = ""
    var thumbnailUrl3 = ""
    var imageUrl3 = ""
    var imageUrl4 = ""
    var containerHeight3 = ""
    var containerWidth3 = ""
    var thumbnailUrl4 = ""
    var containerHeight4 = ""
    var containerWidth4 = ""
    var imageCount = ""
    var likeStatus = ""
    var clickCount = ""
    var city = ""
    var countrySname = ""
    var productUrl = ""
    var cloudinaryPublicId = ""
    var cloudinaryPublicId1 = ""
    var cloudinaryPublicId2 = ""
    var cloudinaryPublicId3 = ""
    var cloudinaryPublicId4 = ""
    var ship_address_one = ""
    var ship_country = ""
    var ship_town = ""
    var ship_address_two = ""
    var ship_state = ""
    var ship_pin_code = ""

    var showQuestion = 0
    var is_recieved = 0
    var userMqttId = ""
    var reviewCount = ""
        get() = field
        set(reviewCount) {
            field = reviewCount
        }
    var bluedart_link = ""
    var offerAccepted = 0
    var swapPost: ArrayList<SwapPost>? = null
    var isSwap = 0
    var isPromoted = 0

    var offerPrice: String? = null

    var sold:Int = 0
    var memberMqttId = ""
    var category = ""
    var subCategory = ""
    var postFilter: ArrayList<PostFilter>? = null

    var categoryData: ArrayList<ProductCategoryDatas>? = null
    var likedByUsers: ArrayList<ExploreLikedByUsersDatas>? = null

    fun getIsSold(): Int {
        return sold
    }

//    fun setSold(sold: Int) {
//        isSold = sold
//    }

}