package com.zoptal.cellableapp.pojo_class.add_review_pojo

import java.util.*

/**
 * Created by hello on 25-Jul-17.
 */
class AddReviewData {
    /*"postedByUserPushToken":null,
            "nodePostId":1428,
            "label":[],
            "postLikedBy":null,
            "containerWidth":512,
            "containerHeight":288,
            "mainUrl":"https://res.cloudinary.com/dxaxmyifi/image/upload/fl_progressive:steep/v1500984020/df14b9cev3q40maqaaeg.jpg",
            "postId":1500984022512,
            "taggedUserCoordinates":null,
            "hasAudio":0,
            "likes":null,
            "longitude":77.589553,
            "usersTagged":null,
            "latitude":13.028651,
            "place":"10th Cross Road, RT Nagar, Bengaluru, Karnataka 560024, India",
            "postCaption":null,
            "thumbnailImageUrl":"https://res.cloudinary.com/dxaxmyifi/image/upload/w_150,h_150/v1500984020/df14b9cev3q40maqaaeg.jpg",
            "hashTags":"null",
            "username":"shobhitkc",
            "fullName":"Shobhit Kumar",
            "profilePicUrl":"http://res.cloudinary.com/dxaxmyifi/image/upload/v1500878731/z19ejraqhh8zyy7mclru.jpg",
            "postedByUserName":"vabs",
            "commentData":[],
            "totalComments":3*/
    var postedByUserPushToken = ""
    var nodePostId = ""
    var postLikedBy = ""
    var containerWidth = ""
    var containerHeight = ""
    var mainUrl = ""
    var postId = ""
    var taggedUserCoordinates = ""
    var hasAudio = ""
    var likes = ""
    var longitude = ""
    var usersTagged = ""
    var latitude = ""
    var place = ""
    var postCaption = ""
    var thumbnailImageUrl = ""
    var hashTags = ""
    var username = ""
    var fullName = ""
    var profilePicUrl = ""
    var postedByUserName = ""
    var totalComments = ""
    var commentData: ArrayList<AddReviewCommentData>? = null

}