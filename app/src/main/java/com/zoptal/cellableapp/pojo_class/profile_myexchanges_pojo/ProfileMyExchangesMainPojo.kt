package com.zoptal.cellableapp.pojo_class.profile_myexchanges_pojo

import java.util.*

/**
 * Created by ${3embed} on ${27-10-2017}.
 * Banglore
 */
class ProfileMyExchangesMainPojo {
    var message: String? = null
    var data: ArrayList<ProfileMyExchangesData>? = null
    var code: String? = null

    override fun toString(): String {
        return "ClassPojo [message = $message, data = $data, code = $code]"
    }
}