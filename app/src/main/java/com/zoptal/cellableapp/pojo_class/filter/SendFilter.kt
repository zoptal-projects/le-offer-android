package com.zoptal.cellableapp.pojo_class.filter

import java.io.Serializable

/**
 * Created by embed on 23/5/18.
 */
/*
* This model used while applied filtered data to be send to server
* we take data while inserted in view or selected
* */
class SendFilter : Serializable {
    var type: String
    var filedName: String
    var value: String? = null
    var from: String? = null
    var to: String? = null

    constructor(type: String, filedName: String, value: String?) {
        this.type = type
        this.filedName = filedName
        this.value = value
    }

    constructor(type: String, filedName: String, from: String?, to: String?) {
        this.type = type
        this.filedName = filedName
        this.from = from
        this.to = to
    }

}