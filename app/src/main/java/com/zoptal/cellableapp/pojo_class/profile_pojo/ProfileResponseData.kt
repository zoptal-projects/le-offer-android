package com.zoptal.cellableapp.pojo_class.profile_pojo

/**
 * Created by hello on 4/10/2017.
 */
class ProfileResponseData {
    /*"followers":3,
"following":5,
"totalPosts":20,
"fullName":"Jay",
"profilePicUrl":"https://res.cloudinary.com/yelo/image/upload/v1491806167/zdmagazzoinsalkstfxa.png",
"username":"jayz",
"bio":null,
"websiteUrl":null,
"gender":"Male",
"private":null,
"phoneNumber":"In +919023585008",
"email":"jayz@gmail.com",
"businessProfile":1,
"businessName":null,
"aboutBusiness":null*/
    var followers = ""
    var following = ""
    var totalPosts = ""
    var fullName = ""
    var profilePicUrl = ""
    var username = ""
    var bio = ""
    var websiteUrl = ""
    var gender = ""
    var phoneNumber = ""
    var email = ""
    var businessProfile = ""
    var businessName = ""
    var aboutBusiness = ""

}