package com.zoptal.cellableapp.pojo_class

import java.io.Serializable

/**
 * <h2>CloudData</h2>
 * <P>
 * Coudinary data holder.
</P> *
 * C@since 6/28/16.
 */
class CloudData : Serializable {
    var path: String? = null
    var isVideo = false

}