package com.zoptal.cellableapp.pojo_class.profile_myexchanges_pojo

/**
 * Created by ${3embed} on ${27-10-2017}.
 * Banglore
 */
class ProfileMyExchangesData {
    var swapThumbnailImageUrl: String? = null
    var negotiable: String? = null
    var swapPostId: String? = null
    var acceptedOn: String? = null
    var swapProductName: String? = null
    var currency: String? = null
    var city: String? = null
    var imageUrl4: String? = null
    var description: String? = null
    var longitude: String? = null
    var place: String? = null
    var thumbnailImageUrl: String? = null
    var condition: String? = null
    var mainUrl: String? = null
    var imageUrl3: String? = null
    var imageUrl2: String? = null
    var postId: String? = null
    var imageUrl1: String? = null
    var swapMainUrl: String? = null
    var countrySname: String? = null
    var swapImageUrl2: String? = null
    var price: String? = null
    var swapImageUrl1: String? = null
    var swapImageUrl4: String? = null
    var swapImageUrl3: String? = null
    var priceInUSD: String? = null
    var latitude: String? = null
    var productsTagged: String? = null
    var productName: String? = null

    override fun toString(): String {
        return "ClassPojo [swapThumbnailImageUrl = $swapThumbnailImageUrl, negotiable = $negotiable, swapPostId = $swapPostId, acceptedOn = $acceptedOn, swapProductName = $swapProductName, currency = $currency, city = $city, imageUrl4 = $imageUrl4, description = $description, longitude = $longitude, place = $place, thumbnailImageUrl = $thumbnailImageUrl, condition = $condition, mainUrl = $mainUrl, imageUrl3 = $imageUrl3, imageUrl2 = $imageUrl2, postId = $postId, imageUrl1 = $imageUrl1, swapMainUrl = $swapMainUrl, countrySname = $countrySname, swapImageUrl2 = $swapImageUrl2, price = $price, swapImageUrl1 = $swapImageUrl1, swapImageUrl4 = $swapImageUrl4, swapImageUrl3 = $swapImageUrl3, priceInUSD = $priceInUSD, latitude = $latitude, productsTagged = $productsTagged, productName = $productName]"
    }
}