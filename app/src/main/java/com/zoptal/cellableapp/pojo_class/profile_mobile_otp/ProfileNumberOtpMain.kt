package com.zoptal.cellableapp.pojo_class.profile_mobile_otp

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by hello on 11-Jul-17.
 */
class ProfileNumberOtpMain {
    @SerializedName("code")
    @Expose
    var code: String? = null

    @SerializedName("message")
    @Expose
    var message: String? = null

    @SerializedName("data")
    @Expose
    var data: ProfileNumberOtpData? = null

    @SerializedName("otp")
    @Expose
    var otp: String? = null

}