package com.zoptal.cellableapp.pojo_class

import java.io.Serializable

/**
 * Created by hello on 11-Nov-17.
 */
class ProductCategoryDatas : Serializable {
    /*"category":"baby and child",
            "mainUrl":"http://138.197.65.222/public/appAssets/1496832967645.png",
            "activeImageUrl":"http://138.197.65.222/public/appAssets/1496832967650.png"*/
    var category = ""
    var mainUrl = ""
    var activeImageUrl = ""

}