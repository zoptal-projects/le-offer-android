package com.zoptal.cellableapp.pojo_class.insight_pojo

import java.util.*

/**
 * Created by hello on 22-Aug-17.
 */
class BasicInsight {
    /*"code":200,
        "type":1,
        "typeMessage":"basic insights",
        "data":[]*/
    var code = ""
    var type = ""
    var typeMessage = ""
    var data: ArrayList<BasicInsightData>? = null

}