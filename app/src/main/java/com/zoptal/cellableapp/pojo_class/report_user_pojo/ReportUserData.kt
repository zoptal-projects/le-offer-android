package com.zoptal.cellableapp.pojo_class.report_user_pojo

/**
 * Created by hello on 28-Jul-17.
 */
class ReportUserData {
    /*"_id":"5937d48cb765f3154ba1fd17",
            "reportReason":"It's prohibited on Yelo",
            "reasonDate":1496831116602*/
    private var _id = ""
    var reportReason = ""
    var reasonDate = ""
    fun get_id(): String {
        return _id
    }

    fun set_id(_id: String) {
        this._id = _id
    }

}