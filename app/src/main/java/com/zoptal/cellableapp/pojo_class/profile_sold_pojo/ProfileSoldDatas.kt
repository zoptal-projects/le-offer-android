package com.zoptal.cellableapp.pojo_class.profile_sold_pojo

import com.zoptal.cellableapp.pojo_class.ProductCategoryDatas
import java.util.*

/**
 * Created by hello on 26-Oct-17.
 */
class ProfileSoldDatas {
    /* "postNodeId":1924,
            "label":[],
            "isPromoted":0,
            "planId":null,
            "likes":null,
            "mainUrl":"http://res.cloudinary.com/dxaxmyifi/image/upload/v1509005518/vf7ldpd5hwy98e52tcbv.jpg",
            "postLikedBy":null,
            "place":"19, 1st Main Road, RBI Colony, Hebbal, Bengaluru, Karnataka 560024, India",
            "thumbnailImageUrl":"http://res.cloudinary.com/dxaxmyifi/image/upload/q_60,w_150,h_150,c_thumb/v1509005518/vf7ldpd5hwy98e52tcbv.jpg",
            "postId":1509005520480,
            "productsTagged":null,
            "productsTaggedCoordinates":null,
            "hasAudio":0,
            "containerHeight":4160,
            "containerWidth":3120,
            "hashTags":"null",
            "postCaption":null,
            "latitude":13.0285743,
            "longitude":77.5893994,
            "thumbnailUrl1":null,
            "imageUrl1":null,
            "containerHeight1":null,
            "containerWidth1":null,
            "imageUrl2":null,
            "thumbnailUrl2":null,
            "containerHeight2":null,
            "containerWidth2":null,
            "thumbnailUrl3":null,
            "imageUrl3":null,
            "containerHeight3":null,
            "containerWidth3":null,
            "thumbnailUrl4":null,
            "imageUrl4":null,
            "containerHeight4":null,
            "containerWidth4":null,
            "postsType":0,
            "postedOn":1509012849914,
            "likeStatus":0,
            "sold":2,
            "productUrl":null,
            "description":"black color",
            "negotiable":1,
            "condition":"For Parts",
            "price":1000,
            "currency":"USD",
            "productName":"keyboard & mouse",
            "totalComments":0,
            "commentData":[],
            "categoryData":[]*/
    var postNodeId = ""
    var isPromoted = ""
    var planId = ""
    var likes = ""
    var mainUrl = ""
    var postLikedBy = ""
    var place = ""
    var thumbnailImageUrl = ""
    var postId = ""
    var productsTagged = ""
    var productsTaggedCoordinates = ""
    var hasAudio = ""
    var containerHeight = ""
    var containerWidth = ""
    var hashTags = ""
    var postCaption = ""
    var latitude = ""
    var longitude = ""
    var thumbnailUrl1 = ""
    var imageUrl1 = ""
    var containerHeight1 = ""
    var containerWidth1 = ""
    var imageUrl2 = ""
    var thumbnailUrl2 = ""
    var containerHeight2 = ""
    var containerWidth2 = ""
    var thumbnailUrl3 = ""
    var imageUrl3 = ""
    var containerHeight3 = ""
    var containerWidth3 = ""
    var thumbnailUrl4 = ""
    var imageUrl4 = ""
    var containerHeight4 = ""
    var containerWidth4 = ""
    var postsType = ""
    var postedOn = ""
    var likeStatus = ""
    var sold = ""
    var productUrl = ""
    var description = ""
    var negotiable = ""
    var condition = ""
    var price = ""
    var currency = ""
    var productName = ""
    var totalComments = ""
    var categoryData: ArrayList<ProductCategoryDatas>? = null

}