package com.zoptal.cellableapp.pojo_class.phone_contact_pojo

/**
 * Created by hello on 04-Jul-17.
 */
class PhoneContactPostData {
    /*"latitude":13.02901,
            "hashTags":"null",
            "containerHeight":"1920",
            "thumbnailImageUrl":"https://res.cloudinary.com/dxaxmyifi/image/upload/w_150,h_150/fl_progressive:steep/v1498845586/wqyyzb1ov5g7edhqpz6r.jpg",
            "postedOn":1498857587516,
            "longitude":77.589652,
            "likes":0,
            "postCaption":null,
            "containerWidth":"1080",
            "postId":1498845589167,
            "taggedUserCoordinates":null,
            "place":"10th Cross Road, RT Nagar, Bengaluru, Karnataka 560024, India",
            "usersTagged":null,
            "mainUrl":"https://res.cloudinary.com/dxaxmyifi/image/upload/fl_progressive:steep/v1498845586/wqyyzb1ov5g7edhqpz6r.jpg",
            "postLikedBy":"null",
            "hasAudio":null,
            "comments":null*/
    var latitude = ""
    var hashTags = ""
    var containerHeight = ""
    var thumbnailImageUrl = ""
    var postedOn = ""
    var longitude = ""
    var likes = ""
    var postCaption = ""
    var containerWidth = ""
    var postId = ""
    var taggedUserCoordinates = ""
    var place = ""
    var usersTagged = ""
    var mainUrl = ""
    var postLikedBy = ""
    var hasAudio = ""
    var comments = ""

}