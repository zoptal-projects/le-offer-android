package com.zoptal.cellableapp.pojo_class.cloudinary_details_pojo

/**
 * @since  20/12/16.
 */
class Cloudinary_reponse {
    var timestamp: String? = null
    var signature: String? = null
    var apiKey: String? = null
    var cloudName: String? = null

}