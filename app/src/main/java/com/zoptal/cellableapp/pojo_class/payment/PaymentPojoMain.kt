package com.zoptal.cellableapp.pojo_class.payment

import com.zoptal.cellableapp.pojo_class.profile_pojo.ProfileResultDatas
import java.util.*

/**
 * Created by hello on 4/10/2017.
 */
class PaymentPojoMain {
    /*
"code":200,
"message":"success",
"data":[]*/
    var code = ""
    var message = ""
    var data: ArrayList<ProfileResultDatas>? = null

}