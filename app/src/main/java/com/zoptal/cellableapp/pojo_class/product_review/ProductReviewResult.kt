package com.zoptal.cellableapp.pojo_class.product_review

/**
 * Created by hello on 09-Jun-17.
 */
class ProductReviewResult {
    /*"userId":1255,
"username":"shobhitkc",
"commentedByUser":"shobhitkc",
"phoneNumber":"+919035617542",
"email":"shobhit@mobifyi.com",
"profilePicUrl":"http://res.cloudinary.com/dxaxmyifi/image/upload/v1500878731/z19ejraqhh8zyy7mclru.jpg",
"userFullname":"Shobhit Kumar",
"commentId":9232,
"commentedOn":1500985691063,
"commentBody":"hello",
"postNodeId":1428,
"likes":null,
"mainUrl":"https://res.cloudinary.com/dxaxmyifi/image/upload/fl_progressive:steep/v1500984020/df14b9cev3q40maqaaeg.jpg",
"usersTagged":null,
"place":"10th Cross Road, RT Nagar, Bengaluru, Karnataka 560024, India",
"thumbnailImageUrl":"https://res.cloudinary.com/dxaxmyifi/image/upload/w_150,h_150/v1500984020/df14b9cev3q40maqaaeg.jpg",
"postId":1500984022512,
"hashTags":"null",
"usersMentioned":null,
"postCaption":null*/
    var userId = ""
    var username = ""
    var commentedByUser = ""
    var phoneNumber = ""
    var email = ""
    var profilePicUrl = ""
    var userFullname = ""
    var commentId = ""
    var commentedOn = ""
    var commentBody = ""
    var postNodeId = ""
    var likes = ""
    var mainUrl = ""
    var usersTagged = ""
    var place = ""
    var thumbnailImageUrl = ""
    var postId = ""
    var hashTags = ""
    var usersMentioned = ""
    var postCaption = ""

}