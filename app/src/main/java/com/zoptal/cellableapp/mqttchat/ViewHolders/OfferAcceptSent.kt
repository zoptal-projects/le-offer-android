package com.zoptal.cellableapp.mqttchat.ViewHolders

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.mqttchat.AppController

/**
 * Created by user on 9/15/2017.
 */
class OfferAcceptSent(itemView: View) : RecyclerView.ViewHolder(itemView) {
    @kotlin.jvm.JvmField
    var offerAmount: TextView

    init {
        offerAmount = itemView.findViewById<View>(R.id.offerAmount) as TextView
        offerAmount.typeface = AppController.instance?.robotoMediumFont
        val sent_offer_view = itemView.findViewById<View>(R.id.sent_offer_view) as TextView
        sent_offer_view.typeface = AppController.instance?.robotoMediumFont
    }
}