package com.zoptal.cellableapp.mqttchat.ViewHolders

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.mqttchat.Utilities.AdjustableImageView

/**
 * View holder for media history video recycler view item
 */
class ViewHolderVideoMedia(view: View) : RecyclerView.ViewHolder(view) {
    @kotlin.jvm.JvmField
    var thumbnail: AdjustableImageView
    @kotlin.jvm.JvmField
    var fnf: TextView

    init {
        fnf = view.findViewById<View>(R.id.fnf) as TextView
        thumbnail = view.findViewById<View>(R.id.vidshow) as AdjustableImageView
    }
}