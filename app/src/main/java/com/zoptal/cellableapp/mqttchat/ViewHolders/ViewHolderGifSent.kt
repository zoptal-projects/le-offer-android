package com.zoptal.cellableapp.mqttchat.ViewHolders

import android.graphics.Typeface
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.mqttchat.AppController

class ViewHolderGifSent(view: View) : RecyclerView.ViewHolder(view) {
    //    public TextView senderName;
    @kotlin.jvm.JvmField
    var time: TextView
    @kotlin.jvm.JvmField
    var date: TextView
    @kotlin.jvm.JvmField
    var singleTick: ImageView
    @kotlin.jvm.JvmField
    var doubleTickGreen: ImageView
    @kotlin.jvm.JvmField
    var doubleTickBlue: ImageView
    @kotlin.jvm.JvmField
    var clock: ImageView
    @kotlin.jvm.JvmField
    var gifImage: ImageView
    @kotlin.jvm.JvmField
    var stillGifImage: ImageView

    init {


//        senderName = (TextView) view.findViewById(R.id.lblMsgFrom);
        date = view.findViewById<View>(R.id.date) as TextView
        time = view.findViewById<View>(R.id.ts) as TextView
        singleTick = view.findViewById<View>(R.id.single_tick_green) as ImageView
        doubleTickGreen = view.findViewById<View>(R.id.double_tick_green) as ImageView
        doubleTickBlue = view.findViewById<View>(R.id.double_tick_blue) as ImageView
        clock = view.findViewById<View>(R.id.clock) as ImageView
        gifImage = view.findViewById<View>(R.id.gifThumbnail) as ImageView
        stillGifImage = view.findViewById<View>(R.id.stillGifImage) as ImageView
        val tf = AppController.instance?.robotoCondensedFont
        time.setTypeface(tf, Typeface.ITALIC)
        date.setTypeface(tf, Typeface.ITALIC)
    }
}