package com.zoptal.cellableapp.mqttchat.Utilities

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/*
 * Created by moda on 07/04/16.
 */ /**
 * To sort the chats based on the date of last message in the chatlist
 */
class TimestampSorter : Comparator<Any?> {
    private var date1: Date? = null
    private var date2: Date? = null
    override fun compare(firstObjToCompare: Any?, secondObjToCompare: Any?): Int {
        val firstDateString = (firstObjToCompare as Map<String?, Any?>?)!!["lastMessageDate"] as String?
        val secondDateString = (secondObjToCompare as Map<String?, Any?>?)!!["lastMessageDate"] as String?
        if (secondDateString == null || firstDateString == null) {
            return 0
        }
        val sdf = SimpleDateFormat("yyyyMMddHHmmssSSS z")
        try {
            date1 = sdf.parse(firstDateString)
            date2 = sdf.parse(secondDateString)
        } catch (e: ParseException) {
        }
        if (date1 == null && date2 == null) {
            return 0
        }
        if (date1 == null) {
            return 1
        }
        if (date2 == null) {
            return 1
        }
        return if (date1!!.after(date2)) -1 else if (date1!!.before(date2)) 1 else 0
    }
}