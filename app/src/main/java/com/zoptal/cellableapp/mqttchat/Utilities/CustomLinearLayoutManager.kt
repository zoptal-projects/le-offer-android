package com.zoptal.cellableapp.mqttchat.Utilities

import android.content.Context
import androidx.recyclerview.widget.LinearLayoutManager

/*
 * Created by moda on 05/12/16.
 */ /**
 * Disable predictive animations. There is a bug in RecyclerView which causes views that
 * are being reloaded to pull invalid ViewHolders from the internal recycler stack if the
 * adapter size has decreased since the ViewHolder was recycled.
 */
class CustomLinearLayoutManager(context: Context?, orientation: Int, reverseLayout: Boolean) : LinearLayoutManager(context, orientation, reverseLayout) {
    override fun supportsPredictiveItemAnimations(): Boolean {
        return false
    }
}