package com.zoptal.cellableapp.mqttchat.ViewHolders

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.zoptal.cellableapp.R

class ViewHolderServerMessage(view: View) : RecyclerView.ViewHolder(view) {
    @kotlin.jvm.JvmField
    var serverupdate: TextView
    @kotlin.jvm.JvmField
    var gap: View

    init {
        serverupdate = view.findViewById<View>(R.id.servermessage) as TextView
        gap = view.findViewById(R.id.gap)
    }
}