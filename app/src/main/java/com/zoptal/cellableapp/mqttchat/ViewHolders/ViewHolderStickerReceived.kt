package com.zoptal.cellableapp.mqttchat.ViewHolders

import android.graphics.Typeface
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.mqttchat.AppController

class ViewHolderStickerReceived(view: View) : RecyclerView.ViewHolder(view) {
    var senderName: TextView? = null
    @kotlin.jvm.JvmField
    var time: TextView
    @kotlin.jvm.JvmField
    var date: TextView
    @kotlin.jvm.JvmField
    var imageView: ImageView
    var download: ImageView? = null
    @kotlin.jvm.JvmField
    var relative_layout_message: RelativeLayout

    init {
        imageView = view.findViewById<View>(R.id.imgshow) as ImageView
        date = view.findViewById<View>(R.id.date) as TextView
        time = view.findViewById<View>(R.id.ts) as TextView
        relative_layout_message = view.findViewById<View>(R.id.relative_layout_message) as RelativeLayout
        val tf = AppController.instance?.robotoCondensedFont
        time.setTypeface(tf, Typeface.ITALIC)
        date.setTypeface(tf, Typeface.ITALIC)
    }
}