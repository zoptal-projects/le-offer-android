package com.zoptal.cellableapp.mqttchat.Utilities

import android.animation.Animator

/*
 * Created by moda on 17/08/16.
 */internal abstract class AnimatorListener : Animator.AnimatorListener {
    override fun onAnimationStart(animation: Animator) {}
    override fun onAnimationCancel(animation: Animator) {}
    override fun onAnimationRepeat(animation: Animator) {}
}