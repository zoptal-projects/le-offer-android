package com.zoptal.cellableapp.mqttchat.ViewHolders

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.mqttchat.AppController

/**
 * @since 9/20/2017.
 */
class PaypalLinkShared(itemView: View) : RecyclerView.ViewHolder(itemView) {
    init {
        val sent_offer_view = itemView.findViewById<View>(R.id.sent_offer_view) as TextView
        sent_offer_view.typeface = AppController.instance?.robotoMediumFont
    }
}