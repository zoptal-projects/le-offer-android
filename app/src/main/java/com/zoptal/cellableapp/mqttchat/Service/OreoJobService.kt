package com.zoptal.cellableapp.mqttchat.Service

import android.annotation.TargetApi
import android.app.job.JobInfo
import android.app.job.JobParameters
import android.app.job.JobScheduler
import android.app.job.JobService
import android.content.ComponentName
import android.content.Context
import android.os.PersistableBundle
import com.zoptal.cellableapp.mqttchat.AppController.Companion.instance
import com.zoptal.cellableapp.mqttchat.MQtt.MqttAndroidClient
import org.eclipse.paho.client.mqttv3.MqttException

/**
 * Created by moda on 05/09/18.
 */
@TargetApi(21)
class OreoJobService : JobService() {
    private var mParams: JobParameters? = null
    private var mqttAndroidClient: MqttAndroidClient? = null
    override fun onStartJob(params: JobParameters): Boolean {
        mParams = params
        val userId = instance!!.userId ?: return false
        if (mqttAndroidClient == null) {
            if (params.extras.containsKey("fromJobScheduler")) {
                instance!!.setApplicationKilled(true)
            }
            mqttAndroidClient = instance!!.createMQttConnection(userId, false)
        }
        val command = params.extras.getString("command")
        return if (command != null && command == "stop") {
            try {
                mqttAndroidClient!!.disconnect()
            } catch (e: MqttException) {
                e.printStackTrace()
            }
            endJob()
            false
        } else {
            if (!mqttAndroidClient!!.isConnected) {
                connect()
            } else {
                scheduleJob(MQTT_constants.MQTT_JOB_INTERVAL_MS.toLong())
            }
            true
        }
    }

    fun scheduleReconnect() {
        //To automatically retry after mRetryInterval
        if (mRetryInterval < 60000L) {
            mRetryInterval = Math.min(mRetryInterval * 2L, 60000L)
        }
        scheduleJob(mRetryInterval)
    }

    fun endJob() {
        jobFinished(mParams, false)
    }

    private fun connect() {
        instance!!.connectMqttClient()
        scheduleReconnect()
    }

    fun scheduleJob(interval: Long) {
        val serviceName = ComponentName(this.packageName, OreoJobService::class.java.name)
        val extras = PersistableBundle()
        extras.putString("command", "start")
        extras.putInt("fromJobScheduler", 1)
        val jobInfo = JobInfo.Builder(MQTT_constants.MQTT_JOB_ID, serviceName).setExtras(extras).setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY).setMinimumLatency(interval).setOverrideDeadline(interval).build()
        val jobScheduler = this.getSystemService(Context.JOB_SCHEDULER_SERVICE) as JobScheduler
        jobScheduler.schedule(jobInfo)
        endJob()
    }

    override fun onStopJob(params: JobParameters): Boolean {
        return false
    }

    companion object {
        //Assuming it takes maximum 5 seconds for
        private var mRetryInterval: Long = 2500
    }
}