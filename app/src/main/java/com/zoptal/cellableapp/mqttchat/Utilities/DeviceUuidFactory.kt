package com.zoptal.cellableapp.mqttchat.Utilities

import android.content.Context
import android.provider.Settings
import java.util.*

/**
 * To fetch the public deviceId for the device
 */
class DeviceUuidFactory private constructor() {
    fun getDeviceUuid(context: Context): String {
        return collectID(context)
    }

    private fun collectID(context: Context): String {
        val prefs = context
                .getSharedPreferences(PREFS_FILE, Context.MODE_PRIVATE)
        val id = prefs.getString(PREFS_DEVICE_ID, null)
        return if (id != null && !id.isEmpty()) {
            id
        } else {
            var temp_ID: String?
            temp_ID = Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID)
            if (temp_ID == null) {
                temp_ID = UUID.randomUUID().toString()
            }
            if (temp_ID != null && !temp_ID.isEmpty()) {
                prefs.edit().putString(PREFS_DEVICE_ID, temp_ID).apply()
                temp_ID
            } else {
                prefs.edit().putString(PREFS_DEVICE_ID, null).apply()
                "9774d56d682e549c"
            }
        }
    }

    companion object {
        private const val PREFS_FILE = "yelodIdPreferences"
        private const val PREFS_DEVICE_ID = "deviceId"
        val instance = DeviceUuidFactory()
    }
}