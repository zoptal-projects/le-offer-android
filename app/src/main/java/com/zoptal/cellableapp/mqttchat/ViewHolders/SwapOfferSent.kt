package com.zoptal.cellableapp.mqttchat.ViewHolders

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.zoptal.cellableapp.R

/**
 * Created by embed on 10/5/18.
 */
class SwapOfferSent(itemView: View) : RecyclerView.ViewHolder(itemView) {
    @kotlin.jvm.JvmField
    var iV_otherProduct: ImageView
    @kotlin.jvm.JvmField
    var iV_ownProduct: ImageView
    @kotlin.jvm.JvmField
    var tV_swapMessage: TextView

    init {
        iV_ownProduct = itemView.findViewById<View>(R.id.iV_ownProduct) as ImageView
        iV_otherProduct = itemView.findViewById<View>(R.id.iV_otherProduct) as ImageView
        tV_swapMessage = itemView.findViewById<View>(R.id.tV_swapMessage) as TextView
    }
}