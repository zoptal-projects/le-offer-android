package com.zoptal.cellableapp.mqttchat.ViewHolders

import android.graphics.Typeface
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.mqttchat.AppController

/**
 * View holder for text message sent recycler view item
 */
class ViewHolderMessageSent(view: View) : RecyclerView.ViewHolder(view) {
    //    public TextView senderName;
    @kotlin.jvm.JvmField
    var message: TextView
    @kotlin.jvm.JvmField
    var time: TextView
    @kotlin.jvm.JvmField
    var date: TextView
    @kotlin.jvm.JvmField
    var singleTick: ImageView
    @kotlin.jvm.JvmField
    var doubleTickGreen: ImageView
    @kotlin.jvm.JvmField
    var doubleTickBlue: ImageView
    @kotlin.jvm.JvmField
    var clock //, blocked;
            : ImageView

    init {
        //        blocked = (ImageView) view.findViewById(R.id.blocked);
        date = view.findViewById<View>(R.id.date) as TextView
        // senderName = (TextView) view.findViewById(R.id.lblMsgFrom);
        message = view.findViewById<View>(R.id.txtMsg) as TextView
        time = view.findViewById<View>(R.id.ts) as TextView
        singleTick = view.findViewById<View>(R.id.single_tick_green) as ImageView
        doubleTickGreen = view.findViewById<View>(R.id.double_tick_green) as ImageView
        doubleTickBlue = view.findViewById<View>(R.id.double_tick_blue) as ImageView
        clock = view.findViewById<View>(R.id.clock) as ImageView
        val tf = AppController.instance?.robotoCondensedFont
        time.setTypeface(tf, Typeface.ITALIC)
        date.setTypeface(tf, Typeface.ITALIC)
        message.setTypeface(tf, Typeface.NORMAL)
    }
}