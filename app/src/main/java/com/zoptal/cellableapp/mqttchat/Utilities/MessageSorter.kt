package com.zoptal.cellableapp.mqttchat.Utilities

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by moda on 29/06/17.
 */
/**
 * To sort the message received after inserting new message in to the database locally,so messages are always stored in database in the sortedn order of time received
 */
class MessageSorter : Comparator<Any?> {
    private var date1: Date? = null
    private var date2: Date? = null
    override fun compare(firstObjToCompare: Any?, secondObjToCompare: Any?): Int {
        val firstDateString = (firstObjToCompare as Map<String?, Any?>?)!!["Ts"] as String?
        val secondDateString = (secondObjToCompare as Map<String?, Any?>?)!!["Ts"] as String?
        if (secondDateString == null || firstDateString == null) {
            return 0
        }
        val sdf = SimpleDateFormat("yyyyMMddHHmmssSSS z")
        try {
            date1 = sdf.parse(firstDateString)
            date2 = sdf.parse(secondDateString)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return if (date1!!.after(date2)) 1 else if (date1!!.before(date2)) -1 else 0
    }
}