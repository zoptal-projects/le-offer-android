package com.zoptal.cellableapp.mqttchat.Utilities

import android.content.Context
import android.util.AttributeSet
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView

/**
 * Created by moda on 20/06/17.
 */
open class AdjustableImageView : AppCompatImageView {
    private var mAdjustViewBounds = false

    constructor(context: Context?) : super(context) {}
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {}
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {}

    override fun setAdjustViewBounds(adjustViewBounds: Boolean) {
        mAdjustViewBounds = adjustViewBounds
        super.setAdjustViewBounds(adjustViewBounds)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val mDrawable = drawable
        if (mDrawable == null) {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec)
            return
        }
        if (mAdjustViewBounds) {
            val mDrawableWidth = mDrawable.intrinsicWidth
            val mDrawableHeight = mDrawable.intrinsicHeight
            val heightSize = MeasureSpec.getSize(heightMeasureSpec)
            val widthSize = MeasureSpec.getSize(widthMeasureSpec)
            val heightMode = MeasureSpec.getMode(heightMeasureSpec)
            val widthMode = MeasureSpec.getMode(widthMeasureSpec)
            if (heightMode == MeasureSpec.EXACTLY && widthMode != MeasureSpec.EXACTLY) {
                // Fixed Height & Adjustable Width
                val width = heightSize * mDrawableWidth / mDrawableHeight
                if (isInScrollingContainer) setMeasuredDimension(width, heightSize) else setMeasuredDimension(Math.min(width, widthSize), Math.min(heightSize, heightSize))
            } else if (widthMode == MeasureSpec.EXACTLY && heightMode != MeasureSpec.EXACTLY) {
                // Fixed Width & Adjustable Height
                val height = widthSize * mDrawableHeight / mDrawableWidth
                if (isInScrollingContainer) setMeasuredDimension(widthSize, height) else setMeasuredDimension(Math.min(widthSize, widthSize), Math.min(height, heightSize))
            } else {
                super.onMeasure(widthMeasureSpec, heightMeasureSpec)
            }
        } else {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        }
    }

    private val isInScrollingContainer: Boolean
        private get() {
            var p = parent
            while (p != null && p is ViewGroup) {
                if (p.shouldDelayChildPressedState()) {
                    return true
                }
                p = p.getParent()
            }
            return false
        }
}