package com.zoptal.cellableapp.mqttchat.Utilities

import com.zoptal.cellableapp.mqttchat.AppController.Companion.instance
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by moda on 04/05/17.
 */
/**
 * Created by rahul on 25/3/16.
 */
class Utilities {
    fun gmtToEpoch(tsingmt: String?): String {
        var d: Date? = null
        var epoch: Long = 0
        val formater = SimpleDateFormat("yyyyMMddHHmmssSSS z")
        try {
            d = formater.parse(tsingmt)
            epoch = d.time
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return epoch.toString()
    }

    companion object {
        @kotlin.jvm.JvmStatic
        fun tsInGmt(): String {


//        Log.d("log31",System.currentTimeMillis()+"");
            val localTime = Date(System.currentTimeMillis() - instance!!.timeDelta)
            //        Log.d("log32",localTime.toString());
            val formater = SimpleDateFormat("yyyyMMddHHmmssSSS z")
            formater.timeZone = TimeZone.getTimeZone("GMT")

//        Log.d("log33",s);
            return formater.format(localTime)
        }

        //converting time to localtime zone from gmt time
        @kotlin.jvm.JvmStatic
        fun tsFromGmt(tsingmt: String?): String? {
            var d: Date? = null
            var s: String? = null
            val formater = SimpleDateFormat("yyyyMMddHHmmssSSS z")
            try {
                d = formater.parse(tsingmt)
                val tz = TimeZone.getDefault()
                formater.timeZone = tz
                s = formater.format(d)
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            return s
        }

        fun Daybetween(date1: String?, date2: String?, pattern: String): Long {
            var sdf = SimpleDateFormat(pattern, Locale.ENGLISH)
            sdf.timeZone = TimeZone.getDefault()
            var startDate: Date? = null
            var endDate: Date? = null
            try {
                startDate = sdf.parse(date1)
                endDate = sdf.parse(date2)
            } catch (e: Exception) {
                e.printStackTrace()
                sdf = SimpleDateFormat(pattern.substring(0, 19), Locale.ENGLISH)
                try {
                    startDate = sdf.parse(date1)
                    endDate = sdf.parse(date2)
                } catch (ef: ParseException) {
                    ef.printStackTrace()
                }
            }
            val sDate = getDatePart(startDate)
            val eDate = getDatePart(endDate)
            var daysBetween: Long = 0
            while (sDate.before(eDate)) {
                sDate.add(Calendar.DAY_OF_MONTH, 1)
                daysBetween++
            }
            return daysBetween
        }

        fun getDatePart(date: Date?): Calendar {
            val cal = Calendar.getInstance() // get calendar instance
            cal.time = date
            cal[Calendar.HOUR_OF_DAY] = 0 // set hour to midnight
            cal[Calendar.MINUTE] = 0 // set minute in hour
            cal[Calendar.SECOND] = 0 // set second in minute
            cal[Calendar.MILLISECOND] = 0 // set millisecond in second
            return cal // return the date part
        }

        @kotlin.jvm.JvmStatic
        fun formatDate(ts: String?): String? {
            var s: String? = null
            var d: Date? = null
            var formater = SimpleDateFormat("yyyyMMddHHmmssSSS z")
            try {
                d = formater.parse(ts)
                formater = SimpleDateFormat("HH:mm:ss EEE dd/MMM/yyyy z")
                s = formater.format(d)
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            return s
        }

        @kotlin.jvm.JvmStatic
        fun epochtoGmt(tsingmt: String): String? {
            var d: Date? = null
            var s: String? = null
            var epoch: Long = 0
            val formater = SimpleDateFormat("yyyyMMddHHmmssSSS z")
            formater.timeZone = TimeZone.getTimeZone("GMT")
            epoch = tsingmt.toLong()
            d = Date(epoch)
            s = formater.format(d)
            return s
        }

        @kotlin.jvm.JvmStatic
        fun changeStatusDateFromGMTToLocal(ts: String?): String? {
            var s: String? = null
            val d: Date


//        SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
            val formater = SimpleDateFormat("yyyyMMddHHmmssSSS z")
            try {
                d = formater.parse(ts)
                val tz = TimeZone.getDefault()
                formater.timeZone = tz
                s = formater.format(d)
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            return s
        }

        /**
         * To get the epoch time of the current time in gmt
         */
        @kotlin.jvm.JvmStatic
        val gmtEpoch: Long
            get() {
                val localTime = Date(System.currentTimeMillis() - instance!!.timeDelta)
                val d: Date
                val formater = SimpleDateFormat("yyyyMMddHHmmssSSS z")
                formater.timeZone = TimeZone.getTimeZone("GMT")
                val tsingmt = formater.format(localTime)
                var epoch: Long = 0
                try {
                    d = formater.parse(tsingmt)
                    epoch = d.time
                } catch (e: ParseException) {
                    e.printStackTrace()
                }
                return epoch
            }

        /*
     * Get calls time based on the timezone
     */
        fun tsFromGmtToLocalTimeZone(tsingmt: String?): String? {

//        Date d = null;
//        String s = null;
//
//        SimpleDateFormat formater = new SimpleDateFormat("yyyyMMddHHmmssSSS z");
//
//        try {
//            d = formater.parse(tsingmt);
//            TimeZone tz = TimeZone.getDefault();
//            formater.setTimeZone(tz);
//            s = formater.format(d);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//
//        return s;
            var s: String? = null
            var d: Date? = null
            var formater = SimpleDateFormat("yyyyMMddHHmmssSSS z")
            try {
                d = formater.parse(tsingmt)
                formater = SimpleDateFormat("yyyy-MM-dd HH:mm:ss z")
                formater.timeZone = TimeZone.getDefault()
                s = formater.format(d)
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            return s
        }
    }
}