package com.zoptal.cellableapp.mqttchat.ViewHolders

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.mqttchat.Utilities.AdjustableImageView

/**
 * View holder for media history image recycler view item
 */
class ViewHolderImageMedia(view: View) : RecyclerView.ViewHolder(view) {
    @kotlin.jvm.JvmField
    var fnf: TextView
    @kotlin.jvm.JvmField
    var image: AdjustableImageView

    init {
        fnf = view.findViewById<View>(R.id.fnf) as TextView
        image = view.findViewById<View>(R.id.imageView28) as AdjustableImageView
    }
}