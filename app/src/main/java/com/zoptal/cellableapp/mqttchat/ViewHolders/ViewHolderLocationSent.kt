package com.zoptal.cellableapp.mqttchat.ViewHolders

import android.graphics.Typeface
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.mqttchat.AppController

/*
 * Created by moda on 02/04/16.
 */ /**
 * View holder for location sent recycler view item
 */
class ViewHolderLocationSent(view: View) : RecyclerView.ViewHolder(view), OnMapReadyCallback {
    //    public TextView senderName;
    @kotlin.jvm.JvmField
    var time: TextView
    @kotlin.jvm.JvmField
    var date: TextView
    @kotlin.jvm.JvmField
    var singleTick: ImageView
    @kotlin.jvm.JvmField
    var doubleTickGreen: ImageView
    @kotlin.jvm.JvmField
    var doubleTickBlue: ImageView
    @kotlin.jvm.JvmField
    var clock //,blocked;
            : ImageView
    var mapView: MapView
    @kotlin.jvm.JvmField
    var mMap: GoogleMap? = null
    var nameSelected = ""
    @kotlin.jvm.JvmField
    var positionSelected = LatLng(0.0, 0.0)
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap!!.uiSettings.isMapToolbarEnabled = true
        mMap!!.addMarker(MarkerOptions().position(positionSelected).title(nameSelected))
        mMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(positionSelected, 16.0f))
    }

    init {
        date = view.findViewById<View>(R.id.date) as TextView

        //  senderName = (TextView) view.findViewById(R.id.lblMsgFrom);

//        blocked = (ImageView) view.findViewById(R.id.blocked);
        time = view.findViewById<View>(R.id.ts) as TextView
        singleTick = view.findViewById<View>(R.id.single_tick_green) as ImageView
        doubleTickGreen = view.findViewById<View>(R.id.double_tick_green) as ImageView
        doubleTickBlue = view.findViewById<View>(R.id.double_tick_blue) as ImageView
        clock = view.findViewById<View>(R.id.clock) as ImageView
        mapView = view.findViewById<View>(R.id.map) as MapView
        mapView.onCreate(null)
        mapView.onResume()
        mapView.getMapAsync(this)
        val tf = AppController.instance?.robotoCondensedFont
        time.setTypeface(tf, Typeface.ITALIC)
        date.setTypeface(tf, Typeface.ITALIC)
    }
}