package com.zoptal.cellableapp.mqttchat.ViewHolders

import android.graphics.Typeface
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.mqttchat.AppController
import com.zoptal.cellableapp.mqttchat.Utilities.RingProgressBar

class ViewHolderAudioSentRetrieve(view: View) : RecyclerView.ViewHolder(view) {
    @kotlin.jvm.JvmField
    var time: TextView
    @kotlin.jvm.JvmField
    var date: TextView
    var tv: TextView
    @kotlin.jvm.JvmField
    var fnf: TextView

    //    public TextView senderName;
    @kotlin.jvm.JvmField
    var singleTick: ImageView
    @kotlin.jvm.JvmField
    var doubleTickGreen: ImageView
    @kotlin.jvm.JvmField
    var doubleTickBlue: ImageView
    @kotlin.jvm.JvmField
    var clock: ImageView
    @kotlin.jvm.JvmField
    var playButton: ImageView
    @kotlin.jvm.JvmField
    var download: ImageView
    @kotlin.jvm.JvmField
    var cancel //, blocked;
            : ImageView
    @kotlin.jvm.JvmField
    var progressBar: RingProgressBar
    @kotlin.jvm.JvmField
    var progressBar2: ProgressBar

    init {


        // senderName = (TextView) view.findViewById(R.id.lblMsgFrom);
        tv = view.findViewById<View>(R.id.tv) as TextView
        //        blocked = (ImageView) view.findViewById(R.id.blocked);
        playButton = view.findViewById<View>(R.id.imageView26) as ImageView
        date = view.findViewById<View>(R.id.date) as TextView
        time = view.findViewById<View>(R.id.ts) as TextView
        singleTick = view.findViewById<View>(R.id.single_tick_green) as ImageView
        doubleTickGreen = view.findViewById<View>(R.id.double_tick_green) as ImageView
        doubleTickBlue = view.findViewById<View>(R.id.double_tick_blue) as ImageView
        clock = view.findViewById<View>(R.id.clock) as ImageView
        fnf = view.findViewById<View>(R.id.fnf) as TextView
        progressBar2 = view.findViewById<View>(R.id.progress2) as ProgressBar
        progressBar = view.findViewById<View>(R.id.progress) as RingProgressBar
        download = view.findViewById<View>(R.id.download) as ImageView
        cancel = view.findViewById<View>(R.id.cancel) as ImageView
        val tf = AppController.instance?.robotoCondensedFont
        time.setTypeface(tf, Typeface.ITALIC)
        date.setTypeface(tf, Typeface.ITALIC)
        fnf.setTypeface(tf, Typeface.NORMAL)
    }
}