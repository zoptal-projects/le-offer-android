package com.zoptal.cellableapp.mqttchat.Giphy;

import android.view.View;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.zoptal.cellableapp.R;


/**
 * Created by embed on 4/1/17.
 */
public class ViewHolderTrendingGif extends RecyclerView.ViewHolder {

    public ImageView image;

    public ViewHolderTrendingGif(View view) {
        super(view);
        image = (ImageView) view.findViewById(R.id.imageView29);
    }
}
