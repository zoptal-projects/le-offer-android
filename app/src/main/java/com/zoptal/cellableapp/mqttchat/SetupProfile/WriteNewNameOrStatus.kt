package com.zoptal.cellableapp.mqttchat.SetupProfile

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.Gravity
import android.view.KeyEvent
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputLayout
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.mqttchat.AppController
import com.zoptal.cellableapp.mqttchat.AppController.Companion.instance
import github.ankushsachdeva.emojicon.EmojiconGridView.OnEmojiconClickedListener
import github.ankushsachdeva.emojicon.EmojiconsPopup
import github.ankushsachdeva.emojicon.EmojiconsPopup.OnEmojiconBackspaceClickedListener
import github.ankushsachdeva.emojicon.EmojiconsPopup.OnSoftKeyboardOpenCloseListener
import github.ankushsachdeva.emojicon.emoji.Emojicon

/*
 * Activity to let user enter the new status or name which can both contain text and emoticons
 */
class WriteNewNameOrStatus() : AppCompatActivity() {
    private var title: TextView? = null
    private var editText: EditText? = null
    private var editText_til: TextInputLayout? = null
    private val bus = AppController.bus
    private var selEmoji: ImageView? = null
    private var selKeybord: ImageView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState)
        setContentView(R.layout.edit_name_status)
        title = findViewById<View>(R.id.title) as TextView
        selEmoji = findViewById<View>(R.id.emojiButton) as ImageView
        selKeybord = findViewById<View>(R.id.chat_keyboard_icon) as ImageView
        editText = findViewById<View>(R.id.et) as EditText
        val root: RelativeLayout? = findViewById<View>(R.id.root) as RelativeLayout
        editText_til = findViewById<View>(R.id.input_layout_name2) as TextInputLayout
        setUpActivity(intent)
        val popup = EmojiconsPopup(root, this)
        editText!!.addTextChangedListener(MyTextWatcher((editText)!!))


        /*
         * To save the new status or name
         */
        val save = findViewById<View>(R.id.rl7) as RelativeLayout
        save.setOnClickListener(View.OnClickListener { view ->
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)

            /*
                 * To save the updated details locally
                 */if (editText!!.text.toString().trim { it <= ' ' }.length > 4) {
            /*
        * Eventually we may be required to update name or status directly from this page
        */
            val intent = Intent()
            intent.putExtra("updatedValue", editText!!.text.toString().trim { it <= ' ' })
            setResult(Activity.RESULT_OK, intent)
            supportFinishAfterTransition()
        } else {
            if (root != null) {
                val snackbar = Snackbar.make(root, getString(R.string.name_save), Snackbar.LENGTH_SHORT)
                snackbar.show()
                val view2 = snackbar.view
                val txtv = view2.findViewById<View>(R.id.snackbar_text) as TextView
                txtv.gravity = Gravity.CENTER_HORIZONTAL
            }
        }
        })
        popup.setSizeForSoftKeyboard()
        popup.setOnDismissListener(object : PopupWindow.OnDismissListener {
            override fun onDismiss() {
                selKeybord!!.visibility = View.GONE
                selEmoji!!.visibility = View.VISIBLE
            }
        }
        )
        popup.setOnSoftKeyboardOpenCloseListener(object : OnSoftKeyboardOpenCloseListener {
            override fun onKeyboardOpen(keyBoardHeight: Int) {}
            override fun onKeyboardClose() {
                if (popup.isShowing) popup.dismiss()
            }
        }
        )
        popup.setOnEmojiconClickedListener(object : OnEmojiconClickedListener {
            override fun onEmojiconClicked(emojicon: Emojicon) {
                if (editText == null || emojicon == null) {
                    return
                }
                val start = editText!!.selectionStart
                val end = editText!!.selectionEnd
                if (start < 0) {
                    editText!!.append(emojicon.emoji)
                } else {
                    editText!!.text.replace(Math.min(start, end),
                            Math.max(start, end), emojicon.emoji, 0,
                            emojicon.emoji.length)
                }
            }
        }
        )
        popup.setOnEmojiconBackspaceClickedListener(object : OnEmojiconBackspaceClickedListener {
            override fun onEmojiconBackspaceClicked(v: View) {
                val event = KeyEvent(
                        0, 0, 0, KeyEvent.KEYCODE_DEL, 0, 0, 0, 0, KeyEvent.KEYCODE_ENDCALL)
                editText!!.dispatchKeyEvent(event)
            }
        }
        )
        selEmoji!!.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {
                selKeybord!!.visibility = View.VISIBLE
                selEmoji!!.visibility = View.GONE
                if (!popup.isShowing) {
                    if (popup.isKeyBoardOpen) {
                        popup.showAtBottom()
                    } else {
                        editText!!.isFocusableInTouchMode = true
                        editText!!.requestFocus()
                        popup.showAtBottomPending()
                        val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                        inputMethodManager.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT)
                    }
                } else {
                    popup.dismiss()
                }
            }
        }
        )
        selKeybord!!.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {
                selKeybord!!.visibility = View.GONE
                selEmoji!!.visibility = View.VISIBLE
                if (!popup.isShowing) {
                    editText!!.isFocusableInTouchMode = true
                    editText!!.requestFocus()
                    popup.showAtBottomPending()
                    val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    inputMethodManager.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT)
                } else {
                    popup.dismiss()
                }
            }
        }
        )
        val close = findViewById<View>(R.id.close) as ImageView
        close.setOnClickListener(object : View.OnClickListener {
            override fun onClick(view: View) {
                onBackPressed()
            }
        })
        /*
         * To set the typeface values
         */
        val tf = instance!!.robotoCondensedFont
        title!!.setTypeface(tf, Typeface.BOLD)
        bus.register(this)
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        setUpActivity(intent)
    }

    private fun setUpActivity(intent: Intent) {

/*
 *    type 0- name change
 *    type 1- status change
 */
        val extras = intent.extras
        title!!.text = getString(R.string.NewName)
        editText!!.hint = getString(R.string.EnterName)
        val currentValue = extras!!.getString("currentValue")
        if (currentValue != null && !currentValue.isEmpty()) {
            editText!!.setText(currentValue)
        }
    }

    private inner class MyTextWatcher(private val view: View) : TextWatcher {
        override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
        override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
        override fun afterTextChanged(editable: Editable) {
            when (view.id) {
                R.id.et -> validateNameOrStatus()
            }
        }

    }

    private fun validateNameOrStatus(): Boolean {
        if (editText!!.text.toString().trim { it <= ' ' }.isEmpty()) {


            /*
                 * Name change
                 */
            editText_til!!.error = getString(R.string.name_empty)
            requestFocus(editText)
            return false
        } else if (editText!!.text.toString().trim { it <= ' ' }.length < 4) {


            /*
                 * Name change
                 */
            editText_til!!.error = getString(R.string.name_atleast)
            requestFocus(editText)
            return false
        } else {
            editText_til!!.isErrorEnabled = false
        }
        return true
    }

    private fun requestFocus(view: View?) {
        if (view!!.requestFocus()) {
            window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
        }
    }

    override fun onBackPressed() {
        val intent = Intent()
        setResult(Activity.RESULT_CANCELED, intent)
        supportFinishAfterTransition()
    }
}