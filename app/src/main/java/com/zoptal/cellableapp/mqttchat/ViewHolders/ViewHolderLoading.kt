package com.zoptal.cellableapp.mqttchat.ViewHolders

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.mqttchat.Utilities.SlackLoadingView

/**
 * Created by moda on 08/08/17.
 */
/**
 * View holder for the loading more results item in recycler view
 */
class ViewHolderLoading(view: View) : RecyclerView.ViewHolder(view) {
    @kotlin.jvm.JvmField
    var slack: SlackLoadingView

    init {
        slack = view.findViewById<View>(R.id.slack) as SlackLoadingView
    }
}