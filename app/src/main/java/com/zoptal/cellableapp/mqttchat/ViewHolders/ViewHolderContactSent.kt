package com.zoptal.cellableapp.mqttchat.ViewHolders

import android.graphics.Typeface
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.mqttchat.AppController

/**
 * View holder for contact sent recycler view item
 */
class ViewHolderContactSent(view: View) : RecyclerView.ViewHolder(view) {
    //    public  TextView senderName;
    @kotlin.jvm.JvmField
    var time: TextView
    @kotlin.jvm.JvmField
    var contactName: TextView
    @kotlin.jvm.JvmField
    var contactNumber: TextView
    @kotlin.jvm.JvmField
    var date: TextView
    @kotlin.jvm.JvmField
    var singleTick: ImageView
    @kotlin.jvm.JvmField
    var doubleTickGreen: ImageView
    @kotlin.jvm.JvmField
    var doubleTickBlue: ImageView
    @kotlin.jvm.JvmField
    var clock //,blocked;
            : ImageView

    init {

        // senderName = (TextView) view.findViewById(R.id.lblMsgFrom);
        contactName = view.findViewById<View>(R.id.contactName) as TextView
        contactNumber = view.findViewById<View>(R.id.contactNumber) as TextView
        date = view.findViewById<View>(R.id.date) as TextView
        //        blocked = (ImageView) view.findViewById(R.id.blocked);
        time = view.findViewById<View>(R.id.ts) as TextView
        singleTick = view.findViewById<View>(R.id.single_tick_green) as ImageView
        doubleTickGreen = view.findViewById<View>(R.id.double_tick_green) as ImageView
        doubleTickBlue = view.findViewById<View>(R.id.double_tick_blue) as ImageView
        clock = view.findViewById<View>(R.id.clock) as ImageView
        val tf = AppController.instance?.robotoCondensedFont
        time.setTypeface(tf, Typeface.ITALIC)
        date.setTypeface(tf, Typeface.ITALIC)
        contactName.setTypeface(tf, Typeface.NORMAL)
        contactNumber.setTypeface(tf, Typeface.NORMAL)
    }
}