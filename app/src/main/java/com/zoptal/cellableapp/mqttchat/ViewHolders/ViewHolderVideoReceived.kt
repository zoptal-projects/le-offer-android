package com.zoptal.cellableapp.mqttchat.ViewHolders

import android.graphics.Typeface
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.mqttchat.AppController
import com.zoptal.cellableapp.mqttchat.Utilities.AdjustableImageView
import com.zoptal.cellableapp.mqttchat.Utilities.RingProgressBar

/*
 * Created by moda on 02/04/16.
 */ /**
 * View holder for video received recycler view item
 */
class ViewHolderVideoReceived(view: View) : RecyclerView.ViewHolder(view) {
    var senderName: TextView? = null
    @kotlin.jvm.JvmField
    var time: TextView
    @kotlin.jvm.JvmField
    var date: TextView
    @kotlin.jvm.JvmField
    var fnf: TextView
    @kotlin.jvm.JvmField
    var download: ImageView
    @kotlin.jvm.JvmField
    var cancel: ImageView
    @kotlin.jvm.JvmField
    var progressBar2: ProgressBar
    @kotlin.jvm.JvmField
    var progressBar: RingProgressBar
    @kotlin.jvm.JvmField
    var thumbnail: AdjustableImageView

    init {
        date = view.findViewById<View>(R.id.date) as TextView

        // senderName = (TextView) view.findViewById(R.id.lblMsgFrom);
        time = view.findViewById<View>(R.id.ts) as TextView
        thumbnail = view.findViewById<View>(R.id.vidshow) as AdjustableImageView
        cancel = view.findViewById<View>(R.id.cancel) as ImageView
        progressBar2 = view.findViewById<View>(R.id.progress2) as ProgressBar
        progressBar = view.findViewById<View>(R.id.progress) as RingProgressBar
        download = view.findViewById<View>(R.id.download) as ImageView
        fnf = view.findViewById<View>(R.id.fnf) as TextView
        val tf = AppController.instance?.robotoCondensedFont
        time.setTypeface(tf, Typeface.ITALIC)
        date.setTypeface(tf, Typeface.ITALIC)
        fnf.setTypeface(tf, Typeface.NORMAL)
    }
}