package com.zoptal.cellableapp.mqttchat.Utilities

/**
 * @since  20/06/17.
 * @author 3Embed.
 * /45.55.223.151:8009
 */
object ApiOnServer {
    const val MQTT_SERVICE_PATH = "com.zoptal.cellableapp.mqttchat.MQtt.MqttService"
    const val CHAT_RECEIVED_THUMBNAILS_FOLDER = "/Yelo/receivedThumbnails"
    const val CHAT_UPLOAD_THUMBNAILS_FOLDER = "/Yelo/upload"
    const val CHAT_DOODLES_FOLDER = "/Yelo/doodles"
    const val CHAT_DOWNLOADS_FOLDER = "/Yelo/"
    const val IMAGE_CAPTURE_URI = "/Yelo"
    const val CHAT_MULTER_UPLOAD_URL = "https://upload.cellableapp.com/"
    private const val CHAT_UPLOAD_SERVER_URL = "https://fetch.cellableapp.com/"
    const val VIDEO_THUMBNAILS = "/Yelo/thumbnails"
    const val CHAT_UPLOAD_PATH = CHAT_UPLOAD_SERVER_URL //+ "chat/profilePics/"
    const val PROFILEPIC_UPLOAD_PATH = CHAT_UPLOAD_SERVER_URL + "chat/profilePics/"
    const val DELETE_DOWNLOAD = CHAT_MULTER_UPLOAD_URL + "deleteImage"
    const val HOST = "dev.cellableapp.com" //"dev.cellableapp.com";// "3.22.206.241";//104.236.56.43
    const val PORT = "1883"
    const val API_MAIN_LINK = "https://mqtt.cellableapp.com/" //"http://3.22.206.241:5010";
    const val LOGIN_API = API_MAIN_LINK + "User/LoginWithEmail"
    const val VERIFY_API = API_MAIN_LINK + "User/verifyEmail"
    const val SIGNUP_API = API_MAIN_LINK + "User/SignupWithEmail"
    const val GET_USERS_API = API_MAIN_LINK + "User"
    const val TRENDING_STICKERS = "http://api.giphy.com/v1/stickers/trending?api_key=dc6zaTOxFJmzC"
    const val TRENDING_GIFS = "http://api.giphy.com/v1/gifs/trending?api_key=dc6zaTOxFJmzC"
    const val GIPHY_APIKEY = "&api_key=dc6zaTOxFJmzC"
    const val SEARCH_STICKERS = "http://api.giphy.com/v1/stickers/search?q="
    const val SEARCH_GIFS = "http://api.giphy.com/v1/gifs/search?q="
    const val USER_PROFILE = API_MAIN_LINK + "User/Profile"
    const val FETCH_CHATS = API_MAIN_LINK + "Chats"

    /*
     *Mqtt user name and password */
    const val MQTTUSER_NAME = "cellable"
    const val MQTTPASSWORD = "cellable"

    /**
     * GET api to fetch the messages into the list */
    const val FETCH_MESSAGES = API_MAIN_LINK + "Messages"

    /*
     * Authorization key for chat */
    const val AUTH_KEY = "KMajNKHPqGt6kXwUbFN3dU46PjThSNTtrEnPZUefdasdfghsaderf1234567890ghfghsdfghjfghjkswdefrtgyhdfghj" //change

    //firebase server key
    const val PUSH_KEY = "AAAApOrCycs:APA91bEzvsDwHHouMDxox49iD8Nn0Fx_7EDPQiEIxlZ2clEUljp3W0u2CjgBQ_F2K1qiAlA6Jtalo8M3wzEQ4wY_b6nrXFA9jIzYFpVi_pddMHR4kS3dQKWw51YMykSh8SD-izEhN4y7" //change
}