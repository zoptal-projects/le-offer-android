package com.zoptal.cellableapp.mqttchat.Utilities

import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.RectF
import android.util.AttributeSet
import android.view.View
import android.view.animation.LinearInterpolator
import java.util.*

/**
 * View for the loading more items
 */
class SlackLoadingView @JvmOverloads constructor(context: Context?, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : View(context, attrs, defStyleAttr) {
    private val MAX_LINE_LENGTH = dp2px(getContext(), 36f)
    private val MIN_LINE_LENGTH = dp2px(getContext(), 12f)
    private var mPaint: Paint? = null
    private val mColors = intArrayOf(-0x4f9c5004, -0x4f9c5004, -0x4f9c5004, -0x4f9c5004)
    private var mWidth = 0
    private var mHeight = 0
    private var mDuration = MIN_DURATION
    private var mEntireLineLength = MIN_LINE_LENGTH
    private var mCircleRadius = 0
    private val mAnimList: MutableList<Animator> = ArrayList()
    private val CANVAS_ROTATE_ANGLE = 60
    private var mStatus = STATUS_STILL
    private var mCanvasAngle = 0
    private var mLineLength = 0f
    private var mCircleY = 0f
    private var mStep = 0
    private fun initView() {
        mPaint = Paint()
        mPaint!!.isAntiAlias = true
        mPaint!!.color = mColors[0]
    }

    private fun initData() {
        mCanvasAngle = CANVAS_ROTATE_ANGLE
        mLineLength = mEntireLineLength.toFloat()
        mCircleRadius = mEntireLineLength / 5
        mPaint!!.strokeWidth = mCircleRadius * 2.toFloat()
        mStep = 0
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        mWidth = w
        mHeight = h
        initData()
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        when (mStep % 4) {
            0 -> {
                var i = 0
                while (i < mColors.size) {
                    mPaint!!.color = mColors[i]
                    drawCRLC(canvas, mWidth / 2 - mEntireLineLength / 2.2f, mHeight / 2 - mLineLength, mWidth / 2 - mEntireLineLength / 2.2f, mHeight / 2 + mEntireLineLength.toFloat(), mPaint!!, mCanvasAngle + i * 90)
                    i++
                }
            }
            1 -> {
                var i = 0
                while (i < mColors.size) {
                    mPaint!!.color = mColors[i]
                    drawCR(canvas, mWidth / 2 - mEntireLineLength / 2.2f, mHeight / 2 + mEntireLineLength.toFloat(), mPaint!!, mCanvasAngle + i * 90)
                    i++
                }
            }
            2 -> {
                var i = 0
                while (i < mColors.size) {
                    mPaint!!.color = mColors[i]
                    drawCRCC(canvas, mWidth / 2 - mEntireLineLength / 2.2f, mHeight / 2 + mCircleY, mPaint!!, mCanvasAngle + i * 90)
                    i++
                }
            }
            3 -> {
                var i = 0
                while (i < mColors.size) {
                    mPaint!!.color = mColors[i]
                    drawLC(canvas, mWidth / 2 - mEntireLineLength / 2.2f, mHeight / 2 + mEntireLineLength.toFloat(), mWidth / 2 - mEntireLineLength / 2.2f, mHeight / 2 + mLineLength, mPaint!!, mCanvasAngle + i * 90)
                    i++
                }
            }
        }
    }

    private fun drawCRLC(canvas: Canvas, startX: Float, startY: Float, stopX: Float, stopY: Float, paint: Paint, rotate: Int) {
        canvas.rotate(rotate.toFloat(), mWidth / 2.toFloat(), mHeight / 2.toFloat())
        canvas.drawArc(RectF(startX - mCircleRadius, startY - mCircleRadius, startX + mCircleRadius, startY + mCircleRadius), 180f, 180f, true, mPaint!!)
        canvas.drawLine(startX, startY, stopX, stopY, paint)
        canvas.drawArc(RectF(stopX - mCircleRadius, stopY - mCircleRadius, stopX + mCircleRadius, stopY + mCircleRadius), 0f, 180f, true, mPaint!!)
        canvas.rotate(-rotate.toFloat(), mWidth / 2.toFloat(), mHeight / 2.toFloat())
    }

    private fun drawCR(canvas: Canvas, x: Float, y: Float, paint: Paint, rotate: Int) {
        canvas.rotate(rotate.toFloat(), mWidth / 2.toFloat(), mHeight / 2.toFloat())
        canvas.drawCircle(x, y, mCircleRadius.toFloat(), paint)
        canvas.rotate(-rotate.toFloat(), mWidth / 2.toFloat(), mHeight / 2.toFloat())
    }

    private fun drawCRCC(canvas: Canvas, x: Float, y: Float, paint: Paint, rotate: Int) {
        canvas.rotate(rotate.toFloat(), mWidth / 2.toFloat(), mHeight / 2.toFloat())
        canvas.drawCircle(x, y, mCircleRadius.toFloat(), paint)
        canvas.rotate(-rotate.toFloat(), mWidth / 2.toFloat(), mHeight / 2.toFloat())
    }

    private fun drawLC(canvas: Canvas, startX: Float, startY: Float, stopX: Float, stopY: Float, paint: Paint, rotate: Int) {
        canvas.rotate(rotate.toFloat(), mWidth / 2.toFloat(), mHeight / 2.toFloat())
        canvas.drawArc(RectF(startX - mCircleRadius, startY - mCircleRadius, startX + mCircleRadius, startY + mCircleRadius), 0f, 180f, true, mPaint!!)
        canvas.drawLine(startX, startY, stopX, stopY, paint)
        canvas.drawArc(RectF(stopX - mCircleRadius, stopY - mCircleRadius, stopX + mCircleRadius, stopY + mCircleRadius), 180f, 180f, true, mPaint!!)
        canvas.rotate(-rotate.toFloat(), mWidth / 2.toFloat(), mHeight / 2.toFloat())
    }

    private fun startCRLCAnim() {
        val animList: MutableCollection<Animator> = ArrayList()
        val canvasRotateAnim = ValueAnimator.ofInt(CANVAS_ROTATE_ANGLE, CANVAS_ROTATE_ANGLE + 360)
        canvasRotateAnim.addUpdateListener { animation -> mCanvasAngle = animation.animatedValue as Int }
        animList.add(canvasRotateAnim)
        val lineWidthAnim = ValueAnimator.ofFloat(mEntireLineLength.toFloat(), -mEntireLineLength.toFloat())
        lineWidthAnim.addUpdateListener { animation ->
            mLineLength = animation.animatedValue as Float
            invalidate()
        }
        animList.add(lineWidthAnim)
        val animationSet = AnimatorSet()
        animationSet.duration = mDuration.toLong()
        animationSet.playTogether(animList)
        animationSet.interpolator = LinearInterpolator()
        animationSet.addListener(object : AnimatorListener() {
            override fun onAnimationEnd(animation: Animator) {
                if (mStatus == STATUS_LOADING) {
                    mStep++
                    startCRAnim()
                }
            }
        })
        animationSet.start()
        mAnimList.add(animationSet)
    }

    private fun startCRAnim() {
        val canvasRotateAnim = ValueAnimator.ofInt(mCanvasAngle, mCanvasAngle + 180)
        canvasRotateAnim.duration = mDuration / 2.toLong()
        canvasRotateAnim.interpolator = LinearInterpolator()
        canvasRotateAnim.addUpdateListener { animation ->
            mCanvasAngle = animation.animatedValue as Int
            invalidate()
        }
        canvasRotateAnim.addListener(object : AnimatorListener() {
            override fun onAnimationEnd(animation: Animator) {
                if (mStatus == STATUS_LOADING) {
                    mStep++
                    startCRCCAnim()
                }
            }
        })
        canvasRotateAnim.start()
        mAnimList.add(canvasRotateAnim)
    }

    private fun startCRCCAnim() {
        val animList: MutableCollection<Animator> = ArrayList()
        val canvasRotateAnim = ValueAnimator.ofInt(mCanvasAngle, mCanvasAngle + 90, mCanvasAngle + 180)
        canvasRotateAnim.addUpdateListener { animation -> mCanvasAngle = animation.animatedValue as Int }
        animList.add(canvasRotateAnim)
        val circleYAnim = ValueAnimator.ofFloat(mEntireLineLength.toFloat(), mEntireLineLength / 4.toFloat(), mEntireLineLength.toFloat())
        circleYAnim.addUpdateListener { animation ->
            mCircleY = animation.animatedValue as Float
            invalidate()
        }
        animList.add(circleYAnim)
        val animationSet = AnimatorSet()
        animationSet.duration = mDuration.toLong()
        animationSet.playTogether(animList)
        animationSet.interpolator = LinearInterpolator()
        animationSet.addListener(object : AnimatorListener() {
            override fun onAnimationEnd(animation: Animator) {
                if (mStatus == STATUS_LOADING) {
                    mStep++
                    startLCAnim()
                }
            }
        })
        animationSet.start()
        mAnimList.add(animationSet)
    }

    private fun startLCAnim() {
        val lineWidthAnim = ValueAnimator.ofFloat(mEntireLineLength.toFloat(), -mEntireLineLength.toFloat())
        lineWidthAnim.duration = mDuration.toLong()
        lineWidthAnim.interpolator = LinearInterpolator()
        lineWidthAnim.addUpdateListener { animation ->
            mLineLength = animation.animatedValue as Float
            invalidate()
        }
        lineWidthAnim.addListener(object : AnimatorListener() {
            override fun onAnimationEnd(animation: Animator) {
                if (mStatus == STATUS_LOADING) {
                    mStep++
                    startCRLCAnim()
                }
            }
        })
        lineWidthAnim.start()
        mAnimList.add(lineWidthAnim)
    }

    fun setLineLength(scale: Float) {
        mEntireLineLength = (scale * (MAX_LINE_LENGTH - MIN_LINE_LENGTH)).toInt() + MIN_LINE_LENGTH
        reset()
    }

    fun setDuration(scale: Float) {
        mDuration = (scale * (MAX_DURATION - MIN_DURATION)).toInt() + MIN_DURATION
        reset()
    }

    fun start() {
        if (mStatus == STATUS_STILL) {
            mAnimList.clear()
            mStatus = STATUS_LOADING
            startCRLCAnim()
        }
    }

    fun reset() {
        if (mStatus == STATUS_LOADING) {
            mStatus = STATUS_STILL
            for (anim in mAnimList) {
                anim.cancel()
            }
        }
        initData()
        invalidate()
    }

    private fun dp2px(context: Context, dp: Float): Int {
        val scale = context.resources.displayMetrics.density
        return (dp * scale + 0.5f).toInt()
    }

    companion object {
        private const val STATUS_STILL = 0
        private const val STATUS_LOADING = 1
        private const val MAX_DURATION = 800
        private const val MIN_DURATION = 500
    }

    init {
        initView()
    }
}