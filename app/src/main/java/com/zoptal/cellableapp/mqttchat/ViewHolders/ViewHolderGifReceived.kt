package com.zoptal.cellableapp.mqttchat.ViewHolders

import android.graphics.Typeface
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.mqttchat.AppController

class ViewHolderGifReceived(view: View) : RecyclerView.ViewHolder(view) {
    //    public TextView senderName;
    @kotlin.jvm.JvmField
    var time: TextView
    @kotlin.jvm.JvmField
    var date: TextView
    @kotlin.jvm.JvmField
    var gifImage: ImageView
    @kotlin.jvm.JvmField
    var gifStillImage: ImageView

    init {

//        senderName = (TextView) view.findViewById(R.id.lblMsgFrom);
        gifImage = view.findViewById<View>(R.id.vidshow) as ImageView
        date = view.findViewById<View>(R.id.date) as TextView
        time = view.findViewById<View>(R.id.ts) as TextView
        gifStillImage = view.findViewById<View>(R.id.gifStillImage) as ImageView
        val tf = AppController.instance?.robotoCondensedFont
        time.setTypeface(tf, Typeface.ITALIC)
        date.setTypeface(tf, Typeface.ITALIC)
    }
}