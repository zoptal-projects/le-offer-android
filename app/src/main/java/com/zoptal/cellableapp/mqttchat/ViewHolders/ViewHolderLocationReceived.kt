package com.zoptal.cellableapp.mqttchat.ViewHolders

import android.graphics.Typeface
import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.mqttchat.AppController

/*
 * Created by moda on 02/04/16.
 */ /**
 * View holder for location received recycler view item
 */
class ViewHolderLocationReceived(view: View) : RecyclerView.ViewHolder(view), OnMapReadyCallback {
    //    public TextView senderName;
    @kotlin.jvm.JvmField
    var time: TextView
    @kotlin.jvm.JvmField
    var date: TextView
    var mapView: MapView
    @kotlin.jvm.JvmField
    var mMap: GoogleMap? = null
    var nameSelected = ""
    @kotlin.jvm.JvmField
    var positionSelected = LatLng(0.0, 0.0)
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap!!.uiSettings.isMapToolbarEnabled = true
        mMap!!.addMarker(MarkerOptions().position(positionSelected).title(nameSelected))
        mMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(positionSelected, 16.0f))
    }

    init {
        date = view.findViewById<View>(R.id.date) as TextView
        // senderName = (TextView) view.findViewById(R.id.lblMsgFrom);
        time = view.findViewById<View>(R.id.ts) as TextView
        mapView = view.findViewById<View>(R.id.map) as MapView
        mapView.onCreate(null)
        mapView.onResume()
        mapView.getMapAsync(this)
        val tf = AppController.instance?.robotoCondensedFont
        time.setTypeface(tf, Typeface.ITALIC)
        date.setTypeface(tf, Typeface.ITALIC)
    }
}