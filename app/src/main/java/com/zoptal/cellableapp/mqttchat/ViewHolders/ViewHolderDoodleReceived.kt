package com.zoptal.cellableapp.mqttchat.ViewHolders

import android.graphics.Typeface
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.RecyclerView
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.mqttchat.AppController
import com.zoptal.cellableapp.mqttchat.Utilities.RingProgressBar

class ViewHolderDoodleReceived(view: View) : RecyclerView.ViewHolder(view) {
    //    public TextView senderName;
    @kotlin.jvm.JvmField
    var time: TextView
    @kotlin.jvm.JvmField
    var date: TextView
    @kotlin.jvm.JvmField
    var fnf: TextView
    @kotlin.jvm.JvmField
    var imageView: AppCompatImageView
    @kotlin.jvm.JvmField
    var download: ImageView
    @kotlin.jvm.JvmField
    var progressBar: RingProgressBar
    @kotlin.jvm.JvmField
    var progressBar2: ProgressBar
    @kotlin.jvm.JvmField
    var cancel: ImageView

    init {

        // senderName = (TextView) view.findViewById(R.id.lblMsgFrom);
        date = view.findViewById<View>(R.id.date) as TextView
        imageView = view.findViewById<View>(R.id.imgshow) as AppCompatImageView
        time = view.findViewById<View>(R.id.ts) as TextView
        progressBar = view.findViewById<View>(R.id.progress) as RingProgressBar
        cancel = view.findViewById<View>(R.id.cancel) as ImageView
        progressBar2 = view.findViewById<View>(R.id.progress2) as ProgressBar
        download = view.findViewById<View>(R.id.download) as ImageView
        fnf = view.findViewById<View>(R.id.fnf) as TextView
        val tf = AppController.instance?.robotoCondensedFont
        time.setTypeface(tf, Typeface.ITALIC)
        date.setTypeface(tf, Typeface.ITALIC)
        fnf.setTypeface(tf, Typeface.NORMAL)
    }
}