package com.zoptal.cellableapp.mqttchat.Utilities

import android.app.Activity
import android.graphics.Point
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.PopupWindow
import androidx.core.content.ContextCompat
import com.zoptal.cellableapp.R

/*
 * Created by moda on 04/04/16.
 */ /* Floating view is used to display a custom view for attachments
 in the chat screen */
object FloatingView {
    private var popWindow: PopupWindow? = null
    @kotlin.jvm.JvmStatic
    fun onShowPopup(activity: Activity, inflatedView: View?) {
        val display = activity.windowManager.defaultDisplay
        val density = activity.resources.displayMetrics.density
        val size = Point()
        display.getSize(size)
        popWindow = PopupWindow(inflatedView, size.x, ViewGroup.LayoutParams.WRAP_CONTENT,
                true)
        popWindow!!.setBackgroundDrawable(ContextCompat.getDrawable(activity,
                R.drawable.comment_popup_bg))
        popWindow!!.isFocusable = true
        popWindow!!.isOutsideTouchable = true
        popWindow!!.softInputMode = WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN
        popWindow!!.showAtLocation(inflatedView, Gravity.TOP, 0,
                Math.round(86 * density))
    }

    @kotlin.jvm.JvmStatic
    fun dismissWindow() {
        if (popWindow != null) {
            popWindow!!.dismiss()
        }
    }
}