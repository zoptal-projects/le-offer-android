package com.zoptal.cellableapp.mqttchat.DownloadFile;

import okhttp3.logging.HttpLoggingInterceptor;

public class RestClient {

        public HttpLoggingInterceptor getClient() {

            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            return logging;
        }
    }