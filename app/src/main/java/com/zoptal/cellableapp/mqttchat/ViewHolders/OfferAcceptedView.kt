package com.zoptal.cellableapp.mqttchat.ViewHolders

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.mqttchat.AppController

/**
 * @since  9/15/2017.
 */
class OfferAcceptedView(itemView: View) : RecyclerView.ViewHolder(itemView) {
    @kotlin.jvm.JvmField
    var text_msg: TextView

    init {
        text_msg = itemView.findViewById<View>(R.id.sent_offer_view) as TextView
        text_msg.typeface = AppController.instance?.robotoMediumFont
        val accept_text = itemView.findViewById<View>(R.id.accept_text) as TextView
        accept_text.typeface = AppController.instance?.robotoMediumFont
    }
}