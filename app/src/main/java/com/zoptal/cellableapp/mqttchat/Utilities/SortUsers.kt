package com.zoptal.cellableapp.mqttchat.Utilities

import com.zoptal.cellableapp.mqttchat.ModelClasses.SelectUserItem
import java.util.*

/**
 * Created by moda on 02/08/17.
 */
class SortUsers : Comparator<Any?> {
    override fun compare(firstObjToCompare: Any?, secondObjToCompare: Any?): Int {
        val firstNameString = (firstObjToCompare as SelectUserItem?)!!.userName
        val secondNameString = (secondObjToCompare as SelectUserItem?)!!.userName
        return if (secondNameString == null || firstNameString == null) {
            0
        } else firstNameString.compareTo(secondNameString, ignoreCase = true)
    }
}