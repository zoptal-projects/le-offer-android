package com.zoptal.cellableapp.mqttchat.ViewHolders

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.zoptal.cellableapp.R

class ViewHolderSelectUser(view: View) : RecyclerView.ViewHolder(view) {
    @kotlin.jvm.JvmField
    var userName: TextView
    @kotlin.jvm.JvmField
    var userIdentifier: TextView
    @kotlin.jvm.JvmField
    var userImage: ImageView

    init {
        userName = view.findViewById<View>(R.id.userName) as TextView
        userIdentifier = view.findViewById<View>(R.id.userIdentifier) as TextView
        userImage = view.findViewById<View>(R.id.userImage) as ImageView
    }
}