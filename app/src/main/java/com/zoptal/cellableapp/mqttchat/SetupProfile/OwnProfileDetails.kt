package com.zoptal.cellableapp.mqttchat.SetupProfile

import android.Manifest
import android.annotation.TargetApi
import android.app.Activity
import android.content.ContentUris
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.*
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.FileProvider
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory
import com.android.volley.AuthFailureError
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.BitmapImageViewTarget
import com.bumptech.glide.signature.StringSignature
import com.google.android.material.snackbar.Snackbar
import com.zoptal.cellableapp.BuildConfig
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.mqttchat.AppController.Companion.instance
import com.zoptal.cellableapp.mqttchat.DownloadFile.FileUploadService
import com.zoptal.cellableapp.mqttchat.DownloadFile.FileUtils
import com.zoptal.cellableapp.mqttchat.DownloadFile.ServiceGenerator
import com.zoptal.cellableapp.mqttchat.ImageCropper.CropImage
import com.zoptal.cellableapp.mqttchat.SetupProfile.OwnProfileDetails
import com.zoptal.cellableapp.mqttchat.Utilities.ApiOnServer
import com.zoptal.cellableapp.mqttchat.Utilities.Utilities
import com.zoptal.cellableapp.mqttchat.Utilities.Utilities.Companion.tsInGmt
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.*

/**
 * Created by moda on 02/08/17.
 */
class OwnProfileDetails : AppCompatActivity() {
    private var profileImage: ImageView? = null
    private var root: RelativeLayout? = null
    private var picturePath: String? = null
    private var bitmap: Bitmap? = null
    private var imageUri: Uri? = null
    private var userName: TextView? = null
    private var userIdentifier: TextView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState)
        setContentView(R.layout.profile_screen)
        root = findViewById<View>(R.id.root) as RelativeLayout
        userIdentifier = findViewById<View>(R.id.phoneNumber) as TextView
        userName = findViewById<View>(R.id.profileStatus) as TextView
        val selectImage = findViewById<View>(R.id.profileImageSelector) as ImageView
        selectImage.setOnClickListener { selectImage() }
        val editName = findViewById<View>(R.id.profilePenStatus) as ImageView
        profileImage = findViewById<View>(R.id.profileImage) as ImageView
        setUpActivity()


/*
 * Start activity to update the status
 */editName.setOnClickListener { /*
                 * To set the current status value
                 */
            val i = Intent(this@OwnProfileDetails, WriteNewNameOrStatus::class.java)
            i.putExtra("currentValue", instance!!.getUserName())
            startActivityForResult(i, RESULT_UPDATE_NAME)
        }
        val close = findViewById<View>(R.id.close) as ImageView
        close.setOnClickListener { onBackPressed() }


        /*
         * To set the typeface values
         */
        val tf = instance!!.robotoCondensedFont
        val title = findViewById<View>(R.id.title) as TextView
        val userDetails = findViewById<View>(R.id.phonetext) as TextView
        //TextView statusHeading = (TextView) findViewById(R.id.tv);
        val notUrName = findViewById<View>(R.id.textView_notURUserName) as TextView
        title.setTypeface(tf, Typeface.BOLD)
        userDetails.setTypeface(tf, Typeface.NORMAL)
        notUrName.setTypeface(tf, Typeface.NORMAL)


//        statusHeading.setTypeface(tf, Typeface.NORMAL);
        userName!!.setTypeface(tf, Typeface.NORMAL)
        userIdentifier!!.setTypeface(tf, Typeface.NORMAL)
    }

    /**
     * Result of the permission request
     */
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>,
                                            grantResults: IntArray) {
        if (requestCode == 24) {
            if (grantResults.size == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.checkSelfPermission(this@OwnProfileDetails, Manifest.permission.CAMERA)
                        == PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.checkSelfPermission(this@OwnProfileDetails, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            == PackageManager.PERMISSION_GRANTED) {
                        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                        if (intent.resolveActivity(packageManager) != null) {
                            intent.putExtra(MediaStore.EXTRA_OUTPUT, setImageUri())
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
                            } else {
                                val resInfoList = packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY)
                                for (resolveInfo in resInfoList) {
                                    val packageName = resolveInfo.activityInfo.packageName
                                    grantUriPermission(packageName, imageUri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION or Intent.FLAG_GRANT_READ_URI_PERMISSION)
                                }
                            }
                            startActivityForResult(intent, RESULT_CAPTURE_IMAGE)
                        } else {
                            val snackbar = Snackbar.make(root!!, R.string.CameraAbsent,
                                    Snackbar.LENGTH_SHORT)
                            snackbar.show()
                            val view = snackbar.view
                            val txtv = view.findViewById<View>(R.id.snackbar_text) as TextView
                            txtv.gravity = Gravity.CENTER_HORIZONTAL
                        }
                    } else {
                        requestReadImagePermission(0)
                    }
                } else {
                    val snackbar = Snackbar.make(root!!, R.string.AccessCameraDenied,
                            Snackbar.LENGTH_SHORT)
                    snackbar.show()
                    val view = snackbar.view
                    val txtv = view.findViewById<View>(R.id.snackbar_text) as TextView
                    txtv.gravity = Gravity.CENTER_HORIZONTAL
                }
            } else {
                val snackbar = Snackbar.make(root!!, R.string.AccessCameraDenied,
                        Snackbar.LENGTH_SHORT)
                snackbar.show()
                val view = snackbar.view
                val txtv = view.findViewById<View>(R.id.snackbar_text) as TextView
                txtv.gravity = Gravity.CENTER_HORIZONTAL
            }
        } else if (requestCode == 26) {
            if (grantResults.size == 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.checkSelfPermission(this@OwnProfileDetails, Manifest.permission.READ_EXTERNAL_STORAGE)
                        == PackageManager.PERMISSION_GRANTED) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
                        intent.addCategory(Intent.CATEGORY_OPENABLE)
                        intent.type = "image/*"
                        startActivityForResult(Intent.createChooser(intent, getString(R.string.SelectPicture)), RESULT_LOAD_IMAGE)
                    } else {
                        val intent = Intent()
                        intent.type = "image/*"
                        intent.action = Intent.ACTION_GET_CONTENT
                        startActivityForResult(Intent.createChooser(intent, getString(R.string.SelectPicture)), RESULT_LOAD_IMAGE)
                    }
                } else {
                    val snackbar = Snackbar.make(root!!, R.string.AccessStorageDenied,
                            Snackbar.LENGTH_SHORT)
                    snackbar.show()
                    val view = snackbar.view
                    val txtv = view.findViewById<View>(R.id.snackbar_text) as TextView
                    txtv.gravity = Gravity.CENTER_HORIZONTAL
                }
            } else {
                val snackbar = Snackbar.make(root!!, R.string.AccessStorageDenied,
                        Snackbar.LENGTH_SHORT)
                snackbar.show()
                val view = snackbar.view
                val txtv = view.findViewById<View>(R.id.snackbar_text) as TextView
                txtv.gravity = Gravity.CENTER_HORIZONTAL
            }
        } else if (requestCode == 27) {
            if (grantResults.size == 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.checkSelfPermission(this@OwnProfileDetails, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        == PackageManager.PERMISSION_GRANTED) {
                    val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                    if (intent.resolveActivity(packageManager) != null) {
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, setImageUri())
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
                        } else {
                            val resInfoList = packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY)
                            for (resolveInfo in resInfoList) {
                                val packageName = resolveInfo.activityInfo.packageName
                                grantUriPermission(packageName, imageUri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION or Intent.FLAG_GRANT_READ_URI_PERMISSION)
                            }
                        }
                        startActivityForResult(intent, RESULT_CAPTURE_IMAGE)
                    } else {
                        val snackbar = Snackbar.make(root!!, R.string.CameraAbsent,
                                Snackbar.LENGTH_SHORT)
                        snackbar.show()
                        val view = snackbar.view
                        val txtv = view.findViewById<View>(R.id.snackbar_text) as TextView
                        txtv.gravity = Gravity.CENTER_HORIZONTAL
                    }
                } else {
                    val snackbar = Snackbar.make(root!!, R.string.AccessStorageDenied,
                            Snackbar.LENGTH_SHORT)
                    snackbar.show()
                    val view = snackbar.view
                    val txtv = view.findViewById<View>(R.id.snackbar_text) as TextView
                    txtv.gravity = Gravity.CENTER_HORIZONTAL
                }
            } else {
                val snackbar = Snackbar.make(root!!, R.string.AccessStorageDenied,
                        Snackbar.LENGTH_SHORT)
                snackbar.show()
                val view = snackbar.view
                val txtv = view.findViewById<View>(R.id.snackbar_text) as TextView
                txtv.gravity = Gravity.CENTER_HORIZONTAL
            }
        }
    }

    /*
      * To save the byte array received in to file
      */
    fun convertByteArrayToFile(data: ByteArray?, name: String, extension: String): File? {
        var file: File? = null
        try {
            val folder = File(Environment.getExternalStorageDirectory().path + ApiOnServer.CHAT_UPLOAD_THUMBNAILS_FOLDER)
            if (!folder.exists() && !folder.isDirectory) {
                folder.mkdirs()
            }
            file = File(Environment.getExternalStorageDirectory().path + ApiOnServer.CHAT_UPLOAD_THUMBNAILS_FOLDER, name + extension)
            file.createNewFile()
            val fos = FileOutputStream(file)
            fos.write(data)
            fos.flush()
            fos.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return file
    }

    public override fun onSaveInstanceState(outState: Bundle) {
        outState.putParcelable("file_uri", imageUri)
        outState.putString("file_path", picturePath)
        super.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        if (savedInstanceState != null) {
            imageUri = savedInstanceState.getParcelable("file_uri")
            picturePath = savedInstanceState.getString("file_path")
        }
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RESULT_LOAD_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                try {
                    picturePath = getPath(this@OwnProfileDetails, data!!.data)
                    if (picturePath != null) {
                        val options = BitmapFactory.Options()
                        options.inJustDecodeBounds = true
                        BitmapFactory.decodeFile(picturePath, options)
                        if (options.outWidth > 0 && options.outHeight > 0) {


                            /*
                             * Have to start the intent for the image cropping
                             */
                            CropImage.activity(data.data)
                                    .start(this)
                        } else {
                            if (root != null) {
                                val snackbar = Snackbar.make(root!!, R.string.AnotherImage, Snackbar.LENGTH_SHORT)
                                snackbar.show()
                                val view = snackbar.view
                                val txtv = view.findViewById<View>(R.id.snackbar_text) as TextView
                                txtv.gravity = Gravity.CENTER_HORIZONTAL
                            }
                        }
                    } else {
                        if (root != null) {
                            val snackbar = Snackbar.make(root!!, R.string.AnotherImage, Snackbar.LENGTH_SHORT)
                            snackbar.show()
                            val view = snackbar.view
                            val txtv = view.findViewById<View>(R.id.snackbar_text) as TextView
                            txtv.gravity = Gravity.CENTER_HORIZONTAL
                        }
                    }
                } catch (e: OutOfMemoryError) {
                    val snackbar = Snackbar.make(root!!, R.string.OOM, Snackbar.LENGTH_SHORT)
                    snackbar.show()
                    val view = snackbar.view
                    val txtv = view.findViewById<View>(R.id.snackbar_text) as TextView
                    txtv.gravity = Gravity.CENTER_HORIZONTAL
                }
            } else {
                if (resultCode == Activity.RESULT_CANCELED) {
                    val snackbar = Snackbar.make(root!!, R.string.SelectionCanceled, Snackbar.LENGTH_SHORT)
                    snackbar.show()
                    val view = snackbar.view
                    val txtv = view.findViewById<View>(R.id.snackbar_text) as TextView
                    txtv.gravity = Gravity.CENTER_HORIZONTAL
                } else {
                    val snackbar = Snackbar.make(root!!, R.string.ImageFailed, Snackbar.LENGTH_SHORT)
                    snackbar.show()
                    val view = snackbar.view
                    val txtv = view.findViewById<View>(R.id.snackbar_text) as TextView
                    txtv.gravity = Gravity.CENTER_HORIZONTAL
                }
            }
        } else if (requestCode == RESULT_CAPTURE_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                try {
                    // picturePath = getPath(Deal_Add.this, imageUri);
                    val options = BitmapFactory.Options()
                    options.inJustDecodeBounds = true
                    BitmapFactory.decodeFile(picturePath, options)
                    if (options.outWidth > 0 && options.outHeight > 0) {
                        CropImage.activity(imageUri)
                                .start(this)
                    } else {
                        picturePath = null
                        val snackbar = Snackbar.make(root!!, R.string.CaptureFailed, Snackbar.LENGTH_SHORT)
                        snackbar.show()
                        val view = snackbar.view
                        val txtv = view.findViewById<View>(R.id.snackbar_text) as TextView
                        txtv.gravity = Gravity.CENTER_HORIZONTAL
                    }
                } catch (e: OutOfMemoryError) {
                    picturePath = null
                    val snackbar = Snackbar.make(root!!, R.string.OOM, Snackbar.LENGTH_SHORT)
                    snackbar.show()
                    val view = snackbar.view
                    val txtv = view.findViewById<View>(R.id.snackbar_text) as TextView
                    txtv.gravity = Gravity.CENTER_HORIZONTAL
                }
            } else {
                if (resultCode == Activity.RESULT_CANCELED) {
                    picturePath = null
                    val snackbar = Snackbar.make(root!!, R.string.CaptureCanceled, Snackbar.LENGTH_SHORT)
                    snackbar.show()
                    val view = snackbar.view
                    val txtv = view.findViewById<View>(R.id.snackbar_text) as TextView
                    txtv.gravity = Gravity.CENTER_HORIZONTAL
                } else {
                    picturePath = null
                    val snackbar = Snackbar.make(root!!, R.string.CaptureFailed, Snackbar.LENGTH_SHORT)
                    snackbar.show()
                    val view = snackbar.view
                    val txtv = view.findViewById<View>(R.id.snackbar_text) as TextView
                    txtv.gravity = Gravity.CENTER_HORIZONTAL
                }
            }
        } else if (requestCode == RESULT_UPDATE_NAME) {

            /*
             *
             * For return of the update user name activity
             */
            if (resultCode == Activity.RESULT_OK) {
                val updatedName = data!!.extras!!.getString("updatedValue")
                userName!!.text = updatedName


                /*
                 * Hit the put profile api on server in background
                 */updateTheValueOnServer(updatedName, 0)
            }
            //            else {
//                /*
//                 * Update name canceled
//                 */
//
//
//            }
        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            try {
                val result = CropImage.getActivityResult(data)
                if (resultCode == Activity.RESULT_OK) {
                    picturePath = getPath(this@OwnProfileDetails, result.uri)
                    if (picturePath != null) {
                        bitmap = getCircleBitmap(BitmapFactory.decodeFile(picturePath))
                        if (bitmap != null && bitmap!!.width > 0 && bitmap!!.height > 0) {
                            profileImage!!.setImageBitmap(bitmap)
                            /*
                         * To start uploading of the bitmap on the background thread
                         */uploadProfilePic()
                        } else {
                            picturePath = null
                            if (root != null) {
                                val snackbar = Snackbar.make(root!!, R.string.CropFailed, Snackbar.LENGTH_SHORT)
                                snackbar.show()
                                val view = snackbar.view
                                val txtv = view.findViewById<View>(R.id.snackbar_text) as TextView
                                txtv.gravity = Gravity.CENTER_HORIZONTAL
                            }
                        }
                    } else {
                        picturePath = null
                        if (root != null) {
                            val snackbar = Snackbar.make(root!!, R.string.CropFailed, Snackbar.LENGTH_SHORT)
                            snackbar.show()
                            val view = snackbar.view
                            val txtv = view.findViewById<View>(R.id.snackbar_text) as TextView
                            txtv.gravity = Gravity.CENTER_HORIZONTAL
                        }
                    }
                } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                    picturePath = null
                    if (root != null) {
                        val snackbar = Snackbar.make(root!!, R.string.CropFailed, Snackbar.LENGTH_SHORT)
                        snackbar.show()
                        val view = snackbar.view
                        val txtv = view.findViewById<View>(R.id.snackbar_text) as TextView
                        txtv.gravity = Gravity.CENTER_HORIZONTAL
                    }
                }
            } catch (e: OutOfMemoryError) {
                picturePath = null
                val snackbar = Snackbar.make(root!!, R.string.OOM, Snackbar.LENGTH_SHORT)
                snackbar.show()
                val view = snackbar.view
                val txtv = view.findViewById<View>(R.id.snackbar_text) as TextView
                txtv.gravity = Gravity.CENTER_HORIZONTAL
            }
        }
    }

    private fun selectImage() {
        val builder: AlertDialog.Builder
        builder = AlertDialog.Builder(this@OwnProfileDetails)
        builder.setTitle(R.string.ProfileImage)
        builder.setIcon(R.drawable.orca_attach_camera_pressed)
        builder.setItems(arrayOf<CharSequence>(getString(R.string.FromGallery),
                getString(R.string.FromCamera), getString(R.string.cancel))
        ) { dialog, which ->
            when (which) {
                1 -> {
                    checkCameraPermissionImage()
                    dialog.cancel()
                }
                0 -> checkReadImage()
                2 -> {
                }
            }
        }
        val dialog = builder.create()
        dialog.show()
    }

    /**
     * Check access gallery permission
     */
    private fun checkReadImage() {
        if (ActivityCompat.checkSelfPermission(this@OwnProfileDetails, Manifest.permission.READ_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this@OwnProfileDetails, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
                intent.addCategory(Intent.CATEGORY_OPENABLE)
                intent.type = "image/*"
                startActivityForResult(Intent.createChooser(intent, getString(R.string.SelectPicture)), RESULT_LOAD_IMAGE)
            } else {
                val intent = Intent()
                intent.type = "image/*"
                intent.action = Intent.ACTION_GET_CONTENT
                startActivityForResult(Intent.createChooser(intent, getString(R.string.SelectPicture)), RESULT_LOAD_IMAGE)
            }
        } else {
            requestReadImagePermission(1)
        }
    }

    /**
     * Request access gallery permission
     */
    private fun requestReadImagePermission(k: Int) {
        if (k == 1) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this@OwnProfileDetails,
                            Manifest.permission.READ_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(this@OwnProfileDetails,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                val snackbar = Snackbar.make(root!!, R.string.SelectProfile,
                        Snackbar.LENGTH_INDEFINITE).setAction(getString(R.string.Ok)) {
                    ActivityCompat.requestPermissions(this@OwnProfileDetails, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE),
                            26)
                }
                snackbar.show()
                val view = snackbar.view
                val txtv = view.findViewById<View>(R.id.snackbar_text) as TextView
                txtv.gravity = Gravity.CENTER_HORIZONTAL
            } else {
                ActivityCompat.requestPermissions(this@OwnProfileDetails, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE),
                        26)
            }
        } else if (k == 0) {


            /*
             * For capturing the image permission
             */
            if (ActivityCompat.shouldShowRequestPermissionRationale(this@OwnProfileDetails,
                            Manifest.permission.READ_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(this@OwnProfileDetails,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                val snackbar = Snackbar.make(root!!, R.string.CaptureImagePermission,
                        Snackbar.LENGTH_INDEFINITE).setAction(getString(R.string.Ok)) {
                    ActivityCompat.requestPermissions(this@OwnProfileDetails, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE),
                            27)
                }
                snackbar.show()
                val view = snackbar.view
                val txtv = view.findViewById<View>(R.id.snackbar_text) as TextView
                txtv.gravity = Gravity.CENTER_HORIZONTAL
            } else {
                ActivityCompat.requestPermissions(this@OwnProfileDetails, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE),
                        27)
            }
        }
    }

    /**
     * Check access camera permission
     */
    private fun checkCameraPermissionImage() {
        if (ActivityCompat.checkSelfPermission(this@OwnProfileDetails, Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.checkSelfPermission(this@OwnProfileDetails, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                if (intent.resolveActivity(packageManager) != null) {
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, setImageUri())
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
                    } else {
                        val resInfoList = packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY)
                        for (resolveInfo in resInfoList) {
                            val packageName = resolveInfo.activityInfo.packageName
                            grantUriPermission(packageName, imageUri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION or Intent.FLAG_GRANT_READ_URI_PERMISSION)
                        }
                    }
                    startActivityForResult(intent, RESULT_CAPTURE_IMAGE)
                } else {
                    val snackbar = Snackbar.make(root!!, R.string.CameraAbsent,
                            Snackbar.LENGTH_SHORT)
                    snackbar.show()
                    val view = snackbar.view
                    val txtv = view.findViewById<View>(R.id.snackbar_text) as TextView
                    txtv.gravity = Gravity.CENTER_HORIZONTAL
                }
            } else {


                /*
                 *permission required to save the image captured
                 */
                requestReadImagePermission(0)
            }
        } else {
            requestCameraPermissionImage()
        }
    }

    /**
     * Request access camera permission
     */
    private fun requestCameraPermissionImage() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this@OwnProfileDetails,
                        Manifest.permission.CAMERA)) {
            val snackbar = Snackbar.make(root!!, R.string.CaptureProfile,
                    Snackbar.LENGTH_INDEFINITE).setAction(getString(R.string.Ok)) {
                ActivityCompat.requestPermissions(this@OwnProfileDetails, arrayOf(Manifest.permission.CAMERA),
                        24)
            }
            snackbar.show()
            val view = snackbar.view
            val txtv = view.findViewById<View>(R.id.snackbar_text) as TextView
            txtv.gravity = Gravity.CENTER_HORIZONTAL
        } else {
            ActivityCompat.requestPermissions(this@OwnProfileDetails, arrayOf(Manifest.permission.CAMERA),
                    24)
        }
    }

    private fun setImageUri(): Uri {
        var name: String? = tsInGmt()
        name = Utilities().gmtToEpoch(name)
        var folder: File? = File(Environment.getExternalStorageDirectory().path + ApiOnServer.IMAGE_CAPTURE_URI)
        if (!folder!!.exists() && !folder.isDirectory) {
            folder.mkdirs()
        }
        var file: File? = File(Environment.getExternalStorageDirectory().path + ApiOnServer.IMAGE_CAPTURE_URI, "$name.jpg")
        if (!file!!.exists()) {
            try {
                file.createNewFile()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
        val imgUri = FileProvider.getUriForFile(this@OwnProfileDetails, applicationContext.packageName + ".provider", file)
        imageUri = imgUri
        picturePath = file.absolutePath
        name = null
        folder = null
        file = null
        return imgUri
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        setUpActivity()
    }

    private fun setUpActivity() {
        userName!!.text = instance!!.getUserName()
        userIdentifier!!.text = instance!!.userIdentifier
        try {
            val userImageUrl = instance!!.getUserImageUrl()

/*
 *To load the new image everytime
 *
 */if (userImageUrl != null && !userImageUrl.isEmpty()) {
                Glide.with(this@OwnProfileDetails)
                        .load(userImageUrl).asBitmap() //                        .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .signature(StringSignature(System.currentTimeMillis().toString()))
                        .centerCrop()
                        .placeholder(R.drawable.chat_attachment_profile_default_image_frame).into(object : BitmapImageViewTarget(profileImage) {
                            override fun setResource(resource: Bitmap) {
                                val circularBitmapDrawable = RoundedBitmapDrawableFactory.create(resources, resource)
                                circularBitmapDrawable.isCircular = true
                                profileImage!!.setImageDrawable(circularBitmapDrawable)
                            }
                        })
            }
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }
    }

    private fun updateTheValueOnServer(valueToUpdate: String?, type: Int) {
/*
 * Hit the put profile api
 */
        val obj = JSONObject()
        try {
            obj.put("deviceId", instance!!.deviceId)

            /*
             * DeviceType  1-ios,2-android
             */obj.put("deviceType", 2)
            obj.put("deviceModel", Build.MODEL)
            obj.put("deviceMake", Build.MANUFACTURER)
            obj.put("deviceOs", Build.VERSION.SDK_INT.toString())
            obj.put("pushToken", "")
            obj.put("appVersion", BuildConfig.VERSION_NAME)
            obj.put("versionCode", BuildConfig.VERSION_CODE)
            when (type) {
                0 -> obj.put("userName", valueToUpdate)
                1 -> {
                    obj.put("userName", instance!!.getUserName())
                    obj.put("profilePic", valueToUpdate)
                }
            }

            /*
             *
             *For update of the profile pic details
             * */

//            if (profilePic != null) {
//                obj.put("profilePic", profilePic);
//            } else {
///*
// * Might be possible that profile pic was there but was deleted later,So server guy will delete if any such file exists for given user
// */
////                obj.put("profilePic", "");
//
//
//                if (userAlreadyHasImage) {
//                    obj.put("profilePic", userImageUrl);
//                } else {
//                    obj.put("profilePic", "null");
//                }
//            }
            Log.d("log61", obj.toString())
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        val jsonObjReq: JsonObjectRequest = object : JsonObjectRequest(Method.PUT,
                ApiOnServer.USER_PROFILE, obj, Response.Listener { response ->
            try {
                when (response.getInt("code")) {
                    200 -> when (type) {
                        0 ->                                     /*
                 * Name updated successfully
                  */instance!!.setUserName(valueToUpdate)
                        1 ->                 /*
                 * Profile pic updated successfully
                  */instance!!.setUserImageUrl(valueToUpdate)
                    }
                    else -> if (root != null) {
                        val snackbar = Snackbar.make(root!!, response.getString("message"), Snackbar.LENGTH_SHORT)
                        snackbar.show()
                        val view = snackbar.view
                        val txtv = view.findViewById<View>(R.id.snackbar_text) as TextView
                        txtv.gravity = Gravity.CENTER_HORIZONTAL
                    }
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }, Response.ErrorListener { error -> // requesting = false;
            error.printStackTrace()
            when (type) {
                0 -> {
                    val snackbar = Snackbar.make(root!!, getString(R.string.UpdateNameFailed), Snackbar.LENGTH_SHORT)
                    snackbar.show()
                    val view = snackbar.view
                    val txtv = view.findViewById<View>(R.id.snackbar_text) as TextView
                    txtv.gravity = Gravity.CENTER_HORIZONTAL
                }
                1 -> {
                    val snackbar2 = Snackbar.make(root!!, getString(R.string.UpdatePicFailed), Snackbar.LENGTH_SHORT)
                    snackbar2.show()
                    val view2 = snackbar2.view
                    val txtv2 = view2.findViewById<View>(R.id.snackbar_text) as TextView
                    txtv2.gravity = Gravity.CENTER_HORIZONTAL
                }
            }
        }) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["Authorization"] = "KMajNKHPqGt6kXwUbFN3dU46PjThSNTtrEnPZUefdasdfghsaderf1234567890ghfghsdfghjfghjkswdefrtgyhdfghj"
                headers["token"] = instance!!.apiToken!!
                return headers
            }
        }
        jsonObjReq.retryPolicy = DefaultRetryPolicy(
                20 * 1000, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        /* Add the request to the RequestQueue.*/instance!!.addToRequestQueue(jsonObjReq, "updateProfileApi")
    }

    private fun getCircleBitmap(bitmap: Bitmap): Bitmap {
        val circuleBitmap = Bitmap.createBitmap(bitmap.width,
                bitmap.width, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(circuleBitmap)
        val color = Color.GRAY
        val paint = Paint()
        val rect = Rect(0, 0, bitmap.width, bitmap.width)
        val rectF = RectF(rect)
        paint.isAntiAlias = true
        canvas.drawARGB(0, 0, 0, 0)
        paint.color = color
        canvas.drawOval(rectF, paint)
        paint.xfermode = PorterDuffXfermode(PorterDuff.Mode.SRC_IN)
        canvas.drawBitmap(bitmap, rect, rect, paint)
        return circuleBitmap
    }

    private fun uploadProfilePic() {
        if (picturePath != null) {
            val fileUri: Uri
            val name = instance!!.userId + System.currentTimeMillis() //String.valueOf(System.currentTimeMillis());
            if (bitmap != null) {
                var baos: ByteArrayOutputStream? = ByteArrayOutputStream()
                bitmap!!.compress(Bitmap.CompressFormat.JPEG, IMAGE_QUALITY, baos)


                // bm = null;
                var b = baos!!.toByteArray()
                try {
                    baos.close()
                } catch (e: IOException) {
                    e.printStackTrace()
                }
                baos = null
                var f = convertByteArrayToFile(b, name, ".jpg")
                b = null
                fileUri = Uri.fromFile(f)
                f = null
                val service = ServiceGenerator.createService(FileUploadService::class.java)
                val file = FileUtils.getFile(this, fileUri)
                var url: String? = null
                url = "$name.jpg"
                val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file)
                val body = MultipartBody.Part.createFormData("photo", url, requestFile)
                val descriptionString = getString(R.string.string_803)
                val description = RequestBody.create(
                        MediaType.parse("multipart/form-data"), descriptionString)
                val call = service.uploadProfilePic(description, body, ApiOnServer.AUTH_KEY)
                call.enqueue(object : Callback<ResponseBody?> {
                    override fun onResponse(call: Call<ResponseBody?>,
                                            response: retrofit2.Response<ResponseBody?>) {


/*
 *
 *
 * has to get url from the server in response
 *
 *
 * */
                        //   Log.d("log91", response.code() + "");
                        if (response.code() == 200) {
                            var url: String? = null
                            url = "$name.jpg"
                            updateTheValueOnServer(ApiOnServer.PROFILEPIC_UPLOAD_PATH + url, 1)
                            val fdelete = File(fileUri.path)
                            if (fdelete.exists()) fdelete.delete()
                        } else {
                            if (root != null) {
                                val snackbar = Snackbar.make(root!!, R.string.Upload_Failed, Snackbar.LENGTH_SHORT)
                                snackbar.show()
                                val view = snackbar.view
                                val txtv = view.findViewById<View>(R.id.snackbar_text) as TextView
                                txtv.gravity = Gravity.CENTER_HORIZONTAL
                            }
                        }
                    }

                    override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                        if (root != null) {
                            val snackbar = Snackbar.make(root!!, R.string.Upload_Failed, Snackbar.LENGTH_SHORT)
                            snackbar.show()
                            val view = snackbar.view
                            val txtv = view.findViewById<View>(R.id.snackbar_text) as TextView
                            txtv.gravity = Gravity.CENTER_HORIZONTAL
                        }
                    }
                })
            } else {
                if (root != null) {
                    val snackbar = Snackbar.make(root!!, R.string.Upload_Failed, Snackbar.LENGTH_SHORT)
                    snackbar.show()
                    val view = snackbar.view
                    val txtv = view.findViewById<View>(R.id.snackbar_text) as TextView
                    txtv.gravity = Gravity.CENTER_HORIZONTAL
                }
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        supportFinishAfterTransition()
    }

    companion object {
        private const val RESULT_CAPTURE_IMAGE = 0
        private const val RESULT_LOAD_IMAGE = 1
        private const val RESULT_UPDATE_NAME = 2
        private const val IMAGE_QUALITY = 50 //change it to higher level if want,but then slower image uploading/downloading

        @TargetApi(Build.VERSION_CODES.KITKAT)
        fun getPath(context: Context, uri: Uri?): String? {
            val isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT
            if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
                if (isExternalStorageDocument(uri)) {
                    val docId = DocumentsContract.getDocumentId(uri)
                    val split = docId.split(":".toRegex()).toTypedArray()
                    val type = split[0]
                    if ("primary".equals(type, ignoreCase = true)) {
                        return Environment.getExternalStorageDirectory().toString() + "/" + split[1]
                    }
                } else if (isDownloadsDocument(uri)) {
                    val id = DocumentsContract.getDocumentId(uri)
                    val contentUri = ContentUris.withAppendedId(
                            Uri.parse("content://downloads/public_downloads"), java.lang.Long.valueOf(id))
                    return getDataColumn(context, contentUri, null, null)
                } else if (isMediaDocument(uri)) {
                    val docId = DocumentsContract.getDocumentId(uri)
                    val split = docId.split(":".toRegex()).toTypedArray()
                    val type = split[0]
                    var contentUri: Uri? = null
                    if ("image" == type) {
                        contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                    } else if ("video" == type) {
                        contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI
                    } else if ("audio" == type) {
                        contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
                    }
                    val selection = "_id=?"
                    val selectionArgs = arrayOf(
                            split[1]
                    )
                    return getDataColumn(context, contentUri, selection, selectionArgs)
                }
            } else if ("content".equals(uri!!.scheme, ignoreCase = true)) {
                return if (isGooglePhotosUri(uri)) uri.lastPathSegment else getDataColumn(context, uri, null, null)
            } else if ("file".equals(uri.scheme, ignoreCase = true)) {
                return uri.path
            }
            return null
        }

        /**
         * Get the value of the data column for this Uri. This is useful for
         * MediaStore Uris, and other file-based ContentProviders.
         *
         * @param context       The context.
         * @param uri           The Uri to query.
         * @param selection     (Optional) Filter used in the query.
         * @param selectionArgs (Optional) Selection arguments used in the query.
         * @return The value of the _data column, which is typically a file path.
         */
        fun getDataColumn(context: Context, uri: Uri?, selection: String?,
                          selectionArgs: Array<String>?): String? {
            var cursor: Cursor? = null
            val column = "_data"
            val projection = arrayOf(
                    column
            )
            try {
                cursor = context.contentResolver.query(uri!!, projection, selection, selectionArgs,
                        null)
                if (cursor != null && cursor.moveToFirst()) {
                    val index = cursor.getColumnIndexOrThrow(column)
                    return cursor.getString(index)
                }
            } finally {
                cursor?.close()
            }
            return null
        }

        /**
         * @param uri The Uri to check.
         * @return Whether the Uri authority is ExternalStorageProvider.
         */
        fun isExternalStorageDocument(uri: Uri?): Boolean {
            return "com.android.externalstorage.documents" == uri!!.authority
        }

        /**
         * @param uri The Uri to check.
         * @return Whether the Uri authority is DownloadsProvider.
         */
        fun isDownloadsDocument(uri: Uri?): Boolean {
            return "com.android.providers.downloads.documents" == uri!!.authority
        }
        /*
     *To allow user to select the method by which he would like to add the new profile image
     */
        /**
         * @param uri The Uri to check.
         * @return Whether the Uri authority is MediaProvider.
         */
        fun isMediaDocument(uri: Uri?): Boolean {
            return "com.android.providers.media.documents" == uri!!.authority
        }

        /**
         * @param uri The Uri to check.
         * @return Whether the Uri authority is Google Photos.
         */
        fun isGooglePhotosUri(uri: Uri?): Boolean {
            return "com.google.android.apps.photos.content" == uri!!.authority
        }
    }
}