package com.zoptal.cellableapp.mqttchat.ViewHolders

import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.mqttchat.AppController

/**
 * @since 9/20/2017.
 */
class PaypalLinkReceived(itemView: View) : RecyclerView.ViewHolder(itemView) {
    @kotlin.jvm.JvmField
    var relative_layout_message: RelativeLayout
    @kotlin.jvm.JvmField
    var offerAmount: TextView

    init {
        offerAmount = itemView.findViewById<View>(R.id.offerAmount) as TextView
        offerAmount.typeface = AppController.instance?.robotoMediumFont
        relative_layout_message = itemView.findViewById<View>(R.id.relative_layout_message) as RelativeLayout
        val offerAmount = itemView.findViewById<View>(R.id.offerAmount) as TextView
        offerAmount.typeface = AppController.instance?.robotoMediumFont
    }
}