package com.zoptal.cellableapp.mqttchat.Utilities

import android.content.Context
import android.content.Intent
import androidx.legacy.content.WakefulBroadcastReceiver
import com.zoptal.cellableapp.mqttchat.AppController.Companion.instance

/*
 * To keep the device awake when it is booting
 */
class BootCompletedIntentReceiver : WakefulBroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        instance!!.createMQttConnection(instance!!.userId, true)
    }
}