package com.zoptal.cellableapp.mqttchat.ViewHolders

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.mqttchat.AppController

/**
 *
 * @since  9/11/2017.
 */
class OfferReceived(itemView: View) : RecyclerView.ViewHolder(itemView) {
    @kotlin.jvm.JvmField
    var offerPrice: TextView

    init {
        offerPrice = itemView.findViewById<View>(R.id.offerPrice) as TextView
        offerPrice.typeface = AppController.instance?.robotoMediumFont
        val sent_offer_view = itemView.findViewById<View>(R.id.sent_offer_view) as TextView
        sent_offer_view.typeface = AppController.instance?.robotoMediumFont
    }
}