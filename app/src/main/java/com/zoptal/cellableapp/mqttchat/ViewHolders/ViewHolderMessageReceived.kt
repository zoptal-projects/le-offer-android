package com.zoptal.cellableapp.mqttchat.ViewHolders

import android.graphics.Typeface
import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.mqttchat.AppController

/*
 * Created by moda on 02/04/16.
 */ /**
 * View holder for text message received recycler view item
 */
class ViewHolderMessageReceived(view: View) : RecyclerView.ViewHolder(view) {
    //    public TextView senderName;
    @kotlin.jvm.JvmField
    var message: TextView
    @kotlin.jvm.JvmField
    var time: TextView
    @kotlin.jvm.JvmField
    var date: TextView

    init {
        date = view.findViewById<View>(R.id.date) as TextView
        // senderName = (TextView) view.findViewById(R.id.lblMsgFrom);
        message = view.findViewById<View>(R.id.txtMsg) as TextView
        time = view.findViewById<View>(R.id.ts) as TextView
        val tf = AppController.instance?.robotoCondensedFont
        time.setTypeface(tf, Typeface.ITALIC)
        date.setTypeface(tf, Typeface.ITALIC)
        message.setTypeface(tf, Typeface.NORMAL)
    }
}