package com.zoptal.cellableapp.mqttchat.ViewHolders

import android.graphics.Typeface
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.mqttchat.AppController
import com.zoptal.cellableapp.mqttchat.Utilities.RingProgressBar

/**
 * View holder for audio received recycler view item
 */
class ViewHolderAudioReceived(view: View) : RecyclerView.ViewHolder(view) {
    //    public TextView senderName;
    @kotlin.jvm.JvmField
    var time: TextView
    @kotlin.jvm.JvmField
    var date: TextView
    var tv: TextView
    @kotlin.jvm.JvmField
    var fnf: TextView
    @kotlin.jvm.JvmField
    var progressBar: RingProgressBar
    @kotlin.jvm.JvmField
    var playButton: ImageView
    @kotlin.jvm.JvmField
    var download: ImageView
    @kotlin.jvm.JvmField
    var progressBar2: ProgressBar
    @kotlin.jvm.JvmField
    var cancel: ImageView

    init {
        playButton = view.findViewById<View>(R.id.imageView26) as ImageView


        // senderName = (TextView) view.findViewById(R.id.lblMsgFrom);
        tv = view.findViewById<View>(R.id.tv) as TextView
        time = view.findViewById<View>(R.id.ts) as TextView
        date = view.findViewById<View>(R.id.date) as TextView
        progressBar2 = view.findViewById<View>(R.id.progress2) as ProgressBar
        progressBar = view.findViewById<View>(R.id.progress) as RingProgressBar
        download = view.findViewById<View>(R.id.download) as ImageView
        cancel = view.findViewById<View>(R.id.cancel) as ImageView
        fnf = view.findViewById<View>(R.id.fnf) as TextView
        val tf = AppController.instance?.robotoCondensedFont
        time.setTypeface(tf, Typeface.ITALIC)
        date.setTypeface(tf, Typeface.ITALIC)
        tv.setTypeface(tf, Typeface.NORMAL)
        fnf.setTypeface(tf, Typeface.NORMAL)
    }
}