package com.zoptal.cellableapp.mqttchat.Utilities

import android.app.Activity
import android.app.AlertDialog
import android.app.Service
import android.content.Context
import android.content.Intent
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.os.IBinder
import android.provider.Settings
import android.util.Log
import androidx.core.app.ActivityOptionsCompat
import com.zoptal.cellableapp.R

/*
 * Created by moda on 15/04/16.
 */ /**
 * Helper class to check if location services are enabled
 */
class GPSTracker(private val mContext: Context) : Service(), LocationListener {

    // flag for GPS status
    var isGPSEnabled = false

    // flag for network status
    var isNetworkEnabled = false

    // flag for GPS status
    var canGetLocation = false
    var mLocation: Location? = null // location
    var mLatitude =0.0// latitude = 0.0
    var mLongitude =0.0// longitude = 0.0

    // Declaring a Location Manager
    protected var locationManager: LocationManager? = null
    fun getLocation(): Location? {
        try {
            locationManager = mContext
                    .getSystemService(Context.LOCATION_SERVICE) as LocationManager

            // getting GPS status
            isGPSEnabled = locationManager!!
                    .isProviderEnabled(LocationManager.GPS_PROVIDER)

            // getting network status
            isNetworkEnabled = locationManager!!
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER)
            if (!isGPSEnabled && !isNetworkEnabled) {
                // no network provider is enabled
                canGetLocation = false
            } else {
                canGetLocation = true
                if (isNetworkEnabled) {
                    try {
                        locationManager!!.requestLocationUpdates(
                                LocationManager.NETWORK_PROVIDER,
                                MIN_TIME_BW_UPDATES,
                                MIN_DISTANCE_CHANGE_FOR_UPDATES.toFloat(), this)
                    } catch (e: SecurityException) {
                        e.printStackTrace()
                    }
                    if (locationManager != null) {
                        try {
                            mLocation = locationManager!!
                                    .getLastKnownLocation(LocationManager.NETWORK_PROVIDER)
                        } catch (e: SecurityException) {
                            e.printStackTrace()
                        }
                        if (mLocation != null) {
                            mLatitude = mLocation!!.latitude
                            mLongitude = mLocation!!.longitude
                            Log.e("Your Location",
                                    "latitude2:" + mLocation!!.latitude
                                            + ", longitude2: "
                                            + mLocation!!.longitude)
                        }
                    }
                }
                // if GPS Enabled get lat/long using GPS Sexrvices
                if (isGPSEnabled) {
                    if (mLocation == null) {
                        try {
                            locationManager!!.requestLocationUpdates(
                                    LocationManager.GPS_PROVIDER,
                                    MIN_TIME_BW_UPDATES,
                                    MIN_DISTANCE_CHANGE_FOR_UPDATES.toFloat(), this)
                        } catch (e: SecurityException) {
                            e.printStackTrace()
                        }
                        if (locationManager != null) {
                            try {
                                mLocation = locationManager!!
                                        .getLastKnownLocation(LocationManager.GPS_PROVIDER)
                            } catch (e: SecurityException) {
                                e.printStackTrace()
                            }
                            if (mLocation != null) {
                                mLatitude = mLocation!!.getLatitude()
                                mLongitude = mLocation!!.getLongitude()
                                Log.e("Your Location",
                                        "latitude1:" + mLocation!!.getLatitude()
                                                + ", longitude1: "
                                                + mLocation!!.getLongitude())
                            }
                        }
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return mLocation!!
    }

    /**
     * Stop using GPS listener Calling this function will stop using GPS in your
     * app
     */
    fun stopUsingGPS() {
        if (locationManager != null) {
            try {
                locationManager!!.removeUpdates(this@GPSTracker)
            } catch (e: SecurityException) {
                e.printStackTrace()
            }
        }
    }

    /**
     * Function to get latitude
     */
    fun getLatitude(): Double {
        if (mLocation != null) {
            mLatitude = mLocation!!.latitude
        }

        // return latitude
        return mLatitude
    }

    /**
     * Function to get longitude
     */
    fun getLongitude(): Double {
        if (mLocation != null) {
            mLongitude = mLocation!!.longitude
        }

        // return longitude
        return mLongitude
    }

    /**
     * Function to check GPS/wifi enabled
     *
     * @return boolean
     */
    fun canGetLocation(): Boolean {
        return canGetLocation
    }

    /**
     * Function to show settings alert dialog On pressing Settings button will
     * lauch Settings Options
     */
    fun showSettingsAlert() {
        val alertDialog = AlertDialog.Builder(mContext)

        // Setting Dialog Title
        alertDialog.setTitle(getString(R.string.LocationSettings))

        // Setting Dialog Message
        alertDialog
                .setMessage(getString(R.string.LocationServices))

        // On pressing Settings button
        alertDialog.setPositiveButton(R.string.Enable
        ) { dialog, which ->
            val intent = Intent(
                    Settings.ACTION_LOCATION_SOURCE_SETTINGS)
            mContext.startActivity(intent, ActivityOptionsCompat.makeSceneTransitionAnimation((mContext as Activity)).toBundle())
        }

        // on pressing cancel button
        alertDialog.setNegativeButton(R.string.Cancel
        ) { dialog, which -> dialog.cancel() }

        // Showing Alert Message
        alertDialog.show()
    }

    override fun onLocationChanged(location: Location) {}
    override fun onProviderDisabled(provider: String) {}
    override fun onProviderEnabled(provider: String) {}
    override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {}
    override fun onBind(arg0: Intent): IBinder? {
        return null
    }

    companion object {
        // The minimum distance to change Updates in meters
        private const val MIN_DISTANCE_CHANGE_FOR_UPDATES: Long = 10 // 10 meters

        // The minimum time between updates in milliseconds
        private const val MIN_TIME_BW_UPDATES = 1000 * 60 // 1 minute
                .toLong()
    }

    init {
        getLocation()
    }
}