package com.zoptal.cellableapp.mqttchat.ViewHolders

import android.graphics.Typeface
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.mqttchat.AppController

class ViewHolderChatlist(view: View) : RecyclerView.ViewHolder(view) {
    @kotlin.jvm.JvmField
    var newMessageTime: TextView
    @kotlin.jvm.JvmField
    var newMessage: TextView
    @kotlin.jvm.JvmField
    var storeName: TextView
    @kotlin.jvm.JvmField
    var newMessageDate: TextView
    @kotlin.jvm.JvmField
    var newMessageCount: TextView
    @kotlin.jvm.JvmField
    var storeImage: ImageView
    @kotlin.jvm.JvmField
    var tick: ImageView
    @kotlin.jvm.JvmField
    var rl: RelativeLayout

    init {
        newMessageTime = view.findViewById<View>(R.id.newMessageTime) as TextView
        newMessage = view.findViewById<View>(R.id.newMessage) as TextView
        newMessageDate = view.findViewById<View>(R.id.newMessageDate) as TextView
        storeName = view.findViewById<View>(R.id.storeName) as TextView
        storeImage = view.findViewById<View>(R.id.storeImage2) as ImageView
        tick = view.findViewById<View>(R.id.tick) as ImageView
        rl = view.findViewById<View>(R.id.rl) as RelativeLayout
        newMessageCount = view.findViewById<View>(R.id.newMessageCount) as TextView
        val tf = AppController.instance?.robotoCondensedFont
        newMessageCount.setTypeface(tf, Typeface.BOLD)
        newMessageDate.setTypeface(tf, Typeface.NORMAL)
        newMessageTime.setTypeface(tf, Typeface.NORMAL)
        newMessage.setTypeface(tf, Typeface.NORMAL)
    }
}