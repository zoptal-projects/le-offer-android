package com.zoptal.cellableapp.mqttchat.Utilities

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import com.zoptal.cellableapp.R

/*
 * Created by moda on 28/10/16.
 */ /**
 * Helper class to show the progress of download when downloading the image/audio or video received in chat message
 */
class RingProgressBar @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyle: Int = 0) : View(context, attrs, defStyle) {
    private val paint: Paint
    private var mwidth = 0
    private var mheight = 0
    private var result = 0
    private var padding = 0
    /**
     * get ring color
     */
    /**
     * set ring color
     */
    var ringColor: Int
    /**
     * get ring progress color
     */
    /**
     * set ring progress color
     */
    var ringProgressColor: Int
    /**
     * get text color
     */
    /**
     * set text color
     */
    var textColor: Int
    /**
     * get text size
     */
    /**
     * set text size
     */
    var textSize: Float
    /**
     * get width of progress ring
     */
    /**
     * set width of progress ring
     */
    var ringWidth: Float
    private var max: Int
    private var progress = 0
    private val textIsShow: Boolean
    private val style: Int
    private var mOnProgressListener: OnProgressListener? = null
    private var centre = 0
    private var radius = 0

    @SuppressLint("DrawAllocation")
    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        centre = getWidth() / 2
        radius = (centre - ringWidth / 2).toInt()
        drawCircle(canvas)
        drawTextContent(canvas)
        drawProgress(canvas)
    }

    /**
     * @param canvas Canvas
     */
    private fun drawCircle(canvas: Canvas) {
        paint.color = ringColor
        paint.style = Paint.Style.STROKE
        paint.strokeWidth = ringWidth
        paint.isAntiAlias = true
        canvas.drawCircle(centre.toFloat(), centre.toFloat(), radius.toFloat(), paint)
    }

    /**
     * @param canvas Canvas
     */
    private fun drawTextContent(canvas: Canvas) {
        paint.strokeWidth = 0f
        paint.color = textColor
        paint.textSize = textSize
        paint.typeface = Typeface.DEFAULT
        val percent = (progress.toFloat() / max.toFloat() * 100).toInt()
        val textWidth = paint.measureText("$percent%")
        if (textIsShow && percent != 0 && style == STROKE) {
            canvas.drawText("$percent%", centre - textWidth / 2, centre + textSize / 2, paint)
        }
    }

    /**
     * @param canvas Canvas
     */
    private fun drawProgress(canvas: Canvas) {
        paint.strokeWidth = ringWidth
        paint.color = ringProgressColor

        //Stroke
        val strokeOval = RectF((centre - radius).toFloat(), (centre - radius).toFloat(), (centre + radius).toFloat(), (centre + radius).toFloat())
        //FIll
        val fillOval = RectF(centre - radius + ringWidth + padding, centre - radius + ringWidth + padding, centre + radius - ringWidth - padding, centre + radius - ringWidth - padding)
        when (style) {
            STROKE -> {
                paint.style = Paint.Style.STROKE
                paint.strokeCap = Paint.Cap.ROUND
                canvas.drawArc(strokeOval, -90f, 360 * progress / max.toFloat(), false, paint)
            }
            FILL -> {
                paint.style = Paint.Style.FILL_AND_STROKE
                paint.strokeCap = Paint.Cap.ROUND
                if (progress != 0) canvas.drawArc(fillOval, -90f, 360 * progress / max.toFloat(), true, paint)
            }
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        val widthMode = MeasureSpec.getMode(widthMeasureSpec)
        val widthSize = MeasureSpec.getSize(widthMeasureSpec)
        val heightMode = MeasureSpec.getMode(heightMeasureSpec)
        val heightSize = MeasureSpec.getSize(heightMeasureSpec)
        mwidth = if (widthMode == MeasureSpec.AT_MOST) result else widthSize
        mheight = if (heightMode == MeasureSpec.AT_MOST) result else heightSize
        setMeasuredDimension(mwidth, mheight)
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        mwidth = w
        mheight = h
        padding = dp2px(5)
    }

    /**
     * @return max value
     */
    @Synchronized
    fun getMax(): Int {
        return max
    }

    /**
     * @param max max value
     */
    @Synchronized
    fun setMax(max: Int) {
        require(max >= 0) { "The max progress of 0" }
        this.max = max
    }

    /**
     * @return progress value
     */
    @Synchronized
    fun getProgress(): Int {
        return progress
    }

    /**
     * @param progress progress value
     */
    @Synchronized
    fun setProgress(progress: Int) {
        var progress = progress
        require(progress >= 0) { "The progress of 0" }
        if (progress > max) {
            progress = max
        }
        if (progress <= max) {
            this.progress = progress
            postInvalidate()
        }
        if (progress == max) {
            if (mOnProgressListener != null) {
                mOnProgressListener!!.progressToComplete()
            }
        }
    }

    fun dp2px(dp: Int): Int {
        val density = context.resources.displayMetrics.density
        return (dp * density + 0.5f).toInt()
    }

    /**
     * To implement callback for progress update
     */
    interface OnProgressListener {
        fun progressToComplete()
    }

    fun setOnProgressListener(mOnProgressListener: OnProgressListener?) {
        this.mOnProgressListener = mOnProgressListener
    }

    companion object {
        const val STROKE = 0
        const val FILL = 1
    }

    init {
        paint = Paint()
        result = dp2px(100)
        val mTypedArray = context.obtainStyledAttributes(attrs, R.styleable.RingProgressBar)
        ringColor = mTypedArray.getColor(R.styleable.RingProgressBar_ringColor, Color.BLACK)
        ringProgressColor = mTypedArray.getColor(R.styleable.RingProgressBar_ringProgressColor, Color.WHITE)
        textColor = mTypedArray.getColor(R.styleable.RingProgressBar_textColor, Color.BLACK)
        textSize = mTypedArray.getDimension(R.styleable.RingProgressBar_textSize, 16f)
        ringWidth = mTypedArray.getDimension(R.styleable.RingProgressBar_ringWidth, 5f)
        max = mTypedArray.getInteger(R.styleable.RingProgressBar_max, 100)
        textIsShow = mTypedArray.getBoolean(R.styleable.RingProgressBar_textIsShow, true)
        style = mTypedArray.getInt(R.styleable.RingProgressBar_style, 0)
        mTypedArray.recycle()
    }
}