package com.zoptal.cellableapp.mqttchat.Utilities

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.zoptal.cellableapp.mqttchat.AppController.Companion.instance

/**
 * @since 13/07/17.
 */
class ShutdownReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        //Insert code here
        if (!instance!!.sharedPreferences!!.getBoolean("applicationKilled", true)) {
            instance!!.disconnect()
            instance!!.setApplicationKilled(true)
        }
    }
}