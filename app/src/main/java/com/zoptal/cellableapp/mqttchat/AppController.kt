package com.zoptal.cellableapp.mqttchat

import android.app.*
import android.app.job.JobInfo
import android.app.job.JobScheduler
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Typeface
import android.media.RingtoneManager
import android.media.ThumbnailUtils
import android.net.Uri
import android.os.*
import android.provider.MediaStore
import android.text.TextUtils
import android.util.Base64
import android.util.Log
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.app.NotificationCompat
import androidx.multidex.MultiDexApplication
import com.android.volley.AuthFailureError
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.couchbase.lite.android.AndroidContext
import com.crashlytics.android.Crashlytics
import com.google.firebase.messaging.FirebaseMessaging
import com.google.gson.Gson
import com.squareup.otto.Bus
import com.squareup.otto.ThreadEnforcer
import com.suresh.innapp_purches.InAppConstants.Purchase_item.Companion.purchaseItemList
import com.suresh.innapp_purches.InAppConstants.base64EncodedPublicKey
import com.suresh.innapp_purches.Inn_App_billing.BillingProcessor
import com.suresh.innapp_purches.Inn_App_billing.BillingProcessor.IBillingHandler
import com.suresh.innapp_purches.Inn_App_billing.SkuDetails
import com.suresh.innapp_purches.Inn_App_billing.TransactionDetails
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.Twiter_manager.TweetManger.Companion.initialization
import com.zoptal.cellableapp.mqttchat.Activities.ChatMessageScreen
import com.zoptal.cellableapp.mqttchat.AppStateChange.AppStateListener
import com.zoptal.cellableapp.mqttchat.AppStateChange.RxAppStateMonitor
import com.zoptal.cellableapp.mqttchat.Database.CouchDbController
import com.zoptal.cellableapp.mqttchat.DownloadFile.FileUploadService
import com.zoptal.cellableapp.mqttchat.DownloadFile.FileUtils
import com.zoptal.cellableapp.mqttchat.DownloadFile.ServiceGenerator
import com.zoptal.cellableapp.mqttchat.MQtt.CustomMQtt.*
import com.zoptal.cellableapp.mqttchat.MQtt.MqttAndroidClient
import com.zoptal.cellableapp.mqttchat.Service.AppKilled
import com.zoptal.cellableapp.mqttchat.Service.MQTT_constants
import com.zoptal.cellableapp.mqttchat.Service.OreoJobService
import com.zoptal.cellableapp.mqttchat.Utilities.ApiOnServer
import com.zoptal.cellableapp.mqttchat.Utilities.DeviceUuidFactory
import com.zoptal.cellableapp.mqttchat.Utilities.MqttEvents
import com.zoptal.cellableapp.mqttchat.Utilities.Utilities
import com.zoptal.cellableapp.pojo_class.chat_suggesion_message_pojo.MessageMainPojo
import com.zoptal.cellableapp.utility.*
import com.zoptal.cellableapp.utility.OkHttp3Connection.OkHttp3RequestCallback
import com.zoptal.cellableapp.utility.OkHttp3Connection.doOkHttp3Connection
import io.fabric.sdk.android.Fabric
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import org.eclipse.paho.client.mqttv3.MqttException
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.*
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL
import java.util.*

//import org.eclipse.paho.client.mqttv3.DisconnectedBufferOptions;
//import org.eclipse.paho.client.mqttv3.IMqttActionListener;
//import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
//import org.eclipse.paho.client.mqttv3.IMqttToken;
//import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
//import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
//import org.eclipse.paho.client.mqttv3.MqttException;
//import org.eclipse.paho.client.mqttv3.MqttMessage;
//import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
/**
 * <h2>AppController</h2>
 * <P>
 *
</P> *
 * @since   29/06/17.
 */
class AppController : MultiDexApplication(), Application.ActivityLifecycleCallbacks {
    var sharedPreferences: SharedPreferences? = null
        private set
    var isForeground = false
        private set
    var chatDocId: String? = null
        private set
    private var unsentMessageDocId: String? = null
    private var mqttTokenDocId: String? = null
    private var notificationDocId: String? = null
    private var userName: String? = null
    var userId: String? = null
        private set
    private var userImageUrl: String? = null
    var userIdentifier: String? = null
        private set
    var indexDocId: String? = null
        private set
    var dbController: CouchDbController? = null
        private set
    var isSignStatusChanged = false
    var signedIn = false
        private set
    var activeReceiverId = ""
    private var mRequestQueue: RequestQueue? = null
    private var colors: ArrayList<String>? = null
    private var mqttAndroidClient: MqttAndroidClient? = null
    private var mqttConnectOptions: MqttConnectOptions? = null
    private var flag = true
    private var tokenMapping = ArrayList<HashMap<String, Any?>>()
    private val set = HashSet<IMqttDeliveryToken?>()
    private var applicationKilled = false
    var robotoCondensedFont: Typeface? = null
        private set
    var robotoRegularFont: Typeface? = null
        private set
    var robotoMediumFont: Typeface? = null
        private set
    var timeDelta: Long = 0
        private set
    private var activeSecretId = ""
    var deviceId: String? = null
        private set
    private var chatSynced = true
    private var notifications = ArrayList<HashMap<String, Any>>()
    var apiToken: String? = null
        private set
    private var mSessionManager: SessionManager? = null
    private var serviceAlreadyScheduled = false
    override fun onCreate() {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        super.onCreate()
        mSessionManager = SessionManager(this)
        Fabric.with(this, Crashlytics())
        initialization(this, VariableConstants.TWITTER_KEY, VariableConstants.TWITTER_SECRET)
        GoogleAdMob(applicationContext)
        instance = this
        val appStateMonitor = RxAppStateMonitor.create(this)
        deviceId = DeviceUuidFactory.instance.getDeviceUuid(this)
        appStateMonitor.addListener(object : AppStateListener {
            override fun onAppDidEnterForeground() {
                updatePresence(1, false)
                isForeground = true
            }

            override fun onAppDidEnterBackground() {
                updatePresence(0, false)
                isForeground = false
                updateTokenMapping()
            }
        })
        appStateMonitor.start()
        setBackgroundColorArray()

        /* final Intent changeStatus = new Intent(mInstance, AppKilled.class);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            changeStatus.setAction(VariableConstants.ACTION_START_FOURGROUND);
            startForegroundService(changeStatus);
        }
        else {
            startService(changeStatus);
        }*/sharedPreferences = getSharedPreferences("yeloPreferefr", Context.MODE_PRIVATE)
        applicationKilled = sharedPreferences!!.getBoolean("applicationKilled", false)
        indexDocId = sharedPreferences?.getString("indexDoc", null)
        dbController = CouchDbController(AndroidContext(this))
        if (indexDocId == null) {
            indexDocId = dbController!!.createIndexDocument()
            sharedPreferences!!.edit().putString("indexDoc", indexDocId).apply()
        }
        chatSynced = sharedPreferences?.getBoolean("chatSynced", false)!!
        registerActivityLifecycleCallbacks(instance)
        robotoCondensedFont = Typeface.createFromAsset(instance!!.assets, "fonts/Roboto-Condensed.ttf")
        robotoRegularFont = Typeface.createFromAsset(assets, "fonts/RobotoRegular.ttf")
        robotoMediumFont = Typeface.createFromAsset(assets, "fonts/roboto-medium.ttf")
        if (sharedPreferences!!.getBoolean("deltaRequired", true)) {
            currentTime
        } else {
            timeDelta = sharedPreferences!!.getLong("timeDelta", 0)
        }
        if (indexDocId != null) {
            val signInDetails = dbController!!.isSignedIn(indexDocId)
            signedIn = signInDetails["isSignedIn"] as Boolean
            if (signedIn) {
                userId = signInDetails["signedUserId"] as String?
                getUserDocIdsFromDb(userId)
                createMQttConnection(userId, true)
            } else {
                if (signInDetails.containsKey("notReadyYet")) {
                    Handler().postDelayed({
                        userId = signInDetails["signedUserId"] as String?
                        getUserDocIdsFromDb(userId)
                        createMQttConnection(userId, true)
                    }, 2000)
                }
            }
        }
        collectIN_AppDetails(null)
        if (mSessionManager!!.isUserLoggedIn) chatSuggetionMessageApi
        FirebaseMessaging.getInstance().subscribeToTopic("/topics/" + "sendToAll")
    }

    var bp: BillingProcessor? = null
    var inappItemList: List<SkuDetails?>? = null
        private set

    fun collectIN_AppDetails(callBack: SukoCallBack?) {
        bp = BillingProcessor(this, base64EncodedPublicKey, object : IBillingHandler {
            override fun onProductPurchased(productId: String?, details: TransactionDetails?) {}
            override fun onPurchaseHistoryRestored() {}
            override fun onBillingError(errorCode: Int, error: Throwable?) {
                callBack?.onError(error!!.message)
            }

            override fun onBillingInitialized() {
                inappItemList = bp!!.getPurchaseListingDetails(purchaseItemList)
                Log.d("dsas1", "" + inappItemList)
                callBack?.onSucess(inappItemList)
            }
        })
    }

    /**
     * initialization of the MQtt call back. */
    private fun initMqttListener(): MqttCallback {
//        MqttCallbackExtended mlistener=new MqttCallbackExtended() {
        return object : MqttCallback {
            //            @Override
            //            public void connectComplete(boolean reconnect, String serverURI)
            //            {
            //                handelConnection();
            //                updatePresence(1,false);
            //            }
            override fun connectionLost(cause: Throwable?) {
                handelconnectionLost(cause!!)
            }



            @Throws(Exception::class)
            override fun messageArrived(topic: String?, message: MqttMessage?) {
                handdleArrivedmessage(topic!!, message!!)
                Log.e("========","====messa"+message)
            }

            override fun deliveryComplete(token: IMqttDeliveryToken) {
                handledeliveryComplete(token)
            }
        }
    }

    /*
     *Collect all document details of the logged in user. */
    fun getUserDocIdsFromDb(userId: String?) {
        val userDocId = dbController!!.getUserInformationDocumentId(indexDocId, userId)
        val docIds = dbController!!.getUserDocIds(userDocId)
        if (docIds != null) {
            chatDocId = docIds[0]
            unsentMessageDocId = docIds[1]
            mqttTokenDocId = docIds[2]
            notificationDocId = docIds[3]
        }
        getUserInfoFromDb(userDocId)
        tokenMapping = dbController!!.fetchMqttTokenMapping(mqttTokenDocId)
    }

    fun getUserInfoFromDb(docId: String?) {
        val userInfo = dbController!!.getUserInfo(docId)
        apiToken = userInfo["apiToken"] as String?
        userName = userInfo["userName"] as String?
        userIdentifier = userInfo["userIdentifier"] as String?
        userId = userInfo["userId"] as String?
        userImageUrl = userInfo["userImageUrl"] as String?
        Log.d("log71", "$apiToken $userName $userIdentifier $userId $userImageUrl")
        notifications = dbController!!.fetchAllNotifications(notificationDocId)!!
    }

    /*
     *Handling the connection the app */
    private fun handelConnection() {
        Log.d("log47", "Connected")
        try {
            val obj = JSONObject()
            obj.put("eventName", MqttEvents.Connect.value)
            bus.post(obj)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            if (!chatSynced) {
                val obj = JSONObject()
                obj.put("eventName", "RefreshChats")
                bus.post(obj)
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        if (!applicationKilled) {
            updatePresence(1, false)
        } else {
            updatePresence(0, true)
        }
        /*
                 * To stop the internet checking service
                 */if (flag) {
            flag = false
            Log.d("log77", "subscribe topic$userId")
            subscribeToTopic(MqttEvents.Message.value + "/" + userId, 1)
            subscribeToTopic(MqttEvents.OfferMessage.value + "/" + userId, 1)
            subscribeToTopic(MqttEvents.Acknowledgement.value + "/" + userId, 2)
            subscribeToTopic(MqttEvents.FetchMessages.value + "/" + userId, 1)
            subscribeToTopic(MqttEvents.FetchChats.value + "/" + userId, 1)
            subscribeToTopic(MqttEvents.UpdateProduct.value + "/" + userId, 1)
            subscribeToTopic(MqttEvents.UserUpdates.value + "/" + userId, 2)
            subscribeToTopic(MqttEvents.Typing.value + "/" + userId, 0)
        }
        resendUnsentMessages()
    }

    /*
    * handdling the conecctuon lost*/
    private fun handelconnectionLost(cause: Throwable) {
        //Log.d("log57",""+cause.getMessage());
        try {
            val obj = JSONObject()
            obj.put("eventName", MqttEvents.Disconnect.value)
            bus.post(obj)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /*
     *handel the message received. */
    @Throws(Exception::class)
    private fun handdleArrivedmessage(topic: String, message: MqttMessage) {
        val obj = convertMessageToJsonObject(message)
        Log.e("log56", "$topic $obj")
        if (topic == MqttEvents.Acknowledgement.value + "/" + userId) {
            /*
                     * For an acknowledgement message received
                     */
            val sender = obj.getString("from")
            val document_id_DoubleTick = obj.getString("doc_id")
            val arr_temp = obj.getJSONArray("msgIds")
            val id = arr_temp.getString(0)
            /*
                     * For callback in to activity to update UI
                     */try {
                obj.put("msgId", id)
                obj.put("eventName", topic)
                bus.post(obj)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            if (obj.getString("status") == "2") {
                /*
 * message delivered
 */
                if (!dbController!!.updateMessageStatus(document_id_DoubleTick, id, 1)) {
                    dbController!!.updateMessageStatus(dbController!!.getDocumentIdOfReceiverChatlistScreen(instance!!.chatDocId, sender, obj.getString("secretId")), id, 1)
                }
            } else {
                /*
                         * message read
                         */
                if (!dbController!!.drawBlueTickUptoThisMessage(document_id_DoubleTick, id)) {
                    dbController!!.drawBlueTickUptoThisMessage(dbController!!.getDocumentIdOfReceiverChatlistScreen(instance!!.chatDocId, sender, obj.getString("secretId")), id)
                }
            }
        } else if (topic == MqttEvents.OfferMessage.value + "/" + userId || topic == MqttEvents.Message.value + "/" + userId) {
            /*
                     * For an actual message(Like text,image,video etc.) received
                     */
            var isself = false
            var receiverUid = obj.getString("from")

//            ignore the swap offer msg sent from server, if request by me..
            if (receiverUid == userId && obj.getString("type") == "17" && obj.getString("swapType") == "1") {
                return
            }
            if (userId != null && userId == receiverUid) {
                isself = true
                receiverUid = obj.getString("to")
            }
            var receiverIdentifier: String? = ""
            if (obj.has("receiverIdentifier")) {
                receiverIdentifier = obj.getString("receiverIdentifier")
            }
            val messageType = obj.getString("type")
            var actualMessage = obj.getString("payload").trim { it <= ' ' }
            val timestamp = obj.getString("timestamp").toString()
            val id = obj.getString("id")
            val docIdForDoubleTickAck = obj.getString("toDocId")
            var offerType = "1"
            if (obj.has("offerType")) {
                offerType = obj.getString("offerType")
            }
            var productImage: String? = ""
            var productName: String? = ""
            var productPrice: String? = ""
            val isAccepted: Boolean
            if (obj.has("productImage")) {
                productImage = obj.getString("productImage")
            }
            if (obj.has("productName")) {
                productName = obj.getString("productName")
            }
            if (obj.has("productPrice")) {
                productPrice = obj.getString("productPrice")
            }
            var productAcceptedPrice = ""
            if (offerType == "2") {
                isAccepted = true
                val data = Base64.decode(actualMessage, Base64.DEFAULT)
                try {
                    productAcceptedPrice = String(data, charset("UTF-8"))
                } catch (e: UnsupportedEncodingException) {
                    e.printStackTrace()
                }
            } else {
                isAccepted = false
            }
            var dataSize = -1
            if (messageType == "1" || messageType == "2" || messageType == "5" || messageType == "7") {
                dataSize = obj.getInt("dataSize")
            }
            var swapType = "1"
            var productUrl = ""
            var swapProductUrl = ""
            var swapProductId = ""
            var isRejected = ""
            if (messageType == "17") {
                swapType = obj.getString("swapType")
                if (swapType == "1") actualMessage = obj.getString("swapTextMessage")
                productUrl = obj.getString("productUrl")
                swapProductUrl = obj.getString("swapProductUrl")
                swapProductId = obj.getString("swapProductId")
                isRejected = obj.getString("isRejected")
            }
            val secretId = obj.getString("secretId")
            val receiverName: String
            val userImage: String
            receiverName = obj.getString("name")
            userImage = obj.getString("userImage")
            var documentId = instance!!.findDocumentIdOfReceiver(receiverUid, secretId)
            if (documentId.isEmpty()) {
                /*
                         *If chat document is not exist then creating the prodcut details */
                documentId = findDocumentIdOfReceiver(receiverUid, Utilities.tsInGmt(), receiverName, userImage, secretId, true, receiverIdentifier, "", productImage, productName, productPrice, isAccepted, false)
            } else {
                if (isAccepted) {
                    dbController!!.updateProductAccepted(documentId, true, productAcceptedPrice)
                }
                if (!isself) {
                    dbController!!.updateChatDetails(documentId, receiverName, userImage)
                }
            }
            dbController!!.setDocumentIdOfReceiver(documentId, docIdForDoubleTickAck, receiverUid)
            /*
                     * For callback in to activity to update UI
                     */if (!dbController!!.checkAlreadyExists(documentId, id)) {
                if (messageType == "1" || messageType == "2" || messageType == "7") {
                    instance!!.putMessageInDb(receiverUid, messageType, offerType,
                            actualMessage, timestamp, id, documentId, obj.getString("thumbnail").trim { it <= ' ' },
                            dataSize, receiverName, isself, -1, swapType, productUrl, swapProductUrl, swapProductId, isRejected)
                } else {
                    instance!!.putMessageInDb(receiverUid,
                            messageType, offerType, actualMessage, timestamp, id, documentId, null, dataSize, receiverName, isself, -1, swapType, productUrl, swapProductUrl, swapProductId, isRejected)
                }
                val obj2 = JSONObject()
                obj2.put("from", instance!!.userId)
                obj2.put("msgIds", JSONArray(Arrays.asList(*arrayOf(id))))
                obj2.put("doc_id", docIdForDoubleTickAck)
                obj2.put("to", receiverUid)
                obj2.put("status", "2")
                obj2.put("secretId", secretId)
                instance!!.publish(MqttEvents.Acknowledgement.value + "/" + receiverUid, obj2, 2, false)
                /*
                     * For callback in to activity to update UI
                     */try {
                    obj.put("eventName", topic)
                    bus.post(obj)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                val intent = Intent(instance, ChatMessageScreen::class.java)
                intent.putExtra("isNew", false)
                intent.putExtra("secretId", secretId)
                intent.putExtra("receiverUid", receiverUid)
                intent.putExtra("receiverName", receiverName)
                intent.putExtra("receiverIdentifier", receiverIdentifier)
                intent.putExtra("documentId", documentId)
                intent.putExtra("colorCode", instance!!.getColorCode(5))
                intent.putExtra("receiverImage", userImage)
                intent.putExtra("fromNotification", true)
                /*
                         *To generate the push notification locally
                         */if (!chatSynced && !isself) {
                    generatePushNotificationLocal(documentId, messageType, offerType, receiverName, actualMessage, intent, secretId, receiverUid)
                }
                if (!actualMessage.trim { it <= ' ' }.isEmpty()) {
                    dbController!!.updateSecretInviteImageVisibility(documentId, false)
                }
            }
        } else if (topic.substring(0, 3) == "Onl") {
            /*
                     * To check for the online status
                     */
            try {
                obj.put("eventName", topic)
                bus.post(obj)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        } else if (topic == MqttEvents.Typing.value + "/" + userId) {
            try {
                obj.put("eventName", topic)
                bus.post(obj)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        } else if (topic == MqttEvents.FetchChats.value + "/" + userId) {
            try {
                val chats = obj.getJSONArray("chats")
                var tsInGmt: String?
                var receiverName: String
                var jsonObject: JSONObject
                var wasInvited: Boolean
                var totalUnreadMessageCount: Int
                var hasNewMessage: Boolean
                var isSelf: Boolean
                var profilePic: String?
                for (j in 0 until chats.length()) {
                    jsonObject = chats.getJSONObject(j)
                    isSelf = jsonObject.getString("senderId") == userId
                    totalUnreadMessageCount = jsonObject.getInt("totalUnread")
                    hasNewMessage = totalUnreadMessageCount > 0
                    tsInGmt = Utilities.epochtoGmt(jsonObject.getLong("timestamp").toString())
                    receiverName = jsonObject.getString("userName")
                    var productImage: String? = ""
                    var productName: String? = ""
                    var productPrice: String? = ""
                    var userIdentifier: String? = ""
                    var isSold = false
                    if (obj.has("productImage")) {
                        productImage = obj.getString("productImage")
                    }
                    if (obj.has("productName")) {
                        productName = obj.getString("productName")
                    }
                    if (obj.has("productPrice")) {
                        productPrice = obj.getString("productPrice")
                    }
                    profilePic = if (jsonObject.has("profilePic")) {
                        jsonObject.getString("profilePic")
                    } else {
                        ""
                    }
                    if (jsonObject.has("productSold")) {
                        isSold = jsonObject.getBoolean("productSold")
                    }
                    if (jsonObject.has("userIdentifier")) {
                        userIdentifier = jsonObject.getString("userIdentifier")
                    }
                    var documentId = instance!!.findDocumentIdOfReceiver(jsonObject.getString("recipientId"), jsonObject.getString("secretId"))
                    wasInvited = !jsonObject.getBoolean("initiated")
                    if (documentId.isEmpty()) {
                        documentId = findDocumentIdOfReceiver(jsonObject.getString("recipientId"),
                                tsInGmt, receiverName, profilePic, jsonObject.getString("secretId"),
                                wasInvited, userIdentifier, jsonObject.getString("chatId"), productImage, productName, productPrice, false, isSold)
                    } else {
                        if (isSold) {
                            dbController!!.updateSoldDetails(documentId, true)
                        }
                        dbController!!.updateChatDetails(documentId, jsonObject.getString("chatId"), wasInvited, profilePic)
                    }
                    when (jsonObject.getString("messageType").toInt()) {
                        15 -> {
                            var text_data = ""
                            try {
                                text_data = String(Base64.decode(jsonObject.getString("payload"), Base64.DEFAULT),charset("UTF-8"))
                            } catch (e: UnsupportedEncodingException) {
                                e.printStackTrace()
                            }
                            dbController!!.updateChatListForNewMessageFromHistory(
                                    documentId, text_data, hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"))
                        }
                        16 -> {
                            var paypal_data = ""
                            try {
                                paypal_data = String(Base64.decode(jsonObject.getString("payload"), Base64.DEFAULT),charset("UTF-8"))
                            } catch (e: UnsupportedEncodingException) {
                                e.printStackTrace()
                            }
                            dbController!!.updateChatListForNewMessageFromHistory(documentId, paypal_data, hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"))
                        }
                        17 -> {
                            var data = ""
                            try {
                                data = String(Base64.decode(jsonObject.getString("payload"), Base64.DEFAULT),charset("UTF-8"))
                            } catch (e: UnsupportedEncodingException) {
                                e.printStackTrace()
                            }
                            dbController!!.updateChatListForNewMessageFromHistory(
                                    documentId, data, hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"))
                        }
                        0 -> {
                            var text = ""
                            try {
                                text = String(Base64.decode(jsonObject.getString("payload"), Base64.DEFAULT),charset("UTF-8"))
                            } catch (e: UnsupportedEncodingException) {
                                e.printStackTrace()
                            }
                            if (text.trim { it <= ' ' }.isEmpty()) {
                                text = if (jsonObject.getString("senderId") == userId) {
                                    resources.getString(R.string.YouInvited) + " " + receiverName + " " +
                                            resources.getString(R.string.JoinSecretChat)
                                } else {
                                    resources.getString(R.string.youAreInvited) + " " + receiverName + " " +
                                            resources.getString(R.string.JoinSecretChat)
                                }
                            }
                            dbController!!.updateChatListForNewMessageFromHistory(
                                    documentId, text, hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"))
                        }
                        1 -> /*
 * receiverImage message
 */if (totalUnreadMessageCount > 0) {
                            dbController!!.updateChatListForNewMessageFromHistory(documentId, getString(R.string.NewImage), hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"))
                        } else {
                            dbController!!.updateChatListForNewMessageFromHistory(documentId, getString(R.string.Image), hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"))
                        }
                        2 -> /*
 * Video message
 */if (totalUnreadMessageCount > 0) {
                            dbController!!.updateChatListForNewMessageFromHistory(documentId, getString(R.string.NewVideo),
                                    hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"))
                        } else {
                            dbController!!.updateChatListForNewMessageFromHistory(documentId,
                                    getString(R.string.Video),
                                    hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"))
                        }
                        3 -> /*
 * Location message
 */if (totalUnreadMessageCount > 0) {
                            dbController!!.updateChatListForNewMessageFromHistory(documentId, getString(R.string.NewLocation), hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"))
                        } else {
                            dbController!!.updateChatListForNewMessageFromHistory(documentId, getString(R.string.Location), hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"))
                        }
                        4 -> /*
 * Contact message
 */if (totalUnreadMessageCount > 0) {
                            dbController!!.updateChatListForNewMessageFromHistory(documentId, getString(R.string.NewContact), hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"))
                        } else {
                            dbController!!.updateChatListForNewMessageFromHistory(documentId, getString(R.string.Contact),
                                    hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"))
                        }
                        5 ->                                     /*
                                     * Audio message*/if (totalUnreadMessageCount > 0) {
                            dbController!!.updateChatListForNewMessageFromHistory(documentId, getString(R.string.NewAudio), hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"))
                        } else {
                            dbController!!.updateChatListForNewMessageFromHistory(documentId,
                                    getString(R.string.Audio), hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"))
                        }
                        6 ->                                     /*
             * Sticker*/if (totalUnreadMessageCount > 0) {
                            dbController!!.updateChatListForNewMessageFromHistory(documentId, getString(R.string.NewSticker), hasNewMessage, tsInGmt,
                                    tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"))
                        } else {
                            dbController!!.updateChatListForNewMessageFromHistory(documentId, getString(R.string.Stickers), hasNewMessage, tsInGmt,
                                    tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"))
                        }
                        7 -> /*
 * Doodle
 */if (totalUnreadMessageCount > 0) {
                            dbController!!.updateChatListForNewMessageFromHistory(documentId, getString(R.string.NewDoodle), hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"))
                        } else {
                            dbController!!.updateChatListForNewMessageFromHistory(documentId, getString(R.string.Doodle), hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"))
                        }
                        8 -> /*
 * Gif
 */if (totalUnreadMessageCount > 0) {
                            dbController!!.updateChatListForNewMessageFromHistory(documentId, getString(R.string.NewGiphy), hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"))
                        } else {
                            dbController!!.updateChatListForNewMessageFromHistory(documentId, getString(R.string.Giphy), hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"))
                        }
                    }
                }
                obj.put("eventName", topic)
                bus.post(obj)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        } else if (topic == MqttEvents.FetchMessages.value + "/" + userId) {
            try {
                val secretId = obj.getString("secretId")
                val messages = obj.getJSONArray("messages")
                if (messages.length() > 0) {
                    var messageObject = messages.getJSONObject(0)
                    var receiverUid = messageObject.getString("senderId")
                    if (userId != null && userId == receiverUid) {
                        receiverUid = messageObject.getString("receiverId")
                    }
                    val chatDetails = instance!!.dbController!!.getAllChatDetails(chatDocId)
                    var documentId = ""
                    if (chatDetails != null) {
                        val receiverUidArray = chatDetails["receiverUidArray"] as ArrayList<String>?
                        val receiverDocIdArray = chatDetails["receiverDocIdArray"] as ArrayList<String>?
                        val secretIdArray = chatDetails["secretIdArray"] as ArrayList<String>?
                        for (i in receiverUidArray!!.indices) {
                            if (receiverUidArray[i] == receiverUid && secretIdArray!![i] == secretId) {
                                documentId = receiverDocIdArray!![i]
                                break
                            }
                        }
                    }
                    Log.d("log17", "" + documentId)
                    if (documentId == null || documentId.isEmpty()) {
                        return
                    }
                    /*
                             *Putting msg in DB.*/
                    var isSelf: Boolean
                    for (i in 0 until messages.length()) {
                        messageObject = messages.getJSONObject(i)
                        isSelf = messageObject.getString("senderId") == userId
                        val messageType = messageObject.getString("messageType")
                        var actualMessage = messageObject.getString("payload").trim { it <= ' ' }
                        val timestamp = messageObject.getLong("timestamp").toString()
                        val id = messageObject.getString("messageId")
                        val docIdForDoubleTickAck = messageObject.getString("toDocId")
                        var dataSize = -1
                        if (messageType == "1" || messageType == "2" || messageType == "5" || messageType == "7") {
                            dataSize = messageObject.getInt("dataSize")
                        }
                        val receiverName = messageObject.getString("name")
                        val userImage = messageObject.getString("userImage")
                        var swapType = "1"
                        var productUrl = ""
                        var swapProductUrl = ""
                        var swapProductId = ""
                        var isRejected = ""
                        if (messageType == "17") {
                            swapType = messageObject.getString("swapType")
                            if (swapType == "1") actualMessage = messageObject.getString("swapTextMessage")
                            productUrl = messageObject.getString("productUrl")
                            swapProductUrl = messageObject.getString("swapProductUrl")
                            swapProductId = messageObject.getString("swapProductId")
                            isRejected = messageObject.getString("isRejected")
                        }
                        var offerType = "1"
                        if (messageObject.has("offerType")) {
                            offerType = messageObject.getString("offerType")
                        }
                        var isAccepted: Boolean
                        var productSoldPrice = ""
                        if (offerType == "2") {
                            isAccepted = true
                            val data = Base64.decode(actualMessage, Base64.DEFAULT)
                            try {
                                productSoldPrice = String(data,charset("UTF-8"))
                            } catch (e: UnsupportedEncodingException) {
                                e.printStackTrace()
                            }
                        } else {
                            isAccepted = false
                        }
                        if (isAccepted) {
                            dbController!!.updateProductAccepted(documentId, true, productSoldPrice)
                        }
                        if (!isSelf) {
                            dbController!!.updateChatDetails(documentId, receiverName, userImage)
                        }
                        dbController!!.setDocumentIdOfReceiver(documentId, docIdForDoubleTickAck, receiverUid)
                        if (!dbController!!.checkAlreadyExists(documentId, id)) {
                            if (messageType == "1" || messageType == "2" || messageType == "7") {
                                instance!!.putMessageInDb(receiverUid, messageType, offerType,
                                        actualMessage, timestamp, id, documentId,
                                        messageObject.getString("thumbnail").trim { it <= ' ' },
                                        dataSize, receiverName, isSelf, messageObject.getInt("status"), swapType, productUrl, swapProductUrl, swapProductId, isRejected
                                )
                            } else {
                                instance!!.putMessageInDb(receiverUid,
                                        messageType, offerType, actualMessage, timestamp, id, documentId,
                                        null, dataSize, receiverName, isSelf,
                                        messageObject.getInt("status"), swapType, productUrl, swapProductUrl, swapProductId, isRejected)
                            }
                            if (messageObject.getInt("status") == 1 && isSelf && (messageType != "0" || !actualMessage.isEmpty())) {
                                val obj2 = JSONObject()
                                obj2.put("from", instance!!.userId)
                                obj2.put("msgIds", JSONArray(Arrays.asList(*arrayOf(id))))
                                obj2.put("doc_id", docIdForDoubleTickAck)
                                obj2.put("to", receiverUid)
                                obj2.put("status", "2")
                                obj2.put("secretId", secretId)
                                instance!!.publish(MqttEvents.Acknowledgement.value + "/" + receiverUid, obj2, 2, false)
                            }
                        }
                        if (!actualMessage.trim { it <= ' ' }.isEmpty()) {
                            dbController!!.updateSecretInviteImageVisibility(documentId, false)
                        }
                    }
                }
                obj.put("eventName", topic)
                bus.post(obj)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        } else if (topic == MqttEvents.UpdateProduct.value + "/" + userId) {
            try {
                val id = obj.getString("id")
                if (obj.has("sold")) {
                    dbController!!.updateAllProductSold(id, obj.getBoolean("sold"))
                } else {
                    val productName = obj.getString("name")
                    val productImage = obj.getString("image")
                    val negotiable = obj.getString("negotiable")
                    dbController!!.updateAllProduct(id, productImage, productName, "", negotiable, "")
                }
                obj.put("eventName", topic)
                bus.post(obj)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        } /*else if(topic.equals(MqttEvents.UserUpdates+"/"+userId))
                {}*/
    }

    /*
    * Handeling the token delivery*/
    fun handledeliveryComplete(token: IMqttDeliveryToken) {
        if (set.contains(token)) {
            var id: String? = null
            var docId: String? = null
            val size = tokenMapping.size
            var map: HashMap<String, Any?>
            for (i in 0 until size) {
                map = tokenMapping[i]
                if (map["MQttToken"] == token) {
                    id = map["messageId"] as String?
                    docId = map["docId"] as String?
                    tokenMapping.removeAt(i)
                    set.remove(token)
                    break
                }
            }
            if (docId != null && id != null) {
                try {
                    val obj = JSONObject()
                    obj.put("messageId", id)
                    obj.put("docId", docId)
                    obj.put("eventName", MqttEvents.MessageResponse.value)
                    bus.post(obj)
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
                dbController!!.updateMessageStatus(docId, id, 0)
                dbController!!.removeUnsentMessage(instance!!.unsentMessageDocId, id)
            }
        }
    }

    /**
     * Prepare image or audio or video file for upload
     */
    fun convertByteArrayToFileToUpload(data: ByteArray?, name: String?, extension: String): File? {
        var file: File? = null
        try {
            val folder = File(Environment.getExternalStorageDirectory().path + ApiOnServer.CHAT_UPLOAD_THUMBNAILS_FOLDER)
            if (!folder.exists() && !folder.isDirectory) {
                folder.mkdirs()
            }
            file = File(Environment.getExternalStorageDirectory().path + ApiOnServer.CHAT_UPLOAD_THUMBNAILS_FOLDER, name + extension)
            if (!file.exists()) {
                file.createNewFile()
            }
            val fos = FileOutputStream(file)
            fos.write(data)
            fos.flush()
            fos.close()
        } catch (e: IOException) {
        }
        return file
    }

    /**
     * To prepare image file for upload
     */
    private fun calculateInSampleSize(
            options: BitmapFactory.Options, reqWidth: Int, reqHeight: Int): Int {
        val height = options.outHeight
        val width = options.outWidth
        var inSampleSize = 1
        if (height > reqHeight || width > reqWidth) {
            val halfHeight = height / 2
            val halfWidth = width / 2
            while (halfHeight / inSampleSize > reqHeight
                    && halfWidth / inSampleSize > reqWidth) {
                inSampleSize *= 2
            }
        }
        return inSampleSize
    }

    private fun decodeSampledBitmapFromResource(pathName: String?,
                                                reqWidth: Int, reqHeight: Int): Bitmap {
        val options = BitmapFactory.Options()
        options.inJustDecodeBounds = true
        BitmapFactory.decodeFile(pathName, options)
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight)
        options.inJustDecodeBounds = false
        return BitmapFactory.decodeFile(pathName, options)
    }

    /**
     * To upload image or video or audio to server using multipart upload to avoid OOM exception
     */
    private fun uploadFile(fileUri: Uri, name: String, messageType: Int,
                           obj: JSONObject, receiverUid: String?, id: String?,
                           mapTemp: HashMap<String, Any?>, secretId: String?) {
        val service = ServiceGenerator.createService(FileUploadService::class.java)
        val file = FileUtils.getFile(this, fileUri)
        var url: String? = null
        if (messageType == 1) {
            url = "$name.jpg"
        } else if (messageType == 2) {
            url = "$name.mp4"
        } else if (messageType == 5) {
            url = "$name.mp3"
        } else if (messageType == 7) {
            url = "$name.jpg"
        }
        val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file)
        val body = MultipartBody.Part.createFormData("photo", url, requestFile)
        val descriptionString = "Yelo File Uploading"
        val description = RequestBody.create(MediaType.parse("multipart/form-data"), descriptionString)

        // finally, execute the request
        val call = service.upload(description, body, ApiOnServer.AUTH_KEY)
        call.enqueue(object : Callback<ResponseBody?> {
            override fun onResponse(call: Call<ResponseBody?>, response: Response<ResponseBody?>) {
                /**
                 * has to get url from the server in respons.
                 */
                try {
                    if (response.code() == 200) {
                        var url: String? = null
                        if (messageType == 1) {
                            url = "$name.jpg"
                        } else if (messageType == 2) {
                            url = "$name.mp4"
                        } else if (messageType == 5) {
                            url = "$name.mp3"
                        } else if (messageType == 7) {
                            url = "$name.jpg"
                        }
                        obj.put("payload", Base64.encodeToString((ApiOnServer.CHAT_UPLOAD_PATH + url).toByteArray(charset("UTF-8")), Base64.DEFAULT))
                        obj.put("dataSize", file.length())
                        obj.put("timestamp", Utilities().gmtToEpoch(Utilities.tsInGmt()))
                        val fdelete = File(fileUri.path)
                        if (fdelete.exists()) fdelete.delete()
                    }
                } catch (e: JSONException) {
                } catch (e: IOException) {
                }
                /**
                 *
                 *
                 * emitting to the server the values after the file has been uploaded
                 *
                 */
                val tsInGmt = Utilities.tsInGmt()
                val docId = instance!!.findDocumentIdOfReceiver(receiverUid, secretId)
                dbController!!.updateMessageTs(docId, id, tsInGmt)
                instance!!.publishChatMessage(MqttEvents.Message.value + "/" + receiverUid, obj, 1, false, mapTemp)
            }

            override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {}
        })
    }

    /**
     * Update change in signin status
     * If signed in then have to connect socket,start listening on various socket events and disconnect socket in case of signout, stop listening on various socket events
     */
    fun setSignedIn(signedIn: Boolean, userId: String?, userName: String?, userIdentifier: String?) {
        this.signedIn = signedIn
        if (signedIn) {
            /*
             *Disonnecting the previous user details. */
            disconnect()
            /*
             *making it true for the to add new subscriber */flag = true
            /*
             *For new sign up or login sync the chat again. */chatSynced = false
            instance!!.userId = userId
            instance!!.userIdentifier = userIdentifier
            instance!!.userName = userName
            getUserDocIdsFromDb(userId)
            createMQttConnection(userId, true)
            Handler().postDelayed({
                if (sharedPreferences!!.getString("chatNotificationArray", null) == null) {
                    val prefsEditor = sharedPreferences!!.edit()
                    prefsEditor.putString("chatNotificationArray", Gson().toJson(ArrayList<Map<String, String>>()))
                    prefsEditor.apply()
                }
            }, 1000)
        } else {
            flag = true
            instance!!.userId = null
        }
    }

    fun setActiveSecretId(secretId: String) {
        activeSecretId = secretId
    }

    fun getChatSynced(): Boolean {
        return chatSynced
    }

    fun setChatSynced(synced: Boolean) {
        chatSynced = synced
        sharedPreferences!!.edit().putBoolean("chatSynced", synced).apply()
    }

    fun setApplicationKilled(applicationKilled: Boolean) {
        sharedPreferences!!.edit().putBoolean("applicationKilled", applicationKilled).apply()
        this.applicationKilled = applicationKilled
        if (applicationKilled) {
            sharedPreferences!!.edit().putString("lastSeenTime", Utilities.tsInGmt()).apply()
        }
    }

    fun getUserName(): String? {
        return userName
    }

    fun getUserImageUrl(): String? {
        return userImageUrl
    }

    fun setUserImageUrl(userImageUrl: String?) {
        this.userImageUrl = userImageUrl
        dbController!!.updateUserImageUrl(dbController!!.getUserDocId(userId, indexDocId), userImageUrl)
    }

    fun setUserName(userName: String?) {
        this.userName = userName
        dbController!!.updateUserName(dbController!!.getUserDocId(userId, indexDocId), userName)
    }

    fun getunsentMessageDocId(): String? {
        return unsentMessageDocId
    }

    /**
     * To search if there exists any document containing chat for a particular receiver and if founde returns its document
     * id else return empty string
     */
    fun findDocumentIdOfReceiver(ReceiverUid: String?, secretId: String?): String {
        Log.d("Details", "$ReceiverUid $secretId")
        val docId = ""
        val chatDetails = dbController!!.getAllChatDetails(instance!!.chatDocId)
        if (chatDetails != null) {
            val receiverUidArray = chatDetails["receiverUidArray"] as ArrayList<String>?
            val receiverDocIdArray = chatDetails["receiverDocIdArray"] as ArrayList<String>?
            val secretIdArray = chatDetails["secretIdArray"] as ArrayList<String>?
            for (i in receiverUidArray!!.indices) {
                if (receiverUidArray[i] == ReceiverUid) {
                    if (secretId!!.isEmpty()) {
                        if (secretIdArray!![i].isEmpty()) {
                            return receiverDocIdArray!![i]
                        }
                    } else {
                        if (secretIdArray!![i] == secretId) {
                            return receiverDocIdArray!![i]
                        }
                    }
                }
            }
        }
        return docId
    }

    val requestQueue: RequestQueue?
        get() {
            if (mRequestQueue == null) {
                mRequestQueue = Volley.newRequestQueue(applicationContext)
            }
            return mRequestQueue
        }

    fun <T> addToRequestQueue(req: Request<T>, tag: String?) {
        req.tag = if (TextUtils.isEmpty(tag)) TAG else tag
        requestQueue!!.add(req)
    }

    fun <T> addToRequestQueue(req: Request<T>) {
        req.tag = TAG
        requestQueue!!.add(req)
    }

    fun cancelPendingRequests(tag: Any?) {
        if (mRequestQueue != null) {
            mRequestQueue!!.cancelAll(tag)
        }
    }

    private fun setBackgroundColorArray() {
        colors = ArrayList()
        colors!!.add("#FFCDD2")
        colors!!.add("#D1C4E9")
        colors!!.add("#B3E5FC")
        colors!!.add("#C8E6C9")
        colors!!.add("#FFF9C4")
        colors!!.add("#FFCCBC")
        colors!!.add("#CFD8DC")
        colors!!.add("#F8BBD0")
        colors!!.add("#C5CAE9")
        colors!!.add("#B2EBF2")
        colors!!.add("#DCEDC8")
        colors!!.add("#FFECB3")
        colors!!.add("#D7CCC8")
        colors!!.add("#F5F5F5")
        colors!!.add("#FFE0B2")
        colors!!.add("#F0F4C3")
        colors!!.add("#B2DFDB")
        colors!!.add("#BBDEFB")
        colors!!.add("#E1BEE7")
    }

    fun getColorCode(position: Int): String {
        return colors!![position]
    }

    /**
     * @param clientId is same as the userId
     */
    fun createMQttConnection(clientId: String?, notFromJobScheduler: Boolean): MqttAndroidClient {
        var clientId = clientId
        clientId = "yelo_$clientId$deviceId"
        Log.d("case21", "Yes called $clientId")
        if (mqttAndroidClient == null || mqttConnectOptions == null) {
            val serverUri = "tcp://" + ApiOnServer.HOST + ":" + ApiOnServer.PORT
            mqttAndroidClient = MqttAndroidClient(instance, serverUri, clientId, MemoryPersistence())
            mqttAndroidClient!!.setCallback(initMqttListener())
            mqttConnectOptions = MqttConnectOptions()
            mqttConnectOptions!!.isCleanSession = false
            mqttConnectOptions!!.userName = ApiOnServer.MQTTUSER_NAME
            val password = ApiOnServer.MQTTPASSWORD
            mqttConnectOptions!!.password = password.toCharArray()
            mqttConnectOptions!!.isAutomaticReconnect = true
            val obj = JSONObject()
            try {
                obj.put("lastSeenEnabled", sharedPreferences!!.getBoolean("enableLastSeen", true))
                obj.put("status", 2)
                obj.put("userId", userId)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            mqttConnectOptions!!.setWill(MqttEvents.OnlineStatus.value + "/" + userId, obj.toString().toByteArray(), 0, true)
            mqttConnectOptions!!.keepAliveInterval = 30
        }
        if (!serviceAlreadyScheduled) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val serviceName = ComponentName(instance!!, OreoJobService::class.java.name)
                val extras = PersistableBundle()
                extras.putString("command", "start")
                val jobInfo = JobInfo.Builder(MQTT_constants.MQTT_JOB_ID, serviceName).setExtras(extras).setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY).setMinimumLatency(1L).setOverrideDeadline(1L).build()
                val jobScheduler = instance!!.getSystemService(Context.JOB_SCHEDULER_SERVICE) as JobScheduler
                try {
                    jobScheduler.schedule(jobInfo)
                } catch (errorMessage: IllegalArgumentException) {
                    errorMessage.printStackTrace()
                }
            } else {
                val changeStatus = Intent(instance, AppKilled::class.java)
                startService(changeStatus)
            }
            serviceAlreadyScheduled = true
        }
        if (notFromJobScheduler) {
            connectMqttClient()
        }
        return mqttAndroidClient!!

        /*
     * Has been removed from here to avoid the reace condition for the mqtt
      * connection with the mqtt broker.
     */
        //connectMqttClient();
    }

    fun publish(topicName: String, obj: JSONObject, qos: Int, retained: Boolean) {
        Log.d("t45", "" + topicName)
        try {
            mqttAndroidClient!!.publish(topicName, obj.toString().toByteArray(), qos, retained)
        } catch (e: MqttException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }
    }

    private fun convertMessageToJsonObject(message: MqttMessage): JSONObject {
        var obj = JSONObject()
        try {
            obj = JSONObject(String(message.payload))
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        return obj
    }

    /**
     * To save the message received to the local couchdb
     */
    private fun putMessageInDb(receiverUid: String, messageType: String, offerType: String, actualMessage: String,
                               timestamp: String, id: String, receiverdocId: String, thumbnailMessage: String?,
                               dataSize: Int, senderName: String, isSelf: Boolean, status: Int, swapType: String, productUrl: String, swapProductUrl: String, swapProductId: String, isRejected: String) {
        var data: ByteArray? = ByteArray(100)
        Arrays.fill(data, 1.toByte())
        if (messageType != "17") data = Base64.decode(actualMessage, Base64.DEFAULT)
        var thumbnailData: ByteArray? = null
        if (messageType == "1" || messageType == "2" || messageType == "7") {
            thumbnailData = Base64.decode(thumbnailMessage, Base64.DEFAULT)
        }


        /*
 *
 *
 * initially in db we only store path of the thumbnail and nce downloaded we will replace that field withthe actual path of the downloaded file
 *
 *
 * */
        val tsInGmt = Utilities.epochtoGmt(timestamp)
        when (messageType) {
            "0" -> {
                var text = ""
                try {
                    text = String(data!!,charset("UTF-8"))
                } catch (e: UnsupportedEncodingException) {
                    e.printStackTrace()
                }
                if (text.trim { it <= ' ' }.isEmpty()) {
                    text = if (isSelf) {
                        resources.getString(R.string.YouInvited) + " " + senderName + " " +
                                resources.getString(R.string.JoinSecretChat)
                    } else {
                        resources.getString(R.string.youAreInvited) + " " + senderName + " " +
                                resources.getString(R.string.JoinSecretChat)
                    }
                }
                val map: HashMap<String, Any> = HashMap()
                map["message"] = text
                map["messageType"] = "0"
                map["isSelf"] = isSelf
                map["from"] = receiverUid
                map["Ts"] = tsInGmt!!
                map["id"] = id
                if (isSelf) {
                    map["deliveryStatus"] = status.toString()
                }
                if (status == -1) {
                    dbController!!.addNewChatMessageAndSort(receiverdocId, map, tsInGmt)
                    dbController!!.updateChatListForNewMessage(receiverdocId, text, true, tsInGmt, tsInGmt)
                } else {
                    dbController!!.addNewChatMessageAndSort(receiverdocId, map, null)
                }
            }
            "17" -> {
                val map: HashMap<String, Any> = HashMap()
                map["message"] = actualMessage
                map["messageType"] = messageType
                map["isSelf"] = isSelf
                map["from"] = receiverUid
                map["Ts"] = tsInGmt!!
                map["id"] = id
                map["productPicUrl"] = productUrl
                map["swapProductpicUrl"] = swapProductUrl
                map["swapProductId"] = swapProductId
                map["swapType"] = swapType
                map["isRejected"] = isRejected
                if (isSelf) {
                    map["deliveryStatus"] = status.toString()
                }
                if (status == -1) {
                    dbController!!.addNewChatMessageAndSort(receiverdocId, map, tsInGmt)
                    dbController!!.updateChatListForNewMessage(receiverdocId, actualMessage, true, tsInGmt, tsInGmt)
                } else {
                    dbController!!.addNewChatMessageAndSort(receiverdocId, map, null)
                }
            }
            "16" -> {
                var text = ""
                try {
                    text = String(data!!,charset("UTF-8"))
                } catch (e: UnsupportedEncodingException) {
                    e.printStackTrace()
                }
                val map: HashMap<String, Any> = HashMap()
                map["message"] = text
                map["messageType"] = "16"
                map["isSelf"] = isSelf
                map["from"] = receiverUid
                map["Ts"] = tsInGmt!!
                map["id"] = id
                if (isSelf) {
                    map["deliveryStatus"] = status.toString()
                }
                if (status == -1) {
                    dbController!!.addNewChatMessageAndSort(receiverdocId, map, tsInGmt)
                    dbController!!.updateChatListForNewMessage(receiverdocId, text, true, tsInGmt, tsInGmt)
                } else {
                    dbController!!.addNewChatMessageAndSort(receiverdocId, map, null)
                }
            }
            "15" -> {
                var text = ""
                try {
                    text = String(data!!,charset("UTF-8"))
                } catch (e: UnsupportedEncodingException) {
                    e.printStackTrace()
                }
                if (text.trim { it <= ' ' }.isEmpty()) {
                    text = if (isSelf) {
                        resources.getString(R.string.YouInvited) + " " + senderName + " " +
                                resources.getString(R.string.JoinSecretChat)
                    } else {
                        resources.getString(R.string.youAreInvited) + " " + senderName + " " +
                                resources.getString(R.string.JoinSecretChat)
                    }
                }
                val map: HashMap<String, Any> = HashMap()
                map["message"] = text
                map["messageType"] = "15"
                map["offerType"] = offerType
                map["isSelf"] = isSelf
                map["from"] = receiverUid
                map["Ts"] = tsInGmt!!
                map["id"] = id
                if (isSelf) {
                    map["deliveryStatus"] = status.toString()
                }
                if (status == -1) {
                    dbController!!.addNewChatMessageAndSort(receiverdocId, map, tsInGmt)
                    dbController!!.updateChatListForNewMessage(receiverdocId, text, true, tsInGmt, tsInGmt)
                } else {
                    dbController!!.addNewChatMessageAndSort(receiverdocId, map, null)
                }
            }
            "1" -> {

/*
 * receiverImage message
 */
                val thumbnailPath = convertByteArrayToFile(thumbnailData, timestamp, "jpg")
                val map: HashMap<String, Any> = HashMap()
                try {
                    map["message"] = String(data!!,charset("UTF-8"))
                } catch (e: UnsupportedEncodingException) {
                    e.printStackTrace()
                }
                map["messageType"] = "1"
                map["isSelf"] = isSelf
                map["from"] = receiverUid
                map["Ts"] = tsInGmt!!
                map["id"] = id
                map["downloadStatus"] = 0
                map["dataSize"] = dataSize
                map["thumbnailPath"] = thumbnailPath
                if (isSelf) {
                    map["deliveryStatus"] = status.toString()
                }
                if (status == -1) {
                    dbController!!.addNewChatMessageAndSort(receiverdocId, map, tsInGmt)
                    dbController!!.updateChatListForNewMessage(receiverdocId, getString(R.string.NewImage), true, tsInGmt, tsInGmt)
                } else {
                    dbController!!.addNewChatMessageAndSort(receiverdocId, map, null)
                }
            }
            "2" -> {


/*
 * Video message
 */
                val thumbnailPath = convertByteArrayToFile(thumbnailData, timestamp, "jpg")
                val map: HashMap<String, Any> = HashMap()


/*
 *
 *
 * message key will contail the url on server until downloaded and once downloaded
 * it will contain the local path of the video or image
 *
 * */try {
                    map["message"] = String(data!!,charset("UTF-8"))
                } catch (e: UnsupportedEncodingException) {
                    e.printStackTrace()
                }
                map["messageType"] = "2"
                map["isSelf"] = isSelf
                map["from"] = receiverUid
                map["Ts"] = tsInGmt!!
                map["id"] = id
                map["downloadStatus"] = 0
                map["dataSize"] = dataSize
                map["thumbnailPath"] = thumbnailPath
                if (isSelf) {
                    map["deliveryStatus"] = status.toString()
                }
                if (status == -1) {
                    dbController!!.addNewChatMessageAndSort(receiverdocId, map, tsInGmt)
                    dbController!!.updateChatListForNewMessage(receiverdocId, getString(R.string.NewVideo), true, tsInGmt, tsInGmt)
                } else {
                    dbController!!.addNewChatMessageAndSort(receiverdocId, map, null)
                }
            }
            "3" -> {

/*
 * Location message
 */
                var placeString = ""
                try {
                    placeString = String(data!!,charset("UTF-8"))
                } catch (e: UnsupportedEncodingException) {
                    e.printStackTrace()
                }
                val map: HashMap<String, Any> = HashMap()
                map["message"] = placeString
                map["messageType"] = "3"
                map["isSelf"] = isSelf
                map["from"] = receiverUid
                map["Ts"] = tsInGmt!!
                map["id"] = id
                if (isSelf) {
                    map["deliveryStatus"] = status.toString()
                }
                if (status == -1) {
                    dbController!!.addNewChatMessageAndSort(receiverdocId, map, tsInGmt)
                    dbController!!.updateChatListForNewMessage(receiverdocId, getString(R.string.NewLocation), true, tsInGmt, tsInGmt)
                } else {
                    dbController!!.addNewChatMessageAndSort(receiverdocId, map, null)
                }
            }
            "4" -> {

/*
 * Contact message
 */
                var contactString = ""
                try {
                    contactString = String(data!!,charset("UTF-8"))
                } catch (e: UnsupportedEncodingException) {
                    e.printStackTrace()
                }
                val map: HashMap<String, Any> = HashMap()
                map["message"] = contactString
                map["messageType"] = "4"
                map["isSelf"] = isSelf
                map["from"] = receiverUid
                map["Ts"] = tsInGmt!!
                map["id"] = id
                if (isSelf) {
                    map["deliveryStatus"] = status.toString()
                }
                if (status == -1) {
                    dbController!!.addNewChatMessageAndSort(receiverdocId, map, tsInGmt)
                    dbController!!.updateChatListForNewMessage(receiverdocId, getString(R.string.NewContact), true, tsInGmt, tsInGmt)
                } else {
                    dbController!!.addNewChatMessageAndSort(receiverdocId, map, null)
                }
            }
            "5" -> {


/*
 * Audio message
 */
                val map: HashMap<String, Any> = HashMap()
                try {
                    map["message"] = String(data!!,charset("UTF-8"))
                } catch (e: UnsupportedEncodingException) {
                    e.printStackTrace()
                }
                map["messageType"] = "5"
                map["isSelf"] = isSelf
                map["from"] = receiverUid
                map["Ts"] = tsInGmt!!
                map["id"] = id
                map["downloadStatus"] = 0
                map["dataSize"] = dataSize
                if (isSelf) {
                    map["deliveryStatus"] = status.toString()
                }
                if (status == -1) {
                    dbController!!.addNewChatMessageAndSort(receiverdocId, map, tsInGmt)
                    dbController!!.updateChatListForNewMessage(receiverdocId, getString(R.string.NewAudio), true, tsInGmt, tsInGmt)
                } else {
                    dbController!!.addNewChatMessageAndSort(receiverdocId, map, null)
                }
            }
            "6" -> {


                /*
             * Sticker
             */
                var text = ""
                try {
                    text = String(data!!,charset("UTF-8"))
                } catch (e: UnsupportedEncodingException) {
                }
                val map: HashMap<String, Any> = HashMap()
                map["message"] = text
                map["messageType"] = "6"
                map["isSelf"] = isSelf
                map["from"] = receiverUid
                map["Ts"] = tsInGmt!!
                map["id"] = id
                if (isSelf) {
                    map["deliveryStatus"] = status.toString()
                }
                if (status == -1) {
                    dbController!!.addNewChatMessageAndSort(receiverdocId, map, tsInGmt)
                    dbController!!.updateChatListForNewMessage(receiverdocId, getString(R.string.NewSticker), true, tsInGmt, tsInGmt)
                } else {
                    dbController!!.addNewChatMessageAndSort(receiverdocId, map, null)
                }
            }
            "7" -> {

/*
 * Doodle
 */
                var thumbnailPath: String? = convertByteArrayToFile(thumbnailData, timestamp, "jpg")
                val map: HashMap<String, Any?> = HashMap()
                try {
                    map["message"] = String(data!!,charset("UTF-8"))
                } catch (e: UnsupportedEncodingException) {
                }
                map["messageType"] = "7"
                map["isSelf"] = isSelf
                map["from"] = receiverUid
                map["Ts"] = tsInGmt
                map["id"] = id
                map["downloadStatus"] = 0
                map["dataSize"] = dataSize
                map["thumbnailPath"] = thumbnailPath
                if (isSelf) {
                    map["deliveryStatus"] = status.toString()
                }
                thumbnailPath = null
                if (status == -1) {
                    dbController!!.addNewChatMessageAndSort(receiverdocId, map, tsInGmt)
                    dbController!!.updateChatListForNewMessage(receiverdocId, getString(R.string.NewDoodle), true, tsInGmt, tsInGmt)
                } else {
                    dbController!!.addNewChatMessageAndSort(receiverdocId, map, null)
                }
            }
            "8" -> {


                /*
             * Gif
             */
                var url = ""
                try {
                    url = String(data!!,charset("UTF-8"))
                } catch (e: UnsupportedEncodingException) {
                }
                val map: HashMap<String, Any> = HashMap()
                map["message"] = url
                map["messageType"] = "8"
                map["isSelf"] = isSelf
                map["from"] = receiverUid
                map["Ts"] = tsInGmt!!
                map["id"] = id
                if (isSelf) {
                    map["deliveryStatus"] = status.toString()
                }
                if (status == -1) {
                    dbController!!.addNewChatMessageAndSort(receiverdocId, map, tsInGmt)
                    dbController!!.updateChatListForNewMessage(receiverdocId, getString(R.string.NewGiphy), true, tsInGmt, tsInGmt)
                } else {
                    dbController!!.addNewChatMessageAndSort(receiverdocId, map, null)
                }
            }
        }
    }

    fun convertByteArrayToFile(data: ByteArray?, name: String, extension: String): String {
        val file: File
        val path = filesDir.toString() + ApiOnServer.CHAT_RECEIVED_THUMBNAILS_FOLDER
        try {
            val folder = File(path)
            if (!folder.exists() && !folder.isDirectory) {
                folder.mkdirs()
            }
            file = File(path, "$name.$extension")
            if (!file.exists()) {
                file.createNewFile()
            }
            val fos = FileOutputStream(file)
            fos.write(data)
            fos.flush()
            fos.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return "$path/$name.$extension"
    }

    fun updateReconnected() {
        Log.d("log55", "reconnection ")
        handelConnection()
    }

    /*
    * Connect mqtt client*/
    fun connectMqttClient() {
        Log.d("cas21", "Yes called")
        try {
            Log.d("exe", "mqttAndroidClient$mqttAndroidClient")
            mqttAndroidClient!!.connect(mqttConnectOptions, instance, object : IMqttActionListener {
                override fun onSuccess(asyncActionToken: IMqttToken) {
                    Log.d("case22", "connected success")
                    handelConnection()
                    updatePresence(1, false)
                    //                    DisconnectedBufferOptions disconnectedBufferOptions = new DisconnectedBufferOptions();
//                    disconnectedBufferOptions.setBufferEnabled(true);
//                    disconnectedBufferOptions.setBufferSize(100);
//                    disconnectedBufferOptions.setPersistBuffer(false);
//                    disconnectedBufferOptions.setDeleteOldestMessages(false);
//
//
//                    mqttAndroidClient.setBufferOpts(disconnectedBufferOptions);


//                    Log.d("exe","disconnectedBufferOptions"+disconnectedBufferOptions);
                }

                override fun onFailure(asyncActionToken: IMqttToken, exception: Throwable) {
                    Log.d("log57", "Failed to connect to: " + exception.message)
                }
            })
        } catch (e: MqttException) {
            Log.d("cas57", "Yes called" + e.message)
            e.printStackTrace()
        }
    }

    /*
     *Subscribe topic. */
    fun subscribeToTopic(topic: String?, qos: Int) {
        try {
            if (mqttAndroidClient != null) {
                mqttAndroidClient!!.subscribe(topic!!, qos)
            }
        } catch (e: MqttException) {
            e.printStackTrace()
        }
    }

    fun unsubscribeToTopic(topic: String?) {
        try {
            if (mqttAndroidClient != null) {
                mqttAndroidClient!!.unsubscribe(topic!!)
            }
        } catch (e: MqttException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }
    }

    /*
     *Updating the user presence. */
    fun updatePresence(status: Int, applicationKilled: Boolean) {
        if (signedIn) {
            if (status == 0) {
                try {
                    val obj = JSONObject()
                    obj.put("status", 0)
                    if (applicationKilled) {
                        if (sharedPreferences!!.getString("lastSeenTime", null) != null) {
                            obj.put("timestamp", sharedPreferences!!.getString("lastSeenTime", null))
                        } else {
                            obj.put("timestamp", Utilities.tsInGmt())
                        }
                    } else {
                        obj.put("timestamp", Utilities.tsInGmt())
                    }
                    obj.put("userId", userId)
                    obj.put("lastSeenEnabled", sharedPreferences!!.getBoolean("enableLastSeen", true))
                    publish(MqttEvents.OnlineStatus.value + "/" + userId, obj, 0, true)
                } catch (w: JSONException) {
                    w.printStackTrace()
                }
            } else {
                /*
             *Foreground
             */
                try {
                    val obj = JSONObject()
                    obj.put("status", 1)
                    obj.put("userId", userId)
                    obj.put("lastSeenEnabled", sharedPreferences!!.getBoolean("enableLastSeen", true))
                    publish(MqttEvents.OnlineStatus.value + "/" + userId, obj, 0, true)
                } catch (w: JSONException) {
                    w.printStackTrace()
                }
            }
        }
    }

    fun publishChatMessage(topicName: String, obj: JSONObject, qos: Int, retained: Boolean, map: HashMap<String, Any?>) {
        Log.d("log33", "$topicName $obj")
        var token: IMqttDeliveryToken? = null
        try {
            obj.put("userImage", userImageUrl)
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        if (mqttAndroidClient != null && mqttAndroidClient!!.isConnected) {
            try {
                token = mqttAndroidClient!!.publish(topicName, obj.toString().toByteArray(), qos, retained)
            } catch (e: MqttException) {
                e.printStackTrace()
            }
            map["MQttToken"] = token
            tokenMapping.add(map)
            set.add(token)
        }
        /*
        * Throwing msg to FCM*/try {
            sendMessageToFcm(topicName, obj)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun canPublish(): Boolean {
        return mqttAndroidClient != null && mqttAndroidClient!!.isConnected
    }

    fun updateTokenMapping() {
        if (signedIn) {
            dbController!!.addMqttTokenMapping(mqttTokenDocId, tokenMapping)
        }
    }

    private fun resendUnsentMessages() {
        val documentId = instance!!.unsentMessageDocId
        if (documentId != null) {
            val arr = dbController!!.getUnsentMessages(documentId)
            if (arr.size > 0) {
                var to: String?
                for (i in arr.indices) {
                    val map = arr[i]
                    val obj = JSONObject()
                    try {
                        to = map["to"] as String?
                        obj.put("from", userId)
                        obj.put("to", map["to"])
                        obj.put("receiverIdentifier", userIdentifier)
                        val type = map["type"] as String?
                        val message = map["message"] as String?
                        val id = map["id"] as String?
                        var secretId: String? = ""
                        if (map.containsKey("secretId")) {
                            secretId = map["secretId"] as String?
                        }
                        obj.put("name", map["name"])
                        obj.put("toDocId", map["toDocId"])
                        obj.put("id", id)
                        obj.put("type", type)
                        if (!secretId!!.isEmpty()) {
                            obj.put("secretId", secretId)
                            obj.put("dTime", map["dTime"])
                        }
                        obj.put("timestamp", map["timestamp"])
                        val mapTemp = HashMap<String, Any?>()
                        mapTemp["messageId"] = id
                        mapTemp["docId"] = map["toDocId"]
                        if (type == "0") {


                            /*
                                 * Text message
                                 */
                            try {
                                obj.put("payload", Base64.encodeToString(message!!.toByteArray(charset("UTF-8")), Base64.DEFAULT).trim { it <= ' ' })
                            } catch (e: UnsupportedEncodingException) {
                            }
                            instance!!.publishChatMessage(MqttEvents.Message.value + "/" + to, obj, 1, false, mapTemp)
                            val tsInGmt = Utilities.tsInGmt()
                            val docId = instance!!.findDocumentIdOfReceiver(map["to"] as String?, secretId)
                            dbController!!.updateMessageTs(docId, id, tsInGmt)
                        } else if (type == "1") {
                            /*
                                 * receiverImage message
                                 */
                            var uri: Uri? = null
                            var bm: Bitmap? = null
                            try {
                                val options = BitmapFactory.Options()
                                options.inJustDecodeBounds = true
                                BitmapFactory.decodeFile(message, options)
                                val height = options.outHeight
                                val width = options.outWidth
                                val density = instance!!.resources.displayMetrics.density
                                var reqHeight: Int
                                reqHeight = (150 * density * (height / width)).toInt()
                                bm = instance!!.decodeSampledBitmapFromResource(message, (150 * density).toInt(), reqHeight)
                                if (bm != null) {
                                    var baos: ByteArrayOutputStream? = ByteArrayOutputStream()
                                    bm.compress(Bitmap.CompressFormat.JPEG, 50, baos)
                                    var b = baos!!.toByteArray()
                                    try {
                                        baos.close()
                                    } catch (e: IOException) {
                                    }
                                    baos = null
                                    var f = instance!!.convertByteArrayToFileToUpload(b, id, ".jpg")
                                    b = null
                                    uri = Uri.fromFile(f)
                                    f = null
                                }
                            } catch (e: OutOfMemoryError) {
                            } catch (e: Exception) {

                                /*
                                     *
                                     * to handle the file not found exception
                                     *
                                     *
                                     * */
                            }
                            if (uri != null) {


                                /*
                                     *
                                     *
                                     * make thumbnail
                                     *
                                     * */
                                var baos: ByteArrayOutputStream? = ByteArrayOutputStream()
                                bm!!.compress(Bitmap.CompressFormat.JPEG, 1, baos)
                                bm = null
                                var b = baos!!.toByteArray()
                                try {
                                    baos.close()
                                } catch (e: IOException) {
                                }
                                baos = null
                                obj.put("thumbnail", Base64.encodeToString(b, Base64.DEFAULT))
                                instance!!.uploadFile(uri,
                                        instance!!.userId + id, 1, obj, map["to"] as String?,
                                        id, mapTemp, secretId)
                                uri = null
                                b = null
                                bm = null
                            }
                        } else if (type == "2") {
                            /*
                                 * Video message
                                 */
                            var uri: Uri?
                            try {
                                var video: File? = File(message)
                                if (video!!.exists()) {
                                    var b = convertFileToByteArray(video)
                                    video = null
                                    var f = instance!!.convertByteArrayToFileToUpload(b, id, ".mp4")
                                    b = null
                                    uri = Uri.fromFile(f)
                                    f = null
                                    b = null
                                    if (uri != null) {
                                        var baos: ByteArrayOutputStream? = ByteArrayOutputStream()
                                        var bm = ThumbnailUtils.createVideoThumbnail(message,
                                                MediaStore.Images.Thumbnails.MINI_KIND)
                                        if (bm != null) {
                                            bm.compress(Bitmap.CompressFormat.JPEG, 1, baos)
                                            bm = null
                                            b = baos!!.toByteArray()
                                            try {
                                                baos.close()
                                            } catch (e: IOException) {
                                            }
                                            baos = null
                                            obj.put("thumbnail", Base64.encodeToString(b, Base64.DEFAULT))
                                            instance!!.uploadFile(uri,
                                                    instance!!.userId + id, 2, obj,
                                                    map["to"] as String?, id, mapTemp, secretId)
                                            uri = null
                                            b = null
                                        }
                                    }
                                }
                            } catch (e: OutOfMemoryError) {
                            } catch (e: Exception) {


                                /*
                                     *
                                     * to handle the file not found exception incase file has not been found
                                     *
                                     * */
                            }
                        } else if (type == "3") {


                            /*
                                 * Location message
                                 */
                            try {
                                obj.put("payload", Base64.encodeToString(message!!.toByteArray(charset("UTF-8")), Base64.DEFAULT).trim { it <= ' ' })
                            } catch (e: UnsupportedEncodingException) {
                            }
                            instance!!.publishChatMessage(MqttEvents.Message.value + "/" + to, obj, 1, false, mapTemp)
                            val tsInGmt = Utilities.tsInGmt()
                            val docId = instance!!.findDocumentIdOfReceiver(map["to"] as String?,
                                    secretId)
                            dbController!!.updateMessageTs(docId, id, tsInGmt)
                        } else if (type == "4") {

                            /*
                                 * Contact message
                                 */
                            try {
                                obj.put("payload", Base64.encodeToString(message!!.toByteArray(charset("UTF-8")), Base64.DEFAULT).trim { it <= ' ' })
                            } catch (e: UnsupportedEncodingException) {
                            }
                            instance!!.publishChatMessage(MqttEvents.Message.value + "/" + to, obj, 1, false, mapTemp)
                            val tsInGmt = Utilities.tsInGmt()
                            val docId = instance!!.findDocumentIdOfReceiver(map["to"] as String?, secretId)
                            dbController!!.updateMessageTs(docId, id, tsInGmt)
                        } else if (type == "5") {

                            /*
                                 * Audio message
                                 */
                            var uri: Uri?
                            try {
                                var audio: File? = File(message)
                                if (audio!!.exists()) {
                                    var b = convertFileToByteArray(audio)
                                    audio = null
                                    var f = instance!!.convertByteArrayToFileToUpload(b, id, ".mp3")
                                    b = null
                                    uri = Uri.fromFile(f)
                                    f = null
                                    b = null
                                    if (uri != null) {
                                        instance!!.uploadFile(uri, instance!!.userId + id,
                                                5, obj, map["to"] as String?, id, mapTemp, secretId)
                                    }
                                }
                            } catch (e: Exception) {


                                /*
                                     *
                                     * to handle the file not found exception incase file has not been found
                                     *
                                     * */
                            }
                        } else if (type == "6") {
                            /*
                             * Sticker
                             */
                            try {
                                obj.put("payload", Base64.encodeToString(message!!.toByteArray(charset("UTF-8")), Base64.DEFAULT).trim { it <= ' ' })
                            } catch (e: UnsupportedEncodingException) {
                            }
                            instance!!.publishChatMessage(MqttEvents.Message.value + "/" + to, obj, 1, false, mapTemp)
                            val tsInGmt = Utilities.tsInGmt()
                            val docId = instance!!.findDocumentIdOfReceiver(map["to"] as String?, secretId)
                            dbController!!.updateMessageTs(docId, id, tsInGmt)
                        } else if (type == "7") {
                            /*
                             *Doodle
                             */
                            var uri: Uri? = null
                            var bm: Bitmap? = null
                            try {
                                val options = BitmapFactory.Options()
                                options.inJustDecodeBounds = true
                                BitmapFactory.decodeFile(message, options)
                                val height = options.outHeight
                                val width = options.outWidth
                                val density = instance!!.resources.displayMetrics.density
                                var reqHeight: Int
                                reqHeight = (150 * density * (height / width)).toInt()
                                bm = instance!!.decodeSampledBitmapFromResource(message, (150 * density).toInt(), reqHeight)
                                if (bm != null) {
                                    var baos: ByteArrayOutputStream? = ByteArrayOutputStream()
                                    bm.compress(Bitmap.CompressFormat.JPEG, 50, baos)
                                    var b = baos!!.toByteArray()
                                    try {
                                        baos.close()
                                    } catch (e: IOException) {
                                    }
                                    baos = null
                                    var f = instance!!.convertByteArrayToFileToUpload(b, id, ".jpg")
                                    b = null
                                    uri = Uri.fromFile(f)
                                    f = null
                                }
                            } catch (e: OutOfMemoryError) {
                            } catch (e: Exception) {

                                /*
                                     *
                                     * to handle the file not found exception
                                     *
                                     *
                                     * */
                            }
                            if (uri != null) {


                                /*
                                     *
                                     *
                                     * make thumbnail
                                     *
                                     * */
                                var baos: ByteArrayOutputStream? = ByteArrayOutputStream()
                                bm!!.compress(Bitmap.CompressFormat.JPEG, 1, baos)
                                bm = null
                                var b = baos!!.toByteArray()
                                try {
                                    baos.close()
                                } catch (e: IOException) {
                                    e.printStackTrace()
                                }
                                baos = null
                                obj.put("thumbnail", Base64.encodeToString(b, Base64.DEFAULT))
                                instance!!.uploadFile(uri, instance!!.userId + id, 7, obj,
                                        map["to"] as String?, id, mapTemp, secretId)
                                uri = null
                                b = null
                                bm = null
                            }
                        } else if (type == "8") {
                            /*
                             *Gif
                             */
                            try {
                                obj.put("payload", Base64.encodeToString(message!!.toByteArray(charset("UTF-8")), Base64.DEFAULT).trim { it <= ' ' })
                            } catch (e: UnsupportedEncodingException) {
                                e.printStackTrace()
                            }
                            instance!!.publishChatMessage(MqttEvents.Message.value + "/" + to, obj, 1, false, mapTemp)
                            val tsInGmt = Utilities.tsInGmt()
                            val docId = instance!!.findDocumentIdOfReceiver(map["to"] as String?, secretId)
                            dbController!!.updateMessageTs(docId, id, tsInGmt)
                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    } catch (e: OutOfMemoryError) {
                        e.printStackTrace()
                    }
                }
            }
        }
    }

    /*
     *disconnect the last mqtt client */
    fun disconnect() {
        if (null != mqttAndroidClient && mqttAndroidClient!!.isConnected) {
            try {
                updatePresence(0, true)
                unSubscribeAll()
                mqttAndroidClient!!.disconnect()
                mqttAndroidClient!!.unregisterResources()
                mqttAndroidClient = null
            } catch (e: MqttException) {
                e.printStackTrace()
            }
        }
    }

    /**
     * To generate the push notifications locally
     */
    private fun generatePushNotificationLocal(notificationId: String, messageType: String, offertype: String, senderName: String, actualMessage: String, intent: Intent, secretId: String, receiverUid: String) {
        Log.d("Notification", "Yes in")
        val message_type = messageType.toInt()
        if (!isForeground || activeReceiverId.isEmpty() || !(activeReceiverId == receiverUid && activeSecretId == secretId)) {
            try {
                var pushMessage = ""
                when (message_type) {
                    15 -> when (offertype) {
                        "1" -> pushMessage = "$senderName has made an offer on your product"
                        "2" -> pushMessage = "Congratulations User $senderName has accepted your offer."
                        "3" -> pushMessage = "$senderName has sent a counter offer on your product"
                        else -> try {
                            pushMessage = String(Base64.decode(actualMessage, Base64.DEFAULT),charset("UTF-8"))
                            pushMessage = "Got offer $pushMessage"
                        } catch (e: UnsupportedEncodingException) {
                            e.printStackTrace()
                        }
                    }
                    16 -> pushMessage = "" + senderName + "has shared the payment link with you."
                    0 -> try {
                        pushMessage = String(Base64.decode(actualMessage, Base64.DEFAULT),charset("UTF-8"))
                        if (pushMessage.trim { it <= ' ' }.isEmpty()) {
                            pushMessage = resources.getString(R.string.youAreInvited) + " " + senderName + " " +
                                    resources.getString(R.string.JoinSecretChat)
                        }
                    } catch (e: UnsupportedEncodingException) {
                        e.printStackTrace()
                    }
                    1 -> pushMessage = "$senderName has sent you an image"
                    2 -> pushMessage = "Video"
                    3 -> pushMessage = "$senderName has shared the location with you."
                    4 -> pushMessage = "Contact"
                    5 -> pushMessage = "Audio"
                    6 -> pushMessage = "Sticker"
                    7 -> pushMessage = "Doodle"
                    17 -> pushMessage = "Swap Offer"
                    else -> pushMessage = "Gif"
                }
                val inboxStyle = NotificationCompat.InboxStyle()
                var notificationInfo = fetchNotificationInfo(notificationId)
                val unreadMessageCount: Int
                val systemNotificationId: Int
                val messages: ArrayList<String>?
                if (notificationInfo == null) {
                    notificationInfo = HashMap()
                    messages = ArrayList()
                    messages.add(pushMessage)
                    notificationInfo["notificationMessages"] = messages
                    notificationInfo["notificationId"] = notificationId
                    systemNotificationId = System.currentTimeMillis().toString().substring(9).toInt()
                    notificationInfo["systemNotificationId"] = systemNotificationId
                    unreadMessageCount = 0
                } else {
                    messages = notificationInfo["notificationMessages"] as ArrayList<String>?
                    messages!!.add(0, pushMessage)
                    if (messages.size > NOTIFICATION_SIZE) {
                        messages.removeAt(messages.size - 1)
                    }
                    systemNotificationId = notificationInfo["systemNotificationId"] as Int

                    notificationInfo.put("notificationMessages",  messages!!)
                    unreadMessageCount = notificationInfo["messagesCount"] as Int
                }
                notificationInfo.put("messagesCount", unreadMessageCount + 1)
                addOrUpdateNotification(notificationInfo, notificationId)
                inboxStyle.setBigContentTitle(senderName)
                for (i in messages!!.indices) {
                    inboxStyle.addLine(messages[i])
                }
                if (unreadMessageCount > NOTIFICATION_SIZE - 1) {
                    inboxStyle.setSummaryText("+" + (unreadMessageCount - (NOTIFICATION_SIZE - 1)) + " " + getString(R.string.MoreMessage))
                }
                val pendingIntent = PendingIntent.getActivity(this, systemNotificationId, intent, PendingIntent.FLAG_ONE_SHOT)
                val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
                val title: String
                val tickerText: String
                if (message_type == 15 || message_type == 16) {
                    pushMessage = "" + pushMessage
                    tickerText = pushMessage
                    title = getString(R.string.app_name)
                } else if (messages.size == 1) {
                    pushMessage = "$senderName: $pushMessage"
                    tickerText = pushMessage
                    title = getString(R.string.app_name)
                } else {
                    tickerText = "$senderName: $pushMessage"
                    title = senderName
                }
                createChatNotificationChannel()
                val notificationBuilder = NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setLargeIcon(BitmapFactory.decodeResource(resources,
                                R.mipmap.ic_launcher))
                        .setContentTitle(title)
                        .setContentText(pushMessage)
                        .setTicker(tickerText)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent)
                        .setStyle(inboxStyle)
                        .setDefaults(Notification.DEFAULT_VIBRATE)
                        .setPriority(Notification.PRIORITY_HIGH)
                        .setChannelId(VariableConstants.CHAT_NOTIFICATION_CHANNEL)
                if (Build.VERSION.SDK_INT >= 21) notificationBuilder.setVibrate(LongArray(0))
                val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                /*
                 *Notification details. */notificationManager.notify(notificationId, systemNotificationId, notificationBuilder.build())
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun createChatNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name: CharSequence = resources.getString(R.string.chat_notification)
            val description = resources.getString(R.string.chat_notification_desc)
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(VariableConstants.CHAT_NOTIFICATION_CHANNEL, name, importance)
            channel.description = description
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            val notificationManager = getSystemService(NotificationManager::class.java)!!
            notificationManager.createNotificationChannel(channel)
        }
    }

    override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
        /*
        * As app can also be started when clicked on the chat notification*/
        if (instance!!.signedIn && activity.javaClass.simpleName == "MainActivity") {
            isForeground = true
            setApplicationKilled(false)
            updatePresence(1, false)
        }
    }

    override fun onActivityDestroyed(activity: Activity) {}
    override fun onActivityPaused(activity: Activity) {}
    override fun onActivityResumed(activity: Activity) {}
    override fun onActivitySaveInstanceState(activity: Activity,
                                             outState: Bundle) {
    }

    override fun onActivityStarted(activity: Activity) {}
    override fun onActivityStopped(activity: Activity) {}
    fun updateLastSeenSettings(lastSeenSetting: Boolean) {
        sharedPreferences!!.edit().putBoolean("enableLastSeen", lastSeenSetting).apply()
    }

    val currentTime: Unit
        get() {
            FetchTime().execute()
        }

    fun randomString(): String {
        val chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".toCharArray()
        val sb = StringBuilder()
        val random = Random()
        for (i in 0..19) {
            val c = chars[random.nextInt(chars.size)]
            sb.append(c)
        }
        sb.append("PnPLabs3Embed")
        return sb.toString()
    }

    private inner class FetchTime : AsyncTask<Any?, Any?, Any?>() {
        override fun doInBackground(params: Array<Any?>): Any? {
            sharedPreferences!!.edit().putBoolean("deltaRequired", true).apply()
            val url_ping = "https://google.com/"
            var url: URL? = null
            try {
                url = URL(url_ping)
            } catch (e: MalformedURLException) {
            }
            try {
/*
 * Maybe inaccurate due to network inaccuracy
 */
                val urlc = url!!.openConnection() as HttpURLConnection
                if (urlc.responseCode == 200 || urlc.responseCode == 503) {
                    val dateStr = urlc.date
                    //Here I do something with the Date String
                    timeDelta = System.currentTimeMillis() - dateStr
                    sharedPreferences!!.edit().putBoolean("deltaRequired", false).apply()
                    sharedPreferences!!.edit().putLong("timeDelta", timeDelta).apply()
                    urlc.disconnect()
                }
            } catch (e: IOException) {


                /*
                 * Should disable user from using the app
                 */
            } catch (e: NullPointerException) {
            }
            return null
        }
    }

    /**
     * To fetch a particular notification info
     */
    private fun fetchNotificationInfo(notificationId: String): HashMap<String, Any>? {
        for (i in notifications.indices) {
            if (notifications[i]["notificationId"] == notificationId) {
                return notifications[i]
            }
        }
        return null
    }

    /**
     * To add or update the content of the notifications
     */
    private fun addOrUpdateNotification(notification: HashMap<String, Any>, notificationId: String) {
        dbController!!.addOrUpdateNotificationContent(notificationDocId, notificationId, notification)
        for (i in notifications.indices) {
            if (notifications[i]["notificationId"] == notificationId) {
                notifications[i] = notification
                return
            }
        }
        notifications.add(0, notification)
    }

    /*
     * To remove a particular notification
     */
    fun removeNotification(notificationId: String) {
        var found = false
        for (i in notifications.indices) {
            if (notifications[i]["notificationId"] == notificationId) {
                notifications.removeAt(i)
                found = true
                break
            }
        }
        if (found) {
            //  db.getParticularNotificationId(notificationDocId, notificationId);
            val systemNotificationId = dbController!!.removeNotification(notificationDocId, notificationId)
            if (systemNotificationId != -1) {
                val nMgr = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                nMgr.cancel(notificationId, systemNotificationId)
            }
        }
    }

    /*
     *Doing user logged out. */
    fun doLoggedOut() {
        disconnect()
        dbController!!.updateIndexDocumentOnSignOut(indexDocId)
        signedIn = false
    }

    /*
     *Doing the logged out of the previous user topic. */
    private fun unSubscribeAll() {
        if (userId != null && !userId!!.isEmpty()) {
            unsubscribeToTopic(MqttEvents.Message.value + "/" + userId)
            unsubscribeToTopic(MqttEvents.OfferMessage.value + "/" + userId)
            unsubscribeToTopic(MqttEvents.Acknowledgement.value + "/" + userId)
            unsubscribeToTopic(MqttEvents.FetchMessages.value + "/" + userId)
            unsubscribeToTopic(MqttEvents.FetchChats.value + "/" + userId)
            unsubscribeToTopic(MqttEvents.UpdateProduct.value + "/" + userId)
            unsubscribeToTopic(MqttEvents.UserUpdates.value + "/" + userId)
            unsubscribeToTopic(MqttEvents.Typing.value + "/" + userId)
        }
    }

    /*
    *Sending message to the FCM for IOS device Notifcation
    * problem.*/
    @Throws(Exception::class)
    fun sendMessageToFcm(topicName: String, messageObject: JSONObject) {
        val topic = topicName.substring(topicName.indexOf("/") + 1, topicName.length)
        val to = "/topics/$topic"
        val messageType = messageObject.getString("type")
        val actualMessage = messageObject.getString("payload").trim { it <= ' ' }
        val senderName = messageObject.getString("name")
        var offerType: String? = "1"
        if (messageObject.has("offerType")) {
            offerType = messageObject.getString("offerType")
        }
        val productId = messageObject.getString("secretId")
        val receiverId = messageObject.getString("from")
        var pushMessage = ""
        val message_type = messageType.toInt()
        var title = ""
        var swapType: String? = "1"
        if (messageObject.has("swapType")) {
            swapType = messageObject.getString("swapType")
        }
        when (message_type) {
            15 -> when (offerType) {
                "1" -> pushMessage = "" + "You got an offer on your product"
                "2" -> pushMessage = "Congratulations User $senderName has accepted your offer."
                "3" -> pushMessage = "" + "You got a counter offer on your product"
                else -> try {
                    pushMessage = String(Base64.decode(actualMessage, Base64.DEFAULT),charset("UTF-8"))
                    pushMessage = "Got offer $pushMessage"
                } catch (e: UnsupportedEncodingException) {
                    e.printStackTrace()
                }
            }
            17 -> when (swapType) {
                "1" -> pushMessage = "" + "You got an swap offer on your product"
                "2" -> pushMessage = "Congratulations User $senderName has accepted your swap offer."
                "3" -> pushMessage = "Sorry User $senderName has rejected your swap offer. Try on other product."
            }
            16 -> pushMessage = "" + senderName + "has shared the payment link with you."
            0 -> {
                title = senderName
                pushMessage = try {
                    String(Base64.decode(actualMessage, Base64.DEFAULT),charset("UTF-8"))
                } catch (e: UnsupportedEncodingException) {
                    e.printStackTrace()
                    ""
                }
            }
            1 -> pushMessage = "$senderName has sent you an image"
            2 -> pushMessage = "Video"
            3 -> pushMessage = "$senderName has shared the location with you."
            4 -> pushMessage = "Contact"
            5 -> pushMessage = "Audio"
            6 -> pushMessage = "Sticker"
            7 -> pushMessage = "Doodle"
            else -> pushMessage = "Gif"
        }
        sendPushToSingleInstance(to, title, pushMessage, productId, receiverId)
    }

    /*
     * Sending message to the FCM*/
    private fun sendPushToSingleInstance(topicName: String, title: String, message: String, productId: String, receiverID: String) {
        val url = "https://fcm.googleapis.com/fcm/send"
        val jsonObjReq: JsonObjectRequest = object : JsonObjectRequest(Method.POST,
                url, null, com.android.volley.Response.Listener { }, com.android.volley.Response.ErrorListener { error -> error.printStackTrace() }) {
            override fun getBody(): ByteArray {
                val notification = JSONObject()
                val root = JSONObject()
                try {
                    if (title.isEmpty()) {
                        notification.put("title", "" + message)
                    } else {
                        notification.put("title", "" + title)
                        notification.put("body", "" + message)
                    }
                    notification.put("sound", "default")
                    val body = JSONObject()
                    body.put("receiverID", receiverID)
                    body.put("secretID", productId)
                    val data = JSONObject()
                    data.put("message", message)
                    data.put("body", body)
                    root.put("notification", notification)
                    root.put("data", data)
                    root.put("to", topicName) //"condition": "'\(topic)' in topics"
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
                Log.d("ddfs", root.toString())
                return root.toString().toByteArray()
            }

            override fun getBodyContentType(): String {
                return "application/json; charset=utf-8"
            }

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["Authorization"] = "key=" + ApiOnServer.PUSH_KEY
                return headers
            }
        }
        instance!!.addToRequestQueue(jsonObjReq, "PushSend")
    }

    // on app launch once we are getting chat suggestion messages from server
    @JvmField
    var chatSuggestionList: ArrayList<String?>? = null
    val chatSuggetionMessageApi: Unit
        get() {
            val requestData = JSONObject()
            try {
                requestData.put("token", mSessionManager!!.authToken)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            val url = ApiUrl.CHAT_SUGGESTION_MESSAGE + "?token=" + mSessionManager!!.authToken
            doOkHttp3Connection("", url, OkHttp3Connection.Request_type.GET, requestData, object : OkHttp3RequestCallback {
                override fun onSuccess(result: String?, user_tag: String?) {
                    println("chat suggestion messages=$result")
                    try {
                        val gson = Gson()
                        val messageMainPojo = gson.fromJson(result, MessageMainPojo::class.java)
                        when (messageMainPojo.code) {
                            "200" -> {
                                chatSuggestionList = ArrayList()
                                val data = messageMainPojo.data
                                for (d in data!!) {
                                    chatSuggestionList!!.add(d.message)
                                }
                            }
                            else -> println("chat suggestion messages=" + messageMainPojo.message)
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

                override fun onError(error: String?, user_tag: String?) {}
            })
        }

    companion object {
        val TAG = AppController::class.java.simpleName
        @JvmStatic
        var bus = Bus(ThreadEnforcer.ANY)

        @get:Synchronized
        var instance: AppController? = null
            private set

        private const val NOTIFICATION_SIZE = 5

        /**
         * Convert image or video or audio to byte[] so that it can be sent on socket(Unsetn messages)
         */
        private fun convertFileToByteArray(f: File?): ByteArray? {
            var byteArray: ByteArray? = null
            var b: ByteArray?
            val bos = ByteArrayOutputStream()
            try {
                val inputStream: InputStream = FileInputStream(f)
                b = ByteArray(2663)
                var bytesRead: Int
                while (inputStream.read(b).also { bytesRead = it } != -1) {
                    bos.write(b, 0, bytesRead)
                }
                byteArray = bos.toByteArray()
            } catch (e: IOException) {
            } catch (e: OutOfMemoryError) {
            } finally {
                b = null
                try {
                    bos.close()
                } catch (e: IOException) {
                }
            }
            return byteArray
        }

        /**
         * To find the document id of the receiver on receipt of new message,if exists or create a new document for chat with that receiver and return its document id
         */
        @JvmStatic
        fun findDocumentIdOfReceiver(receiverUid: String, timestamp: String?, receiverName: String?,
                                     receiverImage: String?, secretId: String, invited: Boolean, receiverIdentifier: String?, chatId: String?, productImage: String?, productName: String?, productPrice: String?, isSlod: Boolean, isAccepted: Boolean): String {
            val db = instance!!.dbController
            val chatDetails = db!!.getAllChatDetails(instance!!.chatDocId)
            if (chatDetails != null) {
                val receiverUidArray = chatDetails["receiverUidArray"] as ArrayList<String>?
                val receiverDocIdArray = chatDetails["receiverDocIdArray"] as ArrayList<String>?
                val secretIdArray = chatDetails["secretIdArray"] as ArrayList<String>?
                for (i in receiverUidArray!!.indices) {
                    if (receiverUidArray[i] == receiverUid && secretIdArray!![i] == secretId) {
                        return receiverDocIdArray!![i]
                    }
                }
            }
            /*  here we also need to enter receiver name*/
            val docId = db.createDocumentForChat(timestamp, receiverUid, receiverName, receiverImage, secretId, invited, receiverIdentifier, chatId, productImage, productName, productPrice, isSlod, isAccepted)
            db.addChatDocumentDetails(receiverUid, docId, instance!!.chatDocId, secretId)
            return docId
        }
    }
}