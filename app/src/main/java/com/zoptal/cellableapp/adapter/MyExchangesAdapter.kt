package com.zoptal.cellableapp.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.adapter.ExploreRvAdapter
import com.zoptal.cellableapp.pojo_class.profile_myexchanges_pojo.ProfileMyExchangesData
import com.zoptal.cellableapp.utility.CircleTransform
import com.zoptal.cellableapp.utility.CommonClass
import java.util.*

/**
 * Created by ${3embed} on ${27-10-2017}.
 * Banglore
 */
class MyExchangesAdapter(private val profileMyExchangesDataArrayList: ArrayList<ProfileMyExchangesData>, private val mActivity: Activity) : RecyclerView.Adapter<MyExchangesAdapter.MyViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(mActivity).inflate(R.layout.single_row_profile_myexchanges, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val profileMyExchangesData = profileMyExchangesDataArrayList[position]
        val mainUrl = profileMyExchangesData.mainUrl
        val thumbNailUrl = profileMyExchangesData.thumbnailImageUrl
        val swapUrl = profileMyExchangesData.swapThumbnailImageUrl
        val time = profileMyExchangesData.acceptedOn
        holder.myPostIv.layoutParams.width = CommonClass.getDeviceWidth(mActivity) / 9
        holder.myPostIv.layoutParams.height = CommonClass.getDeviceWidth(mActivity) / 9
        holder.swapIv.layoutParams.width = CommonClass.getDeviceWidth(mActivity) / 9
        holder.swapIv.layoutParams.height = CommonClass.getDeviceWidth(mActivity) / 9
        println("$TAG thumbNailUrl$thumbNailUrl")

        // main image
        try {
            if (mainUrl != null && !mainUrl.isEmpty()) Picasso.with(mActivity)
                    .load(mainUrl)
                    .resize(CommonClass.getDeviceWidth(mActivity), CommonClass.getDeviceWidth(mActivity))
                    .centerCrop()
                    .placeholder(R.color.add_title)
                    .error(R.color.add_title)
                    .into(holder.iV_image)
            /*Glide.with(mActivity)
                        .load(mainUrl)
                        .asBitmap()
                        .centerCrop()
                        .placeholder(R.color.add_title)
                        .error(R.color.add_title)
                        .into(holder.iV_image)*/
        } catch (e: OutOfMemoryError) {
            e.printStackTrace()
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        }

        //Thumb image
        if (thumbNailUrl != null && !thumbNailUrl.isEmpty()) {
            Picasso.with(mActivity)
                    .load(thumbNailUrl)
                    .transform(CircleTransform())
                    .placeholder(R.drawable.default_circle_img)
                    .error(R.drawable.default_circle_img)
                    .into(holder.myPostIv)
        }

        //swap image
        if (swapUrl != null && !swapUrl.isEmpty()) {
            Picasso.with(mActivity)
                    .load(swapUrl)
                    .transform(CircleTransform())
                    .placeholder(R.drawable.default_circle_img)
                    .error(R.drawable.default_circle_img)
                    .into(holder.swapIv)
        }

        // Posted on
        if (time != null && !time.isEmpty()) holder.acceptedOnTv.text = CommonClass.getTimeDifference(time)
        holder.descriptionTv.text = "You swaped " + profileMyExchangesData.productName + " " + "for " + profileMyExchangesData.swapProductName
    }

    override fun getItemCount(): Int {
        return profileMyExchangesDataArrayList.size
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val iV_image: ImageView
        val myPostIv: ImageView
        val swapIv: ImageView
        val acceptedOnTv: TextView
        val descriptionTv: TextView

        init {
            iV_image = itemView.findViewById<View>(R.id.iV_image) as ImageView
            myPostIv = itemView.findViewById<View>(R.id.myPostIv) as ImageView
            swapIv = itemView.findViewById<View>(R.id.swapIv) as ImageView
            acceptedOnTv = itemView.findViewById<View>(R.id.acceptedOnTv) as TextView
            descriptionTv = itemView.findViewById<View>(R.id.descriptionTv) as TextView
        }
    }

    companion object {
        private val TAG = ExploreRvAdapter::class.java.simpleName
    }

}