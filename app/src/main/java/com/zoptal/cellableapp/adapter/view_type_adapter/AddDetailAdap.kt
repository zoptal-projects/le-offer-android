package com.zoptal.cellableapp.adapter.view_type_adapter

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.pojo_class.product_category.FilterData
import com.zoptal.cellableapp.utility.ClickListener
import java.util.*

/**
 * Created by embed on 17/5/18.
 */
class AddDetailAdap(private val mContext: Context, private val data: ArrayList<FilterData>) : RecyclerView.Adapter<AddDetailAdap.MyViewHolder?>() {
    private val clickListener: ClickListener
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.single_row_add_detail_field_item, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        if (data[position].isMandatory) {
            holder.tV_mendatory.visibility = View.VISIBLE
        } else {
            holder.tV_mendatory.visibility = View.GONE
        }
        if (data[position].isSelected || !data[position].selectedValues.isEmpty()) {
            holder.tV_value.text = data[position].selectedValues
            data[position].isSelected = true
        } else {
            holder.tV_value.text = ""
            data[position].isSelected = false
        }
        holder.tV_fieldName.text = data[position].fieldName
        holder.itemView.setOnClickListener(View.OnClickListener { view -> clickListener.onItemClick(view, position) })
    }

    override fun getItemCount(): Int {
        return data.size
    }


    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tV_fieldName: TextView
        val tV_value: TextView
        val tV_mendatory: TextView

        init {
            tV_fieldName = itemView.findViewById<View>(R.id.tV_fieldName) as TextView
            tV_value = itemView.findViewById<View>(R.id.tV_value) as TextView
            tV_mendatory = itemView.findViewById<View>(R.id.tV_mendatory) as TextView
        }
    }

    init {
        clickListener = mContext as ClickListener
    }
}