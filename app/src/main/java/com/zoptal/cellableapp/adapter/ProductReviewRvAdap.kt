package com.zoptal.cellableapp.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.pojo_class.product_review.ProductReviewResult
import com.zoptal.cellableapp.utility.CircleTransform
import com.zoptal.cellableapp.utility.CommonClass
import java.util.*

/**
 * <h>HomeFragRvAdapter</h>
 *
 *
 * In class is called from ProductReviewActivity class. In this recyclerview adapter class we used to inflate
 * single_row_item_reviews layout and shows the all review into list.
 *
 * @since 09-Jun-17
 */
class ProductReviewRvAdap
/**
 * <h>ProductReviewRvAdap</h>
 *
 *
 * This is simple constructor to initailize list datas and context.
 *
 * @param mActivity The current context
 * @param arrayListReview The list datas
 */(private val mActivity: Activity, private val arrayListReview: ArrayList<ProductReviewResult>) : RecyclerView.Adapter<ProductReviewRvAdap.MyViewHolder>() {

    /**
     * <h>OnCreateViewHolder</h>
     *
     *
     * In this method The adapter prepares the layout of the items by inflating the correct
     * layout for the individual data elements.
     *
     * @param parent A ViewGroup is a special view that can contain other views (called children.)
     * @param viewType Within the getItemViewType method the recycler view determines which type should be used for data.
     * @return It returns an object of type ViewHolder per visual entry in the recycler view.
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(mActivity).inflate(R.layout.single_row_item_reviews, parent, false)
        return MyViewHolder(view)
    }

    /**
     * <h>OnBindViewHolder</h>
     *
     *
     * In this method Every visible entry in a recycler view is filled with the
     * correct data model item by the adapter. Once a data item becomes visible,
     * the adapter assigns this data to the individual widgets which he inflated
     * earlier.
     *
     * @param holder The referece of MyViewHolder class of current class.
     * @param position The position of particular item
     */
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val reviewResult = arrayListReview[position]
        val profilePic: String?
        val userName: String?
        val messageBody: String?
        val time: String?
        profilePic = reviewResult.profilePicUrl
        userName = reviewResult.username
        messageBody = reviewResult.commentBody
        time = reviewResult.commentedOn
        println(TAG + " " + "time=" + time + " " + "diff time=" + CommonClass.getTimeDifference(time))

        // user image
        if (profilePic != null && !profilePic.isEmpty()) Picasso.with(mActivity)
                .load(profilePic)
                .transform(CircleTransform())
                .placeholder(R.drawable.default_circle_img)
                .error(R.drawable.default_circle_img)
                .into(holder.iV_userPic)

        // user name
        if (userName != null) holder.tV_userName.text = userName

        // message body
        if (messageBody != null) holder.tV_description.text = messageBody

        // time
        if (time != null) holder.tV_time.text = CommonClass.getTimeDifference(time)
    }

    /**
     * Return the size of your dataset
     * @return the total number of rows
     */
    override fun getItemCount(): Int {
        return arrayListReview.size
    }

    /**
     * <h>MyViewHolder</h>
     *
     *
     * In this class we used to declare and assign the xml variables.
     *
     */
    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var iV_userPic: ImageView
        var tV_userName: TextView
        var tV_description: TextView
        var tV_time: TextView

        init {
            iV_userPic = itemView.findViewById<View>(R.id.iV_userPic) as ImageView
            iV_userPic.layoutParams.width = CommonClass.getDeviceWidth(mActivity) / 8
            iV_userPic.layoutParams.height = CommonClass.getDeviceWidth(mActivity) / 8
            tV_userName = itemView.findViewById<View>(R.id.tV_userName) as TextView
            tV_description = itemView.findViewById<View>(R.id.tV_description) as TextView
            tV_time = itemView.findViewById<View>(R.id.tV_time) as TextView
        }
    }

    fun removeItem(position: Int, textView: RelativeLayout) {
        arrayListReview.removeAt(position)
        notifyItemRemoved(position)
        notifyItemRangeChanged(position, arrayListReview.size)
        if (arrayListReview.size == 0) textView.visibility = View.VISIBLE
    }

    companion object {
        private val TAG = ProductReviewRvAdap::class.java.simpleName
    }

}