package com.zoptal.cellableapp.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.FitCenter
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.main.activity.GalleryActivity
import com.zoptal.cellableapp.pojo_class.GalleryImagePojo
import com.zoptal.cellableapp.utility.CommonClass
import com.zoptal.cellableapp.utility.MyTransformation
import java.util.*

/**
 * <h>GalleryRvAdap</h>
 *
 *
 * In class is called from GalleryActivity class. In this recyclerview adapter class we used to inflate
 * single_row_gallery_images layout and shows the all gallery images.
 *
 * @since 20-Jun-17
 */
class GalleryRvAdap
/**
 * <h>GalleryRvAdap</h>
 *
 *
 * This is simple constructor to initailize list datas and context.
 *
 * @param activity The current context
 * @param arrayListGalleryImg The list datas
 */(private val mActivity: Activity, private val arrayListGalleryImg: ArrayList<GalleryImagePojo>, private val aL_cameraImgPath: ArrayList<String>) : RecyclerView.Adapter<GalleryRvAdap.MyViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.single_row_gallery_images, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        if (arrayListGalleryImg[position].isSelected) {
            holder.itemViewVIew.setBackgroundResource(R.color.pink_color)
            holder.iV_tick_mark.visibility = View.VISIBLE
            holder.rL_tick_mark.setBackgroundResource(R.drawable.circle_with_pink_color_stroke_and_solid_shape)
        } else {
            holder.itemViewVIew.setBackgroundResource(R.color.transparant)
            holder.rL_tick_mark.setBackgroundResource(R.drawable.circle_with_white_color_stroke_and_solid_shape)
            holder.iV_tick_mark.visibility = View.GONE
        }
        val imageUrl = arrayListGalleryImg[position].galleryImagePath
        val rotationAngle = arrayListGalleryImg[position].rotationAngle
        println("$TAG rotaion angle=$rotationAngle")

        /*if (imageUrl!=null && !imageUrl.isEmpty())
            Picasso.with(mActivity)
                    .load("file://"+imageUrl)
                    .transform(new MyTransformation(mActivity, rotationAngle, String.valueOf(System.currentTimeMillis())), new FitCenter(mActivity))
                    .placeholder(R.drawable.default_image)
                    .error(R.drawable.default_image)
                    .into(holder.imageView);*/try {
            Glide.with(mActivity) //.load("file://"+path)
                    .load("file://$imageUrl")
                    .asBitmap()
                    .transform(MyTransformation(mActivity, rotationAngle.toFloat(), System.currentTimeMillis().toString()), FitCenter(mActivity))
                    .placeholder(R.drawable.default_image) // .fitCenter()
                    .into(holder.imageView)
        } catch (e: OutOfMemoryError) {
            e.printStackTrace()
        }
        holder.itemViewVIew.setOnClickListener {
            println(TAG + " " + "is item selected=" + arrayListGalleryImg[holder.adapterPosition].isSelected)
            if (arrayListGalleryImg[holder.adapterPosition].isSelected) {
                holder.itemViewVIew.setBackgroundResource(R.color.transparant)
                holder.rL_tick_mark.setBackgroundResource(R.drawable.circle_with_white_color_stroke_and_solid_shape)
                holder.iV_tick_mark.visibility = View.GONE
                arrayListGalleryImg[holder.adapterPosition].isSelected = false

                // horizontal images
                aL_cameraImgPath.remove(arrayListGalleryImg[holder.adapterPosition].galleryImagePath)
                (mActivity as GalleryActivity).imagesHorizontalRvAdap!!.notifyDataSetChanged()
            } else {
                if (aL_cameraImgPath.size < 5) {
                    holder.itemViewVIew.setBackgroundResource(R.color.pink_color)
                    holder.iV_tick_mark.visibility = View.VISIBLE
                    holder.rL_tick_mark.setBackgroundResource(R.drawable.circle_with_pink_color_stroke_and_solid_shape)
                    arrayListGalleryImg[holder.adapterPosition].isSelected = true

                    // horizontal images
                    aL_cameraImgPath.add(arrayListGalleryImg[holder.adapterPosition].galleryImagePath)
                    (mActivity as GalleryActivity).imagesHorizontalRvAdap!!.notifyDataSetChanged()
                } else CommonClass.showSnackbarMessage((mActivity as GalleryActivity).rL_rootview, mActivity.resources.getString(R.string.you_can_select_only_upto))
            }
        }
    }

    override fun getItemCount(): Int {
        return arrayListGalleryImg.size
    }

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val imageView: ImageView
        val iV_tick_mark: ImageView
         val itemViewVIew: View
        val rL_tick_mark: RelativeLayout

        init {
            imageView = view.findViewById<View>(R.id.iV_gallery_img) as ImageView
            imageView.layoutParams.height = CommonClass.getDeviceWidth(mActivity) / 3
            rL_tick_mark = view.findViewById<View>(R.id.rL_tick_mark) as RelativeLayout
            iV_tick_mark = view.findViewById<View>(R.id.iV_tick_mark) as ImageView
            itemViewVIew = view
        }
    }

    companion object {
        private val TAG = GalleryRvAdap::class.java.simpleName
    }

}