package com.zoptal.cellableapp.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.main.tab_fragments.HomeFrag
import com.zoptal.cellableapp.pojo_class.product_category.ProductCategoryResDatas
import java.util.*

/**
 * Created by ${3embed} on ${27-10-2017}.
 * Banglore
 */
class HomeFilterAdapter(private val homeFrag: HomeFrag, private val mActivity: Activity, private val aL_categoryDatas: ArrayList<ProductCategoryResDatas>) : RecyclerView.Adapter<HomeFilterAdapter.MyViewHolder>() {
    private val selectedColor: Int
    private val unSelectedColor: Int
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.single_row_cat_item_home_pg, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var name: String?
        val isFilterCateSelected: Boolean
        val seletedImage: String
        val unseletedImage: String
        name = aL_categoryDatas[position].name

        // make first character of character is uppercase
        if (name != null && !name.isEmpty()) name = name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase()
        //aL_categoryDatas.get(position).setName(name);
        seletedImage = aL_categoryDatas[position].activeimage
        unseletedImage = aL_categoryDatas[position].deactiveimage
        isFilterCateSelected = aL_categoryDatas[position].isSelected
        holder.tV_category.setTextColor(ContextCompat.getColor(mActivity, R.color.white))

        // set values like category image and name
        /* if (isFilterCateSelected)
            setCategoryImage(holder.iV_category, seletedImage, holder.tV_category, name, selectedColor);
        else
            setCategoryImage(holder.iV_category, unseletedImage, holder.tV_category, name, unSelectedColor);*/setCategoryImage(holder.iV_category, seletedImage, holder.tV_category, name, selectedColor)

        // To select or unselect category
        holder.view.setOnClickListener {
            val isViewSelected = aL_categoryDatas[holder.adapterPosition].isSelected
            if (!isViewSelected && HomeFrag.isClickableRv) {
                aL_categoryDatas[holder.adapterPosition].isSelected = true

                //set all others to unselected
                for (i in aL_categoryDatas.indices) {
                    if (i != holder.adapterPosition) aL_categoryDatas[i].isSelected = false
                }

                //setCategoryImage(holder.iV_category, seletedImage, holder.tV_category, aL_categoryDatas.get(holder.getAdapterPosition()).getName(), selectedColor);
                homeFrag.filterData
            } else if (HomeFrag.isClickableRv) {
                aL_categoryDatas[holder.adapterPosition].isSelected = false
                //setCategoryImage(holder.iV_category, unseletedImage, holder.tV_category, aL_categoryDatas.get(holder.getAdapterPosition()).getName(), unSelectedColor);
                homeFrag.filterData
            }
            notifyDataSetChanged()
        }
    }

    private fun setCategoryImage(imageView: ImageView, url: String?, tV_category: TextView, categoryName: String?, color: Int) {
        // set image
        /* if (url!=null && !url.isEmpty())
            Picasso.with(mActivity)
                    .load(url)
                    .placeholder(R.drawable.default_circle_img)
                    .error(R.drawable.default_circle_img)
                    .into(imageView);*/
        var categoryName = categoryName
        if (url != null && !url.isEmpty()) Glide.with(mActivity)
                .load(url) //.placeholder(R.drawable.default_circle_img)
                .error(R.drawable.default_circle_img)
                .into(imageView)

        // set category name
        if (categoryName != null) {
            // make first character of character is uppercase
            categoryName = categoryName.substring(0, 1).toUpperCase() + categoryName.substring(1).toLowerCase()
            tV_category.text = categoryName
            tV_category.setTextColor(color)
        }
    }

    override fun getItemCount(): Int {
        return aL_categoryDatas.size
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val iV_category: ImageView
        val tV_category: TextView
        val view: View

        init {
            iV_category = itemView.findViewById<View>(R.id.iV_category) as ImageView
            tV_category = itemView.findViewById<View>(R.id.tV_category) as TextView
            view = itemView
        }
    }

    init {
        selectedColor = ContextCompat.getColor(mActivity, R.color.white)
        unSelectedColor = ContextCompat.getColor(mActivity, R.color.white)
    }
}