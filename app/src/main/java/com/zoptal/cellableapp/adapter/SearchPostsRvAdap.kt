package com.zoptal.cellableapp.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.pojo_class.search_post_pojo.SearchPostDatas
import com.zoptal.cellableapp.utility.CircleTransform
import com.zoptal.cellableapp.utility.CommonClass
import com.zoptal.cellableapp.utility.ProductItemClickListener
import java.util.*

/**
 * Created by hello on 30-Jun-17.
 */
class SearchPostsRvAdap(private val mActivity: Activity, private val aL_searchedPosts: ArrayList<SearchPostDatas>) : RecyclerView.Adapter<SearchPostsRvAdap.MyViewHolder?>() {
    private var clickListener: ProductItemClickListener? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(mActivity).inflate(R.layout.single_row_search_product, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val productName: String?
        val productImage: String?
        val category: String?
        productName = aL_searchedPosts[position].productName
        productImage = aL_searchedPosts[position].mainUrl
        category = aL_searchedPosts[position].category

        // set Profile pic
        if (productImage != null && !productImage.isEmpty()) Picasso.with(mActivity)
                .load(productImage)
                .placeholder(R.drawable.default_circle_img)
                .error(R.drawable.default_circle_img)
                .transform(CircleTransform())
                .into(holder.image)

        // set product name
        if (productName != null && !productName.isEmpty()) holder.tV_heading.text = productName

        // set category
        if (category != null && !category.isEmpty()) holder.tV_subHeading.text = category
        ViewCompat.setTransitionName(holder.image, aL_searchedPosts[position].productName)
        holder.mView.setOnClickListener { if (clickListener != null) clickListener!!.onItemClick(holder.adapterPosition, holder.image) }
    }

    override fun getItemCount(): Int {
        return aL_searchedPosts.size
    }

    inner class MyViewHolder(var mView: View) : RecyclerView.ViewHolder(mView) {
        var image: ImageView
        var tV_heading: TextView
        var tV_subHeading: TextView

        init {
            image = mView.findViewById<View>(R.id.image) as ImageView
            image.layoutParams.width = CommonClass.getDeviceWidth(mActivity) / 7
            image.layoutParams.height = CommonClass.getDeviceWidth(mActivity) / 7
            tV_heading = mView.findViewById<View>(R.id.tV_heading) as TextView
            tV_subHeading = mView.findViewById<View>(R.id.tV_subHeading) as TextView
        }
    }

    fun setOnItemClick(listener: ProductItemClickListener?) {
        clickListener = listener
    }

}