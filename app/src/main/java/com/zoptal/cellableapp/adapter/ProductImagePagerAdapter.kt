package com.zoptal.cellableapp.adapter

import android.app.Activity
import android.content.Context
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.viewpager.widget.PagerAdapter
import com.squareup.picasso.Picasso
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.utility.CommonClass

/**
 * This is GetOrderActivity class
 *
 *
 * This class is getting called from ItemDetailsActivity class.
 * In this class we set paging for each image using PagerAdapter.
 *
 * @author 3embed
 * @version 1.0
 * @since 18/5/16
 */
class ProductImagePagerAdapter(private val context: Activity, image_array: Array<String>) : PagerAdapter() {
    private val layoutInflater: LayoutInflater

    //ArrayList<String> arrayList;
    private val image_array: Array<String>?
    override fun getCount(): Int {
        return image_array?.size ?: 0
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        println("instantiate..$container $position")
        val itemView = layoutInflater.inflate(R.layout.product_image_viewpager_layout, container, false)
        val imageView: ImageView
        imageView = itemView.findViewById<View>(R.id.viewPagerItem_image1) as ImageView
        if (image_array != null) if (Build.MANUFACTURER.equals("Samsung", ignoreCase = true)) {
            Picasso.with(context)
                    .load(image_array[position])
                    .resize(CommonClass.getDeviceWidth(context), CommonClass.getDeviceWidth(context) + CommonClass.getDeviceWidth(context) / 2)
                    .centerCrop()
                    .placeholder(R.drawable.default_image)
                    .error(R.drawable.default_image)
                    .into(imageView)
            container.addView(itemView)
        } else {
            Picasso.with(context)
                    .load(image_array[position]) //.resize(CommonClass.getDeviceWidth(context),CommonClass.getDeviceWidth(context))
                    .placeholder(R.drawable.default_image)
                    .error(R.drawable.default_image)
                    .into(imageView)
            container.addView(itemView)
        }
        return itemView
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as LinearLayout)
    }

    init {
        layoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        this.image_array = image_array
        println("images url..." + image_array.size)
    }
}