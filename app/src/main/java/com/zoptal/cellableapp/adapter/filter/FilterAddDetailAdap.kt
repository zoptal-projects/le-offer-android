package com.zoptal.cellableapp.adapter.filter

import android.app.Activity
import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.filter_view_holder.FilterEditTextViewHolder
import com.zoptal.cellableapp.filter_view_holder.FilterFromToViewHolder
import com.zoptal.cellableapp.filter_view_holder.MultipleSelectionViewHolder
import com.zoptal.cellableapp.main.activity.FilterActivity
import com.zoptal.cellableapp.pojo_class.filter.SendFilter
import com.zoptal.cellableapp.pojo_class.product_category.FilterData
import com.zoptal.cellableapp.utility.ClickListener
import com.zoptal.cellableapp.utility.CommonClass
import java.util.*

/**
 * Created by embed on 17/5/18.
 */
class FilterAddDetailAdap(private val mContext: Context, data: ArrayList<FilterData>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val mActivity: Activity
    private val data: ArrayList<FilterData>
    private val clickListener: ClickListener
    private var multipleSelectionAdap: FilterMultipleSelectionAdap? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view: View
        return when (viewType) {
            VIEW_TEXBOX -> {
                view = LayoutInflater.from(mContext).inflate(R.layout.filter_view_type_edit_text, parent, false)
                FilterEditTextViewHolder(view)
            }
            VIEW_MULTIPLE -> {
                view = LayoutInflater.from(mContext).inflate(R.layout.filter_view_type_multiple_selection, parent, false)
                MultipleSelectionViewHolder(view)
            }
            VIEW_FROM_TO -> {
                view = LayoutInflater.from(mContext).inflate(R.layout.filter_view_type_from_to, parent, false)
                FilterFromToViewHolder(view)
            }
            else -> {
                view = LayoutInflater.from(mContext).inflate(R.layout.filter_view_type_edit_text, parent, false)
                FilterEditTextViewHolder(view)
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (getItemViewType(position)) {
            VIEW_TEXBOX -> {
                val eTHolder = holder as FilterEditTextViewHolder
                val name1 = CommonClass.getFirstCaps(data[position].fieldName)
                eTHolder.tV_fieldName.text = name1
                //eTHolder.eT_fieldValue.setHint(data.get(position).getFieldName());

                // if already value in sendFilter list of filterActivity then set
                for (s in (mContext as FilterActivity).sendFilters) {
                    if (s.filedName == data[position].fieldName) {
                        eTHolder.eT_fieldValue.setText(s.value)
                    }
                }
                eTHolder.eT_fieldValue.addTextChangedListener(object : TextWatcher {
                    override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
                    override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
                    override fun afterTextChanged(editable: Editable) {
                        addValueSendFilterList(eTHolder.eT_fieldValue.text.toString().trim { it <= ' ' }, eTHolder.getAdapterPosition())
                    }
                })
            }
            VIEW_MULTIPLE -> {
                val msHolder = holder as MultipleSelectionViewHolder
                val name2 = CommonClass.getFirstCaps(data[position].fieldName)
                msHolder.tV_fieldName.text = name2
                multipleSelectionAdap = FilterMultipleSelectionAdap(mContext, getArrayListFromValues(data[position].values!!), data[position].fieldName!!)

                // if already selcted data in sendFilter list of filterActivity then make it selected
                for (s in (mContext as FilterActivity).sendFilters) {
                    if (s.filedName == data[position].fieldName) {
                        multipleSelectionAdap!!.selectedData.add(s.value!!)
                    }
                }
                msHolder.rV_multiple.setLayoutManager(LinearLayoutManager(mContext))
                msHolder.rV_multiple.setAdapter(multipleSelectionAdap)
            }
            VIEW_FROM_TO -> {
                val fromToHolder = holder as FilterFromToViewHolder
                val name3 = CommonClass.getFirstCaps(data[position].fieldName)
                fromToHolder.tV_fieldName.text = name3

                // if already value in sendFilter list of filterActivity then set
                for (s in (mContext as FilterActivity).sendFilters) {
                    if (s.filedName == data[position].fieldName) {
                        fromToHolder.eT_fromValue.setText(s.from)
                        fromToHolder.eT_toValue.setText(s.to)
                    }
                }
                fromToHolder.eT_fromValue.addTextChangedListener(object : TextWatcher {
                    override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
                    override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
                    override fun afterTextChanged(editable: Editable) {
                        addFromValueInSendFilter(fromToHolder.eT_fromValue.text.toString().trim { it <= ' ' }, fromToHolder.getAdapterPosition())
                    }
                })
                fromToHolder.eT_toValue.addTextChangedListener(object : TextWatcher {
                    override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
                    override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
                    override fun afterTextChanged(editable: Editable) {
                        addToValueInSendFilter(fromToHolder.eT_toValue.text.toString().trim { it <= ' ' }, fromToHolder.getAdapterPosition())
                    }
                })
            }
            else -> {
            }
        }
    }

    /**
     * 1 : textbox
     * 2 : checkbox
     * 3 : slider
     * 4 : radio button
     * 5 : range
     * 6 : drop down
     * 7 : date
     */
    override fun getItemViewType(position: Int): Int {
        return when (data[position].type) {
            1 -> VIEW_TEXBOX
            2 -> VIEW_MULTIPLE
            3 -> VIEW_FROM_TO
            4 -> VIEW_MULTIPLE
            5 -> VIEW_FROM_TO
            6 -> VIEW_MULTIPLE
            7 -> VIEW_TEXBOX
            else -> VIEW_TEXBOX
        }
    }

    override fun getItemCount(): Int {
        return data.size
    }

    // split the values from values tag in list
    private fun getArrayListFromValues(values: String): ArrayList<String> {
        val arrayList = ArrayList<String>()
        val `val` = values.split(",".toRegex()).toTypedArray()
        arrayList.addAll(Arrays.asList(*`val`))
        return arrayList
    }

    private fun addValueSendFilterList(value: String, pos: Int) {
        //if added is become true
        var added = false

        // list is empty then add new
        // else check is contains same key then added values to perticular key
        if ((mContext as FilterActivity).sendFilters.size > 0) {
            for (i in (mContext as FilterActivity).sendFilters.indices) {
                if ((mContext as FilterActivity).sendFilters[i].filedName.equals(data[pos].fieldName, ignoreCase = true)) {
                    (mContext as FilterActivity).sendFilters[i].value = value
                    added = true
                }
            }
        } else {
            added = true
            (mContext as FilterActivity).sendFilters.add(SendFilter("equlTo", data[pos].fieldName!!, value))
        }

        // if list not contain same key then add new
        if (!added) {
            (mContext as FilterActivity).sendFilters.add(SendFilter("equlTo", data[pos].fieldName!!, value))
        }
        if (value.isEmpty()) {
            for (i in (mContext as FilterActivity).sendFilters.indices) {
                if ((mContext as FilterActivity).sendFilters[i].filedName.equals(data[pos].fieldName, ignoreCase = true)) {
                    (mContext as FilterActivity).sendFilters.removeAt(i)
                }
            }
        }

        //print to check
        for (f in (mContext as FilterActivity).sendFilters) {
            println("entered values=" + f.filedName + "," + f.value)
        }
    }

    private fun addFromValueInSendFilter(value: String, pos: Int) {
        //if added is become true
        var added = false

        // list is empty then add new
        // else check is contains same key then added values to perticular key
        if ((mContext as FilterActivity).sendFilters.size > 0) {
            for (i in (mContext as FilterActivity).sendFilters.indices) {
                if ((mContext as FilterActivity).sendFilters[i].filedName.equals(data[pos].fieldName, ignoreCase = true)) {
                    (mContext as FilterActivity).sendFilters[i].from = value
                    added = true
                }
            }
        } else {
            added = true
            (mContext as FilterActivity).sendFilters.add(SendFilter("range", data[pos].fieldName!!, value, ""))
        }

        // if list not contain same key then add new
        if (!added) {
            (mContext as FilterActivity).sendFilters.add(SendFilter("range", data[pos].fieldName!!, value, ""))
        }

        //print to check
        for (f in (mContext as FilterActivity).sendFilters) {
            println("from to values = " + f.filedName + ":" + f.from + "," + f.to)
        }
    }

    private fun addToValueInSendFilter(value: String, pos: Int) {
        //if added is become true
        var added = false

        // list is empty then add new
        // else check is contains same key then added values to perticular key
        if ((mContext as FilterActivity).sendFilters.size > 0) {
            for (i in (mContext as FilterActivity).sendFilters.indices) {
                if ((mContext as FilterActivity).sendFilters[i].filedName.equals(data[pos].fieldName, ignoreCase = true)) {
                    (mContext as FilterActivity).sendFilters[i].to = value
                    added = true
                }
            }
        } else {
            added = true
            (mContext as FilterActivity).sendFilters.add(SendFilter("range", data[pos].fieldName!!, "", value))
        }

        // if list not contain same key then add new
        if (!added) {
            (mContext as FilterActivity).sendFilters.add(SendFilter("range", data[pos].fieldName!!, "", value))
        }

        //print to check
        for (f in (mContext as FilterActivity).sendFilters) {
            println("from to values = " + f.filedName + ":" + f.from + "," + f.to)
        }
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val tV_fieldName: TextView
        private val eT_fieldValue: EditText

        init {
            tV_fieldName = itemView.findViewById<View>(R.id.tV_fieldName) as TextView
            eT_fieldValue = itemView.findViewById<View>(R.id.eT_fieldValue) as EditText
        }
    }

    companion object {
        private const val VIEW_TEXBOX = 1
        private const val VIEW_FROM_TO = 2
        private const val VIEW_MULTIPLE = 3
        private val TAG = FilterAddDetailAdap::class.java.simpleName
    }

    init {
        mActivity = mContext as Activity
        this.data = data
        clickListener = mContext as ClickListener
    }
}