package com.zoptal.cellableapp.adapter

import android.app.Activity
import android.content.Intent

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.FitCenter
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.Uploader.ProductImageDatas
import com.zoptal.cellableapp.main.activity.UpdatePostActivity
import com.zoptal.cellableapp.main.camera_kit.CameraKitActivity
import com.zoptal.cellableapp.recyleview_drag_drop.ItemTouchHelperAdapter
import com.zoptal.cellableapp.recyleview_drag_drop.ItemTouchHelperViewHolder
import com.zoptal.cellableapp.recyleview_drag_drop.OnCustomerListChangedListener
import com.zoptal.cellableapp.recyleview_drag_drop.OnStartDragListener
import com.zoptal.cellableapp.utility.CommonClass
import com.zoptal.cellableapp.utility.MyTransformation
import com.zoptal.cellableapp.utility.VariableConstants
import java.util.*

/**
 * <h>HomeFragRvAdapter</h>
 *
 *
 * In class is called from UpdatePostActivity class. In this recyclerview adapter class we used to inflate
 * single_row_post_images layout and shows the all Images.
 *
 * @since 29-Aug-17
 */
class UpdatePostRvAdapter
/**
 * <h>CurrencyRvAdap</h>
 *
 *
 * This is simple constructor to initailize list datas and context.
 *
 * @param mActivity The current context
 * @param aLProductImageDatases The list datas
 */(private val mActivity: Activity, private val aLProductImageDatases: ArrayList<ProductImageDatas?>, private val aLUpdateProductImage: ArrayList<ProductImageDatas?>, private val tV_add_more_image: TextView, private val mDragStartListener: OnStartDragListener,
    private val mListChangedListener: OnCustomerListChangedListener) : RecyclerView.Adapter<RecyclerView.ViewHolder?>(), ItemTouchHelperAdapter {

    /**
     * <h>OnCreateViewHolder</h>
     *
     *
     * In this method The adapter prepares the layout of the items by inflating the correct
     * layout for the individual data elements.
     *
     * @param parent A ViewGroup is a special view that can contain other views (called children.)
     * @param viewType Within the getItemViewType method the recycler view determines which type should be used for data.
     * @return It returns an object of type ViewHolder per visual entry in the recycler view.
     */
   override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view: View
        when (viewType) {
            TYPE_ITEM -> {
                view = LayoutInflater.from(mActivity).inflate(R.layout.single_row_post_images, parent, false)
                return MyViewHolder(view)
            }
            TYPE_FOOTER -> {
                view = LayoutInflater.from(mActivity).inflate(R.layout.footer_post_product_images, parent, false)
                return FooterViewHolder(view)
            }
        }
        return null!!
    }

    /**
     * <h>OnBindViewHolder</h>
     *
     *
     * In this method Every visible entry in a recycler view is filled with the
     * correct data model item by the adapter. Once a data item becomes visible,
     * the adapter assigns this data to the individual widgets which he inflated
     * earlier.
     *
     * @param holder The referece of MyViewHolder class of current class.
     * @param position The position of particular item
     */
   override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        // Image
        if (holder is MyViewHolder) {
            val myViewHolder = holder as MyViewHolder
            val path = aLProductImageDatases[position]!!.mainUrl
            val rotationAngle = aLProductImageDatases[position]!!.rotationAngle
            println(TAG + " " + "image url=" + aLProductImageDatases[position]!!.isImageUrl + " " + "isImageUrl=" + aLProductImageDatases[position]!!.isImageUrl)
            if (path != null && !path.isEmpty()) {
                /*if (aLProductImageDatases.get(position).isImageUrl())
                    Picasso.with(mActivity)
                            .load(path)
                            .placeholder(R.drawable.default_image)
                            .error(R.drawable.default_image)
                            .into(myViewHolder.iV_captured_img);
                else
                    Picasso.with(mActivity)
                            .load("file://"+path)
                            .placeholder(R.drawable.default_image)
                            .error(R.drawable.default_image)
                            .into(myViewHolder.iV_captured_img);*/
                Glide.with(mActivity)
                        .load(path)
                        .asBitmap()
                        .transform(MyTransformation(mActivity, rotationAngle.toFloat(), System.currentTimeMillis().toString()), FitCenter(mActivity))
                        .placeholder(R.drawable.default_image)
                        .into(myViewHolder.iV_captured_img)
            }
            when (aLProductImageDatases.size) {
                0 -> tV_add_more_image.text = mActivity.resources.getString(R.string.at_least_one_image_is)
                1 -> tV_add_more_image.text = mActivity.resources.getString(R.string.add_upto_4_more_img)
                2 -> tV_add_more_image.text = mActivity.resources.getString(R.string.add_upto_3_more_img)
                3 -> tV_add_more_image.text = mActivity.resources.getString(R.string.add_upto_2_more_img)
                4 -> tV_add_more_image.text = mActivity.resources.getString(R.string.add_upto_1_more_img)
                5 -> tV_add_more_image.visibility = View.GONE
                else -> if (aLProductImageDatases.size > 5) tV_add_more_image.visibility = View.GONE
            }
            // remove image
            myViewHolder.iV_deleteImg.setOnClickListener {
                (mActivity as UpdatePostActivity).removedNewAddedPhotos(aLProductImageDatases[holder.getAdapterPosition()]!!.mainUrl!!)
                aLProductImageDatases.removeAt(holder.getAdapterPosition())
                if (aLUpdateProductImage.size > position) {
                    aLUpdateProductImage.removeAt(holder.getAdapterPosition())
                }
                notifyDataSetChanged()
                tV_add_more_image.visibility = View.VISIBLE
                when (aLProductImageDatases.size) {
                    0 -> tV_add_more_image.text = mActivity.resources.getString(R.string.at_least_one_image_is)
                    1 -> tV_add_more_image.text = mActivity.resources.getString(R.string.add_upto_4_more_img)
                    2 -> tV_add_more_image.text = mActivity.resources.getString(R.string.add_upto_3_more_img)
                    3 -> tV_add_more_image.text = mActivity.resources.getString(R.string.add_upto_2_more_img)
                    4 -> tV_add_more_image.text = mActivity.resources.getString(R.string.add_upto_1_more_img)
                    5 -> tV_add_more_image.visibility = View.GONE
                }
            }
            holder.itemView.setOnLongClickListener(View.OnLongClickListener {
                mDragStartListener.onStartDrag(holder)
                false
            })
        }

        // footer
        if (holder is FooterViewHolder) {
            val footerViewHolder = holder as FooterViewHolder
            footerViewHolder.iV_add_image.setOnClickListener {
                val cameraIntent: Intent
                /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                        {
                            cameraIntent=new Intent(mActivity, Camera2Activity.class);
                            cameraIntent.putExtra("isUpdatePost",true);
                            mActivity.startActivityForResult(cameraIntent, VariableConstants.UPDATE_IMAGE_REQ_CODE);
                        }
                        else
                        {*/cameraIntent = Intent(mActivity, CameraKitActivity::class.java)
                cameraIntent.putExtra("isUpdatePost", true)
                mActivity.startActivityForResult(cameraIntent, VariableConstants.UPDATE_IMAGE_REQ_CODE)
                //}
            }
        }
    }

   override fun getItemViewType(position: Int): Int {
        return if (aLProductImageDatases.size < 5) {
            if (isPositionFooter(position)) {
                TYPE_FOOTER
            } else TYPE_ITEM
        } else TYPE_ITEM
    }

    private fun isPositionFooter(position: Int): Boolean {
        return position == aLProductImageDatases.size
    }

    override fun getItemCount(): Int {
       return if (aLProductImageDatases.size < 5) aLProductImageDatases.size + 1 else if (aLProductImageDatases.size > 5) {
           removeOtherImage()
           5
       } else aLProductImageDatases.size
    }


    private fun removeOtherImage() {
        for (imageCount in aLProductImageDatases.indices) {
            if (imageCount > 4) aLProductImageDatases.removeAt(imageCount)
        }
    }

    override fun onItemMove(fromPosition: Int, toPosition: Int) {
        if (getItemViewType(toPosition) == TYPE_ITEM) {
            Collections.swap(aLProductImageDatases, fromPosition, toPosition)
            mListChangedListener.onUpdateImagesListChanged(aLProductImageDatases)
            notifyItemMoved(fromPosition, toPosition)
        }
    }

    override fun onItemDismiss(position: Int) {}
    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), ItemTouchHelperViewHolder {
        var iV_captured_img: ImageView
        var iV_deleteImg: ImageView
        override fun onItemSelected() {
            itemView.setBackgroundColor(0)
        }

        override fun onItemClear() {
            itemView.setBackgroundColor(0)
        }

        init {
            iV_captured_img = itemView.findViewById<View>(R.id.iV_image) as ImageView
            iV_captured_img.layoutParams.width = CommonClass.getDeviceWidth(mActivity) / 4
            iV_captured_img.layoutParams.height = CommonClass.getDeviceWidth(mActivity) / 4
            iV_deleteImg = itemView.findViewById<View>(R.id.iV_delete_icon) as ImageView
            iV_deleteImg.visibility = View.VISIBLE
        }
    }

    private inner class FooterViewHolder internal constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var iV_add_image: ImageView

        init {
            iV_add_image = itemView.findViewById<View>(R.id.iV_add_image) as ImageView
            iV_add_image.layoutParams.width = CommonClass.getDeviceWidth(mActivity) / 4
            iV_add_image.layoutParams.height = CommonClass.getDeviceWidth(mActivity) / 4
        }
    }

    companion object {
        private val TAG = UpdatePostRvAdapter::class.java.simpleName
        private const val TYPE_ITEM = 1
        private const val TYPE_FOOTER = 2
    }

}