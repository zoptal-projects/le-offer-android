package com.zoptal.cellableapp.adapter

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable

import android.text.SpannableString
import android.text.Spanned
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.event_bus.EventBusDatasHandler
import com.zoptal.cellableapp.main.activity.RateUserActivity
import com.zoptal.cellableapp.main.activity.SelfProfileActivity
import com.zoptal.cellableapp.main.activity.UserProfileActivity
import com.zoptal.cellableapp.main.activity.products.ProductDetailsActivity
import com.zoptal.cellableapp.pojo_class.UserPostDataPojo
import com.zoptal.cellableapp.pojo_class.explore_for_you_pojo.ForYouResposeDatas
import com.zoptal.cellableapp.utility.*
import java.util.*

/**
 * <h>YouFragRvAdapter</h>
 *
 *
 * In class is called from YouFrag. In this recyclerview adapter class we used to inflate
 * single_row_images layout and shows the all post posted by logged-in user.
 *
 * @since 4/17/2017
 */
class YouFragRvAdapter(private val mActivity: Activity, private val al_selfActivity_data: ArrayList<ForYouResposeDatas>) : RecyclerView.Adapter<YouFragRvAdapter.MyViewHolder?>() {
    private val mApiCall: ApiCall
    private val mSessionManager: SessionManager
    private val mEventBusDatasHandler: EventBusDatasHandler

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(mActivity).inflate(R.layout.single_row_following_activity, parent, false)
        return MyViewHolder(view)
    }

   override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val membername: String
        val username: String
        val notificationType: String?
        val thumbnailImageUrl: String
        val memberProfilePicUrl: String?
        val setNotificationMessage: String
        val postId: String
        val productName: String
        var createdOn: String?
        val followRequestStatus: String?
        username = al_selfActivity_data[position].username
        membername = al_selfActivity_data[position].membername
        createdOn = al_selfActivity_data[position].createdOn
        notificationType = al_selfActivity_data[position].notificationType
        thumbnailImageUrl = al_selfActivity_data[position].thumbnailImageUrl
        memberProfilePicUrl = al_selfActivity_data[position].memberProfilePicUrl
        postId = al_selfActivity_data[position].postId
        followRequestStatus = al_selfActivity_data[position].followRequestStatus
        productName = al_selfActivity_data[position].productName

        // set member Profile Pic
        if (memberProfilePicUrl != null && !memberProfilePicUrl.isEmpty()) Picasso.with(mActivity)
                .load(memberProfilePicUrl)
                .transform(CircleTransform())
                .placeholder(R.drawable.default_circle_img)
                .error(R.drawable.default_circle_img)
                .into(holder.iV_user1_profilePic)
        holder.iV_user1_profilePic.setOnClickListener { openUserProfileScreen(membername) }
        val arrayListPostData = al_selfActivity_data[position].postData

        // set message according to notification Type
        if (notificationType != null) {
            when (notificationType) {
                "2" -> {
                    holder.iV_user2_profilePic.visibility = View.VISIBLE
                    holder.relative_follow.visibility = View.GONE
                    holder.relative_rate_user.visibility = View.GONE
                    setThumbnailImage(holder.iV_user2_profilePic, thumbnailImageUrl, arrayListPostData)
                    setNotificationMessage = mActivity.resources.getString(R.string.liked_your_post)
                    setActivityMessage(holder.tV_activity, membername, setNotificationMessage)
                    openProduct(holder.itemView, thumbnailImageUrl, postId, username, false, arrayListPostData)
                }
                "3" -> {
                    holder.iV_user2_profilePic.visibility = View.GONE
                    holder.relative_rate_user.visibility = View.GONE
                    holder.relative_follow.visibility = View.VISIBLE
                    setNotificationMessage = mActivity.resources.getString(R.string.startedFollowingYou)
                    setActivityMessage(holder.tV_activity, membername, setNotificationMessage)
                }
                "5" -> {
                    holder.iV_user2_profilePic.visibility = View.VISIBLE
                    holder.relative_follow.visibility = View.GONE
                    holder.relative_rate_user.visibility = View.GONE
                    setThumbnailImage(holder.iV_user2_profilePic, thumbnailImageUrl, arrayListPostData)
                    setNotificationMessage = mActivity.resources.getString(R.string.commented_your_post)
                    setActivityMessage(holder.tV_activity, membername, setNotificationMessage)
                    openProduct(holder.itemView, thumbnailImageUrl, postId, username, true, arrayListPostData)
                }
                "6" -> {
                    holder.iV_user2_profilePic.visibility = View.VISIBLE
                    holder.relative_follow.visibility = View.GONE
                    holder.relative_rate_user.visibility = View.GONE
                    setThumbnailImage(holder.iV_user2_profilePic, thumbnailImageUrl, arrayListPostData)
                    setNotificationMessage = mActivity.resources.getString(R.string.send_a_offer_on_your_product)
                    setActivityMessage(holder.tV_activity, membername, setNotificationMessage)
                    openProduct(holder.itemView, thumbnailImageUrl, postId, username, false, arrayListPostData)
                }
                "8" -> {
                    holder.relative_follow.visibility = View.GONE
                    holder.iV_user2_profilePic.visibility = View.GONE
                    holder.relative_rate_user.visibility = View.VISIBLE
                    setRateUserMessage(holder.tV_activity, productName, postId, membername, username)
                    holder.relative_rate_user.setOnClickListener {
                        val intent = Intent(mActivity, RateUserActivity::class.java)
                        intent.putExtra("userName", membername)
                        intent.putExtra("userImage", memberProfilePicUrl)
                        intent.putExtra("postId", postId)
                        intent.putExtra("isFromNotification", true)
                        mActivity.startActivityForResult(intent, VariableConstants.RATE_USER_REQ_CODE)
                    }
                }
            }
        }

        // set follow or following status
        if (followRequestStatus != null && !followRequestStatus.isEmpty()) {
            holder.relative_follow.setBackgroundResource(R.drawable.rect_purple_color_with_solid_shape)
            holder.tV_follow.setTextColor(ContextCompat.getColor(mActivity, R.color.white))
            holder.tV_follow.text = mActivity.resources.getString(R.string.Following)
        } else {
            holder.relative_follow.setBackgroundResource(R.drawable.rect_purple_color_with_stroke_shape)
            holder.tV_follow.setTextColor(ContextCompat.getColor(mActivity, R.color.purple_color))
            holder.tV_follow.text = mActivity.resources.getString(R.string.follow)
        }


        // Follow
        holder.relative_follow.setOnClickListener {
            val url: String
            assert(notificationType != null)
            if (notificationType == "3") {
                val followStatus = al_selfActivity_data[holder.getAdapterPosition()].followRequestStatus
                if (followStatus == null || followStatus.isEmpty()) {
                    al_selfActivity_data[holder.getAdapterPosition()].followRequestStatus = "1"
                    holder.relative_follow.setBackgroundResource(R.drawable.rect_purple_color_with_solid_shape)
                    holder.tV_follow.setTextColor(ContextCompat.getColor(mActivity, R.color.white))
                    holder.tV_follow.text = mActivity.resources.getString(R.string.Following)
                    url = ApiUrl.FOLLOW + membername
                    if (arrayListPostData != null && arrayListPostData.size > 0) mEventBusDatasHandler.setSocialDatasFromDiscovery(username, memberProfilePicUrl, arrayListPostData[0], true)
                    mApiCall.followUserApi(url)
                } else {
                    unfollowUserAlert(membername, memberProfilePicUrl, holder.relative_follow, holder.tV_follow, holder.getAdapterPosition())
                }
            }
        }
        if (createdOn != null && !createdOn.isEmpty()) {
            createdOn = CommonClass.getTimeDifference(createdOn)
            holder.tV_time.text = createdOn
        }
    }

    /**
     * <h>OpenProduct</h>
     *
     *
     * In this method we used to open a product details.
     *
     * @param view whole notification view
     * @param productPic The item image
     * @param postId The post id of the product
     */
    private fun openProduct(view: View, productPic: String, postId: String?, username: String, isFromNotificationReview: Boolean, userPostDataPojos: ArrayList<UserPostDataPojo>?) {
        view.setOnClickListener {
            if (postId != null && !postId.isEmpty()) {
                val intent = Intent(mActivity, ProductDetailsActivity::class.java)
                intent.putExtra("image", productPic)
                intent.putExtra("postId", postId)
                intent.putExtra("postedByUserName", username)
                intent.putExtra("isFromReview", isFromNotificationReview)
                mActivity.startActivity(intent)
            } else {
                val intent = Intent(mActivity, ProductDetailsActivity::class.java)
                intent.putExtra("image", userPostDataPojos!![0].thumbnailImageUrl)
                intent.putExtra("postId", userPostDataPojos[0].postId)
                intent.putExtra("postedByUserName", username)
                intent.putExtra("isFromReview", isFromNotificationReview)
                mActivity.startActivity(intent)
            }
        }
    }

    /**
     * <h>SetThumbnailImage</h>
     *
     *
     * In this method we used to set the right side rectangular image.
     *
     * @param iV_user2_profilePic The right side image view.
     * @param thumbnailImageUrl The image url of the image
     */
    private fun setThumbnailImage(iV_user2_profilePic: ImageView, thumbnailImageUrl: String?, arrayListPostData: ArrayList<UserPostDataPojo>?) {
        if (thumbnailImageUrl != null && !thumbnailImageUrl.isEmpty()) Picasso.with(mActivity)
                .load(thumbnailImageUrl)
                .placeholder(R.drawable.default_image)
                .error(R.drawable.default_image)
                .into(iV_user2_profilePic) else if (arrayListPostData != null && !arrayListPostData.isEmpty()) {
            Picasso.with(mActivity)
                    .load(arrayListPostData[0].thumbnailImageUrl)
                    .error(R.drawable.default_image)
                    .placeholder(R.drawable.default_image)
                    .into(iV_user2_profilePic)
        } else {
            iV_user2_profilePic.setImageDrawable(mActivity.resources.getDrawable(R.drawable.default_image))
        }
    }

    /**
     * <h>SetActivityMessage</h>
     *
     *
     * In this method we use SpannableString to make clickable part-wise on TextView.
     *
     * @param tV_activity The TextView on which operation occured
     * @param membername The first user name
     * @param notificationMessage The message like started following, Liked Post etc
     */
    private fun setActivityMessage(tV_activity: TextView, membername: String, notificationMessage: String) {
        val setNotificationMessage = "$membername $notificationMessage"
        val spannableString = SpannableString(setNotificationMessage)
        spannableString.setSpan(MyClickableSpan(membername), 0, membername.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        tV_activity.text = spannableString
        tV_activity.movementMethod = LinkMovementMethod.getInstance()
        tV_activity.highlightColor = Color.TRANSPARENT
    }

    /**
     * <h>SetRateUserMessage</h>
     *
     *
     * In this method we used to set notification message for notification type 8 i.e(When any user rate your product)
     *
     * @param tV_activity The TextView where we set complete notification message
     * @param productName The product name of the Item.
     * @param postId The post Id the product.
     * @param membername The username
     */
    private fun setRateUserMessage(tV_activity: TextView, productName: String, postId: String, membername: String, userName: String) {
        val s1 = mActivity.resources.getString(R.string.you_just_bought)
        val s3 = mActivity.resources.getString(R.string.from)
        val s5 = mActivity.resources.getString(R.string.pls_rate_your_exp_with)
        val message = "$s1 $productName $s3 $membername $s5"
        val spannableString = SpannableString(message)
        spannableString.setSpan(RateUserClickableSpan(productName, postId, "", userName), s1.length + 1, s1.length + productName.length + 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        spannableString.setSpan(RateUserClickableSpan("", "", membername, userName), s1.length + 1 + productName.length + 1 + s3.length + 1, s1.length + 1 + productName.length + 1 + s3.length + 1 + membername.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        tV_activity.text = spannableString
        tV_activity.movementMethod = LinkMovementMethod.getInstance()
        tV_activity.highlightColor = Color.TRANSPARENT
    }

    /**
     * <h>MyClickableSpan</h>
     *
     *
     * This class extends ClickableSpan from that we have two overrided method
     * 1> onClick() in which we do clickable oparation
     * 2> updateDrawState() in this method we do operation like change TextView Style
     *
     */
    private inner class MyClickableSpan internal constructor(private val membername: String) : ClickableSpan() {
        override fun onClick(widget: View) {
            openUserProfileScreen(membername)
        }

        override fun updateDrawState(ds: TextPaint) {
            ds.isFakeBoldText = true
            ds.isUnderlineText = false
            ds.isFakeBoldText = true
        }

    }

    /**
     * <h>RateUserClickableSpan</h>
     *
     *
     * This class extends ClickableSpan from that we have two overrided method
     * 1> onClick() in which we do clickable oparation
     * 2> updateDrawState() in this method we do operation like change TextView Style
     *
     */
    private inner class RateUserClickableSpan internal constructor(productName: String, postId: String, membername: String, username: String) : ClickableSpan() {
        private var productName = ""
        private var postId = ""
        private var membername = ""
        private var username = ""
        override fun onClick(widget: View) {
            val intent: Intent
            if (!productName.isEmpty()) {
                intent = Intent(mActivity, ProductDetailsActivity::class.java)
                intent.putExtra("postId", postId)
                intent.putExtra("productName", productName)
                intent.putExtra("postedByUserName", username)
                mActivity.startActivity(intent)
            } else {
                openUserProfileScreen(membername)
            }
        }

        override fun updateDrawState(ds: TextPaint) {
            ds.isFakeBoldText = true
            ds.isUnderlineText = false
            ds.isFakeBoldText = true
        }

        init {
            this.productName = productName
            this.postId = postId
            this.membername = membername
            this.username = username
        }
    }

    private fun openUserProfileScreen(membername: String) {
        val intent: Intent
        if (mSessionManager.isUserLoggedIn && mSessionManager.userName == membername) {
            intent = Intent(mActivity, SelfProfileActivity::class.java)
            intent.putExtra("membername", membername)
        } else {
            intent = Intent(mActivity, UserProfileActivity::class.java)
            intent.putExtra("membername", membername)
        }
        mActivity.startActivity(intent)
    }

    override fun getItemCount(): Int {
        return al_selfActivity_data.size
    }


    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val iV_user1_profilePic: ImageView
        val iV_user2_profilePic: ImageView
        val tV_activity: TextView
        val tV_time: TextView
        val tV_follow: TextView
        val relative_follow: RelativeLayout
        val relative_rate_user: RelativeLayout

        init {
            iV_user1_profilePic = itemView.findViewById<View>(R.id.iV_user1_profilePic) as ImageView
            iV_user2_profilePic = itemView.findViewById<View>(R.id.iV_user2_profilePic) as ImageView
            iV_user2_profilePic.layoutParams.height = CommonClass.getDeviceWidth(mActivity) / 6
            iV_user2_profilePic.layoutParams.width = CommonClass.getDeviceWidth(mActivity) / 6
            tV_activity = itemView.findViewById<View>(R.id.tV_activity) as TextView
            tV_time = itemView.findViewById<View>(R.id.tV_time) as TextView
            relative_follow = itemView.findViewById<View>(R.id.relative_follow) as RelativeLayout
            relative_rate_user = itemView.findViewById<View>(R.id.relative_rate_user) as RelativeLayout
            tV_follow = itemView.findViewById<View>(R.id.tV_follow) as TextView
        }
    }

    /**
     * <h>unfollowUserAlert</h>
     *
     *
     * In this method we used to open a simple dialog pop-up to show
     * alert to unfollow
     *
     */
    private fun unfollowUserAlert(membername: String?, memberProfilePicUrl: String?, rL_follow: RelativeLayout, tV_follow: TextView, position: Int) {
        val unfollowUserDialog = Dialog(mActivity)
        unfollowUserDialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        unfollowUserDialog.setContentView(R.layout.dialog_unfollow_user)
        unfollowUserDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        unfollowUserDialog.window!!.setLayout((CommonClass.getDeviceWidth(mActivity) * 0.9).toInt(), RelativeLayout.LayoutParams.WRAP_CONTENT)

        // set user pic
        val imageViewPic = unfollowUserDialog.findViewById<View>(R.id.iV_userPic) as ImageView
        imageViewPic.layoutParams.width = CommonClass.getDeviceWidth(mActivity) / 5
        imageViewPic.layoutParams.height = CommonClass.getDeviceWidth(mActivity) / 5

        // posted by pic
        if (memberProfilePicUrl != null && !memberProfilePicUrl.isEmpty()) Picasso.with(mActivity)
                .load(memberProfilePicUrl)
                .transform(CircleTransform())
                .placeholder(R.drawable.default_profile_image)
                .error(R.drawable.default_profile_image)
                .into(imageViewPic)

        // set user name
        val tV_userName = unfollowUserDialog.findViewById<View>(R.id.tV_userName) as TextView
        if (membername != null && !membername.isEmpty()) {
            val setUserName = mActivity.resources.getString(R.string.at_the_rate) + membername + mActivity.resources.getString(R.string.question_mark)
            tV_userName.text = setUserName
        }

        // set cancel button
        val tV_cancel = unfollowUserDialog.findViewById<View>(R.id.tV_cancel) as TextView
        tV_cancel.setOnClickListener { unfollowUserDialog.dismiss() }

        // set done button
        val tV_unfollow = unfollowUserDialog.findViewById<View>(R.id.tV_unfollow) as TextView
        tV_unfollow.setOnClickListener {
            val url = ApiUrl.UNFOLLOW + membername
            mApiCall.followUserApi(url)
            rL_follow.setBackgroundResource(R.drawable.rect_purple_color_with_stroke_shape)
            tV_follow.setTextColor(ContextCompat.getColor(mActivity, R.color.purple_color))
            tV_follow.text = mActivity.resources.getString(R.string.follow)
            al_selfActivity_data[position].followRequestStatus = "0"
            if (al_selfActivity_data[position].postData != null && al_selfActivity_data[position].postData!!.size > 0) mEventBusDatasHandler.setSocialDatasFromDiscovery(membername, memberProfilePicUrl, al_selfActivity_data[position].postData!![0], false)
            unfollowUserDialog.dismiss()
        }
        unfollowUserDialog.show()
    }

    init {
        mApiCall = ApiCall(mActivity)
        mSessionManager = SessionManager(mActivity)
        mEventBusDatasHandler = EventBusDatasHandler(mActivity)
    }


}