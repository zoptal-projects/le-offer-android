package com.zoptal.cellableapp.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.main.activity.WillingToExchangeActivity
import com.zoptal.cellableapp.pojo_class.exchange_suggtions_pojo.MyExchangesData
import java.util.*

/**
 * Created by ${3embed} on ${27-10-2017}.
 * Banglore
 */
class IwantSuggessionAdapter(private val arrayList: ArrayList<MyExchangesData>, private val mActivity: WillingToExchangeActivity) : RecyclerView.Adapter<IwantSuggessionAdapter.MyViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(mActivity).inflate(R.layout.single_row_iwant_suggession_item, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val myExchangesData = arrayList[position]
        val productName = myExchangesData.productName
        val postId = myExchangesData.postId
        holder.tV_suggession.text = productName
        holder.itemView.setOnClickListener { mActivity.suggessionAdapterClick(productName, postId!!) }
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tV_suggession: TextView

        init {
            tV_suggession = itemView.findViewById<View>(R.id.tV_suggession) as TextView
        }
    }

}