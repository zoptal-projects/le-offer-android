package com.zoptal.cellableapp.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.main.camera_kit.CameraKitActivity
import com.zoptal.cellableapp.utility.CapturedImage
import com.zoptal.cellableapp.utility.CommonClass
import com.zoptal.cellableapp.utility.MyTransformation
import java.util.*

/**
 * <h>ImagesHorizontalRvAdap</h>
 *
 *
 * In class is called from CameraActivity class. In this recyclerview adapter class we used to inflate
 * single_row_camera_images layout and shows the images horizontally
 *
 * @since 21-Jun-17
 */
class ImagesHorizontalRvAdap
/**
 * <h>CurrencyRvAdap</h>
 *
 *
 * This is simple constructor to initailize list datas and context.
 *
 * @param mActivity The current context
 * @param arrayListImgPath The list datas
 */
//    public ImagesHorizontalRvAdap(Activity mActivity, ArrayList<String> arrayListImgPath, TextView tV_upload) {
//        this.mActivity = mActivity;
//        this.arrayListImgPath = arrayListImgPath;
//        this.tV_upload=tV_upload;
//    }
(private val mActivity: Activity, private val arrayListImgPath: ArrayList<CapturedImage>, private val tV_upload: TextView?, private val isFromCamera2: Boolean) : RecyclerView.Adapter<RecyclerView.ViewHolder?>() {

    /**
     * <h>OnCreateViewHolder</h>
     *
     *
     * In this method The adapter prepares the layout of the items by inflating the correct
     * layout for the individual data elements.
     *
     * @param parent A ViewGroup is a special view that can contain other views (called children.)
     * @param viewType Within the getItemViewType method the recycler view determines which type should be used for data.
     * @return It returns an object of type ViewHolder per visual entry in the recycler view.
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view: View
        when (viewType) {
            TYPE_ITEM -> {
                view = LayoutInflater.from(mActivity).inflate(R.layout.single_row_camera_images, parent, false)
                return MyViewHolder(view)
            }
            TYPE_FOOTER -> {
                view = LayoutInflater.from(mActivity).inflate(R.layout.single_row_camera_last_image, parent, false)
                return FooterViewHolder(view)
            }
        }
        return null!!
    }

    /**
     * <h>OnBindViewHolder</h>
     *
     *
     * In this method Every visible entry in a recycler view is filled with the
     * correct data model item by the adapter. Once a data item becomes visible,
     * the adapter assigns this data to the individual widgets which he inflated
     * earlier.
     *
     * @param holder The referece of MyViewHolder class of current class.
     * @param position The position of particular item
     */
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        // Horizontal Images
        if (holder is MyViewHolder) {
            val myViewHolder = holder
            val image = arrayListImgPath[position]
            val path = image.imagePath
            if (path != null && !path.isEmpty()) {
                try {
                    Glide.with(mActivity)
                            .load(path)
                            .asBitmap()
                            .centerCrop()
                            .transform(MyTransformation(mActivity, image.rotateAngle.toFloat(), System.currentTimeMillis().toString()), CenterCrop(mActivity))
                            .placeholder(R.drawable.default_image)
                            .into(myViewHolder.iV_captured_img)
                } catch (e: OutOfMemoryError) {
                    e.printStackTrace()
                } catch (e: IllegalArgumentException) {
                    e.printStackTrace()
                } catch (e: NullPointerException) {
                    e.printStackTrace()
                }
            }

            // remove image
            myViewHolder.iV_deleteImg.setOnClickListener {
                arrayListImgPath.removeAt(holder.getAdapterPosition())
                notifyDataSetChanged()

                /*if (isFromCamera2)
                            ((Camera2Activity)mActivity).isToCaptureImage = arrayListImgPath.size()<5;
                        else */(mActivity as CameraKitActivity).isToCaptureImage = arrayListImgPath.size < 5
                if (arrayListImgPath.size == 0 && tV_upload != null) tV_upload.setTextColor(ContextCompat.getColor(mActivity, R.color.reset_button_bg))
            }
        }

        // Footer View
        if (holder is FooterViewHolder) {
            val footerViewHolder = holder
            println(TAG + " " + "arrayListImgPath=" + arrayListImgPath.size)
            if (arrayListImgPath.size == 0) footerViewHolder.tV_coverPicText.visibility = View.VISIBLE else footerViewHolder.tV_coverPicText.visibility = View.GONE
        }
    }

    /**
     * Return the size of your dataset
     * @return the total number of rows
     */
    override fun getItemCount(): Int {
        return if (arrayListImgPath.size < 5) arrayListImgPath.size + 1 else arrayListImgPath.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (arrayListImgPath.size < 5) {
            if (position == arrayListImgPath.size) TYPE_FOOTER else TYPE_ITEM
        } else TYPE_ITEM
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var iV_captured_img: ImageView
        var iV_deleteImg: ImageView

        init {
            iV_captured_img = itemView.findViewById<View>(R.id.iV_image) as ImageView
            iV_captured_img.layoutParams.width = CommonClass.getDeviceWidth(mActivity) / 4
            iV_captured_img.layoutParams.height = CommonClass.getDeviceWidth(mActivity) / 4
            iV_deleteImg = itemView.findViewById<View>(R.id.iV_deleteImg) as ImageView
        }
    }

    private inner class FooterViewHolder internal constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var iV_dotted_image: ImageView
        var tV_coverPicText: TextView

        init {
            iV_dotted_image = itemView.findViewById<View>(R.id.iV_dotted_image) as ImageView
            iV_dotted_image.layoutParams.width = CommonClass.getDeviceWidth(mActivity) / 4
            iV_dotted_image.layoutParams.height = CommonClass.getDeviceWidth(mActivity) / 4
            tV_coverPicText = itemView.findViewById<View>(R.id.tV_coverPicText) as TextView
            tV_coverPicText.visibility = View.GONE
        }
    }

    companion object {
        private const val TYPE_ITEM = 1
        private const val TYPE_FOOTER = 2
        private val TAG = ImagesHorizontalRvAdap::class.java.simpleName
    }

}