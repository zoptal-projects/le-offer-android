package com.zoptal.cellableapp.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.pojo_class.promote_item_pojo.PromoteItemData
import java.util.*

/**
 * <h>PromoteItemRvAdapter</h>
 *
 *
 * In class is called from PromoteItemActivity class. In this recyclerview adapter class we used to inflate
 * single_row_promote_item layout and shows the all promote lists.
 *
 * @since 30-Aug-17
 */
class PromoteItemRvAdapter(private val mActivity: Activity, private val arrayListPromoteItem: ArrayList<PromoteItemData>) : RecyclerView.Adapter<PromoteItemRvAdapter.MyViewHolder>() {
    private var mSelectedItem = -1
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(mActivity).inflate(R.layout.single_row_promote_item, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        if (position == mSelectedItem) {
            arrayListPromoteItem[position].isItemSelected = true
            holder.rL_single_shell.setBackgroundResource(R.drawable.rect_pink_color_with_solid_shape)
            holder.tV_price.setTextColor(ContextCompat.getColor(mActivity, R.color.white))
            holder.tV_uniqueViews.setTextColor(ContextCompat.getColor(mActivity, R.color.white))
        } else {
            arrayListPromoteItem[position].isItemSelected = false
            holder.rL_single_shell.setBackgroundResource(R.drawable.rect_gray_color_stroke_and_solid_shape)
            holder.tV_price.setTextColor(ContextCompat.getColor(mActivity, R.color.item_name_color))
            holder.tV_uniqueViews.setTextColor(ContextCompat.getColor(mActivity, R.color.item_name_color))
        }
        val price: String?
        val uniqueViews: String?
        price = arrayListPromoteItem[position].price
        uniqueViews = arrayListPromoteItem[position].name

        // set price
        if (price != null && !price.isEmpty()) holder.tV_price.text = setPriceValue(price)

        // set unique views
        if (uniqueViews != null && !uniqueViews.isEmpty()) holder.tV_uniqueViews.text = uniqueViews
    }

    override fun getItemCount(): Int {
        return arrayListPromoteItem.size
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        var tV_price: TextView
        var tV_uniqueViews: TextView
        var rL_single_shell: RelativeLayout
        override fun onClick(v: View) {
            println("$TAG item clicked pos=$adapterPosition")
            mSelectedItem = adapterPosition
            notifyItemRangeChanged(0, arrayListPromoteItem.size)
        }

        init {
            itemView.setOnClickListener(this)
            tV_uniqueViews = itemView.findViewById<View>(R.id.tV_uniqueViews) as TextView
            tV_price = itemView.findViewById<View>(R.id.tV_price) as TextView
            rL_single_shell = itemView.findViewById<View>(R.id.rL_single_shell) as RelativeLayout
        }
    }

    private fun setPriceValue(price: String): String {
        return price
    }

    companion object {
        private val TAG = PromoteItemRvAdapter::class.java.simpleName
    }

}