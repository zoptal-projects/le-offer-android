package com.zoptal.cellableapp.adapter

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.app.ActivityOptionsCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.FitCenter
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.main.activity.ImagePreviewActivity
import com.zoptal.cellableapp.main.activity.PostProductActivity
import com.zoptal.cellableapp.main.camera_kit.CameraKitActivity
import com.zoptal.cellableapp.recyleview_drag_drop.ItemTouchHelperAdapter
import com.zoptal.cellableapp.recyleview_drag_drop.ItemTouchHelperViewHolder
import com.zoptal.cellableapp.recyleview_drag_drop.OnCustomerListChangedListener
import com.zoptal.cellableapp.recyleview_drag_drop.OnStartDragListener
import com.zoptal.cellableapp.utility.CapturedImage
import com.zoptal.cellableapp.utility.CommonClass
import com.zoptal.cellableapp.utility.MyTransformation
import com.zoptal.cellableapp.utility.VariableConstants
import java.util.*

/**
 * <h>PostProductImagesRvAdap</h>
 *
 *
 * In this class we used to set the custom adapter class for recycler view. In this recyclerview adapter class we used to inflate
 * single_row_images layout and shows the all the seleted image for posting.
 *
 * @since 21-Jun-17
 */
class PostProductImagesRvAdap //    public PostProductImagesRvAdap(Activity mActivity, ArrayList<String> arrayListImgPath,TextView tV_add_more_image) {
//        this.mActivity = mActivity;
//        this.arrayListImgPath = arrayListImgPath;
//        this.tV_add_more_image=tV_add_more_image;
//    }
(private val mActivity: Activity, private val arrayListImgPath: ArrayList<CapturedImage?>, private val tV_add_more_image: TextView, private val mDragStartListener: OnStartDragListener,
 private val mListChangedListener: OnCustomerListChangedListener) : RecyclerView.Adapter<RecyclerView.ViewHolder?>(), ItemTouchHelperAdapter {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view: View
        when (viewType) {
            TYPE_ITEM -> {
                view = LayoutInflater.from(mActivity).inflate(R.layout.single_row_post_images, parent, false)
                return MyViewHolder(view)
            }
            TYPE_FOOTER -> {
                view = LayoutInflater.from(mActivity).inflate(R.layout.footer_post_product_images, parent, false)
                return FooterViewHolder(view)
            }
        }
        return null!!
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        // Image
        if (holder is MyViewHolder) {
            val myViewHolder = holder

//            String path=arrayListImgPath.get(position);
//            if (path!=null && !path.isEmpty())
//                Picasso.with(mActivity)
//                        .load("file://"+path)
//                        .placeholder(R.drawable.default_image)
//                        .error(R.drawable.default_image)
//                        .into(myViewHolder.iV_captured_img);
            val image = arrayListImgPath[position]
            val path = image!!.imagePath
            if (path != null && !path.isEmpty()) {
                try {
                    Glide.with(mActivity)
                            .load(path)
                            .asBitmap()
                            .transform(MyTransformation(mActivity, image.rotateAngle.toFloat(), System.currentTimeMillis().toString()), FitCenter(mActivity))
                            .placeholder(R.drawable.default_image)
                            .into(myViewHolder.iV_captured_img)
                } catch (e: OutOfMemoryError) {
                    e.printStackTrace()
                } catch (e: IllegalArgumentException) {
                    e.printStackTrace()
                } catch (e: NullPointerException) {
                    e.printStackTrace()
                }
            }


            // remove image
            myViewHolder.iV_deleteImg.setOnClickListener {
                (mActivity as PostProductActivity).removedNewAddedPhotos(arrayListImgPath[holder.getAdapterPosition()]!!.imagePath!!)
                arrayListImgPath.removeAt(holder.getAdapterPosition())
                notifyDataSetChanged()
                tV_add_more_image.visibility = View.VISIBLE
                when (arrayListImgPath.size) {
                    0 -> tV_add_more_image.text = mActivity.getResources().getString(R.string.at_least_one_image_is)
                    1 -> tV_add_more_image.text = mActivity.getResources().getString(R.string.add_upto_4_more_img)
                    2 -> tV_add_more_image.text = mActivity.getResources().getString(R.string.add_upto_3_more_img)
                    3 -> tV_add_more_image.text = mActivity.getResources().getString(R.string.add_upto_2_more_img)
                    4 -> tV_add_more_image.text = mActivity.getResources().getString(R.string.add_upto_1_more_img)
                    5 -> tV_add_more_image.visibility = View.GONE
                }
            }
            holder.itemView.setOnLongClickListener {
                mDragStartListener.onStartDrag(holder)
                false
            }
            holder.itemView.setOnClickListener {
                val intent = Intent(mActivity, ImagePreviewActivity::class.java)
                intent.putExtra("url", path)
                val options = ActivityOptionsCompat.makeSceneTransitionAnimation(mActivity, (myViewHolder.iV_captured_img as View), "preview")
                mActivity.startActivity(intent, options.toBundle())
            }
        }

        // footer
        if (holder is FooterViewHolder) {
            /*FooterViewHolder footerViewHolder= (FooterViewHolder) holder;
            footerViewHolder.iV_add_image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mActivity.finish();
                }
            });*/
            holder.iV_add_image.setOnClickListener { /*Intent intent=new Intent();
                    intent.putExtra("arrayListImgPath",arrayListImgPath);
                    intent.putExtra("isFromAddNewPhoto", true);
                    mActivity.setResult(VariableConstants.CAMERA_IMAGE_LIST_REQ,intent);
                    mActivity.finish();*/
                val intent = Intent(mActivity, CameraKitActivity::class.java)
                intent.putExtra("arrayListImgPath", arrayListImgPath)
                intent.putExtra("isFromAddNewPhoto", true)
                mActivity.startActivityForResult(intent, VariableConstants.CAMERA_IMAGE_LIST_REQ)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (arrayListImgPath.size < 5) {
            if (isPositionFooter(position)) {
                TYPE_FOOTER
            } else TYPE_ITEM
        } else TYPE_ITEM
    }

    private fun isPositionFooter(position: Int): Boolean {
        return position == arrayListImgPath.size
    }

    override fun getItemCount(): Int {
        return if (arrayListImgPath.size < 5) arrayListImgPath.size + 1 else arrayListImgPath.size
    }

    override fun onItemMove(fromPosition: Int, toPosition: Int) {
        if (getItemViewType(toPosition) == TYPE_ITEM) {
            Collections.swap(arrayListImgPath, fromPosition, toPosition)
            mListChangedListener.onNoteListChanged(arrayListImgPath)
            notifyItemMoved(fromPosition, toPosition)
        }
    }

    override fun onItemDismiss(position: Int) {}
    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), ItemTouchHelperViewHolder {
        var iV_captured_img: ImageView
        var iV_deleteImg: ImageView
        override fun onItemSelected() {
            itemView.setBackgroundColor(0)
        }

        override fun onItemClear() {
            itemView.setBackgroundColor(0)
        }

        init {
            iV_captured_img = itemView.findViewById<View>(R.id.iV_image) as ImageView
            iV_captured_img.layoutParams.width = CommonClass.getDeviceWidth(mActivity) / 4
            iV_captured_img.layoutParams.height = CommonClass.getDeviceWidth(mActivity) / 4
            iV_deleteImg = itemView.findViewById<View>(R.id.iV_delete_icon) as ImageView
            iV_deleteImg.visibility = View.VISIBLE
        }
    }

    private inner class FooterViewHolder internal constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var iV_add_image: ImageView

        init {
            iV_add_image = itemView.findViewById<View>(R.id.iV_add_image) as ImageView
            iV_add_image.layoutParams.width = CommonClass.getDeviceWidth(mActivity) / 4
            iV_add_image.layoutParams.height = CommonClass.getDeviceWidth(mActivity) / 4
        }
    }

    companion object {
        private const val TYPE_ITEM = 1
        private const val TYPE_FOOTER = 2
    }

}