package com.zoptal.cellableapp.adapter

import android.app.Activity

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.pojo_class.card.CardList
import com.zoptal.cellableapp.utility.CardItemClickListener
import java.util.*

/**
 * <h>HomeFragRvAdapter</h>
 *
 *
 * In class is called from CurrencyListActivity class. In this recyclerview adapter class we used to inflate
 * single_row_select_currency layout and shows the all country and their code and symcol into list.
 *
 * @since 05-May-17
 */
class StripeListRvAdap
/**
 * <h>CurrencyRvAdap</h>
 *
 *
 * This is simple constructor to initailize list datas and context.
 *
 * @param mActivity The current context
 * @param arrayListCurrency The list datas
 * @param productItemClickListener
 */(private val mActivity: Activity, private val arrayListCurrency: ArrayList<CardList>, var mItemClick: CardItemClickListener) : RecyclerView.Adapter<StripeListRvAdap.MyViewHolder?>() {

    /**
     * <h>OnCreateViewHolder</h>
     *
     *
     * In this method The adapter prepares the layout of the items by inflating the correct
     * layout for the individual data elements.
     *
     * @param parent A ViewGroup is a special view that can contain other views (called children.)
     * @param viewType Within the getItemViewType method the recycler view determines which type should be used for data.
     * @return It returns an object of type ViewHolder per visual entry in the recycler view.
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(mActivity).inflate(R.layout.adapter_stripe_list, parent, false)
        return MyViewHolder(view)
    }

    /**
     * <h>OnBindViewHolder</h>
     *
     *
     * In this method Every visible entry in a recycler view is filled with the
     * correct data model item by the adapter. Once a data item becomes visible,
     * the adapter assigns this data to the individual widgets which he inflated
     * earlier.
     *
     * @param holder The referece of MyViewHolder class of current class.
     * @param position The position of particular item
     */
   override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        try {
            holder.tv_account_no.text = arrayListCurrency[position].name
            if (arrayListCurrency[position].is_default == "1") {
                holder.ib_bookmark.setImageResource(R.drawable.ic_bookmark)
            } else {
                holder.ib_bookmark.setImageResource(R.drawable.ic_unbookmark)
                holder.ib_bookmark.setOnClickListener { mItemClick.onItemClick(position, 1) }
            }
            holder.itemView.setOnClickListener(View.OnClickListener { mItemClick.onItemClick(position, 3) })
            holder.ib_delete.setOnClickListener { mItemClick.onItemClick(position, 2) }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun getItemCount(): Int {
        return arrayListCurrency.size
    }


    /**
     * <h>GetFilter</h>
     *
     *
     * a filter used to constrain data based on some filtering pattern
     *
     * @return Returns a filter that can be used to constrain data with a filtering pattern.
     *
     *
     * }
     *
     * / **
     * <h>MyViewHolder</h>
     *
     *
     * In this class we used to declare and assign the xml variables.
     *
     */
    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        //OUR VIEWS
        var tv_account_no: TextView
        var ib_bookmark: ImageButton
        var ib_delete: ImageButton

        init {
            tv_account_no = itemView.findViewById(R.id.tv_account_no)
            ib_bookmark = itemView.findViewById(R.id.ib_bookmark)
            ib_delete = itemView.findViewById(R.id.ib_delete)
        }
    }

    companion object {
        private val TAG = StripeListRvAdap::class.java.simpleName
    }

}