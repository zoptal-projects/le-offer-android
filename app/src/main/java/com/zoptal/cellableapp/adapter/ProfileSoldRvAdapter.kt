package com.zoptal.cellableapp.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.pojo_class.profile_sold_pojo.ProfileSoldDatas
import com.zoptal.cellableapp.utility.CommonClass
import com.zoptal.cellableapp.utility.DynamicHeightImageView
import com.zoptal.cellableapp.utility.ProductItemClickListener
import java.util.*

/**
 * <h>ProfileSoldRvAdapter</h>
 *
 *
 * In class is called from SellingFrag. In this recyclerview adapter class we used to inflate
 * single_row_images layout and shows the all post posted by logged-in user.
 *
 * @since 4/10/2017
 */
class ProfileSoldRvAdapter
/**
 * <h>CurrencyRvAdap</h>
 *
 *
 * This is simple constructor to initailize list datas and context.
 *
 * @param mActivity The current context
 * @param arrayListSellingDatas The list datas
 */(private val mActivity: Activity, private val arrayListSellingDatas: ArrayList<ProfileSoldDatas>, private val itemClickListener: ProductItemClickListener?) : RecyclerView.Adapter<ProfileSoldRvAdapter.MyViewHolder>() {

    /**
     * <h>OnCreateViewHolder</h>
     *
     *
     * In this method The adapter prepares the layout of the items by inflating the correct
     * layout for the individual data elements.
     *
     * @param parent A ViewGroup is a special view that can contain other views (called children.)
     * @param viewType Within the getItemViewType method the recycler view determines which type should be used for data.
     * @return It returns an object of type ViewHolder per visual entry in the recycler view.
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val exploreView = LayoutInflater.from(parent.context).inflate(R.layout.single_row_myprofile_images, parent, false)
        return MyViewHolder(exploreView)
    }

    /**
     * <h>OnBindViewHolder</h>
     *
     *
     * In this method Every visible entry in a recycler view is filled with the
     * correct data model item by the adapter. Once a data item becomes visible,
     * the adapter assigns this data to the individual widgets which he inflated
     * earlier.
     *
     * @param holder The referece of MyViewHolder class of current class.
     * @param position The position of particular item
     */
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val productName = arrayListSellingDatas[position].productName
        val productPrice = arrayListSellingDatas[position].price
        val currency = getCurrencySymbol(arrayListSellingDatas[position].currency)
        val pricetag = "$currency $productPrice"
        val postedImageUrl = arrayListSellingDatas[position].mainUrl
        println("$TAG postedImageUrl=$postedImageUrl")
        val containerWidth = arrayListSellingDatas[position].containerWidth
        val containerHeight = arrayListSellingDatas[position].containerHeight

        // set product name
        if (productName != null && !productName.isEmpty()) holder.tV_productName.text = productName

        // set product price
        if (productPrice != null && !productPrice.isEmpty()) holder.tV_productPrice.text = pricetag
        val deviceHalfWidth = CommonClass.getDeviceWidth(mActivity) / 2
        var setHeight = 0
        if (containerWidth != null && !containerWidth.isEmpty()) setHeight = containerHeight.toInt() * deviceHalfWidth / containerWidth.toInt()
        if (setHeight > CommonClass.dpToPx(mActivity, 250)) setHeight = CommonClass.dpToPx(mActivity, 250)
        holder.iV_explore_img.layoutParams.height = setHeight
        val imageUrl = postedImageUrl.replace("upload/", "upload/c_fit,h_500,q_40,w_500/")

        // set Product Image
        if (imageUrl != null && !imageUrl.isEmpty()) Glide.with(mActivity)
                .load(imageUrl)
                .fitCenter()
                .placeholder(R.color.image_bg_color)
                .error(R.color.image_bg_color)
                .into(holder.iV_explore_img)
        ViewCompat.setTransitionName(holder.iV_explore_img, arrayListSellingDatas[position].productName)
        holder.itemView.setOnClickListener { itemClickListener?.onItemClick(holder.adapterPosition, holder.iV_explore_img) }
    }

    /**
     * Return the size of your dataset
     * @return the total number of rows
     */
    override fun getItemCount(): Int {
        return arrayListSellingDatas.size
    }

    /**
     * <h>MyViewHolder</h>
     *
     *
     * In this class we used to declare and assign the xml variables.
     *
     */
    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val iV_explore_img: DynamicHeightImageView
        val tV_productPrice: TextView
        val tV_productName: TextView
        private val tV_swap_item: TextView
        private val rL_swap_tag: RelativeLayout

        init {
            iV_explore_img = itemView.findViewById<View>(R.id.iV_image) as DynamicHeightImageView
            tV_productName = itemView.findViewById<View>(R.id.tV_productName) as TextView
            tV_productPrice = itemView.findViewById<View>(R.id.tV_productPrice) as TextView
            rL_swap_tag = itemView.findViewById<View>(R.id.rL_swap_tag) as RelativeLayout
            tV_swap_item = itemView.findViewById<View>(R.id.tV_swap_item) as TextView
        }
    }

    private fun getCurrencySymbol(currency: String?): String? {
        if (currency != null && !currency.isEmpty()) {
            //..from array..//
            val arrayCurrency = mActivity.resources.getStringArray(R.array.currency_picker)
            if (arrayCurrency.size > 0) {
                var getCurrencyArr: Array<String>
                for (setCurrency in arrayCurrency) {
                    getCurrencyArr = setCurrency.split(",".toRegex()).toTypedArray()
                    val currency_code = getCurrencyArr[1]
                    val currency_symbol = getCurrencyArr[2]
                    if (currency == currency_code) {
                        println("$TAG currency symbol=$currency_symbol my currency=$currency")
                        return currency_symbol
                    }
                }
            }
        }
        return currency
    }

    companion object {
        private val TAG = ProfileSoldRvAdapter::class.java.simpleName
    }

}