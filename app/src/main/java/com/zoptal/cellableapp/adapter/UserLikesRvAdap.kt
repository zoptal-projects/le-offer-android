package com.zoptal.cellableapp.adapter

import android.app.Activity
import android.content.Intent
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.squareup.picasso.Picasso
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.event_bus.EventBusDatasHandler
import com.zoptal.cellableapp.main.activity.SelfProfileActivity
import com.zoptal.cellableapp.main.activity.UserProfileActivity
import com.zoptal.cellableapp.pojo_class.user_likes_pojo.UserLikesResponseDatas
import com.zoptal.cellableapp.utility.*
import java.util.*

/**
 * <h>HomeFragRvAdapter</h>
 *
 *
 * In class is called from CurrencyListActivity class. In this recyclerview adapter class we used to inflate
 * single_row_followers layout and shows the all list of Liked User.
 *
 * @since 4/29/2017
 */
class UserLikesRvAdap(private val mActivity: Activity, private val aL_likesDatas: ArrayList<UserLikesResponseDatas>) : RecyclerView.Adapter<UserLikesRvAdap.MyViewHolder?>() {
    private val mSessionManager: SessionManager
    private val apiCall: ApiCall
    private val mEventBusDatasHandler: EventBusDatasHandler

    /**
     * <h>OnCreateViewHolder</h>
     *
     *
     * In this method The adapter prepares the layout of the items by inflating the correct
     * layout for the individual data elements.
     *
     * @param parent A ViewGroup is a special view that can contain other views (called children.)
     * @param viewType Within the getItemViewType method the recycler view determines which type should be used for data.
     * @return It returns an object of type ViewHolder per visual entry in the recycler view.
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.single_row_followers, parent, false)
        return MyViewHolder(view)
    }

    /**
     * <h>OnBindViewHolder</h>
     *
     *
     * In this method Every visible entry in a recycler view is filled with the
     * correct data model item by the adapter. Once a data item becomes visible,
     * the adapter assigns this data to the individual widgets which he inflated
     * earlier.
     *
     * @param holder The referece of MyViewHolder class of current class.
     * @param position The position of particular item
     */
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val fullName: String?
        val username: String?
        val profilePicUrl: String?
        val followsFlag: String?
        fullName = aL_likesDatas[position].fullname
        username = aL_likesDatas[position].username
        profilePicUrl = aL_likesDatas[position].profilePicUrl
        followsFlag = aL_likesDatas[position].followStatus

        // full name
        if (fullName != null) holder.tV_fullName.text = fullName

        // user name
        if (username != null) holder.tV_userName.text = username

        // set user pic
        if (profilePicUrl != null && !profilePicUrl.isEmpty()) Picasso.with(mActivity)
                .load(profilePicUrl)
                .transform(CircleTransform())
                .placeholder(R.drawable.default_circle_img)
                .error(R.drawable.default_circle_img)
                .into(holder.iV_userPic)

        // Hide the follow option the looged-in user
        if (mSessionManager.userName == aL_likesDatas[position].username) holder.rL_follow.visibility = View.GONE else holder.rL_follow.visibility = View.VISIBLE

        // set follow and following text and background color
        if (followsFlag != null && followsFlag == "1") {
            holder.rL_follow.setBackgroundResource(R.drawable.rect_purple_color_with_solid_shape)
            holder.tV_follow.setTextColor(ContextCompat.getColor(mActivity, R.color.white))
            holder.tV_follow.text = mActivity.resources.getString(R.string.Following)
        } else {
            holder.rL_follow.setBackgroundResource(R.drawable.rect_purple_color_with_stroke_shape)
            holder.tV_follow.setTextColor(ContextCompat.getColor(mActivity, R.color.purple_color))
            holder.tV_follow.text = mActivity.resources.getString(R.string.follow)
        }
        val arrayListPostData = aL_likesDatas[position].postData

        // Follow or unfollow the user
        holder.rL_follow.setOnClickListener {
            val url: String
            if (CommonClass.isNetworkAvailable(mActivity)) {
                val mFollowStatus = aL_likesDatas[holder.getAdapterPosition()].followStatus
                if (mFollowStatus != null && mFollowStatus == "1") {
                    aL_likesDatas[holder.getAdapterPosition()].followStatus = "0"
                    holder.rL_follow.setBackgroundResource(R.drawable.rect_purple_color_with_stroke_shape)
                    holder.tV_follow.setTextColor(ContextCompat.getColor(mActivity, R.color.purple_color))
                    holder.tV_follow.text = mActivity.resources.getString(R.string.follow)
                    url = ApiUrl.UNFOLLOW + aL_likesDatas[holder.getAdapterPosition()].username
                    if (arrayListPostData != null && arrayListPostData.size > 0) mEventBusDatasHandler.setSocialDatasFromDiscovery(username, profilePicUrl, arrayListPostData[0], false)
                    apiCall.followUserApi(url)
                } else {
                    aL_likesDatas[holder.getAdapterPosition()].followStatus = "1"
                    holder.rL_follow.setBackgroundResource(R.drawable.rect_purple_color_with_solid_shape)
                    holder.tV_follow.setTextColor(ContextCompat.getColor(mActivity, R.color.white))
                    holder.tV_follow.text = mActivity.resources.getString(R.string.Following)
                    url = ApiUrl.FOLLOW + aL_likesDatas[holder.getAdapterPosition()].username
                    if (arrayListPostData != null && arrayListPostData.size > 0) mEventBusDatasHandler.setSocialDatasFromDiscovery(username, profilePicUrl, arrayListPostData[0], true)
                    apiCall.followUserApi(url)
                }
            }
        }

        // open user profile
        if (username != null && !username.isEmpty()) {
            holder.rL_user.setOnClickListener {
                val intent: Intent
                if (mSessionManager.isUserLoggedIn && mSessionManager.userName == username) {
                    intent = Intent(mActivity, SelfProfileActivity::class.java)
                    intent.putExtra("membername", username)
                } else {
                    intent = Intent(mActivity, UserProfileActivity::class.java)
                    intent.putExtra("membername", username)
                }
                mActivity.startActivity(intent)
            }
        }
    }

    override fun getItemCount(): Int {
        return aL_likesDatas.size
    }


    /**
     * <h>MyViewHolder</h>
     *
     *
     * In this class we used to declare and assign the xml variables.
     *
     */
    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var iV_userPic: ImageView
        var tV_fullName: TextView
        var tV_userName: TextView
        var tV_follow: TextView
        var rL_follow: RelativeLayout
        var rL_user: RelativeLayout

        init {
            iV_userPic = itemView.findViewById<View>(R.id.iV_userPic) as ImageView
            iV_userPic.layoutParams.width = CommonClass.getDeviceWidth(mActivity) / 7
            iV_userPic.layoutParams.height = CommonClass.getDeviceWidth(mActivity) / 7
            tV_fullName = itemView.findViewById<View>(R.id.tV_fullName) as TextView
            tV_userName = itemView.findViewById<View>(R.id.tV_userName) as TextView
            tV_follow = itemView.findViewById<View>(R.id.tV_follow) as TextView
            rL_follow = itemView.findViewById<View>(R.id.relative_follow) as RelativeLayout
            rL_user = itemView.findViewById<View>(R.id.rL_user) as RelativeLayout
        }
    }

    /**
     * <h>UserLikesRvAdap</h>
     *
     *
     * This is simple constructor to initailize list datas and context.
     *
     * @param mActivity The current context
     * @param aL_likesDatas The list datas
     */
    init {
        mSessionManager = SessionManager(mActivity)
        mEventBusDatasHandler = EventBusDatasHandler(mActivity)
        apiCall = ApiCall(mActivity)
    }
}