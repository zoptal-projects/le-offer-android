package com.zoptal.cellableapp.adapter

import android.app.Activity
import android.content.Context
import android.graphics.Point
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.*
import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.bumptech.glide.Glide
import com.facebook.ads.AdIconView
import com.facebook.ads.MediaView
import com.google.android.gms.ads.VideoController.VideoLifecycleCallbacks
import com.google.android.gms.ads.formats.UnifiedNativeAd
import com.google.android.gms.ads.formats.UnifiedNativeAdView
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.pojo_class.home_explore_pojo.ExploreResponseDatas
import com.zoptal.cellableapp.utility.CommonClass
import com.zoptal.cellableapp.utility.ProductItemClickListener
import java.util.*

/**
 * <h>ExploreRvAdapter</h>
 *
 *
 * In class is called from ExploreFrag. In this recyclerview adapter class we used to inflate
 * single_row_images layout and shows the all post posted by logged-in user.
 *
 * @since 4/6/2017
 */
class ExploreRvAdapter(private val mActivity: Activity, private val arrayListExploreDatas: ArrayList<ExploreResponseDatas>, private val animalItemClickListener: ProductItemClickListener) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        /*if(viewType==AD_VIEW){
            View view = LayoutInflater.from(mActivity).inflate(R.layout.facebook_native_ad_item, parent, false);
            return new NativeAdViewHolder(view);
        }else {*/
        val view = LayoutInflater.from(mActivity).inflate(R.layout.single_row_explore, parent, false)
        return MyViewHolder(view)
        //}
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        /*if(getItemViewType(position)==AD_VIEW){

            //....AdMob Native Ad

          */
        /*  AdViewHolder adViewHolder= (AdViewHolder) holder;

            StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) adViewHolder.itemView.getLayoutParams();
            layoutParams.setFullSpan(true);

            UnifiedNativeAdView adView = (UnifiedNativeAdView) mActivity.getLayoutInflater()
                    .inflate(R.layout.ad_unified, null);
            if(arrayListExploreDatas.get(position).getUnifiedNativeAd()!=null) {
                populateUnifiedNativeAdView(arrayListExploreDatas.get(position).getUnifiedNativeAd(), adView);
                adViewHolder.frameLayout.removeAllViews();
                adViewHolder.frameLayout.addView(adView);
            }*/
        /*

            //....Facebook Native Ad
            final NativeAdViewHolder adHolder = (NativeAdViewHolder) holder;

            StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) adHolder.itemView.getLayoutParams();
            layoutParams.setFullSpan(true);

            final com.facebook.ads.NativeAd nativeAd = (com.facebook.ads.NativeAd) arrayListExploreDatas.get(position).getAd();

            if(nativeAd!=null) {
                nativeAd.unregisterView();

                adHolder.itemView.getLayoutParams().height= ViewGroup.LayoutParams.WRAP_CONTENT;

                // Add the AdChoices icon
                AdChoicesView adChoicesView = new AdChoicesView(mActivity, nativeAd, true);
                adHolder.adChoicesContainer.removeAllViews();
                adHolder.adChoicesContainer.addView(adChoicesView, 0);
                // Set the Text.
                adHolder.nativeAdTitle.setText(nativeAd.getAdvertiserName());
                adHolder. nativeAdBody.setText(nativeAd.getAdBodyText());
                adHolder.nativeAdSocialContext.setText(nativeAd.getAdSocialContext());
                adHolder.nativeAdCallToAction.setVisibility(nativeAd.hasCallToAction() ? View.VISIBLE : View.INVISIBLE);
                adHolder.nativeAdCallToAction.setText(nativeAd.getAdCallToAction());
                adHolder.sponsoredLabel.setText(nativeAd.getSponsoredTranslation());

                // Create a list of clickable views
                List<View> clickableViews = new ArrayList<>();
                clickableViews.add(adHolder.nativeAdTitle);
                clickableViews.add(adHolder.nativeAdCallToAction);

                // Register the Title and CTA button to listen for clicks.
                nativeAd.registerViewForInteraction(
                        adHolder.itemView,
                        adHolder.nativeAdMedia,
                        adHolder.nativeAdIcon,
                        clickableViews);
            }else {
                adHolder.itemView.getLayoutParams().height=0;
            }

        }else {*/
        val exploreResponseDatas = arrayListExploreDatas[position]
        var productName = exploreResponseDatas.productName
        val productPrice = exploreResponseDatas.price
        val currency = getCurrencySymbol(exploreResponseDatas.currency)
        val pricetag = "$currency $productPrice"
        val isSwap = exploreResponseDatas.isSwap
        val exploreHoler = holder as MyViewHolder
        val layoutParams = exploreHoler.itemView.layoutParams as StaggeredGridLayoutManager.LayoutParams
        layoutParams.isFullSpan = false

        // set product name
        productName = CommonClass.getFirstCaps(productName)
        if (productName != null && !productName.isEmpty()) exploreHoler.tV_productName.text = productName

        // set product price
        if (productPrice != null && !productPrice.isEmpty()) exploreHoler.tV_productPrice.text = pricetag
        val postedImageUrl = exploreResponseDatas.mainUrl
        val containerHeight = exploreResponseDatas.containerHeight
        val containerWidth = exploreResponseDatas.containerWidth
        val isPromoted = exploreResponseDatas.isPromoted
        val deviceHalfWidth = CommonClass.getDeviceWidth(mActivity) / 2
        var setHeight = 0
        if (containerWidth != null && !containerWidth.isEmpty()) setHeight = containerHeight.toInt() * deviceHalfWidth / containerWidth.toInt()
        if (setHeight > CommonClass.dpToPx(mActivity, 250)) setHeight = CommonClass.dpToPx(mActivity, 250)
        exploreHoler.iV_explore_img.layoutParams.height = setHeight
        println(TAG + " " + "containerHeight=" + containerHeight + " " + "set height=" + setHeight + " " + "device half height=" + " " + CommonClass.getDeviceWidth(mActivity) + " " + CommonClass.getDeviceWidth(mActivity) / 2)
        val imageUrl = postedImageUrl.replace("upload/", "upload/c_fit,h_500,q_40,w_500/")

        // product image
        try {
            Glide.with(mActivity)
                    .load(imageUrl)
                    .fitCenter()
                    .placeholder(R.color.image_bg_color)
                    .error(R.color.image_bg_color)
                    .into(exploreHoler.iV_explore_img)
        } catch (e: OutOfMemoryError) {
            e.printStackTrace()
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }
        ViewCompat.setTransitionName(exploreHoler.iV_explore_img, exploreResponseDatas.productName)
        holder.itemView.setOnClickListener { animalItemClickListener.onItemClick(holder.getAdapterPosition(), exploreHoler.iV_explore_img) }

        // show featured tag with product
        if (isPromoted != null && !isPromoted.isEmpty()) {
            if (isPromoted != "0") {
                exploreHoler.rL_featured.visibility = View.VISIBLE
            } else exploreHoler.rL_featured.visibility = View.GONE
        } else exploreHoler.rL_featured.visibility = View.GONE
        if (isSwap == 1) {
            exploreHoler.rL_swap_tag.visibility = View.VISIBLE
            var listOfSwapItems = ""
            if (exploreResponseDatas.swapPost != null && exploreResponseDatas!!.swapPost!!.size > 0) {
                for (s in exploreResponseDatas.swapPost!!) listOfSwapItems += s.swapTitle + ", "
                val swap = mActivity.getString(R.string.swap_for_colon) + " " + listOfSwapItems.substring(0, listOfSwapItems.length - 2)
                exploreHoler.tV_swap_item.text = swap
                exploreHoler.tV_swap_item.visibility = View.VISIBLE
            }
        } else {
            exploreHoler.rL_swap_tag.visibility = View.GONE
            exploreHoler.tV_swap_item.visibility = View.GONE
        }
        // }
    }

    override fun getItemCount(): Int {
        return arrayListExploreDatas.size
    }

    override fun getItemViewType(position: Int): Int {
        /*if(position%10==0 && position!=0) {
            if(arrayListExploreDatas.get(position).getAd()!=null)
                return AD_VIEW;
            else
                return ITEM_VIEW;
        }
        else {*/
        return ITEM_VIEW
        //}
    }

    internal inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
         val iV_explore_img: ImageView
         val rL_featured: RelativeLayout
         val rL_swap_tag: RelativeLayout
         val tV_productPrice: TextView
         val tV_productName: TextView
         val tV_swap_item: TextView

        init {
            tV_productName = itemView.findViewById<View>(R.id.tV_productName) as TextView
            tV_productPrice = itemView.findViewById<View>(R.id.tV_productPrice) as TextView
            iV_explore_img = itemView.findViewById<View>(R.id.iV_image) as ImageView
            rL_featured = itemView.findViewById<View>(R.id.rL_featured) as RelativeLayout
            rL_swap_tag = itemView.findViewById<View>(R.id.rL_swap_tag) as RelativeLayout
            tV_swap_item = itemView.findViewById<View>(R.id.tV_swap_item) as TextView
            rL_featured.visibility = View.GONE
        }
    }

    // admob native ad view holder
    internal inner class AdViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val frameLayout: FrameLayout

        init {
            frameLayout = itemView.findViewById<View>(R.id.fl_adplaceholder) as FrameLayout
        }
    }

    //facebook native ads view holder
    private class NativeAdViewHolder internal constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var nativeAdIcon: AdIconView
        var nativeAdTitle: TextView
        var nativeAdMedia: MediaView
        var nativeAdSocialContext: TextView
        var nativeAdBody: TextView
        var sponsoredLabel: TextView
        var nativeAdCallToAction: Button
        var adChoicesContainer: LinearLayout

        init {
            adChoicesContainer = itemView.findViewById<View>(R.id.ad_choices_container) as LinearLayout
            nativeAdIcon = itemView.findViewById<View>(R.id.native_ad_icon) as AdIconView
            nativeAdTitle = itemView.findViewById<View>(R.id.native_ad_title) as TextView
            nativeAdMedia = itemView.findViewById<View>(R.id.native_ad_media) as MediaView
            nativeAdSocialContext = itemView.findViewById<View>(R.id.native_ad_social_context) as TextView
            nativeAdBody = itemView.findViewById<View>(R.id.native_ad_body) as TextView
            sponsoredLabel = itemView.findViewById<View>(R.id.native_ad_sponsored_label) as TextView
            nativeAdCallToAction = itemView.findViewById<View>(R.id.native_ad_call_to_action) as Button
        }
    }

    private fun getCurrencySymbol(currency: String?): String? {
        if (currency != null && !currency.isEmpty()) {
            //..from locale..//
            /*Currency c  = Currency.getInstance(currency);
            currency=c.getSymbol();
            tV_currency.setText(currency);*/

            //..from array..//
            val arrayCurrency = mActivity.resources.getStringArray(R.array.currency_picker)
            if (arrayCurrency.size > 0) {
                var getCurrencyArr: Array<String>
                for (setCurrency in arrayCurrency) {
                    getCurrencyArr = setCurrency.split(",".toRegex()).toTypedArray()
                    val currency_code = getCurrencyArr[1]
                    val currency_symbol = getCurrencyArr[2]
                    if (currency == currency_code) {
                        println("$TAG currency symbol=$currency_symbol my currency=$currency")
                        return currency_symbol
                    }
                }
            }
        }
        return currency
    }
    /*/ */
    /**
     * Populates a [UnifiedNativeAdView] object with data from a given
     * [UnifiedNativeAd].
     *
     * @param nativeAd the object containing the ad's assets
     * @param adView          the view to be populated
     */
    /**/ /**/
    private fun populateUnifiedNativeAdView(nativeAd: UnifiedNativeAd, adView: UnifiedNativeAdView) {
        // Get the video controller for the ad. One will always be provided, even if the ad doesn't
        // have a video asset.
        val vc = nativeAd.videoController

        // Create a new VideoLifecycleCallbacks object and pass it to the VideoController. The
        // VideoController will call methods on this object when events occur in the video
        // lifecycle.
        vc.videoLifecycleCallbacks = object : VideoLifecycleCallbacks() {
            override fun onVideoEnd() {
                // Publishers should allow native ads to complete video playback before refreshing
                // or replacing them with another ad in the same UI location.
                //refresh.setEnabled(true);
                //videoStatus.setText("Video status: Video playback has ended.");
                super.onVideoEnd()
            }
        }
        val mediaView = adView.findViewById<View>(R.id.ad_media) as com.google.android.gms.ads.formats.MediaView
        val mainImageView = adView.findViewById<View>(R.id.ad_image) as ImageView

        // Apps can check the VideoController's hasVideoContent property to determine if the
        // NativeAppInstallAd has a video asset.
        if (vc.hasVideoContent()) {
            adView.mediaView = mediaView
            mainImageView.visibility = View.GONE
            /*videoStatus.setText(String.format(Locale.getDefault(),
                    "Video status: Ad contains a %.2f:1 video asset.",
                    vc.getAspectRatio()));*/
        } else {
            adView.imageView = mainImageView
            mediaView.visibility = View.GONE

            // At least one image is guaranteed.
            val images = nativeAd.images
            mainImageView.setImageDrawable(images[0].drawable)

            //refresh.setEnabled(true);
            // videoStatus.setText("Video status: Ad does not contain a video asset.");
        }
        adView.headlineView = adView.findViewById(R.id.ad_headline)
        adView.bodyView = adView.findViewById(R.id.ad_body)
        adView.callToActionView = adView.findViewById(R.id.ad_call_to_action)
        adView.iconView = adView.findViewById(R.id.ad_app_icon)
        adView.priceView = adView.findViewById(R.id.ad_price)
        adView.starRatingView = adView.findViewById(R.id.ad_stars)
        adView.storeView = adView.findViewById(R.id.ad_store)
        adView.advertiserView = adView.findViewById(R.id.ad_advertiser)

        // Some assets are guaranteed to be in every UnifiedNativeAd.
        (adView.headlineView as TextView).text = nativeAd.headline
        (adView.bodyView as TextView).text = nativeAd.body
        (adView.callToActionView as Button).text = nativeAd.callToAction

        // These assets aren't guaranteed to be in every UnifiedNativeAd, so it's important to
        // check before trying to display them.
        if (nativeAd.icon == null) {
            adView.iconView.visibility = View.GONE
        } else {
            (adView.iconView as ImageView).setImageDrawable(
                    nativeAd.icon.drawable)
            adView.iconView.visibility = View.VISIBLE
        }
        if (nativeAd.price == null) {
            adView.priceView.visibility = View.INVISIBLE
        } else {
            adView.priceView.visibility = View.VISIBLE
            (adView.priceView as TextView).text = nativeAd.price
        }
        if (nativeAd.store == null) {
            adView.storeView.visibility = View.INVISIBLE
        } else {
            adView.storeView.visibility = View.VISIBLE
            (adView.storeView as TextView).text = nativeAd.store
        }
        if (nativeAd.starRating == null) {
            adView.starRatingView.visibility = View.INVISIBLE
        } else {
            (adView.starRatingView as RatingBar).rating = nativeAd.starRating.toFloat()
            adView.starRatingView.visibility = View.VISIBLE
        }
        if (nativeAd.advertiser == null) {
            adView.advertiserView.visibility = View.INVISIBLE
        } else {
            (adView.advertiserView as TextView).text = nativeAd.advertiser
            adView.advertiserView.visibility = View.VISIBLE
        }
        adView.setNativeAd(nativeAd)
    }

    companion object {
        private val TAG = ExploreRvAdapter::class.java.simpleName
        private const val AD_VIEW = 1
        private const val ITEM_VIEW = 0
    }

    init {
        println("$TAG al size in adap=$arrayListExploreDatas")
        val wm = mActivity.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val display = wm.defaultDisplay
        val size = Point()
        display.getSize(size)
    }
}