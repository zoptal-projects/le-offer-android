package com.zoptal.cellableapp.adapter

import android.app.Activity
import android.content.Context
import android.content.Intent

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.squareup.picasso.Picasso
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.event_bus.BusProvider
import com.zoptal.cellableapp.main.activity.UserLikesActivity
import com.zoptal.cellableapp.main.tab_fragments.SocialFrag
import com.zoptal.cellableapp.pojo_class.home_explore_pojo.ExploreLikedByUsersDatas
import com.zoptal.cellableapp.pojo_class.likedPosts.LikedPostResponseDatas
import com.zoptal.cellableapp.pojo_class.social_frag_pojo.SocialDatas
import com.zoptal.cellableapp.utility.*
import java.util.*

/**
 * <h></h>
 *
 *
 * In class is called from SocialFrag. In this recyclerview adapter class we used to inflate
 * single_row_home_news_feed layout and shows the all posted products.
 *
 * @since 4/6/2017
 */
class SocialFragRvAdapter(private val mActivity: Activity, private val arrayListNewsFeedDatas: ArrayList<SocialDatas>, private val rL_rootview: RelativeLayout) : RecyclerView.Adapter<SocialFragRvAdapter.MyViewHolder?>() {
    private val mSessionManager: SessionManager
    private val apiCall: ApiCall
    private var clickListener: ProductItemClickListener? = null

    /**
     * <h>OnCreateViewHolder</h>
     *
     *
     * In this method The adapter prepares the layout of the items by inflating the correct
     * layout for the individual data elements.
     *
     * @param parent A ViewGroup is a special view that can contain other views (called children.)
     * @param viewType Within the getItemViewType method the recycler view determines which type should be used for data.
     * @return It returns an object of type ViewHolder per visual entry in the recycler view.
     */
   override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(mActivity).inflate(R.layout.single_row_home_news_feed, parent, false)
        return MyViewHolder(view)
    }

    /**
     * <h>OnBindViewHolder</h>
     *
     *
     * In this method Every visible entry in a recycler view is filled with the
     * correct data model item by the adapter. Once a data item becomes visible,
     * the adapter assigns this data to the individual widgets which he inflated
     * earlier.
     *
     * @param holder The referece of MyViewHolder class of current class.
     * @param position The position of particular item
     */
   override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val time: String?
        var productName: String?
        var categoryName: String? = ""
        var currency: String?
        val price: String?
        val profilePic: String?
        val productImage: String?
        val viewCount: String?
        val membername: String
        val subCategoryName: String?
        val likeCount: String?
        val likeStatus: String?
        val postId: String
        time = arrayListNewsFeedDatas[position].postedOn
        productName = arrayListNewsFeedDatas[position].productName
        //if (arrayListNewsFeedDatas.get(position).getCategoryData()!=null)
        //categoryName=arrayListNewsFeedDatas.get(position).getCategoryData().get(0).getCategory();
        categoryName = arrayListNewsFeedDatas[position].category
        subCategoryName = arrayListNewsFeedDatas[position].subCategory
        currency = arrayListNewsFeedDatas[position].currency
        price = arrayListNewsFeedDatas[position].price
        profilePic = arrayListNewsFeedDatas[position].memberProfilePicUrl
        productImage = arrayListNewsFeedDatas[position].mainUrl
        likeCount = arrayListNewsFeedDatas[position].likes
        viewCount = arrayListNewsFeedDatas[position].clickCount
        likeStatus = arrayListNewsFeedDatas[position].likeStatus
        postId = arrayListNewsFeedDatas[position].postId
        membername = arrayListNewsFeedDatas[position].membername

        // User name
        //if (userName!=null)
        // holder.tV_userName.setText(userName);

        // user profile image
        if (profilePic != null && !profilePic.isEmpty()) Picasso.with(mActivity)
                .load(profilePic)
                .transform(CircleTransform())
                .placeholder(R.drawable.default_circle_img)
                .error(R.drawable.default_circle_img)
                .into(holder.iV_profilePic) else Picasso.with(mActivity)
                .load(R.drawable.default_circle_img)
                .transform(CircleTransform())
                .placeholder(R.drawable.default_circle_img)
                .error(R.drawable.default_circle_img)
                .into(holder.iV_profilePic)

        // Posted on
        if (time != null && !time.isEmpty()) holder.tV_time.text = CommonClass.getTimeDifference(time)

        // product image
        try {
            if (productImage != null && !productImage.isEmpty()) Glide.with(mActivity)
                    .load(productImage)
                    .asBitmap()
                    .placeholder(R.color.add_title)
                    .error(R.color.add_title).dontAnimate()
                    .into(holder.iV_productImage)
        } catch (e: OutOfMemoryError) {
            e.printStackTrace()
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        }

        // set currency
        if (currency != null && !currency.isEmpty()) {
            val c = Currency.getInstance(currency)
            currency = c.symbol
            println("$TAG symbol=$currency")
            holder.tV_currency.text = currency
        }

        // set Price
        if (price != null && !price.isEmpty()) holder.tV_price.text = price

        // set product name
        if (productName != null && !productName.isEmpty()) {
            productName = CommonClass.getFirstCaps(productName)
            holder.tV_productname.text = membername
            holder.tV_categoryName.text = productName
        }

        // set product's category name
        if (categoryName != null && !categoryName.isEmpty()) {
            var catSubCat = ""
            if (subCategoryName != null && !subCategoryName.isEmpty()) catSubCat = "$categoryName | $subCategoryName"

            //holder.tV_categoryName.setText(catSubCat);
        }

        // set like status
        if (likeStatus != null && likeStatus == "1") {
            holder.iV_like_icon.setImageResource(R.drawable.like_icon_on)
            holder.tV_like_count.setTextColor(ContextCompat.getColor(mActivity, R.color.pink_color))
            holder.rL_like.setBackgroundResource(R.drawable.rect_pink_color_with_stroke_shape)
        } else {
            holder.iV_like_icon.setImageResource(R.drawable.like_icon_off)
            holder.tV_like_count.setTextColor(ContextCompat.getColor(mActivity, R.color.hide_button_border_color))
            holder.rL_like.setBackgroundResource(R.drawable.rect_gray_color_with_with_stroke_shape)
        }

        // set total view count
        if (viewCount != null && !viewCount.isEmpty()) holder.tV_view_count.text = viewCount

        // set like total count
        if (likeCount != null && !likeCount.isEmpty()) holder.tV_like_count.text = likeCount

        // To show liked By Users
        val aL_likedByUsers = arrayListNewsFeedDatas[position].likedByUsers
        inflateUserLikes(likeCount, aL_likedByUsers, holder.linear_likedByUsers)

        // Like product
        holder.rL_like.setOnClickListener {
            if (CommonClass.isNetworkAvailable(mActivity)) {
                val mLikesCount = arrayListNewsFeedDatas[holder.getAdapterPosition()].likes
                val mLikesStatus = arrayListNewsFeedDatas[holder.getAdapterPosition()].likeStatus
                var mLikeCount = 0
                if (mLikesCount != null && !mLikesCount.isEmpty()) mLikeCount = mLikesCount.toInt()

                // unlike
                if (mLikesStatus != null && mLikesStatus == "1") {
                    addFavouriteData(holder.getAdapterPosition(), false)

                    // remove my self
                    if (aL_likedByUsers!!.size > 0) {
                        for (likeCount in aL_likedByUsers.indices) {
                            if (aL_likedByUsers[likeCount].likedByUsers == mSessionManager.userName) {
                                aL_likedByUsers.removeAt(likeCount)
                            }
                        }
                    }
                    mLikeCount -= 1
                    inflateUserLikes(mLikeCount.toString() + "", aL_likedByUsers, holder.linear_likedByUsers)
                    holder.tV_like_count.text = mLikeCount.toString()
                    //mLikesStatus = "0";
                    arrayListNewsFeedDatas[holder.getAdapterPosition()].likeStatus = "0"
                    arrayListNewsFeedDatas[holder.getAdapterPosition()].likes = mLikeCount.toString() + ""
                    holder.iV_like_icon.setImageResource(R.drawable.like_icon_off)
                    holder.tV_like_count.setTextColor(ContextCompat.getColor(mActivity, R.color.hide_button_border_color))
                    holder.rL_like.setBackgroundResource(R.drawable.rect_gray_color_with_with_stroke_shape)
                    apiCall.likeProductApi(ApiUrl.UNLIKE_PRODUCT, postId)
                } else {
                    // set event bus to add favourite data
                    addFavouriteData(holder.getAdapterPosition(), true)

                    // add myself
                    val likedByUsersDatas = ExploreLikedByUsersDatas()
                    likedByUsersDatas.likedByUsers = mSessionManager.userName!!
                    likedByUsersDatas.profilePicUrl = mSessionManager.userImage!!
                    //..handle first time like null image shown..//
                    if (aL_likedByUsers!!.size > 0 && mLikeCount == 0) {
                        if (aL_likedByUsers[0].name == "") {
                            aL_likedByUsers.removeAt(0)
                        }
                    }
                    //..handle end..//
                    aL_likedByUsers.add(0, likedByUsersDatas)
                    mLikeCount += 1
                    inflateUserLikes(mLikeCount.toString() + "", aL_likedByUsers, holder.linear_likedByUsers)
                    holder.tV_like_count.text = mLikeCount.toString()
                    //mLikesStatus = "1";
                    arrayListNewsFeedDatas[holder.getAdapterPosition()].likeStatus = "1"
                    arrayListNewsFeedDatas[holder.getAdapterPosition()].likes = mLikeCount.toString() + ""
                    holder.iV_like_icon.setImageResource(R.drawable.like_icon_on)
                    holder.tV_like_count.setTextColor(ContextCompat.getColor(mActivity, R.color.pink_color))
                    holder.rL_like.setBackgroundResource(R.drawable.rect_pink_color_with_stroke_shape)
                    apiCall.likeProductApi(ApiUrl.LIKE_PRODUCT, postId)
                }
                println("$TAG mLike count=$mLikeCount")
            } else CommonClass.showSnackbarMessage(rL_rootview, mActivity.resources.getString(R.string.NoInternetAccess))
        }

        // Get user all likes
        holder.iV_three_dots.setOnClickListener {
            val intent = Intent(mActivity, UserLikesActivity::class.java)
            intent.putExtra("postId", arrayListNewsFeedDatas[holder.getAdapterPosition()].postId)
            intent.putExtra("postType", arrayListNewsFeedDatas[holder.getAdapterPosition()].postsType)
            mActivity.startActivity(intent)
        }
        ViewCompat.setTransitionName(holder.iV_productImage, arrayListNewsFeedDatas[position].productName)

        // click on item view
        holder.mView.setOnClickListener { if (clickListener != null) clickListener!!.onItemClick(holder.getAdapterPosition(), holder.iV_productImage) }
    }

    /**
     * <h>InflateUserLikes</h>
     *
     *
     * In this method we used to inflate the user likes list into
     * LinearLayout horizontally.
     *
     */
    private fun inflateUserLikes(likesCount: String?, aL_likedByUsers: ArrayList<ExploreLikedByUsersDatas>?, linear_followed_images: LinearLayout) {
        var mLikeCount = 0
        if (likesCount != null && !likesCount.isEmpty()) mLikeCount = likesCount.toInt()
        if (mLikeCount > 0) {
            if (aL_likedByUsers != null && aL_likedByUsers.size > 0) {
                linear_followed_images.removeAllViews()
                for (likedCount in aL_likedByUsers.indices) {
                    val layoutInflater = mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                    val followedView = layoutInflater.inflate(R.layout.single_row_images, null)
                    val viewPagerItem_image = followedView.findViewById<View>(R.id.iV_image) as ImageView
                    viewPagerItem_image.setBackgroundColor(ContextCompat.getColor(mActivity, R.color.white))
                    viewPagerItem_image.layoutParams.width = CommonClass.getDeviceWidth(mActivity) / 9
                    viewPagerItem_image.layoutParams.height = CommonClass.getDeviceWidth(mActivity) / 9
                    viewPagerItem_image.setImageResource(R.drawable.default_circle_img)
                    val likedUserImg = aL_likedByUsers[likedCount].profilePicUrl
                    //viewPagerItem_image.setX(getResources().getDimension(R.dimen.dim));
                    if (likedUserImg != null && !likedUserImg.isEmpty()) {
                        Picasso.with(mActivity)
                                .load(likedUserImg)
                                .placeholder(R.drawable.default_circle_img)
                                .error(R.drawable.default_circle_img)
                                .transform(CircleTransform())
                                .into(viewPagerItem_image)
                    }
                    linear_followed_images.addView(followedView)
                }
            }
        } else linear_followed_images.removeAllViews()
    }

    /**
     * <h>AddFavouriteData</h>
     *
     *
     * In this method we used to make a Liked pojo class instance and set all required param.
     * Once we done it then just pass one event to pass the liked image datas to the Fav frag.
     *
     * @param position The position of image liked image
     */
    private fun addFavouriteData(position: Int, isToAdd: Boolean) {
        val likedPostCategoryDatas = LikedPostResponseDatas()
        likedPostCategoryDatas.isToAddLikedItem = isToAdd
        likedPostCategoryDatas.mainUrl = arrayListNewsFeedDatas[position].mainUrl
        likedPostCategoryDatas.productName = arrayListNewsFeedDatas[position].productName
        likedPostCategoryDatas.likes = arrayListNewsFeedDatas[position].likes
        likedPostCategoryDatas.likeStatus = arrayListNewsFeedDatas[position].likeStatus
        likedPostCategoryDatas.currency = arrayListNewsFeedDatas[position].currency
        likedPostCategoryDatas.price = arrayListNewsFeedDatas[position].price
        likedPostCategoryDatas.postedOn = arrayListNewsFeedDatas[position].postedOn
        likedPostCategoryDatas.thumbnailImageUrl = arrayListNewsFeedDatas[position].thumbnailImageUrl
        likedPostCategoryDatas.likedByUsers = arrayListNewsFeedDatas[position].likedByUsers
        //likedPostCategoryDatas.setDescription(arrayListNewsFeedDatas.get(position).getDescription());
        likedPostCategoryDatas.condition = arrayListNewsFeedDatas[position].condition
        likedPostCategoryDatas.place = arrayListNewsFeedDatas[position].place
        likedPostCategoryDatas.latitude = arrayListNewsFeedDatas[position].latitude
        likedPostCategoryDatas.longitude = arrayListNewsFeedDatas[position].longitude
        likedPostCategoryDatas.postId = arrayListNewsFeedDatas[position].postId
        likedPostCategoryDatas.postsType = arrayListNewsFeedDatas[position].postsType
        likedPostCategoryDatas.containerWidth = arrayListNewsFeedDatas[position].containerWidth
        likedPostCategoryDatas.containerHeight = arrayListNewsFeedDatas[position].containerHeight
        BusProvider.instance.post(likedPostCategoryDatas)
    }

    override fun getItemCount(): Int {
        return arrayListNewsFeedDatas.size
    }

    /**
     * <h>MyViewHolder</h>
     *
     *
     * In this class we used to declare and assign the xml variables.
     *
     */
    inner class MyViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        var iV_profilePic: ImageView
        var iV_productImage: ImageView
        var iV_like_icon: ImageView
        var iV_three_dots: ImageView
//        var tV_userName: TextView
        var tV_time: TextView
        var tV_currency: TextView
        var tV_price: TextView
        var tV_like_count: TextView
        var tV_view_count: TextView
        var tV_productname: TextView
        var tV_categoryName: TextView
        var linear_likedByUsers: LinearLayout
        var rL_like: RelativeLayout

        init {
            iV_profilePic = mView.findViewById<View>(R.id.iV_profilePic) as ImageView
            iV_profilePic.layoutParams.width = CommonClass.getDeviceWidth(mActivity) / 8
            iV_profilePic.layoutParams.height = CommonClass.getDeviceWidth(mActivity) / 8
            iV_productImage = mView.findViewById<View>(R.id.iV_productImage) as ImageView
            iV_productImage.layoutParams.height = CommonClass.getDeviceWidth(mActivity)
            iV_like_icon = mView.findViewById<View>(R.id.like_item_icon) as ImageView
            iV_three_dots = mView.findViewById<View>(R.id.iV_followed_list) as ImageView
//            tV_userName = mView.findViewById<View>(R.id.tV_userName) as TextView
            tV_time = mView.findViewById<View>(R.id.tV_time) as TextView
            tV_currency = mView.findViewById<View>(R.id.tV_currency) as TextView
            tV_price = mView.findViewById<View>(R.id.tV_price) as TextView
            tV_like_count = mView.findViewById<View>(R.id.tV_like_count) as TextView
            tV_view_count = mView.findViewById<View>(R.id.tV_view_count) as TextView
            linear_likedByUsers = mView.findViewById<View>(R.id.linear_followed_images) as LinearLayout
            tV_productname = mView.findViewById<View>(R.id.tV_productname) as TextView
            tV_categoryName = mView.findViewById<View>(R.id.tV_categoryName) as TextView
            rL_like = mView.findViewById<View>(R.id.relative_like_product) as RelativeLayout
        }
    }

    fun setItemClick(listener: ProductItemClickListener?) {
        clickListener = listener
    }

    companion object {
        private val TAG = SocialFrag::class.java.simpleName
    }

    /**
     * <h>CurrencyRvAdap</h>
     *
     *
     * This is simple constructor to initailize list datas and context.
     *
     * @param mActivity The current context
     * @param arrayListNewsFeedDatas The list datas
     */
    init {
        mSessionManager = SessionManager(mActivity)
        apiCall = ApiCall(mActivity)
    }
}