package com.zoptal.cellableapp.adapter.view_type_adapter

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.utility.ClickListener
import java.util.*

/**
 * Created by embed on 17/5/18.
 */
class MultipleSelectionAdap(private val mContext: Context, private val data: ArrayList<String>) : RecyclerView.Adapter<MultipleSelectionAdap.MyViewHolder?>() {
    @JvmField
    var selectedData: ArrayList<String>
    private val clickListener: ClickListener
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.single_row_selecetion_item, null)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        if (selectedData.contains(data[position])) {
            holder.iV_select.visibility = View.VISIBLE
        } else {
            holder.iV_select.visibility = View.GONE
        }
        holder.tV_value.text = data[position]
        holder.itemView.setOnClickListener(View.OnClickListener { // if selected list contains values then remove from selection else add to selected list
            if (selectedData.contains(data[holder.getAdapterPosition()])) {
                selectedData.remove(data[holder.getAdapterPosition()])
            } else {
                selectedData.add(data[holder.getAdapterPosition()])
            }
            clickListener.onItemClick(holder.tV_value, holder.getAdapterPosition())
            notifyItemRangeChanged(0, itemCount)
        })
    }

    override fun getItemCount(): Int {
      return data.size
    }


    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tV_value: TextView
        var iV_select: ImageView

        init {
            tV_value = itemView.findViewById<View>(R.id.tV_value) as TextView
            iV_select = itemView.findViewById<View>(R.id.iV_select) as ImageView
        }
    }

    init {
        clickListener = mContext as ClickListener
        selectedData = ArrayList()
    }
}