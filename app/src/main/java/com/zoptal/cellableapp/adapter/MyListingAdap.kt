package com.zoptal.cellableapp.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.pojo_class.profile_selling_pojo.ProfileSellingData
import com.zoptal.cellableapp.utility.CommonClass
import com.zoptal.cellableapp.utility.DynamicHeightImageView
import com.zoptal.cellableapp.utility.ProductItemClickListener
import java.util.*

/**
 * Created by embed on 9/5/18.
 */
class MyListingAdap
/**
 * <h>CurrencyRvAdap</h>
 *
 *
 * This is simple constructor to initailize list datas and context.
 *
 * @param mActivity The current context
 * @param arrayListSellingDatas The list datas
 */(private val mActivity: Activity, private val arrayListSellingDatas: ArrayList<ProfileSellingData>, private val itemClickListener: ProductItemClickListener?) : RecyclerView.Adapter<MyListingAdap.MyViewHolder>() {
    private var mSelectedItem = -1

    /**
     * <h>OnCreateViewHolder</h>
     *
     *
     * In this method The adapter prepares the layout of the items by inflating the correct
     * layout for the individual data elements.
     *
     * @param parent A ViewGroup is a special view that can contain other views (called children.)
     * @param viewType Within the getItemViewType method the recycler view determines which type should be used for data.
     * @return It returns an object of type ViewHolder per visual entry in the recycler view.
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val exploreView = LayoutInflater.from(parent.context).inflate(R.layout.single_row_mylisting, parent, false)
        return MyViewHolder(exploreView)
    }

    /**
     * <h>OnBindViewHolder</h>
     *
     *
     * In this method Every visible entry in a recycler view is filled with the
     * correct data model item by the adapter. Once a data item becomes visible,
     * the adapter assigns this data to the individual widgets which he inflated
     * earlier.
     *
     * @param holder The referece of MyViewHolder class of current class.
     * @param position The position of particular item
     */
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        if (position == mSelectedItem) {
            holder.iV_select_icon.visibility = View.VISIBLE
        } else {
            holder.iV_select_icon.visibility = View.GONE
        }
        val postedImageUrl = arrayListSellingDatas[position].mainUrl
        println("$TAG postedImageUrl=$postedImageUrl")
        val containerWidth = arrayListSellingDatas[position].containerWidth
        val containerHeight = arrayListSellingDatas[position].containerHeight
        val isPromoted = arrayListSellingDatas[position].isPromoted
        val deviceHalfWidth = CommonClass.getDeviceWidth(mActivity) / 2
        var setHeight = 0
        if (containerWidth != null && !containerWidth.isEmpty()) setHeight = containerHeight.toInt() * deviceHalfWidth / containerWidth.toInt()
        holder.iV_explore_img.layoutParams.height = setHeight

        // set Product Image
        if (postedImageUrl != null && !postedImageUrl.isEmpty()) Picasso.with(mActivity)
                .load(postedImageUrl)
                .resize(CommonClass.getDeviceWidth(mActivity) / 2, setHeight)
                .placeholder(R.color.image_bg_color)
                .error(R.color.image_bg_color)
                .into(holder.iV_explore_img)
        ViewCompat.setTransitionName(holder.iV_explore_img, arrayListSellingDatas[position].productName)
        holder.itemView.setOnClickListener {
            mSelectedItem = holder.adapterPosition
            notifyItemRangeChanged(0, arrayListSellingDatas.size)
            itemClickListener?.onItemClick(mSelectedItem, holder.iV_explore_img)
        }

        // show featured tag with product
        if (isPromoted != null && !isPromoted.isEmpty()) {
            if (isPromoted != "0") {
                holder.rL_featured.visibility = View.VISIBLE
            } else holder.rL_featured.visibility = View.GONE
        } else holder.rL_featured.visibility = View.GONE
    }

    override fun getItemCount(): Int {
        return arrayListSellingDatas.size
    }

    inner class MyViewHolder internal constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val iV_explore_img: DynamicHeightImageView
        val rL_featured: RelativeLayout
        val iV_select_icon: ImageView

        init {
            iV_explore_img = itemView.findViewById<View>(R.id.iV_image) as DynamicHeightImageView
            rL_featured = itemView.findViewById<View>(R.id.rL_featured) as RelativeLayout
            iV_select_icon = itemView.findViewById<View>(R.id.iV_select_icon) as ImageView
        }
    }

    companion object {
        private val TAG = ProfileSoldRvAdapter::class.java.simpleName
    }

}