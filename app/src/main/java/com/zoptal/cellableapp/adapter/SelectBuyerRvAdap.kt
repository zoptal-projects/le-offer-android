package com.zoptal.cellableapp.adapter

import android.app.Activity

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.main.activity.SelectBuyerActivity
import com.zoptal.cellableapp.pojo_class.accepted_offer.AcceptedOfferDatas
import com.zoptal.cellableapp.utility.CircleTransform
import com.zoptal.cellableapp.utility.ClickListener
import com.zoptal.cellableapp.utility.CommonClass
import com.zoptal.cellableapp.utility.DialogBox
import java.util.*

/**
 * <h>HomeFragRvAdapter</h>
 *
 *
 * In class is called from EditProductActivity class. In this recyclerview adapter class we used to inflate
 * single_row_accepted_offer layout and shows the all accepted offer.
 *
 * @since 13-Jul-17
 */
class SelectBuyerRvAdap(private val mActivity: Activity, private val arrayListAcceptedOffer: ArrayList<AcceptedOfferDatas>) : RecyclerView.Adapter<RecyclerView.ViewHolder?>() {
    private var clickListener: ClickListener? = null
    private val mDialogBox: DialogBox

    /**
     * <h>OnCreateViewHolder</h>
     *
     *
     * In this method The adapter prepares the layout of the items by inflating the correct
     * layout for the individual data elements.
     *
     * @param parent A ViewGroup is a special view that can contain other views (called children.)
     * @param viewType Within the getItemViewType method the recycler view determines which type should be used for data.
     * @return It returns an object of type ViewHolder per visual entry in the recycler view.
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view: View
        when (viewType) {
            TYPE_ITEM -> {
                view = LayoutInflater.from(mActivity).inflate(R.layout.single_row_select_buyer, parent, false)
                return MyViewHolder(view)
            }
            TYPE_FOOTER -> {
                view = LayoutInflater.from(mActivity).inflate(R.layout.footer_select_buyer, parent, false)
                return FooterViewHolder(view)
            }
        }
        return null!!
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        // For Item
        if (holder is MyViewHolder) {
            val myViewHolder = holder as MyViewHolder
            val buyerName: String?
            val buyerImage: String?
            val price: String
            var currency: String?
            buyerName = arrayListAcceptedOffer[position].buyerFullName
            buyerImage = arrayListAcceptedOffer[position].buyerProfilePicUrl
            price = arrayListAcceptedOffer[position].price
            currency = arrayListAcceptedOffer[position].currency

            // set Profile pic
            if (buyerImage != null && !buyerImage.isEmpty()) Picasso.with(mActivity)
                    .load(buyerImage)
                    .placeholder(R.drawable.default_circle_img)
                    .error(R.drawable.default_circle_img)
                    .transform(CircleTransform())
                    .into(myViewHolder.image)

            // set buyer name
            if (buyerName != null && !buyerName.isEmpty()) myViewHolder.tV_heading.text = buyerName

            // set currency
            if (currency != null && !currency.isEmpty()) {
                val c = Currency.getInstance(currency)
                currency = c.symbol
            }
            val setPrice = mActivity.resources.getString(R.string.price_offered) + " " + currency + price
            myViewHolder.tV_subHeading.text = setPrice
        }

        // For footer
        if (holder is FooterViewHolder) {
            val footerViewHolder = holder as FooterViewHolder
            footerViewHolder.itemView.setOnClickListener(View.OnClickListener {
                val postId = (mActivity as SelectBuyerActivity).postId
                if (postId != null && !postId.isEmpty()) mDialogBox.sellSomeWhereDialog(postId, (mActivity as SelectBuyerActivity).rL_rootElement)
            })
        }
    }

    override fun getItemCount(): Int {
        return arrayListAcceptedOffer.size + 1
    }


    override fun getItemViewType(position: Int): Int {
        return if (arrayListAcceptedOffer.size == position) TYPE_FOOTER else TYPE_ITEM
    }

    /**
     * <h>MyViewHolder</h>
     *
     *
     * In this class we used to declare and assign the xml variables.
     *
     */
    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var image: ImageView
        var tV_heading: TextView
        var tV_subHeading: TextView

        init {
            itemView.setOnClickListener { v -> clickListener!!.onItemClick(v, getAdapterPosition()) }
            image = itemView.findViewById<View>(R.id.image) as ImageView
            image.layoutParams.width = CommonClass.getDeviceWidth(mActivity) / 7
            image.layoutParams.height = CommonClass.getDeviceWidth(mActivity) / 7
            tV_heading = itemView.findViewById<View>(R.id.tV_heading) as TextView
            tV_subHeading = itemView.findViewById<View>(R.id.tV_subHeading) as TextView
        }
    }

    private inner class FooterViewHolder internal constructor(itemView: View?) : RecyclerView.ViewHolder(itemView!!)

    fun setItemClick(listener: ClickListener?) {
        clickListener = listener
    }

    companion object {
        private const val TYPE_ITEM = 1
        private const val TYPE_FOOTER = 2
        private val TAG = SelectBuyerRvAdap::class.java.simpleName
    }

    /**
     * <h>CurrencyRvAdap</h>
     *
     *
     * This is simple constructor to initailize list datas and context.
     *
     * @param mActivity The current context
     * @param arrayListAcceptedOffer The list datas
     */
    init {
        mDialogBox = DialogBox(mActivity)
    }
}