package com.zoptal.cellableapp.adapter

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.pojo_class.CurrencyPojo
import com.zoptal.cellableapp.utility.ClickListener
import com.zoptal.cellableapp.utility.VariableConstants
import java.util.*

/**
 * <h>HomeFragRvAdapter</h>
 *
 *
 * In class is called from CurrencyListActivity class. In this recyclerview adapter class we used to inflate
 * single_row_select_currency layout and shows the all country and their code and symcol into list.
 *
 * @since 05-May-17
 */
class CurrencyRvAdap
/**
 * <h>CurrencyRvAdap</h>
 *
 *
 * This is simple constructor to initailize list datas and context.
 *
 * @param mActivity The current context
 * @param arrayListCurrency The list datas
 */(private val mActivity: Activity, private var arrayListCurrency: ArrayList<CurrencyPojo>) : RecyclerView.Adapter<CurrencyRvAdap.MyViewHolder>(), Filterable {
    private var filter: RecordFilter? = null

    /**
     * <h>OnCreateViewHolder</h>
     *
     *
     * In this method The adapter prepares the layout of the items by inflating the correct
     * layout for the individual data elements.
     *
     * @param parent A ViewGroup is a special view that can contain other views (called children.)
     * @param viewType Within the getItemViewType method the recycler view determines which type should be used for data.
     * @return It returns an object of type ViewHolder per visual entry in the recycler view.
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(mActivity).inflate(R.layout.single_row_select_currency, parent, false)
        return MyViewHolder(view)
    }

    /**
     * <h>OnBindViewHolder</h>
     *
     *
     * In this method Every visible entry in a recycler view is filled with the
     * correct data model item by the adapter. Once a data item becomes visible,
     * the adapter assigns this data to the individual widgets which he inflated
     * earlier.
     *
     * @param holder The referece of MyViewHolder class of current class.
     * @param position The position of particular item
     */
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val countryName: String?
        val code: String?
        val symbol: String?
        countryName = arrayListCurrency[position].country
        code = arrayListCurrency[position].code
        symbol = arrayListCurrency[position].symbol

        // country name
        if (countryName != null) holder.tV_countryName.text = countryName

        // code
        if (code != null) holder.tV_code.text = code

        // symbol
        if (symbol != null) holder.tV_symbol.text = symbol


        //IMPLEMENT CLICK LISTENET
        holder.setItemClickListener(object : ClickListener {
            override fun onItemClick(view: View?, position: Int) {
                println("$TAG item clicked")
                val cuurency_code = arrayListCurrency[position].code
                val currency_symbol = arrayListCurrency[position].symbol
                if (cuurency_code != null && currency_symbol != null) {
                    val intent = Intent()
                    intent.putExtra("cuurency_code", cuurency_code)
                    intent.putExtra("currency_symbol", currency_symbol)
                    mActivity.setResult(VariableConstants.CURRENCY_REQUEST_CODE, intent)
                    mActivity.onBackPressed()
                }
            }
        })

    }

    /**
     * Return the size of your dataset
     * @return the total number of rows
     */
    override fun getItemCount(): Int {
        return arrayListCurrency.size
    }

    /**
     * <h>GetFilter</h>
     *
     *
     * a filter used to constrain data based on some filtering pattern
     *
     * @return Returns a filter that can be used to constrain data with a filtering pattern.
     */
    override fun getFilter(): Filter {
        if (filter == null) {
            //filter=new CustomFilter(arrayListCurrency,this);
            filter = RecordFilter()
        }
        return filter!!
    }

    /**
     * <h>RecordFilter</h>
     *
     *
     * This is custom filter class which extend Filter abstract class. The Filter class has 2 abstract
     * methods that must be overridden.
     * 1> performFiltering() – Filter the data based on a pattern in a worker thread. It must return a
     * FilterResults object which holds the results of the filtering operation. A FilterResults object
     * has 2 properties, count that holds the count of results computed by the operation and values that
     * contains the values returned by the same filtering operation.
     * 2> Once the filtering is completed, the results are passed through this method to publish them in
     * the user interface on the UI thread.
     *
     */
    private inner class RecordFilter : Filter() {
        private val filterList = arrayListCurrency

        //FILTERING OCURS
        override fun performFiltering(constraint: CharSequence): FilterResults {
            var constraint: CharSequence? = constraint
            val results = FilterResults()

            //CHECK CONSTRAINT VALIDITY
            if (constraint != null && constraint.length > 0) {
                //CHANGE TO UPPER
                constraint = constraint.toString().toUpperCase()
                //STORE OUR FILTERED PLAYERS
                val filteredPlayers = ArrayList<CurrencyPojo>()
                for (i in filterList.indices) {
                    //CHECK
                    if (filterList[i].country.toUpperCase().contains(constraint)) {
                        //ADD PLAYER TO FILTERED PLAYERS
                        filteredPlayers.add(filterList[i])
                    }
                }
                results.count = filteredPlayers.size
                results.values = filteredPlayers
            } else {
                results.count = filterList.size
                results.values = filterList
            }
            return results
        }

        override fun publishResults(constraint: CharSequence, results: FilterResults) {
            arrayListCurrency = results.values as ArrayList<CurrencyPojo>
            //REFRESH
            notifyDataSetChanged()
        }
    }

    /**
     * <h>MyViewHolder</h>
     *
     *
     * In this class we used to declare and assign the xml variables.
     *
     */
    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        //OUR VIEWS
        var tV_countryName: TextView
        var tV_code: TextView
        var tV_symbol: TextView
        private var itemClickListener: ClickListener? = null
        override fun onClick(v: View) {
            itemClickListener!!.onItemClick(v, layoutPosition)
        }

        fun setItemClickListener(ic: ClickListener?) {
            itemClickListener = ic
        }

        init {
            tV_countryName = itemView.findViewById<View>(R.id.tV_countryName) as TextView
            tV_code = itemView.findViewById<View>(R.id.tV_code) as TextView
            tV_symbol = itemView.findViewById<View>(R.id.tV_symbol) as TextView
            itemView.setOnClickListener(this)
        }
    }

    companion object {
        private val TAG = CurrencyRvAdap::class.java.simpleName
    }

}