package com.zoptal.cellableapp.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.pojo_class.GalleryImagePojo
import com.zoptal.cellableapp.utility.CommonClass
import com.zoptal.cellableapp.utility.MyTransformation
import java.util.*

/**
 * <h>ImagesHorizontalRvAdap</h>
 *
 *
 * In class is called from CameraActivity class. In this recyclerview adapter class we used to inflate
 * single_row_camera_images layout and shows the images horizontally
 *
 * @since 14-Aug-17
 */
class GalleryImgHorizontalRvAdap
/**
 * <h>CurrencyRvAdap</h>
 *
 *
 * This is simple constructor to initailize list datas and context.
 *
 * @param mActivity The current context
 * @param arrayListImgPath The list datas
 */(private val mActivity: Activity, private val arrayListImgPath: ArrayList<String>, private val galleryRvAdap: GalleryRvAdap, private val arrayListGalleryImg: ArrayList<GalleryImagePojo>, private val rotationAngles: ArrayList<Int>) : RecyclerView.Adapter<GalleryImgHorizontalRvAdap.MyViewHolder>() {

    /**
     * <h>OnCreateViewHolder</h>
     *
     *
     * In this method The adapter prepares the layout of the items by inflating the correct
     * layout for the individual data elements.
     *
     * @param parent A ViewGroup is a special view that can contain other views (called children.)
     * @param viewType Within the getItemViewType method the recycler view determines which type should be used for data.
     * @return It returns an object of type ViewHolder per visual entry in the recycler view.
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(mActivity).inflate(R.layout.single_row_camera_images, parent, false)
        return MyViewHolder(view)
    }

    /**
     * <h>OnBindViewHolder</h>
     *
     *
     * In this method Every visible entry in a recycler view is filled with the
     * correct data model item by the adapter. Once a data item becomes visible,
     * the adapter assigns this data to the individual widgets which he inflated
     * earlier.
     *
     * @param holder The referece of MyViewHolder class of current class.
     * @param position The position of particular item
     */
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val path = arrayListImgPath[position]
        var rotationAngle = 0
        if (position < rotationAngles.size) rotationAngle = rotationAngles[position]
        /* if (path!=null && !path.isEmpty())
            Picasso.with(mActivity)
                    .load("file://"+path)
                    .placeholder(R.drawable.default_image)
                    .error(R.drawable.default_image)
                    .into(holder.iV_captured_img);*/try {
            Glide.with(mActivity) //.load("file://"+path)
                    .load("file://$path")
                    .asBitmap()
                    .transform(MyTransformation(mActivity, rotationAngle.toFloat(), System.currentTimeMillis().toString()), CenterCrop(mActivity))
                    .placeholder(R.drawable.default_image) // .fitCenter()
                    .into(holder.iV_captured_img)
        } catch (e: OutOfMemoryError) {
            e.printStackTrace()
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }

        // remove image
        holder.iV_deleteImg.setOnClickListener {
            for (galleryImgCount in arrayListGalleryImg.indices) {
                if (arrayListGalleryImg[galleryImgCount].galleryImagePath == arrayListImgPath[holder.adapterPosition]) {
                    arrayListGalleryImg[galleryImgCount].isSelected = false
                    galleryRvAdap.notifyItemChanged(galleryImgCount)
                }
            }
            arrayListImgPath.removeAt(holder.adapterPosition)
            //rotationAngles.remove(holder.getAdapterPosition());
            if (arrayListImgPath.size == 0) {
                arrayListImgPath.clear()
                rotationAngles.clear()
            }
            notifyDataSetChanged()
        }
    }

    /**
     * Return the size of your dataset
     * @return the total number of rows
     */
    override fun getItemCount(): Int {
        return arrayListImgPath.size
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var iV_captured_img: ImageView
        var iV_deleteImg: ImageView

        init {
            iV_captured_img = itemView.findViewById<View>(R.id.iV_image) as ImageView
            iV_captured_img.layoutParams.width = CommonClass.getDeviceWidth(mActivity) / 4
            iV_captured_img.layoutParams.height = CommonClass.getDeviceWidth(mActivity) / 4
            iV_deleteImg = itemView.findViewById<View>(R.id.iV_deleteImg) as ImageView
        }
    }

}