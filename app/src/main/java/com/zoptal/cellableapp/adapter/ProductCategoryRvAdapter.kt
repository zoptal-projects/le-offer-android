package com.zoptal.cellableapp.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.pojo_class.product_category.ProductCategoryResDatas
import com.zoptal.cellableapp.utility.CircleTransform
import com.zoptal.cellableapp.utility.ClickListener
import java.util.*

/**
 * <h>ProductCategoryRvAdapter</h>
 *
 *
 * This class is called from ProductCategoryActivity class. This class extends RecyclerView.Adapter
 * from that we have three overrided method 1. onCreateViewHolder() In this method we used to inflate
 * single_row_product_category.  2> getItemCount() This method retuns the total number of inflated rows
 * 3>onBindViewHolder() In this method we used to set the all values to that inflated xml from list datas.
 *
 * @since 2017-05-04
 */
class ProductCategoryRvAdapter
/**
 * <h>ProductCategoryRvAdapter</h>
 *
 *
 * This is simple constructor to initailize list datas and context.
 *
 * @param mActivity The current context
 * @param aL_categoryDatas The response datas
 */(private val mActivity: Activity, private val aL_categoryDatas: ArrayList<ProductCategoryResDatas>) : RecyclerView.Adapter<ProductCategoryRvAdapter.MyViewHolder>() {
    private var clickListener: ClickListener? = null
    @JvmField
    var mSelected = -1

    /**
     * <h>OnCreateViewHolder</h>
     *
     *
     * In this method The adapter prepares the layout of the items by inflating the correct
     * layout for the individual data elements.
     *
     * @param parent A ViewGroup is a special view that can contain other views (called children.)
     * @param viewType Within the getItemViewType method the recycler view determines which type should be used for data.
     * @return It returns an object of type ViewHolder per visual entry in the recycler view.
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(mActivity).inflate(R.layout.single_row_product_category, parent, false)
        return MyViewHolder(view)
    }

    /**
     * <h>OnBindViewHolder</h>
     *
     *
     * In this method Every visible entry in a recycler view is filled with the
     * correct data model item by the adapter. Once a data item becomes visible,
     * the adapter assigns this data to the individual widgets which he inflated
     * earlier.
     *
     * @param holder The referece of MyViewHolder class of current class.
     * @param position The position of particular item
     */
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        if (mSelected == position) holder.iV_select.visibility = View.VISIBLE else holder.iV_select.visibility = View.GONE
        var categoryName = aL_categoryDatas[position].name
        println("$TAG categoryName=$categoryName")
        if (categoryName != null && !categoryName.isEmpty()) {
            categoryName = categoryName.substring(0, 1).toUpperCase() + categoryName.substring(1).toLowerCase()
            holder.tV_category.text = categoryName
        }
        if (!aL_categoryDatas[position].activeimage.isEmpty() && aL_categoryDatas[position].activeimage != null) {
            Picasso.with(mActivity)
                    .load(aL_categoryDatas[position].activeimage)
                    .resizeDimen(R.dimen.thirty_dp, R.dimen.thirty_dp)
                    .centerCrop()
                    .transform(CircleTransform())
                    .into(holder.iV_category)
        }
    }

    /**
     * Return the size of your dataset
     * @return the total number of rows
     */
    override fun getItemCount(): Int {
        return aL_categoryDatas.size
    }

    /**
     * <h>MyViewHolder</h>
     *
     *
     * In this class we used to declare and assign the xml variables.
     *
     */
    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tV_category: TextView
        var iV_category: ImageView
        var iV_select: ImageView

        init {
            itemView.setOnClickListener { v -> clickListener!!.onItemClick(v, adapterPosition) }
            tV_category = itemView.findViewById<View>(R.id.tV_category) as TextView
            iV_category = itemView.findViewById<View>(R.id.iV_category) as ImageView
            iV_select = itemView.findViewById<View>(R.id.iV_select) as ImageView
        }
    }

    fun setOnItemClick(listener: ClickListener?) {
        clickListener = listener
    }

    companion object {
        private val TAG = ProductCategoryRvAdapter::class.java.simpleName
    }

}