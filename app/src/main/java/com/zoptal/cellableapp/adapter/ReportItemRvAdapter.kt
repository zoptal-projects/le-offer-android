package com.zoptal.cellableapp.adapter

import android.app.Activity
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.RadioButton
import android.widget.RelativeLayout
import androidx.recyclerview.widget.RecyclerView
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.pojo_class.report_product_pojo.ReportProductDatas
import java.util.*

/**
 * <h>ReportItemRvAdapter</h>
 *
 *
 * This class is called from ReportProductActivity class. In this recyclerview adapter class we used to inflate
 * single_row_report_product layout and shows the report reason of the product.
 *
 * @since 13-Jun-17
 */
class ReportItemRvAdapter
/**
 *
 *
 * This is simple constructor to initailize list datas and context.
 *
 * @param mActivity The current context
 * @param arrayListReportItem The list datas
 */(private val mActivity: Activity, private val arrayListReportItem: ArrayList<ReportProductDatas>) : RecyclerView.Adapter<ReportItemRvAdapter.MyViewHolder>() {
    private var mSelectedItem = -1

    /**
     * <h>OnCreateViewHolder</h>
     *
     *
     * In this method The adapter prepares the layout of the items by inflating the correct
     * layout for the individual data elements.
     *
     * @param parent A ViewGroup is a special view that can contain other views (called children.)
     * @param viewType Within the getItemViewType method the recycler view determines which type should be used for data.
     * @return It returns an object of type ViewHolder per visual entry in the recycler view.
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(mActivity).inflate(R.layout.single_row_report_product, parent, false)
        return MyViewHolder(view)
    }

    /**
     * <h>OnBindViewHolder</h>
     *
     *
     * In this method Every visible entry in a recycler view is filled with the
     * correct data model item by the adapter. Once a data item becomes visible,
     * the adapter assigns this data to the individual widgets which he inflated
     * earlier.
     *
     * @param holder The referece of MyViewHolder class of current class.
     * @param position The position of particular item
     */
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        if (position == mSelectedItem) {
            arrayListReportItem[position].isItemSelected = true
            holder.radio_reportReason.isChecked = true
            holder.rL_reasonDesc.visibility = View.VISIBLE
        } else {
            arrayListReportItem[position].isItemSelected = false
            holder.radio_reportReason.isChecked = false
            holder.rL_reasonDesc.visibility = View.GONE
        }

        // show user report reason given by user
        holder.eT_description.setText(arrayListReportItem[position].reportReasonByUser)

        // save the report reason into array
        holder.eT_description.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable) {
                arrayListReportItem[holder.adapterPosition].reportReasonByUser = holder.eT_description.text.toString()
            }
        })
        holder.radio_reportReason.text = arrayListReportItem[position].reportReason
    }

    /**
     * Return the size of your dataset
     * @return the total number of rows
     */
    override fun getItemCount(): Int {
        return arrayListReportItem.size
    }

    /**
     * Recycler View item variables
     */
    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        var radio_reportReason: RadioButton
        var rL_reasonDesc: RelativeLayout
        var eT_description: EditText
        override fun onClick(v: View) {
            mSelectedItem = adapterPosition
            notifyItemRangeChanged(0, arrayListReportItem.size)
        }

        init {
            radio_reportReason = itemView.findViewById<View>(R.id.radio_reportReason) as RadioButton
            radio_reportReason.setOnClickListener(this)
            rL_reasonDesc = itemView.findViewById<View>(R.id.rL_reasonDesc) as RelativeLayout
            eT_description = itemView.findViewById<View>(R.id.eT_description) as EditText
        }
    }

}