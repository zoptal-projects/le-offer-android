package com.zoptal.cellableapp.adapter

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.text.SpannableString
import android.text.Spanned
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.main.activity.SelfProfileActivity
import com.zoptal.cellableapp.main.activity.UserProfileActivity
import com.zoptal.cellableapp.main.activity.products.ProductDetailsActivity
import com.zoptal.cellableapp.pojo_class.explore_following_pojo.FollowingResponseDatas
import com.zoptal.cellableapp.utility.CircleTransform
import com.zoptal.cellableapp.utility.CommonClass
import com.zoptal.cellableapp.utility.OnLoadMoreListener
import com.zoptal.cellableapp.utility.SessionManager
import java.util.*

/**
 * <h>FollowingFragRvAdap</h>
 *
 *
 * This class is getting called from FollowingFrag. In this recyclerview adapter class we used to inflate
 * single_row_following_activity layout and shows the all following activity done by others users.
 *
 * @since 4/17/2017
 */
class FollowingFragRvAdap(private val mActivity: Activity, private val al_following_data: ArrayList<FollowingResponseDatas>, mRecyclerView: RecyclerView) : RecyclerView.Adapter<FollowingFragRvAdap.ViewHolder>() {

    // load more parameters
    private var onLoadMoreListener: OnLoadMoreListener? = null
    private var isLoading = false
    private var lastVisibleItem = 0
    private var totalItemCount = 0
    private val visibleThresold = 5
    private val mSessionManager: SessionManager
    fun setOnLoadMore(listener: OnLoadMoreListener?) {
        onLoadMoreListener = listener
    }

    fun setLoaded() {
        isLoading = false
    }

    /**
     * <h>OnCreateViewHolder</h>
     *
     *
     * In this method The adapter prepares the layout of the items by inflating the correct
     * layout for the individual data elements.
     *
     * @param parent A ViewGroup is a special view that can contain other views (called children.)
     * @param viewType Within the getItemViewType method the recycler view determines which type should be used for data.
     * @return It returns an object of type ViewHolder per visual entry in the recycler view.
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.single_row_following_activity, parent, false)
        return ViewHolder(view)
    }

    /**
     * <h>OnBindViewHolder</h>
     *
     *
     * In this method Every visible entry in a recycler view is filled with the
     * correct data model item by the adapter. Once a data item becomes visible,
     * the adapter assigns this data to the individual widgets which he inflated
     * earlier.
     *
     * @param holder The referece of MyViewHolder class of current class.
     * @param position The position of particular item
     */
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var time: String?
        val user1_name: String
        val user1_profilePic: String?
        val user2_profilePic: String
        val notificationType: String?
        val mainUrl: String
        val postId: String
        val user2_name: String
        user1_name = al_following_data[position].user1_username
        user1_profilePic = al_following_data[position].user1_profilePicUrl
        user2_name = al_following_data[position].user2_username
        user2_profilePic = al_following_data[position].user2_profilePicUrl
        mainUrl = al_following_data[position].mainUrl
        time = al_following_data[position].createdOn
        notificationType = al_following_data[position].notificationType
        postId = al_following_data[position].postId
        println("$TAG user2 profilePic=$user2_profilePic")

        // set user1 profile pic
        if (user1_profilePic != null && !user1_profilePic.isEmpty()) Picasso.with(mActivity)
                .load(user1_profilePic)
                .transform(CircleTransform())
                .placeholder(R.drawable.default_circle_img)
                .error(R.drawable.default_circle_img)
                .into(holder.iV_user1_profilePic)
        holder.iV_user1_profilePic.setOnClickListener { /*Intent intent=new Intent(mActivity, UserProfileActivity.class);
                intent.putExtra("membername",user1_name);
                mActivity.startActivity(intent);*/
            openUserProfileScreen(user1_name)
        }

        // set activity complete message
        setActivityMessage(holder.tV_activity, user1_name, notificationType, user2_name)
        if (notificationType != null && !notificationType.isEmpty()) {
            when (notificationType) {
                "2" -> setRightSideImage(mainUrl, holder.iV_user2_profilePic)
                "3" -> setRightSideImage(user2_profilePic, holder.iV_user2_profilePic)
                "5" -> setRightSideImage(mainUrl, holder.iV_user2_profilePic)
            }
        }

        // set on post click
        holder.iV_user2_profilePic.setOnClickListener {
            val intent: Intent
            assert(notificationType != null)
            when (notificationType) {
                "2" -> {
                    intent = Intent(mActivity, ProductDetailsActivity::class.java)
                    intent.putExtra("image", mainUrl)
                    intent.putExtra("postId", postId)
                    mActivity.startActivity(intent)
                }
                "3" ->                         /*intent=new Intent(mActivity, UserProfileActivity.class);
                        intent.putExtra("membername",user2_name);
                        mActivity.startActivity(intent);*/openUserProfileScreen(user2_name)
                "5" -> {
                    intent = Intent(mActivity, ProductDetailsActivity::class.java)
                    intent.putExtra("image", mainUrl)
                    intent.putExtra("postId", postId)
                    mActivity.startActivity(intent)
                }
            }
        }

        // set time
        if (time != null) time = CommonClass.getTimeDifference(time)
        holder.tV_time.text = time
    }

    private fun setRightSideImage(imageUrl: String?, imageView: ImageView) {
        // set user2 profile pic
        if (imageUrl != null && !imageUrl.isEmpty()) {
            Picasso.with(mActivity)
                    .load(imageUrl)
                    .placeholder(R.drawable.default_image)
                    .error(R.drawable.default_image)
                    .into(imageView)
        }
    }

    /**
     * <h>SetActivityMessage</h>
     *
     *
     * In this method we use SpannableString to make clickable part-wise on TextView.
     *
     * @param tV_activity The TextView on which operation occured
     * @param user1_username The first user name
     * @param user2_username The second user name
     */
    private fun setActivityMessage(tV_activity: TextView, user1_username: String, notificationType: String?, user2_username: String) {
        // remove extra spaces
        var user1_username: String? = user1_username
        var user2_username: String? = user2_username
        if (user1_username != null && !user1_username.isEmpty()) user1_username = user1_username.trim { it <= ' ' }
        if (user2_username != null && !user2_username.isEmpty()) user2_username = user2_username.trim { it <= ' ' }
        var user1Length = 0
        if (user1_username != null) user1Length = user1_username.length
        var customMessage = ""
        if (notificationType != null && !notificationType.isEmpty()) {
            when (notificationType) {
                "2" -> customMessage = if (user1_username == user2_username) {
                    user1_username + " " + mActivity.resources.getString(R.string.liked) + " " + mActivity.getString(R.string.own) + " " + mActivity.resources.getString(R.string.post)
                } else {
                    user1_username + " " + mActivity.resources.getString(R.string.liked) + " " + user2_username + " " + mActivity.resources.getString(R.string.post)
                }
                "3" -> customMessage = user1_username + " " + mActivity.resources.getString(R.string.startes_following) + " " + user2_username
                "5" -> customMessage = user1_username + " " + mActivity.resources.getString(R.string.left_a_review_on) + " " + user2_username + " " + mActivity.resources.getString(R.string.post)
            }
        }
        val ss = SpannableString(customMessage)
        ss.setSpan(MyClickableSpan(user1_username!!), 0, user1Length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        //ss.setSpan(new MyClickableSpan(user2_username),user1Length+noitificationMsgLen+2,user1Length+noitificationMsgLen+user2Length+2,Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        tV_activity.text = ss
        tV_activity.movementMethod = LinkMovementMethod.getInstance()
        tV_activity.highlightColor = Color.TRANSPARENT
    }

    /**
     * <h>MyClickableSpan</h>
     *
     *
     * This class extends ClickableSpan from that we have two overrided method
     * 1> onClick() in which we do clickable oparation
     * 2> updateDrawState() in this method we do operation like change TextView Style
     *
     */
    private inner class MyClickableSpan internal constructor(private val membername: String) : ClickableSpan() {
        override fun onClick(widget: View) {
            /*Intent intent=new Intent(mActivity, UserProfileActivity.class);
            intent.putExtra("membername",membername);
            mActivity.startActivity(intent);*/
            openUserProfileScreen(membername)
        }

        override fun updateDrawState(ds: TextPaint) {
            ds.isUnderlineText = false
            ds.isFakeBoldText = true
        }

    }

    private fun openUserProfileScreen(membername: String) {
        val intent: Intent
        if (mSessionManager.isUserLoggedIn && mSessionManager.userName == membername) {
            intent = Intent(mActivity, SelfProfileActivity::class.java)
            intent.putExtra("membername", membername)
        } else {
            intent = Intent(mActivity, UserProfileActivity::class.java)
            intent.putExtra("membername", membername)
        }
        mActivity.startActivity(intent)
    }

    /**
     * Return the size of your dataset
     * @return the total number of rows
     */
    override fun getItemCount(): Int {
        return al_following_data.size
    }

    /**
     * In this class we declare and initialize xml all variables.
     */
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var iV_user1_profilePic: ImageView
        var iV_user2_profilePic: ImageView
        var tV_activity: TextView
        var tV_time: TextView

        init {
            iV_user1_profilePic = itemView.findViewById<View>(R.id.iV_user1_profilePic) as ImageView
            iV_user2_profilePic = itemView.findViewById<View>(R.id.iV_user2_profilePic) as ImageView
            iV_user2_profilePic.layoutParams.height = CommonClass.getDeviceWidth(mActivity) / 6
            iV_user2_profilePic.layoutParams.width = CommonClass.getDeviceWidth(mActivity) / 6
            tV_activity = itemView.findViewById<View>(R.id.tV_activity) as TextView
            tV_time = itemView.findViewById<View>(R.id.tV_time) as TextView
        }
    }

    companion object {
        private val TAG = FollowingFragRvAdap::class.java.simpleName
    }

    /**
     * <h>FollowingFragRvAdap</h>
     *
     *
     * This is simple constructor to initailize list datas and context.
     *
     * @param mActivity The current context
     * @param al_following_data The response datas
     */
    init {
        mSessionManager = SessionManager(mActivity)
        val layoutManager = mRecyclerView.layoutManager as LinearLayoutManager?
        mRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                totalItemCount = layoutManager!!.itemCount
                lastVisibleItem = layoutManager.findLastVisibleItemPosition()
                println("$TAG is loading=$isLoading totalItemCount=$totalItemCount lastVisibleItem=$lastVisibleItem")
                isLoading = al_following_data.size < 15
                if (!isLoading && totalItemCount <= lastVisibleItem + visibleThresold) {
                    if (onLoadMoreListener != null) {
                        onLoadMoreListener!!.onLoadMore()
                    }
                    isLoading = true
                }
            }
        })
    }
}