package com.zoptal.cellableapp.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.pojo_class.search_people_pojo.SearchPeopleUsers
import com.zoptal.cellableapp.utility.CircleTransform
import com.zoptal.cellableapp.utility.ClickListener
import com.zoptal.cellableapp.utility.CommonClass
import java.util.*

/**
 * <h>SearchPeopleRvAdap</h>
 *
 *
 * This class is called from PeoplesFrag class. In this recyclerview adapter class we used to inflate
 * single_row_search_product layout and shows all searched people.
 *
 * @since 29-Jun-17
 */
class SearchPeopleRvAdap
/**
 *
 *
 * This is simple constructor to initailize list datas and context.
 *
 * @param mActivity The current context
 * @param aL_users The list datas
 */(private val mActivity: Activity, private val aL_users: ArrayList<SearchPeopleUsers>) : RecyclerView.Adapter<SearchPeopleRvAdap.MyViewHolder?>() {
    private var clickListener: ClickListener? = null

    /**
     * <h>OnCreateViewHolder</h>
     *
     *
     * In this method The adapter prepares the layout of the items by inflating the correct
     * layout for the individual data elements.
     *
     * @param parent A ViewGroup is a special view that can contain other views (called children.)
     * @param viewType Within the getItemViewType method the recycler view determines which type should be used for data.
     * @return It returns an object of type ViewHolder per visual entry in the recycler view.
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(mActivity).inflate(R.layout.single_row_search_product, parent, false)
        return MyViewHolder(view)
    }

    /**
     * <h>OnBindViewHolder</h>
     *
     *
     * In this method Every visible entry in a recycler view is filled with the
     * correct data model item by the adapter. Once a data item becomes visible,
     * the adapter assigns this data to the individual widgets which he inflated
     * earlier.
     *
     * @param holder The referece of MyViewHolder class of current class.
     * @param position The position of particular item
     */
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val username: String?
        val userImage: String?
        val fullName: String?
        username = aL_users[position].username
        userImage = aL_users[position].profilePicUrl
        fullName = aL_users[position].fullName

        // set Profile pic
        if (userImage != null && !userImage.isEmpty()) Picasso.with(mActivity)
                .load(userImage)
                .placeholder(R.drawable.default_circle_img)
                .error(R.drawable.default_circle_img)
                .transform(CircleTransform())
                .into(holder.image)

        // set full name
        if (fullName != null && !fullName.isEmpty()) holder.tV_heading.text = fullName

        // set user name
        if (username != null && !username.isEmpty()) holder.tV_subHeading.text = username
    }

    /**
     * Return the size of your dataset
     * @return the total number of rows
     */
    override fun getItemCount(): Int {
        return aL_users.size
    }

    /**
     * Recycler View item variables
     */
    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var image: ImageView
        var tV_heading: TextView
        var tV_subHeading: TextView

        init {
            itemView.setOnClickListener { v -> if (clickListener != null) clickListener!!.onItemClick(v, adapterPosition) }
            image = itemView.findViewById<View>(R.id.image) as ImageView
            image.layoutParams.width = CommonClass.getDeviceWidth(mActivity) / 7
            image.layoutParams.height = CommonClass.getDeviceWidth(mActivity) / 7
            tV_heading = itemView.findViewById<View>(R.id.tV_heading) as TextView
            tV_subHeading = itemView.findViewById<View>(R.id.tV_subHeading) as TextView
        }
    }

    fun setOnItemClick(listener: ClickListener?) {
        clickListener = listener
    }

}