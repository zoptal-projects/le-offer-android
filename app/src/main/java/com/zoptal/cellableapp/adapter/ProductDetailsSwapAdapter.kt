package com.zoptal.cellableapp.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.pojo_class.product_details_pojo.SwapPost
import java.util.*

/**
 * Created by ${3embed} on ${27-10-2017}.
 * Banglore
 */
class ProductDetailsSwapAdapter(private val arrayList: ArrayList<SwapPost>, private val mActivity: Activity) : RecyclerView.Adapter<ProductDetailsSwapAdapter.MyViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(mActivity).inflate(R.layout.single_row_iwant_item, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.exchangeItemNameTv.text = arrayList[position].swapTitle
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val exchangeItemNameTv: TextView
        private val crossIv: ImageView

        init {
            exchangeItemNameTv = itemView.findViewById<View>(R.id.exchangeItemNameTv) as TextView
            crossIv = itemView.findViewById<View>(R.id.crossIv) as ImageView
            crossIv.visibility = View.GONE
        }
    }

}