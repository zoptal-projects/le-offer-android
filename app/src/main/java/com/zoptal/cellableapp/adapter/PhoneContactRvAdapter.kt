package com.zoptal.cellableapp.adapter

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.event_bus.EventBusDatasHandler
import com.zoptal.cellableapp.main.activity.SelfProfileActivity
import com.zoptal.cellableapp.main.activity.UserProfileActivity
import com.zoptal.cellableapp.pojo_class.phone_contact_pojo.PhoneContactData
import com.zoptal.cellableapp.utility.*
import java.util.*

/**
 * <h>PhoneContactRvAdapter</h>
 *
 *
 * In class is called from PhoneContactsActivity class. In this recyclerview adapter class we used to inflate
 * single_row_discover_people layout and shows the friends from contact list who uses this app.
 *
 * @since 04-Jul-17
 */
class PhoneContactRvAdapter(private val mActivity: Activity, private val arrayListContacts: ArrayList<PhoneContactData>, var followingCount: Int) : RecyclerView.Adapter<PhoneContactRvAdapter.MyViewHolder>() {
    private val apiCall: ApiCall
    private val mEventBusDatasHandler: EventBusDatasHandler
    private val mSessionManager: SessionManager

    /**
     * <h>OnCreateViewHolder</h>
     *
     *
     * In this method The adapter prepares the layout of the items by inflating the correct
     * layout for the individual data elements.
     *
     * @param parent A ViewGroup is a special view that can contain other views (called children.)
     * @param viewType Within the getItemViewType method the recycler view determines which type should be used for data.
     * @return It returns an object of type ViewHolder per visual entry in the recycler view.
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(mActivity).inflate(R.layout.single_row_discover_people, parent, false)
        return MyViewHolder(view)
    }

    /**
     * <h>OnBindViewHolder</h>
     *
     *
     * In this method Every visible entry in a recycler view is filled with the
     * correct data model item by the adapter. Once a data item becomes visible,
     * the adapter assigns this data to the individual widgets which he inflated
     * earlier.
     *
     * @param holder The referece of MyViewHolder class of current class.
     * @param position The position of particular item
     */
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val profilePicUrl: String?
        val followsFlag: String?
        val postedByUserName: String?
        val postedByUserFullName: String?
        profilePicUrl = arrayListContacts[position].profilePicUrl
        followsFlag = arrayListContacts[position].following
        postedByUserName = arrayListContacts[position].membername
        postedByUserFullName = arrayListContacts[position].fullName

        // Profile pic url
        if (profilePicUrl != null && !profilePicUrl.isEmpty()) Picasso.with(mActivity)
                .load(profilePicUrl)
                .transform(CircleTransform())
                .placeholder(R.drawable.default_circle_img)
                .error(R.drawable.default_circle_img)
                .into(holder.iV_profilePicUrl)

        // follow and following icon
        if (followsFlag != null && followsFlag == "0") {
            holder.rL_follow.setBackgroundResource(R.drawable.rect_purple_color_with_stroke_shape)
            holder.tV_follow.setTextColor(ContextCompat.getColor(mActivity, R.color.blue))
            holder.tV_follow.text = mActivity.resources.getString(R.string.follow)
        } else {
            holder.rL_follow.setBackgroundResource(R.drawable.rect_purple_color_with_solid_shape)
            holder.tV_follow.setTextColor(ContextCompat.getColor(mActivity, R.color.white))
            holder.tV_follow.text = mActivity.resources.getString(R.string.Following)
        }

        // hide row
        holder.tV_hide.setOnClickListener { removeItem(holder.adapterPosition) }
        val arrayListPostData = arrayListContacts[position].postData

        // Follow
        holder.rL_follow.setOnClickListener {
            val url: String
            val mFollowsFlag = arrayListContacts[holder.adapterPosition].following
            if (mFollowsFlag != null && mFollowsFlag == "1") {
                followingCount = followingCount - 1
                holder.tV_hide.visibility = View.VISIBLE
                holder.rL_follow.setBackgroundResource(R.drawable.rect_purple_color_with_stroke_shape)
                holder.tV_follow.setTextColor(ContextCompat.getColor(mActivity, R.color.blue))
                holder.tV_follow.text = mActivity.resources.getString(R.string.follow)
                arrayListContacts[holder.adapterPosition].following = "0"
                url = ApiUrl.UNFOLLOW + postedByUserName
                if (arrayListPostData != null && arrayListPostData.size > 0) mEventBusDatasHandler.setSocialDatasFromContactsFriends(arrayListPostData[0], false)
                apiCall.followUserApi(url)
            } else {
                followingCount = followingCount + 1
                holder.tV_hide.visibility = View.GONE
                holder.rL_follow.setBackgroundResource(R.drawable.rect_purple_color_with_solid_shape)
                holder.tV_follow.setTextColor(ContextCompat.getColor(mActivity, R.color.white))
                holder.tV_follow.text = mActivity.resources.getString(R.string.Following)
                arrayListContacts[holder.adapterPosition].following = "1"
                url = ApiUrl.FOLLOW + postedByUserName
                if (arrayListPostData != null && arrayListPostData.size > 0) mEventBusDatasHandler.setSocialDatasFromContactsFriends(arrayListPostData[0], true)
                apiCall.followUserApi(url)
            }
        }

        // user name
        if (postedByUserName != null) holder.tV_postedByUserName.text = postedByUserName

        // full name
        if (postedByUserFullName != null) holder.tV_postedByUserFullName.text = postedByUserFullName

        // Post datas
        if (arrayListPostData != null && arrayListPostData.size > 0) {
            holder.horizontal_posts.visibility = View.VISIBLE
            holder.tV_noPost.visibility = View.GONE
            holder.view_divider.visibility = View.VISIBLE
            val layoutInflater = LayoutInflater.from(mActivity)
            holder.linear_postData.removeAllViews()
            for (postCount in arrayListPostData.indices) {
                val view = layoutInflater.inflate(R.layout.single_row_images, null, false)
                val thumbnailImageUrl = arrayListPostData[postCount].thumbnailImageUrl
                val iV_thumbnailImage = view.findViewById<View>(R.id.iV_image) as ImageView
                iV_thumbnailImage.layoutParams.width = CommonClass.getDeviceWidth(mActivity) / 5
                iV_thumbnailImage.layoutParams.height = CommonClass.getDeviceWidth(mActivity) / 5
                CommonClass.setMargins(iV_thumbnailImage, 5, 0, 5, 0)
                if (thumbnailImageUrl != null && !thumbnailImageUrl.isEmpty()) {
                    Picasso.with(mActivity)
                            .load(thumbnailImageUrl)
                            .transform(RoundedCornersTransform())
                            .placeholder(R.drawable.default_image)
                            .error(R.drawable.default_image)
                            .into(iV_thumbnailImage)
                    holder.linear_postData.addView(view)
                }
            }
        } else {
            holder.horizontal_posts.visibility = View.GONE
            holder.tV_noPost.visibility = View.VISIBLE
            holder.view_divider.visibility = View.GONE
        }

        // open user profile
        holder.rL_memeberName.setOnClickListener { /*Intent intent=new Intent(mActivity, UserProfileActivity.class);
                intent.putExtra("membername",postedByUserName);
                mActivity.startActivity(intent);*/
            val intent: Intent
            if (mSessionManager.isUserLoggedIn && mSessionManager.userName == postedByUserName) {
                intent = Intent(mActivity, SelfProfileActivity::class.java)
                intent.putExtra("membername", postedByUserName)
            } else {
                intent = Intent(mActivity, UserProfileActivity::class.java)
                intent.putExtra("membername", postedByUserName)
            }
            mActivity.startActivity(intent)
        }
    }

    /**
     * Return the size of your dataset
     * @return the total number of rows
     */
    override fun getItemCount(): Int {
        return arrayListContacts.size
    }

    /**
     * <h>MyViewHolder</h>
     *
     *
     * In this class we used to declare and assign the xml variables.
     *
     */
    inner class MyViewHolder internal constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val iV_profilePicUrl: ImageView
        val tV_postedByUserName: TextView
        val tV_postedByUserFullName: TextView
        val tV_hide: TextView
        val tV_follow: TextView
        val tV_noPost: TextView
        val linear_postData: LinearLayout
        val rL_follow: RelativeLayout
        val rL_memeberName: RelativeLayout
        val horizontal_posts: HorizontalScrollView
        val view_divider: View

        init {
            iV_profilePicUrl = itemView.findViewById<View>(R.id.iV_profilePicUrl) as ImageView
            iV_profilePicUrl.layoutParams.width = CommonClass.getDeviceWidth(mActivity) / 7
            iV_profilePicUrl.layoutParams.height = CommonClass.getDeviceWidth(mActivity) / 7
            rL_follow = itemView.findViewById<View>(R.id.relative_follow) as RelativeLayout
            rL_memeberName = itemView.findViewById<View>(R.id.rL_memeberName) as RelativeLayout
            tV_postedByUserName = itemView.findViewById<View>(R.id.tV_postedByUserName) as TextView
            tV_postedByUserFullName = itemView.findViewById<View>(R.id.tV_postedByUserFullName) as TextView
            tV_hide = itemView.findViewById<View>(R.id.tV_hide) as TextView
            tV_hide.visibility = View.GONE
            tV_follow = itemView.findViewById<View>(R.id.tV_follow) as TextView
            linear_postData = itemView.findViewById<View>(R.id.linear_postData) as LinearLayout
            horizontal_posts = itemView.findViewById<View>(R.id.horizontal_posts) as HorizontalScrollView
            view_divider = itemView.findViewById(R.id.view_divider)
            tV_noPost = itemView.findViewById<View>(R.id.tV_noPost) as TextView
        }
    }

    private fun removeItem(position: Int) {
        arrayListContacts.removeAt(position)
        notifyItemRemoved(position)
        notifyItemRangeChanged(position, arrayListContacts.size)
    }

    /**
     * <h>PhoneContactRvAdapter</h>
     *
     *
     * This is simple constructor to initailize list datas and context.
     *
     * @param mActivity The current context
     * @param arrayListContacts The list datas
     */
    init {
        mEventBusDatasHandler = EventBusDatasHandler(mActivity)
        apiCall = ApiCall(mActivity)
        mSessionManager = SessionManager(mActivity)
    }
}