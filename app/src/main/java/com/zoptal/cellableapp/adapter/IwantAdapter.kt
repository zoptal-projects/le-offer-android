package com.zoptal.cellableapp.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.main.activity.PostProductActivity
import com.zoptal.cellableapp.pojo_class.product_details_pojo.SwapPost
import java.util.*

/**
 * Created by ${3embed} on ${27-10-2017}.
 * Banglore
 */
class IwantAdapter(private val exchangeArrayList: ArrayList<SwapPost>, private val mActivity: PostProductActivity) : RecyclerView.Adapter<RecyclerView.ViewHolder?>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        var holder: RecyclerView.ViewHolder? = null
        when (viewType) {
            TYPE_DATA -> {
                val itemView = LayoutInflater.from(parent.context).inflate(R.layout.single_row_iwant_item, parent, false)
                holder = MyViewHolder(itemView)
            }
            TYPE_ADD_MORE -> {
                val empty_view = LayoutInflater.from(parent.context).inflate(R.layout.single_row_iwant_add_more, parent, false)
                holder = AddMore(empty_view)
            }
        }
        return holder!!
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder!!.itemViewType) {
            TYPE_DATA -> {
                val itemHolder = holder as MyViewHolder?
                initItemView(itemHolder)
            }
            TYPE_ADD_MORE -> {
                val addMore = holder as AddMore?
                initAddMoreView(addMore)
            }
        }
    }

    private fun initAddMoreView(addMore: AddMore?) {
        addMore!!.addMoreLl.setOnClickListener { mActivity.addMoreWillingSwapItems() }
    }

    private fun initItemView(viewHolder: MyViewHolder?) {
        val position = viewHolder!!.adapterPosition
        val iwantItemPojo = exchangeArrayList[position]
        viewHolder.exchangeItemNameTv.text = iwantItemPojo.swapTitle
        viewHolder.crossIv.setOnClickListener { mActivity.iWantAdapterClick(iwantItemPojo.swapPostId!!, position) }
    }

    override fun getItemViewType(position: Int): Int {
        return if (exchangeArrayList[position].itemType) {
            TYPE_ADD_MORE
        } else {
            TYPE_DATA
        }
    }

    override fun getItemCount(): Int {
        return exchangeArrayList.size
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val exchangeItemNameTv: TextView
        val crossIv: ImageView

        init {
            exchangeItemNameTv = itemView.findViewById<View>(R.id.exchangeItemNameTv) as TextView
            crossIv = itemView.findViewById<View>(R.id.crossIv) as ImageView
        }
    }

    inner class AddMore(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val addMoreLl: LinearLayout

        init {
            addMoreLl = itemView.findViewById<View>(R.id.addMoreLl) as LinearLayout
        }
    }

    companion object {
        private const val TYPE_DATA = 0
        private const val TYPE_ADD_MORE = 1
    }

}