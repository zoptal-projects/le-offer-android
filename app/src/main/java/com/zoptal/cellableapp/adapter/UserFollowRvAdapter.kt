package com.zoptal.cellableapp.adapter

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.squareup.picasso.Picasso
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.event_bus.EventBusDatasHandler
import com.zoptal.cellableapp.main.activity.SelfProfileActivity
import com.zoptal.cellableapp.main.activity.UserProfileActivity
import com.zoptal.cellableapp.pojo_class.user_follow_pojo.FollowResponseDatas
import com.zoptal.cellableapp.utility.*
import java.util.*

/**
 * <h>HomeFragRvAdapter</h>
 *
 *
 * In class is called from SelfFollowingActivity class. In this recyclerview adapter class we used to inflate
 * single_row_followers layout and shows follower or following lists.
 *
 *
 * @since 4/21/2017
 */
class UserFollowRvAdapter(private val mActivity: Activity, private val arrayListFollow: ArrayList<FollowResponseDatas>, var followingCount: Int) : RecyclerView.Adapter<UserFollowRvAdapter.MyViewHolder?>() {
    private val apiCall: ApiCall
    private val mSessionManager: SessionManager
    private val mEventBusDatasHandler: EventBusDatasHandler

    /**
     * <h>OnCreateViewHolder</h>
     *
     *
     * In this method The adapter prepares the layout of the items by inflating the correct
     * layout for the individual data elements.
     *
     *
     * @param parent   A ViewGroup is a special view that can contain other views (called children.)
     * @param viewType Within the getItemViewType method the recycler view determines which type should be used for data.
     * @return It returns an object of type ViewHolder per visual entry in the recycler view.
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val exploreView = LayoutInflater.from(parent.context).inflate(R.layout.single_row_followers, parent, false)
        return MyViewHolder(exploreView)
    }

    /**
     * <h>OnBindViewHolder</h>
     *
     *
     * In this method Every visible entry in a recycler view is filled with the
     * correct data model item by the adapter. Once a data item becomes visible,
     * the adapter assigns this data to the individual widgets which he inflated
     * earlier.
     *
     *
     * @param holder   The referece of MyViewHolder class of current class.
     * @param position The position of particular item
     */
   override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var fullName: String?
        val followsFlag: String?
        val profilePicUrl: String?
        val username: String?
        fullName = arrayListFollow[position].fullName
        if (fullName == null || fullName.isEmpty()) fullName = arrayListFollow[position].fullname
        username = arrayListFollow[position].username
        profilePicUrl = arrayListFollow[position].profilePicUrl
        followsFlag = arrayListFollow[position].userFollowRequestStatus

        // full name
        if (fullName != null) holder.tV_fullName.text = fullName

        // user name
        if (username != null) holder.tV_userName.text = username

        // set user pic
        if (profilePicUrl != null && !profilePicUrl.isEmpty()) Picasso.with(mActivity)
                .load(profilePicUrl)
                .transform(CircleTransform())
                .placeholder(R.drawable.default_circle_img)
                .error(R.drawable.default_circle_img)
                .into(holder.iV_userPic)

        // for own hide the follow button
        if (username != null && username == mSessionManager.userName) holder.rL_follow.visibility = View.GONE else holder.rL_follow.visibility = View.VISIBLE

        // show following button if the flag is one
        if (followsFlag != null && followsFlag == "1") {
            holder.rL_follow.setBackgroundResource(R.drawable.rect_purple_color_with_solid_shape)
            holder.tV_follow.setTextColor(ContextCompat.getColor(mActivity, R.color.white))
            holder.tV_follow.text = mActivity.resources.getString(R.string.Following)
        } else {
            holder.rL_follow.setBackgroundResource(R.drawable.rect_purple_color_with_stroke_shape)
            holder.tV_follow.setTextColor(ContextCompat.getColor(mActivity, R.color.blue))
            holder.tV_follow.text = mActivity.resources.getString(R.string.follow)
        }

        // Follow or unfollow
        holder.rL_follow.setOnClickListener {
            val url: String
            if (arrayListFollow[holder.getAdapterPosition()].userFollowRequestStatus != null) {
                if (arrayListFollow[holder.getAdapterPosition()].userFollowRequestStatus == "1") {
                    unfollowUserAlert(username, profilePicUrl, holder.rL_follow, holder.tV_follow, holder.getAdapterPosition())
                } else {
                    followingCount = followingCount + 1
                    arrayListFollow[holder.getAdapterPosition()].followsFlag = "1"
                    arrayListFollow[holder.getAdapterPosition()].userFollowRequestStatus = "1"
                    url = ApiUrl.FOLLOW + username
                    apiCall.followUserApi(url)
                    holder.rL_follow.setBackgroundResource(R.drawable.rect_purple_color_with_solid_shape)
                    holder.tV_follow.setTextColor(ContextCompat.getColor(mActivity, R.color.white))
                    holder.tV_follow.text = mActivity.resources.getString(R.string.Following)
                    if (arrayListFollow[holder.getAdapterPosition()].postData!!.size > 0) {
                        for (postCount in arrayListFollow[holder.getAdapterPosition()].postData!!.indices) {
                            mEventBusDatasHandler.setSocialDatasFromDiscovery(username, profilePicUrl, arrayListFollow[holder.adapterPosition].postData!![postCount], true)
                        }
                    }
                }
            } else {
                if (arrayListFollow[holder.getAdapterPosition()].followedBack == "1") {
                    unfollowUserAlert(username, profilePicUrl, holder.rL_follow, holder.tV_follow, holder.getAdapterPosition())
                } else {
                    followingCount = followingCount + 1
                    arrayListFollow[holder.getAdapterPosition()].followsFlag = "1"
                    arrayListFollow[holder.getAdapterPosition()].userFollowRequestStatus = "1"
                    url = ApiUrl.FOLLOW + username
                    apiCall.followUserApi(url)
                    holder.rL_follow.setBackgroundResource(R.drawable.rect_purple_color_with_solid_shape)
                    holder.tV_follow.setTextColor(ContextCompat.getColor(mActivity, R.color.white))
                    holder.tV_follow.text = mActivity.resources.getString(R.string.Following)
                    if (arrayListFollow[holder.getAdapterPosition()].postData!!.size > 0) {
                        for (postCount in arrayListFollow[holder.getAdapterPosition()].postData!!.indices) {
                            mEventBusDatasHandler.setSocialDatasFromDiscovery(username, profilePicUrl, arrayListFollow[holder.getAdapterPosition()].postData!![postCount], true)
                        }
                    }
                }
            }
        }

        // User Profile
        holder.rL_user.setOnClickListener {
            val intent: Intent
            if (mSessionManager.isUserLoggedIn && mSessionManager.userName == username) {
                intent = Intent(mActivity, SelfProfileActivity::class.java)
                intent.putExtra("membername", username)
            } else {
                intent = Intent(mActivity, UserProfileActivity::class.java)
                intent.putExtra("membername", username)
            }
            mActivity.startActivity(intent)
        }
    }

    /**
     * <h>unfollowUserAlert</h>
     *
     *
     * In this method we used to open a simple dialog pop-up to show
     * alert to unfollow
     *
     */
    private fun unfollowUserAlert(membername: String?, memberProfilePicUrl: String?, rL_follow: RelativeLayout, tV_follow: TextView, position: Int) {
        val unfollowUserDialog = Dialog(mActivity)
        unfollowUserDialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        unfollowUserDialog.setContentView(R.layout.dialog_unfollow_user)
        unfollowUserDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        unfollowUserDialog.window!!.setLayout((CommonClass.getDeviceWidth(mActivity) * 0.9).toInt(), RelativeLayout.LayoutParams.WRAP_CONTENT)

        // set user pic
        val imageViewPic = unfollowUserDialog.findViewById<View>(R.id.iV_userPic) as ImageView
        imageViewPic.layoutParams.width = CommonClass.getDeviceWidth(mActivity) / 5
        imageViewPic.layoutParams.height = CommonClass.getDeviceWidth(mActivity) / 5

        // posted by pic
        if (memberProfilePicUrl != null && !memberProfilePicUrl.isEmpty()) Picasso.with(mActivity)
                .load(memberProfilePicUrl)
                .transform(CircleTransform())
                .placeholder(R.drawable.default_profile_image)
                .error(R.drawable.default_profile_image)
                .into(imageViewPic)

        // set user name
        val tV_userName = unfollowUserDialog.findViewById<View>(R.id.tV_userName) as TextView
        if (membername != null && !membername.isEmpty()) {
            val setUserName = mActivity.resources.getString(R.string.at_the_rate) + membername + mActivity.resources.getString(R.string.question_mark)
            tV_userName.text = setUserName
        }

        // set cancel button
        val tV_cancel = unfollowUserDialog.findViewById<View>(R.id.tV_cancel) as TextView
        tV_cancel.setOnClickListener { unfollowUserDialog.dismiss() }

        // set done button
        val tV_unfollow = unfollowUserDialog.findViewById<View>(R.id.tV_unfollow) as TextView
        tV_unfollow.setOnClickListener {
            followingCount = followingCount - 1
            //followingCount--;
            Log.d(TAG, "onClick: following count $followingCount")
            val url = ApiUrl.UNFOLLOW + membername
            apiCall.followUserApi(url)
            rL_follow.setBackgroundResource(R.drawable.rect_purple_color_with_stroke_shape)
            tV_follow.setTextColor(ContextCompat.getColor(mActivity, R.color.blue))
            tV_follow.text = mActivity.resources.getString(R.string.follow)
            arrayListFollow[position].followsFlag = "0"
            arrayListFollow[position].userFollowRequestStatus = "0"
            if (arrayListFollow[position].postData!!.size > 0) {
                for (postCount in arrayListFollow[position].postData!!.indices) {
                    mEventBusDatasHandler.setSocialDatasFromDiscovery(membername, memberProfilePicUrl, arrayListFollow[position].postData!![postCount], false)
                }
            }
            unfollowUserDialog.dismiss()
        }
        unfollowUserDialog.show()
    }

    override fun getItemCount(): Int {
        return arrayListFollow.size
    }


    /**
     * <h>MyViewHolder</h>
     *
     *
     * In this class we used to declare and assign the xml variables.
     *
     */
    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var iV_userPic: ImageView
        var tV_fullName: TextView
        var tV_userName: TextView
        var tV_follow: TextView
        var rL_follow: RelativeLayout
        var rL_user: RelativeLayout

        init {
            iV_userPic = itemView.findViewById<View>(R.id.iV_userPic) as ImageView
            iV_userPic.layoutParams.width = CommonClass.getDeviceWidth(mActivity) / 7
            iV_userPic.layoutParams.height = CommonClass.getDeviceWidth(mActivity) / 7
            tV_fullName = itemView.findViewById<View>(R.id.tV_fullName) as TextView
            tV_userName = itemView.findViewById<View>(R.id.tV_userName) as TextView
            rL_follow = itemView.findViewById<View>(R.id.relative_follow) as RelativeLayout
            rL_user = itemView.findViewById<View>(R.id.rL_user) as RelativeLayout
            tV_follow = itemView.findViewById<View>(R.id.tV_follow) as TextView
            //rL_follow= (RelativeLayout) itemView.findViewById(rL_follow);
        }
    }

    companion object {
        private val TAG = UserFollowRvAdapter::class.java.simpleName
    }

    /**
     * <h>CurrencyRvAdap</h>
     *
     *
     * This is simple constructor to initailize list datas and context.
     *
     *
     * @param mActivity       The current context
     * @param arrayListFollow The list datas
     */
    init {
        mSessionManager = SessionManager(mActivity)
        apiCall = ApiCall(mActivity)
        mEventBusDatasHandler = EventBusDatasHandler(mActivity)
        println(TAG + " " + "al size=" + arrayListFollow.size)
    }
}