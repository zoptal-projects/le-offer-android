package com.zoptal.cellableapp.adapter

import android.app.Activity
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.zoptal.cellableapp.main.activity.products.ProductDetailFragment
import com.zoptal.cellableapp.pojo_class.home_explore_pojo.ExploreResponseDatas
import java.util.*

/**
 * Created by embed on 20/8/18.
 */
class ProductDetailPagerAdap // public ProductDetailFragment fragment;
(fm: FragmentManager?, private val arrayListExploreDatas: ArrayList<ExploreResponseDatas>, var mActivity: Activity) : FragmentPagerAdapter(fm!!) {
    override fun getItem(position: Int): Fragment {
        val fragment = ProductDetailFragment()
        val bundle = Bundle()
        bundle.putSerializable("exploreData", arrayListExploreDatas[position])
        fragment.arguments=bundle
        return fragment
    }

    override fun getCount(): Int {
        return arrayListExploreDatas.size
    }

    override fun getItemPosition(`object`: Any): Int {
        return super.getItemPosition(`object`)
    }

}