package com.zoptal.cellableapp.adapter

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.event_bus.EventBusDatasHandler
import com.zoptal.cellableapp.main.activity.FacebookFriendsActivity
import com.zoptal.cellableapp.main.activity.PhoneContactsActivity
import com.zoptal.cellableapp.main.activity.SelfProfileActivity
import com.zoptal.cellableapp.main.activity.UserProfileActivity
import com.zoptal.cellableapp.pojo_class.discovery_people_pojo.DiscoverPeopleResponse
import com.zoptal.cellableapp.utility.*
import java.util.*

/**
 * <h>DiscoveryPeopleRvAdapter</h>
 *
 *
 * This class is called from myprofile screen. In this class we show the friends
 * list and follow button.
 *
 * @since 4/27/2017
 */
class DiscoveryPeopleRvAdapter(private val mActivity: Activity, private val arrayListDiscoverData: ArrayList<DiscoverPeopleResponse>, var followingCount: Int) : RecyclerView.Adapter<RecyclerView.ViewHolder?>() {
    private val apiCall: ApiCall
    @JvmField
    var tV_fb_friends_count: TextView? = null
    @JvmField
    var tV_contact_friend_count: TextView? = null
    private val mEventBusDatasHandler: EventBusDatasHandler
    private val mSessionManager: SessionManager
    private var username: String? = null

    /**
     * <h>OnCreateViewHolder</h>
     *
     *
     * In this method The adapter prepares the layout of the items by inflating the correct
     * layout for the individual data elements.
     *
     * @param parent A ViewGroup is a special view that can contain other views (called children.)
     * @param viewType Within the getItemViewType method the recycler view determines which type should be used for data.
     * @return It returns an object of type ViewHolder per visual entry in the recycler view.
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view: View
        when (viewType) {
            TYPE_HEADER -> {
                view = LayoutInflater.from(mActivity).inflate(R.layout.header_discover_people, parent, false)
                return HeaderViewHolder(view)
            }
            TYPE_ITEM -> {
                view = LayoutInflater.from(mActivity).inflate(R.layout.single_row_discover_people, parent, false)
                return ItemViewHolder(view)
            }
        }
        return null!!
    }

    /**
     * <h>OnBindViewHolder</h>
     *
     *
     * In this method Every visible entry in a recycler view is filled with the
     * correct data model item by the adapter. Once a data item becomes visible,
     * the adapter assigns this data to the individual widgets which he inflated
     * earlier.
     *
     * @param holder The referece of MyViewHolder class of current class.
     * @param position The position of particular item
     */
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        // Header view
        var position = position
        if (holder is HeaderViewHolder) {
            val headerViewHolder = holder

            // Connect to contact
            headerViewHolder.rL_connectToContact.setOnClickListener {
                val intent = Intent(mActivity, PhoneContactsActivity::class.java)
                intent.putExtra("followingCount", followingCount)
                mActivity.startActivityForResult(intent, VariableConstants.CONTACT_FRIEND_REQ_CODE)
            }

            // search facebook friends
            headerViewHolder.rL_connectTofb.setOnClickListener {
                val intent = Intent(mActivity, FacebookFriendsActivity::class.java)
                intent.putExtra("followingCount", followingCount)
                mActivity.startActivityForResult(intent, VariableConstants.FB_FRIEND_REQ_CODE)
            }
        }

        // item view
        if (holder is ItemViewHolder) {
            position = position!! - 1
            val itemViewHolder = holder
            val profilePicUrl: String?
            val followsFlag: String?
            val postedByUserFullName: String?
            var postedByUserName: String?
            profilePicUrl = arrayListDiscoverData[position].profilePicUrl
            followsFlag = arrayListDiscoverData[position].followsFlag
            postedByUserName = arrayListDiscoverData[position].postedByUserName
            username = arrayListDiscoverData[position].postedByUserName
            postedByUserFullName = arrayListDiscoverData[position].postedByUserFullName

            // Profile pic url
            if (profilePicUrl != null && !profilePicUrl.isEmpty()) Picasso.with(mActivity)
                    .load(profilePicUrl)
                    .transform(CircleTransform())
                    .placeholder(R.drawable.default_circle_img)
                    .error(R.drawable.default_circle_img)
                    .into(itemViewHolder.iV_profilePicUrl) else Picasso.with(mActivity)
                    .load(R.drawable.default_circle_img)
                    .transform(CircleTransform())
                    .placeholder(R.drawable.default_circle_img)
                    .error(R.drawable.default_circle_img)
                    .into(itemViewHolder.iV_profilePicUrl)


            // follow and following icon
            if (followsFlag != null && followsFlag == "1") {
                itemViewHolder.rL_follow.setBackgroundResource(R.drawable.rect_purple_color_with_solid_shape)
                itemViewHolder.tV_follow.setTextColor(ContextCompat.getColor(mActivity, R.color.white))
                itemViewHolder.tV_follow.text = mActivity.resources.getString(R.string.Following)
            } else {
                itemViewHolder.rL_follow.setBackgroundResource(R.drawable.rect_purple_color_with_stroke_shape)
                itemViewHolder.tV_follow.setTextColor(ContextCompat.getColor(mActivity, R.color.blue))
                itemViewHolder.tV_follow.text = mActivity.resources.getString(R.string.follow)
            }

            // hide row
            val finalPosition = position
            itemViewHolder.tV_hide.setOnClickListener { removeItem(finalPosition) }
            val arrayListPostData = arrayListDiscoverData[position].postData

            // Follow
            val finalPostedByUserName = postedByUserName
            itemViewHolder.rL_follow.setOnClickListener {
                val url: String
                val mFollowsFlag = arrayListDiscoverData[holder.getAdapterPosition() - 1].followsFlag

                // unfollow
                if (mFollowsFlag != null && mFollowsFlag == "1") {
                    followingCount = followingCount - 1
                    itemViewHolder.tV_hide.visibility = View.VISIBLE
                    itemViewHolder.rL_follow.setBackgroundResource(R.drawable.rect_purple_color_with_stroke_shape)
                    itemViewHolder.tV_follow.setTextColor(ContextCompat.getColor(mActivity, R.color.blue))
                    itemViewHolder.tV_follow.text = mActivity.resources.getString(R.string.follow)
                    arrayListDiscoverData[holder.getAdapterPosition() - 1].followsFlag = "0"
                    url = ApiUrl.UNFOLLOW + finalPostedByUserName
                    if (arrayListPostData != null && arrayListPostData.size > 0) mEventBusDatasHandler.setSocialDatasFromDiscovery(finalPostedByUserName, profilePicUrl, arrayListPostData[0], false)
                    apiCall.followUserApi(url)
                } else {
                    followingCount = followingCount + 1
                    itemViewHolder.tV_hide.visibility = View.GONE
                    itemViewHolder.rL_follow.setBackgroundResource(R.drawable.rect_purple_color_with_solid_shape)
                    itemViewHolder.tV_follow.setTextColor(ContextCompat.getColor(mActivity, R.color.white))
                    itemViewHolder.tV_follow.text = mActivity.resources.getString(R.string.Following)
                    arrayListDiscoverData[holder.getAdapterPosition() - 1].followsFlag = "1"
                    url = ApiUrl.FOLLOW + finalPostedByUserName
                    if (arrayListPostData != null && arrayListPostData.size > 0) mEventBusDatasHandler.setSocialDatasFromDiscovery(finalPostedByUserName, profilePicUrl, arrayListPostData[0], true)
                    apiCall.followUserApi(url)
                }
                println("$TAG followingCount=$followingCount")
            }

            // user name
            if (postedByUserName != null) {
                postedByUserName = postedByUserName.substring(0, 1).toUpperCase() + postedByUserName.substring(1).toLowerCase()
                itemViewHolder.tV_postedByUserName.text = postedByUserName
            }

            // full name
            if (postedByUserFullName != null) itemViewHolder.tV_postedByUserFullName.text = postedByUserFullName

            // Post datas
            if (arrayListPostData != null && arrayListPostData.size > 0) {
                itemViewHolder.horizontal_posts.visibility = View.VISIBLE
                itemViewHolder.tV_noPost.visibility = View.GONE
                itemViewHolder.view_divider.visibility = View.VISIBLE
                val layoutInflater = LayoutInflater.from(mActivity)
                itemViewHolder.linear_postData.removeAllViews()
                for (postCount in arrayListPostData.indices) {
                    val view = layoutInflater.inflate(R.layout.single_row_images, null, false)
                    val thumbnailImageUrl = arrayListPostData[postCount].thumbnailImageUrl
                    val iV_thumbnailImage = view.findViewById<View>(R.id.iV_image) as ImageView
                    iV_thumbnailImage.layoutParams.width = CommonClass.getDeviceWidth(mActivity) / 5
                    iV_thumbnailImage.layoutParams.height = CommonClass.getDeviceWidth(mActivity) / 5
                    CommonClass.setMargins(iV_thumbnailImage, 5, 0, 5, 0)
                    if (thumbnailImageUrl != null && !thumbnailImageUrl.isEmpty()) {
                        Picasso.with(mActivity)
                                .load(thumbnailImageUrl)
                                .transform(RoundedCornersTransform())
                                .placeholder(R.drawable.default_image)
                                .error(R.drawable.default_image)
                                .into(iV_thumbnailImage)
                        itemViewHolder.linear_postData.addView(view)
                    }
                }
            } else {
                itemViewHolder.horizontal_posts.visibility = View.GONE
                itemViewHolder.tV_noPost.visibility = View.VISIBLE
                itemViewHolder.view_divider.visibility = View.GONE
            }
            val finalPostedByUserName1 = username
            itemViewHolder.rL_memeberName.setOnClickListener { /*Intent intent=new Intent(mActivity, UserProfileActivity.class);
                    intent.putExtra("membername",arrayListDiscoverData.get(itemViewHolder.getAdapterPosition()-1).getPostedByUserName());
                    mActivity.startActivity(intent);*/
                val intent: Intent
                if (mSessionManager.isUserLoggedIn && mSessionManager.userName == finalPostedByUserName1) {
                    intent = Intent(mActivity, SelfProfileActivity::class.java)
                    intent.putExtra("membername", finalPostedByUserName1)
                } else {
                    intent = Intent(mActivity, UserProfileActivity::class.java)
                    intent.putExtra("membername", finalPostedByUserName1)
                }
                mActivity.startActivity(intent)
            }
        }
    }

    /**
     * Return the size of your dataset
     * @return the total number of rows
     */
    override fun getItemCount(): Int {
        return arrayListDiscoverData.size + 1
    }

    /**
     * <h>GetItemViewType</h>
     *
     *
     * In this method we used to return the type whether it is Header or item.
     *
     * @param position The position of the row.
     * @return it returns the type of Item
     */
    override fun getItemViewType(position: Int): Int {
        return if (position == 0) TYPE_HEADER else TYPE_ITEM
    }

    /**
     * In this class we used to declare and assign the HeaderView variables.
     */
    private inner class HeaderViewHolder internal constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var rL_connectToContact: RelativeLayout
        var rL_connectTofb: RelativeLayout

        init {
            rL_connectToContact = itemView.findViewById<View>(R.id.rL_connectToContact) as RelativeLayout
            rL_connectTofb = itemView.findViewById<View>(R.id.rL_connectTofb) as RelativeLayout
            tV_fb_friends_count = itemView.findViewById<View>(R.id.tV_fb_friends_count) as TextView
            tV_contact_friend_count = itemView.findViewById<View>(R.id.tV_contect_friend_count) as TextView
        }
    }

    /**
     * In this class we used to declare and assign the Item variables.
     */
    private inner class ItemViewHolder internal constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val iV_profilePicUrl: ImageView
        val tV_postedByUserName: TextView
        val tV_postedByUserFullName: TextView
        val tV_hide: TextView
        val tV_follow: TextView
        val tV_noPost: TextView
        val linear_postData: LinearLayout
        val rL_follow: RelativeLayout
        val rL_memeberName: RelativeLayout
        val horizontal_posts: HorizontalScrollView
        val view_divider: View

        init {
            iV_profilePicUrl = itemView.findViewById<View>(R.id.iV_profilePicUrl) as ImageView
            iV_profilePicUrl.layoutParams.width = CommonClass.getDeviceWidth(mActivity) / 7
            iV_profilePicUrl.layoutParams.height = CommonClass.getDeviceWidth(mActivity) / 7
            rL_follow = itemView.findViewById<View>(R.id.relative_follow) as RelativeLayout
            rL_memeberName = itemView.findViewById<View>(R.id.rL_memeberName) as RelativeLayout
            tV_postedByUserName = itemView.findViewById<View>(R.id.tV_postedByUserName) as TextView
            tV_postedByUserFullName = itemView.findViewById<View>(R.id.tV_postedByUserFullName) as TextView
            tV_hide = itemView.findViewById<View>(R.id.tV_hide) as TextView
            tV_follow = itemView.findViewById<View>(R.id.tV_follow) as TextView
            linear_postData = itemView.findViewById<View>(R.id.linear_postData) as LinearLayout
            horizontal_posts = itemView.findViewById<View>(R.id.horizontal_posts) as HorizontalScrollView
            view_divider = itemView.findViewById(R.id.view_divider)
            tV_noPost = itemView.findViewById<View>(R.id.tV_noPost) as TextView
        }
    }

    private fun removeItem(position: Int) {
        arrayListDiscoverData.removeAt(position)
        notifyItemRemoved(position)
        notifyItemRangeChanged(position, arrayListDiscoverData.size)
    }

    companion object {
        private const val TYPE_HEADER = 0
        private const val TYPE_ITEM = 1
        private val TAG = DiscoveryPeopleRvAdapter::class.java.simpleName
    }

    /**
     * <h>DiscoveryPeopleRvAdapter</h>
     *
     *
     * This is simple constructor to initialze the variables.
     *
     * @param mActivity The activity context
     * @param arrayListDiscoverData The response datas.
     */
    init {
        mEventBusDatasHandler = EventBusDatasHandler(mActivity)
        apiCall = ApiCall(mActivity)
        mSessionManager = SessionManager(mActivity)
    }



}