package com.zoptal.cellableapp.adapter.filter

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.main.activity.FilterActivity
import com.zoptal.cellableapp.pojo_class.filter.SendFilter
import com.zoptal.cellableapp.utility.ClickListener
import java.util.*

/**
 * Created by embed on 17/5/18.
 */
class FilterMultipleSelectionAdap(private val mContext: Context,val data: ArrayList<String>,val fieldName: String) : RecyclerView.Adapter<FilterMultipleSelectionAdap.MyViewHolder>() {
//    private val mActivity: Activity
//    private val data: ArrayList<String>
    var selectedData: ArrayList<String> = ArrayList()
//    private val fieldName: String
//    val clickListener: ClickListener ?=null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(mContext as Activity).inflate(R.layout.single_row_filter_multiple_selecetion, null)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        if (selectedData.contains(data[position])) {
            holder.iV_select.visibility = View.VISIBLE
        } else {
            holder.iV_select.visibility = View.GONE
        }
        holder.tV_value.text = data[position]
        holder.itemView.setOnClickListener {
            if ((mContext as Activity).window.currentFocus != null) (mContext as Activity).window.currentFocus!!.clearFocus()

            // if selected list contains values then remove from selection else add to selected list
            if (selectedData.contains(data[holder.adapterPosition])) {
                selectedData.remove(data[holder.adapterPosition])
                removeFromSendFilterList(data[holder.adapterPosition])
            } else {
                selectedData.add(data[holder.adapterPosition])
                addToSendFilterList(data[holder.adapterPosition])
            }

            //addSelectedValueToFilterKeyValue();
            notifyItemRangeChanged(0, itemCount)
        }
    }

    override fun getItemCount(): Int {
        return data.size
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tV_value: TextView
        var iV_select: ImageView

        init {
            tV_value = itemView.findViewById<View>(R.id.tV_value) as TextView
            iV_select = itemView.findViewById<View>(R.id.iV_select) as ImageView
        }
    }

    private fun addToSendFilterList(value: String) {
        (mContext as FilterActivity).sendFilters.add(SendFilter("equlTo", fieldName, value))
        //print to check
        for (f in (mContext as FilterActivity).sendFilters) {
            println("selected values=" + f.filedName + "," + f.value)
        }
    }

    private fun removeFromSendFilterList(value: String) {
        for (i in (mContext as FilterActivity).sendFilters.indices) {
            if ((mContext as FilterActivity).sendFilters[i].type == "equlTo") {
                if ((mContext as FilterActivity).sendFilters[i].value == value) {
                    (mContext as FilterActivity).sendFilters.removeAt(i)
                    break
                }
            }
        }

        //print to check
        for (f in (mContext as FilterActivity).sendFilters) {
            println("selected values=" + f.filedName + "," + f.value)
        }
    }

//    init {
//        mActivity = mContext as Activity
//        this.data = data
//        clickListener = clickListener
//        this.fieldName = fieldName
//        selectedData = ArrayList()
//    }
}