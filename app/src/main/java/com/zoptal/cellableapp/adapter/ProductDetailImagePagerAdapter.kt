package com.zoptal.cellableapp.adapter

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.viewpager.widget.PagerAdapter
import com.squareup.picasso.Picasso
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.main.activity.products.ProductImagesActivity
import com.zoptal.cellableapp.utility.CommonClass
import java.util.*

/**
 * Created by embed on 5/1/18.
 */
class ProductDetailImagePagerAdapter(private val context: Activity, private val arrayList: ArrayList<String>) : PagerAdapter() {
    private val layoutInflater: LayoutInflater
    private val image_array: Array<String>?
    override fun getCount(): Int {
        return image_array!!.size!!
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        println("instantiate..$container $position")
        val itemView = layoutInflater.inflate(R.layout.product_detail_viewpager_image, container, false)
        val imageView: ImageView
        imageView = itemView.findViewById<View>(R.id.viewPagerItem_image1) as ImageView
        if (image_array != null) if (Build.MANUFACTURER.equals("Samsung", ignoreCase = true)) {
            Picasso.with(context)
                    .load(image_array[position])
                    .resize(CommonClass.getDeviceWidth(context), CommonClass.getDeviceWidth(context))
                    .centerCrop()
                    .placeholder(R.drawable.default_image)
                    .error(R.drawable.default_image)
                    .into(imageView)
            container.addView(itemView)
        } else {
            Picasso.with(context)
                    .load(image_array[position])
                    .resize(CommonClass.getDeviceWidth(context), CommonClass.getDeviceWidth(context))
                    .centerCrop()
                    .placeholder(R.drawable.default_image)
                    .error(R.drawable.default_image)
                    .into(imageView)
            container.addView(itemView)
        }
        imageView.setOnClickListener {
            val intent = Intent(context, ProductImagesActivity::class.java)
            intent.putExtra("imagesArrayList", arrayList)
            context.startActivity(intent)
        }
        return itemView!!
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as LinearLayout)
    }

    init {
        layoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        image_array = arrayList.toTypedArray()
        println("images url..." + image_array.size)
    }
}