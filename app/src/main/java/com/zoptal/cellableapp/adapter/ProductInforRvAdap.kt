package com.zoptal.cellableapp.adapter

import android.content.Context
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.pojo_class.product_details_pojo.PostFilter
import java.util.*

/**
 * Created by embed on 12/6/18.
 */
class ProductInforRvAdap(var mContext: Context, var data: ArrayList<PostFilter>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.single_row_product_information, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val myViewHolder = holder as MyViewHolder
        myViewHolder.tV_fieldName.text = data[position].fieldName
        myViewHolder.tV_fieldValue.text = data[position].values
        if (position != 0 && position % 2 == 1) {
            myViewHolder.tV_fieldName.gravity = Gravity.RIGHT
            myViewHolder.tV_fieldValue.gravity = Gravity.RIGHT
        }
    }

    override fun getItemCount(): Int {
        return data.size
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tV_fieldName: TextView
        var tV_fieldValue: TextView

        init {
            tV_fieldName = itemView.findViewById<View>(R.id.tV_fieldName) as TextView
            tV_fieldValue = itemView.findViewById<View>(R.id.tV_fieldValue) as TextView
        }
    }

}