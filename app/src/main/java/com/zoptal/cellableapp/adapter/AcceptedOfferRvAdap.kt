package com.zoptal.cellableapp.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.pojo_class.accepted_offer.AcceptedOfferDatas
import com.zoptal.cellableapp.utility.CircleTransform
import com.zoptal.cellableapp.utility.CommonClass
import java.util.*

/**
 * <h>HomeFragRvAdapter</h>
 *
 *
 * In class is called from EditProductActivity class. In this recyclerview adapter class we used to inflate
 * single_row_accepted_offer layout and shows the all accepted offer.
 *
 * @since 13-Jul-17
 */
class AcceptedOfferRvAdap
/**
 * <h>CurrencyRvAdap</h>
 *
 *
 * This is simple constructor to initailize list datas and context.
 *
 * @param mActivity The current context
 * @param arrayListAcceptedOffer The list datas
 */(private val mActivity: Activity, private val arrayListAcceptedOffer: ArrayList<AcceptedOfferDatas>) : RecyclerView.Adapter<AcceptedOfferRvAdap.MyViewHolder>() {

    /**
     * <h>OnCreateViewHolder</h>
     *
     *
     * In this method The adapter prepares the layout of the items by inflating the correct
     * layout for the individual data elements.
     *
     * @param parent A ViewGroup is a special view that can contain other views (called children.)
     * @param viewType Within the getItemViewType method the recycler view determines which type should be used for data.
     * @return It returns an object of type ViewHolder per visual entry in the recycler view.
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(mActivity).inflate(R.layout.single_row_accepted_offer, parent, false)
        return MyViewHolder(view)
    }

    /**
     * <h>OnBindViewHolder</h>
     *
     *
     * In this method Every visible entry in a recycler view is filled with the
     * correct data model item by the adapter. Once a data item becomes visible,
     * the adapter assigns this data to the individual widgets which he inflated
     * earlier.
     *
     * @param holder The referece of MyViewHolder class of current class.
     * @param position The position of particular item
     */
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val buyerName: String?
        val buyerImage: String?
        val price: String
        var currency: String?
        val offerCreatedOn: String?
        buyerName = arrayListAcceptedOffer[position].buyerFullName
        buyerImage = arrayListAcceptedOffer[position].buyerProfilePicUrl
        price = arrayListAcceptedOffer[position].price
        currency = arrayListAcceptedOffer[position].currency

        // set Profile pic
        if (buyerImage != null && !buyerImage.isEmpty()) Picasso.with(mActivity)
                .load(buyerImage)
                .placeholder(R.drawable.default_circle_img)
                .error(R.drawable.default_circle_img)
                .transform(CircleTransform())
                .into(holder.image)

        // set buyer name
        if (buyerName != null && !buyerName.isEmpty()) holder.tV_heading.text = buyerName

        // set currency
        if (currency != null && !currency.isEmpty()) {
            val c = Currency.getInstance(currency)
            currency = c.symbol
        }
        val setPrice = mActivity.resources.getString(R.string.price_offered) + " " + currency + price
        holder.tV_subHeading.text = setPrice

        // posted on
        offerCreatedOn = arrayListAcceptedOffer[position].offerCreatedOn
        if (offerCreatedOn != null && !offerCreatedOn.isEmpty()) holder.tV_postedOn.text = CommonClass.getTimeDifference(offerCreatedOn)
    }

    /**
     * Return the size of your dataset
     * @return the total number of rows
     */
    override fun getItemCount(): Int {
        return arrayListAcceptedOffer.size
    }

    /**
     * <h>MyViewHolder</h>
     *
     *
     * In this class we used to declare and assign the xml variables.
     *
     */
    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var image: ImageView
        var tV_heading: TextView
        var tV_subHeading: TextView
        var tV_postedOn: TextView

        init {
            image = itemView.findViewById<View>(R.id.image) as ImageView
            image.layoutParams.width = CommonClass.getDeviceWidth(mActivity) / 7
            image.layoutParams.height = CommonClass.getDeviceWidth(mActivity) / 7
            tV_heading = itemView.findViewById<View>(R.id.tV_heading) as TextView
            tV_subHeading = itemView.findViewById<View>(R.id.tV_subHeading) as TextView
            tV_postedOn = itemView.findViewById<View>(R.id.tV_postedOn) as TextView
        }
    }

}