package com.zoptal.cellableapp.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.main.activity.FilterActivity
import com.zoptal.cellableapp.pojo_class.product_category.ProductCategoryResDatas
import com.zoptal.cellableapp.utility.CommonClass
import com.zoptal.cellableapp.utility.ProductItemClickListener
import java.util.*

/**
 * <h>FilterCategoryRvAdapter</h>
 *
 *
 * In class is called from FilterActivity class. In this recyclerview adapter class we used to inflate
 * single_row_filter_category layout and shows the product all category.
 *
 * @since 17-May-17
 * @version 1.0
 * @author 3Embed.
 */
class FilterCategoryRvAdapter(private val mActivity: Activity, private val aL_categoryDatas: ArrayList<ProductCategoryResDatas>, categoryItemClickListener: ProductItemClickListener) : RecyclerView.Adapter<FilterCategoryRvAdapter.MyViewHolder>() {
    private val selectedColor: Int
    private val unSelectedColor: Int
    private val categoryItemClickListener: ProductItemClickListener
    private val mSeleted = -1

    /**
     * <h>OnCreateViewHolder</h>
     *
     *
     * In this method The adapter prepares the layout of the items by inflating the correct
     * layout for the individual data elements.
     *
     * @param parent A ViewGroup is a special view that can contain other views (called children.)
     * @param viewType Within the getItemViewType method the recycler view determines which type should be used for data.
     * @return It returns an object of type ViewHolder per visual entry in the recycler view.
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.single_row_filter_category, parent, false)
        return MyViewHolder(view)
    }

    /**
     * <h>OnBindViewHolder</h>
     *
     *
     * In this method Every visible entry in a recycler view is filled with the
     * correct data model item by the adapter. Once a data item becomes visible,
     * the adapter assigns this data to the individual widgets which he inflated
     * earlier.
     *
     * @param holder The referece of MyViewHolder class of current class.
     * @param position The position of particular item
     */
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        if (aL_categoryDatas[position].isSelected) {
            holder.iV_select.visibility = View.VISIBLE
            (mActivity as FilterActivity).categoryName = aL_categoryDatas[position].name
            if (aL_categoryDatas[position].subCategoryCount > 0) {
                (mActivity as FilterActivity).subCategoriesService
            } else if (aL_categoryDatas[position].filterCount > 0) {
                (mActivity as FilterActivity).setCategoryFilterData(position)
            }
        } else {
            holder.iV_select.visibility = View.GONE
        }
        var name: String?
        val isFilterCateSelected: Boolean
        val seletedImage: String
        val unseletedImage: String
        name = aL_categoryDatas[position].name

        // make first character of character is uppercase
        if (name != null && !name.isEmpty()) name = name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase()
        //aL_categoryDatas.get(position).setName(name);
        seletedImage = aL_categoryDatas[position].activeimage
        unseletedImage = aL_categoryDatas[position].deactiveimage
        isFilterCateSelected = aL_categoryDatas[position].isSelected
        holder.tV_category.setTextColor(ContextCompat.getColor(mActivity, R.color.heading_color))

        // set values like category image and name
        /* if (isFilterCateSelected)
            setCategoryImage(holder.iV_category,seletedImage,holder.tV_category,name,selectedColor);
        else setCategoryImage(holder.iV_category,unseletedImage,holder.tV_category,name,unSelectedColor);*/setCategoryImage(holder.iV_category, seletedImage, holder.tV_category, name, selectedColor)

        // To select or unselect category
        holder.view.setOnClickListener {
            val isViewSelected = aL_categoryDatas[holder.adapterPosition].isSelected
            if (!isViewSelected) {
                aL_categoryDatas[holder.adapterPosition].isSelected = true

                //set all others to unselected
                for (i in aL_categoryDatas.indices) {
                    if (i != holder.adapterPosition) aL_categoryDatas[i].isSelected = false
                }
                //setCategoryImage(holder.iV_category,seletedImage,holder.tV_category,aL_categoryDatas.get(holder.getAdapterPosition()).getName(),selectedColor);
            } else {
                aL_categoryDatas[holder.adapterPosition].isSelected = false
                //setCategoryImage(holder.iV_category,unseletedImage,holder.tV_category,aL_categoryDatas.get(holder.getAdapterPosition()).getName(),unSelectedColor);
            }
            notifyDataSetChanged()
            categoryItemClickListener.onItemClick(position, holder.iV_category)
        }
    }

    /**
     * <h>SetCategoryImage</h>
     *
     *
     * In this method we used to set category image for selected and unseleted sate.
     *
     * @param imageView The category image view.
     * @param url The category image url.
     */
    private fun setCategoryImage(imageView: ImageView, url: String?, tV_category: TextView, categoryName: String?, color: Int) {
        // set image
        /* if (url!=null && !url.isEmpty())
            Picasso.with(mActivity)
                    .load(url)
                    .placeholder(R.drawable.default_circle_img)
                    .error(R.drawable.default_circle_img)
                    .into(imageView);*/
        var categoryName = categoryName
        if (url != null && !url.isEmpty()) Glide.with(mActivity)
                .load(url)
                .centerCrop() //.placeholder(R.drawable.default_circle_img)
                .error(R.drawable.default_circle_img)
                .into(imageView)

        // set category name
        if (categoryName != null) {
            // make first character of character is uppercase
            categoryName = categoryName.substring(0, 1).toUpperCase() + categoryName.substring(1).toLowerCase()
            tV_category.text = categoryName
            tV_category.setTextColor(color)
        }
    }

    override fun getItemCount(): Int {
        return aL_categoryDatas.size
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val iV_category: ImageView
        val iV_select: ImageView
        val tV_category: TextView
        val view: View

        init {
            //itemView.setBackgroundResource(R.drawable.rect_border_color_with_stroke_shape);
            iV_category = itemView.findViewById<View>(R.id.iV_category) as ImageView
            iV_category.layoutParams.width = CommonClass.getDeviceWidth(mActivity) / 7
            iV_category.layoutParams.height = CommonClass.getDeviceWidth(mActivity) / 7
            tV_category = itemView.findViewById<View>(R.id.tV_category) as TextView
            tV_category.setTextColor(ContextCompat.getColor(mActivity, R.color.heading_color))
            iV_select = itemView.findViewById<View>(R.id.iV_select) as ImageView
            view = itemView
        }
    }

    /**
     * <h>FilterCategoryRvAdapter</h>
     *
     *
     * This is simple constructor to initailize list datas and context.
     *
     * @param mActivity The current context
     * @param aL_categoryDatas The list datas
     */
    init {
        selectedColor = ContextCompat.getColor(mActivity, R.color.sub_heading_color)
        unSelectedColor = ContextCompat.getColor(mActivity, R.color.heading_color)
        this.categoryItemClickListener = categoryItemClickListener
    }
}