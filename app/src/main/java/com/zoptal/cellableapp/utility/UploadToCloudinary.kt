package com.zoptal.cellableapp.utility


import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import com.cloudinary.Cloudinary
import com.cloudinary.utils.ObjectUtils
import com.zoptal.cellableapp.pojo_class.CloudData
import java.io.File
import java.io.IOException
import java.util.*

/**
 * @author 3Embed.
 * @since 5/16/16
 * <h1>Upload to cloudinary</h1>
 *
 * this class are used to upload file to cloudinary server
 *
 * This class containing abstract method @callback which will give result to called class
 */
abstract class UploadToCloudinary private constructor(private val sessionBundle: Bundle) {

    protected constructor(sessionBundle: Bundle, cloudData: CloudData?) : this(sessionBundle) {
        UploadFileToCloudinary().execute(cloudData)
    }

    /**
     * <H1>Upload file to cloudinary server</H1>
     *
     * This class will run in helper thread
     */
    private inner class UploadFileToCloudinary : AsyncTask<CloudData?, Void?, Map<*, *>?>() {
        var isError = false
        override fun doInBackground(vararg params: CloudData?): Map<*, *>? {
            var uploaded_details: Map<*, *>? = null
            val config: HashMap<String, String> = HashMap<String, String>()
            config["cloud_name"] = sessionBundle.getString("cloudName")!!
            val cloudinary = Cloudinary(config)
            val mCloudData = params[0]
            try {
                val data: Map<*, *>
                if (mCloudData!!.isVideo) {
                    Log.d("Yes_video", "Yes data")
                    data = ObjectUtils.asMap("signature", sessionBundle.getString("signature"), "timestamp", sessionBundle.getString("timestamp"), "api_key", sessionBundle.getString("apiKey"), "resource_type", "video")
                    uploaded_details = cloudinary.uploader().upload(File(mCloudData!!.path), data)
                } else {
                    Log.d("Yes_IMage", "Yes data")
                    data = ObjectUtils.asMap("signature", sessionBundle.getString("signature"), "timestamp", sessionBundle.getString("timestamp"), "api_key", sessionBundle.getString("apiKey"))
                    uploaded_details = cloudinary.uploader().upload(File(mCloudData!!.path), data)
                }
                isError = false
            } catch (e: IOException) {
                isError = true
                e.printStackTrace()
            }
            return uploaded_details
        }

        override fun onPostExecute(details: Map<*, *>?) {
            super.onPostExecute(details)
            if (!isError) {
                details?.let { callBack(it) }
            } else {
                errorCallBack("error while upLoading to cloudinary")
            }
        }
    }

    /**
     * <h1>callback</h1>
     *
     * This metohd need to implement on called class this metohd containg result data
     * @param resultData Map resultData
     */
    abstract fun callBack(resultData: Map<*, *>?)

    /**
     * If any error happen the this service called. */
    abstract fun errorCallBack(error: String?)

}