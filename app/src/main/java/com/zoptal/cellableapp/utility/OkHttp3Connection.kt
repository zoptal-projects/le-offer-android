package com.zoptal.cellableapp.utility

import android.os.AsyncTask
import android.util.Log
import okhttp3.*
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.io.UnsupportedEncodingException
import java.security.SecureRandom
import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import java.util.concurrent.TimeUnit
import javax.net.ssl.HttpsURLConnection
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager

/**
 * <h1>OkHttp3Connection</h1>
 * <P>
 * this class handel all the service call in this app using okhttp3 class provided
 * by the third party lib.
</P> *
 * @author 3Embed.
 * @since  10/6/16.
 */
object OkHttp3Connection {
    private val JSON = MediaType.parse("application/json; charset=utf-8")

    /**
     * <h2>doOkhttpRequest</h2>
     *
     *
     * This method receive all the data and Store then into to single
     * array of class
     * Service Call using okHttp Request.
     *
     *
     *
     * this Method Take a Request Body and a url,and OkHttpRequestCallback and does a Asyntask,
     * and does a request to the given Url
     *
     * @param TAG contains the Tag for the the calling Service extra tag.
     * @param request_Url contains the url of the given Service link to do performance.
     * @param requestBody contains the require data to send the given Url link.
     * @param callbacks contains the reference to set the call back response to the calling class.
     */
    @kotlin.jvm.JvmStatic
    fun doOkHttp3Connection(TAG: String?, request_Url: String?, request_type: Request_type?, requestBody: JSONObject, callbacks: OkHttp3RequestCallback?) {
        try {
            requestBody.put(VariableConstants.BODY_AUT_KEY, VariableConstants.BODY_AUT_PASSWORD)
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        printLog("Request_data$request_Url $requestBody")
        val data = OkHttpRequestData()
        data.request_Url = request_Url
        data.requestBody = requestBody
        data.callbacks = callbacks
        data.request_type = request_type
        data.Tag = TAG
        /*
         * Calling the Async task to perform the Service call.*/OkHttpRequest().execute(data)
    }

    /**
     * <h2></h2> */
    private fun getUrl(url: String?, jsonObject: JSONObject?): String {
        val service_url = "$url?"
        var query = ""
        val object_keys = jsonObject!!.keys()
        try {
            while (object_keys.hasNext()) {
                val keys_value = object_keys.next()
                query = query + keys_value + "=" + jsonObject.getString(keys_value) + "&"
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        Log.d("Value", service_url + query)
        return service_url + query
    }

    /**
     * Creating the safe connection */
    private fun getSafeOkHttpClient(builder: OkHttpClient.Builder): OkHttpClient {
        return try {
            val x509TrustManager: X509TrustManager = object : X509TrustManager {
                @Throws(CertificateException::class)
                override fun checkClientTrusted(chain: Array<X509Certificate>, authType: String) {
                }

                @Throws(CertificateException::class)
                override fun checkServerTrusted(chain: Array<X509Certificate>, authType: String) {
                }

                override fun getAcceptedIssuers(): Array<X509Certificate?> {
                    return arrayOfNulls(0)
                }
            }
            val trustAllCerts = arrayOf<TrustManager>(x509TrustManager)
            val sslContext = SSLContext.getInstance("SSL")
            sslContext.init(null, trustAllCerts, SecureRandom())
            val sslSocketFactory = sslContext.socketFactory
            builder.sslSocketFactory(sslSocketFactory, x509TrustManager)
            builder.hostnameVerifier { hostname, session ->
                val hv = HttpsURLConnection.getDefaultHostnameVerifier()
                hv.verify("dev.cellableapp.com", session)
            }
            builder.build()
        } catch (e: Exception) {
            throw RuntimeException(e)
        }
    }

    private fun printLog(message: String?) {
        Log.e("OKHTTPCONNECTION", "" + message)
    }

    /**
     * <h1>OkHttpRequestData</h1>
     *
     *
     * Class is use to hold three parameter i.e String object,RequestBody object and OkHttpRequestCallback
     * in a single place.
     * Because async Task takes only single parameter nad i have to send three parameter so.
     * Wrapping three things into a single object and sending one object to async task.
     *
     * @see RequestBody
     */
    private class OkHttpRequestData {
        var Tag: String? = null
        var request_Url: String? = null
        var requestBody: JSONObject? = null
        var callbacks: OkHttp3RequestCallback? = null
        var request_type: Request_type? = null
    }

    /**
     * Contains the result  data Packet with the given tag from the user to provide the
     * right destination. */
    private class DataPacket {
        var result_data: String? = null
        var Tag: String? = null
        var isError = false
        var callbacks_packet: OkHttp3RequestCallback? = null
    }

    /**
     * <h1>OkHttpRequest</h1>
     * OkHttpRequest extends async task to perform the function indecently .
     * Does a service call using OkHttp client.
     * <P>
     * This class extends async task and override the method of async task .
     * on doInBackground method of async task.
     * performing a service call to th given url and sending data given to the class.
     * By the help of the OkHttpClient and sending the call back method to the calling Activity by setting
     * data to the given reference of call-Back Interface object.
    </P> *
     * If Any thing Happened to the service call like Connection Failed or any thin else.
     * Telling to the User that connection is too slow when handling Exception.
     * @see Response
     *
     * @see OkHttpClient
     *
     */
    private class OkHttpRequest : AsyncTask<OkHttpRequestData?, Void?, DataPacket>() {
        override fun onPreExecute() {
            super.onPreExecute()
        }

        override fun doInBackground(vararg params: OkHttpRequestData?): DataPacket {
            val dataPacket = DataPacket()
            dataPacket.callbacks_packet = params[0]!!.callbacks
            dataPacket.isError = false
            dataPacket.Tag = params[0]!!.Tag
            var result: String
            try {
                val builder = OkHttpClient.Builder()
                builder.connectTimeout(30, TimeUnit.SECONDS)
                builder.readTimeout(30, TimeUnit.SECONDS)
                builder.writeTimeout(30, TimeUnit.SECONDS)
                builder.addInterceptor(BasicAuthInterceptor(VariableConstants.BODY_AUT_KEY, VariableConstants.BODY_AUT_PASSWORD))
                val httpClient = getSafeOkHttpClient(builder)
                val request: Request
                if (params[0]!!.request_type == Request_type.URl) {
                    val url = getUrl(params[0]!!.request_Url, params[0]!!.requestBody)
                    request = Request.Builder()
                            .url(url)
                            .build()
                } else if (params[0]!!.request_type == Request_type.POST) {
                    val body = RequestBody.create(JSON, params[0]!!.requestBody.toString())
                    request = Request.Builder()
                            .url(params[0]!!.request_Url)
                            .header("Content-Type", "text/json; Charset=UTF-8")
                            .post(body)
                            .build()
                } else if (params[0]!!.request_type == Request_type.PUT) {
                    val body = RequestBody.create(JSON, params[0]!!.requestBody.toString())
                    request = Request.Builder()
                            .url(params[0]!!.request_Url)
                            .addHeader("Content-Type", "text/json; Charset=UTF-8")
                            .put(body)
                            .build()
                } else if (params[0]!!.request_type == Request_type.DELETE) {
                    val body = RequestBody.create(JSON, params[0]!!.requestBody.toString())
                    request = Request.Builder()
                            .url(params[0]!!.request_Url)
                            .header("Content-Type", "text/json; Charset=UTF-8")
                            .delete(body)
                            .build()
                } else {
                    val body = RequestBody.create(JSON, params[0]!!.requestBody.toString())
                    request = Request.Builder()
                            .url(params[0]!!.request_Url)
                            .header("Content-Type", "text/json; Charset=UTF-8")
                            .put(body)
                            .get()
                            .build()
                    println("OKHTTP" + " " + "get url=" + params[0]!!.request_Url)
                }
                val response = httpClient.newCall(request).execute()
                val statusCode = response.code()
                println("statusCode=$statusCode")
                when (statusCode) {
                    502 -> {
                        dataPacket.isError = true
                        result = "Server Error (502 Bad Gateway)"
                    }
                    else -> result = response.body()!!.string()
                }
            } catch (e: UnsupportedEncodingException) {
                dataPacket.isError = true
                printLog("UnsupportedEncodingException$e")
                result = "Connection Failed..Retry !"
                e.printStackTrace()
            } catch (e: IOException) {
                dataPacket.isError = true
                printLog("Read IO exception$e")
                result = "Connection is too slow...Retry!"
                e.printStackTrace()
            } catch (e: Exception) {
                dataPacket.isError = true
                printLog("Read Exception$e")
                result = "Connection is too slow...Retry!"
                e.printStackTrace()
            }
            /*
             * Adding result data.*/dataPacket.result_data = result
            return dataPacket
        }

        override fun onPostExecute(dataPacket: DataPacket) {
            super.onPostExecute(dataPacket)
            /*
             * Comment for live app.*/printLog(dataPacket.result_data)
            if (!dataPacket.isError) {
                if (dataPacket.callbacks_packet != null) dataPacket.callbacks_packet!!.onSuccess(dataPacket.result_data, dataPacket.Tag)
            } else {
                if (dataPacket.callbacks_packet != null) dataPacket.callbacks_packet!!.onError(dataPacket.result_data, dataPacket.Tag)
            }
        }
    }

    /**
     * interface for Session Call back request
     */
    interface OkHttp3RequestCallback {
        /**
         * Called When Success result of JSON request
         *
         * @param result contains the service result.
         * @param user_tag contains the user given Tag.
         */
        fun onSuccess(result: String?, user_tag: String?)

        /**
         * Called When Error result of JSON request
         *
         * @param error contain the error message.
         * @param user_tag contains the user given tag .
         */
        fun onError(error: String?, user_tag: String?)
    }

    /**
     * <h2>BasicAuthInterceptor</h2>
     * <P>
     * Authenticator provider .
    </P> */
    private class BasicAuthInterceptor internal constructor(user: String?, password: String?) : Interceptor {
        private val credentials: String

        @Throws(IOException::class)
        override fun intercept(chain: Interceptor.Chain): Response {
            val request = chain.request()
            val authenticatedRequest = request.newBuilder()
                    .header("Authorization", credentials).build()
            return chain.proceed(authenticatedRequest)
        }

        init {
            credentials = Credentials.basic(user, password)
        }
    }

    /**
     * <H3>Request_type</H3>
     *
     *
     * Request type enum class.
     */
    enum class Request_type(var value: String) {
        GET("getRequest"), URl("urlRequest"), PUT("putRequest"), DELETE("deleteRequest"), POST("postRequest");

    }
}