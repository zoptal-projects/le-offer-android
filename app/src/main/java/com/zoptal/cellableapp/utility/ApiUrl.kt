package com.zoptal.cellableapp.utility


/**
 * <h>ApiUrl</h>
 *
 *
 * In this clas we declare on base url and from the next time
 * we used to append api name with this base url.
 *
 * @since 3/31/2017
 */
object ApiUrl {
    //private static final String BASE_URL="http://api.yelo-app.xyz/api/";
    private const val BASE_URL = "https://dev.cellableapp.com/api/" //"http://3.22.206.241:3000/api/";

    /**
     * Login service
     */
    const val LOGIN = BASE_URL + "login"

    /**
     * Reset password
     */
    const val RESET_PASSWORD = BASE_URL + "resetPassword"

    /**
     * usernameCheck
     */
    const val USER_NAME_CHECK = BASE_URL + "usernameCheck"

    /**
     * emailCheck
     */
    const val EMAIL_ID_CHECK = BASE_URL + "emailCheck"

    /**
     * update Phone Number
     */
    const val OTP_PROFILE_NUMBER = BASE_URL + "profile/phoneNumber"

    /**
     * phoneNumberCheck
     */
    const val PHONE_NUMBER_CHECK = BASE_URL + "phoneNumberCheck"

    /**
     * generate otp
     */
    const val OTP = BASE_URL + "otp"

    /**
     * Device Info
     */
    const val LOG_DEVICE = BASE_URL + "logDevice"

    /**
     * LOG GUEST
     */
    const val LOG_GUEST = BASE_URL + "logGuest"

    /**
     * Sign up service
     */
    const val SIGN_UP = BASE_URL + "register"

    /**
     * email verification
     */
    const val VERIFY_FACEBOOK_LOGIN = BASE_URL + "facebook/me"

    /**
     * email verification
     */
    const val VERIFY_EMAIL = BASE_URL + "email/me"

    /**
     * email verification
     */
    const val VERIFY_GOOGLE_PLUS = BASE_URL + "google/me"

    /**
     * Link paypal
     */
    const val LINK_PAYPAL = BASE_URL + "paypal/me"

    /**
     * edit Profile
     */
    const val EDIT_PROFILE = BASE_URL + "editProfile"

    /**
     * Get Cloudinary details
     */
    const val GET_CLOUDINARY_DETAILS = BASE_URL + "getSignature"

    /**
     * Search Filter
     */
    const val SEARCH_FILTER = BASE_URL + "searchFilter/staging"

    /**
     * Get All posts for logged in user
     */
    const val GET_USER_ALL_POSTS = BASE_URL + "allPosts/users/m"

    /**
     * allPosts/guests/m
     */
    const val GET_GUEST_ALL_POSTS = BASE_URL + "allPosts/guests/m"

    /**
     * getPostsById/users
     */
    const val GET_POST_BY_ID_USER = BASE_URL + "getPostsById/users"

    /**
     * getPostsById/users
     */
    const val GET_POST_BY_ID_GUEST = BASE_URL + "getPostsById/guests"

    /**
     * Like the product
     */
    const val LIKE_PRODUCT = BASE_URL + "like"

    /**
     * unlike the product
     */
    const val UNLIKE_PRODUCT = BASE_URL + "unlike"

    /**
     * Follow
     */
    const val FOLLOW = BASE_URL + "follow/"

    /**
     * Un follow
     */
    const val UNFOLLOW = BASE_URL + "unfollow/"

    /**
     * get notification(following Activity)
     */
    const val FOLLOWING_ACTIVITY = BASE_URL + "followingActivity"

    /**
     * get notification(following Activity)
     */
    const val SELF_ACTIVITY = BASE_URL + "selfActivity"

    /**
     * Get all news feed datas
     */
    const val HOME = BASE_URL + "home"

    /**
     * user profile
     */
    const val USER_PROFILE = BASE_URL + "profile/users"

    /**
     * user profile
     */
    const val STRIPE_DISCONNECT = BASE_URL + "stripe_disconnect"

    /**
     * profile guests
     */
    const val GUEST_PROFILE = BASE_URL + "profile/guests"

    /**
     * profile posts
     */
    const val PROFILE_POST = BASE_URL + "profile/posts/"

    /**
     * profile posts
     */
    const val PURCHASED_PRODUCT_LIST = BASE_URL + "purchased_product_list"

    /**
     * profile posts
     */
    const val GUEST_PROFILE_POST = BASE_URL + "profile/guests/posts"

    /**
     * get Follower
     */
    const val GET_FOLLOWER = BASE_URL + "getFollowers"

    /**
     * add card
     */
    const val ADD_CARD = BASE_URL + "add_card"

    /**
     * get card
     */
    const val GET_CARD = BASE_URL + "get_added_card"

    /**
     * delete card
     */
    const val DELETE_CARD = BASE_URL + "delete_card"

    /**
     * set card as default
     */
    const val SET_CARD_AS_DEFAULT = BASE_URL + "set_card_as_default"

    /**
     * set card as default
     */
    const val MAKE_PAYMENT = BASE_URL + "make_payment"

    /**
     * get Following
     */
    const val GET_FOLLOWING = BASE_URL + "getFollowing"

    /**
     * get Member Following
     */
    const val GET_MEMBER_FOLLOWING = BASE_URL + "getMemberFollowing"

    /**
     * get Member Followers
     */
    const val GET_MEMBER_FOLLOWER = BASE_URL + "getMemberFollowers"

    /**
     * update Profile
     */
    const val SAVE_PROFILE = BASE_URL + "saveProfile"

    /**
     * Add_ADDRESS
     */
    const val ADD_ADDRESS = BASE_URL + "add_shipping_address"

    /**
     * Add_ADDRESS
     */
    const val UPDATE_ADDRESS = BASE_URL + "update_shipping_address"

    /**
     * GET_ADDRESS
     */
    const val GET_ADDRESS = BASE_URL + "get_shipping_address"

    /**
     * GET_ADDRESS
     */
    const val PRODUCT_RECEIVED = BASE_URL + "recieved_product"

    /**
     * GET_ADDRESS
     */
    const val ADD_UPDATE_BLUEDART_LINK = BASE_URL + "add_update_bluedart_link"

    /**
     * liked Posts
     */
    const val LIKED_POST = BASE_URL + "likedPosts"

    /**
     * Logout
     */
    const val LOGOUT = BASE_URL + "logout"

    /**
     * discover people
     */
    const val DISCOVER_PEOPLE = BASE_URL + "discover-people-website"

    /**
     * Get All Likes
     */
    const val GET_ALL_LIKES = BASE_URL + "getAllLikes"

    /**
     * Get Categories
     */
    const val GET_CATEGORIES = BASE_URL + "getCategories"

    /**
     * Get Categories
     */
    const val STRIPE_CONNECT = BASE_URL + "stripe_connect"

    /**
     * Get Post Comments
     */
    const val GET_POST_COMMENTS = BASE_URL + "getPostComments"

    /**
     * add comments
     */
    const val ADD_COMMENTS = BASE_URL + "comments"

    /**
     * DeleteComments From Post
     */
    const val DELETE_COMMENTS = BASE_URL + "deleteCommentsFromPost"

    /**
     * post Report Reason
     */
    const val REPORT_POST_REASON = BASE_URL + "postReportReason"

    /**
     * Report Reason
     */
    const val REPORT_USER_REASON = BASE_URL + "reportReason"

    /**
     * report Post
     */
    const val REPORT_POST = BASE_URL + "reportPost"

    /**
     * report User
     * /report/:membername
     * params : {reasonId,membername,description}
     * manager : post
     */
    const val REPORT_USER = BASE_URL + "report/"

    /**
     * Post product
     */
    const val PRODUCT = BASE_URL + "product/v2"

    /**
     * search Users
     */
    const val SEARCH_USER = BASE_URL + "searchUsers"

    /**
     * search user from guest user
     */
    const val SEARCH_GUEST_USER = BASE_URL + "guests/search/member"

    /**
     * Search product
     */
    const val SEARCH_POST = BASE_URL + "search/"

    /**
     * Phone Contacts
     */
    const val PHONE_CONTACTS = BASE_URL + "phoneContacts"

    /**
     * Facebook Contact Sync
     */
    const val FACEBOOK_CONTACT_SYNC = BASE_URL + "facebookContactSync"

    /**
     * Accepted Offers
     */
    const val ACCEPTED_OFFER = BASE_URL + "acceptedOffers"

    /**
     * Accepted Offers
     */
    const val RATE_USER = BASE_URL + "rate/"

    /**
     * Accepted Offers
     */
    const val MARK_SELLING = BASE_URL + "markSelling"

    /**
     * Sold some Where else
     */
    const val ELSE_WHERE = BASE_URL + "sold/elseWhere"

    /**
     * mark Sold
     */
    const val MARK_SOLD = BASE_URL + "markSold"

    /**
     * Insights
     */
    const val INSIGHT = BASE_URL + "insights"

    /**
     * user campaign
     */
    const val USER_CAMPAIGN = BASE_URL + "user/campaign"

    /**
     * unseen Notification Count
     */
    const val UNSEEN_NOTIFICATION_COUNT = BASE_URL + "unseenNotificationCount"

    /**
     * promotionPlans
     *
     * promote post call this api
     * https://api.yelo-app.xyz/api/inAppPurchase
     * params : {token,postId,postType,purchaseId,noOfViews}
     * manager : post
     */
    const val PROMOTE_PLANS = BASE_URL + "promotionPlans"

    //public static final String PURCHASED_PLAN=BASE_URL+"promotePosts";
    const val PURCHASED_PLAN = BASE_URL + "inAppPurchase"

    /**
     * getPostsById/users
     */
    const val MAKE_OFFER = BASE_URL + "makeOffer"

    /**
     * getPostsById/users
     */
    const val GET_POST_BY_ID = BASE_URL + "getPostsById/users"

    /*
 * subcategory in collegestax
https://collegestax.com/api/subCategory
params : {categoryName}
manager : get
 * */
    const val GET_SUB_CATEGORIES = BASE_URL + "subCategory"

    /*
 send me the category,subCategory and filter

filter should be a json object you have to send me like this
"filter": {
   "bedrooms" : 1,
  "construction status":"Under Construction",
  "course":""
  },
 * */
    /*Filter Product Api *
 *mandatory params is :{token,location,latitude,longitude,distanceMax}
  other params: {category,subCategory,filter,
  sortBy : values{newFirst,closestFirst,heighToLow,lowToHigh},
  postedWithin : values {1:for 24 hours,2:for 7 days,3:for 30 days},
  price : {currency,from,to}
} */
    const val FILTER_PRODUCT = BASE_URL + "filterProduct"
    //.......................exchanges apis..........................//
    /**
     * getExchangesByProductName
     */
    const val GET_EXCHAGES_BY_PRODUCT_NAME = BASE_URL + "suggestionPost"

    /*
 * Swap Offer Api
 * http://159.65.235.141:3000/api/swapOffer
 * manager : post
 * params : {token,postId,swapPostId,swapStatus,memberName,price,sendChat}
 * */
    const val SWAP_OFFER = BASE_URL + "swapOffer"

    /*  SWAP OFFER ACCEPT API
 * http://159.65.235.141:3000/api/makeExchangePost
 manager : post
 params :{token,postId,swapPostId,memberName}
 * */
    const val SWAP_OFFER_ACCEPT = BASE_URL + "makeExchangePost"
    const val MY_EXCHANGES = BASE_URL + "myExchanges"

    /* admin messages
    http://dev.yelo-app.xyz/api/message
    manager : get
    params : {token}*/
    const val CHAT_SUGGESTION_MESSAGE = BASE_URL + "message"
}