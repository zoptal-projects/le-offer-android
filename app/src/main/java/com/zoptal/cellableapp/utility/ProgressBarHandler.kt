package com.zoptal.cellableapp.utility


import android.R
import android.app.Activity
import android.content.Context
import android.graphics.PorterDuff
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.RelativeLayout
import androidx.core.content.ContextCompat

/**
 * Created by hello on 3/23/2017.
 */
class ProgressBarHandler(context: Context) {
    private val mProgressBar: ProgressBar
    fun show() {
        mProgressBar.visibility = View.VISIBLE
    }

    fun hide() {
        mProgressBar.visibility = View.INVISIBLE
    }

    init {
        val layout = (context as Activity).findViewById<View>(R.id.content).rootView as ViewGroup
        mProgressBar = ProgressBar(context, null, R.attr.progressBarStyleLarge)
        //mProgressBar.setIndeterminateDrawable(context.getResources().getDrawable(R.drawable.custom_progress_background));
        mProgressBar.indeterminateDrawable.setColorFilter(ContextCompat.getColor(context, com.zoptal.cellableapp.R.color.login_page_blue_box), PorterDuff.Mode.MULTIPLY)
        mProgressBar.isIndeterminate = true
        val params = RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT)
        val rl = RelativeLayout(context)
        rl.gravity = Gravity.CENTER
        rl.addView(mProgressBar)
        layout.addView(rl, params)
        hide()
    }
}