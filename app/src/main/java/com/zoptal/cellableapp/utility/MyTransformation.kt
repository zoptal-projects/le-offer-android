package com.zoptal.cellableapp.utility


import android.content.Context
import android.graphics.Bitmap
import android.graphics.Matrix
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation

class MyTransformation(context: Context?, rotate: Float, id: String) : BitmapTransformation(context) {
    private var rotate = 0f
    private var id: String =""
    override fun transform(pool: BitmapPool, toTransform: Bitmap,
                           outWidth: Int, outHeight: Int): Bitmap {
        return rotateBitmap(toTransform, rotate)
    }

    override fun getId(): String {
        return "com.yelo.com.utility.MyTransformation.$id"
    }

    companion object {
        fun rotateBitmap(source: Bitmap, angle: Float): Bitmap {
            val matrix = Matrix()
            matrix.postRotate(angle)
            return Bitmap.createBitmap(source, 0, 0, source.width, source.height, matrix, true)
        }
    }

    init {
        this.rotate = rotate
        this.id = id
    }
}