package com.zoptal.cellableapp.utility

import android.app.ActivityManager
import android.content.Context
import android.os.Build
import android.os.Handler
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd
import com.zoptal.cellableapp.R
import java.util.*

/**
 * <h>GoogleAdMob</h>
 *
 *
 * In this class we used to initialize google mob ad. for every 4 minute.
 *
 * @since 02-Sep-17.
 */
class GoogleAdMob(private val mContext: Context) {
    private var mInterstitialAd: InterstitialAd? = null
    private val mSessionManager: SessionManager

    /**
     * <h>InitializeInterstitialAdMob</h>
     *
     *
     * In this method we used to initialize MobileAds and show the ad.
     * Once Interstitial screen will be closed by the used then again
     * call showAdMob method from setAdListener.
     *
     */
    private fun initializeInterstitialAdMob() {
        showAdMob()
        mInterstitialAd = InterstitialAd(mContext)
        mInterstitialAd!!.adUnitId = mContext.resources.getString(R.string.adMobInterstialId)
        mInterstitialAd!!.loadAd(AdRequest.Builder().build())
        mInterstitialAd!!.adListener = object : AdListener() {
            override fun onAdClosed() {
                super.onAdClosed()
                if (!mInterstitialAd!!.isLoading && !mInterstitialAd!!.isLoaded) {
                    mInterstitialAd!!.loadAd(AdRequest.Builder().build())
                    showAdMob()
                }
            }
        }
    }

    /**
     * <h>ShowAdMob</h>
     *
     *
     * In this method we used to set timer for 4-min once it completes then
     * show interstitial google mob ad.
     *
     */
    private fun showAdMob() {
        val handler = Handler()
        val timer = Timer()
        val doAsynchronousTask: TimerTask = object : TimerTask() {
            override fun run() {
                handler.post {
                    println(TAG + " " + "is user logged in=" + mSessionManager.isUserLoggedIn + " " + "is in forground=" + isAppIsInBackground(mContext))
                    if (mSessionManager.isUserLoggedIn && !isAppIsInBackground(mContext)) {
                        if (mInterstitialAd!!.isLoaded) {
                            mInterstitialAd!!.show()
                        } else {
                            mInterstitialAd!!.loadAd(AdRequest.Builder().build())
                            println("$TAG The interstitial wasn't loaded yet.")
                        }
                    }
                }
            }
        }
        timer.schedule(doAsynchronousTask, 0, 400000)
    }

    /**
     * Method checks if the app is in background or not
     */
    private fun isAppIsInBackground(context: Context): Boolean {
        var isInBackground = true
        val am = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            val runningProcesses = am.runningAppProcesses
            if (runningProcesses != null) {
                for (processInfo in runningProcesses) {
                    if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                        for (activeProcess in processInfo.pkgList) {
                            if (activeProcess == context.packageName) {
                                isInBackground = false
                            }
                        }
                    }
                }
            }
        } else {
            val taskInfo = am.getRunningTasks(1)
            val componentInfo = taskInfo[0].topActivity
            if (componentInfo!!.packageName == context.packageName) {
                isInBackground = false
            }
        }
        return isInBackground
    }

    companion object {
        private val TAG = GoogleAdMob::class.java.simpleName
    }

    init {
        mSessionManager = SessionManager(mContext)
        initializeInterstitialAdMob()
    }
}