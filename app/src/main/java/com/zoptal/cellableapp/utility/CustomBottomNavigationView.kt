package com.zoptal.cellableapp.utility

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Typeface

import android.util.AttributeSet
import android.util.SparseIntArray
import android.util.TypedValue
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import androidx.viewpager.widget.ViewPager
import com.google.android.material.bottomnavigation.BottomNavigationItemView
import com.google.android.material.bottomnavigation.BottomNavigationMenuView
import com.google.android.material.bottomnavigation.BottomNavigationView

import java.lang.ref.WeakReference

/**
 * <h>CustomBottomNavigationView</h>
 *
 *
 * In this class we used to set our custom things with bottom navigation view
 * like to set icon size, text, custom font with text etc.
 *
 *
 * @since 2017/11/10
 */
class CustomBottomNavigationView : BottomNavigationView {
    // used for animation
    private var mShiftAmount = 0
    private var mScaleUpFactor = 0f
    private var mScaleDownFactor = 0f
    private var animationRecord = false
    private var mLargeLabelSize = 0f
    private var mSmallLabelSize = 0f

    // used for setupWithViewPager
    private val mMyOnNavigationItemSelectedListener: MyOnNavigationItemSelectedListener? = null
    private var mMenuView: BottomNavigationMenuView? = null
    private var mButtons: Array<BottomNavigationItemView> ? =null

    // used for setupWithViewPager end
    constructor(context: Context?) : super(context) {//        init();
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {//        init();
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {//        init();
    }

    /**
     * enable or disable click item animation(text scale and icon move animation in no item shifting mode)
     *
     * @param enable It means the text won't scale and icon won't move when active it in no item shifting mode if false.
     */
    @SuppressLint("RestrictedApi")
    fun enableAnimation(enable: Boolean) {
        /*
        1. get field in this class
        private final BottomNavigationMenuView mMenuView;

        2. get field in mButtons
        private BottomNavigationItemView[] mButtons;

        3. chang mShiftAmount to 0 in mButtons
        private final int mShiftAmount

        change mScaleUpFactor and mScaleDownFactor to 1f in mButtons
        private final float mScaleUpFactor
        private final float mScaleDownFactor

        4. change label font size in mButtons
        private final TextView mLargeLabel
        private final TextView mSmallLabel
         */

        // 1. get mMenuView
        val mMenuView: BottomNavigationMenuView? = bottomNavigationMenuView
        // 2. get mButtons
        val mButtons: Array<BottomNavigationItemView>? = bottomNavigationItemViews
        // 3. change field mShiftingMode value in mButtons
        for (button in mButtons!!) {
            val mLargeLabel = getField<TextView>(button.javaClass, button, "mLargeLabel")!!
            val mSmallLabel = getField<TextView>(button.javaClass, button, "mSmallLabel")!!

            // if disable animation, need animationRecord the source value
            if (!enable) {
                if (!animationRecord) {
                    animationRecord = true
                    mShiftAmount = getField<Int>(button.javaClass, button, "mShiftAmount")!!
                    mScaleUpFactor = getField<Float>(button.javaClass, button, "mScaleUpFactor")!!
                    mScaleDownFactor = getField<Float>(button.javaClass, button, "mScaleDownFactor")!!
                    mLargeLabelSize = mLargeLabel.textSize
                    mSmallLabelSize = mSmallLabel.textSize

//                    System.out.println("mShiftAmount:" + mShiftAmount + " mScaleUpFactor:"
//                            + mScaleUpFactor + " mScaleDownFactor:" + mScaleDownFactor
//                            + " mLargeLabel:" + mLargeLabelSize + " mSmallLabel:" + mSmallLabelSize);
                }
                // disable
                setField(button.javaClass, button, "mShiftAmount", 0)
                setField(button.javaClass, button, "mScaleUpFactor", 1)
                setField(button.javaClass, button, "mScaleDownFactor", 1)

                // let the mLargeLabel font size equal to mSmallLabel
                mLargeLabel.setTextSize(TypedValue.COMPLEX_UNIT_PX, mSmallLabelSize)

                // debug start
//                mLargeLabelSize = mLargeLabel.getTextSize();
//                System.out.println("mLargeLabel:" + mLargeLabelSize);
                // debug end
            } else {
                // haven't change the value. It means it was the first call this method. So nothing need to do.
                if (!animationRecord) return
                // enable animation
                setField(button.javaClass, button, "mShiftAmount", mShiftAmount)
                setField(button.javaClass, button, "mScaleUpFactor", mScaleUpFactor)
                setField(button.javaClass, button, "mScaleDownFactor", mScaleDownFactor)
                // restore
                mLargeLabel.setTextSize(TypedValue.COMPLEX_UNIT_PX, mLargeLabelSize)
            }
        }
        mMenuView!!.updateMenuView()
    }

    /**
     * enable the shifting mode for navigation
     *
     * @param enable It will has a shift animation if true. Otherwise all items are the same width.
     */
    @SuppressLint("RestrictedApi")
    fun enableShiftingMode(enable: Boolean) {
        /*
        1. get field in this class
        private final BottomNavigationMenuView mMenuView;

        2. change field mShiftingMode value in mMenuView
        private boolean mShiftingMode = true;
         */
        // 1. get mMenuView
        val mMenuView: BottomNavigationMenuView? = bottomNavigationMenuView
        // 2. change field mShiftingMode value in mMenuView
        setField(mMenuView!!.javaClass, mMenuView, "mShiftingMode", enable)
        mMenuView.updateMenuView()
    }

    /**
     * enable the shifting mode for each item
     *
     * @param enable It will has a shift animation for item if true. Otherwise the item text always be shown.
     */
    @SuppressLint("RestrictedApi")
    fun enableItemShiftingMode(enable: Boolean) {
        /*
        1. get field in this class
        private final BottomNavigationMenuView mMenuView;

        2. get field in this mMenuView
        private BottomNavigationItemView[] mButtons;

        3. change field mShiftingMode value in mButtons
        private boolean mShiftingMode = true;
         */
        // 1. get mMenuView
        val mMenuView: BottomNavigationMenuView? = bottomNavigationMenuView
        // 2. get mButtons
        val mButtons: Array<BottomNavigationItemView>? = bottomNavigationItemViews
        // 3. change field mShiftingMode value in mButtons
        for (button in mButtons!!) {
            setField(button.javaClass, button, "mShiftingMode", enable)
        }
        mMenuView!!.updateMenuView()
    }

    /**
     * get menu item position in menu
     *
     * @param item the Menu item
     * @return position if success, -1 otherwise
     */
    fun getMenuItemPosition(item: MenuItem): Int {
        // get item id
        val itemId = item.itemId
        // get meunu
        val menu: Menu = getMenu()
        val size = menu.size()
        for (i in 0 until size) {
            if (menu.getItem(i).itemId == itemId) {
                return i
            }
        }
        return -1
    }

    /**
     * set the current checked item
     *
     * @param item start from 0.
     */
    fun setCurrentItem(item: Int) {
        // check bounds
        if (item < 0 || item >= getMaxItemCount()) {
            throw ArrayIndexOutOfBoundsException(("item is out of bounds, we expected 0 - "
                    + (getMaxItemCount() - 1)) + ". Actually " + item)
        }

        /*
        1. get field in this class
        private final BottomNavigationMenuView mMenuView;

        2. get field in mMenuView
        private BottomNavigationItemView[] mButtons;
        private final OnClickListener mOnClickListener;

        3. call mOnClickListener.onClick();
         */
        // 1. get mMenuView
        val mMenuView: BottomNavigationMenuView? = bottomNavigationMenuView
        // 2. get mButtons
        val mButtons: Array<BottomNavigationItemView>? = bottomNavigationItemViews
        // get mOnClickListener
        val mOnClickListener = getField<View.OnClickListener>(mMenuView!!.javaClass, mMenuView, "mOnClickListener")!!

//        System.out.println("mMenuView:" + mMenuView + " mButtons:" + mButtons + " mOnClickListener" + mOnClickListener);
        // 3. call mOnClickListener.onClick();
        mOnClickListener.onClick(mButtons!![item])
    }

    override fun setOnNavigationItemSelectedListener(@Nullable listener: OnNavigationItemSelectedListener?) {
        // if not set up with view pager, the same with father
        if (null == mMyOnNavigationItemSelectedListener) {
            super.setOnNavigationItemSelectedListener(listener)
            return
        }
        mMyOnNavigationItemSelectedListener.setOnNavigationItemSelectedListener(listener)
    }

    /**
     * get private mMenuView
     *
     * @return returs the menu Bottom View
     */
    private val bottomNavigationMenuView: BottomNavigationMenuView?
        private get() {
            if (null == mMenuView) mMenuView = getField<BottomNavigationMenuView>(BottomNavigationView::class.java, this, "mMenuView")
            return mMenuView
        }/*
         * 1 private final BottomNavigationMenuView mMenuView;
         * 2 private BottomNavigationItemView[] mButtons;
         */

    /**
     * get private mButtons in mMenuView
     *
     * @return the BottomNavigationItemView var
     */
    val bottomNavigationItemViews: Array<BottomNavigationItemView>?
        get() {
            if (null != mButtons) return mButtons
            /*
         * 1 private final BottomNavigationMenuView mMenuView;
         * 2 private BottomNavigationItemView[] mButtons;
         */
            val mMenuView: BottomNavigationMenuView? = bottomNavigationMenuView
            if (mMenuView != null) {
                mButtons = getField<Any>(mMenuView.javaClass, mMenuView, "mButtons") as Array<BottomNavigationItemView>?
            }
            return mButtons
        }

    /**
     * get private mButton in mMenuView at position
     *
     * @param position the position of menu item
     * @return the BottomNavigationItemView position
     */
    fun getBottomNavigationItemView(position: Int): BottomNavigationItemView {
        return bottomNavigationItemViews!![position]
    }

    /**
     * get icon at position
     *
     * @param position of icon
     * @return The fiekld
     */
    fun getIconAt(position: Int): ImageView {
        /*
         * 1 private final BottomNavigationMenuView mMenuView;
         * 2 private BottomNavigationItemView[] mButtons;
         * 3 private ImageView mIcon;
         */
        val mButtons: BottomNavigationItemView = getBottomNavigationItemView(position)
        return getField<ImageView>(BottomNavigationItemView::class.java, mButtons, "mIcon")!!
    }

    /**
     * get small label at position
     * Each item has tow label, one is large, another is small.
     *
     * @param position of small label
     * @return the small label
     */
    fun getSmallLabelAt(position: Int): TextView {
        /*
         * 1 private final BottomNavigationMenuView mMenuView;
         * 2 private BottomNavigationItemView[] mButtons;
         * 3 private final TextView mSmallLabel;
         */
        val mButtons: BottomNavigationItemView = getBottomNavigationItemView(position)
        return getField<TextView>(BottomNavigationItemView::class.java, mButtons, "mSmallLabel")!!
    }

    /**
     * get large label at position
     * Each item has tow label, one is large, another is small.
     *
     * @param position of large label
     * @return the textview
     */
    fun getLargeLabelAt(position: Int): TextView {
        /*
         * 1 private final BottomNavigationMenuView mMenuView;
         * 2 private BottomNavigationItemView[] mButtons;
         * 3 private final TextView mLargeLabel;
         */
        val mButtons: BottomNavigationItemView = getBottomNavigationItemView(position)
        return getField<TextView>(BottomNavigationItemView::class.java, mButtons, "mLargeLabel")!!
    }

    /**
     * return item count
     *
     * @return the menu item size
     */
    val itemCount: Int
        get() {
            val bottomNavigationItemViews = bottomNavigationItemViews
                    ?: return 0
            return bottomNavigationItemViews.size
        }

    /**
     * set all item small TextView size
     * Each item has tow label, one is large, another is small.
     * Small one will be shown when item state is normal
     * Large one will be shown when item checked.
     *
     * @param sp the text size
     */
    @SuppressLint("RestrictedApi")
    fun setSmallTextSize(sp: Float) {
        val count = itemCount
        for (i in 0 until count) {
            getSmallLabelAt(i).textSize = sp
        }
        mMenuView!!.updateMenuView()
    }

    /**
     * set all item large TextView size
     * Each item has tow label, one is large, another is small.
     * Small one will be shown when item state is normal.
     * Large one will be shown when item checked.
     *
     * @param sp the large text size
     */
    fun setLargeTextSize(sp: Float) {
        val count = itemCount
        for (i in 0 until count) {
            getLargeLabelAt(i).textSize = sp
        }
        mMenuView!!.updateMenuView()
    }

    /**
     * set all item large and small TextView size
     * Each item has tow label, one is large, another is small.
     * Small one will be shown when item state is normal
     * Large one will be shown when item checked.
     *
     * @param sp the text size
     */
    fun setTextSize(sp: Float) {
        setLargeTextSize(sp)
        setSmallTextSize(sp)
    }

    /**
     * set item ImageView size which at position
     *
     * @param position position start from 0
     * @param width    in dp
     * @param height   in dp
     */
    fun setIconSizeAt(position: Int, width: Float, height: Float) {
        val icon = getIconAt(position)
        // update size
        val layoutParams = icon.layoutParams
        layoutParams.width = dp2px(getContext(), width)
        layoutParams.height = dp2px(getContext(), height)
        icon.layoutParams = layoutParams
        mMenuView!!.updateMenuView()
    }

    /**
     * set all item ImageView size
     *
     * @param width  in dp
     * @param height in dp
     */
    fun setIconSize(width: Float, height: Float) {
        val count = itemCount
        for (i in 0 until count) {
            setIconSizeAt(i, width, height)
        }
    }

    /**
     * set Typeface for all item TextView
     */
    @SuppressLint("RestrictedApi")
    fun setTypeface(typeface: Typeface?) {
        val count = itemCount
        for (i in 0 until count) {
            getLargeLabelAt(i).setTypeface(typeface)
            getSmallLabelAt(i).setTypeface(typeface)
        }
        if (mMenuView != null) mMenuView!!.updateMenuView()
    }

    /**
     * get private filed in this specific object
     *
     * @param targetClass the target class
     * @param instance    the filed owner
     * @param fieldName   the field name
     * @param <T>         the type
     * @return field if success, null otherwise.
    </T> */
    private fun <T> getField(targetClass: Class<*>, instance: Any?, fieldName: String): T? {
        try {
            val field = targetClass.getDeclaredField(fieldName)
            field.isAccessible = true
            return field[instance] as T
        } catch (e: NoSuchFieldException) {
            e.printStackTrace()
        } catch (e: IllegalAccessException) {
            e.printStackTrace()
        }
        return null
    }

    /**
     * change the field value
     *
     * @param targetClass the target class
     * @param instance    the filed owner
     * @param fieldName   the field name
     */
    private fun setField(targetClass: Class<*>, instance: Any?, fieldName: String, value: Any) {
        try {
            val field = targetClass.getDeclaredField(fieldName)
            field.isAccessible = true
            field[instance] = value
        } catch (e: NoSuchFieldException) {
            e.printStackTrace()
        } catch (e: IllegalAccessException) {
            e.printStackTrace()
        }
    }

    /**
     * Decorate OnNavigationItemSelectedListener for setupWithViewPager
     */
    private class MyOnNavigationItemSelectedListener internal constructor(viewPager: ViewPager, bnve: CustomBottomNavigationView, smoothScroll: Boolean, listener: OnNavigationItemSelectedListener?) : OnNavigationItemSelectedListener {
        private var listener: OnNavigationItemSelectedListener?
        private val viewPagerRef: WeakReference<ViewPager>
        private val smoothScroll: Boolean
        private val items // used for change ViewPager selected item
                : SparseIntArray
        private var previousPosition = -1
        fun setOnNavigationItemSelectedListener(listener: OnNavigationItemSelectedListener?) {
            this.listener = listener
        }

       override fun onNavigationItemSelected(@NonNull item: MenuItem): Boolean {
            val position = items[item.itemId]
            // only set item when item changed
            if (previousPosition == position) {
                return true
            }

            // user listener
            if (null != listener) {
                val bool: Boolean = listener!!.onNavigationItemSelected(item)
                // if the selected is invalid, no need change the view pager
                if (!bool) return false
            }

            // change view pager
            val viewPager: ViewPager = viewPagerRef.get() ?: return false
            viewPager.setCurrentItem(items[item.itemId], smoothScroll)

            // update previous position
            previousPosition = position
            return true
        }

        init {
            viewPagerRef = WeakReference<ViewPager>(viewPager)
            this.listener = listener
            this.smoothScroll = smoothScroll

            // create items
            val menu: Menu = bnve.getMenu()
            val size = menu.size()
            items = SparseIntArray(size)
            for (i in 0 until size) {
                val itemId = menu.getItem(i).itemId
                items.put(itemId, i)
            }
        }
    }

    companion object {
        /**
         * dp to px
         *
         * @param context the current context
         * @param dpValue dp
         * @return px
         */
        fun dp2px(context: Context, dpValue: Float): Int {
            val scale = context.resources.displayMetrics.density
            return (dpValue * scale + 0.5f).toInt()
        }
    }
}