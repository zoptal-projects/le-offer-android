package com.zoptal.cellableapp.utility

import android.app.Activity
import android.app.Dialog
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.view.Gravity
import android.view.View
import android.view.Window
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.zoptal.cellableapp.R

/**
 * <h>RunTimePermission</h>
 *
 *
 * In this class we wrote the code for Android Runtime permissions like
 * firstlly to check whether Given permission is granted or not. it is
 * not then grant the permission.
 *
 */
class RunTimePermission
/**
 * <h>RunTimePermission</h>
 *
 *
 * Simple contructor to initialize values like current like context, permission list etc
 *
 * @param mActivity The current class context
 * @param permissionList The permission list like locations, camera etc.
 * @param isToFinish The boolean flag whether the activity is to finish or not if all permissions are not granted
 */(private val mActivity: Activity, private val permissionList: Array<String>, private val isToFinish: Boolean) {

    /**
     * <h>checkPermissions</h>
     *
     *
     * In this method we used to check whether given permissions are granted or not.
     *
     * @param permissions The permissions array
     * @return The boolean flag
     */
    fun checkPermissions(permissions: Array<String>): Boolean {
        for (permission in permissions) {
            val result = ContextCompat.checkSelfPermission(mActivity, permission!!)
            if (result != PackageManager.PERMISSION_GRANTED) {
                return false
            }
        }
        return true
    }

    /**
     * <h>RequestPermission</h>
     *
     *
     * In this method we used to set runtime permissions.
     *
     */
    fun requestPermission() {
        ActivityCompat.requestPermissions(mActivity, permissionList, VariableConstants.PERMISSION_REQUEST_CODE)
    }

    /**
     * <h>ShowErrorMessage</h>
     *
     *
     * In this method we used to show dialog for error message if
     * the user denies any permissions.
     *
     * @param permissionName the permission name
     */
    fun allowPermissionAlert(permissionName: String) {
        val errorMessageDialog = Dialog(mActivity)
        errorMessageDialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        errorMessageDialog.setContentView(R.layout.dialog_permission_denied)
        errorMessageDialog.window!!.setGravity(Gravity.BOTTOM)
        errorMessageDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        errorMessageDialog.window!!.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT)
        errorMessageDialog.window!!.attributes.windowAnimations = R.style.DialogAnimation
        val tV_permission_name = errorMessageDialog.findViewById<View>(R.id.tV_permission_name) as TextView
        var deniedPermission: String
        deniedPermission = if (permissionName.contains("CAMERA")) {
            mActivity.resources.getString(R.string.camera)
        } else if (permissionName.contains("WRITE_EXTERNAL_STORAGE")) {
            mActivity.resources.getString(R.string.external_storage)
        } else if (permissionName.contains("ACCESS_COARSE_LOCATION")) {
            mActivity.resources.getString(R.string.coarse_location)
        } else if (permissionName.contains("ACCESS_FINE_LOCATION")) {
            mActivity.resources.getString(R.string.fine_location)
        } else if (permissionName.contains("READ_CONTACTS")) {
            mActivity.resources.getString(R.string.read_contact)
        } else permissionName
        deniedPermission = mActivity.resources.getString(R.string.please_allow_the_permission) + " " + deniedPermission + " " + mActivity.resources.getString(R.string.by_clicking_on_allow_button)
        tV_permission_name.text = deniedPermission

        // Cancel
        val tV_cancel = errorMessageDialog.findViewById<View>(R.id.tV_cancel) as TextView
        tV_cancel.setOnClickListener {
            errorMessageDialog.dismiss()
            if (isToFinish) mActivity.finish()
        }

        // allow
        val rL_allow = errorMessageDialog.findViewById<View>(R.id.rL_allow) as RelativeLayout
        rL_allow.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (mActivity.shouldShowRequestPermissionRationale(permissionName)) ActivityCompat.requestPermissions(mActivity, arrayOf(permissionName), VariableConstants.PERMISSION_REQUEST_CODE)
            }
            errorMessageDialog.dismiss()
        }
        errorMessageDialog.show()
    }

}