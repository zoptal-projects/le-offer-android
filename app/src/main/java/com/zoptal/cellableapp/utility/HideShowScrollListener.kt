package com.zoptal.cellableapp.utility

import androidx.recyclerview.widget.RecyclerView


/**
 * This class is a ScrollListener for RecyclerView that allows to show/hide
 * views when list is scrolled.
 * @since 11-Aug-17.
 */
abstract class HideShowScrollListener : RecyclerView.OnScrollListener() {
    private var scrolledDistance = 0
    private var controlsVisible = true
    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView!!, dx, dy)
        onScrolled()
        if (scrolledDistance > HIDE_THRESHOLD && controlsVisible) {
            onHide()
            controlsVisible = false
            scrolledDistance = 0
        } else if (scrolledDistance < -HIDE_THRESHOLD && !controlsVisible) {
            onShow()
            controlsVisible = true
            scrolledDistance = 0
        }
        if (controlsVisible && dy > 0 || !controlsVisible && dy < 0) {
            scrolledDistance += dy
        }
    }

    abstract fun onHide()
    abstract fun onShow()
    abstract fun onScrolled()

    companion object {
        private const val HIDE_THRESHOLD = 20
    }
}