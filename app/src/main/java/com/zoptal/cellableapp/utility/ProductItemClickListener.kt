package com.zoptal.cellableapp.utility


import android.widget.ImageView

/**
 * Created by hello on 22-Jun-17.
 */
interface ProductItemClickListener {
    fun onItemClick(pos: Int, imageView: ImageView?)
}