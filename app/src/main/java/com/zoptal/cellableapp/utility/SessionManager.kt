package com.zoptal.cellableapp.utility

import android.content.Context
import android.content.SharedPreferences

/**
 * <h>SessionManager</h>
 *
 *
 * In this class we used to save a string value like token, user name etc
 * into SharedPreferences. And whereever we want that value then we used to
 * make instance of same class then fetch that.
 *
 * @since 4/5/2017
 */
class SessionManager(mActivity: Context) {
    private val sharedpreferences: SharedPreferences
    private val editor: SharedPreferences.Editor

    /**
     * Save Token created by FCM
     */
    var pushToken: String?
        get() = sharedpreferences.getString(PUSH_TOKEN, "")
        set(pushToken) {
            editor.putString(PUSH_TOKEN, pushToken)
            editor.apply()
        }

    // Save Device id
    var deviceId: String?
        get() = sharedpreferences.getString(DEVICE_ID, "")
        set(deviceId) {
            editor.putString(DEVICE_ID, deviceId)
            editor.apply()
        }

    // User pay pal
    var userPayPal: String?
        get() = sharedpreferences.getString(USER_PAYPAL, "")
        set(pay_pal) {
            editor.putString(USER_PAYPAL, pay_pal)
            editor.apply()
        }

    /**
     * Save auth token after login or sign up
     */
    var authToken: String?
        get() = sharedpreferences.getString(AUTH_TOKEN, "")
        set(authToken) {
            editor.putString(AUTH_TOKEN, authToken)
            editor.apply()
        }

    /**
     * save loggged in user flag i.e true if user is logged in else false.
     */
    var isUserLoggedIn: Boolean
        get() = sharedpreferences.getBoolean(IS_LOGGED_IN, false)
        set(flag) {
            editor.putBoolean(IS_LOGGED_IN, flag)
            editor.commit()
        }

    /**
     * Save user name, user image & user full name after login or sign up api response
     */
    var userName: String?
        get() = sharedpreferences.getString(USER_NAME, "")
        set(userName) {
            editor.putString(USER_NAME, userName)
            editor.apply()
        }

    // user full name
    var userFullName: String?
        get() = sharedpreferences.getString(USER_FULL_NAME, "")
        set(userName) {
            editor.putString(USER_FULL_NAME, userName)
            editor.apply()
        }

    // user image
    var userImage: String?
        get() = sharedpreferences.getString(USER_IMAGE, "")
        set(userImage) {
            editor.putString(USER_IMAGE, userImage)
            editor.apply()
        }

    fun getmqttId(): String? {
        return sharedpreferences.getString("mqttId", null)
    }

    // User ID
    fun setmqttId(mqttId: String?) {
        editor.putString("mqttId", mqttId)
        editor.apply()
    }

    // User ID
    var userId: String?
        get() = sharedpreferences.getString(USER_ID, "")
        set(userId) {
            editor.putString(USER_ID, userId)
            editor.apply()
        }

    // set login with like normal login, facebook or google plus
    var loginWith: String?
        get() = sharedpreferences.getString(LOGIN_WITH, "")
        set(loginWith) {
            editor.putString(LOGIN_WITH, loginWith)
            editor.apply()
        }

    // save phone contect friend count
    var contectFriendCount: Int
        get() = sharedpreferences.getInt(CONTECT_FRIEND_COUNT, 0)
        set(contectFriendCount) {
            editor.putInt(CONTECT_FRIEND_COUNT, contectFriendCount)
            editor.apply()
        }

    // save facebook friend count
    var fbFriendCount: Int
        get() = sharedpreferences.getInt(FB_FRIEND_COUNT, 0)
        set(fbFriendCount) {
            editor.putInt(FB_FRIEND_COUNT, fbFriendCount)
            editor.apply()
        }

    var isBackgroundNotification: Boolean
        get() = sharedpreferences.getBoolean(BACKGROUNG_NOTIFICATION, false)
        set(isBackgroundNotification) {
            editor.putBoolean(BACKGROUNG_NOTIFICATION, isBackgroundNotification)
            editor.apply()
        }

    var chatSync: Boolean
        get() = sharedpreferences.getBoolean(ISCHATLISTSYNCE, false)
        set(isSynce) {
            editor.putBoolean(ISCHATLISTSYNCE, isSynce)
            editor.apply()
        }

    /**
     * save user current lat and lng
     */
    var currentLat: String?
        get() = sharedpreferences.getString(CURRENT_LAT, "")
        set(lat) {
            editor.putString(CURRENT_LAT, lat)
            editor.apply()
        }

    var currentLng: String?
        get() = sharedpreferences.getString(CURRENT_LNG, "")
        set(lng) {
            editor.putString(CURRENT_LNG, lng)
            editor.apply()
        }

    var privacyAccept: Boolean
        get() = sharedpreferences.getBoolean(PRIVACY_ACCEPT, false)
        set(value) {
            editor.putBoolean(PRIVACY_ACCEPT, value)
            editor.apply()
        }

    var signUpPrivacyAccept: Boolean
        get() = sharedpreferences.getBoolean(SIGN_UP_PRIVACY_ACCEPT, false)
        set(value) {
            editor.putBoolean(SIGN_UP_PRIVACY_ACCEPT, value)
            editor.apply()
        }

    companion object {
        private const val MY_PREFERENCES = "MyPrefs"

        // Declare contant variables
        private const val AUTH_TOKEN = "auth_token"
        private const val IS_LOGGED_IN = "logged_in"
        private const val USER_NAME = "user_name"
        private const val USER_FULL_NAME = "user_full_name"
        private const val USER_IMAGE = "user_image"
        private const val USER_ID = "user_id"
        private const val LOGIN_WITH = "login_with"
        private const val PUSH_TOKEN = "push_token"
        private const val DEVICE_ID = "device_id"
        private const val FB_FRIEND_COUNT = "fb_friend_count"
        private const val CONTECT_FRIEND_COUNT = "contect_friend_count"
        private const val BACKGROUNG_NOTIFICATION = "background_notification"
        private const val ISCHATLISTSYNCE = "chatListSync"
        private const val CURRENT_LAT = "current_lat"
        private const val CURRENT_LNG = "current_lng"
        private const val USER_PAYPAL = "userPaypal"
        private const val PRIVACY_ACCEPT = "privacyAccepted"
        private const val SIGN_UP_PRIVACY_ACCEPT = "signUpPrivacyAccepted"
    }

    init {
        sharedpreferences = mActivity.getSharedPreferences(MY_PREFERENCES, Context.MODE_PRIVATE)
        editor = sharedpreferences.edit()
        editor.apply()
    }
}