/*
 * Copyright 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zoptal.cellableapp.utility


import android.content.Context
import android.util.AttributeSet
import android.util.DisplayMetrics
import android.util.Log
import android.view.TextureView
import android.view.WindowManager

/**
 * A [TextureView] that can be adjusted to a specified aspect ratio.
 */
class AutoFitTextureView @JvmOverloads constructor(context: Context?, attrs: AttributeSet? = null, defStyle: Int = 0) : TextureView(context, attrs, defStyle) {
    private var mRatioWidth = 0
    private var mRatioHeight = 0
    private var correctWidth = -1
    private var correctHeight = -1
    var mMetrics = DisplayMetrics()

    /**
     * Sets the aspect ratio for this view. The size of the view will be measured based on the ratio
     * calculated from the parameters. Note that the actual sizes of parameters don't matter, that
     * is, calling setAspectRatio(2, 3) and setAspectRatio(4, 6) make the same result.
     *
     * @param width  Relative horizontal size
     * @param height Relative vertical size
     */
    fun setAspectRatio(width: Int, height: Int) {
        require(!(width < 0 || height < 0)) { "Size cannot be negative." }
        mRatioWidth = width
        mRatioHeight = height
        requestLayout()
    }

    //    @Override
    //    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    //        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    //        int width = MeasureSpec.getSize(widthMeasureSpec);
    //        int height = MeasureSpec.getSize(heightMeasureSpec);
    //        if (0 == mRatioWidth || 0 == mRatioHeight) {
    //            setMeasuredDimension(width, height);
    //        } else {
    //            if (width < height * mRatioWidth / mRatioHeight) {
    //                setMeasuredDimension(width, width * mRatioHeight / mRatioWidth);
    //            } else {
    //                setMeasuredDimension(height * mRatioWidth / mRatioHeight, height);
    //            }
    //        }
    //    }
    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        val width = MeasureSpec.getSize(widthMeasureSpec)
        val height = MeasureSpec.getSize(heightMeasureSpec)
        Log.d("log91", "$width $height")
        if (0 == mRatioWidth || 0 == mRatioHeight) {
            setMeasuredDimension(width, height)
        } else {
            val windowManager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
            windowManager.defaultDisplay.getMetrics(mMetrics)
            val ratio = mRatioWidth.toDouble() / mRatioHeight.toDouble()
            val invertedRatio = mRatioHeight.toDouble() / mRatioWidth.toDouble()
            val portraitHeight = width * invertedRatio
            val portraitWidth = width * (mMetrics.heightPixels / portraitHeight)
            val landscapeWidth = height * ratio
            val landscapeHeight = mMetrics.widthPixels / landscapeWidth * height
            if (width < height * mRatioWidth / mRatioHeight) {
                setMeasuredDimension(portraitWidth.toInt(), mMetrics.heightPixels)
                correctWidth = portraitWidth.toInt()
                correctHeight = mMetrics.heightPixels
                Log.d("log92", (portraitWidth as Int).toString() + " " + mMetrics.heightPixels)
            } else {
                if (correctWidth != -1 && correctHeight != -1) {
                    Log.d("log94", "94")
                    setMeasuredDimension(correctWidth, correctHeight)
                } else {
                    Log.d("log95", "95")
                    setMeasuredDimension(mMetrics.widthPixels, landscapeHeight.toInt())
                }
                Log.d("log93", mMetrics.widthPixels.toString() + " " + landscapeHeight.toInt())
            }
        }
    }
}