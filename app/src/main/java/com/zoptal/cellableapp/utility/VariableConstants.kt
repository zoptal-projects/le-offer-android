package com.zoptal.cellableapp.utility


/**
 * <h>VariableConstants</h>
 *
 *
 * In this class we used to declare the java constant variables
 * which is being used throughout the app.
 *
 * @since 4/4/2017
 */
object VariableConstants {
    const val REVIEW_COUNT_UPDATE = 202
    const val CHAT_MESSAGE_SCREEN = 1230
    const val PRIVACY_POLICY = 212
    const val PRIVACY_POLICY1 = 21112
    const val DATE_FORMAT = "yyyy-MM-dd HH:mm:ss"
    const val PERMISSION_REQUEST_CODE = 8051
    const val HOME_FILTER_LOCATION_ACCESS_REQ = 121
    const val PROFILE_REQUEST_CODE = 100
    const val FILTER_REQUEST_CODE = 578
    const val REQUEST_CHECK_SETTINGS = 317
    const val CATEGORY_REQUEST_CODE = 876
    const val CONDITION_REQUEST_CODE = 856
    const val CURRENCY_REQUEST_CODE = 816
    const val CHOOSE_LOCATION_REQ_CODE = 106
    const val CHANGE_LOC_REQ_CODE = 708
    const val GOOGLE_LOGIN_REQ_CODE = 701
    const val SELECT_GALLERY_IMG_REQ_CODE = 908
    const val CAMERA_IMAGE_LIST_REQ = 909
    const val PRODUCT_DETAILS_REQ_CODE = 903
    const val EXTRA_ANIMAL_IMAGE_TRANSITION_NAME = "animal_image_transition_name"
    const val TYPE_MANUAL = "1"
    const val TYPE_FACEBOOK = "2"
    const val TYPE_GOOGLE = "3"
    const val DEVICE_TYPE = "2"
    const val VERIFY_EMAIL_REQ_CODE = 757
    const val SELLING_REQ_CODE = 680
    const val PAYPAL_REQ_CODE = 979
    const val STRIPE_ACCOUNT_ADDED_PROFILE = 101
    const val STRIPE_ACCOUNT_ADDED_HOME = 102
    const val TWITTER_KEY = "UYTjkp9rrbKct3iY0kCTrP21s"
    const val TWITTER_SECRET = "k2hmiPMUyNnJAp6EChMRfkumGS8YWbsgOSXdn45VgIcgsCC4vg"
    const val TWEETER_REQUEST_CODE = 478
    var IS_TO_SHOW_START_BROWSING = false
    const val GET_PAYPAL_LINK = "https://www.paypal.com/paypalme/grab"
    const val USER_FOLLOW_REQ_CODE = 9795
    const val FB_FRIEND_REQ_CODE = 463
    const val CONTACT_FRIEND_REQ_CODE = 461

    //static final String BODY_AUT_KEY="yelo";
    //static final String BODY_AUT_PASSWORD="&jno-@8az=wSo*NHYVGpF^AQ?4yn36ZvW5ToUCUN+XGOuC?sz#SE$oxXVbwQGP|3WFyjcTAj2SIRQnLE|vo^-|-ATV5FZUf2*5A3Oiu|_EOMmG==&iApzQL3R7HHQj?jtb0mc2mT$Y%Isrgrxveld#Z^g3-ul^|0xAITganIuF23J0waSa6z6aP_+%De5LqtuY&ptx?qhZySECdyE^*4R^b*hFjQ-9?cCSJNfROzztEYbRyN=SqDyhhpzSmmP|Eb";
    const val BODY_AUT_KEY = "basicAuth"
    const val BODY_AUT_PASSWORD = "&jno-@8az=wSo*NHYVGpF^AQ?4yn36ZvW5ToUCUN+XGOuC?sz#SE\$oxXVbwQGP|3WFyjcTAj2SIRQnLE|vo^-|-ATV5FZUf2*5A3Oiu|_EOMmG==&iApzQL3R7HHQj?jtb0mc2mT\$Y%Isrgrxveld#Z^g3-ul^|0xAITganIuF23J0waSa6z6aP_+%De5LqtuY&ptx?qhZySECdyE^*4R^b*hFjQ-9?cCSJNfROzztEYbRyN=SqDyhhpzSmmP|Eb"
    const val FOLLOW_COUNT_REQ_CODE = 703
    const val TEMP_PHOTO_FILE_NAME = "temp_photo.jpg"
    var IS_TO_ADD_SOLD_ITEM = false
    const val UPDATE_IMAGE_REQ_CODE = 568
    const val IS_NOTIFICATION_SEEN_REQ_CODE = 697
    const val FAV_ITEM_REQ_CODE = 902
    const val LANDING_REQ_CODE = 765
    const val LOGIN_SIGNUP_REQ_CODE = 801
    const val NUMBER_VERIFICATION_REQ_CODE = 7076
    const val RATE_USER_REQ_CODE = 890

    // set duration type variables for insight api
    const val WEEK = "week"
    const val MONTH = "month"
    const val YEAR = "year"

    /**
     * Camera Fields
     */
    //keep track of camera capture intent
    const val CAMERA_CAPTURE = 1

    //keep track of cropping intent
    const val PIC_CROP = 3

    //keep track of gallery intent
    const val PICK_IMAGE_REQUEST = 2

    //advanced catefories request
    const val SUB_CATEGORY_REQ_CODE = 1050
    const val ADD_DETAIL_DATA_REQ_CODE = 1051
    const val SELECT_VALUE_REQ_CODE = 1052
    const val ENTER_VALUE_REQ_CODE = 1053

    // exchanges items
    const val Willing_TO_EXCHANGE = 900

    //Notification channel for above 'o'
    const val BACKGROUND_SERVICE_NOTIFICATION = "5001"
    const val APP_NOTIFICATION_CHANNEL = "3001"
    const val CHAT_NOTIFICATION_CHANNEL = "3002"
    const val ACTION_START_FOURGROUND = "5002"
}