package com.zoptal.cellableapp.utility

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatImageView

/**
 * An [android.widget.ImageView] layout that maintains a consistent width to height aspect ratio.
 */
class DynamicHeightImageView : AppCompatImageView {
    private var mHeightRatio = 0.0

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {}
    constructor(context: Context?) : super(context) {}

    var heightRatio: Double
        get() = mHeightRatio
        set(ratio) {
            if (ratio != mHeightRatio) {
                mHeightRatio = ratio
                requestLayout()
            }
        }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        if (mHeightRatio > 0.0) {
            // set the image views size
            val width = MeasureSpec.getSize(widthMeasureSpec)
            val height = (width * mHeightRatio).toInt()
            setMeasuredDimension(width, height)
        } else {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        }
    }
}