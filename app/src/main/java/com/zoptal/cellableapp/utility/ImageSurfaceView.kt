package com.zoptal.cellableapp.utility


import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import android.hardware.Camera
import android.util.Log
import android.view.SurfaceHolder
import android.view.SurfaceView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import java.io.IOException

/**
 * <h>ImageSurfaceView</h>
 *
 *
 * This class is getting called from CameraActivity class. In this we extend to
 * SurfaceView class to present a live mCamera preview to the user. This class
 * implements SurfaceHolder.Callback in order to capture the callback events
 * for creating and destroying the view, which are needed for assigning the mCamera
 * preview input.
 *
 * @since 5/1/2017
 */
class ImageSurfaceView(private val mActivity: Activity, private val camera: Camera) : SurfaceView(mActivity), SurfaceHolder.Callback {
    private val surfaceHolder: SurfaceHolder
    private val permissionsArray: Array<String?>
    override fun surfaceCreated(holder: SurfaceHolder) {
        try {
            if (checkPermissions(permissionsArray)) {
                camera.setPreviewDisplay(holder)
                camera.startPreview()
            } else ActivityCompat.requestPermissions(mActivity, permissionsArray, 635)
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    override fun surfaceChanged(holder: SurfaceHolder, format: Int, width: Int, height: Int) {
        val cameraId = findFrontFacingCamera()
        if (cameraId < 0) {
            Toast.makeText(mActivity, "No  camera found.", Toast.LENGTH_LONG).show()
        } else {
            if (checkPermissions(permissionsArray)) {
                camera.setDisplayOrientation(90)
                camera.startPreview()
                refreshCamera()
            } else ActivityCompat.requestPermissions(mActivity, permissionsArray, 635)
        }
    }

    override fun surfaceDestroyed(holder: SurfaceHolder) {
        val cameraId = findFrontFacingCamera()
        if (cameraId < 0) {
            Toast.makeText(mActivity, "No  camera found.", Toast.LENGTH_LONG).show()
        } else {
            if (checkPermissions(permissionsArray)) {
                surfaceHolder.removeCallback(this)
                camera.stopPreview()
                camera.release()
            } else ActivityCompat.requestPermissions(mActivity, permissionsArray, 635)
        }
    }

    fun checkPermissions(permissions: Array<String?>): Boolean {
        for (permission in permissions) {
            val result = ContextCompat.checkSelfPermission(mActivity, permission!!)
            if (result != PackageManager.PERMISSION_GRANTED) {
                return false
            }
        }
        return true
    }

    fun refreshCamera() {
        if (surfaceHolder.surface == null) {
            // preview surface does not exist
            return
        }

        // stop preview before making changes
        try {
            camera.stopPreview()
        } catch (e: Exception) {
            // ignore: tried to stop a non-existent preview
        }

        // set preview size and make any resize, rotate or
        // reformatting changes here
        // start preview with new settings
        try {
            camera.setPreviewDisplay(surfaceHolder)
            camera.startPreview()
        } catch (e: Exception) {
        }
    }

    private fun findFrontFacingCamera(): Int {
        var cameraId = -1
        // Search for the front facing camera
        val numberOfCameras = Camera.getNumberOfCameras()
        for (i in 0 until numberOfCameras) {
            val info = Camera.CameraInfo()
            Camera.getCameraInfo(i, info)
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                Log.d(TAG, "Camera found")
                cameraId = i
                break
            }
        }
        return cameraId
    }

    companion object {
        private val TAG = ImageSurfaceView::class.java.simpleName
    }

    init {
        surfaceHolder = holder
        surfaceHolder.addCallback(this)
        permissionsArray = arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
    }
}