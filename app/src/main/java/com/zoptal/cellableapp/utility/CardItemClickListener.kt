package com.zoptal.cellableapp.utility



/**
 * Created by hello on 22-Jun-17.
 */
interface CardItemClickListener {
    fun onItemClick(pos: Int, bookmarkOrDelete: Int)
}