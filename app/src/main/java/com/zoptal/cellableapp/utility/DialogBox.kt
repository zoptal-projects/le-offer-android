package com.zoptal.cellableapp.utility


import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.view.Window
import android.widget.Button
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.main.activity.HomePageActivity
import com.zoptal.cellableapp.main.activity.PromoteItemActivity
import com.zoptal.cellableapp.main.activity.UpdatePostActivity
import com.zoptal.cellableapp.main.activity.products.ProductDetailsActivity
import com.zoptal.cellableapp.mqttchat.AppController
import com.zoptal.cellableapp.pojo_class.LogoutPojo
import com.zoptal.cellableapp.utility.OkHttp3Connection.OkHttp3RequestCallback
import com.zoptal.cellableapp.utility.OkHttp3Connection.Request_type
import org.json.JSONException
import org.json.JSONObject
import java.lang.Exception

/**
 * <h>DialogBox</h>
 *
 *
 * In this class we used to show various dialog pop-up.
 *
 *
 * @since 4/4/2017
 */
class DialogBox(private val mActivity: Activity) {
    var progressBarDialog: Dialog? = null
    private val mApiCall: ApiCall
    private val mProgressBar: ProgressBarHandler
    private val mSessionManager: SessionManager
    private val bus = AppController.bus

    /**
     * <h>ShowErrorMessage</h>
     *
     *
     * In this method we used to show dialog for error message like if
     * there is no internet connection.
     *
     *
     * @param errorTitle the message to show
     */
    private fun showErrorMessage(errorTitle: String?) {
        val errorMessageDialog = Dialog(mActivity)
        errorMessageDialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        errorMessageDialog.setContentView(R.layout.error_message_layout)
        errorMessageDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        errorMessageDialog.window!!.setLayout((CommonClass.getDeviceWidth(mActivity) * 0.8).toInt(), RelativeLayout.LayoutParams.WRAP_CONTENT)

        // Message title
        val tV_message_title = errorMessageDialog.findViewById<View>(R.id.tV_message_title) as TextView
        if (errorTitle != null) tV_message_title.text = errorTitle

        // Confirm button
        val confirm_button = errorMessageDialog.findViewById<View>(R.id.confirm_button) as Button
        confirm_button.setOnClickListener { errorMessageDialog.dismiss() }
        errorMessageDialog.show()
    }

    /**
     *
     * ShowLogoutAlert
     *
     *
     * In this method we used to show the alert for logout.
     * in this dialog we used to show two option cancel and
     * logout, If user click on cancel than just dismiss the
     * dialog else call logout api.
     *
     */
    fun showLogoutAlert() {
        val errorMessageDialog = Dialog(mActivity)
        errorMessageDialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        errorMessageDialog.setContentView(R.layout.dialog_show_alert)
        errorMessageDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        errorMessageDialog.window!!.setLayout((CommonClass.getDeviceWidth(mActivity) * 0.8).toInt(), RelativeLayout.LayoutParams.WRAP_CONTENT)
        val tV_logout: TextView
        val tV_cancel: TextView

        // Cancel
        tV_cancel = errorMessageDialog.findViewById<View>(R.id.tV_cancel) as TextView
        tV_cancel.setOnClickListener { errorMessageDialog.dismiss() }

        // logout
        tV_logout = errorMessageDialog.findViewById<View>(R.id.tV_logout) as TextView
        tV_logout.setOnClickListener {
            errorMessageDialog.dismiss()
            logoutService()
        }
        errorMessageDialog.show()
    }

    /**
     * <h>LogoutService</h>
     *
     *
     * In this method we used to call logout api, after getting success response
     * from server we used to call LoginActivity and cleaer entire activity
     * history from stack.
     *
     */
    private fun logoutService() {
        if (CommonClass.isNetworkAvailable(mActivity)) {
            mProgressBar.show()
            val request_datas = JSONObject()
            try {
                request_datas.put("token", mSessionManager.authToken)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            OkHttp3Connection.doOkHttp3Connection(TAG, ApiUrl.LOGOUT, Request_type.POST, request_datas, object : OkHttp3RequestCallback {
                override fun onSuccess(result: String?, user_tag: String?) {
                    mProgressBar.hide()
                    println("$TAG logged out res=$result")
                    val logoutPojo: LogoutPojo
                    val gson = Gson()
                    logoutPojo = gson.fromJson(result, LogoutPojo::class.java)
                    val intent: Intent
                    when (logoutPojo.code) {
                        "200" -> {
                            mSessionManager.isUserLoggedIn = false
                            mSessionManager.chatSync = false
                            AppController.instance?.setChatSynced(false)
                            AppController.instance?.isSignStatusChanged = false
                            AppController.instance?.doLoggedOut()
                            mSessionManager.userName = ""
                            mSessionManager.authToken = ""
                            mSessionManager.userImage = ""
                            AppController.instance?.dbController!!.clearIndexDocument(AppController.instance!!.indexDocId)
                            intent = Intent(mActivity, HomePageActivity::class.java)
                            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                            mActivity.startActivity(intent)
                        }
                        "204" -> {
                            intent = Intent(mActivity, HomePageActivity::class.java)
                            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                            mActivity.startActivity(intent)
                        }
                        "401" -> CommonClass.sessionExpired(mActivity)
                        else -> showErrorMessage(logoutPojo.message)
                    }
                }

                override fun onError(error: String?, user_tag: String?) {
                    mProgressBar.hide()
                    showErrorMessage(error)
                }
            })
        } else showErrorMessage(mActivity.resources.getString(R.string.NoInternetAccess))
    }

    /**
     * <h>SelectGender</h>
     *
     *
     * In this method we used to open a simple dialog pop-up to select
     * gender.
     *
     *
     * @param setGender the textview on which we set the selected text
     */
    fun selectGender(setGender: TextView) {
        val selectGenderDialog = Dialog(mActivity)
        selectGenderDialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        selectGenderDialog.setContentView(R.layout.dialog_select_gender)
        selectGenderDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        //Male text
        val tV_male = selectGenderDialog.findViewById<View>(R.id.tV_male) as TextView
        tV_male.setOnClickListener {
            setGender.text = mActivity.resources.getString(R.string.male)
            selectGenderDialog.dismiss()
        }

        // Female text
        val tV_female = selectGenderDialog.findViewById<View>(R.id.tV_female) as TextView
        tV_female.setOnClickListener {
            setGender.text = mActivity.resources.getString(R.string.female)
            selectGenderDialog.dismiss()
        }
        selectGenderDialog.show()
    }

    /**
     * <h>PostedSuccessDialog</h>
     *
     *
     * In this method we used to show simple dialog popup to show
     * the success message just after the successfull posting of
     * product. Once we click on ok then we used to clear the stack
     * and move to HomePageActivity.
     *
     */
    fun postedSuccessDialog() {
        try {
            val dialog = Dialog(mActivity)
            dialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
            dialog.setContentView(R.layout.dialog_post_product_success)
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.window!!.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT)

            // click on ok to go to Home screen
            val relative_done = dialog.findViewById<View>(R.id.relative_done) as RelativeLayout
            relative_done.setOnClickListener {
                dialog.dismiss()
                val intent = Intent(mActivity, HomePageActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                mActivity.startActivity(intent)
            }
            if (dialog != null && !dialog.isShowing)
                dialog.show()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * <h>localCampaignDialog</h>
     *
     *
     * In this method we used to open a dialog pop-up when local camapaign push
     * notification come.
     *
     *
     * @param username   The logged-in user name
     * @param userId     The logged in user id
     * @param campaignId campaign id
     * @param imageUrl   The image url for the campaign
     * @param title      The title of the campaign
     * @param message    The message
     * @param url        The browser usl
     */
    fun localCampaignDialog(username: String?, userId: String?, campaignId: String?, imageUrl: String?, title: String?, message: String?, url: String?) {
        val campaignDialog = Dialog(mActivity)
        campaignDialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        campaignDialog.setContentView(R.layout.dialog_local_campaign)
        campaignDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        campaignDialog.window!!.setLayout((CommonClass.getDeviceWidth(mActivity) * 0.9).toInt(), RelativeLayout.LayoutParams.WRAP_CONTENT)

        // cancel dialog
        val iV_cancel = campaignDialog.findViewById<View>(R.id.iV_cancel) as ImageView
        iV_cancel.setOnClickListener {
            mApiCall.userCampaignApi(username, userId, campaignId, "1")
            campaignDialog.dismiss()
        }

        // set Image
        val iV_image = campaignDialog.findViewById<View>(R.id.iV_image) as ImageView
        iV_image.layoutParams.height = (CommonClass.getDeviceWidth(mActivity) / 1.5).toInt()

        // set Image
        if (imageUrl != null && imageUrl.contains("defaultImg")) {
            iV_image.setImageResource(R.drawable.start_broswing_icon)
        } else {
            if (imageUrl != null && !imageUrl.isEmpty()) Picasso.with(mActivity)
                    .load(imageUrl)
                    .placeholder(R.drawable.start_broswing_icon)
                    .error(R.drawable.start_broswing_icon)
                    .transform(RoundedCornersTransform())
                    .into(iV_image)
        }

        // first line
        val tV_first_text = campaignDialog.findViewById<View>(R.id.tV_first_text) as TextView
        tV_first_text.visibility = View.GONE

        // set title
        val tV_title = campaignDialog.findViewById<View>(R.id.tV_title) as TextView
        if (title != null && !title.isEmpty()) tV_title.text = title

        // set message
        val tV_message = campaignDialog.findViewById<View>(R.id.tV_message) as TextView
        if (message != null && !message.isEmpty()) tV_message.text = message

        // know more
        val tV_start_browsering = campaignDialog.findViewById<View>(R.id.tV_start_browsering) as TextView
        tV_start_browsering.text = mActivity.resources.getString(R.string.know_more)

        // click on this to go to browser page
        println("$TAG url=$url img url=$imageUrl")
        val rL_start_browsing = campaignDialog.findViewById<View>(R.id.rL_start_browsing) as RelativeLayout

        // hide know more button if url is empty
        if (url == null || url == "http://") rL_start_browsing.visibility = View.GONE
        rL_start_browsing.setOnClickListener {
            if (url != null && !url.isEmpty()) {
                mApiCall.userCampaignApi(username, userId, campaignId, "2")
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
                mActivity.startActivity(browserIntent)
                campaignDialog.dismiss()
            }
        }
        if (!campaignDialog.isShowing) campaignDialog.show()
    }

    /**
     * <h>openShareOptionDialog</h>
     *
     *
     * In this method we used to open a dialog to show option like sharing or copy item url.
     *
     */
    fun openUpdateProductDialog(postId: String?, rootView: View?, bundlePostDatas: Bundle?) {
        val shareDialog = Dialog(mActivity)
        shareDialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        shareDialog.setContentView(R.layout.dialog_update_product)
        shareDialog.window!!.setGravity(Gravity.BOTTOM)
        shareDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        shareDialog.window!!.attributes.windowAnimations = R.style.DialogAnimation
        shareDialog.window!!.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT)

        // delete post
        val rL_delete_post = shareDialog.findViewById<View>(R.id.rL_delete_post) as RelativeLayout
        rL_delete_post.setOnClickListener { /*ProfileFrag profileFrag = new ProfileFrag();
                profileFrag.updatePostcount();*/
            bus.post(1)
            shareDialog.dismiss()
            showProgressDialog(mActivity.resources.getString(R.string.deleting))
            mApiCall.deletePostApi(postId!!, rootView, progressBarDialog)
            bus.unregister(mActivity)
        }

        // Edit post
        val rL_edit_post = shareDialog.findViewById<View>(R.id.rL_edit_post) as RelativeLayout
        rL_edit_post.setOnClickListener {
            shareDialog.dismiss()
            if (bundlePostDatas != null) {
                val intent = Intent(mActivity, UpdatePostActivity::class.java)
                intent.putExtras(bundlePostDatas)
                mActivity.startActivity(intent)
            }
        }

        // cancel
        val cancel_button = shareDialog.findViewById<View>(R.id.cancel_button) as TextView
        cancel_button.setOnClickListener { shareDialog.dismiss() }
        shareDialog.show()
    }

    /**
     * <h>SellSomeWhereDialog</h>
     *
     *
     * In this method we used to call api to switch product from selling tab into selling tab.
     *
     */
    fun sellSomeWhereDialog(postId: String?, rootView: View?) {
        val errorMessageDialog = Dialog(mActivity)
        errorMessageDialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        errorMessageDialog.setContentView(R.layout.dialog_sell_it_again)
        errorMessageDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        errorMessageDialog.window!!.setLayout((CommonClass.getDeviceWidth(mActivity) * 0.8).toInt(), RelativeLayout.LayoutParams.WRAP_CONTENT)

        // message title
        val tV_title = errorMessageDialog.findViewById<View>(R.id.tV_title) as TextView
        tV_title.text = mActivity.resources.getString(R.string.want_to_sell_somewhere_else)

        // dismiss
        val tV_no = errorMessageDialog.findViewById<View>(R.id.tV_no) as TextView
        tV_no.setOnClickListener { errorMessageDialog.dismiss() }

        // yes
        val tV_yes = errorMessageDialog.findViewById<View>(R.id.tV_yes) as TextView
        tV_yes.setOnClickListener {
            if (CommonClass.isNetworkAvailable(mActivity)) {
                showProgressDialog(mActivity.resources.getString(R.string.Loading))
                mApiCall.sellSomeWhereElseApi(rootView, postId, progressBarDialog)
                errorMessageDialog.dismiss()
            } else CommonClass.showSnackbarMessage(rootView, mActivity.resources.getString(R.string.NoInternetAccess))
        }
        errorMessageDialog.show()
    }

    /**
     * <h>ShowProgressDialog</h>
     *
     *
     * In this method we used to show the custom progress dialog.
     *
     */
    fun showProgressDialog(progressBarText: String?) {
        progressBarDialog = Dialog(mActivity)
        progressBarDialog!!.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        progressBarDialog!!.setContentView(R.layout.dialog_custom_progress)
        progressBarDialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        progressBarDialog!!.setCancelable(false)
        val tV_loading = progressBarDialog!!.findViewById<View>(R.id.tV_loading) as TextView
        if (progressBarText != null && !progressBarText.isEmpty()) tV_loading.text = progressBarText

        //The new dim amount, from 0 for no dim to 1 for full dim.
        progressBarDialog!!.window!!.setDimAmount(0.5f)
        if (!mActivity.isFinishing) progressBarDialog!!.show()
    }

    /**
     * <h>StartBrowsingDialog</h>
     *
     *
     * In this method we used to open a simple dialog box to show the start browsing pop-up
     * once just after the registration.
     *
     */
    fun startBrowsingDialog() {
        val dialog = Dialog(mActivity)
        dialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_start_browsing)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.setLayout((CommonClass.getDeviceWidth(mActivity) * 0.9).toInt(), RelativeLayout.LayoutParams.WRAP_CONTENT)
        val rL_start_browsing = dialog.findViewById<View>(R.id.rL_start_browsing) as RelativeLayout

        // start browsing
        rL_start_browsing.setOnClickListener { dialog.dismiss() }

        // close
        val rL_close = dialog.findViewById<View>(R.id.rL_close) as RelativeLayout
        rL_close.setOnClickListener { dialog.dismiss() }
        dialog.show()
    }

    fun openEditProductDialog(postId: String?, rootView: View?, bundlePostDatas: Bundle?) {
        val shareDialog = Dialog(mActivity)
        shareDialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        shareDialog.setContentView(R.layout.dialog_edit_product)
        shareDialog.window!!.setGravity(Gravity.BOTTOM)
        shareDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        shareDialog.window!!.attributes.windowAnimations = R.style.DialogAnimation
        shareDialog.window!!.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT)

        // delete post
        val rL_delete_post = shareDialog.findViewById<View>(R.id.rL_delete_post) as RelativeLayout
        rL_delete_post.setOnClickListener {
            /*ProfileFrag profileFrag = new ProfileFrag();
                profileFrag.updatePostcount();*/
            //BusProvider.getInstance().post(1);
            shareDialog.dismiss()
            showProgressDialog(mActivity.resources.getString(R.string.deleting))
            mApiCall.deletePostApi(postId!!, rootView, progressBarDialog)
            //BusProvider.getInstance().unregister(mActivity);
        }

        // Edit post
        val rL_edit_post = shareDialog.findViewById<View>(R.id.rL_edit_post) as RelativeLayout
        rL_edit_post.setOnClickListener {
            shareDialog.dismiss()
            if (bundlePostDatas != null) {
                val intent = Intent(mActivity, UpdatePostActivity::class.java)
                intent.putExtras(bundlePostDatas)
                mActivity.startActivity(intent)
            }
        }

        // Promote post
        val rL_promote_post = shareDialog.findViewById<View>(R.id.rL_promote_post) as RelativeLayout
        rL_promote_post.setOnClickListener {
            shareDialog.dismiss()
            if (bundlePostDatas != null) {
                val intent = Intent(mActivity, PromoteItemActivity::class.java)
                intent.putExtra("price", bundlePostDatas.getString("price"))
                intent.putExtra("productName", bundlePostDatas.getString("productName"))
                intent.putExtra("productImage", bundlePostDatas.getString("productImage"))
                intent.putExtra("isPromoted", bundlePostDatas.getInt("isPromoted"))
                intent.putExtra("productId", postId)
                mActivity.startActivity(intent)
            }
        }

        // cancel
        val cancel_button = shareDialog.findViewById<View>(R.id.cancel_button) as TextView
        cancel_button.setOnClickListener { shareDialog.dismiss() }
        shareDialog.show()
    }

    companion object {
        private val TAG = DialogBox::class.java.simpleName
    }

    /**
     * <h>DialogBox</h>
     *
     *
     * This is simple constructor to initialize the context from activity.
     *
     *
     * @param mActivity the current context.
     */
    init {
        mApiCall = ApiCall(mActivity)
        mProgressBar = ProgressBarHandler(mActivity)
        mSessionManager = SessionManager(mActivity)
        if (mActivity is ProductDetailsActivity) bus.register(mActivity)

        //BusProvider.getInstance().register(mActivity);
    }
}