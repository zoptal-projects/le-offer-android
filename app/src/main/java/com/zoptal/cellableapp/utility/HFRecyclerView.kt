package com.zoptal.cellableapp.utility


import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

/**
 * Copyright (C) 2016 Mikhael LOPEZ
 * Licensed under the Apache License Version 2.0
 */
abstract class HFRecyclerView<T>(private val data: List<T>, private val mWithHeader: Boolean, private val mWithFooter: Boolean) : RecyclerView.Adapter<RecyclerView.ViewHolder?>() {

    //region Get View
    protected abstract fun getItemView(inflater: LayoutInflater?, parent: ViewGroup?): RecyclerView.ViewHolder
    protected abstract fun getHeaderView(inflater: LayoutInflater?, parent: ViewGroup?): RecyclerView.ViewHolder
    protected abstract fun getFooterView(inflater: LayoutInflater?, parent: ViewGroup?): RecyclerView.ViewHolder

    //endregion
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        if (viewType == TYPE_ITEM) {
            return getItemView(inflater, parent)
        } else if (viewType == TYPE_HEADER) {
            return getHeaderView(inflater, parent)
        } else if (viewType == TYPE_FOOTER) {
            return getFooterView(inflater, parent)
        }
        throw RuntimeException("there is no type that matches the type $viewType + make sure your using types correctly")
    }

    override fun getItemCount(): Int {
        var itemCount = data.size
        if (mWithHeader) itemCount++
        if (mWithFooter) itemCount++
        return itemCount
    }

    override fun getItemViewType(position: Int): Int {
        if (mWithHeader && isPositionHeader(position)) return TYPE_HEADER
        return if (mWithFooter && isPositionFooter(position)) TYPE_FOOTER else TYPE_ITEM
    }

    private fun isPositionHeader(position: Int): Boolean {
        return position == 0
    }

    private fun isPositionFooter(position: Int): Boolean {
        return position == itemCount - 1
    }

    protected fun getItem(position: Int): T {
        return if (mWithHeader) data[position - 1] else data[position]
    }

    companion object {
        private const val TYPE_HEADER = 0
        private const val TYPE_ITEM = 1
        private const val TYPE_FOOTER = 2
    }

}