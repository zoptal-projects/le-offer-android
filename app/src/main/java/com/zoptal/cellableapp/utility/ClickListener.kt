package com.zoptal.cellableapp.utility


import android.view.View

/**
 * <h>ClickListener</h>
 *
 *
 * In this interface we used to define onItemClick method. to get position and view
 * whenever we click on recyclerview any particular row.
 *
 * @since 4/12/2017.
 */
interface ClickListener {
    fun onItemClick(view: View?, position: Int)
}