package com.zoptal.cellableapp.utility



/**
 * Created by tuanhai on 11/3/15.
 */
interface OnLoadMoreListener {
    fun onLoadMore()
}