package com.zoptal.cellableapp.utility


import java.io.Serializable

/**
 * Created by hello on 14-Sep-17.
 */
class CapturedImage : Serializable {
    var imagePath: String? = null
    var rotateAngle = 0

}