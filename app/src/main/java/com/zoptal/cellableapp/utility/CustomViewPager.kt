package com.zoptal.cellableapp.utility

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import androidx.viewpager.widget.ViewPager


/**
 * This Class is used to disable swiping motion as  per google guide lines there is not swiping motion
 * @author Waleed Sarwar
 * @since September 28, 2016 10:47 AM
 */
class CustomViewPager(context: Context?, attrs: AttributeSet?) : ViewPager(context!!, attrs) {
    var isPagingEnabled = true

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        return isPagingEnabled && super.onTouchEvent(event)
    }

    override fun onInterceptTouchEvent(event: MotionEvent?): Boolean {
        return isPagingEnabled && super.onInterceptTouchEvent(event)
    }

}