package com.zoptal.cellableapp.utility

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import android.view.View
import android.widget.ImageView
import com.google.gson.Gson
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.event_bus.EventBusDatasHandler
import com.zoptal.cellableapp.pojo_class.DeletePostPojo
import com.zoptal.cellableapp.pojo_class.FollowUserPojo
import com.zoptal.cellableapp.pojo_class.LikeProductPojo
import com.zoptal.cellableapp.pojo_class.mark_selling.MarkSellingMainPojo
import com.zoptal.cellableapp.pojo_class.sold_somewhere_else_pojo.SoldSomeWhereMainPojo
import com.zoptal.cellableapp.utility.OkHttp3Connection.OkHttp3RequestCallback
import org.apache.http.client.HttpClient
import org.apache.http.client.methods.HttpGet
import org.apache.http.impl.client.DefaultHttpClient
import org.json.JSONException
import org.json.JSONObject
import java.io.InputStream

/**
 * <h>ApiCall</h>
 *
 *
 * In this class we used to do api call like like or unlike the product etc.
 *
 * @since 02-Jun-17
 * @author 3Embed
 * @version 1.0
 */
class ApiCall(private val mActivity: Activity) {
    private val mSessionManager: SessionManager
    private val mEventBusDatasHandler: EventBusDatasHandler

    /**
     * <h>StaticMapApi</h>
     *
     *
     * In this method we used to hit google static map api using HttpClient. In the respose
     * of this api we get the bitmap of static map then we set that map into imageview.
     *
     * @param iv_staticMap The imageview
     * @param lat The given latitude
     * @param lng The given longitude.
     */
    fun staticMapApi(iv_staticMap: ImageView, lat: String, lng: String) {
        val STATIC_MAP_API_ENDPOINT = "https://maps.googleapis.com/maps/api/staticmap?center=$lat,$lng&zoom=14&size=600x250"
        val setImageFromUrl: AsyncTask<Void, Void, Bitmap> = object : AsyncTask<Void, Void, Bitmap>() {
            override fun doInBackground(vararg params: Void?): Bitmap? {
                var bmp: Bitmap? = null
                val httpclient: HttpClient = DefaultHttpClient()
                val request = HttpGet(STATIC_MAP_API_ENDPOINT)
                val `in`: InputStream
                try {
                    `in` = httpclient.execute(request).entity.content
                    bmp = BitmapFactory.decodeStream(`in`)
                    `in`.close()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                return bmp
            }

            override fun onPostExecute(bmp: Bitmap?) {
                if (bmp != null) {
                    iv_staticMap.setImageBitmap(bmp)
                }
            }
        }
        setImageFromUrl.execute()
    }

    /**
     * <h>LikeProductApi</h>
     *
     *
     * In this method we used to do api call for like or unlike product using OkHttpClient.
     *
     * @param ServiceUrl The APiUrl
     */
    fun likeProductApi(ServiceUrl: String?, postId: String?) {
        val request_datas = JSONObject()
        try {
            request_datas.put("token", mSessionManager.authToken)
            request_datas.put("postId", postId)
            request_datas.put("label", "Photo")
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        OkHttp3Connection.doOkHttp3Connection(TAG, ServiceUrl, OkHttp3Connection.Request_type.POST, request_datas, object : OkHttp3RequestCallback {
             override fun onSuccess(result: String?, user_tag: String?) {
                println("$TAG like product res=$result")
                val gson = Gson()
                val likeProductPojo = gson.fromJson(result, LikeProductPojo::class.java)
                when (likeProductPojo.code) {
                    "200" -> println(TAG + " " + "success message=" + likeProductPojo.message)
                    "401" -> CommonClass.sessionExpired(mActivity)
                    else -> println(TAG + " " + "like product error message=" + likeProductPojo.message)
                }
            }

             override fun onError(error: String?, user_tag: String?) {
                println("$TAG like product error message=$error")
            }
        })
    }

    /**
     * <h>FollowUserApi</h>
     *
     *
     * In this build we used to follow or unfollow the user.
     *
     * @param ApiUrl The service url to follow or unfollow
     */
    fun followUserApi(ApiUrl: String) {
        val requestDatas = JSONObject()
        try {
            //requestDatas.put(userNameParam,membername);
            requestDatas.put("token", mSessionManager.authToken)
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        println("$TAG url=$ApiUrl")
        OkHttp3Connection.doOkHttp3Connection(TAG, ApiUrl, OkHttp3Connection.Request_type.POST, requestDatas, object : OkHttp3RequestCallback {
             override fun onSuccess(result: String?, user_tag: String?) {
                println("$TAG follow user api res=$result")
                val followUserPojo: FollowUserPojo
                val gson = Gson()
                followUserPojo = gson.fromJson(result, FollowUserPojo::class.java)
                when (followUserPojo.code) {
                    "200" -> println(TAG + " " + "follow user success=" + followUserPojo.message)
                    "401" -> CommonClass.sessionExpired(mActivity)
                    else -> println(TAG + " " + "follow user success=" + followUserPojo.message)
                }
            }

             override fun onError(error: String?, user_tag: String?) {
                println("$TAG follow user success=$error")
            }
        })
    }

    /**
     * <h>DeletePostComment</h>
     *
     *
     * In this method we used to do api call to delete particular review.
     *
     * @param commentId The product commment node id
     * @param postId The post id of a product
     */
    fun deletePostComment(commentId: String?, postId: String?) {
        if (CommonClass.isNetworkAvailable(mActivity)) {
            val requst_body = JSONObject()
            try {
                requst_body.put("commentId", commentId)
                requst_body.put("label", "Photo")
                requst_body.put("postId", postId)
                requst_body.put("token", mSessionManager.authToken)
                requst_body.put("type", "0")
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            OkHttp3Connection.doOkHttp3Connection(TAG, ApiUrl.DELETE_COMMENTS, OkHttp3Connection.Request_type.POST, requst_body, object : OkHttp3RequestCallback {
                 override fun onSuccess(result: String?, user_tag: String?) {
                    println("$TAG delete comment res=$result")
                }

                 override fun onError(error: String?, user_tag: String?) {
                    println("$TAG ")
                }
            })
        }
    }

    /**
     * <h>markSellingApi</h>
     *
     *
     * In this method we used to do api call to move item from sold to selling within
     * my profile fragment.
     *
     * @param rL_rootview The root element
     * @param postId The post id of that product.
     */
    fun markSellingApi(rL_rootview: View?, postId: String?) {
        // token, postId, type(0 : Photo, 1 : Video)
        val request_datas = JSONObject()
        try {
            request_datas.put("token", mSessionManager.authToken)
            request_datas.put("postId", postId)
            request_datas.put("type", "0")
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        OkHttp3Connection.doOkHttp3Connection(TAG, ApiUrl.MARK_SELLING, OkHttp3Connection.Request_type.POST, request_datas, object : OkHttp3RequestCallback {
             override fun onSuccess(result: String?, user_tag: String?) {
                println("$TAG mark selling success=$result")
                val markSellingMainPojo: MarkSellingMainPojo
                val gson = Gson()
                markSellingMainPojo = gson.fromJson(result, MarkSellingMainPojo::class.java)
                when (markSellingMainPojo.code) {
                    "200" -> println(TAG + " " + "sucess message=" + markSellingMainPojo.code)
                    "401" -> CommonClass.sessionExpired(mActivity)
                    else -> CommonClass.showSnackbarMessage(rL_rootview, markSellingMainPojo.message)
                }
            }

             override fun onError(error: String?, user_tag: String?) {
                CommonClass.showSnackbarMessage(rL_rootview, error)
            }
        })
    }

    /**
     * <h>SellSomeWhereElseApi</h>
     *
     *
     * In this method we used to do api call to switch item from selling to sold tab.
     *
     * @param rootView The root element of the EditProductActivity class
     * @param postId the postId of the product
     */
    fun sellSomeWhereElseApi(rootView: View?, postId: String?, pDialog: Dialog?) {
        if (CommonClass.isNetworkAvailable(mActivity)) {
            val request_datas = JSONObject()
            try {
                request_datas.put("token", mSessionManager.authToken)
                request_datas.put("postId", postId)
                request_datas.put("type", "0")
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            OkHttp3Connection.doOkHttp3Connection(TAG, ApiUrl.ELSE_WHERE, OkHttp3Connection.Request_type.POST, request_datas, object : OkHttp3RequestCallback {
                 override fun onSuccess(result: String?, user_tag: String?) {
                    println("$TAG sell somewhere else res=$result")
                    pDialog?.dismiss()
                    val soldSomeWhereMainPojo: SoldSomeWhereMainPojo
                    val gson = Gson()
                    soldSomeWhereMainPojo = gson.fromJson(result, SoldSomeWhereMainPojo::class.java)
                    when (soldSomeWhereMainPojo.code) {
                        "200" -> {
                            val soldSomeWhereData = soldSomeWhereMainPojo.data
                            mEventBusDatasHandler.addSoldDatasFromEditPost(soldSomeWhereData!!)
                            mEventBusDatasHandler.removeHomePageDatasFromEditPost(soldSomeWhereData.postId)
                            mEventBusDatasHandler.removeSocialDatasFromEditPost(soldSomeWhereData.postId)
                            mEventBusDatasHandler.removeSellingDatasFromEditPost(soldSomeWhereData.postId)
                            val intent = Intent()
                            intent.putExtra("isToSellItAgain", true)
                            mActivity.setResult(VariableConstants.SELLING_REQ_CODE, intent)
                            mActivity.finish()
                        }
                        "401" -> CommonClass.sessionExpired(mActivity)
                        else -> CommonClass.showSnackbarMessage(rootView, soldSomeWhereMainPojo.message)
                    }
                }

                 override fun onError(error: String?, user_tag: String?) {
                    pDialog?.dismiss()
                    CommonClass.showSnackbarMessage(rootView, error)
                }
            })
        } else {
            CommonClass.showSnackbarMessage(rootView, mActivity.resources.getString(R.string.NoInternetAccess))
        }
    }

    /**
     * <h>userCampaignApi</h>
     *
     *
     * In this method we used to do api call after getting local campaign push notification.
     * To register on admin that whether that given url has been opened or not.
     *
     * @param username The logged-in user name
     * @param userId The logged-in user id
     * @param campaignId The campaign id of the notification
     * @param type The type 1 means if user cancel the dialog 2 means user has clicked on see more
     */
    fun userCampaignApi(username: String?, userId: String?, campaignId: String?, type: String?) {
        // token,username,userId,campaignId,type
        val request_datas = JSONObject()
        try {
            request_datas.put("token", mSessionManager.authToken)
            request_datas.put("username", username)
            request_datas.put("userId", userId)
            request_datas.put("campaignId", campaignId)
            request_datas.put("type", type)
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        OkHttp3Connection.doOkHttp3Connection(TAG, ApiUrl.USER_CAMPAIGN, OkHttp3Connection.Request_type.POST, request_datas, object : OkHttp3RequestCallback {
             override fun onSuccess(result: String?, user_tag: String?) {
                println("$TAG user campaign res=$result")
            }

             override fun onError(error: String?, user_tag: String?) {}
        })
    }

    /**
     * <h>DeletePostApi</h>
     *
     *
     * In this method we used to do api call to delete my own post.
     *
     * @param postId The postid of the product which is to be deleted.
     */
    fun deletePostApi(postId: String, rootView: View?, pDialog: Dialog?) {
        try{
        if (CommonClass.isNetworkAvailable(mActivity)) {
            val deletePostUrl = ApiUrl.PRODUCT + "?postId=" + postId + "&token=" + mSessionManager.authToken
            println("$TAG delete post url=$deletePostUrl")
            OkHttp3Connection.doOkHttp3Connection(TAG, deletePostUrl, OkHttp3Connection.Request_type.DELETE, JSONObject(), object : OkHttp3RequestCallback {
                 override fun onSuccess(result: String?, user_tag: String?) {
                     try {
                         println("$TAG delete post res=$result")
                         pDialog?.dismiss()
                         val deletePostPojo: DeletePostPojo
                         val gson = Gson()
                         deletePostPojo = gson.fromJson(result, DeletePostPojo::class.java)
                         when (deletePostPojo.code) {
                             "200" -> {
                                 mEventBusDatasHandler.removeHomePageDatasFromEditPost(postId)
                                 mEventBusDatasHandler.removeSocialDatasFromEditPost(postId)
                                 mEventBusDatasHandler.removeSellingDatasFromEditPost(postId)
                                 val intent = Intent()
                                 intent.putExtra("isPostDeleted", true)
                                 mActivity.setResult(VariableConstants.SELLING_REQ_CODE, intent)
                                 mActivity.finish()
                             }
                             else -> CommonClass.showSnackbarMessage(rootView, deletePostPojo.message)
                         }
                     }catch (e:Exception){e.printStackTrace()}
                }

                 override fun onError(error: String?, user_tag: String?) {
                    pDialog?.dismiss()
                    CommonClass.showSnackbarMessage(rootView, error)
                }
            })
        } else {
            pDialog?.dismiss()
            CommonClass.showSnackbarMessage(rootView, mActivity.resources.getString(R.string.NoInternetAccess))
        }}catch (e:Exception){e.printStackTrace()}
    }

    companion object {
        private val TAG = ApiUrl::class.java.simpleName
    }

    init {
        mSessionManager = SessionManager(mActivity)
        mEventBusDatasHandler = EventBusDatasHandler(mActivity)
    }
}