package com.zoptal.cellableapp.utility

import com.suresh.innapp_purches.Inn_App_billing.SkuDetails

/**
 * Created by embed on 21/12/17.
 */
interface SukoCallBack {
    fun onSucess(data: List<SkuDetails?>?)
    fun onError(message: String?)
}