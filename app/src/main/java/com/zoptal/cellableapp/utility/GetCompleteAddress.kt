package com.zoptal.cellableapp.utility

import android.app.Activity
import android.os.AsyncTask
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import com.zoptal.cellableapp.R
import org.apache.http.HttpResponse
import org.apache.http.HttpStatus
import org.apache.http.client.ClientProtocolException
import org.apache.http.client.HttpClient
import org.apache.http.client.methods.HttpGet
import org.apache.http.impl.client.DefaultHttpClient
import org.json.JSONException
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.io.IOException

/**
 * <h>GetCompleteAddress</h>
 *
 *
 * In this class we used to do google api call to get the complete address for
 * the given lat and lng.
 *
 * @since 22-Nov-17
 */
class GetCompleteAddress(mActivity: Activity, lat: String, lng: String, private val tV_current_location: TextView, private val progress_bar_location: ProgressBar?) {
    private val city: String?
    private val country: String?

    private inner class RequestTask : AsyncTask<String?, String?, String?>() {
        override fun doInBackground(vararg params: String?): String? {
            val httpclient: HttpClient = DefaultHttpClient()
            val response: HttpResponse
            var responseString: String? = null
            try {
                response = httpclient.execute(HttpGet(params[0]))
                val statusLine = response.statusLine
                if (statusLine.statusCode == HttpStatus.SC_OK) {
                    val out = ByteArrayOutputStream()
                    response.entity.writeTo(out)
                    responseString = out.toString()
                    out.close()
                } else {
                    //Closes the connection.
                    response.entity.content.close()
                    throw IOException(statusLine.reasonPhrase)
                }
            } catch (e: ClientProtocolException) {
                //TODO Handle problems..
            } catch (e: IOException) {
                e.printStackTrace()
                //TODO Handle problems..
            }
            return responseString
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            //Do anything with response..
            if (progress_bar_location != null) progress_bar_location.visibility = View.GONE
            tV_current_location.visibility = View.VISIBLE
            println("$TAG address res=$result")
            if (result != null) {
                try {
                    val reader = JSONObject(result)
                    val sys = reader.getJSONArray("results")
                    val formatted_address = sys.getJSONObject(0).getString("formatted_address")
                    println("$TAG formatted_address=$formatted_address")
                    if (formatted_address != null && !formatted_address.isEmpty()) tV_current_location.text = formatted_address
                } catch (e: JSONException) {
                    e.printStackTrace()

                    //..if location not detect from google api then set manually
                    //String cityCaps=CommonClass.getFirstCaps(city);
                    val address = CommonClass.makeAddressFromCityCountry(city, country)
                    tV_current_location.text = address
                }
            }
        }
    }

    companion object {
        private val TAG = GetCompleteAddress::class.java.simpleName
    }

    init {
        city = CommonClass.getCityName(mActivity, lat.toDouble(), lng.toDouble())
        country = CommonClass.getCountryName(mActivity, lat.toDouble(), lng.toDouble())
        val url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=+" + lat + "+,+" + lng + "+&location_type=ROOFTOP&result_type=street_address&key=" + mActivity.resources.getString(R.string.google_maps_key)
        // call google map api
        RequestTask().execute(url)
    }
}