package com.zoptal.cellableapp.utility


import android.annotation.TargetApi
import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.PixelFormat
import android.graphics.drawable.BitmapDrawable
import android.location.Geocoder
import android.location.LocationManager
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.util.Base64
import android.util.DisplayMetrics
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import android.widget.VideoView
import androidx.core.content.ContextCompat
import com.facebook.internal.Utility
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.main.activity.HomePageActivity
import com.zoptal.cellableapp.mqttchat.AppController
import com.zoptal.cellableapp.top_snake_bar.TSnackbar
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.text.DateFormat
import java.text.DecimalFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern

/**
 * <h>CommonClass</h>
 *
 *
 * In this class we used to write the common class. Which is being used in one than one place
 * like isNetworkAvailable(). is being called many places where ever there is a network call.
 *
 * @since 3/31/2017
 */
object CommonClass {
    var isBackFromChat = false
    var isBackFromLogin = false
    @kotlin.jvm.JvmStatic
    fun isNetworkAvailable(activity: Activity): Boolean {
        val connectivityManager = activity.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }

    /**
     * <h>GetDeviceWidth</h>
     *
     *
     * In this method we used to find the width of the current android device.
     *
     * @param activity the current context
     * @return it returns the width of the device in pixel
     */
    @kotlin.jvm.JvmStatic
    fun getDeviceWidth(activity: Activity): Int {
        val metrics = DisplayMetrics()
        activity.windowManager.defaultDisplay.getMetrics(metrics)
        //int height = metrics.heightPixels;
        //int width = metrics.widthPixels;
        return metrics.widthPixels
    }

    /**
     * <h>GetDeviceHeight</h>
     *
     *
     * In this method we used to find the height of the current android device.
     *
     * @param activity the current context
     * @return it returns the height of the device in pixel
     */
    fun getDeviceHeight(activity: Activity): Int {
        val metrics = DisplayMetrics()
        activity.windowManager.defaultDisplay.getMetrics(metrics)
        //int height = metrics.heightPixels;
        //int width = metrics.widthPixels;
        return metrics.heightPixels
    }

    /**
     *
     *
     * This method is used to validate email Id
     *
     *
     * @param email the email-address which is to be checked
     * @return boolean value true is the id format will be correct
     */
    fun isValidEmail(email: String?): Boolean {
        val EMAIL_PATTERN = ("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")
        val pattern = Pattern.compile(EMAIL_PATTERN)
        val matcher = pattern.matcher(email)
        return matcher.matches()
    }

    /**
     *
     *
     * This method is used to validate username
     *
     *
     * @param username the user name which is to be checked
     * @return boolean value true is the id format will be correct
     */
    fun isValidUserName(username: String?): Boolean {
        val USERNAME_PATTERN = "^[a-z0-9_-]{3,15}$"
        val pattern = Pattern.compile(USERNAME_PATTERN)
        val matcher = pattern.matcher(username)
        return matcher.matches()
    }

    fun isValidPhoneNumber(numebr: String): Boolean {
        return if (numebr.length > 6 && numebr.length < 13) {
            true
        } else {
            false
        }
    }

    /**
     * <h>GenerateHashKey</h>
     *
     *
     * In this method we used to generate the debug facebook has-key.
     *
     * @param context the current context
     * @return it returns the haskey of the app.
     */
    fun generateHashKey(context: Context): String {
        var hashKey = ""
        try {
            val info = context.packageManager.getPackageInfo(
                    context.packageName,
                    PackageManager.GET_SIGNATURES)
            for (signature in info.signatures) {
                val md = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                hashKey = Base64.encodeToString(md.digest(), Base64.DEFAULT)
                Log.d("KeyHash:", hashKey)
            }
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        }
        return hashKey
    }

    /**
     * <h>GetTimeDifference</h>
     *
     *
     * In this method we calculate the difference of two time.
     *
     *
     * @param date it accept the given time from where it is being called.
     * @return String
     */
    fun getTimeDifference(date: String): String {
        var days = 0
        var hours = 0
        var min = 0
        var sec = 0
//        var date1: Date
        val date2: Date
        val simpleDateFormat = SimpleDateFormat(VariableConstants.DATE_FORMAT)
        try {
            //date1 = simpleDateFormat.parse(givenTime);
            date2 = simpleDateFormat.parse(currentDateTime)
            val difference = date2.time - date.toLong()
            println("time difference=" + difference.toInt())
            days = ((difference / (1000 * 60 * 60 * 24)) as Long).toInt()
            hours = (((difference - 1000 * 60 * 60 * 24 * days) / (1000 * 60 * 60))as Long).toInt()
            min = ((difference - 1000 * 60 * 60 * 24 * days - 1000 * 60 * 60 * hours) as Long / (1000 * 60)).toInt()
            sec = ((difference - 1000 * 60 * 60 * 24 * days - 1000 * 60 * 60 * hours - 1000 * 60 * min) as Long / 1000).toInt()
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        println("get time diff=days=$days  hours=$hours min=$min sec=$sec")
        if (days != 0) {
            val day: String
            Log.d("days..", days.toString() + "")
            return if (days > 7 && days < 29) {
                day = (days / 7).toString() + "w"
                day
            } else if (days > 29 && days < 365) {
                day = (days / 29).toString() + "m"
                day
            } else if (days > 364) {
                day = (days / 364).toString() + "y"
                day
            } else {
                days.toString() + "d"
            }
        }
        if (hours != 0) {
            return hours.toString() + "h"
        }
        if (min != 0) {
            return min.toString() + "min"
        }
        return if (sec != 0) {
            sec.toString() + "sec"
        } else "just now"
    }

    /**
     * <h>GetMonth</h>
     *
     *
     * In this method we used to extract the given date format(yyyy-MM-dd HH:mm:ss)
     * into required day,month and year.
     *
     *
     * @param date given date
     * @return String
     */
    fun getDate(date: String): String {
        var month = 0
        var dayOfMonth = 0
        var year = 0
        val all_date: String
        try {
            val c = Calendar.getInstance()
            c.timeInMillis = date.toLong()
            val temp = c.time
            val format = SimpleDateFormat(VariableConstants.DATE_FORMAT)
            val time = format.format(temp) //this variable time contains the time in the format of "day/month/year".
            val d = format.parse(time)
            val cal = Calendar.getInstance()
            cal.time = d
            month = cal[Calendar.MONTH]
            dayOfMonth = cal[Calendar.DAY_OF_MONTH]
            year = cal[Calendar.YEAR]
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        all_date = dayOfMonth.toString() + " " + getMonthName(month) + " " + year
        return all_date
    }

    /**
     * <h>getMonthName</h>
     *
     *
     * In this method we used to convert given month in integer format(eg. 1,2)
     * into String format eg. (Jan, Feb)
     *
     *
     * @param month given month
     * @return String
     */
    private fun getMonthName(month: Int): String {
        when (month + 1) {
            1 -> return "Jan"
            2 -> return "Feb"
            3 -> return "Mar"
            4 -> return "Apr"
            5 -> return "May"
            6 -> return "Jun"
            7 -> return "Jul"
            8 -> return "Aug"
            9 -> return "Sep"
            10 -> return "Oct"
            11 -> return "Nov"
            12 -> return "Dec"
        }
        return ""
    }

    /**
     * <h>GetDayName</h>
     *
     *
     * In this method we used to convert day in integer format(1,2) into
     * String format(Sunday,Monday etc)
     *
     *
     * @param day in integer
     * @return String
     */
    private fun getDayName(day: Int): String {
        when (day) {
            1 -> return "Sun"
            2 -> return "Mon"
            3 -> return "Tue"
            4 -> return "Wed"
            5 -> return "Thr"
            6 -> return "Fri"
            7 -> return "Sat"
        }
        return ""
    }//2014/08/06 15:59:48

    /**
     * <h>GetCurrentTime</h>
     *
     *
     * In This method we get the current date and time using
     * SimpleDateFormat. which accept our own required date
     * format. The Date() This constructor initializes the
     * object with the current date and time. format() method
     * Formats the specified date using the rules of this date
     * format.
     *
     * @return String
     */
    private val currentDateTime: String
        private get() {
            val dateFormat: DateFormat = SimpleDateFormat(VariableConstants.DATE_FORMAT)
            val date = Date()
            return dateFormat.format(date) //2014/08/06 15:59:48
        }

    /**
     * <h>statusBarColor</h>
     *
     *
     * This method is used to change device default status default color to
     * our required status bar color. Changing the color of status bar also
     * requires setting two additional flags on the Window; we need to add
     * the FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag and clear the
     * FLAG_TRANSLUCENT_STATUS flag.
     *
     * @param activity the current context
     */
    @kotlin.jvm.JvmStatic
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    fun statusBarColor(activity: Activity) {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            val window = activity.window

// clear FLAG_TRANSLUCENT_STATUS flag:
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)

// finally change the color
            window.statusBarColor = ContextCompat.getColor(activity, R.color.colorPrimaryDark)
        }
    }

    /**
     * <h>sShowSnackbarMessage</h>
     *
     *
     * In this method we used to show the snackbar which comes at a bottom of
     * the screen. It usually shows when any error comes like if there is not
     * internet connection or any error from the server.
     *
     * @param message The content to show
     */
    @kotlin.jvm.JvmStatic
    fun showSnackbarMessage(rootViw: View?, message: String?) {
        if (rootViw != null && message != null) {
            val snackbar = TSnackbar
                    .make(rootViw, message, TSnackbar.LENGTH_LONG)
            snackbar.setActionTextColor(Color.WHITE)
            val snackbarView = snackbar.view
            snackbarView.setBackgroundColor(Color.parseColor("#ff3a3a"))
            val textView = snackbarView.findViewById<View>(R.id.snackbar_text) as TextView
            textView.gravity=Gravity.CENTER_VERTICAL
//            textView.setBackgroundColor(R.color.green_text_color)
            textView.setTextColor(Color.WHITE)
            snackbar.show()
        }
    }

    fun hideKeyboard(context: Context, mView: View) {
        try {
            val inputManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputManager.hideSoftInputFromWindow(mView.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * <h>sShowSnackbarMessage</h>
     *
     *
     * In this method we used to show the snackbar which comes at a bottom of
     * the screen. It usually shows when any error comes like if there is not
     * internet connection or any error from the server.
     *
     * @param message The content to show
     */
    fun showToast(context: Context?, message: String?) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    /**
     * <h>sShowSnackbarMessage</h>
     *
     *
     * In this method we used to show the snackbar which comes at a bottom of
     * the screen. It usually shows when any error comes like if there is not
     * internet connection or any error from the server.
     *
     * @param message The content to show
     */
    fun showSuccessSnackbarMsg(rootViw: View?, message: String?) {
        if (rootViw != null && message != null) {
            val snackbar = TSnackbar
                    .make(rootViw, message, TSnackbar.LENGTH_LONG)
            snackbar.setActionTextColor(Color.WHITE)
            val snackbarView = snackbar.view
            snackbarView.setBackgroundColor(Color.parseColor("#2ecc71"))
            val textView = snackbarView.findViewById<View>(R.id.snackbar_text) as TextView
            textView.setTextColor(Color.WHITE)
            snackbar.show()
        }
    }

    /**
     * <h>showShortSuccessMsg</h>
     *
     *
     * In this method we used to show snack bar from top for short time of period.
     *
     * @param rootViw The activity or frag root view
     * @param message The content to show
     */
    fun showShortSuccessMsg(rootViw: View?, message: String?) {
        if (rootViw != null && message != null) {
            val snackbar = TSnackbar
                    .make(rootViw, message, TSnackbar.LENGTH_SHORT)
            snackbar.setActionTextColor(Color.WHITE)
            val snackbarView = snackbar.view
            snackbarView.setBackgroundColor(Color.parseColor("#2ecc71"))
            val textView = snackbarView.findViewById<View>(R.id.snackbar_text) as TextView
            textView.setTextColor(Color.WHITE)
            snackbar.show()
        }
    }

    /**
     * <h>SetMargins</h>
     *
     *
     * In this method we used to put margin to the given view from all sides.
     *
     * @param view The given view
     * @param left margin from left
     * @param top margin from top
     * @param right margin from right
     * @param bottom margin from bottom
     */
    fun setMargins(view: View, left: Int, top: Int, right: Int, bottom: Int) {
        if (view.layoutParams is ViewGroup.MarginLayoutParams) {
            val p = view.layoutParams as ViewGroup.MarginLayoutParams
            p.setMargins(left, top, right, bottom)
            view.requestLayout()
        }
    }

    /**
     * code get just two digit after the point in decimal value
     */
    @kotlin.jvm.JvmStatic
    fun twoDigitAfterDewcimal(value: String?): String {
        var getValue = ""
        if (value != null && !value.isEmpty()) {
            val decimalFormat = DecimalFormat("0.00")
            getValue = decimalFormat.format(value.toDouble())
        }
        return getValue
    }

    fun hideStatusBar(mActivity: Activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            val decorView = mActivity.window.decorView
            val uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN
            decorView.systemUiVisibility = uiOptions
        } else mActivity.window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
    }

    /**
     * <h>SetViewOpacity</h>
     *
     *
     * In this method we used to set the transparency to the given view
     * according to the given alpha value.
     *
     * @param mActivity The current context
     * @param view The given view
     * @param alphaValue The interger value ranges from 0 to 255.
     */
    fun setViewOpacity(mActivity: Activity?, view: View, alphaValue: Int, id: Int) {
        val d = ContextCompat.getDrawable(mActivity!!, id)
        d!!.alpha = alphaValue
        view.background = d
    }

    /**
     * <h>GetCurrencyLocaleMap</h>
     *
     *
     * In this method we used to find the country iso code and currency.
     * Store that two in Map as key-value pair.
     *
     * @return The map with currency and code pair
     */
    fun getCurrencyLocaleMap(): Map<Currency, Locale> {
        val map: MutableMap<Currency, Locale> = HashMap()
        for (locale in Locale.getAvailableLocales()) {
            try {
                val currency = Currency.getInstance(locale)
                map[currency] = locale
            } catch (e: Exception) {
                // skip strange locale
            }
        }
        return map
    }

    /**
     * <h>GetCompleteAddressString</h>
     *
     *
     * In this method we used to retrieve complete address from
     * given latitude and longitude.
     *
     * @param activity The current context
     * @param latitude The given latitude
     * @param longitude The given longitude
     * @return it returns string with complete address
     */
    fun getCompleteAddressString(activity: Activity?, latitude: Double, longitude: Double): String {
        var strAdd = ""
        val geocoder = Geocoder(activity, Locale.getDefault())
        try {
            val addresses = geocoder.getFromLocation(latitude, longitude, 1)
            if (addresses != null) {
                val returnedAddress = addresses[0]
                val strReturnedAddress = StringBuilder("")
                if (returnedAddress.maxAddressLineIndex > 0) {
                    for (i in 0 until returnedAddress.maxAddressLineIndex) {
                        strReturnedAddress.append(", ").append(returnedAddress.getAddressLine(i))
                        println("My Current loction address inside for loop" + "" + returnedAddress.getAddressLine(i))
                    }
                } else {
                    strReturnedAddress.append(", ").append(returnedAddress.getAddressLine(0))
                    println("My Current loction address at 0" + "" + returnedAddress.getAddressLine(0))
                }
                strAdd = strReturnedAddress.toString()
                if (strAdd.startsWith(",")) strAdd = strAdd.replaceFirst(",".toRegex(), "")
                println("My Current loction address $strReturnedAddress")
            } else {
                println("My Current loction address" + "No Address returned!")
            }
        } catch (e: Exception) {
            e.printStackTrace()
            println("My Current loction address" + "Canont get Address!")
        }
        return strAdd.trim { it <= ' ' }
    }

    // Can be triggered by a view event such as a button press
    fun onShareItem(activity: Activity, iV_logo: ImageView, title: String?) {
        // Get access to bitmap image from view
        // Get access to the URI for the bitmap
        val bmpUri = getLocalBitmapUri(activity, iV_logo)
        if (bmpUri != null) {
            // Construct a ShareIntent with link to image
            val shareIntent = Intent()
            shareIntent.action = Intent.ACTION_SEND
            shareIntent.putExtra(Intent.EXTRA_TEXT, title)
            shareIntent.putExtra(Intent.EXTRA_STREAM, bmpUri)
            shareIntent.type = "image/*"
            shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            activity.startActivity(Intent.createChooser(shareIntent, "Share on..."))
        }
    }

    fun shareItem(activity: Activity, productName: String?, postId: String?) {
        val sendIntent = Intent()
        sendIntent.action = Intent.ACTION_SEND
        sendIntent.putExtra(Intent.EXTRA_TEXT, "Hey check out my app at: https://yelo-app.xyz/item/1502203409899")
        sendIntent.type = "text/plain"
        activity.startActivity(sendIntent)
    }

    // Returns the URI path to the Bitmap displayed in specified ImageView
    private fun getLocalBitmapUri(activity: Activity, imageView: ImageView): Uri? {
        // Extract Bitmap from ImageView drawable
        val drawable = imageView.drawable
        var bmp: Bitmap? = null
        bmp = if (drawable is BitmapDrawable) {
            (imageView.drawable as BitmapDrawable).bitmap
        } else {
            return null
        }
        // Store image to default external storage directory
        var bmpUri: Uri? = null
        try {
            // Use methods on Context to access package-specific directories on external storage.
            // This way, you don't need to request external read/write permission.
            // See https://youtu.be/5xVh-7ywKpE?t=25m25s
            val file = File(activity.getExternalFilesDir(Environment.DIRECTORY_PICTURES), "share_image_" + System.currentTimeMillis() + ".png")
            val out = FileOutputStream(file)
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out)
            out.close()
            // **Warning:** This will fail for API >= 24, use a FileProvider as shown below instead.
            bmpUri = Uri.fromFile(file)
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return bmpUri
    }

    /**
     * <h>GetGmtTimeDiff</h>
     *
     *
     * In this method we calculate the difference of two time.
     *
     *
     * @param date it accept the given time in gmt
     * @return String
     */
    fun getGmtTimeDiff(date: String?): String {
        var days = 0
        var hours = 0
        var min = 0
        var sec = 0
        val date1: Date
        val date2: Date
        val simpleDateFormat = SimpleDateFormat(VariableConstants.DATE_FORMAT)
        try {
            date1 = simpleDateFormat.parse(date)
            date2 = getGMTDate(VariableConstants.DATE_FORMAT)
            val difference = date2.time - date1.time
            days = (difference / (1000 * 60 * 60 * 24)).toInt()
            hours = ((difference - 1000 * 60 * 60 * 24 * days) / (1000 * 60 * 60)).toInt()
            min = (difference - 1000 * 60 * 60 * 24 * days - 1000 * 60 * 60 * hours).toInt() / (1000 * 60)
            sec = (difference - 1000 * 60 * 60 * 24 * days - 1000 * 60 * 60 * hours - 1000 * 60 * min).toInt() / 1000
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        if (days != 0) {
            val day: String
            Log.d("days..", days.toString() + "")
            return if (days > 7 && days < 29) {
                day = (days / 7).toString() + " " + "Weeks"
                day
            } else if (days > 29 && days < 365) {
                day = (days / 29).toString() + " " + "Months"
                day
            } else if (days > 364) {
                day = (days / 364).toString() + " " + "Years"
                day
            } else {
                "$days Days"
            }
        }
        if (hours != 0) {
            return "$hours Hours"
        }
        if (min != 0) {
            return "$min Minutes"
        }
        return if (sec != 0) {
            "$sec Seconds"
        } else ""
    }

    /**
     * <h>getGMTDate</h>
     *
     *
     * In this method we used to generate time in GMT.
     *
     * @param dateFormat The date format i.e yyyy-MM-dd HH:mm:ss
     * @return the current date
     */
    @Throws(ParseException::class)
    private fun getGMTDate(dateFormat: String?): Date {
        val dateFormatGmt = SimpleDateFormat(dateFormat)
        dateFormatGmt.timeZone = TimeZone.getTimeZone("GMT")

        // Local time zone
        val dateFormatLocal = SimpleDateFormat(dateFormat)

        // Time in GMT
        return dateFormatLocal.parse(dateFormatGmt.format(Date()))
    }

    /**
     * <h>showTopSnackBar</h>
     *
     *
     * In this method we used to show the custom snackbar from Top of the screen.
     *
     * @param view The root element of the given layout
     * @param content The Message to show
     */
    fun showTopSnackBar(view: View?, content: String?) {
        if (view != null && content != null) {
            val snackbar = TSnackbar
                    .make(view, content, TSnackbar.LENGTH_LONG)
            snackbar.setActionTextColor(Color.WHITE)
            val snackbarView = snackbar.view
            snackbarView.setBackgroundColor(Color.parseColor("#ff3a3a"))
            val textView = snackbarView.findViewById<View>(R.id.snackbar_text) as TextView
            textView.setTextColor(Color.WHITE)
            snackbar.show()
        }
    }

    /**
     * <h>GetCityName</h>
     *
     *
     * In this method we used to retrive the city name from the given lat and lng.
     *
     * @param mActivity The current context
     * @param latitude The given latitude
     * @param longitude The given longitude
     * @return The city name
     */
    fun getCityName(mActivity: Activity, latitude: Double, longitude: Double): String {
        var cityName = ""
        try {
            val geo = Geocoder(mActivity.applicationContext, Locale.getDefault())
            val addresses = geo.getFromLocation(latitude, longitude, 1)
            if (addresses.size > 0) {
                cityName = addresses[0].locality
                println("locations=" + addresses[0].featureName + ", " + addresses[0].locality + ", " + addresses[0].adminArea + ", " + addresses[0].countryName)
                if (!cityName.isEmpty()) cityName = cityName.toLowerCase()
            }
        } catch (e: Exception) {
            e.printStackTrace() // getFromLocation() may sometimes fail
        }
        return cityName
    }

    /**
     * <h>GetCountryName</h>
     *
     *
     * In this method we used to retrive the Country name from the given lat and lng.
     *
     * @param mActivity The current context
     * @param latitude The given latitude
     * @param longitude The given longitude
     * @return The country name
     */
    fun getCountryName(mActivity: Activity, latitude: Double, longitude: Double): String {
        var countryName = ""
        try {
            val geo = Geocoder(mActivity.applicationContext, Locale.getDefault())
            val addresses = geo.getFromLocation(latitude, longitude, 1)
            if (addresses.size > 0) {
                countryName = addresses[0].countryName
                println("locations=" + addresses[0].featureName + ", " + addresses[0].locality + ", " + addresses[0].adminArea + ", " + addresses[0].countryName)
                //Toast.makeText(getApplicationContext(), "Address:- " + addresses.get(0).getFeatureName() + addresses.get(0).getAdminArea() + addresses.get(0).getLocality(), Toast.LENGTH_LONG).show();
            }
        } catch (e: Exception) {
            e.printStackTrace() // getFromLocation() may sometimes fail
        }
        return countryName
    }

    /**
     * <h>GetCountryName</h>
     *
     *
     * In this method we used to retrive the Country iso code from the given lat and lng.
     *
     * @param mActivity The current context
     * @param latitude The given latitude
     * @param longitude The given longitude
     * @return The country name
     */
    fun getCountryCode(mActivity: Activity, latitude: Double, longitude: Double): String {
        var countryName = ""
        try {
            val geo = Geocoder(mActivity.applicationContext, Locale.getDefault())
            val addresses = geo.getFromLocation(latitude, longitude, 1)
            if (addresses.size > 0) {
                countryName = addresses[0].countryCode
                println("locations=" + addresses[0].featureName + ", " + addresses[0].locality + ", " + addresses[0].adminArea + ", " + addresses[0].countryName)
                if (!countryName.isEmpty()) countryName = countryName.toLowerCase()
            }
        } catch (e: Exception) {
            e.printStackTrace() // getFromLocation() may sometimes fail
        }
        return countryName
    }

    fun playBackgroungVideo(mActivity: Activity, mVideoView: VideoView) {
        mActivity.window.setFormat(PixelFormat.UNKNOWN)
        //Displays a video file.

        //String uriPath="android.resource://"+mActivity.getPackageName()+"/"+R.raw.landing_screen;
        val uriPath = "android.resource://" + mActivity.packageName + "/" + ""
        val uri = Uri.parse(uriPath)
        mVideoView.setVideoURI(uri)
        mVideoView.requestFocus()
        mVideoView.start()
        mVideoView.setOnPreparedListener { mp -> mp.isLooping = true }
    }

    /**
     * <h>ExtractNumberFromString</h>
     *
     *
     * In this method we used to extract the numeric value from given string
     *
     * @param string The given string value
     * @return The numeric value.
     */
    fun extractNumberFromString(string: String?): String {
        val stringBuilder = StringBuilder(100)
        if (string != null && !string.isEmpty()) {
            for (ch in string.toCharArray()) {
                if (ch >= '0' && ch <= '9') {
                    stringBuilder.append(ch)
                }
            }
        }
        return stringBuilder.toString()
    }

    /**
     * simple function for sending mail
     */
    fun sendEmail(context: Context, emailId: String, userName: String, emailSubject: String) {
        Log.i("Send email", "")
        val TO = arrayOf(emailId)
        val CC = arrayOf("")
        val emailIntent = Intent(Intent.ACTION_SEND)
        emailIntent.data = Uri.parse("mailto:")
        emailIntent.type = "text/plain"
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO)
        emailIntent.putExtra(Intent.EXTRA_CC, CC)
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "$emailSubject $userName")
        emailIntent.putExtra(Intent.EXTRA_TEXT, "")
        try {
            context.startActivity(Intent.createChooser(emailIntent, "Send mail..."))
        } catch (ex: ActivityNotFoundException) {
            ex.printStackTrace()
        }
    }

    /**
     * <h>SessionExpired</h>
     *
     *
     * This method is called after api response when response error code will become 401.
     * In this method we used to move the current screen to the Landing Activity screen.
     *
     * @param mActivity The current activity context.
     */
    @kotlin.jvm.JvmStatic
    fun sessionExpired(mActivity: Activity) {
        val sessionManager: SessionManager
        sessionManager = SessionManager(mActivity)
        AppController.instance?.disconnect()
        if (sessionManager.isUserLoggedIn) {
            sessionManager.isUserLoggedIn = false
            sessionManager.chatSync = false
            //clear chat data by docId
            AppController.instance?.dbController!!.clearIndexDocument(AppController.instance?.indexDocId)
            // move to the landing screen
            val intent = Intent(mActivity, HomePageActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            mActivity.startActivity(intent)
        }
    }

    var currencyLocaleMap: SortedMap<Currency, Locale>? = null
    fun getCurrencySymbol(currencyCode: String): String {
        val currency = Currency.getInstance(currencyCode)
        return if (currency != null) currency.getSymbol(currencyLocaleMap!![currency]) else currencyCode
    }

    fun getFirstCaps(name: String?): String {
        var name = name ?: return ""
        if (name.isEmpty()) return ""
        if (name.length <= 1) return name.toUpperCase()
        name = name.substring(0, 1).toUpperCase() + "" + name.substring(1)
        return name
    }

    fun makeAddressFromCityCountry(city: String?, country: String?): String {
        var address = ""
        address = if (country == null || country.isEmpty()) getFirstCaps(city) else if (city == null || city.isEmpty()) getFirstCaps(country) else getFirstCaps(city) + ", " + country
        return address
    }

    fun dpToPx(mActivity: Activity, dp: Int): Int {
        val scale = mActivity.resources.displayMetrics.density
        val padding_in_px = (dp * scale + 0.5f).toInt()
        println("dp to pixel: $dp to $padding_in_px")
        return padding_in_px
    }

    fun isLocationServiceEnabled(mActivity: Activity): Boolean {
        var locationManager: LocationManager? = null
        var gps_enabled = false
        var network_enabled = false
        if (locationManager == null) locationManager = mActivity.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        try {
            gps_enabled = locationManager!!.isProviderEnabled(LocationManager.GPS_PROVIDER)
        } catch (ex: Exception) {
            //do nothing...
        }
        try {
            network_enabled = locationManager!!.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
        } catch (ex: Exception) {
            //do nothing...
        }
        return gps_enabled || network_enabled
    }

    init {
        currencyLocaleMap = TreeMap(Comparator { c1, c2 -> c1.currencyCode.compareTo(c2.currencyCode) })
        for (locale in Locale.getAvailableLocales()) {
            try {
                val currency: Currency = Currency.getInstance(locale)
                (currencyLocaleMap as TreeMap<Currency, Locale>)[currency!!] = locale!!
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }
}