package com.zoptal.cellableapp.utility


import android.util.Log
import android.view.View
import okhttp3.*
import java.io.ByteArrayOutputStream
import java.io.IOException

/**
 * Created by embed on 25/7/18.
 */
class CheckConnectionStatus(view: View?) {
    var startTime: Long
    var endTime: Long = 0
    var fileSize: Long = 0
    var client = OkHttpClient()

    // bandwidth in kbps
    private val POOR_BANDWIDTH = 150
    private val AVERAGE_BANDWIDTH = 550
    private val GOOD_BANDWIDTH = 2000

    companion object {
        private const val TAG = "CheckConnection"
    }

    init {
        val request = Request.Builder()
                .url("http://www.gotravelaz.com/wp-content/uploads/images/Lake_Forest.jpg")
                .build()
        startTime = System.currentTimeMillis()
        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                e.printStackTrace()
            }

            @Throws(IOException::class)
            override fun onResponse(call: Call, response: Response) {
                if (!response.isSuccessful) throw IOException("Unexpected code $response")
                val responseHeaders = response.headers()
                var i = 0
                val size = responseHeaders.size()
                while (i < size) {
                    Log.d(TAG, responseHeaders.name(i) + ": " + responseHeaders.value(i))
                    i++
                }
                val input = response.body()!!.byteStream()
                fileSize = try {
                    val bos = ByteArrayOutputStream()
                    val buffer = ByteArray(1024)
                    while (input.read(buffer) != -1) {
                        bos.write(buffer)
                    }
                    val docBuffer = bos.toByteArray()
                    bos.size().toLong()
                } finally {
                    input.close()
                }
                endTime = System.currentTimeMillis()


                // calculate how long it took by subtracting endtime from starttime
                val timeTakenMills = Math.floor(endTime - startTime.toDouble()) // time taken in milliseconds
                val timeTakenSecs = timeTakenMills / 1000 // divide by 1000 to get time in seconds
                val kilobytePerSec = Math.round(1024 / timeTakenSecs).toInt()
                if (kilobytePerSec <= POOR_BANDWIDTH) {
                    CommonClass.showSnackbarMessage(view, "Your internet connection seems to slow")
                    // slow connection
                }

                // get the download speed by dividing the file size by time taken to download
                val speed = fileSize / timeTakenMills
                Log.d(TAG, "Time taken in secs: $timeTakenSecs")
                Log.d(TAG, "kilobyte per sec: $kilobytePerSec")
                Log.d(TAG, "Download Speed: $speed")
                Log.d(TAG, "File size: $fileSize")
            }
        })
    }
}