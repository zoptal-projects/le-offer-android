package com.zoptal.cellableapp.main.activity.products

import android.app.Activity
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.util.Log
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.viewpager.widget.ViewPager

import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.adapter.ProductImagePagerAdapter
import com.zoptal.cellableapp.fcm_push_notification.Config
import com.zoptal.cellableapp.fcm_push_notification.NotificationMessageDialog
import com.zoptal.cellableapp.fcm_push_notification.NotificationUtils.Companion.clearNotifications
import com.zoptal.cellableapp.main.activity.products.ProductImagesActivity
import com.zoptal.cellableapp.swipe_to_finish.SwipeBackActivity
import com.zoptal.cellableapp.swipe_to_finish.SwipeBackLayout
import java.util.*

/**
 * <h>ProductImagesActivity</h>
 *
 *
 * This class is called from ProductDetailsActivity class. In this class
 * we used to show the multiple images of the product using viewpager.
 *
 * @since 02-Jun-17
 * @version 1.0
 * @author 3Embed
 */
class ProductImagesActivity : SwipeBackActivity() {
    private var tV_current_page: TextView? = null
    private var mNotificationMessageDialog: NotificationMessageDialog? = null
    override fun onCreate(savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_multiple_images)
        setDragEdge(SwipeBackLayout.DragEdge.TOP)
        val intent: Intent = getIntent()
        val aL_multipleImages = intent.getSerializableExtra("imagesArrayList") as ArrayList<String>
        val images_array = aL_multipleImages.toTypedArray()
        val mActivity: Activity = this@ProductImagesActivity
        mNotificationMessageDialog = NotificationMessageDialog(mActivity)
        val view_pager: ViewPager = findViewById(R.id.view_pager) as ViewPager
        tV_current_page = findViewById(R.id.tV_current_page) as TextView?
        val tV_total_pages = findViewById(R.id.tV_total_pages) as TextView
        tV_total_pages.text = images_array.size.toString()
        val productImagePagerAdapter = ProductImagePagerAdapter(mActivity, images_array)
        view_pager.setAdapter(productImagePagerAdapter)
        view_pager.setCurrentItem(0)
        println(TAG + " " + "current page=" + view_pager.getCurrentItem())
        val setCurrentPage: String = java.lang.String.valueOf(view_pager.getCurrentItem() + 1)
        tV_current_page!!.text = setCurrentPage
        view_pager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
           override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
           override fun onPageSelected(position: Int) {
                Log.i(TAG, "page selected $position")
                val setCurrentPage = (position + 1).toString()
                tV_current_page!!.text = setCurrentPage
            }

           override fun onPageScrollStateChanged(state: Int) {}
        })

        // close
        val rL_close = findViewById(R.id.rL_close) as RelativeLayout
        rL_close.setOnClickListener { finish() }
    }

    override fun onResume() {
        super.onResume()

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.REGISTRATION_COMPLETE))

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.PUSH_NOTIFICATION))

        // clear the notification area when the app is opened
        clearNotifications(getApplicationContext())
    }

   override fun onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!)
        super.onPause()
    }

    companion object {
        private val TAG = ProductImagesActivity::class.java.simpleName
    }
}