package com.zoptal.cellableapp.main.tab_fragments

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.*
import android.view.animation.AccelerateInterpolator
import android.view.animation.DecelerateInterpolator
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import androidx.fragment.app.Fragment
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout.OnRefreshListener
import com.facebook.ads.Ad
import com.facebook.ads.AdError
import com.facebook.ads.NativeAd
import com.facebook.ads.NativeAdListener
import com.google.gson.Gson
import com.squareup.otto.Subscribe
import com.squareup.picasso.Picasso
import com.zoptal.cellableapp.BuildConfig
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.adapter.ExploreRvAdapter
import com.zoptal.cellableapp.adapter.HomeFilterAdapter
import com.zoptal.cellableapp.event_bus.BusProvider.instance
import com.zoptal.cellableapp.fcm_push_notification.Config
import com.zoptal.cellableapp.get_current_location.FusedLocationReceiver
import com.zoptal.cellableapp.get_current_location.FusedLocationService
import com.zoptal.cellableapp.main.activity.*
import com.zoptal.cellableapp.main.activity.products.ProductDetailsActivity
import com.zoptal.cellableapp.main.camera_kit.CameraKitActivity
import com.zoptal.cellableapp.main.tab_fragments.HomeFrag
import com.zoptal.cellableapp.pojo_class.LogDevicePojo
import com.zoptal.cellableapp.pojo_class.UnseenNotifiactionCountPojo
import com.zoptal.cellableapp.pojo_class.filter.SendFilter
import com.zoptal.cellableapp.pojo_class.home_explore_pojo.ExploreLikedByUsersDatas
import com.zoptal.cellableapp.pojo_class.home_explore_pojo.ExplorePojoMain
import com.zoptal.cellableapp.pojo_class.home_explore_pojo.ExploreResponseDatas
import com.zoptal.cellableapp.pojo_class.product_category.FilterKeyValue
import com.zoptal.cellableapp.pojo_class.product_category.ProductCategoryMainPojo
import com.zoptal.cellableapp.pojo_class.product_category.ProductCategoryResDatas
import com.zoptal.cellableapp.utility.*
import com.zoptal.cellableapp.utility.OkHttp3Connection.OkHttp3RequestCallback
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.util.*
import kotlin.collections.ArrayList

/**
 * <h>HomeFrag</h>
 *
 *
 * This class is called from first tab of MainActivity class.
 * In this class we used to show the users all posts.
 *
 *
 * @author 3Embed
 * @version 1.0
 * @since 3/31/2017
 */
class HomeFrag : Fragment(), View.OnClickListener, ProductItemClickListener {
    private var category: String? = ""
    private var categoryValue = ""
    private var distance: String? = ""
    private var postedWithin = ""
    private var minPrice: String? = ""
    private var maxPrice: String? = ""
    private var currency = ""
    private var currency_code = ""
    private var currentLatitude: String? = ""
    private var currentLongitude: String? = ""
    private var sortByText: String? = ""
    private var sortBy = ""
    private var postedWithinText: String? = ""
    private var address: String? = ""
    private var mSessionManager: SessionManager? = null
    private var mActivity: Activity? = null
    private var mProgressBar: ProgressBar? = null
    private var arrayListExploreDatas: ArrayList<ExploreResponseDatas>? = null
    private var index = 0
    private var mRefreshLayout: SwipeRefreshLayout? = null
    private var isFromSearch = false
    private var isHomeFragVisible = false
    private var linear_filter: LinearLayout? = null
    private var arrayListFilter: ArrayList<String>? = null
    private var view_filter_divider: View? = null
    private var rL_noProductFound: RelativeLayout? = null
    private var rL_no_internet: RelativeLayout? = null
    private var rL_action_bar: RelativeLayout? = null
    private var rL_sell_stuff: RelativeLayout? = null
    private var rl_LocationOff: RelativeLayout? = null
    private var rl_TurnOnLocation: RelativeLayout? = null
    private var rL_rootview: RelativeLayout? = null
    private var mRecyclerView: RecyclerView? = null
    private var gridLayoutManager: StaggeredGridLayoutManager? = null
//    private val linearLayoutManager: LinearLayoutManager? = null
    private var aL_categoryDatas: ArrayList<ProductCategoryResDatas>? = null
    //arrayOf(resources.getString(R.string.the_last_24hr), resources.getString(R.string.the_last_7day), resources.getString(R.string.the_last_30day), resources.getString(R.string.all_producs))
    //arrayOf(resources.getString(R.string.newest_first), resources.getString(R.string.closest_first), resources.getString(R.string.price_high_to_low), resources.getString(R.string.price_low_to_high))
    private var clickedItemPosition = 0
    private var exploreRvAdapter: ExploreRvAdapter? = null
    private var locationService: FusedLocationService? = null
    private var lat: String? = ""
    private var lng: String? = ""
    private var  permissionsArray = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
    private var runTimePermission: RunTimePermission? = null
    private var tV_notification_count: TextView? = null
    private var mBroadcastReceiver: BroadcastReceiver? = null
    private var notificationCount = 0
    private var fineLocResult = 0
    private var coarseLocResult = 0
    private var isFineLocDenied = false
    private var isCoarseLocDenied = false
    private var homePageActivity: HomePageActivity? = null

    // Sell your stuff
    private var rL_notification: RelativeLayout? = null
    private var rL_filter: RelativeLayout? = null
    private var rL_search: RelativeLayout? = null

    // load more variables
    private var isLoading = false
    private val visibleThreshold = 5
    private var lastVisibleItem = 0
    private var totalItemCount = 0
    private var scrollViewFilter: HorizontalScrollView? = null
    private var recycleviewPaddingTop = 0
    private val homeFiterheight = 0
    private var filterRv: RecyclerView? = null
    private val category_state = 0
    private var homeFilterAdapter: HomeFilterAdapter? = null
    private var categoryName: String? = ""
    private var subCategoryName: String? = ""
    private val filterKeyValues: ArrayList<FilterKeyValue>? = null
    private var sendFilters: ArrayList<SendFilter>? = ArrayList()
    private var result: String? = ""
    private var filterCountry: String? = ""
    private var filterCity: String? = ""
    private var locationChange = ""
    private var cityName = ""
    override fun onCreate(savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        mActivity = activity
        homePageActivity = mActivity as HomePageActivity?
        result = homePageActivity!!.postResultData
        recycleviewPaddingTop = CommonClass.dpToPx(mActivity!!, 65)
        setNotificationBroadCast()
        index = 0
        notificationCount = 0
        isFromSearch = false // load more variables
        mSessionManager = SessionManager(mActivity!!)
        arrayListExploreDatas = ArrayList()
        aL_categoryDatas = ArrayList()



        runTimePermission = RunTimePermission(mActivity!!, permissionsArray, false)
        fineLocResult = ContextCompat.checkSelfPermission(mActivity!!, Manifest.permission.ACCESS_FINE_LOCATION)
        coarseLocResult = ContextCompat.checkSelfPermission(mActivity!!, Manifest.permission.ACCESS_COARSE_LOCATION)
        categoriesService
        if (!mSessionManager!!.isUserLoggedIn) logGuestInfo()

        // to see notification count
        unseenNotificationCountApi()
    }

    private fun setNotificationBroadCast() {
        mBroadcastReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                if (intent.action == Config.PUSH_NOTIFICATION) {
                    // new push notification is received
                    val message = intent.getStringExtra("message")
                    val jsonMessageResponse = intent.getStringExtra("jsonMessage")
                    println("$TAG message=$message jsonMessageResponse=$jsonMessageResponse notificationCount=$notificationCount")
                    unseenNotificationCountApi()
                }
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        println("$TAG RecycleView Padding $recycleviewPaddingTop")
        instance.register(this)
        LocalBroadcastManager.getInstance(mActivity!!).registerReceiver(mBroadcastReceiver!!, IntentFilter(Config.PUSH_NOTIFICATION))
    }

    override fun onDestroyView() {
        super.onDestroyView()
        instance.unregister(this)
        LocalBroadcastManager.getInstance(mActivity!!).unregisterReceiver(mBroadcastReceiver!!)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.frag_home, container, false)
        mProgressBar = view.findViewById<View>(R.id.progress_bar_home) as ProgressBar
        view_filter_divider = view.findViewById(R.id.view_filter_divider)
        view_filter_divider!!.setVisibility(View.GONE)
        rL_no_internet = view.findViewById<View>(R.id.rL_no_internet) as RelativeLayout
        rL_no_internet!!.visibility = View.GONE
        rL_action_bar = view.findViewById<View>(R.id.rL_action_bar) as RelativeLayout
        scrollViewFilter = view.findViewById<View>(R.id.scrollViewFilter) as HorizontalScrollView
        tV_notification_count = view.findViewById<View>(R.id.tV_notification_count) as TextView
        tV_notification_count!!.visibility = View.GONE
        mRecyclerView = view.findViewById<View>(R.id.rV_home) as RecyclerView
        filterRv = view.findViewById<View>(R.id.filterRv) as RecyclerView
        gridLayoutManager = StaggeredGridLayoutManager(2, 1)
        gridLayoutManager!!.gapStrategy = StaggeredGridLayoutManager.GAP_HANDLING_MOVE_ITEMS_BETWEEN_SPANS
        mRecyclerView!!.layoutManager = gridLayoutManager
        exploreRvAdapter = ExploreRvAdapter(activity!!, arrayListExploreDatas!!, this)
        mRecyclerView!!.adapter = exploreRvAdapter
        homeFilterAdapter = HomeFilterAdapter(this, mActivity!!, aL_categoryDatas!!)
        val mlayoutManager: RecyclerView.LayoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        filterRv!!.layoutManager = mlayoutManager
        filterRv!!.adapter = homeFilterAdapter
        linear_filter = view.findViewById<View>(R.id.linear_filter) as LinearLayout
        rL_noProductFound = view.findViewById<View>(R.id.rL_noProductFound) as RelativeLayout
        rL_noProductFound!!.visibility = View.GONE
        rl_LocationOff = view.findViewById<View>(R.id.rL_locationOff) as RelativeLayout
        rl_LocationOff!!.visibility = View.GONE
        rl_TurnOnLocation = view.findViewById<View>(R.id.rl_turnOnLocation) as RelativeLayout
        rl_TurnOnLocation!!.setOnClickListener(this)
        rL_rootview = view.findViewById<View>(R.id.rL_rootview) as RelativeLayout


        // Set spacing the recycler view items
        val spanCount = 2 // 2 columns
        val spacing = CommonClass.dpToPx(mActivity!!, 12) // convert 12dp to pixel
        mRecyclerView!!.addItemDecoration(SpacesItemDecoration(spanCount, spacing))

        // Pull to refresh
        mRefreshLayout = view.findViewById<View>(R.id.swipe_refresh_layout) as SwipeRefreshLayout
        mRefreshLayout!!.setColorSchemeResources(R.color.purple_color, R.color.green_text_color)
        mRefreshLayout!!.setProgressViewOffset(false, CommonClass.dpToPx(mActivity!!, 50), CommonClass.dpToPx(mActivity!!, 150))
        mRefreshLayout!!.setOnRefreshListener(OnRefreshListener {
            if (CommonClass.isNetworkAvailable(mActivity!!) && CommonClass.isLocationServiceEnabled(mActivity!!)) {
                rL_no_internet!!.visibility = View.GONE
                arrayListExploreDatas!!.clear()
                exploreRvAdapter!!.notifyDataSetChanged()
                index = 0
                if (aL_categoryDatas!!.size == 0) categoriesService
                for (productCategoryResDatas in aL_categoryDatas!!) {
                    if (productCategoryResDatas.isSelected) {
                        searchProductsApi(index)
                        return@OnRefreshListener
                    }
                }
                if (isFromSearch) {
                    searchProductsApi(index)
                    Log.d("exe", "isFromSearch$isFromSearch")
                } else {
                    // check is user is logged in or not if its login then show according to location if not then show all posts.
                    if (mSessionManager!!.isUserLoggedIn) {
                        isClickableRv = false
                        println("$TAG lat in refreshing=$lat lng=$lng")
                        if (isLocationFound(lat, lng)) getUserPosts(index) else currentLocation
                    } else if (isLocationFound(lat, lng)) {
                        isClickableRv = false
                        //getGuestPosts(index);
                        if (isLocationFound(lat, lng)) getGuestPosts(index) else currentLocation
                    } else currentLocation
                }
            } else {
                if (!CommonClass.isLocationServiceEnabled(mActivity!!)) {
                    CommonClass.showSnackbarMessage((mActivity as HomePageActivity?)!!.rL_rootElement, getString(R.string.please_enable_location))
                    mRefreshLayout!!.isRefreshing = false
                } else if (!CommonClass.isNetworkAvailable(mActivity!!)) {
                    rL_no_internet!!.visibility = View.VISIBLE
                    mRefreshLayout!!.isRefreshing = false
                }
            }
        })
        rL_sell_stuff = view.findViewById<View>(R.id.rL_sell_stuff) as RelativeLayout
        rL_sell_stuff!!.setOnClickListener(this)
        rL_notification = view.findViewById<View>(R.id.rL_notification) as RelativeLayout
        rL_notification!!.setOnClickListener(this)
        rL_filter = view.findViewById<View>(R.id.rL_filter) as RelativeLayout
        rL_filter!!.setOnClickListener(this)
        rL_search = view.findViewById<View>(R.id.rL_search) as RelativeLayout
        rL_search!!.setOnClickListener(this)

        //new CheckConnectionStatus(((HomePageActivity) mActivity).rL_rootElement);
        println("$TAG frag visible home=$isHomeFragVisible")
        if (CommonClass.isLocationServiceEnabled(mActivity!!)) if (result != null && !result!!.isEmpty()) {
            lat = mSessionManager!!.currentLat
            lng = mSessionManager!!.currentLng
            mRecyclerView!!.setPadding(0, recycleviewPaddingTop + recycleviewPaddingTop, 0, 0)
            responseHandler(result!!)
            if (arrayListExploreDatas!!.size == 0) postApisCalling()
        } else {
            postApisCalling()
        } else disableWhileLocationOff()
        return view
    }

    private fun postApisCalling() {
        // Call all posted api
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            rL_no_internet!!.visibility = View.GONE
            index = 0
            arrayListExploreDatas!!.clear()
            exploreRvAdapter!!.notifyDataSetChanged()
            mProgressBar!!.visibility = View.VISIBLE

            // check is user logged in or not if its logged in then show according to location if not then show all posts.
            if (mSessionManager!!.isUserLoggedIn) {
                if (runTimePermission!!.checkPermissions(permissionsArray)) {
                    currentLocation
                } else {
                    requestPermissions(permissionsArray, VariableConstants.PERMISSION_REQUEST_CODE)
                }
            } else {
                if (runTimePermission!!.checkPermissions(permissionsArray)) {
                    currentLocation
                } else {
                    requestPermissions(permissionsArray, VariableConstants.PERMISSION_REQUEST_CODE)
                }
            }
        } else rL_no_internet!!.visibility = View.VISIBLE
    }

    fun disableWhileLocationOff() {
        rL_filter!!.isEnabled = false
        rL_sell_stuff!!.visibility = View.GONE
        rl_TurnOnLocation!!.visibility = View.VISIBLE
        rL_notification!!.isEnabled = false
        rL_search!!.isEnabled = false
        rL_rootview!!.setBackgroundResource(R.color.white)
        rl_LocationOff!!.visibility = View.VISIBLE
        filterRv!!.visibility = View.GONE
    }

    fun enableWhileLocationOn() {
        rL_rootview!!.setBackgroundResource(R.drawable.home_page_bg)
        rL_filter!!.isEnabled = true
        rL_sell_stuff!!.visibility = View.VISIBLE
        rl_TurnOnLocation!!.visibility = View.GONE
        rL_notification!!.isEnabled = true
        rL_search!!.isEnabled = true
        rl_LocationOff!!.visibility = View.GONE
    }

    override fun onResume() {
        super.onResume()
        if (CommonClass.isLocationServiceEnabled(mActivity!!)) {
            //postApisCalling();
            enableWhileLocationOn()
        } else {
            arrayListExploreDatas!!.clear()
            exploreRvAdapter!!.notifyDataSetChanged()
            disableWhileLocationOff()
        }
    }

    /**
     * <h>UnseenNotificationCountApi</h>
     *
     *
     * In this method we used to do api call to get total unseen notification count.
     *
     */
    private fun unseenNotificationCountApi() {
        if (CommonClass.isNetworkAvailable(mActivity!!) && mSessionManager!!.isUserLoggedIn) {
            val request_datas = JSONObject()
            try {
                request_datas.put("token", mSessionManager!!.authToken)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            val unseenNotificationCountUrl = ApiUrl.UNSEEN_NOTIFICATION_COUNT + "?token=" + mSessionManager!!.authToken
            OkHttp3Connection.doOkHttp3Connection(TAG, unseenNotificationCountUrl, OkHttp3Connection.Request_type.GET, JSONObject(), object : OkHttp3RequestCallback {
                 override fun onSuccess(result: String?, user_tag: String?) {
                    println("$TAG unseen notification count res=$result")
                    val unseenNotifiactionCountPojo: UnseenNotifiactionCountPojo
                    val gson = Gson()
                    unseenNotifiactionCountPojo = gson.fromJson(result, UnseenNotifiactionCountPojo::class.java)
                    when (unseenNotifiactionCountPojo.code) {
                        "200" -> {
                            println(TAG + " " + "Notification count=" + unseenNotifiactionCountPojo.data)
                            notificationCount = unseenNotifiactionCountPojo.data
                            if (notificationCount > 0) {
                                tV_notification_count!!.visibility = View.VISIBLE
                                tV_notification_count!!.text = notificationCount.toString()
                            } else tV_notification_count!!.visibility = View.GONE
                        }
                    }
                }

                 override fun onError(error: String?, user_tag: String?) {}
            })
        }
    }

    /**
     * <h>GetCategoriesService</h>
     *
     *
     * This method is called from onCreate() method of the current class.
     * In this method we used to call the getCategories api using okHttp3.
     * Once we get the data we show that list in recyclerview.
     *
     */
    private val categoriesService: Unit
        private get() {
            if (CommonClass.isNetworkAvailable(mActivity!!)) {
                val request_datas = JSONObject()
                val url = ApiUrl.GET_CATEGORIES + "?limit=50"
                OkHttp3Connection.doOkHttp3Connection(TAG, url, OkHttp3Connection.Request_type.GET, request_datas, object : OkHttp3RequestCallback {
                     override fun onSuccess(result: String?, user_tag: String?) {
                        try {
                            println("$TAG get category res=$result")
                            val categoryMainPojo: ProductCategoryMainPojo
                            val gson = Gson()
                            categoryMainPojo = gson.fromJson(result, ProductCategoryMainPojo::class.java)
                            when (categoryMainPojo.code) {
                                "200" -> {
                                    filterRv!!.visibility = View.VISIBLE
                                    if (categoryMainPojo.data != null && categoryMainPojo.data!!.size > 0) {
                                        aL_categoryDatas!!.addAll(categoryMainPojo.data!!)
                                        println(TAG + " " + "aL_categoryDatas=" + aL_categoryDatas!!.size)
                                        homeFilterAdapter!!.notifyDataSetChanged()
                                        mRecyclerView!!.setPadding(0, recycleviewPaddingTop + recycleviewPaddingTop, 0, 0)
                                    }
                                }
                                "401" -> {
                                    filterRv!!.visibility = View.VISIBLE
                                    CommonClass.sessionExpired(mActivity!!)
                                }
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                     override fun onError(error: String?, user_tag: String?) {}
                })
            }
        }

    /**
     * In this method we find current location using FusedLocationApi.
     * in this we have onUpdateLocation() method in which we check if
     * its not null then We call guest user api.
     */
    private val currentLocation: Unit
        private get() {
            locationService = FusedLocationService(mActivity!!, object : FusedLocationReceiver {
                override fun onUpdateLocation() {
                    val currentLocation = locationService!!.receiveLocation()
                    if (currentLocation != null) {
                        lat = currentLocation.latitude.toString()
                        lng = currentLocation.longitude.toString()
                        println("$TAG lat=$lat lng=$lng")
                        if (isLocationFound(lat, lng)) {
                            mSessionManager!!.currentLat = lat
                            mSessionManager!!.currentLng = lng
                            if (arrayListExploreDatas!!.size == 0) {
                                index = 0
                                if (mSessionManager!!.isUserLoggedIn) getUserPosts(index) else getGuestPosts(index)
                            }
                        }
                    }
                }
            }
            )
        }

    private val userCurrentLocationForHomeFilter: Unit
        private get() {
            locationService = FusedLocationService(mActivity!!, object : FusedLocationReceiver {
                override fun onUpdateLocation() {
                    val currentLocation = locationService!!.receiveLocation()
                    if (currentLocation != null) {
                        lat = currentLocation.latitude.toString()
                        lng = currentLocation.longitude.toString()
                        if (isLocationFound(lat, lng)) {
                            mSessionManager!!.currentLat = lat
                            mSessionManager!!.currentLng = lng
                            currentLatitude = lat
                            currentLongitude = lng
                            address = CommonClass.getCompleteAddressString(mActivity, currentLatitude!!.toDouble(), currentLongitude!!.toDouble())
                            val city = CommonClass.getCityName(mActivity!!, currentLatitude!!.toDouble(), currentLongitude!!.toDouble())
                            val country = CommonClass.getCityName(mActivity!!, currentLatitude!!.toDouble(), currentLongitude!!.toDouble())
                            if (address == null || address!!.isEmpty()) address = CommonClass.makeAddressFromCityCountry(city, country)
                            filterData
                        }
                    }
                }
            })
        }

    /**
     * <h>LogDeviceInfo</h>
     *
     *
     * In this method we used to do api call to send device information like device name
     * model number, device id etc to server to log the the user with specific device.
     *
     */
    private fun logGuestInfo() {
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            if (rL_no_internet != null) rL_no_internet!!.visibility = View.GONE
            //deviceName, deviceId, deviceOs, modelNumber, appVersion
            val request_datas = JSONObject()
            try {
                request_datas.put("deviceName", Build.BRAND)
                request_datas.put("deviceId", mSessionManager!!.deviceId)
                request_datas.put("deviceOs", Build.VERSION.RELEASE)
                request_datas.put("modelNumber", Build.MODEL)
                request_datas.put("appVersion", BuildConfig.VERSION_NAME)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            OkHttp3Connection.doOkHttp3Connection(TAG, ApiUrl.LOG_GUEST, OkHttp3Connection.Request_type.POST, request_datas, object : OkHttp3RequestCallback {
                 override fun onSuccess(result: String?, user_tag: String?) {
                    println("$TAG log guest res=$result")
                    val logDevicePojo: LogDevicePojo
                    val gson = Gson()
                    logDevicePojo = gson.fromJson(result, LogDevicePojo::class.java)
                    when (logDevicePojo.code) {
                        "200" -> {
                        }
                        "401" -> CommonClass.sessionExpired(mActivity!!)
                        else -> CommonClass.showSnackbarMessage((mActivity as HomePageActivity?)!!.rL_rootElement, logDevicePojo.message)
                    }
                }

                 override fun onError(error: String?, user_tag: String?) {
                    CommonClass.showSnackbarMessage((mActivity as HomePageActivity?)!!.rL_rootElement, error)
                }
            })
        } else {
            if (rL_no_internet != null) rL_no_internet!!.visibility = View.VISIBLE
        }
    }

    /**
     * <h>GetGuestPosts</h>
     *
     *
     * In this method we used to call guest user api to get all posts.
     *
     *
     * @param offset The page index
     */
    private fun getGuestPosts(offset: Int) {
        var offset = offset
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            isClickableRv = false
            rL_no_internet!!.visibility = View.GONE
            val requestDatas = JSONObject()
            val limit = 20
            offset = limit * offset
            try {
                requestDatas.put("offset", offset)
                requestDatas.put("limit", limit)
                requestDatas.put("latitude", lat)
                requestDatas.put("longitude", lng)
                requestDatas.put("pushToken", mSessionManager!!.pushToken)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            println("$TAG offset in guest post api=$offset lat=$lat lng=$lng")
            OkHttp3Connection.doOkHttp3Connection(TAG, ApiUrl.GET_GUEST_ALL_POSTS, OkHttp3Connection.Request_type.POST, requestDatas, object : OkHttp3RequestCallback {
                 override fun onSuccess(result: String?, user_tag: String?) {
                    isClickableRv = true
                    mProgressBar!!.visibility = View.GONE
                    println("$TAG get explore guest res=$result")
                    if (result != null && !result.isEmpty()) responseHandler(result)
                }

                 override fun onError(error: String?, user_tag: String?) {
                    isClickableRv = true
                    mProgressBar!!.visibility = View.GONE
                    mRefreshLayout!!.isRefreshing = false
                    CommonClass.showSnackbarMessage((mActivity as HomePageActivity?)!!.rL_rootElement, error)
                }
            })
        } else {
            isClickableRv = true
            mRefreshLayout!!.isRefreshing = false
            rL_no_internet!!.visibility = View.VISIBLE
        }
    }

    /**
     * In this method we used to check whether current lat and
     * long has been received or not.
     *
     * @param lat The current latitude
     * @param lng The current longitude
     * @return boolean flag true or false
     */
    private fun isLocationFound(lat: String?, lng: String?): Boolean {
        return !(lat == null || lat.isEmpty()) && !(lng == null || lng.isEmpty())
    }

    /**
     * <h>GetUserPosts</h>
     *
     *
     * In this method we used to do call getUserPosts api. And get all posts
     * in response. Once we get all post then show that in recyclerview.
     *
     *
     * @param offset The pagination
     */
    private fun getUserPosts(offset: Int) {
        var offset = offset
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            isClickableRv = false
            rL_no_internet!!.visibility = View.GONE
            val requestDatas = JSONObject()
            val limit = 20
            offset = limit * offset
            try {
                requestDatas.put("offset", offset)
                requestDatas.put("limit", limit)
                requestDatas.put("token", mSessionManager!!.authToken)
                requestDatas.put("latitude", lat)
                requestDatas.put("longitude", lng)
                requestDatas.put("pushToken", mSessionManager!!.pushToken)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            println("$TAG offset in user post api=$offset lat=$lat lng=$lng")
            OkHttp3Connection.doOkHttp3Connection(TAG, ApiUrl.GET_USER_ALL_POSTS, OkHttp3Connection.Request_type.POST, requestDatas, object : OkHttp3RequestCallback {
                 override fun onSuccess(result: String?, user_tag: String?) {
                    isClickableRv = true
                    mProgressBar!!.visibility = View.GONE
                    println("$TAG get explore res=$result")
                    /*if(arrayListExploreDatas!=null)
                        arrayListExploreDatas.clear();*/if (result != null && !result.isEmpty()) responseHandler(result)

                    //new DialogBox(mActivity).localCampaignDialog("shobhit","","","http://dev.yelo-app.xyz/public/defaultImg.png","title","message","");
                }

                 override fun onError(error: String?, user_tag: String?) {
                    isClickableRv = true
                    mProgressBar!!.visibility = View.GONE
                    mRefreshLayout!!.isRefreshing = false
                    CommonClass.showSnackbarMessage((mActivity as HomePageActivity?)!!.rL_rootElement, error)
                }
            })
        } else {
            isClickableRv = true
            mRefreshLayout!!.isRefreshing = false
            rL_no_internet!!.visibility = View.VISIBLE
        }
    }

    /**
     * <h>ResponseHandler</h>
     *
     *
     * This method is called from onSuccess of getUserPosts(). In this method
     * we used to handle the above api response like if we get the error code
     * 200 then we do futher process or else show error message.
     *
     *
     * @param result The response of the allPosts api
     */
    private fun responseHandler(result: String) {
        val explorePojoMain: ExplorePojoMain
        val gson = Gson()
        explorePojoMain = gson.fromJson(result, ExplorePojoMain::class.java)
        Log.d("exe", "code is" + explorePojoMain.code)
        when (explorePojoMain.code) {
            "200" -> {
                mRefreshLayout!!.isRefreshing = false
                //arrayListExploreDatas.clear();
                arrayListExploreDatas!!.addAll(explorePojoMain.data!!)

                //.. add the ads at every consecutive position; here is 10
                /*for (int i = (lastVisibleItem + visibleThreshold); i < arrayListExploreDatas.size(); i++) {
                    if (i % 10 == 0 && i != 0) {
                        // after adding  other empty data we load native at at that position. eg; setUnifiedNativeAd
                        if (mActivity != null && isAdded())
                            loadFacebookNativeAd(i);
                    }
                }*/
                ///////////////////////////////////////////////////////////
                if (arrayListExploreDatas != null && arrayListExploreDatas!!.size > 0) {
                    isLoading = arrayListExploreDatas!!.size < 15
                    println("$TAG home page set isLoading=$isLoading")
                    rL_noProductFound!!.visibility = View.GONE
                    exploreRvAdapter!!.notifyDataSetChanged()
                    mRecyclerView!!.addOnScrollListener(object : HideShowScrollListener() {

                        override fun onHide() {
                            hideViews()
                        }

                        override fun onShow() {
                            showViews()
                        }


                        override fun onScrolled() {
                            val firstVisibleItemPositions = IntArray(2)
                            totalItemCount = gridLayoutManager!!.itemCount
                            lastVisibleItem = gridLayoutManager!!.findLastVisibleItemPositions(firstVisibleItemPositions)[0]
                            println("$TAG home activity totalItemCount=$totalItemCount lastVisibleItem=$lastVisibleItem visibleThreshold=$visibleThreshold is load more=$isLoading")
                            if (lastVisibleItem == -1) isLoading = true
                            if (!isLoading && totalItemCount <= lastVisibleItem + visibleThreshold) {
                                println("$TAG home page load more...")
                                isLoading = true
                                mRefreshLayout!!.isRefreshing = true
                                index = index + 1
                                if (isFromSearch) searchProductsApi(index) else {
                                    // check is user is logged in or not if its login then show according to location if not then show all posts.
                                    if (mSessionManager!!.isUserLoggedIn) getUserPosts(index) else getGuestPosts(index)
                                }
                            }
                        }
                    })
                }
            }
            "204" -> {
                mRefreshLayout!!.isRefreshing = false
                println(TAG + " " + "no more product=" + explorePojoMain.message)
                if (arrayListExploreDatas!!.size == 0) {
                    rL_noProductFound!!.visibility = View.VISIBLE
                }
            }
            "401" -> {
                mRefreshLayout!!.isRefreshing = false
                CommonClass.sessionExpired(mActivity!!)
            }
            else -> {
                mRefreshLayout!!.isRefreshing = false
                CommonClass.showSnackbarMessage((mActivity as HomePageActivity?)!!.rL_rootElement, explorePojoMain.message)
            }
        }
    }

    private fun hideViews() {
        //   filterView.animate().translationY(-rL_action_bar.getHeight()).setInterpolator(new AccelerateInterpolator(2)).start();
        scrollViewFilter!!.animate().translationY(-rL_action_bar!!.height.toFloat()).setInterpolator(AccelerateInterpolator(2f)).start()
        rL_action_bar!!.animate().translationY(-rL_action_bar!!.height.toFloat()).setInterpolator(AccelerateInterpolator(2f)).start()
        homePageActivity!!.bottomNavigationView!!.animate().translationY(homePageActivity!!.bottomNavigationView!!.height.toFloat()).setInterpolator(AccelerateInterpolator(2f)).start()
        rL_sell_stuff!!.animate().translationY(homePageActivity!!.bottomNavigationView!!.height.toFloat()).setInterpolator(AccelerateInterpolator(2f)).start()
        filterRv!!.animate().translationY(-rL_action_bar!!.height.toFloat()).setInterpolator(AccelerateInterpolator(2f)).start()
    }

    private fun showViews() {
        //  filterView.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2)).start();
        scrollViewFilter!!.animate().translationY(0f).setInterpolator(DecelerateInterpolator(2f)).start()
        rL_action_bar!!.animate().translationY(0f).setInterpolator(DecelerateInterpolator(2f)).start()
        homePageActivity!!.bottomNavigationView!!.animate().translationY(0f).setInterpolator(DecelerateInterpolator(2f)).start()
        rL_sell_stuff!!.animate().translationY(0f).setInterpolator(DecelerateInterpolator(2f)).start()
        filterRv!!.animate().translationY(0f).setInterpolator(DecelerateInterpolator(2f)).start()
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.rL_notification -> if (mSessionManager!!.isUserLoggedIn) {
                val intent = Intent(mActivity, NotificationActivity::class.java)
                startActivityForResult(intent, VariableConstants.IS_NOTIFICATION_SEEN_REQ_CODE)
            } else this@HomeFrag.startActivityForResult(Intent(mActivity, LandingActivity::class.java), VariableConstants.LANDING_REQ_CODE)
            R.id.rL_filter -> {
                val filterIntent = Intent(mActivity, FilterActivity::class.java)
                filterIntent.putExtra("aL_categoryDatas", aL_categoryDatas)
                filterIntent.putExtra("address", address)
                filterIntent.putExtra("distance", distance)
                filterIntent.putExtra("sortBy", sortBy)
                filterIntent.putExtra("postedWithin", postedWithin)
                filterIntent.putExtra("currency_code", currency_code)
                filterIntent.putExtra("currency_symbol", currency)
                filterIntent.putExtra("minPrice", minPrice)
                filterIntent.putExtra("maxPrice", maxPrice)
                filterIntent.putExtra("userLat", currentLatitude)
                filterIntent.putExtra("userLng", currentLongitude)
                filterIntent.putExtra("subCategoryName", subCategoryName)
                filterIntent.putExtra("sendFilters", sendFilters)
                this@HomeFrag.startActivityForResult(filterIntent, VariableConstants.FILTER_REQUEST_CODE)
            }
            R.id.rL_sell_stuff -> if (mSessionManager!!.isUserLoggedIn) {
                // startActivity(new Intent(mActivity, CameraActivity.class));
//                   if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
//                        startActivity(new Intent(mActivity, Camera2Activity.class));
//                    else
//                   callIsStripeConnectApi();
                startActivity(Intent(mActivity, CameraKitActivity::class.java))
            } else this@HomeFrag.startActivityForResult(Intent(mActivity, LandingActivity::class.java), VariableConstants.LANDING_REQ_CODE)
            R.id.rL_search -> startActivity(Intent(mActivity, SearchProductActivity::class.java))
            R.id.rl_turnOnLocation -> startActivityForResult(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), 0)
        }
    }

    //    private void callIsStripeConnectApi() {
    //        try {
    //            if (CommonClass.isNetworkAvailable(mActivity)) {
    //                JSONObject request_datas = new JSONObject();
    //                request_datas.put("token", mSessionManager.getAuthToken());
    ////                request_datas.put("accountId", "acct_1Gpy19CqyPeW9hWx");
    //                String url = ApiUrl.STRIPE_CONNECT ;
    //                OkHttp3Connection.doOkHttp3Connection(TAG, url, OkHttp3Connection.Request_type.POST, request_datas, new OkHttp3Connection.OkHttp3RequestCallback() {
    //                    @Override
    //                    public void onSuccess(String result, String user_tag) {
    //                        try {
    //                            System.out.println(TAG + " " + "get category res=" + result);
    //                            StripeMainPojo categoryMainPojo;
    //                            Gson gson = new Gson();
    //                            categoryMainPojo = gson.fromJson(result, StripeMainPojo.class);
    //
    //                            switch (categoryMainPojo.getCode()) {
    //                                // success
    //                                case "200":
    //                                    if(categoryMainPojo.isStatus()){
    //                                        startActivity(new Intent(mActivity, CameraKitActivity.class));
    //                                    }else{
    //                                        PaymentAddAccount.callActivity(getContext(),categoryMainPojo.getData().getWeburl(),STRIPE_ACCOUNT_ADDED_HOME);
    //                                    }
    //                                    break;
    //                                // auth token expired
    //                                case "401":
    //                                    CommonClass.sessionExpired(mActivity);
    //                                    break;
    //                            }
    //                        } catch (Exception e) {
    //                            e.printStackTrace();
    //                        }
    //                    }
    //
    //                    @Override
    //                    public void onError(String error, String user_tag) {
    //                        Log.e(TAG,error);
    //                    }
    //                });
    //            }
    //        }catch (Exception e){e.printStackTrace();}
    //    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        println("$TAG onactivity resultt res code=$resultCode req code=$requestCode data=$data")
        super.onActivityResult(requestCode, resultCode, data)
        if (data != null) {
            when (requestCode) {
                VariableConstants.FILTER_REQUEST_CODE -> {
                    filterRv!!.visibility = View.GONE
                    mRecyclerView!!.setPadding(0, recycleviewPaddingTop, 0, 0)

                    //  filterView.setVisibility(View.GONE);
                    aL_categoryDatas!!.clear()
                    aL_categoryDatas!!.addAll((data.getSerializableExtra("aL_categoryDatas") as ArrayList<ProductCategoryResDatas>))
                    category = data.getStringExtra("category")
                    distance = data.getStringExtra("distance")
                    sortBy = data.getStringExtra("sortBy")
                    postedWithin = data.getStringExtra("postedWithin")
                    minPrice = data.getStringExtra("minPrice")
                    maxPrice = data.getStringExtra("maxPrice")
                    currency = data.getStringExtra("currency")
                    currency_code = data.getStringExtra("currency_code")
                    locationChange = data.getStringExtra("locationChange")
                    currentLatitude = data.getStringExtra("currentLatitude")
                    currentLongitude = data.getStringExtra("currentLongitude")
                    //for change location from filter its need to be update in userPost Api
                    lat = currentLatitude
                    lng = currentLongitude
                    address = data.getStringExtra("address")
                    cityName = data.getStringExtra("cityName")
                    postedWithinText = data.getStringExtra("postedWithinText")
                    sortByText = data.getStringExtra("sortByText")
                    categoryName = data.getStringExtra("categoryName")
                    subCategoryName = data.getStringExtra("subCategoryName")
                    // filterKeyValues= (ArrayList<FilterKeyValue>) data.getSerializableExtra("filterKeyValues");
                    sendFilters = data.getSerializableExtra("sendFilters") as ArrayList<SendFilter>
                    arrayListExploreDatas!!.clear()
                    exploreRvAdapter!!.notifyDataSetChanged()
                    isFromSearch = true
                    setFilterDatasList()
                }
                VariableConstants.PRODUCT_DETAILS_REQ_CODE -> {
                    val followRequestStatus = data.getStringExtra("followRequestStatus")
                    println("$TAG followRequestStatus=$followRequestStatus")
                    val likesCount = data.getStringExtra("likesCount")
                    val likeStatus = data.getStringExtra("likeStatus")
                    val clickCount = data.getStringExtra("clickCount")
                    var aL_likedByUsers=ArrayList<ExploreLikedByUsersDatas>()
                    if(data.hasExtra("aL_likedByUsers") && data.getSerializableExtra("aL_likedByUsers")!=null) {
                         aL_likedByUsers = data.getSerializableExtra("aL_likedByUsers") as ArrayList<ExploreLikedByUsersDatas>

                    }
                    if (followRequestStatus != null && !followRequestStatus.isEmpty()) {
                        if (arrayListExploreDatas!!.size > clickedItemPosition) {
                            arrayListExploreDatas!![clickedItemPosition].followRequestStatus = followRequestStatus
                            arrayListExploreDatas!![clickedItemPosition].likes = likesCount
                            arrayListExploreDatas!![clickedItemPosition].likeStatus = likeStatus
                            arrayListExploreDatas!![clickedItemPosition].clickCount = clickCount
                            arrayListExploreDatas!![clickedItemPosition].likedByUsers = aL_likedByUsers
                        }
                    }
                }
                VariableConstants.IS_NOTIFICATION_SEEN_REQ_CODE -> {
                    val isNotificationSeen = data.getBooleanExtra("isNotificationSeen", false)
                    if (isNotificationSeen) {
                        notificationCount = 0
                        tV_notification_count!!.visibility = View.GONE
                    }
                }
                VariableConstants.REQUEST_CHECK_SETTINGS -> when (resultCode) {
                    Activity.RESULT_CANCELED -> if (arrayListExploreDatas!!.size == 0) {
                        index = 0
                        getUserPosts(index)
                    }
                }
                VariableConstants.LANDING_REQ_CODE -> {
                    val isToRefreshHomePage = data.getBooleanExtra("isToRefreshHomePage", false)
                    val isFromSignup = data.getBooleanExtra("isFromSignup", false)
                    println("$TAG isToRefreshHomePage=$isToRefreshHomePage isFromSignup=$isFromSignup")
                    if (isToRefreshHomePage) {
                        index = 0
                        arrayListExploreDatas!!.clear()
                        exploreRvAdapter!!.notifyDataSetChanged()
                        mProgressBar!!.visibility = View.VISIBLE
                        exploreRvAdapter!!.notifyDataSetChanged()
                        if (runTimePermission!!.checkPermissions(permissionsArray)) {
                            currentLocation
                        } else {
                            requestPermissions(permissionsArray, VariableConstants.PERMISSION_REQUEST_CODE)
                        }

                        // open start browsering screen
                        if (isFromSignup) DialogBox(mActivity!!).startBrowsingDialog()
                    }
                }
            }
        }
    }

    @Subscribe
    fun getMessage(setExploreResponseDatas: ExploreResponseDatas?) {
        try {
            if (setExploreResponseDatas != null) {
                // add item
                if (!isContainsId(setExploreResponseDatas.postId)) {
                    arrayListExploreDatas!!.add(0, setExploreResponseDatas)
                    exploreRvAdapter!!.notifyDataSetChanged()
                } else {
                    if (arrayListExploreDatas?.size!! > 0 && setExploreResponseDatas.isToRemoveHomeItem) {
                        for (homeItemCount in arrayListExploreDatas?.indices!!) {
                            if (arrayListExploreDatas!!.size>homeItemCount && setExploreResponseDatas.postId == arrayListExploreDatas!![homeItemCount].postId) {
                                arrayListExploreDatas!!.removeAt(clickedItemPosition)
                                exploreRvAdapter!!.notifyDataSetChanged()
                            }
                        }
                    }
                }
            }
            if (arrayListExploreDatas!!.size > 0) rL_noProductFound!!.visibility = View.GONE else rL_noProductFound!!.visibility = View.VISIBLE
        }catch (e:Exception){e.printStackTrace()}
    }

    /**
     * <h>IsContainsId</h>
     *
     *
     * In this method we used to check whether the given post id is
     * present or not in the current list.
     *
     *
     * @param postId the given post id of product
     * @return the boolean value
     */
    fun isContainsId(postId: String): Boolean {
        var flag = false
        for (`object` in arrayListExploreDatas!!) {
            println(TAG + " " + "given post id=" + postId + " " + "current post id=" + `object`.postId)
            if (postId == `object`.postId) {
                flag = true
            }
        }
        return flag
    }
    // private void
    /**
     * <h>SetFilterDatasList</h>
     *
     *
     * In this method we used to set the all filter datas like categories(Baby abd child, electronics etc),
     * posted within(The last 24hr) etc into list.
     *
     */
    private fun setFilterDatasList() {
        var unSelectedCount = 0

        //////////////////////////////
        if (aL_categoryDatas != null && aL_categoryDatas!!.size > 0) {
            category = ""
            categoryValue = ""
            for (productCategoryResDatas in aL_categoryDatas!!) {
                if (productCategoryResDatas.isSelected) {
                    val name = productCategoryResDatas.name
                    category += "^" + name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase()
                    categoryValue += ",$name"
                } else {
                    unSelectedCount += 1
                }
            }
            if (unSelectedCount == aL_categoryDatas!!.size) {
                category = ""
                categoryValue = ""
            }
            println("$TAG unselected count=$unSelectedCount")
        }

        // remove comma
        if (!categoryValue.isEmpty()) categoryValue = categoryValue.substring(1)
        if (categoryName != null && !categoryName!!.isEmpty()) categoryValue = categoryName!!
        println("$TAG categoryValue=$categoryValue")

        // remove first character
        if (category != null && !category!!.isEmpty()) category = category!!.substring(1)
        println("$TAG selected category=$category")

        ////////////////////
        val category_arr: Array<String>
        category_arr = if (category != null && !category!!.isEmpty()) {
            category!!.split("\\^".toRegex()).toTypedArray()
        } else arrayOf()

        // create empty arraylist
        arrayListFilter = ArrayList()

        //set category
        Collections.addAll(arrayListFilter, *category_arr)
        println(TAG + " " + "arrayListFilter size=" + arrayListFilter!!.size)

        // add distance
        if (distance != null && !distance!!.isEmpty() && distance != "0") arrayListFilter!!.add(distance + " " + resources.getString(R.string.km))

        // add sorted by
        if (sortByText != null && !sortByText!!.isEmpty()) arrayListFilter!!.add(sortByText!!)

        // add posted within
        if (postedWithinText != null && !postedWithinText!!.isEmpty()) arrayListFilter!!.add(postedWithinText!!)
        if (minPrice != null && !minPrice!!.isEmpty()) arrayListFilter!!.add(resources.getString(R.string.min_price) + " " + currency + minPrice)
        if (maxPrice != null && !maxPrice!!.isEmpty()) arrayListFilter!!.add(resources.getString(R.string.max_price) + " " + currency + maxPrice)

        // add subcategory
        if (subCategoryName != null && !subCategoryName!!.isEmpty()) arrayListFilter!!.add(subCategoryName!!)

        // add filter meta data
        if (sendFilters != null && sendFilters!!.size > 0) for (f in sendFilters!!) {
            if (f.value != null) arrayListFilter!!.add(f.filedName + ":" + f.value)
            if (f.from != null) arrayListFilter!!.add(f.filedName + ":" + f.from + "," + f.to)
        }


        // add location
        if (locationChange == "1") if (currentLatitude != null && !currentLatitude!!.isEmpty() && currentLongitude != null && !currentLongitude!!.isEmpty()) {
            // arrayListFilter.add("Lat " + currentLatitude);
            // arrayListFilter.add("Lng " + currentLongitude);
            filterCity = CommonClass.getCityName(mActivity!!, currentLatitude!!.toDouble(), currentLongitude!!.toDouble())
            filterCountry = CommonClass.getCountryName(mActivity!!, currentLatitude!!.toDouble(), currentLongitude!!.toDouble())
            if (filterCity != null && !filterCity!!.isEmpty()) if (filterCountry != null && !filterCountry!!.isEmpty()) arrayListFilter!!.add("$filterCity, $filterCountry") else arrayListFilter!!.add(filterCity!!) else if (filterCountry != null && !filterCountry!!.isEmpty()) arrayListFilter!!.add("$cityName, $filterCountry")
        }
        inflateFilterDatas()
    }

    /**
     * <h>InflateFilterDatas</h>
     *
     *
     * In this method we used to inflate the the filtered data list like distance, price etc on the top
     * of the screen.
     *
     */
    private fun inflateFilterDatas() {
        println(TAG + " " + "array list filter size=" + arrayListFilter!!.size)
        if (arrayListFilter != null && arrayListFilter!!.size > 0) {
            // view_filter_divider.setVisibility(View.VISIBLE);
            linear_filter!!.removeAllViews()
            val layoutInflater = LayoutInflater.from(mActivity)
            linear_filter!!.removeAllViews()

            //..remove all filter..//
            val v = layoutInflater.inflate(R.layout.single_row_selected_filter_remove_icon, null, false)
            if (arrayListFilter!!.size > 0) {
                val iV_remove_all = v.findViewById<View>(R.id.iV_remove_all) as ImageView
                iV_remove_all.setOnClickListener {
                    for (i in aL_categoryDatas!!.indices) {
                        aL_categoryDatas!![i].isSelected = false
                    }
                    sendFilters!!.clear()
                    subCategoryName = ""
                    categoryName = ""
                    distance = ""
                    minPrice = ""
                    maxPrice = ""
                    postedWithinText = ""
                    postedWithin = ""
                    sortBy = ""
                    sortByText = ""
                    currentLongitude = ""
                    currentLatitude = ""
                    locationChange = ""
                    //                        getCurrentLocation();
                    setFilterDatasList()
                }
                linear_filter!!.addView(v)
            }
            ////////////////////////
            for (postCount in arrayListFilter!!.indices) {
                val view = layoutInflater.inflate(R.layout.single_row_selected_filter_list, null, false)
                val tV_filter = view.findViewById<View>(R.id.tV_filter) as TextView
                val iV_filter = view.findViewById<View>(R.id.iV_filter) as ImageView
                if (aL_categoryDatas!!.size > 0) {
                    for (i in aL_categoryDatas!!.indices) {
                        if (aL_categoryDatas!![i].name.equals(arrayListFilter!![postCount], ignoreCase = true)) {
                            Picasso.with(mActivity)
                                    .load(aL_categoryDatas!![i].activeimage)
                                    .resizeDimen(R.dimen.fifteen_dp, R.dimen.fifteen_dp)
                                    .centerCrop()
                                    .transform(CircleTransform())
                                    .placeholder(R.drawable.default_circle_img)
                                    .into(iV_filter)
                        }
                    }
                }
                tV_filter.text = arrayListFilter!![postCount]
                val tV_delete = view.findViewById<View>(R.id.tV_delete) as ImageView
                val sortByArr = resources.getStringArray(R.array.sortArr)
                val postedWithinArr = resources.getStringArray(R.array.postedArr)
                tV_delete.setOnClickListener {
                    if (arrayListFilter!!.size > 0) {
                        val deletedValue = arrayListFilter!![postCount]
                        println("$TAG filter value=$deletedValue")

                        // remove category value
                        if (aL_categoryDatas!!.size > 0) {
                            for (i in aL_categoryDatas!!.indices) {
                                println(TAG + " get cate name=" + aL_categoryDatas!![i].name)
                                if (aL_categoryDatas!![i].name.equals(deletedValue, ignoreCase = true)) {
                                    println(TAG + "removed=" + aL_categoryDatas!![i].name)
                                    aL_categoryDatas!![i].isSelected = false
                                    sendFilters!!.clear()
                                    subCategoryName = ""
                                    categoryName = ""
                                }
                            }
                        }

                        // remove filter metadata
                        if (sendFilters != null && sendFilters!!.size > 0) {
                            for (i in sendFilters!!.indices) {
                                if (deletedValue.contains(sendFilters!![i].filedName)) sendFilters!!.removeAt(i)
                            }
                        }

                        // remove subcategory
                        if (deletedValue.equals(subCategoryName, ignoreCase = true)) {
                            subCategoryName = ""
                            sendFilters!!.clear()
                        }

                        // remove distance value
                        if (deletedValue.contains(resources.getString(R.string.km))) distance = ""

                        // remove min price
                        if (deletedValue.contains(resources.getString(R.string.min_price))) minPrice = ""

                        // remove max price
                        if (deletedValue.contains(resources.getString(R.string.max_price))) maxPrice = ""

                        // remove posted within value
                        if (postedWithinArr.size > 0) {
                            for (post in postedWithinArr) {
                                if (post.equals(deletedValue, ignoreCase = true)) {
                                    postedWithinText = ""
                                    postedWithin = ""
                                }
                            }
                        }

                        // remove sort by value
                        if (sortByArr.size > 0) {
                            for (sort in sortByArr) {
                                if (sort.equals(deletedValue, ignoreCase = true)) {
                                    sortBy = ""
                                    sortByText = ""
                                }
                            }
                        }

                        // remove location lat nad long
                        if (deletedValue.contains(cityName) || deletedValue.contains(filterCountry!!) || deletedValue.contains(filterCity!!)) {
                            currentLatitude = ""
                            currentLongitude = ""
                            locationChange = ""
                        }
                        arrayListFilter!!.removeAt(postCount)
                        linear_filter!!.removeView(view)
                        setFilterDatasList()
                        //inflateFilterDatas();
                    }
                }
                linear_filter!!.addView(view)
            }
            mRecyclerView!!.setPadding(0, recycleviewPaddingTop + CommonClass.dpToPx(mActivity!!, 40), 0, 0)
        } else {
            linear_filter!!.removeAllViews()
            filterRv!!.visibility = View.VISIBLE
            mRecyclerView!!.setPadding(0, recycleviewPaddingTop + recycleviewPaddingTop, 0, 0)
        }

        //call filter api
        arrayListExploreDatas!!.clear()
        exploreRvAdapter!!.notifyDataSetChanged()
        mRefreshLayout!!.isRefreshing = true
        index = 0
        if (arrayListFilter!!.size > 0) {
            isFromSearch = true
            searchProductsApi(index)
        } else {
            isFromSearch = false
            filterRv!!.visibility = View.VISIBLE
            // filterView.setVisibility(View.VISIBLE);

            // check is user is logged in or not if its login then show according to location if not then show all posts.
            if (mSessionManager!!.isUserLoggedIn) {
                println("$TAG lat in refreshing=$lat lng=$lng")
                if (isLocationFound(lat, lng)) getUserPosts(index) else currentLocation
            } else if (isLocationFound(lat, lng)) {
                //getGuestPosts(index);
                if (isLocationFound(lat, lng)) getGuestPosts(index) else currentLocation
            } else currentLocation
        }
    }

    /**
     * <h>SearchProductsApi</h>
     *
     *
     * In this method we do api call for searching product based on filtering
     * like on category, min price, max price etc.
     *
     */
    private fun searchProductsApi(offset: Int) {
        var offset = offset
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            //mProgressBar.setVisibility(View.VISIBLE);
            rL_no_internet!!.visibility = View.GONE
            val limit = 20
            offset = limit * offset
            val request_datas = JSONObject()
            try {
                request_datas.put("token", mSessionManager!!.authToken) //mendatory
                request_datas.put("limit", limit) //mendatory
                request_datas.put("offset", offset) //mendatory
                request_datas.put("latitude", currentLatitude) //mendatory
                request_datas.put("longitude", currentLongitude) //mendatory
                request_datas.put("location", address) //mendatory
                request_datas.put("distanceMax", distance) //mendatory
                request_datas.put("category", categoryValue)
                request_datas.put("subCategory", subCategoryName)
                request_datas.put("sortBy", sortBy)
                request_datas.put("postedWithin", postedWithin)
                request_datas.put("filter", createFilterJsonArray().toString())
                request_datas.put("price", createPriceJsonObj().toString())
                /* no need to send in new Api filterProduct
                request_datas.put("currency", currency_code);
                request_datas.put("maxPrice", maxPrice);
                request_datas.put("minPrice", minPrice);
                request_datas.put("pushToken", mSessionManager.getPushToken());*/
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            println("$TAG offset in search api=$offset")
            OkHttp3Connection.doOkHttp3Connection(TAG, ApiUrl.FILTER_PRODUCT, OkHttp3Connection.Request_type.POST, request_datas, object : OkHttp3RequestCallback {
                 override fun onSuccess(result: String?, user_tag: String?) {
                    mProgressBar!!.visibility = View.GONE
                    println("$TAG product search res=$result")
                    if (result != null && !result.isEmpty()) responseHandler(result)
                }

                 override fun onError(error: String?, user_tag: String?) {
                    mProgressBar!!.visibility = View.GONE
                    mRefreshLayout!!.isRefreshing = false
                    CommonClass.showSnackbarMessage((mActivity as HomePageActivity?)!!.rL_rootElement, error)
                }
            })
        } else {
            mRefreshLayout!!.isRefreshing = false
            rL_no_internet!!.visibility = View.VISIBLE
        }
    }

    fun createFilterJsonArray(): JSONArray {
        val array = JSONArray()
        try {
            for (s in sendFilters!!) {
                val `object` = JSONObject()
                `object`.put("type", s.type)
                `object`.put("fieldName", s.filedName)
                `object`.put("value", s.value)
                `object`.put("from", s.from)
                `object`.put("to", s.to)
                array.put(`object`)
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        return array
    }

    fun createPriceJsonObj(): JSONObject {
        val `object` = JSONObject()
        try {
            `object`.put("currency", currency_code)
            `object`.put("from", minPrice)
            `object`.put("to", maxPrice)
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        return `object`
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        isHomeFragVisible = isVisibleToUser
    }

    override fun onItemClick(pos: Int, imageView: ImageView?) {
        println(TAG + " " + "pos=" + pos + " " + "list size=" + arrayListExploreDatas!!.size)

        // swipe pager adapter using for swipe to next
        /*if (arrayListExploreDatas.size() > pos){
            Intent intent=new Intent(mActivity, ProductsActivity.class);
            intent.putExtra("arrayListExploreDatas",arrayListExploreDatas);
            intent.putExtra("position",pos);
            startActivity(intent);
        }*/if (arrayListExploreDatas!!.size > pos && pos >= 0) {
            clickedItemPosition = pos
            val intent = Intent(mActivity, ProductDetailsActivity::class.java)
            intent.putExtra("productName", arrayListExploreDatas!![pos].productName)
            intent.putExtra("image", arrayListExploreDatas!![pos].mainUrl)
            intent.putExtra("category", arrayListExploreDatas!![pos].category)
            intent.putExtra("subCategory", arrayListExploreDatas!![pos].subCategory)
            intent.putExtra("likes", arrayListExploreDatas!![pos].likes)
            intent.putExtra("likeStatus", arrayListExploreDatas!![pos].likeStatus)
            intent.putExtra("currency", arrayListExploreDatas!![pos].currency)
            intent.putExtra("price", arrayListExploreDatas!![pos].price)
            intent.putExtra("postedOn", arrayListExploreDatas!![pos].postedOn)
            intent.putExtra("thumbnailImageUrl", arrayListExploreDatas!![pos].thumbnailImageUrl)
            intent.putExtra("likedByUsersArr", arrayListExploreDatas!![pos].likedByUsers)
            intent.putExtra("description", arrayListExploreDatas!![pos].description)
            intent.putExtra("condition", arrayListExploreDatas!![pos].condition)
            intent.putExtra("place", arrayListExploreDatas!![pos].place)
            intent.putExtra("latitude", arrayListExploreDatas!![pos].latitude)
            intent.putExtra("longitude", arrayListExploreDatas!![pos].longitude)
            intent.putExtra("postedByUserName", arrayListExploreDatas!![pos].postedByUserName)
            intent.putExtra("postId", arrayListExploreDatas!![pos].postId)
            intent.putExtra("postsType", arrayListExploreDatas!![pos].postsType)
            intent.putExtra("followRequestStatus", arrayListExploreDatas!![pos].followRequestStatus)
            intent.putExtra("clickCount", arrayListExploreDatas!![pos].clickCount)
            intent.putExtra("negotiable", arrayListExploreDatas!![pos].negotiable)
            intent.putExtra("memberProfilePicUrl", arrayListExploreDatas!![pos].memberProfilePicUrl)
            intent.putExtra("isPromoted", arrayListExploreDatas!![pos].isPromoted)
            intent.putExtra(VariableConstants.EXTRA_ANIMAL_IMAGE_TRANSITION_NAME, ViewCompat.getTransitionName(imageView!!))
            println(TAG + " " + "followRequestStatus=" + arrayListExploreDatas!![pos].followRequestStatus)
            this@HomeFrag.startActivityForResult(intent, VariableConstants.PRODUCT_DETAILS_REQ_CODE)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        println("$TAG on request permission result called...")
        when (requestCode) {
            VariableConstants.PERMISSION_REQUEST_CODE -> {
                println("grant result=" + grantResults.size)
                if (grantResults.size > 0) {
                    var count = 0
                    while (count < grantResults.size) {
                        if (grantResults[count] != PackageManager.PERMISSION_GRANTED) allowPermissionAlert(permissions[count])
                        count++
                    }
                    println("isAllPermissionGranted=" + runTimePermission!!.checkPermissions(permissionsArray))
                    if (runTimePermission!!.checkPermissions(permissionsArray)) {
                        currentLocation
                    }
                }
            }
            VariableConstants.HOME_FILTER_LOCATION_ACCESS_REQ -> {
                println("grant result=" + grantResults.size)
                if (grantResults.size > 0) {
                    var count = 0
                    while (count < grantResults.size) {
                        if (grantResults[count] != PackageManager.PERMISSION_GRANTED) CommonClass.showSnackbarMessage(rL_rootview, getString(R.string.location_permission_request))
                        count++
                    }
                    println("isAllPermissionGranted=" + runTimePermission!!.checkPermissions(permissionsArray))
                    if (runTimePermission!!.checkPermissions(permissionsArray)) {
                        userCurrentLocationForHomeFilter
                    }
                }
            }
        }
    }

    /**
     * <h>ShowErrorMessage</h>
     *
     *
     * In this method we used to show dialog for error message if
     * the user denies any permissions.
     *
     *
     * @param permissionName the permission name
     */
    fun allowPermissionAlert(permissionName: String) {
        val errorMessageDialog = Dialog(mActivity!!)
        errorMessageDialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        errorMessageDialog.setContentView(R.layout.dialog_permission_denied)
        errorMessageDialog.window!!.setGravity(Gravity.BOTTOM)
        errorMessageDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        errorMessageDialog.window!!.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT)
        errorMessageDialog.window!!.attributes.windowAnimations = R.style.DialogAnimation
        val tV_permission_name = errorMessageDialog.findViewById<View>(R.id.tV_permission_name) as TextView
        var deniedPermission: String
        deniedPermission = if (permissionName.contains("CAMERA")) {
            mActivity!!.resources.getString(R.string.camera)
        } else if (permissionName.contains("WRITE_EXTERNAL_STORAGE")) {
            mActivity!!.resources.getString(R.string.external_storage)
        } else if (permissionName.contains("ACCESS_COARSE_LOCATION")) {
            mActivity!!.resources.getString(R.string.coarse_location)
        } else if (permissionName.contains("ACCESS_FINE_LOCATION")) {
            mActivity!!.resources.getString(R.string.fine_location)
        } else if (permissionName.contains("READ_CONTACTS")) {
            mActivity!!.resources.getString(R.string.read_contact)
        } else permissionName
        deniedPermission = mActivity!!.resources.getString(R.string.please_allow_the_permission) + " " + deniedPermission + " " + mActivity!!.resources.getString(R.string.by_clicking_on_allow_button)
        tV_permission_name.text = deniedPermission

        // Cancel
        val tV_cancel = errorMessageDialog.findViewById<View>(R.id.tV_cancel) as TextView
        tV_cancel.setOnClickListener {
            if (permissionName.contains("ACCESS_COARSE_LOCATION") && coarseLocResult == PackageManager.PERMISSION_DENIED) {
                println("$TAG location fine denied...")
                isFineLocDenied = true
            }
            if (permissionName.contains("ACCESS_FINE_LOCATION") && fineLocResult == PackageManager.PERMISSION_DENIED) {
                println("$TAG location coarse denied...")
                isCoarseLocDenied = true
            }
            println("$TAG fineLocResult denied=$isFineLocDenied coarseLocResult denied=$isCoarseLocDenied")
            if (isCoarseLocDenied && isFineLocDenied) {
                index = 0
                lng = ""
                lat = lng
                arrayListExploreDatas!!.clear()
                exploreRvAdapter!!.notifyDataSetChanged()
                getUserPosts(index)
            }
            errorMessageDialog.dismiss()
        }

        // allow
        val rL_allow = errorMessageDialog.findViewById<View>(R.id.rL_allow) as RelativeLayout
        rL_allow.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (mActivity!!.shouldShowRequestPermissionRationale(permissionName)) requestPermissions(permissionsArray, VariableConstants.PERMISSION_REQUEST_CODE)
            }
            errorMessageDialog.dismiss()
        }
        errorMessageDialog.show()
    }

    val filterData: Unit
        get() {
            currentLatitude = mSessionManager!!.currentLat
            currentLongitude = mSessionManager!!.currentLng
            if (isLocationFound(currentLatitude, currentLongitude)) {
                filterRv!!.visibility = View.GONE
                var city: String? = ""
                var country: String? = ""
                try {
                    address = CommonClass.getCompleteAddressString(mActivity, currentLatitude!!.toDouble(), currentLongitude!!.toDouble())
                    city = CommonClass.getCityName(mActivity!!, currentLatitude!!.toDouble(), currentLongitude!!.toDouble())
                    country = CommonClass.getCityName(mActivity!!, currentLatitude!!.toDouble(), currentLongitude!!.toDouble())
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if (address == null || address!!.isEmpty()) address = CommonClass.makeAddressFromCityCountry(city, country)
                arrayListExploreDatas!!.clear()
                exploreRvAdapter!!.notifyDataSetChanged()
                setFilterDatasList()
            } else {
                if (runTimePermission!!.checkPermissions(permissionsArray)) {
                    userCurrentLocationForHomeFilter
                } else {
                    requestPermissions(permissionsArray, VariableConstants.HOME_FILTER_LOCATION_ACCESS_REQ)
                }
            }
        }

    /*
     * Facebook Native Ads
     * In this method we load ad and insert in to explore list.
     *
     * */
    private fun loadFacebookNativeAd(pos: Int) {
        val nativeAd = NativeAd(this.context, getString(R.string.facebookNativeAdId))
        nativeAd.setAdListener(object : NativeAdListener {
            override fun onMediaDownloaded(ad: Ad) {
                println(TAG + "facebook Native ad finished downloading all assets.")
            }

            override fun onError(ad: Ad, adError: AdError) {
                println(TAG + " " + "facebook native ad not added at " + pos + " error:" + adError.errorCode + " " + adError.errorMessage)
            }

            override fun onAdLoaded(ad: Ad) {
                if (arrayListExploreDatas!!.size > pos) {
                    arrayListExploreDatas!!.add(pos, ExploreResponseDatas())
                    arrayListExploreDatas!![pos].ad = ad
                    println("$TAG facebook native ad added at $pos")
                    exploreRvAdapter!!.notifyItemChanged(pos)
                }
            }

            override fun onAdClicked(ad: Ad) {}
            override fun onLoggingImpression(ad: Ad) {}
        })
        nativeAd.loadAd()
    }

    companion object {
        private val TAG = HomeFrag::class.java.simpleName
        var isClickableRv = true
    }
}