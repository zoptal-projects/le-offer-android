package com.zoptal.cellableapp.main.activity

import android.app.Activity
import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.text.SpannableString
import android.text.Spanned
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import butterknife.BindView
import butterknife.ButterKnife
//import butterknife.OnClick
import butterknife.Unbinder
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.main.activity.HomePageActivity
import com.zoptal.cellableapp.utility.SessionManager
import kotlinx.android.synthetic.main.dialog_show_privacy.*

/**
 * <h>WebActivity class</h>
 *
 *
 * This activity mainly load the url to webView, url passed via intent form other activity.
 *
 * @author 3Embed
 * @since 30/01/18.
 */
class PermissionAcceptActivity : AppCompatActivity() {
    /* @Inject
    TypeFaceManager typeFaceManager;*/
//    @BindView(R.id.page_title_tv)
//    var tvPageTitle: TextView? = null

//    @BindView(R.id.rL_allow)
//    var rL_allow: RelativeLayout? = null

//    @BindView(R.id.tV_deny)
//    var tV_deny: TextView? = null
//    private var unbinder: Unbinder? = null
    val mActivity: Activity = this
    private val url: String? = null
    private val pageTitle: String? = ""
    private var postResultData = ""
    override fun onCreate(savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.dialog_show_privacy)
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
//        unbinder = ButterKnife.bind(this)
        //url = getResources().getString(R.string.privacyPolicyUrl);
        //pageTitle = getIntent().getStringExtra("title");
//        page_title_tv=findViewById(R.id.page_title_tv)
        initView()
        //initWebview();
    }

    /**
     * set the page title and typeface.
     */
    private fun initView() {
        try {
            if (page_title_tv != null) {
                page_title_tv!!.setText(getResources().getString(R.string.privacyPolicy))
            }
            postResultData = getIntent().getStringExtra("postResultData")

            //tvPageTitle.setTypeface(typeFaceManager.getCircularAirBold());
            val mPrivacyLink: TextView = findViewById(R.id.tV_privacy)
            val s1 = mActivity.resources.getString(R.string.privacy_link_text)
            val s2 = mActivity.resources.getString(R.string.privacyPolicy)
            val privacy: String = getResources().getString(R.string.privacyPolicyUrl)
            val privacyTitle: String = getResources().getString(R.string.privacyPolicy)
            val message = "$s1 $s2"
            val spannableString = SpannableString(message)
            //spannableString.setSpan(new MyClickableSpan(true,terms,termsConditions),s1.length()+1,s1.length()+s2.length()+1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            spannableString.setSpan(MyClickableSpan(true, privacy, privacyTitle), s1.length + 1, s1.length + s2.length + 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
            mPrivacyLink.text = spannableString
            mPrivacyLink.movementMethod = LinkMovementMethod.getInstance()
            mPrivacyLink.setOnClickListener {
                val intent = Intent(mActivity, PrivacyAndTermsActivity::class.java)
                intent.putExtra("url", mActivity.resources.getString(R.string.privacyPolicyUrl))
                intent.putExtra("title", mActivity.resources.getString(R.string.privacyPolicy))
                mActivity.startActivity(intent)
            }
        }catch (e:Exception){e.printStackTrace()}
    }

 override fun onBackPressed() {
     super.onBackPressed()
     mActivity.overridePendingTransition(R.anim.slide_up, R.anim.slide_down)
    }

//    @OnClick(R.id.close_button)
    fun onBack( v:View) {
        onBackPressed()
    }

//    override fun onDestroy() {
//        super.onDestroy()
//        unbinder!!.unbind()
//    }

//    @OnClick(R.id.rL_allow)
    fun accept(v:View) {
        val sessionManager = SessionManager(mActivity)
        sessionManager.privacyAccept = true
        val intent = Intent(mActivity, HomePageActivity::class.java)
        intent.putExtra("postResultData", postResultData)
        startActivity(intent)
        finish()
    }

//    @OnClick(R.id.tV_deny)
    fun deny(v:View) {
        onBackPressed()
    }

    /**
     * <h>MyClickableSpan</h>
     *
     *
     * In this method we used to set the text clickable part by part seperately
     * like "Terms and Conditions" seperate and "Privacy Policy" seperate.
     *
     */
    private inner class MyClickableSpan internal constructor(private val isUnderLine: Boolean, private val setUrl: String, private val title: String) : ClickableSpan() {
        override fun onClick(widget: View) {
            val privacyIntent = Intent(mActivity, PrivacyAndTermsActivity::class.java)
            privacyIntent.putExtra("url", setUrl)
            privacyIntent.putExtra("title", title)
            startActivity(privacyIntent)
        }

        override fun updateDrawState(ds: TextPaint) {
            super.updateDrawState(ds)
            ds.isUnderlineText = isUnderLine
            ds.color = ContextCompat.getColor(mActivity, R.color.color_overlay)
            ds.isFakeBoldText = false
        }

    }
}