package com.zoptal.cellableapp.main.view_pager.my_profile_frag

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.annotation.Nullable
import androidx.core.app.ActivityOptionsCompat

import androidx.core.view.ViewCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout

import com.google.gson.Gson
import com.squareup.otto.Subscribe
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.adapter.LikedPostsRvAdap
import com.zoptal.cellableapp.event_bus.BusProvider.instance
import com.zoptal.cellableapp.main.activity.HomePageActivity
import com.zoptal.cellableapp.main.activity.products.ProductDetailsActivity
import com.zoptal.cellableapp.pojo_class.likedPosts.LikedPostPojoMain
import com.zoptal.cellableapp.pojo_class.likedPosts.LikedPostResponseDatas
import com.zoptal.cellableapp.utility.*
import com.zoptal.cellableapp.utility.OkHttp3Connection.OkHttp3RequestCallback
import org.json.JSONException
import org.json.JSONObject
import java.util.*

/**
 * <h>FavouriteFrag</h>
 *
 *
 * In this class we used to show Liked item listing in staggered grid view.
 *
 * @since 4/7/2017
 */
class FavouriteFrag : Fragment() {
    private var mActivity: Activity? = null
    private var mSessionManager: SessionManager? = null
    private var progress_bar_profile: ProgressBar? = null
    private var rL_noProductFound: RelativeLayout? = null
    private var memberName: String? = null
    private var arrayListLikedPosts: ArrayList<LikedPostResponseDatas>? = null
    private var likedPostsRvAdap: LikedPostsRvAdap? = null
    private var gridLayoutManager: StaggeredGridLayoutManager? = null
    private var mSwipeRefreshLayout: SwipeRefreshLayout? = null
    private var rV_myprofile_fav: RecyclerView? = null
    private var pageIndex = 0
    private var clickedItemPosition = 0
    private var isToCallFirstTime = false

    // Load more var
    private var isLoadingRequired = false
    private var visibleItemCount = 0
    private var totalItemCount = 0
    private val visibleThreshold = 5
    override fun onCreate(@Nullable savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        mActivity = getActivity()
        mSessionManager = SessionManager(mActivity!!)
        memberName = getArguments()!!.getString("memberName")
        arrayListLikedPosts = ArrayList()
        isToCallFirstTime = true
    }

    @Nullable
    override fun onCreateView(inflater: LayoutInflater, @Nullable container: ViewGroup?, @Nullable savedInstanceState: Bundle?): View {
        println("$TAG fragment fav called...$isToCallFirstTime")
        isLoadingRequired = false
        pageIndex = 0
        val view = inflater.inflate(R.layout.frag_profile_buying, container, false)
        progress_bar_profile = view.findViewById<View>(R.id.progress_bar_profile) as ProgressBar
        rV_myprofile_fav = view.findViewById<View>(R.id.rV_myprofile_selling) as RecyclerView
        mSwipeRefreshLayout = view.findViewById<View>(R.id.swipeRefreshLayout) as SwipeRefreshLayout
        mSwipeRefreshLayout!!.setColorSchemeResources(R.color.pink_color)
        rL_noProductFound = view.findViewById<View>(R.id.rL_noProductFound) as RelativeLayout
        rL_noProductFound!!.visibility = View.GONE

        // start discovering
        val rL_start_selling = view.findViewById<View>(R.id.rL_start_selling) as RelativeLayout
        rL_start_selling.setOnClickListener {
            val i = mActivity!!.intent
            i.putExtra("isToFinishLoginSignup", false)
            i.putExtra("isFromSignup", false)
            startActivity(i)
            mActivity!!.finish()
        }

        // set empty favourite icon
        val iV_default_icon = view.findViewById<View>(R.id.iV_default_icon) as ImageView
        iV_default_icon.setImageResource(R.drawable.empty_fav_icon)
        val tV_no_ads = view.findViewById<View>(R.id.tV_no_ads) as TextView
        tV_no_ads.setText(getResources().getString(R.string.found_your_fav_yet))
        val tV_snapNpost = view.findViewById<View>(R.id.tV_snapNpost) as TextView
        tV_snapNpost.setText(getResources().getString(R.string.ads_you_love_and_mark))
        val tV_start_discovering = view.findViewById<View>(R.id.tV_start_discovering) as TextView
        tV_start_discovering.setText(getResources().getString(R.string.start_discovery))

        // set favourite recycler view adapter
        likedPostsRvAdap = LikedPostsRvAdap(mActivity!!, arrayListLikedPosts!!, object : ProductItemClickListener {
            override fun onItemClick(pos: Int, imageView: ImageView?) {
                clickedItemPosition = pos
                val intent = Intent(mActivity, ProductDetailsActivity::class.java)
                intent.putExtra("productName", arrayListLikedPosts!![pos].productName)
                if (arrayListLikedPosts!![pos].categoryData != null) intent.putExtra("category", arrayListLikedPosts!![pos].categoryData!![0].category)
                intent.putExtra("likes", arrayListLikedPosts!![pos].likes)
                intent.putExtra("likeStatus", arrayListLikedPosts!![pos].likeStatus)
                intent.putExtra("currency", arrayListLikedPosts!![pos].currency)
                intent.putExtra("price", arrayListLikedPosts!![pos].price)
                intent.putExtra("postedOn", arrayListLikedPosts!![pos].postedOn)
                intent.putExtra("image", arrayListLikedPosts!![pos].mainUrl)
                intent.putExtra("thumbnailImageUrl", arrayListLikedPosts!![pos].thumbnailImageUrl)
                intent.putExtra("likedByUsersArr", arrayListLikedPosts!![pos].likedByUsers)
                intent.putExtra("description", arrayListLikedPosts!![pos].description)
                intent.putExtra("condition", arrayListLikedPosts!![pos].condition)
                intent.putExtra("place", arrayListLikedPosts!![pos].place)
                intent.putExtra("latitude", arrayListLikedPosts!![pos].latitude)
                intent.putExtra("longitude", arrayListLikedPosts!![pos].longitude)
                intent.putExtra("postedByUserName", arrayListLikedPosts!![pos].postedByUserName)
                intent.putExtra("postId", arrayListLikedPosts!![pos].postId)
                intent.putExtra("postsType", arrayListLikedPosts!![pos].postsType)
                intent.putExtra("followRequestStatus", "")
                intent.putExtra("clickCount", "")
                intent.putExtra("memberProfilePicUrl", "")
                intent.putExtra(VariableConstants.EXTRA_ANIMAL_IMAGE_TRANSITION_NAME, ViewCompat.getTransitionName(imageView!!))
                val options = ActivityOptionsCompat.makeSceneTransitionAnimation(mActivity!!, imageView, ViewCompat.getTransitionName(imageView)!!)
                startActivityForResult(intent, VariableConstants.PRODUCT_DETAILS_REQ_CODE, options.toBundle())
            }
        })
        gridLayoutManager = StaggeredGridLayoutManager(2, 1)
        rV_myprofile_fav!!.setLayoutManager(gridLayoutManager)
        rV_myprofile_fav!!.setAdapter(likedPostsRvAdap)

        // set space equility between recycler view items
        val spanCount = 2 // 2 columns
        val spacing = CommonClass.dpToPx(mActivity!!, 12) // convert 12dp to pixel
        rV_myprofile_fav!!.addItemDecoration(SpacesItemDecoration(spanCount, spacing))
        if (CommonClass.isNetworkAvailable(mActivity!!) && isToCallFirstTime) {
            progress_bar_profile!!.visibility = View.VISIBLE
            arrayListLikedPosts!!.clear()
            likedPostsApi(pageIndex)
        } else CommonClass.showSnackbarMessage((mActivity as HomePageActivity?)!!.rL_rootElement, getResources().getString(R.string.NoInternetAccess))

        // pull to refresh
        mSwipeRefreshLayout!!.setOnRefreshListener(object : SwipeRefreshLayout.OnRefreshListener {
            override fun onRefresh() {
                pageIndex = 0
                arrayListLikedPosts!!.clear()
                likedPostsRvAdap!!.notifyDataSetChanged()
                likedPostsApi(pageIndex)
            }
        })
        return view
    }

    override  fun onViewCreated(view: View, @Nullable savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        instance.register(this)
    }

    /**
     * <h>LikedPostsApi</h>
     *
     *
     * In this method we used to do api call to get all the favourite item which is
     * being liked by the user.
     *
     * @param offset The index
     */
    private fun likedPostsApi(offset: Int) {
        var offset = offset
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            val limit = 20
            offset = offset * limit
            val request_datas = JSONObject()
            try {
                request_datas.put("token", mSessionManager!!.authToken)
                request_datas.put("membername", memberName)
                request_datas.put("offset", offset)
                request_datas.put("limit", limit)
                println("$TAG offset=$offset")
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            OkHttp3Connection.doOkHttp3Connection(TAG, ApiUrl.LIKED_POST, OkHttp3Connection.Request_type.POST, request_datas, object : OkHttp3RequestCallback {
                 override fun onSuccess(result: String?, user_tag: String?) {
                    mSwipeRefreshLayout!!.setRefreshing(false)
                    progress_bar_profile!!.visibility = View.GONE
                    println("$TAG fav post res=$result")
                    val likedPostPojo: LikedPostPojoMain
                    val gson = Gson()
                    likedPostPojo = gson.fromJson(result, LikedPostPojoMain::class.java)
                    when (likedPostPojo.code) {
                        "200" -> if (likedPostPojo.data != null && likedPostPojo.data!!.size > 0) {
                            isToCallFirstTime = false
                            rL_noProductFound!!.visibility = View.GONE
                            arrayListLikedPosts!!.addAll(likedPostPojo.data!!)
                            isLoadingRequired = arrayListLikedPosts!!.size > 14
                            likedPostsRvAdap!!.notifyDataSetChanged()

                            // Load more
                            rV_myprofile_fav!!.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                                override  fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                                    super.onScrolled(recyclerView, dx, dy)
                                    val firstVisibleItemPositions = IntArray(2)
                                    totalItemCount = gridLayoutManager!!.getItemCount()
                                    visibleItemCount = gridLayoutManager!!.findLastVisibleItemPositions(firstVisibleItemPositions).get(0)
                                    if (isLoadingRequired && totalItemCount <= visibleItemCount + visibleThreshold) {
                                        isLoadingRequired = false
                                        pageIndex = pageIndex + 1
                                        mSwipeRefreshLayout!!.setRefreshing(true)
                                        likedPostsApi(pageIndex)
                                    }
                                }
                            })

                            // Product item click
                            /*likedPostsRvAdap.setItemClickListener(new ProductItemClickListener() {
                                    @Override
                                    public void onItemClick(int pos, ImageView imageView)
                                    {
                                        clickedItemPosition = pos;
                                        Intent intent = new Intent(mActivity, ProductDetailsActivity.class);
                                        intent.putExtra("productName", arrayListLikedPosts.get(pos).getProductName());
                                        if (arrayListLikedPosts.get(pos).getCategoryData()!=null)
                                            intent.putExtra("category", arrayListLikedPosts.get(pos).getCategoryData().get(0).getCategory());
                                        intent.putExtra("likes", arrayListLikedPosts.get(pos).getLikes());
                                        intent.putExtra("likeStatus", arrayListLikedPosts.get(pos).getLikeStatus());
                                        intent.putExtra("currency", arrayListLikedPosts.get(pos).getCurrency());
                                        intent.putExtra("price", arrayListLikedPosts.get(pos).getPrice());
                                        intent.putExtra("postedOn", arrayListLikedPosts.get(pos).getPostedOn());
                                        intent.putExtra("image",arrayListLikedPosts.get(pos).getMainUrl());
                                        intent.putExtra("thumbnailImageUrl",arrayListLikedPosts.get(pos).getThumbnailImageUrl());
                                        intent.putExtra("likedByUsersArr",arrayListLikedPosts.get(pos).getLikedByUsers());
                                        intent.putExtra("description",arrayListLikedPosts.get(pos).getDescription());
                                        intent.putExtra("condition",arrayListLikedPosts.get(pos).getCondition());
                                        intent.putExtra("place",arrayListLikedPosts.get(pos).getPlace());
                                        intent.putExtra("latitude",arrayListLikedPosts.get(pos).getLatitude());
                                        intent.putExtra("longitude",arrayListLikedPosts.get(pos).getLongitude());
                                        intent.putExtra("postedByUserName",arrayListLikedPosts.get(pos).getPostedByUserName());
                                        intent.putExtra("postId",arrayListLikedPosts.get(pos).getPostId());
                                        intent.putExtra("postsType",arrayListLikedPosts.get(pos).getPostsType());
                                        intent.putExtra("followRequestStatus","");
                                        intent.putExtra("clickCount","");
                                        intent.putExtra("memberProfilePicUrl","");
                                        intent.putExtra(VariableConstants.EXTRA_ANIMAL_IMAGE_TRANSITION_NAME, ViewCompat.getTransitionName(imageView));
                                        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(mActivity, imageView, ViewCompat.getTransitionName(imageView));
                                        startActivityForResult(intent,VariableConstants.PRODUCT_DETAILS_REQ_CODE, options.toBundle());
                                    }
                                });*/
                        }
                        "401" -> {
                            mSwipeRefreshLayout!!.setRefreshing(false)
                            CommonClass.sessionExpired(mActivity!!)
                        }
                        "204" -> {
                            mSwipeRefreshLayout!!.setRefreshing(false)
                            if (arrayListLikedPosts!!.size == 0) rL_noProductFound!!.visibility = View.VISIBLE
                        }
                        else -> {
                            mSwipeRefreshLayout!!.setRefreshing(false)
                            CommonClass.showSnackbarMessage((mActivity as HomePageActivity?)!!.rL_rootElement, likedPostPojo.message)
                        }
                    }
                }

                 override fun onError(error: String?, user_tag: String?) {
                    mSwipeRefreshLayout!!.setRefreshing(false)
                    progress_bar_profile!!.visibility = View.GONE
                    CommonClass.showSnackbarMessage((mActivity as HomePageActivity?)!!.rL_rootElement, error)
                }
            })
        } else CommonClass.showSnackbarMessage((mActivity as HomePageActivity?)!!.rL_rootElement, getResources().getString(R.string.NoInternetAccess))
    }

    @Subscribe
    fun getMessage(likedPostResponseDatas: LikedPostResponseDatas?) {
        if (likedPostResponseDatas != null) {
            if (likedPostResponseDatas.isToAddLikedItem) {
                if (!isContainsId(likedPostResponseDatas.postId)) {
                    arrayListLikedPosts!!.add(0, likedPostResponseDatas)
                    likedPostsRvAdap!!.notifyDataSetChanged()
                }
            } else {
                for (favItemCount in arrayListLikedPosts!!.indices) {
                    if (arrayListLikedPosts!![favItemCount].postId == likedPostResponseDatas.postId) {
                        arrayListLikedPosts!!.removeAt(favItemCount)
                        likedPostsRvAdap!!.notifyDataSetChanged()
                    }
                }
            }
        }

        // show no fav item logo if list is zero or else hide
        if (arrayListLikedPosts!!.size > 0) rL_noProductFound!!.visibility = View.GONE else rL_noProductFound!!.visibility = View.VISIBLE
    }

    /**
     * <h>IsContainsId</h>
     *
     *
     * In this method we used to check whether the given post id is
     * present or not in the current list.
     *
     * @param postId the given post id of product
     * @return the boolean value
     */
    fun isContainsId(postId: String): Boolean {
        var flag = false
        for (`object` in arrayListLikedPosts!!) {
            println(TAG + " " + "given post id=" + postId + " " + "current post id=" + `object`.postId)
            if (postId == `object`.postId) {
                flag = true
            }
        }
        return flag
    }

    override fun onDestroy() {
        instance.unregister(this)
        super.onDestroy()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (data != null) {
            when (requestCode) {
                VariableConstants.PRODUCT_DETAILS_REQ_CODE -> {
                    val likeStatus = data.getStringExtra("likeStatus")
                    if (likeStatus != null && likeStatus != "1") {
                        /*if (arrayListLikedPosts.size()>clickedItemPosition)
                        {
                            arrayListLikedPosts.remove(clickedItemPosition);
                            likedPostsRvAdap.notifyDataSetChanged();
                        }

                        // show no product as favourite if list size is zero
                        if (arrayListLikedPosts.size()>0)
                            rL_noProductFound.setVisibility(View.GONE);
                        else rL_noProductFound.setVisibility(View.VISIBLE);*/
                    }
                }
            }
        }
    }

    companion object {
        private val TAG = FavouriteFrag::class.java.simpleName
        @JvmStatic
        fun newInstance(memberName: String?): FavouriteFrag {
            val bundle = Bundle()
            bundle.putString("memberName", memberName)
            val favouriteFrag = FavouriteFrag()
            favouriteFrag.setArguments(bundle)
            return favouriteFrag
        }
    }
}