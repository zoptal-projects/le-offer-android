package com.zoptal.cellableapp.main.activity

import android.Manifest
import android.app.Activity
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.hardware.Camera
import android.media.AudioManager
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.Surface
import android.view.SurfaceHolder
import android.view.SurfaceView
import android.view.View
import android.widget.*
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import androidx.core.content.ContextCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.adapter.ImagesHorizontalRvAdap
import com.zoptal.cellableapp.fcm_push_notification.Config
import com.zoptal.cellableapp.fcm_push_notification.NotificationMessageDialog
import com.zoptal.cellableapp.fcm_push_notification.NotificationUtils.Companion.clearNotifications
import com.zoptal.cellableapp.main.activity.CameraActivity
import com.zoptal.cellableapp.utility.CapturedImage
import com.zoptal.cellableapp.utility.CommonClass
import com.zoptal.cellableapp.utility.RunTimePermission
import com.zoptal.cellableapp.utility.VariableConstants
import java.io.File
import java.io.FileOutputStream
import java.util.*

/**
 * <h>CameraActivity</h>
 *
 *
 * In this class we used to do the custom camera funtionality on surface view. we
 * have a custom button to capture image. Once we capture image we save the path
 * of image into list from File. In this we have maximum 5 picture to take either
 * by camera or gallery.
 *
 * @since 5/2/2017
 * @author 3Embed
 * @version 1.0
 */
class CameraActivity : AppCompatActivity(), View.OnClickListener, SurfaceHolder.Callback {
    private var camera: Camera? = null
    private var surfaceHolder: SurfaceHolder? = null
    private var jpegCallback: Camera.PictureCallback? = null
    private var mActivity: Activity? = null
    private var permissionsArray: Array<String> = arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
    private var runTimePermission: RunTimePermission? = null
    private var arrayListImgPath: ArrayList<CapturedImage>? = null
    var isToCaptureImage = false
    private var pBar_captureImg: ProgressBar? = null
    private var rL_rootview: RelativeLayout? = null
    var tV_upload: TextView? = null
    private var imagesHorizontalRvAdap: ImagesHorizontalRvAdap? = null
    private var isFlashOn = false
    private var isFromUpdatePost = false
    private var iV_flashIcon: ImageView? = null
    private var param: Camera.Parameters? = null
    private var mNotificationMessageDialog: NotificationMessageDialog? = null
    override fun onCreate(@Nullable savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_camera)
        overridePendingTransition(R.anim.slide_up, R.anim.stay)
        initVariables()
    }

    /**
     * <h>InitVariables</h>
     *
     *
     * In this method we used to initialize all xml variables and data member.
     *
     */
    private fun initVariables() {
        mActivity = this@CameraActivity
        mNotificationMessageDialog = NotificationMessageDialog(mActivity)
        isToCaptureImage = true
        arrayListImgPath = ArrayList()

        // receive datas from last activity
        val intent: Intent = getIntent()
        isFromUpdatePost = intent.getBooleanExtra("isUpdatePost", false)

        // set adpter for horizontal images
        tV_upload = findViewById(R.id.tV_upload) as TextView?
        iV_flashIcon = findViewById(R.id.iV_flashIcon) as ImageView?
        iV_flashIcon!!.setOnClickListener(this)
        imagesHorizontalRvAdap = ImagesHorizontalRvAdap(mActivity!!, arrayListImgPath!!, tV_upload, isToCaptureImage)
        val rV_cameraImages: RecyclerView = findViewById(R.id.rV_cameraImages) as RecyclerView
        val linearLayoutManager = LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false)
        rV_cameraImages.setLayoutManager(linearLayoutManager)
        rV_cameraImages.setAdapter(imagesHorizontalRvAdap)

        // set status bar color
        CommonClass.statusBarColor(mActivity!!)

        runTimePermission = RunTimePermission(mActivity!!, permissionsArray, true)
        pBar_captureImg = findViewById(R.id.pBar_captureImg) as ProgressBar?
        pBar_captureImg!!.visibility = View.GONE
        val surfaceView = findViewById(R.id.surfaceView) as SurfaceView
        val rL_done = findViewById(R.id.rL_done) as RelativeLayout
        rL_done.setOnClickListener(this)
        surfaceHolder = surfaceView.holder
        rL_rootview = findViewById(R.id.rL_rootview) as RelativeLayout?

        // Install a SurfaceHolder.Callback so we get notified when the
        // underlying surface is created and destroyed.
        surfaceHolder!!.addCallback(this)

        // deprecated setting, but required on Android versions prior to 3.0
        surfaceHolder!!.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS)
        jpegCallback = Camera.PictureCallback { data, camera ->
            try {
                camera.takePicture(null, null, pictureCallback)
            } catch (e: Exception) {
                e.printStackTrace()
                isToCaptureImage = true
                pBar_captureImg!!.visibility = View.GONE
                CommonClass.showTopSnackBar(rL_rootview, e.message)
            }
            //saveCapturedImage(data);
        }

        // gallery
        val iV_gallery = findViewById(R.id.iV_gallery) as ImageView
        iV_gallery.setOnClickListener(this)

        // Capture image
        val iV_captureImage = findViewById(R.id.iV_captureImage) as ImageView
        iV_captureImage.setOnClickListener {
            // check captured images are less than 5 or not
            if (arrayListImgPath!!.size < 5) {
                //take the picture
                if (isToCaptureImage) {
                    //   pBar_captureImg.setVisibility(View.VISIBLE);
                    //camera.takePicture(shutterCallback, null, jpegCallback);
                    camera!!.takePicture(null, null, jpegCallback)
                    isToCaptureImage = false
                }
            } else CommonClass.showSnackbarMessage(rL_rootview, getResources().getString(R.string.you_can_select_only_upto))
        }

        // Cancel
        val rL_cancel_btn = findViewById(R.id.rL_cancel_btn) as RelativeLayout
        rL_cancel_btn.setOnClickListener(this)
    }

    override fun onResume() {
        super.onResume()

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.REGISTRATION_COMPLETE))

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.PUSH_NOTIFICATION))

        // clear the notification area when the app is opened
        clearNotifications(getApplicationContext())
    }

    override fun onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!)
        super.onPause()
    }

    // play sound while capturing image
    private val shutterCallback = Camera.ShutterCallback {
        val mgr = getSystemService(Context.AUDIO_SERVICE) as AudioManager
        mgr.playSoundEffect(AudioManager.FLAG_PLAY_SOUND)
    }
    var pictureCallback = Camera.PictureCallback { data, camera -> saveCapturedImage(data) }

    /**
     * <h>saveCapturedImage</h>
     *
     *
     * In this method we used to save the captured image into file.
     *
     * @param data The image data-array
     */
    private fun saveCapturedImage(data: ByteArray) {
        // File imageFile;
        try {
            val folderPath: String
            folderPath = if (Environment.getExternalStorageState() == Environment.MEDIA_MOUNTED) {
                Environment.getExternalStorageDirectory().toString() + "/" + getResources().getString(R.string.app_name)
            } else {
                mActivity!!.filesDir.toString() + "/" + getResources().getString(R.string.app_name)
            }
            val folder = File(folderPath)
            if (!folder.exists() && !folder.isDirectory) {
                folder.mkdirs()
            }
            val name = System.currentTimeMillis().toString() + ".jpg"
            val imageFile = File(folderPath, name)
            try {
                if (!imageFile.exists()) {
                    imageFile.createNewFile()
                }
            } catch (e: Exception) {
                Toast.makeText(getBaseContext(), "Image Not saved", Toast.LENGTH_SHORT).show()
                return
            }
            val file_out = FileOutputStream(imageFile)
            file_out.write(data)
            file_out.close()
            val values = ContentValues()
            values.put(MediaStore.Images.Media.DATE_TAKEN, System.currentTimeMillis())
            values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg")
            values.put(MediaStore.MediaColumns.DATA, imageFile.absolutePath)
            getApplicationContext().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)

            // Code For Captured Image Save in a ImageView.
            val imagePath = folderPath + File.separator + name
            mActivity!!.runOnUiThread {
                pBar_captureImg!!.visibility = View.GONE
                println("$TAG image path=$imagePath")
                val image = CapturedImage()
                image.imagePath = imagePath
                image.rotateAngle = getRoatationAngle(Camera.CameraInfo.CAMERA_FACING_FRONT)
                arrayListImgPath!!.add(image)
                imagesHorizontalRvAdap!!.notifyItemInserted(arrayListImgPath!!.size)
                refreshCamera()
                isToCaptureImage = arrayListImgPath!!.size < 5

                // enable upload button
                if (arrayListImgPath!!.size > 0) tV_upload!!.setTextColor(ContextCompat.getColor(mActivity!!, R.color.pink_color))
            }

            // mCamera.startPreview();
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * <h>RefreshCamera</h>
     *
     *
     * In this method we just refresh the custom camera by
     * using startPreview() method.
     *
     */
    fun refreshCamera() {
        if (surfaceHolder!!.surface == null) {
            // preview surface does not exist
            return
        }

        // stop preview before making changes
        try {
            camera!!.stopPreview()
        } catch (e: Exception) {
            // ignore: tried to stop a non-existent preview
        }

        // set preview size and make any resize, rotate or
        // reformatting changes here
        // start preview with new settings
        try {
            camera!!.setPreviewDisplay(surfaceHolder)
            camera!!.startPreview()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * Get Rotation Angle
     *
     * @param cameraId
     * probably front cam
     * @return angel to rotate
     */
    fun getRoatationAngle(cameraId: Int): Int {
        val info = Camera.CameraInfo()
        Camera.getCameraInfo(cameraId, info)
        val rotation = mActivity!!.windowManager.defaultDisplay.rotation
        var degrees = 0
        when (rotation) {
            Surface.ROTATION_0 -> degrees = 0
            Surface.ROTATION_90 -> degrees = 90
            Surface.ROTATION_180 -> degrees = 180
            Surface.ROTATION_270 -> degrees = 270
        }
        var result: Int
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360
            result = (360 - result) % 360 // compensate the mirror
        } else { // back-facing
            result = (info.orientation - degrees + 360) % 360
        }
        return result
    }

   override fun onBackPressed() {
        finish()
        overridePendingTransition(R.anim.stay, R.anim.slide_down)
    }

    override fun onClick(v: View) {
        val intent: Intent
        when (v.id) {
            R.id.rL_cancel_btn -> onBackPressed()
            R.id.iV_gallery -> {
                intent = Intent(mActivity, GalleryActivity::class.java)
                val galleryImagePaths = ArrayList<String>()
                val rotationAngles = ArrayList<Int>()
                var i = 0
                while (i < arrayListImgPath!!.size) {
                    galleryImagePaths.add(arrayListImgPath!![i].imagePath!!)
                    rotationAngles.add(arrayListImgPath!![i].rotateAngle)
                    i++
                }
                intent.putExtra("arrayListImgPath", galleryImagePaths)
                intent.putExtra("rotationAngles", rotationAngles)
                startActivityForResult(intent, VariableConstants.SELECT_GALLERY_IMG_REQ_CODE)
            }
            R.id.rL_done -> {
                val galleryImagePaths = ArrayList<String>()
                val rotationAngles = ArrayList<Int>()
                var i = 0
                while (i < arrayListImgPath!!.size) {
                    galleryImagePaths.add(arrayListImgPath!![i].imagePath!!)
                    rotationAngles.add(arrayListImgPath!![i].rotateAngle)
                    i++
                }
                if (isFromUpdatePost) {
                    intent = Intent()
                    intent.putExtra("arrayListImgPath", galleryImagePaths)
                    intent.putExtra("rotationAngles", rotationAngles)
                    setResult(VariableConstants.UPDATE_IMAGE_REQ_CODE, intent)
                    onBackPressed()
                } else {
                    if (arrayListImgPath!!.size > 0) {
                        intent = Intent(mActivity, PostProductActivity::class.java)
                        intent.putExtra("arrayListImgPath", galleryImagePaths)
                        intent.putExtra("rotationAngles", rotationAngles)
                        startActivity(intent)
                    }
                }
            }
            R.id.iV_flashIcon -> {
            }
        }
    }

    override fun surfaceCreated(holder: SurfaceHolder) {
        println("$TAG camera=surfaceCreated called...")
        if (runTimePermission!!.checkPermissions(permissionsArray)) {
            openCameraPreview()
        } else {
            runTimePermission!!.requestPermission()
        }
    }

    private fun openCameraPreview() {
        camera = try {
            // open the camera
            Camera.open()
        } catch (e: RuntimeException) {
            // check for exceptions
            System.err.println(e.message)
            return
        }
        param = camera!!.getParameters()

        // modify parameter
        param!!.setPreviewSize(352, 288)
        try {
            camera!!.setParameters(param)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            // The Surface has been created, now tell the camera where to draw
            // the preview.
            camera!!.setPreviewDisplay(surfaceHolder)
            camera!!.startPreview()
        } catch (e: Exception) {
            // check for exceptions
            System.err.println(e.message)
        }
    }

    /*
 * Turning On flash
 */
    private fun turnOnFlash() {
        if (!isFlashOn) {
            if (camera == null || param == null) {
                return
            }
            // play sound
            param!!.flashMode = Camera.Parameters.FLASH_MODE_TORCH
            camera!!.parameters = param
            //camera.startPreview();
            isFlashOn = true

            // changing button/switch image
            toggleButtonImage()
        }
    }

    /*
     * Turning Off flash
     */
    private fun turnOffFlash() {
        if (isFlashOn) {
            if (camera == null || param == null) {
                return
            }
            param!!.flashMode = Camera.Parameters.FLASH_MODE_OFF
            camera!!.parameters = param
            //camera.stopPreview();
            isFlashOn = false

            // changing button/switch image
            toggleButtonImage()
        }
    }

    /*
 * Toggle switch button images changing image states to on / off
 */
    private fun toggleButtonImage() {
        if (isFlashOn) {
            iV_flashIcon!!.setImageResource(R.drawable.flash_light_on)
        } else {
            iV_flashIcon!!.setImageResource(R.drawable.flash_light_off)
        }
    }

    override fun surfaceChanged(holder: SurfaceHolder, format: Int, width: Int, height: Int) {
        if (runTimePermission!!.checkPermissions(permissionsArray)) {
            println("$TAG camera=surfaceChanged called...")
            // Now that the size is known, set up the camera parameters and begin
            // the preview.
            if (camera != null) camera!!.setDisplayOrientation(90)
            refreshCamera()
        }
    }

    override fun surfaceDestroyed(holder: SurfaceHolder) {
        if (runTimePermission!!.checkPermissions(permissionsArray)) {
            println("$TAG camera=surfaceDestroyed called...")
            // stop preview and release camera
            camera!!.stopPreview()
            camera!!.release()
            camera = null
        }
    }

    override  fun onRequestPermissionsResult(requestCode: Int, @NonNull permissions: Array<String?>, @NonNull grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            VariableConstants.PERMISSION_REQUEST_CODE -> {
                println("grant result=" + grantResults.size)
                if (grantResults.size > 0) {
                    var count = 0
                    while (count < grantResults.size) {
                        if (grantResults[count] != PackageManager.PERMISSION_GRANTED) runTimePermission!!.allowPermissionAlert(permissions[count]!!)
                        count++
                    }
                    println("isAllPermissionGranted=" + runTimePermission!!.checkPermissions(permissionsArray))
                    if (runTimePermission!!.checkPermissions(permissionsArray)) {
                        openCameraPreview()
                        camera!!.setDisplayOrientation(90)
                    }
                }
            }
        }
    }

  override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        println("$TAG on activity result called...$requestCode")
        if (data != null) {
            when (requestCode) {
                VariableConstants.SELECT_GALLERY_IMG_REQ_CODE -> if (data.getStringArrayListExtra("arrayListImgPath") != null && data.getStringArrayListExtra("arrayListImgPath").size > 0) {
                    tV_upload!!.setTextColor(ContextCompat.getColor(mActivity!!, R.color.pink_color))
                    arrayListImgPath!!.clear()
                    val imagePathsFromGallery = data.getStringArrayListExtra("arrayListImgPath")
                    var image: CapturedImage
                    var i = 0
                    while (i < imagePathsFromGallery.size) {
                        image = CapturedImage()
                        image.rotateAngle = 0
                        image.imagePath = imagePathsFromGallery[i]
                        arrayListImgPath!!.add(image)
                        i++
                    }
                    //                        arrayListImgPath.addAll(data.getStringArrayListExtra("arrayListImgPath"));
                    imagesHorizontalRvAdap!!.notifyDataSetChanged()
                } else tV_upload!!.setTextColor(ContextCompat.getColor(mActivity!!, R.color.reset_button_bg))
            }
        }
    }

    companion object {
        private val TAG = CameraActivity::class.java.simpleName
    }
}