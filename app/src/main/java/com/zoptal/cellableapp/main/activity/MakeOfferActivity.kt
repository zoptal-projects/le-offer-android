package com.zoptal.cellableapp.main.activity

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.util.Base64
import android.util.Log
import android.view.View
import android.widget.*
import androidx.annotation.Nullable
import androidx.localbroadcastmanager.content.LocalBroadcastManager

import com.squareup.picasso.Picasso
import com.zoptal.cellableapp.mqttchat.Activities.ChatMessageScreen
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.fcm_push_notification.Config
import com.zoptal.cellableapp.fcm_push_notification.NotificationMessageDialog
import com.zoptal.cellableapp.fcm_push_notification.NotificationUtils.Companion.clearNotifications
import com.zoptal.cellableapp.get_current_location.FusedLocationReceiver
import com.zoptal.cellableapp.get_current_location.FusedLocationService
import com.zoptal.cellableapp.main.activity.MakeOfferActivity
import com.zoptal.cellableapp.main.activity.products.ProductDetailsActivity

import com.zoptal.cellableapp.mqttchat.AppController
import com.zoptal.cellableapp.mqttchat.Utilities.MqttEvents
import com.zoptal.cellableapp.mqttchat.Utilities.Utilities
import com.zoptal.cellableapp.utility.*
import com.zoptal.cellableapp.utility.OkHttp3Connection.OkHttp3RequestCallback
import org.json.JSONObject

/**
 * <h>MakeOfferActivity</h>
 *
 *
 * In this class we used to set my own offer.
 *
 * @since 4/29/2017
 */
class MakeOfferActivity : AppCompatActivity(), View.OnClickListener {
    private var progress_bar_save: ProgressBar? = null
    private var mActivity: Activity? = null
    private var locationService: FusedLocationService? = null
    private var permissionsArray = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
    private var runTimePermission: RunTimePermission? = null
    private var latitude: String? = ""
    private var longitude: String? = ""
    private var tV_distance: TextView? = null
    private var tv_currency: TextView? = null
    private var mNotificationMessageDialog: NotificationMessageDialog? = null
    private var parentView: RelativeLayout? = null
    private var sessionManager: SessionManager? = null
    private var postId: String? = ""
    private var member_name: String? = ""
    private var receiverMqttId: String? = ""
    private var tV_price: EditText? = null
    private var makeoffer: RelativeLayout? = null
    private var productName: String? = null
    private var productPicUrl: String? = null
    private val currency: String? = null
    private var price: String? = null
    private var memberPicUrl: String? = null
    private var fromChatScreen: String? = "0"
    private val TAG: String? = MakeOfferActivity::class.java.simpleName
     override fun onCreate(@Nullable savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_make_offer)
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale)
        sessionManager = SessionManager(this)
        initVariable()
    }

    /**
     * <h>InitVariable</h>
     *
     *
     * In this method we used to initialize all variables.
     *
     */
    private fun initVariable() {
        mActivity = this@MakeOfferActivity
        // Receiving datas from last activity
        val intent: Intent = getIntent()
        val data = intent.extras
        val place: String?
        productPicUrl = data!!.getString("productPicUrl")
        productName = data.getString("productName")
        place = data.getString("place")
        postId = data.getString("postId")
        latitude = data.getString("latitude")
        longitude = data.getString("longitude")
        val currency = data.getString("currency")
        price = data.getString("price")
        memberPicUrl = data.getString("memberPicUrl")
        member_name = data.getString("membername")
        receiverMqttId = data.getString("receiverMqttId")
        val negotiable = data.getString("negotiable")
        fromChatScreen = "0"
        fromChatScreen = if (intent.hasExtra("fromChatScreen")) {
            intent.getStringExtra("fromChatScreen")
        } else {
            "0"
        }
        mNotificationMessageDialog = NotificationMessageDialog(mActivity)
        CommonClass.statusBarColor(mActivity!!)
        // get current location
        tV_distance = findViewById(R.id.tV_distance) as TextView?

        runTimePermission = RunTimePermission(mActivity!!, permissionsArray, false)
        if (runTimePermission!!.checkPermissions(permissionsArray)) {
            currentLocation
        } else {
            runTimePermission!!.requestPermission()
        }
        val rL_back_btn = findViewById(R.id.rL_back_btn) as RelativeLayout?
        rL_back_btn!!.setOnClickListener(this)

        // xml Variables
        val iV_productImage = findViewById(R.id.iV_productImage) as ImageView?
        val tV_productname: TextView?
        val tV_location: TextView?
        val tV_asking_price: TextView?
        tV_productname = findViewById(R.id.tV_productname) as TextView?
        tV_location = findViewById(R.id.tV_location) as TextView?
        tV_asking_price = findViewById(R.id.tV_asking_price) as TextView?
        tV_price = findViewById(R.id.tV_price) as EditText?
        tv_currency = findViewById(R.id.tv_currency) as TextView?
        progress_bar_save = findViewById(R.id.progress_bar_save) as ProgressBar?
        makeoffer = findViewById(R.id.make_offer) as RelativeLayout?
        makeoffer!!.setOnClickListener(this)
        parentView = findViewById(R.id.poarentView) as RelativeLayout?
        // set product image
        iV_productImage!!.layoutParams.width = CommonClass.getDeviceWidth(mActivity!!) / 5
        iV_productImage.layoutParams.height = CommonClass.getDeviceWidth(mActivity!!) / 5
        if (productPicUrl != null && !productPicUrl!!.isEmpty()) Picasso.with(mActivity)
                .load(productPicUrl)
                .placeholder(R.drawable.default_image)
                .error(R.drawable.default_image)
                .into(iV_productImage)

        // product name
        if (productName != null) tV_productname!!.text = productName

        // location
        if (place != null) tV_location!!.text = place

        // set asking price
        if (price != null) {
            val askingPrice: String
            askingPrice = if (currency != null && !currency.isEmpty()) {
                getResources().getString(R.string.asking_price).toString() + " " + currency + " " + price
            } else {
                getResources().getString(R.string.asking_price).toString() + " " + price
            }
            tV_asking_price!!.text = askingPrice
            if (negotiable != null && negotiable == "1") {
                tV_price!!.isEnabled = true
            } else {
                tV_price!!.isEnabled = false
            }
            if (currency != null && !currency.isEmpty()) {
                tv_currency!!.text = currency
            }
            tV_price!!.setText(price)
            price = "$currency $price"
        }
    }

    override fun onResume() {
        super.onResume()

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.REGISTRATION_COMPLETE))

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.PUSH_NOTIFICATION))

        // clear the notification area when the app is opened
        clearNotifications(getApplicationContext())
    }

   override fun onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!)
        super.onPause()
    }

    /**
     * <h>GetDistance</h>
     *
     *
     * In this method we used to find the distance of the product from my current location.
     *
     * @param lat1 The cureent lat
     * @param lon1 The current lng
     * @param lat2 The product lat
     * @param lon2 The product lng
     * @return The double value of distance
     */
    private fun getDistance(lat1: Double, lon1: Double, lat2: Double, lon2: Double): Double {
        /* double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1))
                * Math.sin(deg2rad(lat2))
                + Math.cos(deg2rad(lat1))
                * Math.cos(deg2rad(lat2))
                * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        return (dist);*/

        // The math module contains a function
        // named toRadians which converts from
        // degrees to radians.
        var lat1 = lat1
        var lon1 = lon1
        var lat2 = lat2
        var lon2 = lon2
        lon1 = Math.toRadians(lon1)
        lon2 = Math.toRadians(lon2)
        lat1 = Math.toRadians(lat1)
        lat2 = Math.toRadians(lat2)

        // Haversine formula
        val dlon = lon2 - lon1
        val dlat = lat2 - lat1
        val a = (Math.pow(Math.sin(dlat / 2), 2.0)
                + (Math.cos(lat1) * Math.cos(lat2)
                * Math.pow(Math.sin(dlon / 2), 2.0)))
        val c = 2 * Math.asin(Math.sqrt(a))

        // Radius of earth in kilometers. Use 3956
        // for miles
        val r = 6371.0

        // calculate the result
        return c * r
    }

    /**
     * In this method we find current location using FusedLocationApi.
     * in this we have onUpdateLocation() method in which we check if
     * its not null then We call guest user api.
     */
    private val currentLocation: Unit
        private get() {
            locationService = FusedLocationService(mActivity!!, object : FusedLocationReceiver {
                override fun onUpdateLocation() {
                    val currentLocation = locationService!!.receiveLocation()
                    if (currentLocation != null) {
                        val lat: String
                        val lng: String
                        lat = currentLocation.latitude.toString()
                        lng = currentLocation.longitude.toString()
                        if (isLocationFound(lat, lng) && isLocationFound(latitude, longitude)) {
                            val givenLat = latitude!!.toDouble()
                            val givenLong = longitude!!.toDouble()
                            val distance = getDistance(currentLocation.latitude, currentLocation.longitude, givenLat, givenLong)
                            val setDistance = CommonClass.twoDigitAfterDewcimal(distance.toString()) + " " + getResources().getString(R.string.km) + " " + getResources().getString(R.string.away)
                            tV_distance!!.text = setDistance
                        }
                    }
                }
            }
            )
        }

    /**
     * In this method we used to check whether current lat and
     * long has been received or not.
     * @param lat The current latitude
     * @param lng The current longitude
     * @return boolean flag true or false
     */
    private fun isLocationFound(lat: String?, lng: String?): Boolean {
        return !(lat == null || lat.isEmpty()) && !(lng == null || lng.isEmpty())
    }

    private fun deg2rad(deg: Double): Double {
        return deg * Math.PI / 180.0
    }

    private fun rad2deg(rad: Double): Double {
        return rad * 180.0 / Math.PI
    }

 override fun onBackPressed() {
        finish()
        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.rL_back_btn -> onBackPressed()
            R.id.make_offer -> makeOffer()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions!!, grantResults!!)
        when (requestCode) {
            VariableConstants.PERMISSION_REQUEST_CODE -> {
                println("grant result=" + grantResults!!.size)
                if (grantResults.size > 0) {
                    var count = 0
                    while (count < grantResults.size) {
                        if (grantResults[count] != PackageManager.PERMISSION_GRANTED) runTimePermission!!.allowPermissionAlert(permissions!![count])
                        count++
                    }
                    println("isAllPermissionGranted=" + runTimePermission!!.checkPermissions(permissionsArray))
                    if (runTimePermission!!.checkPermissions(permissionsArray)) {
                        currentLocation
                    }
                }
            }
        }
    }

    /*
     *Make offer api */
    private var requestDats: JSONObject? = null
    private fun makeOffer() {
        if (receiverMqttId == null || receiverMqttId!!.isEmpty()) {
            Toast.makeText(this, R.string.mqtt_user_not_text, Toast.LENGTH_SHORT).show()
            return
        }
        val digivalue: String
        var priceValue = tV_price!!.text.toString()
        if (!priceValue.isEmpty()) {
            if (priceValue.toDouble() > 0) {
                priceValue = priceValue.replace("[^a-zA-Z0-9 .,]|(?<!\\d)[.,]|[.,](?!\\d)".toRegex(), "")
                Log.d("amountvalue", "" + priceValue)
                //priceValue = priceValue.replaceAll("\\D+","");
                digivalue = priceValue.trim { it <= ' ' }
                if (digivalue.length < 1) {
                    return
                }
            } else {
                Toast.makeText(this, R.string.price_should_greater_than, Toast.LENGTH_SHORT).show()
                digivalue = priceValue.trim { it <= ' ' }
                return
            }
        } else {
            return
        }
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            handelButton(true)
            requestDats = JSONObject()
            try {
                requestDats!!.put("token", sessionManager!!.authToken)
                requestDats!!.put("offerStatus", "1")
                requestDats!!.put("postId", postId)
                requestDats!!.put("price", digivalue)
                requestDats!!.put("type", "0")
                requestDats!!.put("membername", member_name)
                requestDats!!.put("sendchat", createMessageObject(digivalue))
            } catch (e: Exception) {
                e.printStackTrace()
                handelButton(false)
            }
            OkHttp3Connection.doOkHttp3Connection("", ApiUrl.MAKE_OFFER, OkHttp3Connection.Request_type.POST, requestDats!!, object : OkHttp3RequestCallback {
                override fun onSuccess(result: String?, user_tag: String?) {
                    handelButton(false)
                    try {
                        handelResponse(result)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

                override fun onError(error: String?, user_tag: String?) {
                    handelButton(false)
                    CommonClass.showSnackbarMessage(parentView, error)
                }
            })
        } else CommonClass.showSnackbarMessage(parentView, getResources().getString(R.string.NoInternetAccess))
    }

    /*
    * Handling the response data.*/
    @Throws(Exception::class)
    private fun handelResponse(resaponse: String?) {
        val jsonObject = JSONObject(resaponse)
        val code_Data = jsonObject.getString("code")
        when (code_Data) {
            "200" -> {

                AppController.instance?.sendMessageToFcm(MqttEvents.OfferMessage.toString() + "/" + requestDats!!.getJSONObject("sendchat").getString("to"), requestDats!!.getJSONObject("sendchat"))
                val intent: Intent?
                intent = Intent(this, ChatMessageScreen::class.java)
                intent!!.putExtra("isNew", isNew)
                intent.putExtra("receiverUid", receiverMqttId)
                intent.putExtra("receiverName", member_name)
                intent.putExtra("documentId", doucumentId)
                if(AppController.instance !=null) {
                    intent.putExtra("receiverIdentifier", AppController.instance?.userIdentifier)
                }
                intent.putExtra("receiverImage", memberPicUrl)
                if(AppController.instance !=null) {
                    intent.putExtra("colorCode", AppController.instance?.getColorCode(1 % 19))
                }
                intent.putExtra("isFromOfferPage", true)
                if (fromChatScreen == "0") {
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    startActivity(intent)
                } else {
                    ProductDetailsActivity.productDetailsActivity!!.finish()
                }
                mActivity!!.finish()
            }
            "401" -> CommonClass.sessionExpired(mActivity!!)
            "409" -> CommonClass.showTopSnackBar(parentView, getString(R.string.already_sold_Text))
            else -> {
            }
        }
    }

    /*
    * Handel make offer button data.*/
    private fun handelButton(isEnable: Boolean) {
        if (isEnable) {
            progress_bar_save!!.visibility = View.VISIBLE
            makeoffer!!.isEnabled = false
        } else {
            makeoffer!!.isEnabled = true
            progress_bar_save!!.visibility = View.GONE
        }
    }

    /*
     *creating the msg object */
    private var doucumentId: String? = null
    private var isNew = false

    @Throws(Exception::class)
    private fun createMessageObject(amount: String?): JSONObject? {
        try {
            if (AppController.instance != null) {
                doucumentId = AppController.instance?.findDocumentIdOfReceiver(receiverMqttId, postId)
                if (doucumentId!!.isEmpty()) {
                    isNew = true
                    doucumentId = AppController.findDocumentIdOfReceiver(receiverMqttId!!, Utilities.tsInGmt(), member_name, memberPicUrl, postId!!, false, "", "", productPicUrl, productName, price, false, false)
                } else {
                    isNew = false
                    AppController.instance?.dbController?.updateChatDetails(doucumentId, member_name, memberPicUrl)
                }
            }
            Log.d("log88", "" + amount)
        }catch (e:Exception){e.printStackTrace()}
            val byteArray = amount!!.toByteArray(charset("UTF-8"))
            val messageInbase64 = Base64.encodeToString(byteArray, Base64.DEFAULT).trim { it <= ' ' }
            val tsForServer = Utilities.tsInGmt()
            val tsForServerEpoch = Utilities().gmtToEpoch(tsForServer)

        val messageData = JSONObject()
        messageData.put("name", sessionManager!!.userName)
        messageData.put("from", sessionManager!!.getmqttId())
        messageData.put("to", receiverMqttId)
        messageData.put("payload", messageInbase64)
        messageData.put("type", "15")
        messageData.put("offerType", "1")
        messageData.put("id", tsForServerEpoch)
        messageData.put("secretId", postId)
        messageData.put("thumbnail", "")
        messageData.put("userImage", sessionManager!!.userImage)
        messageData.put("toDocId", doucumentId)
        messageData.put("dataSize", 1)
        messageData.put("isSold", "0")
        messageData.put("productImage", productPicUrl)
        messageData.put("productId", postId)
        messageData.put("productName", productName)
        messageData.put("productPrice", "" + price)
        return messageData
    }
}