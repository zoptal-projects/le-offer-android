package com.zoptal.cellableapp.main.activity

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Environment
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.annotation.NonNull
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import co.simplecrop.android.simplecropimage.CropImage
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.device_camera.HandleCameraEvents
import com.zoptal.cellableapp.event_bus.BusProvider.instance
import com.zoptal.cellableapp.fcm_push_notification.Config
import com.zoptal.cellableapp.fcm_push_notification.NotificationMessageDialog
import com.zoptal.cellableapp.fcm_push_notification.NotificationUtils.Companion.clearNotifications
import com.zoptal.cellableapp.main.activity.ChangeLocationActivity
import com.zoptal.cellableapp.main.activity.EditProfileActivity
import com.zoptal.cellableapp.pojo_class.CloudData
import com.zoptal.cellableapp.pojo_class.EditProfilePojo
import com.zoptal.cellableapp.pojo_class.cloudinary_details_pojo.Cloudinary_Details_reponse
import com.zoptal.cellableapp.pojo_class.profile_pojo.ProfileResultDatas
import com.zoptal.cellableapp.utility.*
import com.zoptal.cellableapp.utility.OkHttp3Connection.OkHttp3RequestCallback
import org.json.JSONException
import org.json.JSONObject
import java.io.File
import java.io.FileOutputStream

/**
 * <h>EditProfileActivity</h>
 *
 *
 * This class is called from MyProfileFrag class. In this class firstly we do
 * api call to get user complete information. We have all the field editable
 * to mobify last datas with new one.
 *
 * @since 4/14/2017
 */
class EditProfileActivity : Activity(), View.OnClickListener {
    private var mActivity: Activity? = null
    private var eT_userName: EditText? = null
    private var eT_fullName: EditText? = null
    private var eT_website: EditText? = null
    private var eT_bio: EditText? = null
    private var mHandleCameraEvents: HandleCameraEvents? = null
    private var iV_profile_pic: ImageView? = null
    private var iv_edit_icon: ImageView? = null
    private var profilePicUrl: String? = ""
    private var mSessionManager: SessionManager? = null
    private var linear_rootElement: RelativeLayout? = null
    private var rL_pBar: RelativeLayout? = null
    private var tV_save: TextView? = null
    private var tV_gender: TextView? = null
    private var tV_mobileNo: TextView? = null
    private var tV_location: TextView? = null
    private var tV_emailId: TextView? = null
    private var mDialogBox: DialogBox? = null
    private var scrollView_editProfile: ScrollView? = null
    private var pBar_editProfile: ProgressBar? = null
    private var isToStartActivity = false

    // run time permission
    private var runTimePermission: RunTimePermission? = null
    private var permissionsArray: Array<String> = arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
    private var isPictureTaken = false
    private var mFile: File? = null
    private var mNotificationMessageDialog: NotificationMessageDialog? = null
    override fun onCreate(savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_edit_profile)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale)
        instance.register(this)
        initVariables()
    }

    /**
     * <h>InitVariables</h>
     *
     *
     * In this method we used to initialize all the xml variables.
     *
     */
    private fun initVariables() {
        isPictureTaken = false
        isToStartActivity = true
        mActivity = this@EditProfileActivity
        mNotificationMessageDialog = NotificationMessageDialog(mActivity)
        editProfileInstance = this@EditProfileActivity
        mDialogBox = DialogBox(mActivity!!)
        CommonClass.statusBarColor(mActivity!!)

        // progress bar
        rL_pBar = findViewById<View>(R.id.rL_pBar) as RelativeLayout
        rL_pBar!!.visibility = View.GONE

        // gender
        tV_gender = findViewById<View>(R.id.tV_gender) as TextView
        val rL_select_gender = findViewById<View>(R.id.rL_select_gender) as RelativeLayout
        rL_select_gender.setOnClickListener(this)
        mSessionManager = SessionManager(mActivity!!)
        iV_profile_pic = findViewById<View>(R.id.iV_profile_pic) as ImageView
        iv_edit_icon = findViewById<View>(R.id.iv_edit_icon) as ImageView
        scrollView_editProfile = findViewById<View>(R.id.scrollView_editProfile) as ScrollView
        scrollView_editProfile!!.visibility = View.GONE

        runTimePermission = RunTimePermission(mActivity!!, permissionsArray, false)
        pBar_editProfile = findViewById<View>(R.id.pBar_editProfile) as ProgressBar
        val rL_back_btn = findViewById<View>(R.id.rL_back_btn) as RelativeLayout
        rL_back_btn.setOnClickListener(this)
        linear_rootElement = findViewById<View>(R.id.linear_rootElement) as RelativeLayout
        iV_profile_pic!!.setOnClickListener(this)
        iV_profile_pic!!.layoutParams.width = CommonClass.getDeviceWidth(mActivity!!) / 4
        iV_profile_pic!!.layoutParams.height = CommonClass.getDeviceWidth(mActivity!!) / 4

        // User name
        eT_userName = findViewById<View>(R.id.eT_userName) as EditText

        //..While user enter space then remove
        eT_userName!!.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                if (eT_userName!!.text.toString().contains(" ")) {
                    eT_userName!!.setText(eT_userName!!.text.toString().replace(" ", ""))
                    eT_userName!!.setSelection(eT_userName!!.text.toString().length)
                }
            }

            override fun afterTextChanged(editable: Editable) {}
        })

        // Full name
        eT_fullName = findViewById<View>(R.id.eT_fullName) as EditText

        // Email
        tV_emailId = findViewById<View>(R.id.tV_emailId) as TextView
        tV_emailId!!.setOnClickListener(this)

        // Mobile number
        tV_mobileNo = findViewById<View>(R.id.tV_mobileNo) as TextView
        tV_mobileNo!!.setOnClickListener(this)

        // website
        eT_website = findViewById<View>(R.id.eT_website) as EditText

        // location
        tV_location = findViewById<View>(R.id.tV_location) as TextView
        tV_location!!.setOnClickListener(this)

        // Bio
        eT_bio = findViewById<View>(R.id.eT_bio) as EditText

        // save
        tV_save = findViewById<View>(R.id.tV_save) as TextView
        tV_save!!.setOnClickListener(this)
        val state = Environment.getExternalStorageState()
        mFile = if (Environment.MEDIA_MOUNTED == state) {
            File(Environment.getExternalStorageDirectory(), VariableConstants.TEMP_PHOTO_FILE_NAME)
        } else {
            File(filesDir, VariableConstants.TEMP_PHOTO_FILE_NAME)
        }
        mHandleCameraEvents = HandleCameraEvents(mActivity!!, mFile!!)
        profileDatasApi
    }

    override fun onResume() {
        super.onResume()
        isToStartActivity = true

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.REGISTRATION_COMPLETE))

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.PUSH_NOTIFICATION))

        // clear the notification area when the app is opened
        clearNotifications(applicationContext)
    }

    override fun onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!)
        super.onPause()
    }// Set user profile pic

    // user name

    // Full name

    // website

    // bio

    // Email-Id

    // phone number

    // address

    // gender

    /**
     * <h>GetProfileDatasApi</h>
     *
     *
     * In this method we used to do api call to get user complete information.
     * Once we get information then set all the field to the xml variables.
     *
     */
    private val profileDatasApi: Unit
        private get() {
            if (CommonClass.isNetworkAvailable(mActivity!!)) {
                pBar_editProfile!!.visibility = View.VISIBLE
                scrollView_editProfile!!.visibility = View.GONE
                val request_datas = JSONObject()
                try {
                    request_datas.put("token", mSessionManager!!.authToken)
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
                OkHttp3Connection.doOkHttp3Connection(TAG, ApiUrl.EDIT_PROFILE, OkHttp3Connection.Request_type.POST, request_datas, object : OkHttp3RequestCallback {
                     override fun onSuccess(result: String?, user_tag: String?) {
                        println("$TAG edit profile res=$result")
                        val editProfilePojo: EditProfilePojo
                        val gson = Gson()
                        editProfilePojo = gson.fromJson(result, EditProfilePojo::class.java)
                        when (editProfilePojo.code) {
                            "200" -> {
                                pBar_editProfile!!.visibility = View.GONE
                                scrollView_editProfile!!.visibility = View.VISIBLE
                                val editProfileData = editProfilePojo.data
                                if (editProfileData != null) {
                                    val fullName: String?
                                    val username: String?
                                    val websiteUrl: String?
                                    val bio: String?
                                    val email: String?
                                    val phoneNumber: String?
                                    val gender: String?
                                    val place: String?
                                    fullName = editProfileData.fullName
                                    profilePicUrl = editProfileData.profilePicUrl
                                    username = editProfileData.username
                                    websiteUrl = editProfileData.websiteUrl
                                    bio = editProfileData.bio
                                    email = editProfileData.email
                                    phoneNumber = editProfileData.phoneNumber
                                    gender = editProfileData.gender
                                    place = editProfileData.place
                                    mSessionManager!!.userImage = profilePicUrl

                                    // Set user profile pic
                                    if (profilePicUrl != null && !profilePicUrl!!.isEmpty()) {
                                        isPictureTaken = false
                                        iv_edit_icon!!.visibility = View.VISIBLE
                                        Picasso.with(mActivity)
                                                .load(profilePicUrl)
                                                .transform(CircleTransform())
                                                .placeholder(R.drawable.default_profile_image)
                                                .error(R.drawable.default_profile_image)
                                                .into(iV_profile_pic)
                                    } else iv_edit_icon!!.visibility = View.GONE

                                    // user name
                                    if (username != null && !username.isEmpty()) eT_userName!!.setText(username)

                                    // Full name
                                    if (fullName != null && !fullName.isEmpty()) eT_fullName!!.setText(fullName)

                                    // website
                                    if (websiteUrl != null && !websiteUrl.isEmpty()) eT_website!!.setText(websiteUrl)

                                    // bio
                                    if (bio != null && !bio.isEmpty()) eT_bio!!.setText(bio)

                                    // Email-Id
                                    if (email != null && !email.isEmpty()) tV_emailId!!.text = email

                                    // phone number
                                    if (phoneNumber != null && !phoneNumber.isEmpty()) tV_mobileNo!!.text = phoneNumber

                                    // address
                                    if (place != null && !place.isEmpty()) tV_location!!.text = place

                                    // gender
                                    if (gender != null && !gender.isEmpty()) tV_gender!!.text = gender
                                }
                            }
                            "401" -> CommonClass.sessionExpired(mActivity!!)
                            else -> CommonClass.showSnackbarMessage(linear_rootElement, editProfilePojo.message)
                        }
                    }

                     override fun onError(error: String?, user_tag: String?) {
                        pBar_editProfile!!.visibility = View.GONE
                    }
                })
            } else CommonClass.showSnackbarMessage(linear_rootElement, resources.getString(R.string.NoInternetAccess))
        }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        try {
            super.onActivityResult(requestCode, resultCode, data)
            if (resultCode != RESULT_OK) return

            when (requestCode) {
                VariableConstants.SELECT_GALLERY_IMG_REQ_CODE -> try {
                    val inputStream = contentResolver.openInputStream(data?.data!!)
                    val fileOutputStream = FileOutputStream(mFile)
                    mHandleCameraEvents!!.copyStream(inputStream!!, fileOutputStream)
                    fileOutputStream.close()
                    assert(inputStream != null)
                    inputStream.close()
                    mHandleCameraEvents!!.startCropImage()
                } catch (e: Exception) {
                    Log.e(TAG, "Error while creating temp file", e)
                }
                VariableConstants.CAMERA_CAPTURE -> mHandleCameraEvents!!.startCropImage()
                VariableConstants.PIC_CROP -> {
                    if (data == null) {
                        return
                    }
                    val path = data.getStringExtra(CropImage.IMAGE_PATH) ?: return
                    isPictureTaken = true
                    iv_edit_icon!!.visibility = View.VISIBLE
                    val bitmap = BitmapFactory.decodeFile(mFile!!.path)
                    iV_profile_pic!!.setImageBitmap(mHandleCameraEvents!!.getCircleBitmap(bitmap))
                }
                VariableConstants.CHANGE_LOC_REQ_CODE -> if (data != null) {
                    println("$TAG data=$data")
                    val userLat = data.getStringExtra("lat")
                    val userLng = data.getStringExtra("lng")
                    val address = data.getStringExtra("address")
                    tV_location!!.text = address
                    println("$TAG lat=$userLat lng=$userLng address=$address")
                }
                VariableConstants.VERIFY_EMAIL_REQ_CODE -> if (data != null) {
                    val email = data.getStringExtra("emailId")
                    if (email != null && !email.isEmpty()) tV_emailId!!.text = email
                }
            }
        }catch (e:Exception){e.printStackTrace()}
    }

    override fun onClick(v: View) {
        val intent: Intent
        when (v.id) {
            R.id.rL_back_btn -> onBackPressed()
            R.id.iV_profile_pic -> if (runTimePermission!!.checkPermissions(permissionsArray)) chooseImage() else runTimePermission!!.requestPermission()
            R.id.rL_select_gender -> mDialogBox!!.selectGender(tV_gender!!)
            R.id.tV_mobileNo -> if (isToStartActivity) {
                intent = Intent(mActivity, VerifyPhoneNoActivity::class.java)
                startActivity(intent)
                isToStartActivity = false
            }
            R.id.tV_emailId -> if (isToStartActivity) {
                intent = Intent(mActivity, VerifyEmailIdActivity::class.java)
                startActivityForResult(intent, VariableConstants.VERIFY_EMAIL_REQ_CODE)
                isToStartActivity = false
            }
            R.id.tV_location -> if (isToStartActivity) {
                intent = Intent(mActivity, ChangeLocationActivity::class.java)
                startActivityForResult(intent, VariableConstants.CHANGE_LOC_REQ_CODE)
                isToStartActivity = false
            }
            R.id.tV_save -> {
                if (eT_userName!!.text.toString().isEmpty()) {
                    CommonClass.showSnackbarMessage(linear_rootElement, getString(R.string.empty_username))
                    return
                }
                println("$TAG isPictureTaken=$isPictureTaken profilePicUrl=$profilePicUrl")
                if (isPictureTaken) {
                    rL_pBar!!.visibility = View.VISIBLE
                    tV_save!!.visibility = View.GONE
                    cloudinaryDetailsApi
                } else {
                    rL_pBar!!.visibility = View.VISIBLE
                    tV_save!!.visibility = View.GONE
                    updateProfileApi()
                }
            }
        }
    }

    /**
     * <h>SelectImage</h>
     *
     *
     * Using this method we open a pop-up. when
     * user click on profile image. it contains
     * thee field Take Photo,Choose from Gallery,
     * Cancel.
     *
     */
    fun chooseImage() {
        val selectImgDialog = Dialog(mActivity!!)
        selectImgDialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        mActivity!!.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        selectImgDialog.setContentView(R.layout.select_image_layout)

        // Take picture
        val rL_take_pic = selectImgDialog.findViewById<View>(R.id.rL_take_pic) as RelativeLayout
        rL_take_pic.setOnClickListener {
            selectImgDialog.dismiss()
            mHandleCameraEvents!!.takePicture()
        }

        // Choose Image from gallery
        val rL_select_pic = selectImgDialog.findViewById<View>(R.id.rL_select_pic) as RelativeLayout
        rL_select_pic.setOnClickListener {
            selectImgDialog.dismiss()
            mHandleCameraEvents!!.openGallery()
        }

        // Remove profile pic
        val rL_remove_pic = selectImgDialog.findViewById<View>(R.id.rL_remove_pic) as RelativeLayout
        if (profilePicUrl!!.isEmpty()) {
            iv_edit_icon!!.visibility = View.GONE
            rL_remove_pic.visibility = View.GONE
        } else {
            iv_edit_icon!!.visibility = View.VISIBLE
            rL_remove_pic.visibility = View.VISIBLE
        }
        rL_remove_pic.setOnClickListener {
            removeProfileImage()
            selectImgDialog.dismiss()
        }
        selectImgDialog.show()
    }

    private fun removeProfileImage() {
        profilePicUrl = ""
        isPictureTaken = false
        iv_edit_icon!!.visibility = View.GONE
        iV_profile_pic!!.setImageResource(R.drawable.default_profile_image)
    }

    /**
     * <h2>getCloud_details</h2>
     *
     *
     * Collecting the cloudinary details from the server..
     *
     */
    private val cloudinaryDetailsApi: Unit
        private get() {
            if (CommonClass.isNetworkAvailable(mActivity!!)) {
                val request_data = JSONObject()
                OkHttp3Connection.doOkHttp3Connection("", ApiUrl.GET_CLOUDINARY_DETAILS, OkHttp3Connection.Request_type.POST, request_data, object : OkHttp3RequestCallback {
                    override fun onSuccess(result: String?, user_tag: String?) {
                        val response = Gson().fromJson(result, Cloudinary_Details_reponse::class.java)
                        if (response.code == "200") {
                            val cloudName: String
                            val timestamp: String
                            val apiKey: String
                            val signature: String
                            cloudName = response.response!!.cloudName!!
                            timestamp = response.response!!.timestamp!!
                            apiKey = response.response!!.apiKey!!
                            signature = response.response!!.signature!!
                            val remaining_data = Bundle()
                            remaining_data.putString("signature", signature)
                            remaining_data.putString("timestamp", timestamp)
                            remaining_data.putString("apiKey", apiKey)
                            remaining_data.putString("cloudName", cloudName)
                            postMedia(remaining_data)
                        } else {
                            CommonClass.showSnackbarMessage(linear_rootElement, response.message)
                        }
                    }

                    override fun onError(error: String?, tag: String?) {
                        CommonClass.showSnackbarMessage(linear_rootElement, error)
                    }
                })
            } else CommonClass.showSnackbarMessage(linear_rootElement, resources.getString(R.string.NoInternetAccess))
        }

    /**
     * <h>PostMedia</h>
     *
     *
     * In this method we are uploading file(Image or video) to the cloudinary server.
     * in Respose we will get Url. Once we get Url of the image then call registration
     * api.
     *
     *
     * @param remaining_data The bundle of cloudinary details data
     */
    private fun postMedia(remaining_data: Bundle) {
        /*
         * Creating the path holder.*/
        val cloudData = CloudData()
        cloudData.path = mFile!!.absolutePath
        cloudData.isVideo = false
        object : UploadToCloudinary(remaining_data, cloudData) {
            override fun callBack(resultData: Map<*, *>?) {
                if (resultData != null) {
                    val main_url = resultData["url"] as String?
                    println("$TAG main url=$main_url")
                    profilePicUrl = main_url
                    updateProfileApi()
                }
            }

            override fun errorCallBack(error: String?) {}
        }
    }

    /**
     * <h>UpdateProfileApi</h>
     *
     *
     * In this method we used to send the modified datas to the server.
     *
     */
    private fun updateProfileApi() {
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            val request_datas = JSONObject()
            try {
                request_datas.put("token", mSessionManager!!.authToken)
                request_datas.put("username", eT_userName!!.text.toString())
                request_datas.put("fullName", eT_fullName!!.text.toString())
                request_datas.put("bio", eT_bio!!.text.toString())
                request_datas.put("website", eT_website!!.text.toString())
                request_datas.put("gender", tV_gender!!.text.toString())
                request_datas.put("profilePicUrl", profilePicUrl)
                request_datas.put("location", tV_location!!.text.toString())
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            OkHttp3Connection.doOkHttp3Connection(TAG, ApiUrl.SAVE_PROFILE, OkHttp3Connection.Request_type.POST, request_datas, object : OkHttp3RequestCallback {
                 override fun onSuccess(result: String?, user_tag: String?) {
                    println("$TAG update profile res=$result")
                    val editProfilePojo: EditProfilePojo
                    val gson = Gson()
                    editProfilePojo = gson.fromJson(result, EditProfilePojo::class.java)
                    when (editProfilePojo.code) {
                        "200" -> {
                            // save user name and user profile pic into session manager class
                            mSessionManager!!.userImage = profilePicUrl
                            mSessionManager!!.userName = eT_userName!!.text.toString()

                            // set datas to notify the updated datas in profile screen
                            val profileResultDatas = ProfileResultDatas()
                            profileResultDatas.profilePicUrl = profilePicUrl!!
                            profileResultDatas.username = eT_userName!!.text.toString()
                            profileResultDatas.fullName = eT_fullName!!.text.toString()
                            profileResultDatas.bio = eT_bio!!.text.toString()
                            profileResultDatas.website = eT_website!!.text.toString()
                            instance.post(profileResultDatas)
                            onBackPressed()
                        }
                        "401" -> CommonClass.sessionExpired(mActivity!!)
                        else -> {
                            rL_pBar!!.visibility = View.GONE
                            tV_save!!.visibility = View.VISIBLE
                            tV_save!!.visibility = View.VISIBLE
                            CommonClass.showSnackbarMessage(linear_rootElement, editProfilePojo.message)
                        }
                    }
                }

                 override fun onError(error: String?, user_tag: String?) {
                    rL_pBar!!.visibility = View.GONE
                    tV_save!!.visibility = View.VISIBLE
                    CommonClass.showSnackbarMessage(linear_rootElement, error)
                }
            })
        } else CommonClass.showSnackbarMessage(linear_rootElement, resources.getString(R.string.NoInternetAccess))
    }

    override fun onBackPressed() {
        finish()
        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate)
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        if (imm != null && currentFocus != null) imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
        return true
    }

    override fun onDestroy() {
        super.onDestroy()
        instance.unregister(this)
    }

    override fun onRequestPermissionsResult(requestCode: Int, @NonNull permissions: Array<String>, @NonNull grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            VariableConstants.PERMISSION_REQUEST_CODE -> {
                println("grant result=" + grantResults.size)
                if (grantResults.size > 0) {
                    var count = 0
                    while (count < grantResults.size) {
                        if (grantResults[count] != PackageManager.PERMISSION_GRANTED) runTimePermission!!.allowPermissionAlert(permissions[count])
                        count++
                    }
                    println("isAllPermissionGranted=" + runTimePermission!!.checkPermissions(permissionsArray))
                    if (runTimePermission!!.checkPermissions(permissionsArray)) {
                        chooseImage()
                    }
                }
            }
        }
    }

    companion object {
        private val TAG = EditProfileActivity::class.java.simpleName
        @JvmField
        var editProfileInstance: EditProfileActivity? = null
    }
}