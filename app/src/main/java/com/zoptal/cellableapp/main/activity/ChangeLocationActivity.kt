package com.zoptal.cellableapp.main.activity

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.os.Handler
import android.os.ResultReceiver
import android.provider.Settings
import androidx.appcompat.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.core.app.ActivityCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.android.gms.common.*
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.Status
import com.google.android.gms.location.LocationListener
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.places.ui.PlaceAutocomplete
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.GoogleMap.OnCameraIdleListener
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.AutocompleteSupportFragment
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.fcm_push_notification.Config
import com.zoptal.cellableapp.fcm_push_notification.NotificationMessageDialog
import com.zoptal.cellableapp.fcm_push_notification.NotificationUtils.Companion.clearNotifications
import com.zoptal.cellableapp.main.activity.ChangeLocationActivity
import com.zoptal.cellableapp.moveCameraObj.AppUtils
import com.zoptal.cellableapp.moveCameraObj.FetchAddressIntentService
import com.zoptal.cellableapp.utility.CommonClass
import com.zoptal.cellableapp.utility.RunTimePermission
import com.zoptal.cellableapp.utility.VariableConstants
import java.io.IOException
import java.util.*

/**
 * <h>ChangeLocationActivity</h>
 *
 *
 * In this class we used to show google map and move camera oject feature
 * for user to select the required location. When User will move the
 * camera then parallely address will also getting change. User can also
 * search address by typing with help of AutoComplete address.
 *
 * @since 24-May-2017
 * @version 1.0
 * @author 3Embed
 */
class ChangeLocationActivity constructor() : AppCompatActivity(), OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener, View.OnClickListener {
    private var mMap: GoogleMap? = null
    private var mGoogleApiClient: GoogleApiClient? = null
    private var mActivity: Activity? = null
    private var mCenterLatLong: LatLng? = null
    private var selectedLat: String = ""
    private var selectedLng: String = ""
    private var mapView: View? = null

    /**
     * Receiver registered with this activity to get the response from FetchAddressIntentService.
     */
    private var mResultReceiver: AddressResultReceiver? = null

    /**
     * The formatted location address.
     */
    protected var mAddressOutput: String? = null
    protected var mAreaOutput: String? = null
    protected var mCityOutput: String? = null
    protected var mStateOutput: String? = null
    private var mLocationText: TextView? = null
    private var runTimePermission: RunTimePermission? = null
    private var permissionsArray: Array<String> = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
    private var mNotificationMessageDialog: NotificationMessageDialog? = null
    private var cityName: String = ""
    private var autocompleteFragment: AutocompleteSupportFragment? = null
    override fun onCreate(savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_change_location)
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale)
        initVariables()
    }

    /**
     * <h>InitVariables</h>
     *
     *
     * In this method we used to initialize all data member.
     *
     */
    private fun initVariables() {
        mActivity = this@ChangeLocationActivity
        mNotificationMessageDialog = NotificationMessageDialog(mActivity)
        CommonClass.statusBarColor(mActivity!!)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment: SupportMapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapView = mapFragment.getView()

        mLocationText = findViewById(R.id.Locality) as TextView?
        val rL_searchLoc: RelativeLayout = findViewById(R.id.rL_searchLoc) as RelativeLayout
        rL_searchLoc.setOnClickListener(object : View.OnClickListener {
            public override fun onClick(v: View) {
                openAutocompleteActivity()
            }
        })

        runTimePermission = RunTimePermission(this, permissionsArray, true)
        //runTimePermission.requestPermission();

        /*mLocationText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openAutocompleteActivity();
            }


        });*/mapFragment.getMapAsync(this)
        mResultReceiver = AddressResultReceiver(Handler())
        if (checkPlayServices()) {
            // If this check succeeds, proceed with normal processing.
            // Otherwise, prompt user to get valid Play Services APK.
            if (!AppUtils.isLocationEnabled(mActivity!!)) {
                // notify user
                val dialog: AlertDialog.Builder = AlertDialog.Builder(mActivity)
                dialog.setMessage("Location not enabled!")
                dialog.setPositiveButton("Open location settings", object : DialogInterface.OnClickListener {
                    public override fun onClick(paramDialogInterface: DialogInterface, paramInt: Int) {
                        val myIntent: Intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                        startActivity(myIntent)
                    }
                })
                dialog.setNegativeButton("Cancel", object : DialogInterface.OnClickListener {
                    public override fun onClick(paramDialogInterface: DialogInterface, paramInt: Int) {
                        // TODO Auto-generated method stub
                    }
                })
                dialog.show()
            }
            buildGoogleApiClient()
        } else {
            Toast.makeText(mActivity, "Location not supported in this device", Toast.LENGTH_SHORT).show()
        }

        // Confirm location
        val rL_confirm_location: RelativeLayout = findViewById(R.id.rL_confirm_location) as RelativeLayout
        rL_confirm_location.setOnClickListener(this)

        // back button
        val rL_back_btn: RelativeLayout = findViewById(R.id.rL_back_btn) as RelativeLayout
        rL_back_btn.setOnClickListener(this)
        Places.initialize(getApplicationContext(), "AIzaSyAPdu5iIPOtGiOsqipo38PNYdMTVsB5SqA") //server key
        // Initialize the AutocompleteSupportFragment.
        autocompleteFragment = supportFragmentManager.findFragmentById(R.id.autocomplete_fragment) as AutocompleteSupportFragment?


        // Specify the types of place data to return.
        mLocationText!!.setVisibility(View.GONE)
        val intent: Intent = getIntent()
        val location: String? = intent.getStringExtra("location")
//        Log.d(TAG, " location : " + location)
        if(location!=null) {
            autocompleteFragment!!.setText(location)
        }
        autocompleteFragment!!.a.setTextSize(14.0f)
        autocompleteFragment!!.a.setPaddingRelative(5, 0, 0, 0)
        // ((TextView)placeAutocompleteFragment.getView().findViewById(R.id.place_autocomplete_search_input)).setTextSize(10.0f);
        //(autocompleteFragment.getView().findViewById(R.id.autocomplete_fragment)).setTextSize(10);
        autocompleteFragment!!.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG))


        // Set up a PlaceSelectionListener to handle the response.
        autocompleteFragment!!.setOnPlaceSelectedListener(object : PlaceSelectionListener {
            @SuppressLint("MissingPermission")
            public override fun onPlaceSelected(place: Place) {
                // TODO: Get info about the selected place.
                Log.i(TAG, "Place: " + place.getName() + ", " + place.getId() + " , " + place.getLatLng()!!.latitude + " , " + place.getLatLng()!!.longitude)
                /*LatLng MOUNTAIN_VIEW = new LatLng(place.getLatLng().latitude, place.getLatLng().longitude);
                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(MOUNTAIN_VIEW)
                        .zoom(17)
                        .tilt(30)
                        .build();*/selectedLat = place.getLatLng()!!.latitude.toString()
                selectedLng = place.getLatLng()!!.longitude.toString()
                mLocationText!!.setText(place.getAddress())
                val cameraPosition: CameraPosition = CameraPosition.Builder().target(place.getLatLng()).zoom(17f).tilt(0f).build()
                mMap!!.setMyLocationEnabled(true)
                mMap!!.animateCamera(CameraUpdateFactory
                        .newCameraPosition(cameraPosition))
                Log.i("exe", "Place: " + place.getName() + ", " + place.getId())
            }

             override fun onError(@NonNull status: Status) {
                Log.i("exe", "An error occurred: " + status)
            }
        })
    }

    override fun onResume() {
        super.onResume()

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.REGISTRATION_COMPLETE))

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.PUSH_NOTIFICATION))

        // clear the notification area when the com is opened
        clearNotifications(getApplicationContext())
    }

   override fun onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!)
        super.onPause()
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near own current location.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the com.
     */
    public override fun onMapReady(googleMap: GoogleMap) {
        Log.d(TAG, "OnMapReady")
        mMap = googleMap

        // Change MyLocationButton position
        if (mapView != null && mapView!!.findViewById<View?>("1".toInt()) != null) {
            // Get the button view
            val locationButton: View = (mapView!!.findViewById<View>("1".toInt()).getParent() as View).findViewById("2".toInt())
            // and next place it, on bottom right (as Google Maps com)
            val layoutParams: RelativeLayout.LayoutParams = locationButton.getLayoutParams() as RelativeLayout.LayoutParams
            // position on right bottom
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0)
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE)
            layoutParams.setMargins(0, 0, 30, 30)
        }
        mMap!!.setOnCameraIdleListener(object : OnCameraIdleListener {
            public override fun onCameraIdle() {
                mCenterLatLong = mMap!!.getCameraPosition().target
                mMap!!.clear()
                try {
                    val mLocation: Location = Location("")
                    mLocation.setLatitude(mCenterLatLong!!.latitude)
                    mLocation.setLongitude(mCenterLatLong!!.longitude)
                    startIntentService(mLocation)
                    println(TAG + " " + "map lat=" + mCenterLatLong!!.latitude + " " + "lng=" + mCenterLatLong!!.longitude)
                    selectedLat = mCenterLatLong!!.latitude.toString()
                    selectedLng = mCenterLatLong!!.longitude.toString()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            runTimePermission!!.requestPermission()
        }
    }

    public override fun onConnected(bundle: Bundle?) {
        if (runTimePermission!!.checkPermissions(permissionsArray)) {
            runTimePermission!!.requestPermission()
        } else {
            onConnectWithLoc()
        }
    }

    private fun onConnectWithLoc() {
        println(TAG + " " + "fused location onConnected...")
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }
        val mLastLocation: Location? = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient)
        if (mLastLocation != null) {
            println(TAG + " " + "location onConnected=" + mLastLocation.getLatitude() + " " + "")
            changeMap(mLastLocation)
            Log.d(TAG, "ON connected")
        } else try {
            LocationServices.FusedLocationApi.removeLocationUpdates(
                    mGoogleApiClient, this)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            val mLocationRequest: LocationRequest = LocationRequest()
            mLocationRequest.setInterval(10000)
            mLocationRequest.setFastestInterval(5000)
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient, mLocationRequest, this)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    public override fun onConnectionSuspended(i: Int) {
        Log.i(TAG, "Connection suspended")
        mGoogleApiClient!!.connect()
    }

    public override fun onLocationChanged(location: Location) {
        println(TAG + " " + "fused location onLocationChanged...")
        try {
            if (location != null) changeMap(location)
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    public override fun onConnectionFailed(@NonNull connectionResult: ConnectionResult) {}

    @Synchronized
    protected fun buildGoogleApiClient() {
        mGoogleApiClient = GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build()
    }

    override fun onStart() {
        super.onStart()
        try {
            mGoogleApiClient!!.connect()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onStop() {
        super.onStop()
        if (mGoogleApiClient != null && mGoogleApiClient!!.isConnected()) {
            mGoogleApiClient!!.disconnect()
        }
    }

    private fun checkPlayServices(): Boolean {
        val resultCode: Int = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this)
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show()
            }
            return false
        }
        return true
    }

    private fun changeMap(location: Location) {
        Log.d(TAG, "Reaching map" + mMap)
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            runTimePermission!!.requestPermission()
        }


        // check if map is created successfully or not
        if (mMap != null) {
            mMap!!.getUiSettings().setZoomControlsEnabled(false)
            val latLong: LatLng
            latLong = LatLng(location.getLatitude(), location.getLongitude())
            val cameraPosition: CameraPosition = CameraPosition.Builder().target(latLong).zoom(17f).tilt(0f).build()
            mMap!!.setMyLocationEnabled(true)
            mMap!!.getUiSettings().setMyLocationButtonEnabled(true)
            mMap!!.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
            mMap!!.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
            println(TAG + " " + "map lat change map=" + location.getLatitude() + " " + "lng=" + location.getLongitude())
            selectedLat = location.getLatitude().toString()
            selectedLng = location.getLongitude().toString()
            startIntentService(location)
        } else {
            Toast.makeText(getApplicationContext(),
                    "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                    .show()
        }
    }

    public override fun onClick(v: View) {
        when (v.getId()) {
            R.id.rL_confirm_location -> {
                val intent: Intent = Intent()
                intent.putExtra("lat", selectedLat)
                intent.putExtra("lng", selectedLng)
                intent.putExtra("address", mLocationText!!.getText().toString())
                intent.putExtra("cityName", cityName)
                setResult(VariableConstants.CHANGE_LOC_REQ_CODE, intent)
                println(TAG + " " + "lat=" + selectedLat + " " + "lng=" + selectedLng + " " + "address=" + mLocationText!!.getText().toString())
                onBackPressed()
            }
            R.id.rL_back_btn -> onBackPressed()
        }
    }

    /**
     * Receiver for data sent from FetchAddressIntentService.
     */
    private inner class AddressResultReceiver internal constructor(handler: Handler?) : ResultReceiver(handler) {
        /**
         * Receives data sent from FetchAddressIntentService and updates the UI in MainActivity.
         */
        override fun onReceiveResult(resultCode: Int, resultData: Bundle) {

            // Display the address string or an error message sent from the intent service.
            mAddressOutput = resultData.getString(AppUtils.LocationConstants.RESULT_DATA_KEY)
            mAreaOutput = resultData.getString(AppUtils.LocationConstants.LOCATION_DATA_AREA)
            mCityOutput = resultData.getString(AppUtils.LocationConstants.LOCATION_DATA_CITY)
            mStateOutput = resultData.getString(AppUtils.LocationConstants.LOCATION_DATA_STREET)
            println(TAG + " " + "message=" + mAddressOutput + " " + "address=" + mAreaOutput + " " + "mCityOutput=" + mCityOutput + " " + "mStateOutput=" + mStateOutput)
            if (mCityOutput == null || (mCityOutput == "")) {
                val geocoder: Geocoder = Geocoder(getApplicationContext(), Locale.getDefault())
                var addresses: List<Address>? = null
                try {
                    addresses = geocoder.getFromLocation(selectedLat.toDouble(), selectedLng.toDouble(), 1)
                } catch (e: IOException) {
                    e.printStackTrace()
                }
                cityName = addresses!!.get(0).getAdminArea()
                val stateName: String = addresses.get(0).getAdminArea()
                val countryName: String = addresses.get(0).getCountryName()
                println(TAG + " city name= " + cityName + " state name= " + stateName + " country name= " + countryName)
            }
            displayAddressOutput()
        }
    }

    /**
     * Updates the address in the UI.
     */
    protected fun displayAddressOutput() {
        try {
            if (mAreaOutput != null && !mAddressOutput!!.isEmpty()) {
                autocompleteFragment!!.setText(mAddressOutput)
                mLocationText!!.setText(mAddressOutput)
            } else {
                autocompleteFragment!!.setText(mStateOutput)
                mLocationText!!.setText(mStateOutput)
            }
            Log.d(TAG, "displayAddressOutput: sdfskdjghs")
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * Creates an intent, adds location data to it as an extra, and starts the intent service for
     * fetching an address.
     */
    protected fun startIntentService(mLocation: Location?) {
        // Create an intent for passing to the intent service responsible for fetching the address.
        val intent: Intent = Intent(this, FetchAddressIntentService::class.java)

        // Pass the result receiver as an extra to the service.
        intent.putExtra(AppUtils.LocationConstants.RECEIVER, mResultReceiver)

        // Pass the location data as an extra to the service.
        intent.putExtra(AppUtils.LocationConstants.LOCATION_DATA_EXTRA, mLocation)

        // Start the service. If the service isn't already running, it is instantiated and started
        // (creating a process for it if needed); if it is running then it remains running. The
        // service kills itself automatically once all intents are processed.
        startService(intent)
    }

    private fun openAutocompleteActivity() {
        try {
            // The autocomplete activity requires Google Play Services to be available. The intent
            // builder checks this and throws an exception if it is not the case.
            val intent: Intent = PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN).build(this)
            startActivityForResult(intent, REQUEST_CODE_AUTOCOMPLETE)
        } catch (e: GooglePlayServicesRepairableException) {
            // Indicates that Google Play Services is either not installed or not up to date. Prompt
            // the user to correct the issue.
            GoogleApiAvailability.getInstance().getErrorDialog(this, e.getConnectionStatusCode(),
                    0 /* requestCode */).show()
        } catch (e: GooglePlayServicesNotAvailableException) {
            // Indicates that Google Play Services is not available and the problem is not easily
            // resolvable.
            val message: String = "Google Play Services is not available: " +
                    GoogleApiAvailability.getInstance().getErrorString(e.errorCode)
            Toast.makeText(mActivity, message, Toast.LENGTH_SHORT).show()
        }
    }

    /**
     * Called after the autocomplete activity has finished to return its result.
     */
   override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        // Check that the result was from the autocomplete widget.
        if (requestCode == REQUEST_CODE_AUTOCOMPLETE) {
            if (resultCode == RESULT_OK) {
                // Get the user's selected place from the Intent.
                val place: com.google.android.gms.location.places.Place = PlaceAutocomplete.getPlace(mActivity, data)

                // TODO call location based filter
                val latLong: LatLng
                latLong = place.getLatLng()

                //mLocationText.setText(place.getName() + "");
                val cameraPosition: CameraPosition = CameraPosition.Builder().target(latLong).zoom(17f).tilt(0f).build()
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return
                }
                mMap!!.setMyLocationEnabled(true)
                mMap!!.animateCamera(CameraUpdateFactory
                        .newCameraPosition(cameraPosition))
            }
        }
    }

 override fun onBackPressed() {
        finish()
        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate)
    }

    override fun onRequestPermissionsResult(requestCode: Int, @NonNull permissions: Array<String?>, @NonNull grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            VariableConstants.PERMISSION_REQUEST_CODE -> {
                println("grant result=" + grantResults.size)
                if (grantResults.size > 0) {
                    var count: Int = 0
                    while (count < grantResults.size) {
                        if (grantResults.get(count) != PackageManager.PERMISSION_GRANTED) runTimePermission!!.allowPermissionAlert(permissions.get(count)!!)
                        count++
                    }
                    println("isAllPermissionGranted=" + runTimePermission!!.checkPermissions(permissionsArray))
                    if (runTimePermission!!.checkPermissions(permissionsArray)) {
                        onConnectWithLoc()
                    }
                }
            }
        }
    }

    companion object {
        private val PLAY_SERVICES_RESOLUTION_REQUEST: Int = 9000
        private val TAG: String = ChangeLocationActivity::class.java.getSimpleName()
        private val REQUEST_CODE_AUTOCOMPLETE: Int = 1
    }
}