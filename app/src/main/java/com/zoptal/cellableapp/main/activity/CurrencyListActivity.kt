package com.zoptal.cellableapp.main.activity

import android.app.Activity
import android.content.Context
import android.content.IntentFilter
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.MotionEvent
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.RelativeLayout
import androidx.annotation.Nullable
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.adapter.CurrencyRvAdap
import com.zoptal.cellableapp.fcm_push_notification.Config
import com.zoptal.cellableapp.fcm_push_notification.NotificationMessageDialog
import com.zoptal.cellableapp.fcm_push_notification.NotificationUtils.Companion.clearNotifications
import com.zoptal.cellableapp.main.activity.CurrencyListActivity
import com.zoptal.cellableapp.pojo_class.CurrencyPojo
import com.zoptal.cellableapp.utility.CommonClass
import java.util.*

/**
 * <h>CurrencyListActivity</h>
 *
 *
 * This class is called from PostProductActivity class. In this class we used
 * to get all country name and its currency symbol and to show in recyclerview
 * using its adpter. On the click of any particular country. we used to send
 * its cuurency code and symbol to the previous activity.
 *
 * @since 05-May-17
 * @author 3embed
 * @version 1.0
 */
class CurrencyListActivity constructor() : AppCompatActivity(), View.OnClickListener {

    private var arrayListCurrency: ArrayList<CurrencyPojo>? = null
    private var rV_currency: RecyclerView? = null
    private var mActivity: Activity? = null
    private var currencyRvAdap: CurrencyRvAdap? = null
    private var mNotificationMessageDialog: NotificationMessageDialog? = null
     override fun onCreate(@Nullable savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_currency_list)
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
        initVariables()
    }

    /**
     * <h>initVariables</h>
     *
     *
     * In this method we used to initialize all variables.
     *
     */
    private fun initVariables() {
        mActivity = this@CurrencyListActivity
        mNotificationMessageDialog = NotificationMessageDialog(mActivity)
        CommonClass.statusBarColor(mActivity!!)
        arrayListCurrency = ArrayList()
        val rL_back_btn: RelativeLayout = findViewById(R.id.rL_back_btn) as RelativeLayout
        rL_back_btn.setOnClickListener(this)
        rV_currency = findViewById(R.id.rV_currency) as RecyclerView?
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale)
//        println(TAG + " " + "arrayCurrency size=" + arrayCurrency.size)
        storeCurrencyIntoList()
        /**
         * Search currency
         */
        val eT_searchCode: EditText = findViewById(R.id.eT_searchCode) as EditText
        eT_searchCode.addTextChangedListener(object : TextWatcher {
            public override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            public override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                currencyRvAdap!!.getFilter().filter(s)
            }

            public override fun afterTextChanged(s: Editable) {}
        })
    }

    override fun onResume() {
        super.onResume()

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.REGISTRATION_COMPLETE))

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.PUSH_NOTIFICATION))

        // clear the notification area when the app is opened
        clearNotifications(getApplicationContext())
    }

   override fun onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!)
        super.onPause()
    }

    /**
     * <h>StoreCurrencyIntoList</h>
     *
     *
     * This method is called from onCreate() method of the same class. In this
     * method we used to save all country name, currency code and its symbol
     * into generic type arraylist. After we used to send list to its adpter
     * class and show in recyclerview.
     *
     */
    private fun storeCurrencyIntoList() {
        var arrayCurrency: Array<String> = getResources().getStringArray(R.array.currency_picker)
        if (arrayCurrency.size > 0) {
            var getCurrencyArr: Array<String?>
            for (currency: String in arrayCurrency) {
                getCurrencyArr = currency.split(",".toRegex()).toTypedArray()
                val currencyPojo: CurrencyPojo = CurrencyPojo()
                currencyPojo.country=(getCurrencyArr.get(0)!!)
                currencyPojo.code=(getCurrencyArr.get(1)!!)
                currencyPojo.symbol=(getCurrencyArr.get(2)!!)
                arrayListCurrency!!.add(currencyPojo)
            }

            // show country and its currency
            if (arrayListCurrency!!.size > 0) {
                currencyRvAdap = CurrencyRvAdap((mActivity)!!, (arrayListCurrency)!!)
                val layoutManager: LinearLayoutManager = LinearLayoutManager(mActivity)
                rV_currency!!.setLayoutManager(layoutManager)
                rV_currency!!.setAdapter(currencyRvAdap)
            }
        }
    }

 override fun onBackPressed() {
        finish()
        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate)
    }

    public override fun onClick(v: View) {
        when (v.getId()) {
            R.id.rL_back_btn -> onBackPressed()
        }
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        val imm: InputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(View(this).getWindowToken(), 0)
        return true
    }

    companion object {
        private val TAG: String = CurrencyListActivity::class.java.getSimpleName()
    }
}