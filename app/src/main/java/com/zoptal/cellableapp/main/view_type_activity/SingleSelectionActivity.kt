package com.zoptal.cellableapp.main.view_type_activity

import android.app.Activity
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.adapter.view_type_adapter.SingleSelectionAdap
import com.zoptal.cellableapp.fcm_push_notification.Config
import com.zoptal.cellableapp.fcm_push_notification.NotificationMessageDialog
import com.zoptal.cellableapp.fcm_push_notification.NotificationUtils.Companion.clearNotifications
import com.zoptal.cellableapp.main.view_type_activity.SingleSelectionActivity
import com.zoptal.cellableapp.utility.ClickListener
import com.zoptal.cellableapp.utility.VariableConstants
import java.util.*

class SingleSelectionActivity : AppCompatActivity(), ClickListener, View.OnClickListener {
    private var mNotificationMessageDialog: NotificationMessageDialog? = null
    private var rV_singleSelection: RecyclerView? = null
    private var arrayList: ArrayList<String>? = null
    private var singleSelectionAdap: SingleSelectionAdap? = null
    private var rL_done: RelativeLayout? = null
    private var values: String? = null
    private var fieldName: String? = null
    private var title: String? = null
    private var tV_title: TextView? = null
    private var selectedValue: String? = ""
    private var mActivity: Activity? = null
    protected override fun onCreate(savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_single_selection)
        mActivity = this@SingleSelectionActivity
        mNotificationMessageDialog = NotificationMessageDialog(mActivity)
        arrayList = ArrayList()
        values = getIntent().getStringExtra("values")
        title = getIntent().getStringExtra("title")
        fieldName = getIntent().getStringExtra("fieldName")
        selectedValue = getIntent().getStringExtra("selectedValue")
        arrayList = getArrayListFromValues(values)
        tV_title = findViewById(R.id.tV_title) as TextView?
        tV_title!!.text = title
        rV_singleSelection = findViewById(R.id.rV_singleSelection) as RecyclerView?
        singleSelectionAdap = SingleSelectionAdap(this, arrayList!!)
        rV_singleSelection!!.setLayoutManager(LinearLayoutManager(this))
        if (selectedValue != null && !selectedValue!!.isEmpty()) {
            for (i in arrayList!!.indices) {
                if (selectedValue.equals(arrayList!![i], ignoreCase = true)) {
                    singleSelectionAdap!!.mSelected = i
                }
            }
        }
        rV_singleSelection!!.setAdapter(singleSelectionAdap)
        val rL_back_btn = findViewById(R.id.rL_back_btn) as RelativeLayout
        rL_back_btn.setOnClickListener(this)
        rL_done = findViewById(R.id.rL_done) as RelativeLayout?
        //rL_done.setOnClickListener(this);
    }

    override fun onItemClick(view: View?, position: Int) {
        if (singleSelectionAdap!!.mSelected >= 0) {
            val intent: Intent = getIntent()
            intent.putExtra("selectedValue", arrayList!![singleSelectionAdap!!.mSelected])
            setResult(VariableConstants.SELECT_VALUE_REQ_CODE, intent)
            onBackPressed()
            //rL_done.setVisibility(View.VISIBLE);
        }
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.rL_back_btn -> onBackPressed()
            R.id.rL_done -> {
                val intent: Intent = getIntent()
                intent.putExtra("selectedValue", arrayList!![singleSelectionAdap!!.mSelected])
                setResult(VariableConstants.SELECT_VALUE_REQ_CODE, intent)
                onBackPressed()
            }
        }
    }

    fun getArrayListFromValues(values: String?): ArrayList<String> {
        val arrayList = ArrayList<String>()
        val `val` = values!!.split(",".toRegex()).toTypedArray()
        arrayList.addAll(Arrays.asList(*`val`))
        return arrayList
    }

    override fun onResume() {
        super.onResume()

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.REGISTRATION_COMPLETE))

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.PUSH_NOTIFICATION))

        // clear the notification area when the app is opened
        clearNotifications(getApplicationContext())
    }

   override fun onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!)
        super.onPause()
    }
}