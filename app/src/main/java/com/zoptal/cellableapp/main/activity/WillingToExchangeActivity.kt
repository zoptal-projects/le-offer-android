package com.zoptal.cellableapp.main.activity

import android.app.Activity
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.adapter.IwantExchangeAdapter
import com.zoptal.cellableapp.adapter.IwantSuggessionAdapter
import com.zoptal.cellableapp.fcm_push_notification.Config
import com.zoptal.cellableapp.fcm_push_notification.NotificationMessageDialog
import com.zoptal.cellableapp.fcm_push_notification.NotificationUtils.Companion.clearNotifications
import com.zoptal.cellableapp.main.activity.PostProductActivity
import com.zoptal.cellableapp.main.activity.WillingToExchangeActivity
import com.zoptal.cellableapp.pojo_class.exchange_suggtions_pojo.MyExchangesData
import com.zoptal.cellableapp.pojo_class.exchange_suggtions_pojo.MyExchangesPojo
import com.zoptal.cellableapp.pojo_class.product_details_pojo.SwapPost
import com.zoptal.cellableapp.utility.*
import com.zoptal.cellableapp.utility.OkHttp3Connection.OkHttp3RequestCallback
import org.json.JSONObject
import java.util.*

class WillingToExchangeActivity : AppCompatActivity(), View.OnClickListener {
    private var mNotificationMessageDialog: NotificationMessageDialog? = null
    private var mActivity: Activity? = null
    private var eT_searchProduct: EditText? = null
    private var crossIv: ImageView? = null
    private var tV_save: TextView? = null
    private var iWantArrayList: ArrayList<SwapPost>? = null
    private var iwantAdapter: IwantExchangeAdapter? = null
    private var iwantSuggessionAdapter: IwantSuggessionAdapter? = null
    private var iwantRv: RecyclerView? = null
    private var suggessionRv: RecyclerView? = null
    private val arrayList: ArrayList<MyExchangesData> = ArrayList<MyExchangesData>()
    private var mSessionManager: SessionManager? = null
    private var rL_rootElement: LinearLayout? = null
    private val arrayListPostIds = ArrayList<String>()
    private var addItem = false
    override fun onCreate(savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_willing_to_exchange)
        intializeVariable()
    }

    private fun intializeVariable() {
        mActivity = this@WillingToExchangeActivity
        mNotificationMessageDialog = NotificationMessageDialog(mActivity)
        val intent = intent
        iWantArrayList = intent.getSerializableExtra("iWantArrayList") as ArrayList<SwapPost>
        for (i in iWantArrayList!!.indices) {
            val iwantItemPojo = iWantArrayList!![i]
            if (!iwantItemPojo.itemType) arrayListPostIds.add(iwantItemPojo.swapPostId!!)
            if (iwantItemPojo.itemType) {
                iWantArrayList!!.remove(iwantItemPojo)
            }
        }
        mSessionManager = SessionManager(mActivity!!)
        eT_searchProduct = findViewById<View>(R.id.eT_searchProduct) as EditText
        crossIv = findViewById<View>(R.id.crossIv) as ImageView
        tV_save = findViewById<View>(R.id.tV_save) as TextView
        iwantRv = findViewById<View>(R.id.iwantRv) as RecyclerView
        suggessionRv = findViewById<View>(R.id.suggessionRv) as RecyclerView
        rL_rootElement = findViewById<View>(R.id.rL_rootElement) as LinearLayout
        tV_save!!.setOnClickListener(this)
        crossIv!!.setOnClickListener(this)
        iwantAdapter = IwantExchangeAdapter(iWantArrayList!!, this)
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        iwantRv!!.layoutManager = layoutManager
        iwantRv!!.adapter = iwantAdapter
        iwantSuggessionAdapter = IwantSuggessionAdapter(arrayList, this)
        val mlayoutManager: RecyclerView.LayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        suggessionRv!!.layoutManager = mlayoutManager
        suggessionRv!!.adapter = iwantSuggessionAdapter
        eT_searchProduct!!.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                getExchangeData(s.toString())
            }

            override fun afterTextChanged(s: Editable) {}
        })

        /* // Setting EditorActionListener for the EditText
        eT_searchProduct.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId== EditorInfo.IME_ACTION_DONE){
                    SwapPost iwantItemPojo = new SwapPost();
                    iwantItemPojo.setSwapTitle(eT_searchProduct.getText().toString().trim());
                    iWantArrayList.add(iwantItemPojo);
                    iwantAdapter.notifyDataSetChanged();
                }
                return false;
            }
        });*/
    }

    private fun getExchangeData(productName: String) {
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            val url = ApiUrl.GET_EXCHAGES_BY_PRODUCT_NAME + "?token=" + mSessionManager!!.authToken + "&productName=" + productName
            OkHttp3Connection.doOkHttp3Connection(TAG, url, OkHttp3Connection.Request_type.GET, JSONObject(), object : OkHttp3RequestCallback {
                 override fun onSuccess(result: String?, user_tag: String?) {
                    //pBar_resend.setVisibility(View.GONE);
                    println(TAG + " " + mSessionManager!!.authToken)
                    println("$TAG post product  res=$result")
                    val myExchangesPojo: MyExchangesPojo
                    val gson = Gson()
                    myExchangesPojo = gson.fromJson(result, MyExchangesPojo::class.java)
                    when (myExchangesPojo.code) {
                        "200" -> {
                            suggessionRv!!.visibility = View.VISIBLE
                            arrayList.clear()
                            arrayList.addAll(myExchangesPojo.data!!)
                            iwantSuggessionAdapter!!.notifyDataSetChanged()
                        }
                        "401" -> CommonClass.sessionExpired(mActivity!!)
                        else -> {
                            arrayList.clear()
                            iwantSuggessionAdapter!!.notifyDataSetChanged()
                        }
                    }
                }

                 override fun onError(error: String?, user_tag: String?) {
                    //  pBar_resend.setVisibility(View.GONE);
                    //  tV_reSend.setVisibility(View.VISIBLE);
                }
            })
        } else CommonClass.showSnackbarMessage(rL_rootElement, resources.getString(R.string.NoInternetAccess))
    }

    fun iWantAdapterClick(postId: String, position: Int) {
        arrayListPostIds.remove(postId)
        iWantArrayList!!.removeAt(position)
        iwantAdapter!!.notifyDataSetChanged()
    }

    fun suggessionAdapterClick(productName: String?, postId: String) {
        Log.d("exe", "postId$postId")
        Log.d("exe", "arrayListPostIds" + arrayListPostIds.contains(postId))
        if (!arrayListPostIds.contains(postId)) {
            arrayListPostIds.add(postId)
            addItem = true
        }
        if (addItem) {
            val iwantItemPojo = SwapPost()
            iwantItemPojo.swapTitle = productName
            iwantItemPojo.swapPostId = postId
            iWantArrayList!!.add(iwantItemPojo)
            iwantAdapter!!.notifyDataSetChanged()
            addItem = false
        }
        eT_searchProduct!!.setText("")
    }

    override fun onClick(v: View) {
        if (v.id == R.id.tV_save) {
            callPostProduct()
        } else {
            onBackPressed()
        }
    }

    private fun callPostProduct() {
        val postProductIntent = Intent()
        postProductIntent.putExtra("iWantArrayList", iWantArrayList)
        setResult(VariableConstants.Willing_TO_EXCHANGE, postProductIntent)
        onBackPressed()
    }

    override fun onBackPressed() {
        finish()
        overridePendingTransition(R.anim.stay, R.anim.slide_down)
    }

    override fun onResume() {
        super.onResume()

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.REGISTRATION_COMPLETE))

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.PUSH_NOTIFICATION))

        // clear the notification area when the app is opened
        clearNotifications(applicationContext)
    }

    override fun onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!)
        super.onPause()
    }

    companion object {
        private val TAG = PostProductActivity::class.java.simpleName
    }
}