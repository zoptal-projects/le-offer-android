package com.zoptal.cellableapp.main.view_pager.notification_frag

import android.app.Activity
import android.os.Bundle

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.RelativeLayout
import androidx.annotation.Nullable
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.gson.Gson
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.adapter.FollowingFragRvAdap
import com.zoptal.cellableapp.pojo_class.explore_following_pojo.FollowingActivityMainPojo
import com.zoptal.cellableapp.pojo_class.explore_following_pojo.FollowingResponseDatas
import com.zoptal.cellableapp.utility.*
import com.zoptal.cellableapp.utility.OkHttp3Connection.OkHttp3RequestCallback
import org.json.JSONException
import org.json.JSONObject
import java.util.*

/**
 * <h>FollowingFrag</h>
 *
 *
 * This class is called from ExploreNotificationActivity class. In this class
 * we show the user following notification message.
 *
 * @since 4/15/2017
 * @version 1.0
 * @author 3Embed
 */
class FollowingFrag : Fragment() {
    private var mSessionManager: SessionManager? = null
    private var progress_bar_notification: ProgressBar? = null
    private var mActivity: Activity? = null
    private var followingFragRvAdap: FollowingFragRvAdap? = null
    private var al_following_data: ArrayList<FollowingResponseDatas>? = null
    private var index = 0
    private var swipe_refresh_layout: SwipeRefreshLayout? = null
    private var rL_rootview: RelativeLayout? = null
    private var linear_no_activity: LinearLayout? = null
    override fun onCreate(@Nullable savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        mActivity = getActivity()
        mSessionManager = SessionManager(mActivity!!)
        index = 0
    }

    @Nullable
    override fun onCreateView(inflater: LayoutInflater, @Nullable container: ViewGroup?, @Nullable savedInstanceState: Bundle?): View {
        val view = inflater.inflate(R.layout.frag_following_notification, container, false)
        initVariables(view)
        return view
    }

    /**
     * <h>initVariables</h>
     *
     *
     * In this method we initialize the xml all variables.
     *
     * @param view The parent view
     */
    private fun initVariables(view: View) {
        al_following_data = ArrayList()
        rL_rootview = view.findViewById<View>(R.id.rL_rootview) as RelativeLayout
        progress_bar_notification = view.findViewById<View>(R.id.progress_bar_notification) as ProgressBar
        val rV_notification: RecyclerView = view.findViewById<View>(R.id.rV_notification) as RecyclerView
        val layoutManager = LinearLayoutManager(mActivity)
        rV_notification.setLayoutManager(layoutManager)
        followingFragRvAdap = FollowingFragRvAdap(mActivity!!, al_following_data!!, rV_notification)
        rV_notification.setAdapter(followingFragRvAdap)

        // if no activity found
        linear_no_activity = view.findViewById<View>(R.id.linear_no_activity) as LinearLayout
        linear_no_activity!!.visibility = View.GONE
        val iV_icon = view.findViewById<View>(R.id.iV_icon) as ImageView
        iV_icon.setImageResource(R.drawable.empty_following)

        // call api method
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            progress_bar_notification!!.visibility = View.VISIBLE
            followingActivityApi(index)
        }

        // pull to refresh
        swipe_refresh_layout = view.findViewById<View>(R.id.swipe_refresh_layout) as SwipeRefreshLayout
        swipe_refresh_layout!!.setColorSchemeResources(R.color.pink_color)
        swipe_refresh_layout!!.setOnRefreshListener(object : SwipeRefreshLayout.OnRefreshListener {
           override fun onRefresh() {
                al_following_data!!.clear()
                index = 0
                followingActivityApi(index)
            }
        })
    }

    /**
     * <h>FollowingActivityApi</h>
     *
     *
     * In this method we do api followingActivity call to get users all
     * following notifications.
     *
     * @param offsetValue The page index
     */
    private fun followingActivityApi(offsetValue: Int) {
        var offsetValue = offsetValue
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            val limit = 20
            offsetValue = offsetValue * limit
            val request_data = JSONObject()
            try {
                request_data.put("token", mSessionManager!!.authToken)
                request_data.put("offset", offsetValue)
                request_data.put("limit", limit)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            println("$TAG index in followingActivity=$offsetValue")
            OkHttp3Connection.doOkHttp3Connection(TAG, ApiUrl.FOLLOWING_ACTIVITY, OkHttp3Connection.Request_type.POST, request_data, object : OkHttp3RequestCallback {
                 override fun onSuccess(result: String?, user_tag: String?) {
                    progress_bar_notification!!.visibility = View.GONE
                    println("$TAG following Activity res=$result")
                    val followingActivityPojo: FollowingActivityMainPojo
                    val gson = Gson()
                    followingActivityPojo = gson.fromJson(result, FollowingActivityMainPojo::class.java)
                    when (followingActivityPojo.code) {
                        "200" -> {
                            swipe_refresh_layout!!.setRefreshing(false)
                            al_following_data!!.addAll(followingActivityPojo.data!!)
                            if (al_following_data != null && al_following_data!!.size > 0) {
                                followingFragRvAdap!!.notifyDataSetChanged()
                                followingFragRvAdap!!.setLoaded()
                                followingFragRvAdap!!.setOnLoadMore(object : OnLoadMoreListener {
                                    override fun onLoadMore() {
                                        println("$TAG onLoadMore called")
                                        index = index + 1
                                        swipe_refresh_layout!!.setRefreshing(true)
                                        followingActivityApi(index)
                                    }
                                })
                            } else linear_no_activity!!.visibility = View.VISIBLE
                        }
                        "401" -> CommonClass.sessionExpired(mActivity!!)
                        else -> swipe_refresh_layout!!.setRefreshing(false)
                    }
                }

                 override fun onError(error: String?, user_tag: String?) {
                    progress_bar_notification!!.visibility = View.GONE
                    CommonClass.showSnackbarMessage(rL_rootview, error)
                }
            })
        } else CommonClass.showSnackbarMessage(rL_rootview, getResources().getString(R.string.NoInternetAccess))
    }

    companion object {
        private val TAG = FollowingFrag::class.java.simpleName
    }
}