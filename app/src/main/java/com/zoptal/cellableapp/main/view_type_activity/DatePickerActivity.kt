package com.zoptal.cellableapp.main.view_type_activity

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.android.material.snackbar.Snackbar
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.fcm_push_notification.Config
import com.zoptal.cellableapp.fcm_push_notification.NotificationMessageDialog
import com.zoptal.cellableapp.fcm_push_notification.NotificationUtils.Companion.clearNotifications
import com.zoptal.cellableapp.main.view_type_activity.DatePickerActivity
import com.zoptal.cellableapp.utility.VariableConstants
import java.util.*

class DatePickerActivity : AppCompatActivity(), View.OnClickListener {
    private var mNotificationMessageDialog: NotificationMessageDialog? = null
    var tV_date: TextView? = null
    var tV_title: TextView? = null
    private var iV_datePicker: ImageView? = null
    private var mYear = 0
    private var mMonth = 0
    private var mDay = 0
    private var title: String? = null
    private var type: String? = null
    private var selectedValue: String? = ""
    private var fieldName: String? = null
    private var rL_done: RelativeLayout? = null
    private var isPick = false
    private var mActivity: Activity? = null
    protected override fun onCreate(savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_date_picker)
        mActivity = this@DatePickerActivity
        mNotificationMessageDialog = NotificationMessageDialog(mActivity)
        title = getIntent().getStringExtra("title")
        fieldName = getIntent().getStringExtra("fieldName")
        if (getIntent().hasExtra("isNumber")) {
            type = getIntent().getStringExtra("isNumber")
        }
        if (getIntent().hasExtra("selectedValue")) {
            selectedValue = getIntent().getStringExtra("selectedValue")
        }
        tV_title = findViewById(R.id.tV_title) as TextView?
        if (title != null && !title!!.isEmpty()) tV_title!!.text = title
        tV_date = findViewById(R.id.tV_date) as TextView?
        if (selectedValue != null && !selectedValue!!.isEmpty()) tV_date!!.text = selectedValue
        iV_datePicker = findViewById(R.id.iV_datePicker) as ImageView?
        iV_datePicker!!.setOnClickListener(this)
        rL_done = findViewById(R.id.rL_done) as RelativeLayout?
        rL_done!!.setOnClickListener(this)
        val rL_back_btn = findViewById(R.id.rL_back_btn) as RelativeLayout
        rL_back_btn.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.iV_datePicker -> {
                // Get Current Date
                val c = Calendar.getInstance()
                mYear = c[Calendar.YEAR]
                mMonth = c[Calendar.MONTH]
                mDay = c[Calendar.DAY_OF_MONTH]
                val datePickerDialog = DatePickerDialog(this,
                        DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                            tV_date!!.text = dayOfMonth.toString() + "/" + (monthOfYear + 1) + "/" + year
                            isPick = true
                        }, mYear, mMonth, mDay)
                datePickerDialog.show()
            }
            R.id.rL_back_btn -> onBackPressed()
            R.id.rL_done -> if (isPick) {
                val intent: Intent = getIntent()
                intent.putExtra("selectedValue", tV_date!!.text.toString())
                setResult(VariableConstants.SELECT_VALUE_REQ_CODE, intent)
                onBackPressed()
            } else {
                Snackbar.make(rL_done!!, "Please select First !", Snackbar.LENGTH_SHORT).show()
            }
        }
    }

    override fun onResume() {
        super.onResume()

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.REGISTRATION_COMPLETE))

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.PUSH_NOTIFICATION))

        // clear the notification area when the app is opened
        clearNotifications(getApplicationContext())
    }

   override fun onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!)
        super.onPause()
    }
}