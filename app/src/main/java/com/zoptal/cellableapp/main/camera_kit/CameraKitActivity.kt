package com.zoptal.cellableapp.main.camera_kit
//import static android.Manifest.permission.RECORD_AUDIO;
import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.core.content.ContextCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
//import butterknife.BindView
//import butterknife.ButterKnife
//import butterknife.OnClick
import com.google.gson.Gson
import com.wonderkiln.camerakit.CameraKit
import com.wonderkiln.camerakit.CameraView
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.Uploader.ProductImageDatas
import com.zoptal.cellableapp.adapter.ImagesHorizontalRvAdap
import com.zoptal.cellableapp.fcm_push_notification.Config
import com.zoptal.cellableapp.fcm_push_notification.NotificationMessageDialog
import com.zoptal.cellableapp.fcm_push_notification.NotificationUtils.Companion.clearNotifications
import com.zoptal.cellableapp.main.activity.GalleryActivity
import com.zoptal.cellableapp.main.activity.PaymentAddAccount.Companion.callActivity
import com.zoptal.cellableapp.main.activity.PostProductActivity
import com.zoptal.cellableapp.main.camera_kit.CameraKitActivity
import com.zoptal.cellableapp.pojo_class.stripe.StripeMainPojo
import com.zoptal.cellableapp.utility.*
import com.zoptal.cellableapp.utility.OkHttp3Connection.OkHttp3RequestCallback
import kotlinx.android.synthetic.main.activity_camera_kit.*
import org.json.JSONObject
import java.util.*

class CameraKitActivity : AppCompatActivity(), ClickListener {
//    @BindView(R.id.camera)
//    var camera: CameraView? = null
    private var cameraMethod = CameraKit.Constants.METHOD_STANDARD
    private var cropOutput = false
    private var mActivity: Activity? = null
    var imagesHorizontalRvAdap: ImagesHorizontalRvAdap? = null
    var arrayListImgPath: ArrayList<CapturedImage>? = null
    var isToCaptureImage = false
    var isFromUpdatePost = false
    private var mNotificationMessageDialog: NotificationMessageDialog? = null
    private var  permissionsArray = arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
    private var runTimePermission: RunTimePermission? = null
    private var isFromAddNewPhoto = false

    /**
     * Xml Variables
     */
    var rL_rootview: RelativeLayout? = null
    var tV_upload: TextView? = null
    private var mSessionManager: SessionManager? = null
    override fun onCreate(savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_camera_kit)
//        ButterKnife.bind(this)
        mActivity = this@CameraKitActivity
        mSessionManager = SessionManager(mActivity!!)
        mNotificationMessageDialog = NotificationMessageDialog(mActivity)
        camera!!.setMethod(cameraMethod)
        camera!!.setCropOutput(cropOutput)
        initVarible()
        callIsStripeConnectApi()
    }

    private fun callIsStripeConnectApi() {
        try {
            if (CommonClass.isNetworkAvailable(mActivity!!)) {
                val request_datas = JSONObject()
                request_datas.put("token", mSessionManager!!.authToken)
                //                request_datas.put("accountId", "acct_1Gpy19CqyPeW9hWx");
                val url = ApiUrl.STRIPE_CONNECT
                OkHttp3Connection.doOkHttp3Connection(TAG, url, OkHttp3Connection.Request_type.POST, request_datas, object : OkHttp3RequestCallback {
                     override fun onSuccess(result: String?, user_tag: String?) {
                        try {
                            println("$TAG get category res=$result")
                            val categoryMainPojo: StripeMainPojo
                            val gson = Gson()
                            categoryMainPojo = gson.fromJson(result, StripeMainPojo::class.java)
                            when (categoryMainPojo.code) {
                                "200" -> if (!categoryMainPojo.status) {
                                    callActivity(this@CameraKitActivity, categoryMainPojo.data!!.weburl, VariableConstants.STRIPE_ACCOUNT_ADDED_HOME)
                                }
                                "401" -> CommonClass.sessionExpired(mActivity!!)
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                     override fun onError(error: String?, user_tag: String?) {
                        Log.e(TAG, error)
                    }
                })
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun initVarible() {
        try {
            isToCaptureImage = false

            runTimePermission = RunTimePermission(mActivity!!, permissionsArray, true)
            if (!runTimePermission!!.checkPermissions(permissionsArray)) {
                runTimePermission!!.requestPermission()
            }

            // receive datas from last activity
            val intent: Intent = getIntent()
            isFromUpdatePost = intent.getBooleanExtra("isUpdatePost", false)

            // set status bar color
            CommonClass.statusBarColor(mActivity!!)
            arrayListImgPath = ArrayList()

            // root element
            rL_rootview = findViewById(R.id.rL_rootview) as RelativeLayout?


            // Adapter
            tV_upload = findViewById(R.id.tV_upload) as TextView?
            imagesHorizontalRvAdap = ImagesHorizontalRvAdap(mActivity!!, arrayListImgPath!!, tV_upload, isToCaptureImage)
            val rV_cameraImages: RecyclerView = findViewById(R.id.rV_cameraImages) as RecyclerView
            val linearLayoutManager = LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false)
            rV_cameraImages.setLayoutManager(linearLayoutManager)
            rV_cameraImages.setAdapter(imagesHorizontalRvAdap)
            if (getIntent() != null) isFromAddNewPhoto = getIntent().getBooleanExtra("isFromAddNewPhoto", false)
            if (isFromAddNewPhoto) {
                val previousPhotoList = getIntent().getSerializableExtra("arrayListImgPath") as ArrayList<CapturedImage>
                arrayListImgPath!!.addAll(previousPhotoList)
                imagesHorizontalRvAdap!!.notifyDataSetChanged()
                tV_upload!!.setTextColor(ContextCompat.getColor(mActivity!!, R.color.pink_color))
            }
            if (isFromUpdatePost) {
                if (getIntent() != null && getIntent().hasExtra("arrayListImgPath")) {
                    val uploadedPhoto = getIntent().getSerializableExtra("arrayListImgPath") as ArrayList<ProductImageDatas>
                    val listUploaded = ArrayList<CapturedImage>()
                    for (photoSize in uploadedPhoto.indices) {
                        val uploaded = CapturedImage()
                        uploaded.imagePath = uploadedPhoto[photoSize].mainUrl
                        uploaded.rotateAngle = 90
                        listUploaded.add(uploaded)
                    }
                    arrayListImgPath!!.addAll(listUploaded)
                    imagesHorizontalRvAdap!!.notifyDataSetChanged()
                }
            }
        }catch (e:Exception){e.printStackTrace()}
    }

//    @OnClick(R.id.rL_done)
    fun done(v:View) {
        val intent: Intent
        val galleryImagePaths = ArrayList<String>()
        val rotationAngles = ArrayList<Int>()
        for (i in arrayListImgPath!!.indices) {
            galleryImagePaths.add(arrayListImgPath!![i]!!.imagePath!!)
            rotationAngles.add(arrayListImgPath!![i]!!.rotateAngle)
        }
        if (isFromUpdatePost) {
            intent = Intent()
            intent.putExtra("arrayListImgPath", galleryImagePaths)
            intent.putExtra("rotationAngles", rotationAngles)
            setResult(VariableConstants.UPDATE_IMAGE_REQ_CODE, intent)
            onBackPressed()
        } else {
            if (arrayListImgPath!!.size > 0) {
                if (!isFromAddNewPhoto) {
                    intent = Intent(mActivity, PostProductActivity::class.java)
                    intent.putExtra("arrayListImgPath", galleryImagePaths)
                    intent.putExtra("rotationAngles", rotationAngles)
                    startActivityForResult(intent, VariableConstants.CAMERA_IMAGE_LIST_REQ)
                    finish()
                } else {
                    intent = Intent()
                    intent.putExtra("arrayListImgPath", galleryImagePaths)
                    intent.putExtra("rotationAngles", rotationAngles)
                    setResult(VariableConstants.CAMERA_IMAGE_LIST_REQ, intent)
                    finish()

                    //onBackPressed();
                }
            }
        }
    }

//    @OnClick(R.id.rL_cancel_btn)
    fun cancle(v:View) {
        onBackPressed()
    }

    override fun onResume() {
        super.onResume()
        if (camera != null) camera!!.start()
        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.REGISTRATION_COMPLETE))

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.PUSH_NOTIFICATION))

        // clear the notification area when the app is opened
        clearNotifications(getApplicationContext())
    }

   override fun onPause() {
        if (camera != null) camera!!.stop()
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!)
        super.onPause()
    }

 override fun onBackPressed() {
        super.onBackPressed()
    }

    override fun onItemClick(view: View?, position: Int) {
        val intent: Intent
        when (position) {
            1 -> {
                println(TAG + " " + "captured image size=" + arrayListImgPath!!.size)
                intent = Intent(mActivity, GalleryActivity::class.java)
                val galleryImagePaths = ArrayList<String>()
                val rotationAngles = ArrayList<Int>()
                var i = 0
                while (i < arrayListImgPath!!.size) {
                    galleryImagePaths.add(arrayListImgPath!![i]!!.imagePath!!)
                    rotationAngles.add(arrayListImgPath!![i]!!.rotateAngle)
                    i++
                }
                intent.putExtra("arrayListImgPath", galleryImagePaths)
                intent.putExtra("rotationAngles", rotationAngles)
                intent.putExtra("isFromAddNewPhoto", isFromAddNewPhoto)
                startActivityForResult(intent, VariableConstants.SELECT_GALLERY_IMG_REQ_CODE)
            }
        }
    }

   override  protected fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        println("$TAG on activity result called...$requestCode")
        if (data != null) {
            when (requestCode) {
                VariableConstants.SELECT_GALLERY_IMG_REQ_CODE -> if (data.getStringArrayListExtra("arrayListImgPath") != null && data.getStringArrayListExtra("arrayListImgPath").size > 0) {
                    tV_upload!!.setTextColor(ContextCompat.getColor(mActivity!!, R.color.pink_color))
                    arrayListImgPath!!.clear()
                    val imagePathsFromGallery = data.getStringArrayListExtra("arrayListImgPath")
                    isFromAddNewPhoto = data.getBooleanExtra("isFromAddNewPhoto", false)
                    var image: CapturedImage
                    var i = 0
                    while (i < imagePathsFromGallery.size) {
                        image = CapturedImage()
                        image.rotateAngle = 0
                        image.imagePath = imagePathsFromGallery[i]
                        arrayListImgPath!!.add(image)
                        i++
                    }
                    //                        arrayListImgPath.addAll(data.getStringArrayListExtra("arrayListImgPath"));
                    imagesHorizontalRvAdap!!.notifyDataSetChanged()
                } else tV_upload!!.setTextColor(ContextCompat.getColor(mActivity!!, R.color.reset_button_bg))
                VariableConstants.CAMERA_IMAGE_LIST_REQ -> {
                    val list = data.getSerializableExtra("arrayListImgPath") as ArrayList<CapturedImage>
                    if (list != null) {
                        arrayListImgPath!!.clear()
                        arrayListImgPath!!.addAll(list)
                        imagesHorizontalRvAdap!!.notifyDataSetChanged()
                    }
                }
                VariableConstants.STRIPE_ACCOUNT_ADDED_HOME -> finish()
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, @NonNull permissions: Array<String?>, @NonNull grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            VariableConstants.PERMISSION_REQUEST_CODE -> {
                println("grant result=" + grantResults.size)
                if (grantResults.size > 0) {
                    var count = 0
                    while (count < grantResults.size) {
                        if (grantResults[count] != PackageManager.PERMISSION_GRANTED) runTimePermission!!.allowPermissionAlert(permissions[count]!!)
                        count++
                    }
                    println("isAllPermissionGranted=" + runTimePermission!!.checkPermissions(permissionsArray))
                    if (runTimePermission!!.checkPermissions(permissionsArray)) {
                        println("$TAG openCamera from onRequestPermissionsResult")
                    }
                }
            }
        }
    }

    private abstract class CameraSetting {
        abstract val title: String
        abstract val value: String?
        abstract fun toggle()
    }

    private val captureMethodSetting: CameraSetting = object : CameraSetting() {
        override val title: String
            get() = "ckMethod"

        override val value: String?
            get() = when (cameraMethod) {
                CameraKit.Constants.METHOD_STANDARD -> {
                    "standard"
                }
                CameraKit.Constants.METHOD_STILL -> {
                    "still"
                }
                else -> null
            }

        override fun toggle() {
            cameraMethod = if (cameraMethod == CameraKit.Constants.METHOD_STANDARD) {
                CameraKit.Constants.METHOD_STILL
            } else {
                CameraKit.Constants.METHOD_STANDARD
            }
            camera!!.setMethod(cameraMethod)
        }
    }
    private val cropSetting: CameraSetting = object : CameraSetting() {
        override val title: String
            get() = "ckCropOutput"

        override val value: String?
            get() = if (cropOutput) {
                "true"
            } else {
                "false"
            }

        override fun toggle() {
            cropOutput = !cropOutput
            camera!!.setCropOutput(cropOutput)
        }
    }

    override fun onStop() {
        if (camera != null) {
            camera!!.stop()
        }
        super.onStop()
    }

    companion object {
        private val TAG = CameraKitActivity::class.java.simpleName
    }
}