package com.zoptal.cellableapp.main.camera_kit

import androidx.annotation.Nullable
import com.wonderkiln.camerakit.Size
import java.io.File

object ResultHolder {
    @get:Nullable
    var image: ByteArray?=null

    @get:Nullable
    var video: File? = null

    @get:Nullable
    var nativeCaptureSize: Size? = null
    var timeToCallback: Long = 0

    fun dispose() {
        image = null
        nativeCaptureSize = null
        timeToCallback = 0
    }
}