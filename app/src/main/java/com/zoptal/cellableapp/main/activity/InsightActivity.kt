package com.zoptal.cellableapp.main.activity

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Typeface
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.annotation.Nullable
import androidx.appcompat.widget.PopupMenu
import androidx.core.content.ContextCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager

import com.google.gson.Gson
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.fcm_push_notification.Config
import com.zoptal.cellableapp.fcm_push_notification.NotificationMessageDialog
import com.zoptal.cellableapp.fcm_push_notification.NotificationUtils.Companion.clearNotifications
import com.zoptal.cellableapp.main.activity.InsightActivity
import com.zoptal.cellableapp.pojo_class.insight_pojo.InsightMainPojo
import com.zoptal.cellableapp.pojo_class.insight_pojo.LocationInsightData
import com.zoptal.cellableapp.utility.*
import com.zoptal.cellableapp.utility.OkHttp3Connection.OkHttp3RequestCallback
import org.achartengine.ChartFactory
import org.achartengine.chart.BarChart
import org.achartengine.model.XYMultipleSeriesDataset
import org.achartengine.model.XYSeries
import org.achartengine.renderer.XYMultipleSeriesRenderer
import org.achartengine.renderer.XYSeriesRenderer
import org.json.JSONException
import org.json.JSONObject
import java.util.*

/**
 * <h>InsightActivity</h>
 *
 *
 * In this class we used to show the graphical represation for the user
 * post total clicks week, month or year wise.
 *
 * @since 21-Aug-17
 */
class InsightActivity : AppCompatActivity(), View.OnClickListener {
    private var tV_postedOn: TextView? = null
    private var tV_unique_click: TextView? = null
    private var tV_reviews: TextView? = null
    private var tV_total_click: TextView? = null
    private var tV_saved: TextView? = null
    private var mActivity: Activity? = null
    private var postId: String? = ""
    private var linear_rootElement: LinearLayout? = null
    private var mSessionManager: SessionManager? = null
    private var linear_location: LinearLayout? = null
    private var rL_duration: RelativeLayout? = null
    private var tV_duration: TextView? = null
    private var mNotificationMessageDialog: NotificationMessageDialog? = null
     override fun onCreate(@Nullable savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_insight)
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale)

        // receive data from last activity
        val intent: Intent = getIntent()
        postId = intent.getStringExtra("postId")
        mActivity = this@InsightActivity
        mNotificationMessageDialog = NotificationMessageDialog(mActivity)
        mSessionManager = SessionManager(mActivity!!)
        linear_rootElement = findViewById(R.id.linear_rootElement) as LinearLayout?
        initVariables()

        // Call get insight api
        getInsight(VariableConstants.WEEK)
    }

    private fun initVariables() {
        tV_postedOn = findViewById(R.id.tV_postedOn) as TextView?
        tV_unique_click = findViewById(R.id.tV_unique_click) as TextView?
        tV_reviews = findViewById(R.id.tV_reviews) as TextView?
        tV_total_click = findViewById(R.id.tV_total_click) as TextView?
        tV_saved = findViewById(R.id.tV_saved) as TextView?
        linear_location = findViewById(R.id.linear_location) as LinearLayout?
        rL_duration = findViewById(R.id.rL_duration) as RelativeLayout?
        rL_duration!!.setOnClickListener(this)
        tV_duration = findViewById(R.id.tV_duration) as TextView?

        //back button
        val rL_back_btn = findViewById(R.id.rL_back_btn) as RelativeLayout?
        rL_back_btn!!.setOnClickListener(this)
    }

    override fun onResume() {
        super.onResume()

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.REGISTRATION_COMPLETE))

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.PUSH_NOTIFICATION))

        // clear the notification area when the app is opened
        clearNotifications(getApplicationContext())
    }

   override fun onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!)
        super.onPause()
    }

    /**
     * <h>GetInsight</h>
     *
     *
     * In this method we used to do api call and pass values like week, month and year.
     *
     * @param durationType The string value like week, month and year
     */
    private fun getInsight(durationType: String?) {
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            // token, postId, durationType
            val request_datas = JSONObject()
            try {
                request_datas.put("token", mSessionManager!!.authToken)
                request_datas.put("postId", postId)
                request_datas.put("durationType", durationType)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            OkHttp3Connection.doOkHttp3Connection(TAG, ApiUrl.INSIGHT, OkHttp3Connection.Request_type.POST, request_datas, object : OkHttp3RequestCallback {
                override fun onSuccess(result: String?, user_tag: String?) {
                    println("$TAG insight response=$result")
                    val insightMainPojo: InsightMainPojo
                    val gson = Gson()
                    insightMainPojo = gson.fromJson(result, InsightMainPojo::class.java)
                    when (insightMainPojo.code) {
                        "200" -> if (insightMainPojo.data != null && insightMainPojo.data!!.size > 0) {
                            // type 1 i.e basic insight
                            val basicInsight = insightMainPojo.data!![0].basicInsight
                            if (basicInsight != null) {
                                val aL_basicInsightDatas = basicInsight.data
                                if (aL_basicInsightDatas != null && aL_basicInsightDatas.size > 0) {
                                    val totalViews: String
                                    val distinctViews: String
                                    val commented: String
                                    val likes: String
                                    var postedOn: String
                                    val basicInsightData = aL_basicInsightDatas[0]
                                    totalViews = basicInsightData!!.totalViews
                                    distinctViews = basicInsightData.distinctViews
                                    commented = basicInsightData.commented
                                    likes = basicInsightData.likes
                                    postedOn = basicInsightData.postedOn

                                    // posted on
                                    if (postedOn != null && !postedOn.isEmpty()) {
                                        println(TAG + " " + "posted on=" + CommonClass.getDate(postedOn))
                                        postedOn = getResources().getString(R.string.posted_on).toString() + " " + CommonClass.getDate(postedOn)
                                        tV_postedOn!!.text = postedOn
                                    }

                                    // Unique click
                                    if (distinctViews != null && !distinctViews.isEmpty()) tV_unique_click!!.text = distinctViews

                                    // Comments
                                    if (commented != null && !commented.isEmpty()) tV_reviews!!.text = commented

                                    // total reviews
                                    if (totalViews != null && !totalViews.isEmpty()) tV_total_click!!.text = totalViews

                                    // likes
                                    if (likes != null && !likes.isEmpty()) tV_saved!!.text = likes
                                }
                            }

                            // type 2 i.e Time Insight
                            val timeInsight = insightMainPojo.data!![1].timeInsight
                            if (timeInsight != null) {
                                val aL_count = timeInsight.data!!.count
                                val al_day = timeInsight.data!!.day
                                openChart(al_day!!, aL_count!!)
                            }

                            // set location insight
                            val locationInsight = insightMainPojo.data!![2].locationInsight
                            if (locationInsight != null) {
                                val arrayListLocation = locationInsight.data
                                if (arrayListLocation != null && arrayListLocation.size > 0) {
                                    inflateLocationInsight(arrayListLocation)
                                }
                            }
                        }
                    }
                }

                override fun onError(error: String?, user_tag: String?) {}
            })
        } else CommonClass.showSnackbarMessage(linear_rootElement, getResources().getString(R.string.NoInternetAccess))
    }

    fun popup() {
        val popup = PopupMenu(mActivity!!, rL_duration!!) //the v is the view that you click replace it with your menuitem like : menu.getItem(1)
        popup.getMenuInflater().inflate(R.menu.insight_duration_menu, popup.getMenu())
        popup.show()
        popup.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
         override   fun onMenuItemClick(item2: MenuItem?): Boolean {
                when (item2!!.itemId) {
                    R.id.item_week -> {
                        tV_duration!!.setText(getResources().getString(R.string.this_week))
                        getInsight(VariableConstants.WEEK)
                    }
                    R.id.item_month -> {
                        tV_duration!!.setText(getResources().getString(R.string.this_month))
                        getInsight(VariableConstants.MONTH)
                    }
                    R.id.item_year -> {
                        tV_duration!!.setText(getResources().getString(R.string.this_year))
                        getInsight(VariableConstants.YEAR)
                    }
                }
                return true
            }
        })
    }

    private fun inflateLocationInsight(arrayListLocation: ArrayList<LocationInsightData>?) {
        val inflater = this.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater?
        linear_location!!.removeAllViews()
        for (locCount in arrayListLocation!!.indices) {
            val view = inflater!!.inflate(R.layout.single_row_insight_location, null)
            val tV_country_name = view!!.findViewById<View?>(R.id.tV_country_name) as TextView
            val tV_totalViews = view.findViewById<View?>(R.id.tV_totalViews) as TextView
            val countrySname = arrayListLocation[locCount]!!.countrySname
            val totalViews = arrayListLocation[locCount]!!.totalViews

            // set country name
            if (countrySname != null && !countrySname.isEmpty()) {
                val loc = Locale("", countrySname)
                //locale.getCountry();
                tV_country_name.text = loc.displayCountry
            }

            // set total reviews
            if (totalViews != null && !totalViews.isEmpty()) tV_totalViews.text = totalViews

            // click on view
            view.setOnClickListener {
                println("$TAG pos=$locCount")
                val intent = Intent(mActivity, GetTotalClicksActivity::class.java)
                intent.putExtra("postId", postId)
                intent.putExtra("countrySname", countrySname)
                startActivity(intent)
            }
            linear_location!!.addView(view)
        }
    }

    private fun openChart(arrayList_day: ArrayList<String>?, aL_count: ArrayList<Int>?) {
        // Creating an XYSeries for Expense
        val expenseSeries = XYSeries(getResources().getString(R.string.click))

        // set count
        for (i in aL_count!!.indices) {
            expenseSeries.add(i.toDouble(), aL_count[i].toDouble())
        }
        println(TAG + " " + "max n0=" + Collections.max(aL_count))

        // Creating a dataset to hold  series
        val dataset = XYMultipleSeriesDataset()

        // Adding Income Series to the dataset
        dataset.addSeries(expenseSeries)

        // Creating XYSeriesRenderer to customize expenseSeries
        val expenseRenderer = XYSeriesRenderer()
        expenseRenderer.color = ContextCompat.getColor(mActivity!!, R.color.purple_color) //color of the graph set to cyan
        expenseRenderer.isFillPoints = true
        expenseRenderer.lineWidth = 100f
        expenseRenderer.isDisplayChartValues = true
        expenseRenderer.isDisplayChartValues = true

        // Creating a XYMultipleSeriesRenderer to customize the whole chart
        val multiRenderer = XYMultipleSeriesRenderer()
        multiRenderer.orientation = XYMultipleSeriesRenderer.Orientation.HORIZONTAL
        multiRenderer.xLabels = 0
        multiRenderer.xLabelsColor = ContextCompat.getColor(mActivity!!, R.color.text_color)
        multiRenderer.setYLabelsColor(0, ContextCompat.getColor(mActivity!!, R.color.text_color))
        multiRenderer.labelsColor = ContextCompat.getColor(mActivity!!, R.color.text_color)
        //multiRenderer.setYTitle(getResources().getString(R.string.click));
        /***
         * Customizing graphs
         */
        //setting text size of the axis title
        multiRenderer.axisTitleTextSize = 30f

        //setting text size of the graph lable
        multiRenderer.labelsTextSize = 30f
        multiRenderer.labelsColor = ContextCompat.getColor(mActivity!!, R.color.purple_color)

        //setting zoom buttons visiblity
        multiRenderer.isZoomButtonsVisible = true

        //setting pan enablity which uses graph to move on both axis
        multiRenderer.setPanEnabled(false, false)

        //setting click false on graph
        multiRenderer.isClickEnabled = false

        //setting zoom to false on both axis
        multiRenderer.setZoomEnabled(false, false)

        //setting lines to display on y axis
        multiRenderer.isShowGridY = false

        //setting lines to display on x axis
        multiRenderer.isShowGridX = false

        //setting legend to fit the screen size
        multiRenderer.isFitLegend = true

        //setting displaying line on grid
        multiRenderer.setShowGrid(false)

        //setting zoom to false
        multiRenderer.isZoomEnabled = false

        //setting external zoom functions to false
        multiRenderer.isExternalZoomEnabled = false

        //setting displaying lines on graph to be formatted(like using graphics)
        multiRenderer.isAntialiasing = true

        //setting to in scroll to false
        multiRenderer.isInScroll = false

        //setting to set legend height of the graph
        multiRenderer.legendHeight = 50

        //setting x axis label align
        multiRenderer.xLabelsAlign = Paint.Align.LEFT

        //setting y axis label to align
        multiRenderer.setYLabelsAlign(Paint.Align.LEFT)

        //setting text style
        multiRenderer.setTextTypeface("sans_serif", Typeface.NORMAL)

        //setting no of values to display in y axis
        multiRenderer.yLabels = 10

        // setting y axis max value, Since i'm using static values inside the graph so i'm setting y max value to 4000.
        // if you use dynamic values then get the max y value and set here
        multiRenderer.yAxisMax = Collections.max(aL_count).toDouble()

        //setting used to move the graph on xaxiz to .5 to the right
        multiRenderer.xAxisMin = -0.5

        //setting max values to be display in x axis
        multiRenderer.xAxisMax = arrayList_day!!.size.toDouble()

        //setting bar size or space between two bars
        multiRenderer.barSpacing = 2.0

        //Setting background color of the graph to transparent
        multiRenderer.backgroundColor = Color.TRANSPARENT

        //Setting margin color of the graph to transparent
        multiRenderer.marginsColor = ContextCompat.getColor(mActivity!!, R.color.transparent_background)
        multiRenderer.isApplyBackgroundColor = true

        //setting the margin size for the graph in the order top, left, bottom, right
        multiRenderer.margins = intArrayOf(30, 30, 30, 30)

        // set day, week or month
        for (i in arrayList_day.indices) {
            multiRenderer.addXTextLabel(i.toDouble(), arrayList_day[i])
        }

        // Adding expenseRenderer to multipleRenderer
        multiRenderer.addSeriesRenderer(expenseRenderer)


        //this part is used to display graph on the xml
        val chartContainer = findViewById(R.id.chart_container) as LinearLayout?

        //remove any views before u paint the chart
        chartContainer!!.removeAllViews()

        //drawing bar chart
        val chart: View? = ChartFactory.getBarChartView(mActivity, dataset, multiRenderer, BarChart.Type.DEFAULT)

        //adding the view to the linearlayout
        chartContainer.addView(chart)
    }

 override fun onBackPressed() {
        finish()
        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.rL_back_btn -> onBackPressed()
            R.id.rL_duration -> popup()
        }
    }

    companion object {
        private val TAG: String? = InsightActivity::class.java.simpleName
    }
}