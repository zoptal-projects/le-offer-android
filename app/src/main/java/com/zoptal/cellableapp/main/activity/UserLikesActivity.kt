package com.zoptal.cellableapp.main.activity

import android.app.Activity
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import android.widget.RelativeLayout
import androidx.annotation.Nullable
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout

import com.google.gson.Gson
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.adapter.UserLikesRvAdap
import com.zoptal.cellableapp.fcm_push_notification.Config
import com.zoptal.cellableapp.fcm_push_notification.NotificationMessageDialog
import com.zoptal.cellableapp.fcm_push_notification.NotificationUtils.Companion.clearNotifications
import com.zoptal.cellableapp.main.activity.UserLikesActivity
import com.zoptal.cellableapp.pojo_class.user_likes_pojo.UserLikesMainPojo
import com.zoptal.cellableapp.pojo_class.user_likes_pojo.UserLikesResponseDatas
import com.zoptal.cellableapp.utility.ApiUrl
import com.zoptal.cellableapp.utility.CommonClass
import com.zoptal.cellableapp.utility.OkHttp3Connection
import com.zoptal.cellableapp.utility.OkHttp3Connection.OkHttp3RequestCallback
import com.zoptal.cellableapp.utility.SessionManager
import org.json.JSONException
import org.json.JSONObject
import java.util.*

/**
 * <h>UserLikesActivity</h>
 *
 *
 * In this class we used to show the list of user those who liked the product. Alongwith
 * the follow & following option to follow or unfollow.
 *
 * @since 4/28/2017
 * @version 1.0
 * @author 3Embed
 */
class UserLikesActivity : AppCompatActivity(), View.OnClickListener {
    private var mActivity: Activity? = null
    private var mSessionManager: SessionManager? = null
    private var postId = ""
    private var postType = ""
    private var rL_rootview: RelativeLayout? = null
    private var rL_noLikesFound: RelativeLayout? = null
    private var mProgress_bar: ProgressBar? = null
    private var rV_userLikes: RecyclerView? = null
    private var mRefreshLayout: SwipeRefreshLayout? = null
    private var aL_likesDatas: ArrayList<UserLikesResponseDatas>? = null
    private var userLikesRvAdap: UserLikesRvAdap? = null
    private var layoutManager: LinearLayoutManager? = null

    // Load more
    private var isLoading = false
    private var pageIndex = 0
    private val visibleThreshold = 5
    private var totalVisibleItem = 0
    private var totalItemCount = 0
    private var mNotificationMessageDialog: NotificationMessageDialog? = null
     override fun onCreate(@Nullable savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_user_likes)
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale)
        initVariables()
    }

    /**
     * <h>InitVariables</h>
     *
     *
     * In this method we used to initialize the all xml variables.
     *
     */
    private fun initVariables() {
        mActivity = this@UserLikesActivity
        mNotificationMessageDialog = NotificationMessageDialog(mActivity)
        CommonClass.statusBarColor(mActivity!!)

        // receiving variables
        val intent: Intent = getIntent()
        postId = intent.getStringExtra("postId")
        postType = intent.getStringExtra("postType")
        Log.d(TAG, "initVariables: postype $postType")
        pageIndex = 0
        aL_likesDatas = ArrayList()
        mSessionManager = SessionManager(mActivity!!)
        mRefreshLayout = findViewById(R.id.mRefreshLayout) as SwipeRefreshLayout?
        rL_rootview = findViewById(R.id.rL_rootview) as RelativeLayout?
        rL_noLikesFound = findViewById(R.id.rL_noLikesFound) as RelativeLayout?
        rL_noLikesFound!!.visibility = View.GONE
        val rL_back_btn = findViewById(R.id.rL_back_btn) as RelativeLayout
        rL_back_btn.setOnClickListener(this)
        mProgress_bar = findViewById(R.id.progress_bar) as ProgressBar?
        rV_userLikes = findViewById(R.id.rV_userLikes) as RecyclerView?

        // set recyclerview adapter
        userLikesRvAdap = UserLikesRvAdap(mActivity!!, aL_likesDatas!!)
        layoutManager = LinearLayoutManager(mActivity)
        rV_userLikes!!.setLayoutManager(layoutManager)
        rV_userLikes!!.setAdapter(userLikesRvAdap)

        // Pull to refresh
        mRefreshLayout!!.setOnRefreshListener(object : SwipeRefreshLayout.OnRefreshListener {
           override fun onRefresh() {
                pageIndex = 0
                aL_likesDatas!!.clear()
                getAllLikesApi(pageIndex)
            }
        })

        // call api call method
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            mProgress_bar!!.visibility = View.VISIBLE
            getAllLikesApi(pageIndex)
        } else CommonClass.showSnackbarMessage(rL_rootview, getResources().getString(R.string.NoInternetAccess))
    }

    override fun onResume() {
        super.onResume()

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.REGISTRATION_COMPLETE))

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.PUSH_NOTIFICATION))

        // clear the notification area when the app is opened
        clearNotifications(getApplicationContext())
    }

   override fun onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!)
        super.onPause()
    }

    /**
     * <h>GetAllLikesApi</h>
     *
     *
     * In this method we used to do the api call using OkHttp client
     * to get all likes users.
     *
     */
    private fun getAllLikesApi(offset: Int) {
        var offset = offset
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            val limit = 20
            offset = limit * offset
            val request_datas = JSONObject()
            try {
                request_datas.put("postId", postId)
                request_datas.put("postType", postType)
                request_datas.put("offset", offset)
                request_datas.put("limit", limit)
                request_datas.put("token", mSessionManager!!.authToken)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            println("$TAG offset=$offset")
            OkHttp3Connection.doOkHttp3Connection(TAG, ApiUrl.GET_ALL_LIKES, OkHttp3Connection.Request_type.POST, request_datas, object : OkHttp3RequestCallback {
                 override fun onSuccess(result: String?, user_tag: String?) {
                    println(TAG)
                    mProgress_bar!!.visibility = View.GONE
                    mRefreshLayout!!.setRefreshing(false)
                    val userLikesMainPojo: UserLikesMainPojo
                    val gson = Gson()
                    userLikesMainPojo = gson.fromJson(result, UserLikesMainPojo::class.java)
                    when (userLikesMainPojo.code) {
                        "200" -> {
                            mRefreshLayout!!.setRefreshing(false)
                            aL_likesDatas!!.addAll(userLikesMainPojo.data!!)
                            if (aL_likesDatas != null && aL_likesDatas!!.size > 0) {
                                userLikesRvAdap!!.notifyDataSetChanged()

                                // prevent loading more datas when item count is less then 15
                                if (aL_likesDatas!!.size < 15) {
                                    isLoading = true
                                }

                                // Load more
                                rV_userLikes!!.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                                    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                                        super.onScrolled(recyclerView, dx, dy)
                                        totalItemCount = layoutManager!!.getItemCount()
                                        totalVisibleItem = layoutManager!!.findLastVisibleItemPosition()
                                        if (!isLoading && totalItemCount <= totalVisibleItem + visibleThreshold) {
                                            pageIndex = pageIndex + 1
                                            getAllLikesApi(pageIndex)
                                        }
                                    }
                                })
                            } else rL_noLikesFound!!.visibility = View.VISIBLE
                        }
                        "401" -> CommonClass.sessionExpired(mActivity!!)
                        "56713" -> rL_noLikesFound!!.visibility = View.VISIBLE
                        else -> CommonClass.showSnackbarMessage(rL_rootview, userLikesMainPojo.message)
                    }
                }

                 override fun onError(error: String?, user_tag: String?) {
                    mProgress_bar!!.visibility = View.GONE
                    CommonClass.showSnackbarMessage(rL_rootview, error)
                }
            })
        } else CommonClass.showSnackbarMessage(rL_rootview, getResources().getString(R.string.NoInternetAccess))
    }

 override fun onBackPressed() {
        finish()
        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.rL_back_btn -> onBackPressed()
        }
    }

    companion object {
        private val TAG = UserLikesActivity::class.java.simpleName
    }
}