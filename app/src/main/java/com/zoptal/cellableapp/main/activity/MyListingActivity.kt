package com.zoptal.cellableapp.main.activity

import android.app.Activity
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView

import android.util.Base64
import android.util.Log
import android.view.View
import android.widget.*
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout

import com.google.gson.Gson
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.adapter.MyListingAdap
import com.zoptal.cellableapp.fcm_push_notification.Config
import com.zoptal.cellableapp.fcm_push_notification.NotificationMessageDialog
import com.zoptal.cellableapp.fcm_push_notification.NotificationUtils.Companion.clearNotifications
import com.zoptal.cellableapp.main.activity.MyListingActivity
import com.zoptal.cellableapp.main.activity.products.ProductDetailsActivity
import com.zoptal.cellableapp.main.camera_kit.CameraKitActivity
import com.zoptal.cellableapp.mqttchat.Activities.ChatMessageScreen
import com.zoptal.cellableapp.mqttchat.AppController
import com.zoptal.cellableapp.mqttchat.Utilities.MqttEvents
import com.zoptal.cellableapp.mqttchat.Utilities.Utilities
import com.zoptal.cellableapp.pojo_class.profile_selling_pojo.ProfileSellingData
import com.zoptal.cellableapp.pojo_class.profile_selling_pojo.ProfileSellingMainPojo
import com.zoptal.cellableapp.utility.*
import com.zoptal.cellableapp.utility.OkHttp3Connection.OkHttp3RequestCallback
import org.json.JSONException
import org.json.JSONObject
import java.util.*

/*
* In this Activity use same data of profile selling data.
* */
class MyListingActivity : AppCompatActivity(), View.OnClickListener, ProductItemClickListener {
    private var mActivity: Activity? = null
    private var mNotificationMessageDialog: NotificationMessageDialog? = null
    private var rV_myListing: RecyclerView? = null
    private var rL_noProductFound: RelativeLayout? = null
    private var arrayListSellingDatas: ArrayList<ProfileSellingData>? = null
    private var mSwipeRefreshLayout: SwipeRefreshLayout? = null
    private var pageIndex = 0
    private var progress_bar_profile: ProgressBar? = null
    private var gridLayoutManager: StaggeredGridLayoutManager? = null
    private var myListingAdap: MyListingAdap? = null
    private var mSessionManager: SessionManager? = null
    private var memberName: String? = null

    // Load more var
    private var isLoadingRequired = false
    private var visibleItemCount = 0
    private var totalItemCount = 0
    private val visibleThreshold = 5
    private var postId: String? = ""
    private var member_name: String? = ""
    private var receiverMqttId: String? = ""
    private var productName: String? = null
    private var productPicUrl: String? = null
    private val currency: String? = null
    private var price: String? = null
    private var memberPicUrl: String? = null
    private val fromChatScreen = "0"
    private var latitude: String? = ""
    private var longitude: String? = ""
    private var root_MyListing: RelativeLayout? = null
    private var rL_swap: RelativeLayout? = null
    private var progress_bar_save: ProgressBar? = null
    private var mSelected = -1
    private var swapPostId: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_my_listing)
        mActivity = this@MyListingActivity
        mNotificationMessageDialog = NotificationMessageDialog(mActivity)
        mSessionManager = SessionManager(mActivity!!)
        memberName = mSessionManager!!.userName
        initVar()
    }

    fun initVar() {
        val intent: Intent = getIntent()
        val data = intent.extras
        val place: String?
        if (data != null) {
            productPicUrl = data.getString("productPicUrl")
            productName = data.getString("productName")
            place = data.getString("place")
            postId = data.getString("postId")
            latitude = data.getString("latitude")
            longitude = data.getString("longitude")
            val currency = data.getString("currency")
            price = data.getString("price")
            memberPicUrl = data.getString("memberPicUrl")
            member_name = data.getString("membername")
            receiverMqttId = data.getString("receiverMqttId")
            val negotiable = data.getString("negotiable")
        }

        //root view
        root_MyListing = findViewById(R.id.root_MyListing) as RelativeLayout?

        // back button
        val rL_back_btn = findViewById(R.id.rL_back_btn) as RelativeLayout
        rL_back_btn.setOnClickListener(this)
        progress_bar_save = findViewById(R.id.progress_bar_save) as ProgressBar?
        rV_myListing = findViewById(R.id.rV_myListing) as RecyclerView?
        pageIndex = 0
        // set space equility between recycler view items
        val spanCount = 2 // 2 columns
        val spacing = 10 // 50px
        rV_myListing!!.addItemDecoration(SpacesItemDecoration(spanCount, spacing))
        mSwipeRefreshLayout = findViewById(R.id.swipeRefreshLayout) as SwipeRefreshLayout?
        progress_bar_profile = findViewById(R.id.progress_bar) as ProgressBar?
        arrayListSellingDatas = ArrayList()
        myListingAdap = MyListingAdap(mActivity!!, arrayListSellingDatas!!, this)
        gridLayoutManager = StaggeredGridLayoutManager(2, 1)
        rV_myListing!!.setLayoutManager(gridLayoutManager)
        rV_myListing!!.setAdapter(myListingAdap)

        // set empty favourite icon
        rL_noProductFound = findViewById(R.id.rL_noProductFound) as RelativeLayout?
        rL_noProductFound!!.visibility = View.GONE

        // set empty favourite icon
        rL_noProductFound = findViewById(R.id.rL_noProductFound) as RelativeLayout?
        rL_noProductFound!!.visibility = View.GONE
        val iV_default_icon = findViewById(R.id.iV_default_icon) as ImageView
        iV_default_icon.setImageResource(R.drawable.empty_selling_icon)
        val tV_no_ads = findViewById(R.id.tV_no_ads) as TextView
        tV_no_ads.setText(getResources().getString(R.string.no_ads_yet))
        val tV_snapNpost = findViewById(R.id.tV_snapNpost) as TextView
        tV_snapNpost.setText(getResources().getString(R.string.snapNpostIn))
        val tV_start_discovering = findViewById(R.id.tV_start_discovering) as TextView
        tV_start_discovering.setText(getResources().getString(R.string.start_selling))
        rL_swap = findViewById(R.id.rL_swap) as RelativeLayout?
        rL_swap!!.setOnClickListener(this)
        val rL_start_selling = findViewById(R.id.rL_start_selling) as RelativeLayout
        rL_start_selling.setOnClickListener { //startActivity(new Intent(mActivity, CameraActivity.class));
            /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                    startActivity(new Intent(mActivity, Camera2Activity.class));
                else*/
            startActivity(Intent(mActivity, CameraKitActivity::class.java))
        }

        // call api call method
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            progress_bar_profile!!.visibility = View.VISIBLE
            if (mSessionManager!!.isUserLoggedIn) {
                profilePosts(pageIndex)
            }
        } else CommonClass.showSnackbarMessage(root_MyListing, getResources().getString(R.string.NoInternetAccess))

        // pull to refresh
        mSwipeRefreshLayout!!.setColorSchemeResources(R.color.pink_color)
        mSwipeRefreshLayout!!.setOnRefreshListener(object : SwipeRefreshLayout.OnRefreshListener {
            override fun onRefresh() {
                pageIndex = 0
                arrayListSellingDatas!!.clear()
                myListingAdap!!.notifyDataSetChanged()
                if (mSessionManager!!.isUserLoggedIn) {
                    profilePosts(pageIndex)
                }
            }
        })
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.rL_back_btn -> onBackPressed()
            R.id.rL_swap -> if (mSelected >= 0) swapOffer()
        }
    }

    override fun onItemClick(pos: Int, imageView: ImageView?) {
        mSelected = pos
        swapPostId = arrayListSellingDatas!![mSelected].postId
        // Toast.makeText(mActivity,pos+"",Toast.LENGTH_SHORT).show();
    }

    private fun profilePosts(offset: Int) {
        var offset = offset
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            val limit = 20
            offset = limit * offset
            val request_datas = JSONObject()
            try {
                request_datas.put("token", mSessionManager!!.authToken)
                request_datas.put("limit", limit)
                request_datas.put("offset", offset)
                request_datas.put("sold", "0")
                request_datas.put("membername", memberName)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            val url: String
            Log.d("username111", memberName)
            Log.d("username112", mSessionManager!!.userName)
            if (mSessionManager!!.userName == memberName) {
                url = ApiUrl.PROFILE_POST
                Log.d("username113", url)
            } else {
                url = ApiUrl.PROFILE_POST + memberName
                Log.d("username114", url)
            }
            OkHttp3Connection.doOkHttp3Connection(TAG, url, OkHttp3Connection.Request_type.POST, request_datas, object : OkHttp3RequestCallback {
                 override fun onSuccess(result: String?, user_tag: String?) {
                    progress_bar_profile!!.visibility = View.GONE
                    println("$TAG profile selling res=$result")
                    mSwipeRefreshLayout!!.setRefreshing(false)

                    /*ExplorePojoMain explorePojoMain;
                    Gson gson = new Gson();
                    explorePojoMain = gson.fromJson(result, ExplorePojoMain.class);*/
                    val profileSellingMainPojo: ProfileSellingMainPojo
                    val gson = Gson()
                    profileSellingMainPojo = gson.fromJson(result, ProfileSellingMainPojo::class.java)
                    when (profileSellingMainPojo.code) {
                        "200" -> if (profileSellingMainPojo.data != null && profileSellingMainPojo.data!!.size > 0) {
                            rL_noProductFound!!.visibility = View.GONE
                            arrayListSellingDatas!!.addAll(profileSellingMainPojo.data!!)
                            isLoadingRequired = arrayListSellingDatas!!.size > 14
                            myListingAdap!!.notifyDataSetChanged()

                            // Load more
                            rV_myListing!!.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                                    super.onScrolled(recyclerView, dx, dy)
                                    val firstVisibleItemPositions = IntArray(2)
                                    totalItemCount = gridLayoutManager!!.getItemCount()
                                    visibleItemCount = gridLayoutManager!!.findLastVisibleItemPositions(firstVisibleItemPositions).get(0)
                                    if (isLoadingRequired && totalItemCount <= visibleItemCount + visibleThreshold) {
                                        isLoadingRequired = false
                                        pageIndex = pageIndex + 1
                                        mSwipeRefreshLayout!!.setRefreshing(true)
                                        profilePosts(pageIndex)
                                    }
                                }
                            })
                        }
                        "204" -> {
                            mSwipeRefreshLayout!!.setRefreshing(false)
                            if (arrayListSellingDatas!!.size == 0) {
                                rL_noProductFound!!.visibility = View.VISIBLE
                            }
                        }
                        "401" -> {
                            mSwipeRefreshLayout!!.setRefreshing(false)
                            CommonClass.sessionExpired(mActivity!!)
                        }
                        else -> {
                            mSwipeRefreshLayout!!.setRefreshing(false)
                            CommonClass.showTopSnackBar(root_MyListing, profileSellingMainPojo.message)
                        }
                    }
                }

                 override fun onError(error: String?, user_tag: String?) {
                    mSwipeRefreshLayout!!.setRefreshing(false)
                    progress_bar_profile!!.visibility = View.GONE
                    CommonClass.showSnackbarMessage(root_MyListing, error)
                }
            })
        } else CommonClass.showSnackbarMessage(root_MyListing, getResources().getString(R.string.NoInternetAccess))
    }

    /*
   * Handel make offer button data.*/
    private fun handelButton(isEnable: Boolean) {
        if (isEnable) {
            progress_bar_save!!.visibility = View.VISIBLE
            rL_swap!!.isEnabled = false
        } else {
            rL_swap!!.isEnabled = true
            progress_bar_save!!.visibility = View.GONE
        }
    }

    /*
     *swap offer api */
    private var requestDats: JSONObject? = null
    private fun swapOffer() {
        if (receiverMqttId == null || receiverMqttId!!.isEmpty()) {
            Toast.makeText(this, R.string.mqtt_user_not_text, Toast.LENGTH_SHORT).show()
            return
        }
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            handelButton(true)
            requestDats = JSONObject()
            try {
                requestDats!!.put("token", mSessionManager!!.authToken)
                //requestDats.put("offerStatus","1");
                requestDats!!.put("postId", postId)
                requestDats!!.put("swapPostId", swapPostId)
                requestDats!!.put("swapStatus", "1")
                requestDats!!.put("price", price)
                //requestDats.put("type","0");
                requestDats!!.put("membername", member_name)
                requestDats!!.put("sendchat", createMessageObject(price))
            } catch (e: Exception) {
                e.printStackTrace()
                handelButton(false)
            }
            OkHttp3Connection.doOkHttp3Connection("", ApiUrl.SWAP_OFFER, OkHttp3Connection.Request_type.POST, requestDats!!, object : OkHttp3RequestCallback {
                 override fun onSuccess(result: String?, user_tag: String?) {
                    println("$TAG swap offer respone=$result")
                    handelButton(false)
                    try {
                        handelResponse(result!!)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

                 override fun onError(error: String?, user_tag: String?) {
                    handelButton(false)
                    CommonClass.showSnackbarMessage(root_MyListing, error)
                }
            })
        } else CommonClass.showSnackbarMessage(root_MyListing, getResources().getString(R.string.NoInternetAccess))
    }

    /*
     *creating the msg object */
    private var doucumentId: String? = null
    private var isNew = false

    @Throws(Exception::class)
    private fun createMessageObject(amount: String?): JSONObject {
        doucumentId = AppController.instance?.findDocumentIdOfReceiver(receiverMqttId, postId)
        if (doucumentId!!.isEmpty()) {
            isNew = true
            doucumentId = AppController.findDocumentIdOfReceiver(receiverMqttId!!, Utilities.tsInGmt(), member_name, memberPicUrl, postId!!, false, "", "", productPicUrl, productName, price, false, false)
        } else {
            isNew = false
            AppController.instance?.dbController!!.updateChatDetails(doucumentId, member_name, memberPicUrl)
        }
        Log.d("log88", "" + amount)
        val swapText: String = getString(R.string.i_would_like_to_offer_my).toString() + " " + arrayListSellingDatas!![mSelected].productName + " " + getString(R.string.in_place_of_your) + " " + productName
        val byteArray = swapText.toByteArray(charset("UTF-8"))
        val messageInbase64 = Base64.encodeToString(byteArray, Base64.DEFAULT).trim { it <= ' ' }
        val tsForServer = Utilities.tsInGmt()
        val tsForServerEpoch = Utilities().gmtToEpoch(tsForServer)
        val messageData = JSONObject()
        messageData.put("name", mSessionManager!!.userName)
        messageData.put("from", mSessionManager!!.getmqttId())
        messageData.put("to", receiverMqttId)
        messageData.put("payload", messageInbase64)
        messageData.put("type", "17") // for swap we use 17
        messageData.put("offerType", "5") // here we send random which is not use
        messageData.put("id", tsForServerEpoch)
        messageData.put("secretId", postId)
        messageData.put("thumbnail", "")
        messageData.put("userImage", mSessionManager!!.userImage)
        messageData.put("toDocId", doucumentId)
        messageData.put("dataSize", 1)
        messageData.put("isSold", "0")
        messageData.put("productUrl", productPicUrl)
        messageData.put("productId", postId)
        messageData.put("productName", productName)
        messageData.put("productPrice", "" + price)
        messageData.put("swapType", "1")
        messageData.put("isSwap", "1")
        messageData.put("swapProductName", arrayListSellingDatas!![mSelected].productName)
        messageData.put("swapProductUrl", arrayListSellingDatas!![mSelected].thumbnailImageUrl)
        messageData.put("swapProductId", arrayListSellingDatas!![mSelected].postId)
        messageData.put("swapTextMessage", swapText)
        messageData.put("isRejected", "0")
        return messageData
    }

    /*
    * Handling the response data.*/
    @Throws(Exception::class)
    private fun handelResponse(resaponse: String) {
        val jsonObject = JSONObject(resaponse)
        val code_Data = jsonObject.getString("code")
        when (code_Data) {
            "200" -> {
                AppController.instance?.sendMessageToFcm(MqttEvents.OfferMessage.toString() + "/" + requestDats!!.getJSONObject("sendchat").getString("to"), requestDats!!.getJSONObject("sendchat"))
                val intent: Intent
                intent = Intent(this, ChatMessageScreen::class.java)
                intent.putExtra("isNew", isNew)
                intent.putExtra("receiverUid", receiverMqttId)
                intent.putExtra("receiverName", member_name)
                intent.putExtra("documentId", doucumentId)
                intent.putExtra("receiverIdentifier", AppController.instance?.userIdentifier)
                intent.putExtra("receiverImage", memberPicUrl)
                intent.putExtra("colorCode", AppController.instance?.getColorCode(1 % 19))
                //                intent.putExtra("swapProduct",true);
                intent.putExtra("productPicUrl", productPicUrl)
                intent.putExtra("swapProductpicUrl", arrayListSellingDatas!![mSelected].mainUrl)
                intent.putExtra("swapProductId", arrayListSellingDatas!![mSelected].postId)
                val swapText: String = getString(R.string.i_would_like_to_offer_my).toString() + " " + arrayListSellingDatas!![mSelected].productName + " " + getString(R.string.in_place_of_your) + " " + productName
                intent.putExtra("swapTextMsg", swapText)
                if (fromChatScreen == "0") {
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    startActivity(intent)
                } else {
                    ProductDetailsActivity.productDetailsActivity!!.finish()
                }
                mActivity!!.finish()
            }
            "401" -> CommonClass.sessionExpired(mActivity!!)
            "409" -> CommonClass.showTopSnackBar(root_MyListing, getString(R.string.already_sold_Text))
            else -> CommonClass.showTopSnackBar(root_MyListing, jsonObject.getString("message"))
        }
    }

    override fun onResume() {
        super.onResume()

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.REGISTRATION_COMPLETE))

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.PUSH_NOTIFICATION))

        // clear the notification area when the app is opened
        clearNotifications(getApplicationContext())
    }

   override fun onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!)
        super.onPause()
    }

    companion object {
        private val TAG = MyListingActivity::class.java.simpleName
    }
}