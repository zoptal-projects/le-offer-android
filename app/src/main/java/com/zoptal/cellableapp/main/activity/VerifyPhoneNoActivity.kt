package com.zoptal.cellableapp.main.activity

import android.app.Activity
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.*
import androidx.annotation.Nullable
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.gson.Gson
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.county_code_picker.Country
import com.zoptal.cellableapp.county_code_picker.DialogCountryList
import com.zoptal.cellableapp.county_code_picker.SetCountryCodeListener
import com.zoptal.cellableapp.fcm_push_notification.Config
import com.zoptal.cellableapp.fcm_push_notification.NotificationMessageDialog
import com.zoptal.cellableapp.fcm_push_notification.NotificationUtils.Companion.clearNotifications
import com.zoptal.cellableapp.main.activity.ProfileNumberVerifyActivity
import com.zoptal.cellableapp.main.activity.VerifyPhoneNoActivity
import com.zoptal.cellableapp.pojo_class.profile_mobile_otp.ProfileNumberOtpMain
import com.zoptal.cellableapp.utility.ApiUrl
import com.zoptal.cellableapp.utility.CommonClass
import com.zoptal.cellableapp.utility.OkHttp3Connection
import com.zoptal.cellableapp.utility.OkHttp3Connection.OkHttp3RequestCallback
import com.zoptal.cellableapp.utility.SessionManager
import org.json.JSONException
import org.json.JSONObject
import java.util.*

/**
 * <h>VerifyPhoneNoActivity</h>
 *
 *
 * This class is getting called from EditProfileActivity class. In this class we used to accept the
 * new mobile number from user and used to send otp to the user, Once we get success from server then
 * launch the verification screen.
 *
 * @since 10-Jul-17
 */
class VerifyPhoneNoActivity : AppCompatActivity(), View.OnClickListener {
    private var arrayListCountry: ArrayList<Country>? = null
    private var dialogCountryList: DialogCountryList? = null
    private var mActivity: Activity? = null
    private var countryIsoNumber = ""
    private var tV_country_iso_no: TextView? = null
    private var tV_country_code: TextView? = null
    private var tV_send: TextView? = null
    private var eT_mobileNo: EditText? = null
    private var mSessionManager: SessionManager? = null
    private var progress_bar: ProgressBar? = null
    private var linear_rootElement: LinearLayout? = null
    private var isSendButtonEnabled = false
    private var mNotificationMessageDialog: NotificationMessageDialog? = null
    private var isToStartActivity = false
     override fun onCreate(@Nullable savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale)
        setContentView(R.layout.activity_verify_number)
        initVariables()
    }

    /**
     * <h>initVariables</h>
     *
     *
     * In this method we used to initialize all variables.
     *
     */
    private fun initVariables() {
        verifyNumberInstance = this@VerifyPhoneNoActivity
        isToStartActivity = true
        arrayListCountry = ArrayList()
        mActivity = this@VerifyPhoneNoActivity
        mNotificationMessageDialog = NotificationMessageDialog(mActivity)
        mSessionManager = SessionManager(mActivity!!)
        tV_country_iso_no = findViewById(R.id.tV_country_iso_no) as TextView?
        tV_country_code = findViewById(R.id.tV_country_code) as TextView?
        tV_send = findViewById(R.id.tV_send) as TextView?
        eT_mobileNo = findViewById(R.id.eT_mobileNo) as EditText?
        progress_bar = findViewById(R.id.progress_bar) as ProgressBar?
        progress_bar!!.visibility = View.GONE
        linear_rootElement = findViewById(R.id.linear_rootElement) as LinearLayout?

        // open currency picker dialog
        val rL_country_picker = findViewById(R.id.rL_country_picker) as RelativeLayout
        rL_country_picker.setOnClickListener(this)

        // Back button
        val rL_back_btn = findViewById(R.id.rL_back_btn) as RelativeLayout
        rL_back_btn.setOnClickListener(this)
        val rL_send = findViewById(R.id.rL_send) as RelativeLayout
        rL_send.setOnClickListener(this)

        // EditText Email Address
        CommonClass.setViewOpacity(mActivity, rL_send, 102, R.drawable.rect_purple_color_with_solid_shape)
        eT_mobileNo!!.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                val mobileNo = eT_mobileNo!!.text.toString()
                if (!mobileNo.isEmpty() && mobileNo.length == 10) {
                    isSendButtonEnabled = true
                    CommonClass.setViewOpacity(mActivity, rL_send, 204, R.drawable.rect_purple_color_with_solid_shape)
                } else {
                    isSendButtonEnabled = false
                    CommonClass.setViewOpacity(mActivity, rL_send, 102, R.drawable.rect_purple_color_with_solid_shape)
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })

        // call method to assign currency
        countryCodeList
    }

    override fun onResume() {
        super.onResume()
        isToStartActivity = true
        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.REGISTRATION_COMPLETE))

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.PUSH_NOTIFICATION))

        // clear the notification area when the app is opened
        clearNotifications(getApplicationContext())
    }

   override fun onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!)
        super.onPause()
    }

    /**
     * <h>GetCountryCodeList</h>
     *
     *
     * In this method we used to get the all country iso code and number into
     * list. And find the user current country iso code and number and set
     * before the mobile number.
     *
     */
    private val countryCodeList: Unit
        private get() {
            val country_array: Array<String> = getResources().getStringArray(R.array.countryCodes)
            if (country_array.size > 0) {
                for (aCountry_array in country_array) {
                    var getCountryList: Array<String>
                    getCountryList = aCountry_array.split(",".toRegex()).toTypedArray()
                    var countryCode: String
                    var countryName: String
                    countryCode = getCountryList[0]
                    countryName = getCountryList[1]
                    val country = Country()
                    country.code = countryCode.trim { it <= ' ' }
                    country.name = countryName.trim { it <= ' ' }
                    arrayListCountry!!.add(country)
                }
                if (arrayListCountry!!.size > 0) {
                    dialogCountryList = DialogCountryList(mActivity!!, arrayListCountry!!)
                    val countryIsoCode = Locale.getDefault().country
                    if (countryIsoCode != null && !countryIsoCode.isEmpty()) {
                        val countryIsoNo = setCurrentCountryCode(countryIsoCode)
                        countryIsoNumber = getResources().getString(R.string.plus).toString() + countryIsoNo
                        println("$TAG countryIsoNumber=$countryIsoNumber")
                        tV_country_iso_no!!.text = countryIsoNo
                        tV_country_code!!.text = countryIsoCode
                    }
                }
            }
        }

    /**
     * <h>SetCurrentCountryCode</h>
     *
     *
     * In this method we used to find the country iso number by giving its
     * iso code.
     *
     * @param isoCode The iso code of the country
     * @return it returns the country iso number e.g +91
     */
    private fun setCurrentCountryCode(isoCode: String): String {
        var countryCode = ""
        for (country in arrayListCountry!!) {
            if (country.name == isoCode) {
                countryCode = country.code
                return countryCode
            }
        }
        return countryCode
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.rL_country_picker -> if (dialogCountryList != null) {
                dialogCountryList!!.showCountryCodePicker(object : SetCountryCodeListener {
                    override fun getCode(code: String?, name: String?) {
                        var code = code
                        countryIsoNumber = getResources().getString(R.string.plus).toString() + code
                        code = getResources().getString(R.string.plus).toString() + code
                        tV_country_iso_no!!.text = code
                        tV_country_code!!.text = name
                    }
                })
            }
            R.id.rL_send -> if (isSendButtonEnabled) generateProfileMobOtp()
            R.id.rL_back_btn -> onBackPressed()
        }
    }

    /**
     * <h>generateProfileMobOtp</h>
     *
     *
     * In this method we used to send call generate otp api to send otp to given
     * mobile number.
     *
     */
    private fun generateProfileMobOtp() {
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            progress_bar!!.visibility = View.VISIBLE
            tV_send!!.visibility = View.GONE
            val request_datas = JSONObject()
            try {
                request_datas.put("token", mSessionManager!!.authToken)
                request_datas.put("phoneNumber", countryIsoNumber + eT_mobileNo!!.text.toString())
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            OkHttp3Connection.doOkHttp3Connection(TAG, ApiUrl.OTP_PROFILE_NUMBER, OkHttp3Connection.Request_type.POST, request_datas, object : OkHttp3RequestCallback {
                 override fun onSuccess(result: String?, user_tag: String?) {
                    progress_bar!!.visibility = View.GONE
                    tV_send!!.visibility = View.VISIBLE
                    println("$TAG generate profile otp res=$result")
                    val profileNumberOtpMain: ProfileNumberOtpMain
                    val gson = Gson()
                    profileNumberOtpMain = gson.fromJson(result, ProfileNumberOtpMain::class.java)
                    when (profileNumberOtpMain.code) {
                        "200" -> if (isToStartActivity) {
                            isToStartActivity = false
                            val otp = CommonClass.extractNumberFromString(profileNumberOtpMain.data!!.body)
                            if (otp != null && !otp.isEmpty()) {
                                println("$TAG profile otp=$otp")
                                val intent = Intent(mActivity, ProfileNumberVerifyActivity::class.java)
                                intent.putExtra("otpCode", otp)
                                intent.putExtra("phoneNumber", countryIsoNumber + eT_mobileNo!!.text.toString())
                                startActivity(intent)
                            }
                        }
                        "401" -> CommonClass.sessionExpired(mActivity!!)
                        else -> CommonClass.showSnackbarMessage(linear_rootElement, profileNumberOtpMain.message)
                    }
                }

                 override fun onError(error: String?, user_tag: String?) {
                    progress_bar!!.visibility = View.GONE
                    tV_send!!.visibility = View.VISIBLE
                    CommonClass.showSnackbarMessage(linear_rootElement, error)
                }
            })
        } else CommonClass.showSnackbarMessage(linear_rootElement, getResources().getString(R.string.NoInternetAccess))
    }

 override fun onBackPressed() {
        finish()
        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate)
    }

    companion object {
        private val TAG = VerifyPhoneNoActivity::class.java.simpleName
        var verifyNumberInstance: Activity? = null
    }
}