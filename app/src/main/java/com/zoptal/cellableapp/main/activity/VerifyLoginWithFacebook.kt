package com.zoptal.cellableapp.main.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.gson.Gson
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.pojo_class.verify_fb_login_pojo.VerifyFacebookPojo
import com.zoptal.cellableapp.utility.ApiUrl
import com.zoptal.cellableapp.utility.CommonClass
import com.zoptal.cellableapp.utility.OkHttp3Connection
import com.zoptal.cellableapp.utility.OkHttp3Connection.OkHttp3RequestCallback
import com.zoptal.cellableapp.utility.SessionManager
import org.json.JSONException
import org.json.JSONObject
import java.util.*

/**
 * <h>VerifyLoginWithFacebook</h>
 *
 *
 * This class is called from ProfileFrag class. In this class we used to
 * login with facebook. Once we get success then retrieve access token
 * and call verify facebook login api and verify facebook button.
 *
 * @since 11-Jul-17
 */
class VerifyLoginWithFacebook(private val mActivity: Activity, rootView: View, pBar_fbVerify: ProgressBar, iV_fbicon: ImageView, facebookVerified: String) {
    private val mSessionManager: SessionManager
    private val callbackManager: CallbackManager
    private val rootView: View
    private val pBar_fbVerify: ProgressBar
    private val iV_fbicon: ImageView
    private var facebookVerified: String

    /**
     * <h>LoginFacebookSdk</h>
     *
     *
     * This method is being called from onCreate method. this method is called
     * when user click on LoginWithFacebook button. it contains three method onSuccess,
     * onCancel and onError. if login will be successfull then success method will be
     * called and in that method we obtain user all details like id, email, name, profile pic
     * etc. onCancel method will be called if user click on facebook with login button and
     * suddenly click back button. onError method will be called if any problem occurs like
     * internet issue.
     *
     */
    private fun initializeFacebookSdk() {
        LoginManager.getInstance().registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(loginResult: LoginResult) {
                val request = GraphRequest.newMeRequest(loginResult.accessToken) { `object`, response ->
                    Log.e("LoginActivity", response.toString())
                    val fb_accessToken: String?
                    if (response.error == null) {
                        loginResult.accessToken.token
                        if (loginResult.accessToken.token != null) {
                            fb_accessToken = loginResult.accessToken.token
                            if (fb_accessToken != null && !fb_accessToken.isEmpty()) {
                                verifyFacebookAccApi(fb_accessToken)
                            }
                        }
                    }
                }
                val parameters = Bundle()
                parameters.putString("fields", "id,email,first_name,last_name") /*,gender, birthday*/
                request.parameters = parameters
                request.executeAsync()
            }

            override fun onCancel() {
                CommonClass.showSnackbarMessage(rootView, mActivity.resources.getString(R.string.Loginfailed))
            }

            override fun onError(error: FacebookException) {
                error.printStackTrace()
                CommonClass.showSnackbarMessage(rootView, error.toString())
                if (error is FacebookAuthorizationException) {
                    if (AccessToken.getCurrentAccessToken() != null) {
                        LoginManager.getInstance().logOut()
                    }
                }
            }
        })
    }

    /**
     * <h>verifyFacebookAccApi</h>
     *
     *
     * This method is called from onSuccess of facebook login graph api.
     * We used to pass access token and call our local server to verify
     * facebook button from my profile.
     *
     * @param accessToken The token from facebook api response
     */
    private fun verifyFacebookAccApi(accessToken: String) {
        if (CommonClass.isNetworkAvailable(mActivity)) {
            pBar_fbVerify.visibility = View.VISIBLE
            iV_fbicon.visibility = View.GONE
            val request_datas = JSONObject()
            try {
                request_datas.put("token", mSessionManager.authToken)
                request_datas.put("accessToken", accessToken)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            OkHttp3Connection.doOkHttp3Connection(TAG, ApiUrl.VERIFY_FACEBOOK_LOGIN, OkHttp3Connection.Request_type.POST, request_datas, object : OkHttp3RequestCallback {
                 override fun onSuccess(result: String?, user_tag: String?) {
                    Log.e(TAG, " verifyFacebookAcc res=$result")
                    pBar_fbVerify.visibility = View.GONE
                    iV_fbicon.visibility = View.VISIBLE
                    val verifyFacebookPojo: VerifyFacebookPojo
                    val gson = Gson()
                    verifyFacebookPojo = gson.fromJson(result, VerifyFacebookPojo::class.java)
                    when (verifyFacebookPojo.code) {
                        "200" -> {
                            facebookVerified = "1"
                            iV_fbicon.setImageResource(R.drawable.facebook_verified_icon)
                        }
                        "401" -> CommonClass.sessionExpired(mActivity)
                        else -> {
                            facebookVerified = "0"
                            CommonClass.showSnackbarMessage(rootView, verifyFacebookPojo.message)
                        }
                    }
                }

                 override fun onError(error: String?, user_tag: String?) {
                    facebookVerified = "0"
                    pBar_fbVerify.visibility = View.GONE
                    iV_fbicon.visibility = View.VISIBLE
                }
            })
        } else CommonClass.showSnackbarMessage(rootView, mActivity.resources.getString(R.string.NoInternetAccess))
    }

    fun fbOnActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        callbackManager.onActivityResult(requestCode, resultCode, data)
        Log.e(TAG, " " + "profile callbackManager called...")
    }

    fun loginWithFbWithSdk() {
        if (CommonClass.isNetworkAvailable(mActivity)) LoginManager.getInstance().logInWithReadPermissions(mActivity, Arrays.asList("public_profile", "email")) //"user_friends",
        else CommonClass.showSnackbarMessage(rootView, mActivity.resources.getString(R.string.NoInternetAccess))
    }

    companion object {
        private val TAG = VerifyLoginWithFacebook::class.java.simpleName
    }

    init {
        mSessionManager = SessionManager(mActivity)
        FacebookSdk.sdkInitialize(mActivity)
        callbackManager = CallbackManager.Factory.create()
        this.rootView = rootView
        this.pBar_fbVerify = pBar_fbVerify
        this.iV_fbicon = iV_fbicon
        this.facebookVerified = facebookVerified
        if (AccessToken.getCurrentAccessToken() == null) {
            initializeFacebookSdk()
        } else {
            verifyFacebookAccApi(AccessToken.getCurrentAccessToken().token)
        }
    }
}