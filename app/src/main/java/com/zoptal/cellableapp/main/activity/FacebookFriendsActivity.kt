package com.zoptal.cellableapp.main.activity

import android.app.Activity
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.annotation.Nullable
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.gson.Gson
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.adapter.FacebookFriendsRvAdapter
import com.zoptal.cellableapp.fcm_push_notification.Config
import com.zoptal.cellableapp.fcm_push_notification.NotificationMessageDialog
import com.zoptal.cellableapp.fcm_push_notification.NotificationUtils.Companion.clearNotifications
import com.zoptal.cellableapp.main.activity.FacebookFriendsActivity
import com.zoptal.cellableapp.pojo_class.facebook_friends_pojo.FacebookFriendsData
import com.zoptal.cellableapp.pojo_class.facebook_friends_pojo.FacebookFriendsMain
import com.zoptal.cellableapp.utility.*
import com.zoptal.cellableapp.utility.OkHttp3Connection.OkHttp3RequestCallback
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.util.*

/**
 * <h>FacebookFriendsActivity</h>
 *
 *
 * This class is called from DiscoverPeopleActivity class. In this class firstly we used to
 * call facebook graph api and get the facebook id from my facebook friend list who used
 * this app. After this we used to send the ids to our server and get the complete details
 * of the user.
 *
 * @since 04-Jul-17
 */
class FacebookFriendsActivity : AppCompatActivity(), View.OnClickListener {
    private var callbackManager: CallbackManager? = null
    private var fbId = ""
    private var mActivity: Activity? = null
    private var mSessionManager: SessionManager? = null
    private var rL_rootElement: RelativeLayout? = null
    private var rL_friend_count: RelativeLayout? = null
    private var mProgressBar: ProgressBar? = null
    private var tV_friend_count: TextView? = null
    private var arrayListFbFriends: ArrayList<FacebookFriendsData>? = null
    private var phoneContactRvAdapter: FacebookFriendsRvAdapter? = null
    private var mNotificationMessageDialog: NotificationMessageDialog? = null
     override fun onCreate(@Nullable savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        facebookSDKInitialize()
        setContentView(R.layout.activity_fb_friends)
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale)
        initVariables()
    }

    /**
     * <h>InitVariables</h>
     *
     *
     * In this method we used to initialize all variables.
     *
     */
    private fun initVariables() {
        // receiving datas from last class
        val intent: Intent = getIntent()
        val followingCount = intent.getIntExtra("followingCount", 0)
        println("$TAG followingCount=$followingCount")
        mActivity = this@FacebookFriendsActivity
        mNotificationMessageDialog = NotificationMessageDialog(mActivity)
        arrayListFbFriends = ArrayList()
        mSessionManager = SessionManager(mActivity!!)
        rL_rootElement = findViewById(R.id.rL_rootElement) as RelativeLayout?
        rL_friend_count = findViewById(R.id.rL_friend_count) as RelativeLayout?
        tV_friend_count = findViewById(R.id.tV_friend_count) as TextView?
        mProgressBar = findViewById(R.id.progress_bar) as ProgressBar?
        val rV_facebookFriends: RecyclerView = findViewById(R.id.rV_facebookFriends) as RecyclerView
        val rL_back_btn = findViewById(R.id.rL_back_btn) as RelativeLayout
        rL_back_btn.setOnClickListener(this)
        LoginManager.getInstance().logInWithReadPermissions(mActivity, listOf("user_friends"))

        // set adapter
        phoneContactRvAdapter = FacebookFriendsRvAdapter(mActivity!!, arrayListFbFriends!!, followingCount)
        val linearLayoutManager = LinearLayoutManager(mActivity)
        rV_facebookFriends.setLayoutManager(linearLayoutManager)
        rV_facebookFriends.setAdapter(phoneContactRvAdapter)
        println("$TAG is logged in from fb=$isLoggedIn")
        if (isLoggedIn) {
            val request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken()
            ) { `object`, response ->
                if (response.error == null) {
                    try {
                        parseResponse(response.jsonObject)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                } else {
                }
            }
            val parameters = Bundle()
            parameters.putString("fields", "id,name,friends")
            request.parameters = parameters
            request.executeAsync()
        } else {
            // call method
            loginDetails
        }
    }

    @Throws(Exception::class)
    private fun parseResponse(jsonObject: JSONObject) {
        val jsonObject1 = jsonObject.getJSONObject("friends")
        println("$TAG json obj=$jsonObject")
        val friendListArr = jsonObject1.getJSONArray("data")
        println("$TAG friends list array when logged in=$friendListArr")
        getFbFriendsList(friendListArr)
    }

    val isLoggedIn: Boolean
        get() {
            val accessToken = AccessToken.getCurrentAccessToken()
            return accessToken != null && !accessToken.isExpired
        }

    override fun onResume() {
        super.onResume()

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.REGISTRATION_COMPLETE))

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.PUSH_NOTIFICATION))

        // clear the notification area when the app is opened
        clearNotifications(getApplicationContext())
    }

   override fun onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!)
        super.onPause()
    }

    /**
     * Initialize the Facebook callbackManager
     */
    private fun facebookSDKInitialize() {
        FacebookSdk.sdkInitialize(getApplicationContext())
        callbackManager = CallbackManager.Factory.create()
    }//  code to handle error// code for cancellation//AccessToken.getCurrentAccessToken(),// Callback registration

    /**
     * <h>GetLoginDetails</h>
     *
     *
     * In this method we used to call facebook graph api and get the Common friends facebook id
     * who used this app.
     *
     */
    private val loginDetails: Unit
        private get() {
            // Callback registration
            LoginManager.getInstance().registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
                override fun onSuccess(login_result: LoginResult) {
                    GraphRequest(
                            login_result.accessToken,  //AccessToken.getCurrentAccessToken(),
                            "/me/friends",
                            null,
                            HttpMethod.GET,
                            GraphRequest.Callback { response ->
                                try {
                                    val friendListArr = response.jsonObject.getJSONArray("data")
                                    getFbFriendsList(friendListArr)
                                } catch (e: JSONException) {
                                    e.printStackTrace()
                                }
                            }
                    ).executeAsync()
                }

                override fun onCancel() {
                    // code for cancellation
                    finish()
                }

                override fun onError(exception: FacebookException) {
                    //  code to handle error
                    finish()
                }
            })
        }

    private fun getFbFriendsList(friendListArr: JSONArray) {
        println("$TAG friends list array=$friendListArr")
        val friendListSize = friendListArr.length()
        for (friendListCount in 0 until friendListSize) {
            try {
                println(TAG + " " + "id=" + friendListArr.getJSONObject(friendListCount).getString("id") + " " + "name=" + friendListArr.getJSONObject(friendListCount).getString("name"))
                fbId = fbId + "," + friendListArr.getJSONObject(friendListCount).getString("id")
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }
        println(TAG + "id initial=" + fbId)
        if (!fbId.isEmpty() && fbId.startsWith(",")) {
            fbId = fbId.replaceFirst(",".toRegex(), "")
            findFbFriendsApi(fbId)
        }
        println(TAG + "id final=" + fbId)
    }

    /**
     * <h>FindFbFriendsApi</h>
     *
     *
     * In this method we do api call to get the complete information for the
     * given facebook ids.
     *
     * @param fbId The saved facebook ids
     */
    private fun findFbFriendsApi(fbId: String) {
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            mProgressBar!!.visibility = View.VISIBLE
            val request_datas = JSONObject()
            try {
                request_datas.put("facebookId", fbId)
                request_datas.put("token", mSessionManager!!.authToken)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            OkHttp3Connection.doOkHttp3Connection(TAG, ApiUrl.FACEBOOK_CONTACT_SYNC, OkHttp3Connection.Request_type.POST, request_datas, object : OkHttp3RequestCallback {
                 override fun onSuccess(result: String?, user_tag: String?) {
                    mProgressBar!!.visibility = View.GONE
                    println("$TAG find fb friends res=$result")
                    val facebookFriendsMain: FacebookFriendsMain
                    val gson = Gson()
                    facebookFriendsMain = gson.fromJson(result, FacebookFriendsMain::class.java)
                    when (facebookFriendsMain.code) {
                        "200" -> if (facebookFriendsMain.facebookUsers != null && facebookFriendsMain.facebookUsers!!.size > 0) {
                            arrayListFbFriends!!.addAll(facebookFriendsMain.facebookUsers!!)
                            mSessionManager!!.fbFriendCount = arrayListFbFriends!!.size
                            // set count
                            rL_friend_count!!.visibility = View.VISIBLE
                            val setCountText: String
                            setCountText = if (arrayListFbFriends!!.size > 1) arrayListFbFriends!!.size.toString() + " " + getResources().getString(R.string.friends_on) + " " + getResources().getString(R.string.app_name) else arrayListFbFriends!!.size.toString() + " " + getResources().getString(R.string.friend_on) + " " + getResources().getString(R.string.app_name)
                            tV_friend_count!!.text = setCountText

                            // Notify recyclerview adapter
                            phoneContactRvAdapter!!.notifyDataSetChanged()
                        }
                        "401" -> CommonClass.sessionExpired(mActivity!!)
                        else -> CommonClass.showSnackbarMessage(rL_rootElement, facebookFriendsMain.message)
                    }
                }

                 override fun onError(error: String?, user_tag: String?) {
                    mProgressBar!!.visibility = View.GONE
                    CommonClass.showSnackbarMessage(rL_rootElement, error)
                }
            })
        } else CommonClass.showSnackbarMessage(rL_rootElement, getResources().getString(R.string.NoInternetAccess))
    }

   override  protected fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        callbackManager!!.onActivityResult(requestCode, resultCode, data)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.rL_back_btn -> onBackPressed()
        }
    }

 override fun onBackPressed() {
        val intent = Intent()
        intent.putExtra("fb_count", arrayListFbFriends!!.size)
        intent.putExtra("followingCount", phoneContactRvAdapter!!.followingCount)
        setResult(VariableConstants.FB_FRIEND_REQ_CODE, intent)
        finish()
        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate)
    }

    companion object {
        private val TAG = FacebookFriendsActivity::class.java.simpleName
    }
}