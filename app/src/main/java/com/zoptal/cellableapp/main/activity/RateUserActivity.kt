package com.zoptal.cellableapp.main.activity

import android.app.Activity
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.View
import android.widget.*
import androidx.annotation.Nullable
import androidx.core.content.ContextCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.event_bus.EventBusDatasHandler
import com.zoptal.cellableapp.fcm_push_notification.Config
import com.zoptal.cellableapp.fcm_push_notification.NotificationMessageDialog
import com.zoptal.cellableapp.fcm_push_notification.NotificationUtils.Companion.clearNotifications
import com.zoptal.cellableapp.main.activity.RateUserActivity
import com.zoptal.cellableapp.pojo_class.rate_seller.RateSellerPojo
import com.zoptal.cellableapp.pojo_class.rate_user_pojo.RateUserMainPojo
import com.zoptal.cellableapp.utility.*
import com.zoptal.cellableapp.utility.OkHttp3Connection.OkHttp3RequestCallback
import org.json.JSONException
import org.json.JSONObject
import java.util.*

/**
 * <h>RateUserActivity</h>
 *
 *
 * In this class we used to do api call to set rating for user.
 *
 * @since 13-Jul-17
 */
class RateUserActivity : AppCompatActivity(), View.OnClickListener {
    private var mRatingBar: RatingBar? = null
    private var tV_absolutely: TextView? = null
    private var tV_not: TextView? = null
    private var tV_yes: TextView? = null
    private var tV_definitely: TextView? = null
    private var tV_submit: TextView? = null
    private var rL_submit: RelativeLayout? = null
    private var rL_rootElement: RelativeLayout? = null
    private var isToSubmit = false
    private var isFromNotification = false
    private var mActivity: Activity? = null
    private var userName: String? = ""
    private var postId = ""
    private var mSessionManager: SessionManager? = null
    private var progress_bar_submit: ProgressBar? = null
    private var mNotificationMessageDialog: NotificationMessageDialog? = null
    private var mEventBusDatasHandler: EventBusDatasHandler? = null
     override fun onCreate(@Nullable savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_rate_user)
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale)
        initVariable()
    }

    /**
     * <h>InitVariable</h>
     *
     *
     * In this method we used to initialize all variables.
     *
     */
    private fun initVariable() {
        mActivity = this@RateUserActivity
        mEventBusDatasHandler = EventBusDatasHandler(mActivity)
        mNotificationMessageDialog = NotificationMessageDialog(mActivity)
        mSessionManager = SessionManager(mActivity!!)
        progress_bar_submit = findViewById(R.id.progress_bar_submit) as ProgressBar?
        tV_submit = findViewById(R.id.tV_submit) as TextView?

        // receive datas from last activity class
        val intent: Intent = getIntent()
        userName = intent.getStringExtra("userName")
        postId = intent.getStringExtra("postId")
        isFromNotification = intent.getBooleanExtra("isFromNotification", false)
        val userImage = intent.getStringExtra("userImage")

        // set user pic
        val iV_userPic = findViewById(R.id.iV_userPic) as ImageView
        iV_userPic.layoutParams.width = CommonClass.getDeviceWidth(mActivity!!) / 4
        iV_userPic.layoutParams.height = CommonClass.getDeviceWidth(mActivity!!) / 4
        if (userImage != null && !userImage.isEmpty()) Picasso.with(mActivity)
                .load(userImage)
                .placeholder(R.drawable.default_profile_image)
                .placeholder(R.drawable.default_profile_image)
                .transform(CircleTransform())
                .into(iV_userPic)
        var setUserName = ""
        if (userName != null) setUserName = getResources().getString(R.string.would_you_sell_to_megan_again).toString() + " " + userName + " " + getResources().getString(R.string.again)
        println("$TAG set user name=$setUserName")

        // set rate user
        val tV_rate_user = findViewById(R.id.tV_rate_user) as TextView
        tV_rate_user.text = setUserName

        // Back button
        val rL_back_btn = findViewById(R.id.rL_back_btn) as RelativeLayout
        rL_back_btn.setOnClickListener(this)

        // rating
        mRatingBar = findViewById(R.id.ratingBar) as RatingBar?
        tV_absolutely = findViewById(R.id.tV_absolutely) as TextView?
        tV_not = findViewById(R.id.tV_not) as TextView?
        tV_yes = findViewById(R.id.tV_yes) as TextView?
        tV_definitely = findViewById(R.id.tV_definitely) as TextView?
        rL_submit = findViewById(R.id.rL_submit) as RelativeLayout?
        rL_rootElement = findViewById(R.id.rL_rootElement) as RelativeLayout?
        val purple_color = ContextCompat.getColor(mActivity!!, R.color.purple_color)
        val gray_color = ContextCompat.getColor(mActivity!!, R.color.login_tab_bg)
        CommonClass.setViewOpacity(mActivity, rL_submit!!, 102, R.color.purple_color)
        mRatingBar!!.onRatingBarChangeListener = RatingBar.OnRatingBarChangeListener { ratingBar, rating, fromUser ->
            println("$TAG rating progress=$rating")
            if (rating == 1f || rating == 2f) {
                tV_absolutely!!.setTextColor(purple_color)
                tV_not!!.setTextColor(purple_color)
                tV_yes!!.setTextColor(gray_color)
                tV_definitely!!.setTextColor(gray_color)
            } else {
                tV_absolutely!!.setTextColor(gray_color)
                tV_not!!.setTextColor(gray_color)
                tV_yes!!.setTextColor(purple_color)
                tV_definitely!!.setTextColor(purple_color)
            }

            // show submit button
            if (rating == 0f) {
                tV_absolutely!!.setTextColor(gray_color)
                tV_not!!.setTextColor(gray_color)
                tV_yes!!.setTextColor(gray_color)
                tV_definitely!!.setTextColor(gray_color)
                isToSubmit = false
                CommonClass.setViewOpacity(mActivity, rL_submit!!, 102, R.color.purple_color)
            } else {
                isToSubmit = true
                CommonClass.setViewOpacity(mActivity, rL_submit!!, 255, R.color.purple_color)
            }
        }

        // submit user rating
        rL_submit!!.setOnClickListener {
            if (isToSubmit) {
                progress_bar_submit!!.visibility = View.VISIBLE
                tV_submit!!.visibility = View.GONE
                if (isFromNotification) rateUserApi() else markSold()
            }
        }
    }

    override fun onResume() {
        super.onResume()

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.REGISTRATION_COMPLETE))

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.PUSH_NOTIFICATION))

        // clear the notification area when the app is opened
        clearNotifications(getApplicationContext())
    }

   override fun onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!)
        super.onPause()
    }

    /**
     * <h>MarkSold</h>
     *
     *
     * In this method we used to rate the buyer when we used to come from
     * EditProduct screen.
     *
     */
    private fun markSold() {
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            // postId, type, ratings, membername, buyername
            val request_datas = JSONObject()
            try {
                request_datas.put("token", mSessionManager!!.authToken)
                request_datas.put("postId", postId)
                request_datas.put("type", "0")
                request_datas.put("ratings", mRatingBar!!.rating.toDouble())
                request_datas.put("membername", userName)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            OkHttp3Connection.doOkHttp3Connection(TAG, ApiUrl.MARK_SOLD, OkHttp3Connection.Request_type.POST, request_datas, object : OkHttp3RequestCallback {
                 override fun onSuccess(result: String?, user_tag: String?) {
                    progress_bar_submit!!.visibility = View.GONE
                    tV_submit!!.visibility = View.VISIBLE
                    val gson = Gson()
                    val rateUserMainPojo = gson.fromJson(result, RateUserMainPojo::class.java)
                    when (rateUserMainPojo.code) {
                        "200" -> {
                            mEventBusDatasHandler!!.addSoldDatasFromRateUser(rateUserMainPojo.data!!)
                            mEventBusDatasHandler!!.removeHomePageDatasFromEditPost(rateUserMainPojo.data!!.postId)
                            mEventBusDatasHandler!!.removeSocialDatasFromEditPost(rateUserMainPojo.data!!.postId)
                            mEventBusDatasHandler!!.removeSellingDatasFromEditPost(rateUserMainPojo.data!!.postId)
                            val intent = Intent()
                            intent.putExtra("isToFinishSelectBuyer", true)
                            intent.putExtra("isToSellItAgain", false)
                            setResult(VariableConstants.RATE_USER_REQ_CODE, intent)
                            finish()
                        }
                        "401" -> CommonClass.sessionExpired(mActivity!!)
                        else -> CommonClass.showSnackbarMessage(rL_rootElement, rateUserMainPojo.message)
                    }
                }

                 override fun onError(error: String?, user_tag: String?) {
                    CommonClass.showSnackbarMessage(rL_rootElement, error)
                }
            })
        } else {
            progress_bar_submit!!.visibility = View.GONE
            tV_submit!!.visibility = View.VISIBLE
            CommonClass.showSnackbarMessage(rL_rootElement, getResources().getString(R.string.NoInternetAccess))
        }
    }

    /**
     * <h>RateUserApi</h>
     *
     *
     * In this method we used to do api call to rate the seller when we usually come
     * from Notification screen.
     *
     */
    private fun rateUserApi() {
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            progress_bar_submit!!.visibility = View.VISIBLE
            tV_submit!!.visibility = View.GONE
            val url = ApiUrl.RATE_USER + userName
            val request_data = JSONObject()
            try {
                request_data.put("token", mSessionManager!!.authToken)
                request_data.put("rating", mRatingBar!!.rating.toDouble())
                request_data.put("postId", postId)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            OkHttp3Connection.doOkHttp3Connection(TAG, url, OkHttp3Connection.Request_type.POST, request_data, object : OkHttp3RequestCallback {
                 override fun onSuccess(result: String?, user_tag: String?) {
                    progress_bar_submit!!.visibility = View.GONE
                    tV_submit!!.visibility = View.VISIBLE
                    println("$TAG rate user res=$result")
                    val rateSellerPojo: RateSellerPojo
                    val gson = Gson()
                    rateSellerPojo = gson.fromJson(result, RateSellerPojo::class.java)
                    when (rateSellerPojo.code) {
                        "200" -> {
                            CommonClass.showSuccessSnackbarMsg(rL_rootElement, rateSellerPojo.message)
                            val t = Timer()
                            t.schedule(object : TimerTask() {
                                override fun run() {
                                    // when the task active then close the activity
                                    t.cancel()
                                    //EditProfileActivity.editProfileInstance.finish();
                                    //SelectBuyerActivity.selectBuyerActInstance.finish();
                                    onBackPressed()
                                }
                            }, 3000)
                        }
                        "401" -> CommonClass.sessionExpired(mActivity!!)
                        else -> CommonClass.showSnackbarMessage(rL_rootElement, rateSellerPojo.message)
                    }
                }

                 override fun onError(error: String?, user_tag: String?) {}
            })
        } else CommonClass.showSnackbarMessage(rL_rootElement, getResources().getString(R.string.NoInternetAccess))
    }

 override fun onBackPressed() {
        val intent = Intent()
        intent.putExtra("rate", true)
        setResult(VariableConstants.RATE_USER_REQ_CODE, intent)
        finish()
        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.rL_back_btn -> onBackPressed()
        }
    }

    companion object {
        private val TAG = RateUserActivity::class.java.simpleName
    }
}