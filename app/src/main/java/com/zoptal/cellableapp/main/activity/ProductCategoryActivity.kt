package com.zoptal.cellableapp.main.activity

import android.app.Activity
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.widget.ProgressBar
import android.widget.RelativeLayout
import androidx.annotation.Nullable
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.gson.Gson
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.adapter.ProductCategoryRvAdapter
import com.zoptal.cellableapp.fcm_push_notification.Config
import com.zoptal.cellableapp.fcm_push_notification.NotificationMessageDialog
import com.zoptal.cellableapp.fcm_push_notification.NotificationUtils.Companion.clearNotifications
import com.zoptal.cellableapp.main.activity.ProductCategoryActivity
import com.zoptal.cellableapp.main.view_type_activity.AddDetailActivity
import com.zoptal.cellableapp.pojo_class.product_category.FilterKeyValue
import com.zoptal.cellableapp.pojo_class.product_category.ProductCategoryMainPojo
import com.zoptal.cellableapp.utility.*
import com.zoptal.cellableapp.utility.OkHttp3Connection.OkHttp3RequestCallback
import org.json.JSONException
import org.json.JSONObject
import java.util.*

/**
 * <h>ProductCategoryActivity</h>
 *
 *
 * This class is getting called from PostProductActivity class. In this
 * class we used to show the list of product categories in RecyclerView.
 * Once user clicks on any category from that list we send back that
 * product category to previous class.
 *
 * @since 2017-05-04
 * @version 1.0
 * @author 3Embed
 */
class ProductCategoryActivity : AppCompatActivity(), View.OnClickListener {
    private var progress_bar: ProgressBar? = null
    private var mActivity: Activity? = null
    private var rL_rootview: RelativeLayout? = null
    private var rV_category: RecyclerView? = null
    private var mNotificationMessageDialog: NotificationMessageDialog? = null
    private var selectedCatgory: String? = ""
    private var selectedSubCategory = ""
    private var selectedFileds: ArrayList<FilterKeyValue>? = null
    private var categoryName = ""
     override fun onCreate(@Nullable savedInstanceState: Bundle?) {
         try {
             super.onCreate(savedInstanceState ?: Bundle())
             setContentView(R.layout.activity_category)
             overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale)
             mActivity = this@ProductCategoryActivity
             mNotificationMessageDialog = NotificationMessageDialog(mActivity)
             CommonClass.statusBarColor(mActivity!!)

             // get data which is selected by user.
             if (getIntent() != null) {
                 selectedCatgory = getIntent().getStringExtra("selectedCategory")
                 if (intent.hasExtra("selectedSubCategory") &&  getIntent().getStringExtra("selectedSubCategory")!=null) {
                     selectedSubCategory = getIntent().getStringExtra("selectedSubCategory")
                 }
                 selectedFileds = getIntent().getSerializableExtra("selectedField") as ArrayList<FilterKeyValue>?
             }
             progress_bar = findViewById(R.id.progress_bar) as ProgressBar?
             rL_rootview = findViewById(R.id.rL_rootview) as RelativeLayout?
             rV_category = findViewById(R.id.rV_category) as RecyclerView?
             val rL_back_btn = findViewById(R.id.rL_back_btn) as RelativeLayout
             rL_back_btn.setOnClickListener(this)
             categoriesService
         }catch (e:Exception){e.printStackTrace()}
    }

    override fun onResume() {
        super.onResume()

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.REGISTRATION_COMPLETE))

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.PUSH_NOTIFICATION))

        // clear the notification area when the app is opened
        clearNotifications(getApplicationContext())
    }

   override fun onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!)
        super.onPause()
    }//when only category is there

    //data.putExtra("filterDataKeyValue", new ArrayList<FilterData>());

    /*String categoryName=aL_categoryDatas.get(position).getName();
    if (categoryName!=null && !categoryName.isEmpty())
    {
        categoryName=categoryName.substring(0,1).toUpperCase()+categoryName.substring(1).toLowerCase();
        Intent intent=new Intent();
        intent.putExtra("categoryName",categoryName);
        setResult(VariableConstants.CATEGORY_REQUEST_CODE,intent);
        onBackPressed();
    }*/
// isForFilter used while view type is open from filter screen// set previously selected category by user
    ///////////////////////////////////////////

    /**
     * <h>GetCategoriesService</h>
     *
     *
     * This method is called from onCreate() method of the current class.
     * In this method we used to call the getCategories api using okHttp3.
     * Once we get the data we show that list in recyclerview.
     *
     */
    private val categoriesService: Unit
        private get() {
            if (CommonClass.isNetworkAvailable(mActivity!!)) {
                progress_bar!!.visibility = View.VISIBLE
                val request_datas = JSONObject()
                try {
                    request_datas.put("token", "20")
                    request_datas.put("offset", "0")
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
                val url = ApiUrl.GET_CATEGORIES + "?limit=50"
                OkHttp3Connection.doOkHttp3Connection(TAG, url, OkHttp3Connection.Request_type.GET, request_datas, object : OkHttp3RequestCallback {
                     override fun onSuccess(result: String?, user_tag: String?) {
                        progress_bar!!.visibility = View.GONE
                        println("$TAG get category res=$result")
                        val categoryMainPojo: ProductCategoryMainPojo
                        val gson = Gson()
                        categoryMainPojo = gson.fromJson(result, ProductCategoryMainPojo::class.java)
                        when (categoryMainPojo.code) {
                            "200" -> {
                                val aL_categoryDatas = categoryMainPojo.data
                                if (aL_categoryDatas != null && aL_categoryDatas.size > 0) {
                                    val categoryRvAdapter = ProductCategoryRvAdapter(mActivity!!, aL_categoryDatas)
                                    val layoutManager = LinearLayoutManager(mActivity)

                                    // set previously selected category by user
                                    var i = 0
                                    while (i < aL_categoryDatas.size) {
                                        if (selectedCatgory != null && !selectedCatgory!!.isEmpty() && selectedCatgory.equals(aL_categoryDatas[i].name, ignoreCase = true)) categoryRvAdapter.mSelected = i
                                        i++
                                    }
                                    ///////////////////////////////////////////
                                    rV_category!!.setLayoutManager(layoutManager)
                                    rV_category!!.setAdapter(categoryRvAdapter)
                                    categoryRvAdapter.setOnItemClick(object : ClickListener {
                                        override fun onItemClick(view: View?, position: Int) {
                                            categoryName = aL_categoryDatas[position].name
                                            if (aL_categoryDatas[position].subCategoryCount > 0) {
                                                val intent = Intent(mActivity, ProductSubCategoryActivity::class.java)
                                                intent.putExtra("categoryDatas", aL_categoryDatas)
                                                intent.putExtra("categoryName", aL_categoryDatas[position].name)
                                                intent.putExtra("selectedSubCategory", selectedSubCategory)
                                                intent.putExtra("selectedField", selectedFileds)
                                                startActivityForResult(intent, VariableConstants.SUB_CATEGORY_REQ_CODE)
                                            } else if (aL_categoryDatas[position].filterCount > 0) {
                                                val intent = Intent(mActivity, AddDetailActivity::class.java)
                                                // isForFilter used while view type is open from filter screen
                                                intent.putExtra("isForFilter", getIntent().getBooleanExtra("isForFilter", false))
                                                intent.putExtra("title", aL_categoryDatas[position].name)
                                                intent.putExtra("filterData", aL_categoryDatas[position].filter)
                                                intent.putExtra("selectedField", selectedFileds)
                                                startActivityForResult(intent, VariableConstants.ADD_DETAIL_DATA_REQ_CODE)
                                            } else { //when only category is there
                                                val data = Intent()
                                                data.putExtra("categoryName", categoryName)
                                                data.putExtra("subCategoryName", "")
                                                data.putExtra("categoryNodeId", aL_categoryDatas[position].categoryNodeId)
                                                data.putExtra("subCatId", "")
                                                data.putExtra("filterKeyValues", ArrayList<FilterKeyValue>())

                                                //data.putExtra("filterDataKeyValue", new ArrayList<FilterData>());
                                                setResult(VariableConstants.CATEGORY_REQUEST_CODE, data)
                                                onBackPressed()
                                            }

                                            /*String categoryName=aL_categoryDatas.get(position).getName();
                                                                            if (categoryName!=null && !categoryName.isEmpty())
                                                                            {
                                                                                categoryName=categoryName.substring(0,1).toUpperCase()+categoryName.substring(1).toLowerCase();
                                                                                Intent intent=new Intent();
                                                                                intent.putExtra("categoryName",categoryName);
                                                                                setResult(VariableConstants.CATEGORY_REQUEST_CODE,intent);
                                                                                onBackPressed();
                                                                            }*/
                                        }
                                    })
                                }
                            }
                            else -> CommonClass.showSnackbarMessage(rL_rootview, categoryMainPojo.message)
                        }
                    }

                     override fun onError(error: String?, user_tag: String?) {
                        progress_bar!!.visibility = View.GONE
                        CommonClass.showSnackbarMessage(rL_rootview, error)
                    }
                })
            } else CommonClass.showSnackbarMessage(rL_rootview, getResources().getString(R.string.NoInternetAccess))
        }

 override fun onBackPressed() {
        finish()
        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.rL_back_btn -> onBackPressed()
        }
    }

   override  protected fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (data != null) {
            when (resultCode) {
                VariableConstants.SUB_CATEGORY_REQ_CODE -> {
                    setResult(VariableConstants.CATEGORY_REQUEST_CODE, data)
                    onBackPressed()
                }
                VariableConstants.ADD_DETAIL_DATA_REQ_CODE -> {
                    data.putExtra("categoryName", categoryName)
                    setResult(VariableConstants.SUB_CATEGORY_REQ_CODE, data)
                    onBackPressed()
                }
            }
        }
    }

    companion object {
        private val TAG = ProductCategoryActivity::class.java.simpleName
    }
}