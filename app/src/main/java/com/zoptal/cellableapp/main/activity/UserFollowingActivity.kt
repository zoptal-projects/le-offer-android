
package com.zoptal.cellableapp.main.activity

import android.app.Activity
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.annotation.Nullable
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout

import com.google.gson.Gson
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.adapter.UserFollowRvAdapter
import com.zoptal.cellableapp.fcm_push_notification.Config
import com.zoptal.cellableapp.fcm_push_notification.NotificationMessageDialog
import com.zoptal.cellableapp.fcm_push_notification.NotificationUtils.Companion.clearNotifications
import com.zoptal.cellableapp.main.activity.UserFollowingActivity
import com.zoptal.cellableapp.pojo_class.user_follow_pojo.FollowMainPojo
import com.zoptal.cellableapp.pojo_class.user_follow_pojo.FollowResponseDatas
import com.zoptal.cellableapp.utility.*
import com.zoptal.cellableapp.utility.OkHttp3Connection.OkHttp3RequestCallback
import org.json.JSONException
import org.json.JSONObject
import java.util.*

/**
 * <h>UserFollowingActivity</h>
 *
 *
 * In this class we used to show the other user follower or following list. We used
 * to do api call to receive the datas for follower or following.
 *
 * @since 01-Aug-17
 */
class UserFollowingActivity : AppCompatActivity(), View.OnClickListener {
    private var mActivity: Activity? = null
    private var mSessionManager: SessionManager? = null
    private var isFollower = false
    private var rL_rootview: RelativeLayout? = null
    private var rL_no_following: RelativeLayout? = null
    private var mProgressBar: ProgressBar? = null
    private var mSwipeRefreshLayout: SwipeRefreshLayout? = null
    private var arrayListFollow: ArrayList<FollowResponseDatas>? = null
    private var followRvAdapter: UserFollowRvAdapter? = null
    var index = 0
    private var mNotificationMessageDialog: NotificationMessageDialog? = null
     override fun onCreate(@Nullable savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_user_following)
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale)
        index = 0
        initVariables()
    }

    /**
     * <h>initVariables</h>
     *
     *
     * In this method we used to initialize xml data member and other variables.
     *
     */
    private fun initVariables() {
        // receiving values from last class
        val intent: Intent = getIntent()
        val title = intent.getStringExtra("title")
        val membername = intent.getStringExtra("membername")
        isFollower = intent.getBooleanExtra("isFollower", false)
        val followerCount = intent.getIntExtra("followerCount", 0)
        arrayListFollow = ArrayList()
        println("$TAG membername=$membername")

        // initialize variables
        mActivity = this@UserFollowingActivity
        mNotificationMessageDialog = NotificationMessageDialog(mActivity)
        CommonClass.statusBarColor(mActivity!!)
        mSessionManager = SessionManager(mActivity!!)
        val tV_title = findViewById(R.id.tV_title) as TextView
        if (title != null) tV_title.text = title
        mSwipeRefreshLayout = findViewById(R.id.swipeRefreshLayout) as SwipeRefreshLayout?
        mSwipeRefreshLayout!!.setColorSchemeResources(R.color.pink_color)
        rL_rootview = findViewById(R.id.rL_rootview) as RelativeLayout?
        val rV_follow: RecyclerView = findViewById(R.id.rV_follow) as RecyclerView
        mProgressBar = findViewById(R.id.progress_bar_follow) as ProgressBar?
        mProgressBar!!.visibility = View.GONE
        val rL_back_btn = findViewById(R.id.rL_back_btn) as RelativeLayout
        rL_back_btn.setOnClickListener(this)

        // set recycler view adapter
        followRvAdapter = UserFollowRvAdapter(mActivity!!, arrayListFollow!!, followerCount)
        val layoutManager = LinearLayoutManager(mActivity)
        rV_follow.setLayoutManager(layoutManager)
        rV_follow.setAdapter(followRvAdapter)

        // No Following or follower default var
        val iV_following_icon = findViewById(R.id.iV_following_icon) as ImageView
        val tV_no_following = findViewById(R.id.tV_no_following) as TextView
        rL_no_following = findViewById(R.id.rL_no_following) as RelativeLayout?
        rL_no_following!!.visibility = View.GONE

        // set api url according to condition like follower or following
        val apiUrl: String
        if (isFollower) {
            apiUrl = ApiUrl.GET_MEMBER_FOLLOWER
            iV_following_icon.setImageResource(R.drawable.empty_follower)
            tV_no_following.setText(getResources().getString(R.string.no_follower_yet))
        } else {
            apiUrl = ApiUrl.GET_MEMBER_FOLLOWING
            iV_following_icon.setImageResource(R.drawable.empty_following)
            tV_no_following.setText(getResources().getString(R.string.no_following_yet))
        }
        if (membername != null && !membername.isEmpty()) {
            mProgressBar!!.visibility = View.VISIBLE
            getFollowingService(index, apiUrl, membername)
        }

        // pull to refresh
        mSwipeRefreshLayout!!.setOnRefreshListener(object : SwipeRefreshLayout.OnRefreshListener {
            override fun onRefresh() {
                arrayListFollow!!.clear()
                getFollowingService(index, apiUrl, membername)
            }
        })
    }

    override fun onResume() {
        super.onResume()

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.REGISTRATION_COMPLETE))

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.PUSH_NOTIFICATION))

        // clear the notification area when the app is opened
        clearNotifications(getApplicationContext())
    }

   override fun onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!)
        super.onPause()
    }

    /**
     * <h>GetFollowingService</h>
     *
     *
     * In this method we used to do api call to get follower or following list
     * for particular user.
     *
     * @param offsetValue The page index
     * @param ApiUrl The url for follower or following
     * @param membername The user name
     */
    private fun getFollowingService(offsetValue: Int, ApiUrl: String, membername: String?) {
        var offsetValue = offsetValue
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            val limit = 20
            offsetValue = offsetValue * limit
            val request_datas = JSONObject()
            try {
                request_datas.put("token", mSessionManager!!.authToken)
                request_datas.put("membername", membername)
                request_datas.put("offset", offsetValue)
                request_datas.put("limit", limit)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            OkHttp3Connection.doOkHttp3Connection(TAG, ApiUrl, OkHttp3Connection.Request_type.POST, request_datas, object : OkHttp3RequestCallback {
                 override fun onSuccess(result: String?, user_tag: String?) {
                    mProgressBar!!.visibility = View.GONE
                    mSwipeRefreshLayout!!.setRefreshing(false)
                    val followMainPojo: FollowMainPojo
                    val gson = Gson()
                    followMainPojo = gson.fromJson(result, FollowMainPojo::class.java)
                    when (followMainPojo.code) {
                        "200" -> {
                            if (isFollower) arrayListFollow!!.addAll(followMainPojo.memberFollowers!!) else arrayListFollow!!.addAll(followMainPojo.following!!)
                            if (arrayListFollow != null && arrayListFollow!!.size > 0) {
                                followRvAdapter!!.notifyDataSetChanged()
                            }
                        }
                        "401" -> CommonClass.sessionExpired(mActivity!!)
                        "204" -> rL_no_following!!.visibility = View.VISIBLE
                        else -> CommonClass.showSnackbarMessage(rL_rootview, followMainPojo.message)
                    }
                }

                 override fun onError(error: String?, user_tag: String?) {
                    mProgressBar!!.visibility = View.GONE
                }
            })
        } else CommonClass.showSnackbarMessage(rL_rootview, getResources().getString(R.string.NoInternetAccess))
    }

 override fun onBackPressed() {
        val intent = Intent()
        intent.putExtra("followerCount", followRvAdapter!!.followingCount)
        setResult(VariableConstants.FOLLOW_COUNT_REQ_CODE, intent)
        finish()
        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.rL_back_btn -> onBackPressed()
        }
    }

    companion object {
        private val TAG = UserFollowingActivity::class.java.simpleName
    }
}