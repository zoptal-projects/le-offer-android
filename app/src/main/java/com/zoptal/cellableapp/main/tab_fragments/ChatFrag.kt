package com.zoptal.cellableapp.main.tab_fragments

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.EditText
import android.widget.RelativeLayout
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import androidx.viewpager.widget.ViewPager.OnPageChangeListener
import com.google.android.material.tabs.TabLayout
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.adapter.ViewPagerAdapter
import com.zoptal.cellableapp.main.activity.HomePageActivity
import com.zoptal.cellableapp.mqttchat.Fragments.BuyingFragment
import com.zoptal.cellableapp.mqttchat.Fragments.SealingFragment

/**
 * <h2>ChatFrag</h2>
 * <P>
 * Chat fragment ot show the details.
</P> *
 * @since 3/31/2017.
 */
class ChatFrag : Fragment(), View.OnClickListener {
    private var buyingFragment: BuyingFragment? = null
    private var sealingFragment: SealingFragment? = null
    private var chatViewPager: ViewPager? = null
    private var search_text: EditText? = null
    private var search_intiate: RelativeLayout? = null
    private var search_edit_view: RelativeLayout? = null
    private var homePageActivity: HomePageActivity? = null
    private val firstTime = true
    override fun onCreate(savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        buyingFragment = BuyingFragment()
        sealingFragment = SealingFragment()
        homePageActivity = activity as HomePageActivity?
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.frag_chat, container, false)
        chatViewPager = view.findViewById<View>(R.id.viewpager) as ViewPager
        val tabs_chat = view.findViewById<View>(R.id.tabs_chat) as TabLayout
        setupViewPager()
        tabs_chat.setupWithViewPager(chatViewPager)
        search_intiate = view.findViewById<View>(R.id.search_intiate) as RelativeLayout
        search_intiate!!.setOnClickListener(this)
        search_edit_view = view.findViewById<View>(R.id.search_edit_view) as RelativeLayout
        view.findViewById<View>(R.id.search_icon).setOnClickListener(this)
        view.findViewById<View>(R.id.refreshButton).setOnClickListener(this)
        view.findViewById<View>(R.id.close_icon).setOnClickListener(this)
        search_text = view.findViewById<View>(R.id.search_text) as EditText
        search_text!!.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                checkSearchFiltre(s.toString())
            }

            override fun afterTextChanged(s: Editable) {}
        })
        return view
    }

    /*
     *Doing the setup for the view pager. */
    private fun setupViewPager() {
        val adapter = ViewPagerAdapter(childFragmentManager)
        adapter.addFragment(buyingFragment!!, resources.getString(R.string.buying))
        adapter.addFragment(sealingFragment!!, resources.getString(R.string.selling))
        chatViewPager!!.adapter = adapter
        chatViewPager!!.addOnPageChangeListener(object : OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
            override fun onPageSelected(position: Int) {
                search_text!!.setText("")
            }

            override fun onPageScrollStateChanged(state: Int) {}
        })
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.close_icon -> {
                search_text!!.setText("")
                animateSearchView()
            }
            R.id.search_intiate -> animateSearchView()
            R.id.search_icon -> animateSearchView()
            R.id.refreshButton -> {
                search_text!!.setText("")
                handelRefreshCall()
            }
        }
    }

    /*
     *Handling the refresh call call to the  */
    fun handelRefreshCall() {
        when (chatViewPager!!.currentItem) {
            0 -> buyingFragment!!.performChatSync()
            1 -> sealingFragment!!.performChatSync()
        }
    }

    /*
     *searching*/
    private fun checkSearchFiltre(searchText: String) {
        when (chatViewPager!!.currentItem) {
            0 -> buyingFragment!!.performFiltre(searchText)
            1 -> sealingFragment!!.performFiltre(searchText)
        }
    }

    /*
     *Animating the chat view. */
    private fun animateSearchView() {
        val animationUtils: Animation
        if (search_edit_view!!.visibility == View.GONE) {
            search_edit_view!!.visibility = View.VISIBLE
            search_intiate!!.visibility = View.GONE
            animationUtils = AnimationUtils.loadAnimation(activity, R.anim.search_view_animaton)
            search_edit_view!!.animation = animationUtils
        } else {
            search_edit_view!!.visibility = View.GONE
            search_intiate!!.visibility = View.VISIBLE
        }
    } /*@Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode)
    }*/
}