package com.zoptal.cellableapp.main.activity

import android.app.Activity
import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.annotation.Nullable
import com.google.gson.Gson
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.main.activity.ForgotPasswordActivity
import com.zoptal.cellableapp.pojo_class.ForgotPasswordPojo
import com.zoptal.cellableapp.utility.ApiUrl
import com.zoptal.cellableapp.utility.CommonClass
import com.zoptal.cellableapp.utility.OkHttp3Connection
import com.zoptal.cellableapp.utility.OkHttp3Connection.OkHttp3RequestCallback
import org.json.JSONException
import org.json.JSONObject
import java.util.*

/**
 * <h>ForgotPasswordActivity</h>
 *
 *
 * This class is called from Login screen. In this class we used to find the
 * forgotten password using user email-id.
 *
 * @since 4/5/2017
 * @author 3Embed
 * @version 1.0
 */
class ForgotPasswordActivity : AppCompatActivity(), View.OnClickListener {
    private var mActivity: Activity? = null
    private var eT_emailId: EditText? = null
    private var tV_send: TextView? = null
    private var isSendButtonEnabled = false
    private var mProgress_bar: ProgressBar? = null
    private var linear_rootElement: LinearLayout? = null
     override fun onCreate(@Nullable savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_forgot_pass)
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale)

        // request keyboard
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
        initVariables()
    }

    /**
     * <h>InitVariables</h>
     *
     *
     * In this method we used to assign all the xml variables and data member like mActivity.
     *
     */
    private fun initVariables() {
        mActivity = this@ForgotPasswordActivity
        CommonClass.statusBarColor(mActivity!!)
        mProgress_bar = findViewById(R.id.progress_bar) as ProgressBar?
        linear_rootElement = findViewById(R.id.linear_rootElement) as LinearLayout?
        val rL_send: RelativeLayout
        val rL_back_btn: RelativeLayout
        rL_send = findViewById(R.id.rL_send) as RelativeLayout
        rL_send.setOnClickListener(this)
        rL_back_btn = findViewById(R.id.rL_back_btn) as RelativeLayout
        rL_back_btn.setOnClickListener(this)
        tV_send = findViewById(R.id.tV_send) as TextView?
        CommonClass.setViewOpacity(mActivity, rL_send, 102, R.drawable.rect_purple_color_with_solid_shape)

        // EditText Email Address
        eT_emailId = findViewById(R.id.eT_emailId) as EditText?
        eT_emailId!!.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                val email = eT_emailId!!.text.toString()
                if (!email.isEmpty() && CommonClass.isValidEmail(email)) {
                    isSendButtonEnabled = true
                    CommonClass.setViewOpacity(mActivity, rL_send, 204, R.drawable.rect_purple_color_with_solid_shape)
                } else {
                    isSendButtonEnabled = false
                    CommonClass.setViewOpacity(mActivity, rL_send, 102, R.drawable.rect_purple_color_with_solid_shape)
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })
        eT_emailId!!.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                if (isSendButtonEnabled) resetPasswordApi()
                return@OnKeyListener true
            }
            false
        })
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.rL_back_btn -> onBackPressed()
            R.id.rL_send -> if (isSendButtonEnabled) resetPasswordApi()
        }
    }

    /**
     * <h>ResetPasswordApi</h>
     *
     *
     * In this method we used to do api call for forgot password.
     *
     */
    private fun resetPasswordApi() {
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            mProgress_bar!!.visibility = View.VISIBLE
            tV_send!!.visibility = View.GONE
            val requestDatas = JSONObject()
            try {
                requestDatas.put("type", "0")
                requestDatas.put("email", eT_emailId!!.text.toString())
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            OkHttp3Connection.doOkHttp3Connection(TAG, ApiUrl.RESET_PASSWORD, OkHttp3Connection.Request_type.POST, requestDatas, object : OkHttp3RequestCallback {
                 override fun onSuccess(result: String?, user_tag: String?) {
                    println("$TAG reset password res=$result")
                    mProgress_bar!!.visibility = View.GONE
                    tV_send!!.visibility = View.VISIBLE
                    val forgotPasswordPojo: ForgotPasswordPojo
                    val gson = Gson()
                    forgotPasswordPojo = gson.fromJson(result, ForgotPasswordPojo::class.java)
                    when (forgotPasswordPojo.code) {
                        "200" -> {
                            mProgress_bar!!.visibility = View.GONE
                            CommonClass.showSuccessSnackbarMsg(linear_rootElement, forgotPasswordPojo.message)
                            val t = Timer()
                            t.schedule(object : TimerTask() {
                                override fun run() {
                                    // when the task active then close the activity
                                    t.cancel()
                                    onBackPressed()
                                }
                            }, 3000)
                        }
                        "401" -> CommonClass.sessionExpired(mActivity!!)
                        else -> CommonClass.showSnackbarMessage(linear_rootElement, forgotPasswordPojo.message)
                    }
                }

                 override fun onError(error: String?, user_tag: String?) {
                    mProgress_bar!!.visibility = View.GONE
                    CommonClass.showSnackbarMessage(linear_rootElement, error)
                }
            })
        } else CommonClass.showSnackbarMessage(linear_rootElement, getResources().getString(R.string.NoInternetAccess))
    }

 override fun onBackPressed() {
        showKeyboard(InputMethodManager.HIDE_IMPLICIT_ONLY)
        finish()
        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate)
    }

    private fun showKeyboard(flag: Int) {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.toggleSoftInput(flag, 0)
    }

    companion object {
        private val TAG = ForgotPasswordActivity::class.java.simpleName
    }
}