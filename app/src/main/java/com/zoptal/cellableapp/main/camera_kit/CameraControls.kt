package com.zoptal.cellableapp.main.camera_kit

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Matrix
import android.os.Environment
import android.os.Handler
import android.os.SystemClock

import android.util.AttributeSet
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.animation.OvershootInterpolator
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.DrawableRes
import androidx.annotation.Nullable
import androidx.core.content.ContextCompat
//import butterknife.BindView
//import butterknife.ButterKnife
//import butterknife.OnTouch
import com.wonderkiln.camerakit.CameraKit
import com.wonderkiln.camerakit.CameraKitImage
import com.wonderkiln.camerakit.CameraKitVideo
import com.wonderkiln.camerakit.CameraView
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.utility.CapturedImage
import com.zoptal.cellableapp.utility.ClickListener
import com.zoptal.cellableapp.utility.CommonClass
import kotlinx.android.synthetic.main.camera_controls.view.*
import java.io.*

class CameraControls(context: Context, @Nullable attrs: AttributeSet?, defStyleAttr: Int) : LinearLayout(context, attrs, defStyleAttr) {
    private var cameraViewId = -1
    private var cameraView: CameraView? = null
    private var coverViewId = -1
    private var coverView: View? = null

//    @BindView(R.id.galleryButton)
//    var galleryButton: ImageView? = null

//    @BindView(R.id.flashButton)
//    var flashButton: ImageView? = null

//    @BindView(R.id.tV_timer)
//    var tV_timer: TextView? = null
    private val captureDownTime: Long = 0
    private var captureStartTime: Long = 0
    private var pendingVideoCapture = false
    private var capturingVideo = false
    private var mActivity: Activity
    private var clickListener: ClickListener

    /**
     * This is the output file for our picture.
     */
    private var mFile: File? = null

    constructor(context: Context) : this(context, null) {
        mActivity = context as Activity
        clickListener = context as ClickListener
    }

    constructor(context: Context, @Nullable attrs: AttributeSet?) : this(context, attrs, 0) {
        mActivity = context as Activity
        clickListener = context as ClickListener
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        if (cameraViewId != -1) {
            val view = rootView.findViewById<View>(cameraViewId)
            if (view is CameraView) {
                cameraView = view
                cameraView!!.isSoundEffectsEnabled = false
                cameraView!!.bindCameraKitListener(this)
                //setFacingImageBasedOnCamera();
            }
        }
        if (coverViewId != -1) {
            val view = rootView.findViewById<View>(coverViewId)
            if (view != null) {
                coverView = view
                coverView!!.visibility = View.GONE
            }
        }
        galleryButton.setOnTouchListener { view, motionEvent ->
            handleViewTouchFeedback(view, motionEvent)
            when (motionEvent.action) {
                MotionEvent.ACTION_UP -> {
                    clickListener.onItemClick(galleryButton, 1)
                }
            }
            return@setOnTouchListener true
        }
        flashButton.setOnTouchListener { view, motionEvent ->
            handleViewTouchFeedback(view, motionEvent)
            when (motionEvent.action) {
                MotionEvent.ACTION_UP -> {
                    if (cameraView!!.flash == CameraKit.Constants.FLASH_OFF) {
                        cameraView!!.flash = CameraKit.Constants.FLASH_ON
                        changeViewImageResource(view as ImageView, R.drawable.flash_light_on)
                    } else {
                        cameraView!!.flash = CameraKit.Constants.FLASH_OFF
                        changeViewImageResource(view as ImageView, R.drawable.flash_light_off)
                    }
                }
            }
            return@setOnTouchListener true
        }
        captureButton.setOnTouchListener { view, motionEvent ->
            handleViewTouchFeedback(view, motionEvent)
            if (motionEvent.action.equals(MotionEvent.ACTION_DOWN)) {
//                MotionEvent.ACTION_DOWN -> {
//                    run {}
//                    run {
                pendingVideoCapture = false
                if (capturingVideo) {
                    capturingVideo = false
                    cameraView!!.stopVideo()

                    // for timer
                    timeSwapBuff += timeInMilliseconds
                    customHandler.removeCallbacks(updateTimerThread)
                    tV_timer!!.text = ""
                    tV_timer!!.visibility = View.INVISIBLE
                } else {
                    if ((mActivity as CameraKitActivity).arrayListImgPath!!.size < 5) {
                        captureStartTime = System.currentTimeMillis()
                        cameraView!!.captureImage { event -> imageCaptured(event) }
                    } else {
                        CommonClass.showTopSnackBar((mActivity as CameraKitActivity).rL_rootview, mActivity.getString(R.string.you_can_select_only_upto))
                    }
                }
            } else if (motionEvent.action.equals(MotionEvent.ACTION_UP)){
//                }
//                MotionEvent.ACTION_UP -> {
                pendingVideoCapture = false
            if (capturingVideo) {
                capturingVideo = false
                cameraView!!.stopVideo()
                timeSwapBuff += timeInMilliseconds
                customHandler.removeCallbacks(updateTimerThread)
                tV_timer!!.text = ""
                tV_timer!!.visibility = View.INVISIBLE
            } else {
                if ((mActivity as CameraKitActivity).arrayListImgPath!!.size < 5) {
                    captureStartTime = System.currentTimeMillis()
                    cameraView!!.captureImage { event -> imageCaptured(event) }
                } else {
                    CommonClass.showTopSnackBar((mActivity as CameraKitActivity).rL_rootview, mActivity.getString(R.string.you_can_select_only_upto))
                }
            }

        }
//                }
//
//            }
            return@setOnTouchListener true
        }

    }

    private fun setFacingImageBasedOnCamera() {
        if (cameraView!!.isFacingFront) {
            galleryButton!!.setImageResource(R.drawable.ic_facing_back)
        } else {
            galleryButton!!.setImageResource(R.drawable.ic_facing_front)
        }
    }

    //@OnCameraKitEvent(CameraKitImage.class)
    fun imageCaptured(image: CameraKitImage) {
        val jpeg = image.jpeg

        /*Bitmap b = BitmapFactory.decodeByteArray(jpeg, 0, jpeg.length);
        int width=CommonClass.getDeviceWidth(mActivity);
        int height=CommonClass.getDeviceHeight(mActivity);
        height=height-CommonClass.dpToPx(mActivity,140);
        b=getResizedBitmap(b,height,width);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        b.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        jpeg = stream.toByteArray();
        b.recycle();*/jpeg?.let { saveImage(it) }
        val callbackTime = System.currentTimeMillis()
        ResultHolder.dispose()
        ResultHolder.image=(jpeg)
        ResultHolder.nativeCaptureSize=(cameraView!!.captureSize)
        ResultHolder.timeToCallback=(callbackTime - captureStartTime)
        /* Intent intent = new Intent(getContext(), PreviewActivity.class);
        getContext().startActivity(intent);*/
    }

    fun getResizedBitmap(bm: Bitmap, newHeight: Int, newWidth: Int): Bitmap {
        val width = bm.width
        val height = bm.height
        val scaleWidth = newWidth.toFloat() / width
        val scaleHeight = newHeight.toFloat() / height
        // create a matrix for the manipulation
        val matrix = Matrix()
        // resize the bit map
        matrix.postScale(scaleWidth, scaleHeight)
        // recreate the new Bitmap
        return Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false)
    }

    //    @OnCameraKitEvent(CameraKitVideo.class)
    fun videoCaptured(video: CameraKitVideo) {
        val videoFile = video.videoFile
        if (videoFile != null) {
            ResultHolder.dispose()
            ResultHolder.video=(videoFile)
            saveVideoFile(videoFile)
            ResultHolder.nativeCaptureSize=(cameraView!!.captureSize)
            val intent = Intent(context, PreviewActivity::class.java)
            context.startActivity(intent)
        }
    }

//    @OnTouch(R.id.captureButton)
//    fun onTouchCapture(view: View, motionEvent: MotionEvent): Boolean {
//        handleViewTouchFeedback(view, motionEvent)
//        when (motionEvent.action) {
//            MotionEvent.ACTION_DOWN -> {
//                run {}
//                run {
//                    pendingVideoCapture = false
//                    if (capturingVideo) {
//                        capturingVideo = false
//                        cameraView!!.stopVideo()
//
//                        // for timer
//                        timeSwapBuff += timeInMilliseconds
//                        customHandler.removeCallbacks(updateTimerThread)
//                        tV_timer!!.text = ""
//                        tV_timer!!.visibility = View.INVISIBLE
//                    } else {
//                        if ((mActivity as CameraKitActivity).arrayListImgPath!!.size < 5) {
//                            captureStartTime = System.currentTimeMillis()
//                            cameraView!!.captureImage { event -> imageCaptured(event) }
//                        } else {
//                            CommonClass.showTopSnackBar((mActivity as CameraKitActivity).rL_rootview, mActivity.getString(R.string.you_can_select_only_upto))
//                        }
//                    }
//                }
//            }
//            MotionEvent.ACTION_UP -> {
//                pendingVideoCapture = false
//                if (capturingVideo) {
//                    capturingVideo = false
//                    cameraView!!.stopVideo()
//                    timeSwapBuff += timeInMilliseconds
//                    customHandler.removeCallbacks(updateTimerThread)
//                    tV_timer!!.text = ""
//                    tV_timer!!.visibility = View.INVISIBLE
//                } else {
//                    if ((mActivity as CameraKitActivity).arrayListImgPath!!.size < 5) {
//                        captureStartTime = System.currentTimeMillis()
//                        cameraView!!.captureImage { event -> imageCaptured(event) }
//                    } else {
//                        CommonClass.showTopSnackBar((mActivity as CameraKitActivity).rL_rootview, mActivity.getString(R.string.you_can_select_only_upto))
//                    }
//                }
//            }
//        }
//        return true
//    }

//    @OnTouch(R.id.galleryButton)
//    fun onTouchFacing(view: View, motionEvent: MotionEvent): Boolean {
//        handleViewTouchFeedback(view, motionEvent)
//        when (motionEvent.action) {
//            MotionEvent.ACTION_UP -> {
//                clickListener.onItemClick(galleryButton, 1)
//            }
//        }
//        return true
//    }

//    @OnTouch(R.id.flashButton)
//    fun onTouchFlash(view: View, motionEvent: MotionEvent): Boolean {
//        handleViewTouchFeedback(view, motionEvent)
//        when (motionEvent.action) {
//            MotionEvent.ACTION_UP -> {
//                if (cameraView!!.flash == CameraKit.Constants.FLASH_OFF) {
//                    cameraView!!.flash = CameraKit.Constants.FLASH_ON
//                    changeViewImageResource(view as ImageView, R.drawable.flash_light_on)
//                } else {
//                    cameraView!!.flash = CameraKit.Constants.FLASH_OFF
//                    changeViewImageResource(view as ImageView, R.drawable.flash_light_off)
//                }
//            }
//        }
//        return true
//    }

    fun handleViewTouchFeedback(view: View, motionEvent: MotionEvent): Boolean {
        return when (motionEvent.action) {
            MotionEvent.ACTION_DOWN -> {
                touchDownAnimation(view)
                true
            }
            MotionEvent.ACTION_UP -> {
                touchUpAnimation(view)
                true
            }
            else -> {
                true
            }
        }
    }

    fun touchDownAnimation(view: View) {
        view.animate()
                .scaleX(0.88f)
                .scaleY(0.88f)
                .setDuration(300)
                .setInterpolator(OvershootInterpolator())
                .start()
    }

    fun touchUpAnimation(view: View) {
        view.animate()
                .scaleX(1f)
                .scaleY(1f)
                .setDuration(300)
                .setInterpolator(OvershootInterpolator())
                .start()
    }

    fun changeViewImageResource(imageView: ImageView, @DrawableRes resId: Int) {
        imageView.rotation = 0f
        imageView.animate()
                .rotationBy(360f)
                .setDuration(400)
                .setInterpolator(OvershootInterpolator())
                .start()
        imageView.postDelayed({ imageView.setImageResource(resId) }, 120)
    }

    private fun saveImage(jpeg: ByteArray) {
        val folderPath: String
        folderPath = if (Environment.getExternalStorageState() == Environment.MEDIA_MOUNTED) {
            Environment.getExternalStorageDirectory().toString() + "/" + resources.getString(R.string.app_name)
        } else {
            context.filesDir.toString() + "/" + resources.getString(R.string.app_name)
        }
        val folder = File(folderPath)
        if (!folder.exists() && !folder.isDirectory) {
            folder.mkdirs()
        }
        if (mFile == null) {
            mFile = File(folderPath, System.currentTimeMillis().toString() + ".jpg")
        }
        try {
            if (!mFile!!.exists()) {
                mFile!!.createNewFile()
            }
        } catch (e: Exception) {
            Toast.makeText(this.context, "Image Not saved", Toast.LENGTH_SHORT).show()
        }
        var output: FileOutputStream? = null
        try {
            output = FileOutputStream(mFile)
            output.write(jpeg)
        } catch (e: IOException) {
            e.printStackTrace()
        } finally {
            if (null != output) {
                try {
                    output.close()
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
        }

        // Get a handler that can be used to post to the main thread
        val mainHandler = Handler(context.mainLooper)
        val myRunnable = Runnable { // set imageView to CameraKitActivity horizontal images list
            val image = CapturedImage()
            image.imagePath = mFile!!.absolutePath
            image.rotateAngle = 0
            (mActivity as CameraKitActivity).arrayListImgPath!!.add(image)
            (mActivity as CameraKitActivity).imagesHorizontalRvAdap!!.notifyItemInserted((mActivity as CameraKitActivity).arrayListImgPath!!.size)
            //imagesHorizontalRvAdap.notifyDataSetChanged();
            (mActivity as CameraKitActivity).isToCaptureImage = (mActivity as CameraKitActivity).arrayListImgPath!!.size < 5

            // enable upload button
            if ((mActivity as CameraKitActivity).arrayListImgPath!!.size > 0) (mActivity as CameraKitActivity).tV_upload!!.setTextColor(ContextCompat.getColor(mActivity, R.color.colorAccent))
            mFile = null
        }
        mainHandler.post(myRunnable)
    }

    private fun saveVideoFile(videoFile: File) {
        val folderPath: String
        folderPath = if (Environment.getExternalStorageState() == Environment.MEDIA_MOUNTED) {
            Environment.getExternalStorageDirectory().toString() + "/" + resources.getString(R.string.app_name)
        } else {
            context.filesDir.toString() + "/" + resources.getString(R.string.app_name)
        }
        val folder = File(folderPath)
        if (!folder.exists() && !folder.isDirectory) {
            folder.mkdirs()
        }
        var `in`: InputStream? = null
        var out: OutputStream? = null
        try {
            Log.d("video file path", """
     ${videoFile.absolutePath}
     ${videoFile.path}
     ${videoFile.canonicalPath}
     """.trimIndent())
            `in` = FileInputStream(videoFile.absolutePath)
            out = FileOutputStream(folderPath + "/" + System.currentTimeMillis().toString() + ".mp4")
            val buffer = ByteArray(1024)
            var read: Int
            while (`in`.read(buffer).also { read = it } != -1) {
                out.write(buffer, 0, read)
            }
            `in`.close()
            `in` = null

            // write the output file (You have now copied the file)
            out.flush()
            out.close()
            out = null
        } catch (fnfe1: FileNotFoundException) {
            Log.e("tag", fnfe1.message)
        } catch (e: Exception) {
            Log.e("tag", e.message)
        }
    }

    private val startTime = 0L
    private val customHandler = Handler()
    var timeInMilliseconds = 0L
    var timeSwapBuff = 0L
    var updatedTime = 0L
    private val updateTimerThread: Runnable = object : Runnable {
        override fun run() {
            timeInMilliseconds = SystemClock.uptimeMillis() - startTime
            updatedTime = timeSwapBuff + timeInMilliseconds
            var secs = (updatedTime / 1000).toInt()
            val mins = secs / 60
            secs = secs % 60
            val milliseconds = (updatedTime % 1000).toInt()
            tV_timer!!.text = ("" + mins + ":"
                    + String.format("%02d", secs))
            customHandler.postDelayed(this, 0)
        }
    }

    init {
        mActivity = context as Activity
        clickListener = context as ClickListener
        LayoutInflater.from(getContext()).inflate(R.layout.camera_controls, this)
//        ButterKnife.bind(this)
        if (attrs != null) {
            val a = context.getTheme().obtainStyledAttributes(
                    attrs,
                    R.styleable.CameraControls,
                    0, 0)
            try {
                cameraViewId = a.getResourceId(R.styleable.CameraControls_camera, -1)
                coverViewId = a.getResourceId(R.styleable.CameraControls_cover, -1)
            } finally {
                a.recycle()
            }
        }
    }
}