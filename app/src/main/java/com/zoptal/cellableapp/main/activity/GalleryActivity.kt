package com.zoptal.cellableapp.main.activity

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.database.Cursor
import android.os.Bundle
import android.provider.MediaStore
import androidx.appcompat.app.AppCompatActivity

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.widget.RelativeLayout
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.GridLayoutManager
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.adapter.GalleryImgHorizontalRvAdap
import com.zoptal.cellableapp.adapter.GalleryRvAdap
import com.zoptal.cellableapp.fcm_push_notification.Config
import com.zoptal.cellableapp.fcm_push_notification.NotificationMessageDialog
import com.zoptal.cellableapp.fcm_push_notification.NotificationUtils.Companion.clearNotifications
import com.zoptal.cellableapp.main.activity.GalleryActivity
import com.zoptal.cellableapp.pojo_class.GalleryImagePojo
import com.zoptal.cellableapp.utility.ItemDecorationRvItems
import com.zoptal.cellableapp.utility.RunTimePermission
import com.zoptal.cellableapp.utility.VariableConstants
import java.util.*

/**
 * <h>GalleryActivity</h>
 *
 *
 * In this class we used to retrieve the all images from gallery and show in
 * grids. Once user clicks on any image then mark and an image selected and
 * shows all selected images below.
 *
 * @since 20-Jun-17
 */
class GalleryActivity : AppCompatActivity(), View.OnClickListener {
    private var permissionsArray: Array<String>? = null
    private var runTimePermission: RunTimePermission? = null
    private var galleryRvAdap: GalleryRvAdap? = null
    private var arrayListGalleryImg: ArrayList<GalleryImagePojo>? = null
    var aL_cameraImgPath: ArrayList<String>? = null
    private var rotationAngles = ArrayList<Int>()
    var imagesHorizontalRvAdap: GalleryImgHorizontalRvAdap? = null
    var rL_rootview: RelativeLayout? = null
    private var mNotificationMessageDialog: NotificationMessageDialog? = null
    private var isFromAddNewPhoto = false
     override fun onCreate(@Nullable savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_gallery)
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale)
        initVariable()
    }

    /**
     * <h>initVariable</h>
     *
     *
     * In this method we used to initialize the all variables.
     *
     */
    private fun initVariable() {
        val mActivity: Activity = this@GalleryActivity
        mNotificationMessageDialog = NotificationMessageDialog(mActivity)
        arrayListGalleryImg = ArrayList()

        // receive images from last activity
        val intent: Intent = getIntent()
        aL_cameraImgPath = intent.getStringArrayListExtra("arrayListImgPath")
        rotationAngles = intent.getIntegerArrayListExtra("rotationAngles")
        isFromAddNewPhoto = intent.getBooleanExtra("isFromAddNewPhoto", false)
        for (path in aL_cameraImgPath!!) {
            println("$TAG path=$path")
        }

        // back button
        val rL_back_btn = findViewById(R.id.rL_back_btn) as RelativeLayout
        rL_back_btn.setOnClickListener(this)

        // camera icon
        val rL_camera_icon = findViewById(R.id.rL_camera_icon) as RelativeLayout
        rL_camera_icon.setOnClickListener(this)

        // initialize permission array
        permissionsArray = arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        runTimePermission = RunTimePermission(mActivity, permissionsArray!!, true)

        // set adapter
        galleryRvAdap = GalleryRvAdap(mActivity, arrayListGalleryImg!!, aL_cameraImgPath!!)
        val gridLayoutManager = GridLayoutManager(mActivity, 3)
        val rV_galleryImg: RecyclerView = findViewById(R.id.rV_galleryImg) as RecyclerView
        rV_galleryImg.setLayoutManager(gridLayoutManager)

        // set space equility between recycler view items
        val spanCount = 3 // 3 columns
        val spacing = 5 // 50px
        rV_galleryImg.addItemDecoration(ItemDecorationRvItems(spanCount, spacing, true))
        rV_galleryImg.setAdapter(galleryRvAdap)

        // retrieve images from gallery
        if (runTimePermission!!.checkPermissions(permissionsArray!!)) {
            if (loadPhotosFromNativeGallery() != null && loadPhotosFromNativeGallery()!!.size > 0) {
                galleryRvAdap!!.notifyDataSetChanged()
            }
        } else runTimePermission!!.requestPermission()

        // set adapter for horizontal images
        rL_rootview = findViewById(R.id.rL_rootview) as RelativeLayout?
        val rL_images = findViewById(R.id.rL_images) as RelativeLayout
        if (aL_cameraImgPath != null && aL_cameraImgPath!!.size > 0) rL_images.visibility = View.VISIBLE
        imagesHorizontalRvAdap = GalleryImgHorizontalRvAdap(mActivity, aL_cameraImgPath!!, galleryRvAdap!!, arrayListGalleryImg!!, rotationAngles)
        val rV_capturedImages: RecyclerView = findViewById(R.id.rV_capturedImages) as RecyclerView
        val linearLayoutManager = LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false)
        rV_capturedImages.setLayoutManager(linearLayoutManager)
        rV_capturedImages.setAdapter(imagesHorizontalRvAdap)

        // Done button
        val rL_done = findViewById(R.id.rL_done) as RelativeLayout
        rL_done.setOnClickListener(this)
    }

    override fun onResume() {
        super.onResume()

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.REGISTRATION_COMPLETE))

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.PUSH_NOTIFICATION))

        // clear the notification area when the app is opened
        clearNotifications(getApplicationContext())
    }

   override fun onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!)
        super.onPause()
    }

    /**
     * <h>loadPhotosFromNativeGallery</h>
     *
     *
     * In this method we used to retrieve all gallery images from device and store all
     * images into a list.
     *
     * @return The list consisting of all images path
     */
    private fun loadPhotosFromNativeGallery(): ArrayList<String>? {
        val columns = arrayOf(MediaStore.Images.Media.DATA, MediaStore.Images.Media._ID)
        val orderBy = MediaStore.Images.Media.DATE_TAKEN
        val imagecursor: Cursor = managedQuery(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, columns, null, null, "$orderBy DESC")
        val imageUrls = ArrayList<String>()
        for (i in 0 until imagecursor.count) {
            imagecursor.moveToPosition(i)
            val dataColumnIndex = imagecursor.getColumnIndex(MediaStore.Images.Media.DATA)
            imageUrls.add(imagecursor.getString(dataColumnIndex))
            val galleryImagePojo = GalleryImagePojo()
            galleryImagePojo.galleryImagePath = imagecursor.getString(dataColumnIndex)
            arrayListGalleryImg!!.add(galleryImagePojo)
            println("=====> Array path => " + imageUrls[i] + " " + "size=" + imageUrls.size)
        }
        if (arrayListGalleryImg!!.size > 0) {
            for (cameraImgPath in aL_cameraImgPath!!) {
                for (galleryImagePojo in arrayListGalleryImg!!) {
                    if (cameraImgPath == galleryImagePojo.galleryImagePath) {
                        galleryImagePojo.isSelected = true
                        galleryImagePojo.rotationAngle = rotationAngles[0]
                    }
                }
            }
        }
        return imageUrls
    }

    override fun onRequestPermissionsResult(requestCode: Int, @NonNull permissions: Array<String?>, @NonNull grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            VariableConstants.PERMISSION_REQUEST_CODE -> {
                println("grant result=" + grantResults.size)
                if (grantResults.size > 0) {
                    var count = 0
                    while (count < grantResults.size) {
                        if (grantResults[count] != PackageManager.PERMISSION_GRANTED) runTimePermission!!.allowPermissionAlert(permissions[count]!!)
                        count++
                    }
                    println("isAllPermissionGranted=" + runTimePermission!!.checkPermissions(permissionsArray!!))
                    if (runTimePermission!!.checkPermissions(permissionsArray!!)) {
                        //imageUrls = loadPhotosFromNativeGallery();
                        if (loadPhotosFromNativeGallery() != null && loadPhotosFromNativeGallery()!!.size > 0) {
                            galleryRvAdap!!.notifyDataSetChanged()
                        }
                    }
                }
            }
        }
    }

 override fun onBackPressed() {
        finish()
        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.rL_back_btn -> onBackPressed()
            R.id.rL_done -> moveToCameraScreen()
            R.id.rL_camera_icon -> moveToCameraScreen()
        }
    }

    /**
     * <h>MoveToCameraScreen</h>
     *
     *
     * In this method we used to move to the previous activity i.e CameraActivity class and
     * pass the images list.
     *
     */
    private fun moveToCameraScreen() {
        val intent = Intent()
        intent.putExtra("arrayListImgPath", aL_cameraImgPath)
        intent.putExtra("isFromAddNewPhoto", isFromAddNewPhoto)
        setResult(VariableConstants.SELECT_GALLERY_IMG_REQ_CODE, intent)
        onBackPressed()
    }

    companion object {
        private val TAG = GalleryActivity::class.java.simpleName
    }
}