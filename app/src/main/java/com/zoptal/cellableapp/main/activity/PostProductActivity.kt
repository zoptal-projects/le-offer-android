package com.zoptal.cellableapp.main.activity

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.MediaScannerConnection
import android.net.Uri
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SwitchCompat
import androidx.core.content.FileProvider
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.facebook.CallbackManager
import com.facebook.FacebookSdk
import com.facebook.share.model.ShareLinkContent
import com.facebook.share.widget.ShareDialog
import com.google.firebase.dynamiclinks.DynamicLink.AndroidParameters
import com.google.firebase.dynamiclinks.DynamicLink.SocialMetaTagParameters
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks
import com.google.gson.Gson
import com.zoptal.cellableapp.BuildConfig
import com.zoptal.cellableapp.Face_book_manger.Facebook_login
import com.zoptal.cellableapp.Face_book_manger.Facebook_login.Facebook_callback
import com.zoptal.cellableapp.Face_book_manger.Facebook_share_mamager
import com.zoptal.cellableapp.Face_book_manger.Facebook_share_mamager.Companion.instance
import com.zoptal.cellableapp.Face_book_manger.Facebook_share_mamager.Share_callback
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.Twiter_manager.TweetManger
import com.zoptal.cellableapp.Twiter_manager.TweetManger.TweetSuccess
import com.zoptal.cellableapp.Uploader.FileUploader
import com.zoptal.cellableapp.Uploader.ProductImageDatas
import com.zoptal.cellableapp.Uploader.UploadedCallback
import com.zoptal.cellableapp.adapter.IwantAdapter
import com.zoptal.cellableapp.adapter.PostProductImagesRvAdap
import com.zoptal.cellableapp.fcm_push_notification.Config
import com.zoptal.cellableapp.fcm_push_notification.NotificationMessageDialog
import com.zoptal.cellableapp.fcm_push_notification.NotificationUtils.Companion.clearNotifications
import com.zoptal.cellableapp.get_current_location.FusedLocationReceiver
import com.zoptal.cellableapp.get_current_location.FusedLocationService
import com.zoptal.cellableapp.main.activity.ChangeLocationActivity
import com.zoptal.cellableapp.main.activity.CurrencyListActivity
import com.zoptal.cellableapp.main.activity.HomePageActivity
import com.zoptal.cellableapp.main.activity.PostProductActivity
import com.zoptal.cellableapp.pojo_class.PostProductMainPojo
import com.zoptal.cellableapp.pojo_class.product_category.FilterKeyValue
import com.zoptal.cellableapp.pojo_class.product_details_pojo.SwapPost
import com.zoptal.cellableapp.recyleview_drag_drop.OnCustomerListChangedListener
import com.zoptal.cellableapp.recyleview_drag_drop.OnStartDragListener
import com.zoptal.cellableapp.recyleview_drag_drop.SimpleItemTouchHelperCallback
import com.zoptal.cellableapp.utility.*
import com.zoptal.cellableapp.utility.OkHttp3Connection.OkHttp3RequestCallback

import org.apache.http.HttpResponse
import org.apache.http.HttpStatus
import org.apache.http.client.ClientProtocolException
import org.apache.http.client.HttpClient
import org.apache.http.client.methods.HttpGet
import org.apache.http.impl.client.DefaultHttpClient
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.*
import java.util.*

/**
 * <h>PostProductActivity</h>
 *
 *
 * This class is getting called from Camera screen. In this class first of all
 * we used to receive the images from previous class and show on the top of
 * the screen horizontally. Apart from that we have option to fill field like
 * product name, description, category etc. then we send all these to our server.
 *
 *
 * @author 3Embed
 * @version 1.0
 * @since 5/3/2017
 */
class PostProductActivity : AppCompatActivity(), View.OnClickListener, OnCustomerListChangedListener, OnStartDragListener {
    var rL_rootElement: RelativeLayout? = null
    private var mActivity: Activity? = null
    private var tV_category: TextView? = null
    private var tV_condition: TextView? = null
    private var tV_currency: TextView? = null
    private var tV_currency_symbol: TextView? = null
    private var tV_current_location: TextView? = null
    private var progress_bar_location: ProgressBar? = null
    private var locationService: FusedLocationService? = null
    private var permissionsArray: Array<String>? = null
    private var runTimePermission: RunTimePermission? = null
    private var lat = ""
    private var lng = ""
    private var negotiable = "0"
    private var city: String? = ""
    private var countryCode = ""
    private var mSessionManager: SessionManager? = null
    private var eT_title: EditText? = null
    private var eT_description: EditText? = null
    private var eT_price: EditText? = null
    private var arrayListImgPath: ArrayList<String>? = null
    private var rotationAngles: ArrayList<Int>? = null
    private val mCapturedImageData = ArrayList<CapturedImage?>()
    private var mNotificationMessageDialog: NotificationMessageDialog? = null
    private var isToPostItem = false
    private var isTwitterSharingOn = false
    private var isFacebookSharingOn = false
    private var isInstagramSharingOn = false
    private var mDialogBox: DialogBox? = null
    private var facebook_login: Facebook_login? = null
    private var callbackManager: CallbackManager? = null
    private var facebook_share_mamager: Facebook_share_mamager? = null
    private var btn_cancel: TextView? = null
    private val linear_filter: LinearLayout? = null

    // advanced categories elements
    private var subCategory = ""
    private var filterKeyValues: ArrayList<FilterKeyValue>? = null

    // exchanges elements
    private var willingtoExchangeLl: RelativeLayout? = null
    private var iWantRv: RecyclerView? = null
    private val iWantArrayList = ArrayList<SwapPost>()
    private var iwantAdapter: IwantAdapter? = null
    private val arrayListPostIds = ArrayList<String>()
    private var isSwap = 0
    private var mItemTouchHelper: ItemTouchHelper? = null
    private var imagesHorizontalRvAdap: PostProductImagesRvAdap? = null
    private var tV_add_more_image: TextView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        facebook_login = Facebook_login(this)
        callbackManager = CallbackManager.Factory.create()
        setContentView(R.layout.activity_post_product)
        facebook_share_mamager = instance
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale)
        initializeVariable()
    }

    /**
     * <h>InitializeVariable</h>
     *
     *
     * In this method we used to initialize the xml data member and other variables.
     *
     */
    private fun initializeVariable() {
        mActivity = this@PostProductActivity
        mDialogBox = DialogBox(mActivity!!)
        isToPostItem = true
        mNotificationMessageDialog = NotificationMessageDialog(mActivity)
        mSessionManager = SessionManager(mActivity!!)
        CommonClass.statusBarColor(mActivity!!)

        // Receiving datas from Camera activity class
        val intent = intent
        arrayListImgPath = intent.getStringArrayListExtra("arrayListImgPath")
        rotationAngles = intent.getIntegerArrayListExtra("rotationAngles")
        var image: CapturedImage
        for (i in rotationAngles!!.indices) {
            image = CapturedImage()
            image.imagePath = arrayListImgPath?.get(i)
            image.rotateAngle = rotationAngles!!.get(i)
            mCapturedImageData.add(image)
        }

        // add more images text
        tV_add_more_image = findViewById<View>(R.id.tV_add_more_image) as TextView

        // set adpter for horizontal images
        if (arrayListImgPath != null && arrayListImgPath!!.size > 0) {
            when (arrayListImgPath!!.size) {
                1 -> tV_add_more_image!!.text = resources.getString(R.string.add_upto_4_more_img)
                2 -> tV_add_more_image!!.text = resources.getString(R.string.add_upto_3_more_img)
                3 -> tV_add_more_image!!.text = resources.getString(R.string.add_upto_2_more_img)
                4 -> tV_add_more_image!!.text = resources.getString(R.string.add_upto_1_more_img)
                5 -> tV_add_more_image!!.visibility = View.GONE
            }
            imagesHorizontalRvAdap = PostProductImagesRvAdap(mActivity!!, mCapturedImageData, tV_add_more_image!!, this, this)
            val rV_cameraImages = findViewById<View>(R.id.rV_cameraImages) as RecyclerView
            val linearLayoutManager = LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false)
            rV_cameraImages.layoutManager = linearLayoutManager
            val callback: ItemTouchHelper.Callback = SimpleItemTouchHelperCallback(imagesHorizontalRvAdap!!)
            mItemTouchHelper = ItemTouchHelper(callback)
            mItemTouchHelper!!.attachToRecyclerView(rV_cameraImages)
            rV_cameraImages.adapter = imagesHorizontalRvAdap
        }

        // find current location
        rL_rootElement = findViewById<View>(R.id.rL_rootElement) as RelativeLayout
        progress_bar_location = findViewById<View>(R.id.progress_bar_location) as ProgressBar
        tV_current_location = findViewById<View>(R.id.tV_current_location) as TextView
        val tV_change_loc = findViewById<View>(R.id.tV_change_loc) as TextView
        tV_change_loc.setOnClickListener(this)
        permissionsArray = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
        runTimePermission = RunTimePermission(mActivity!!, permissionsArray!!, false)
        if (runTimePermission!!.checkPermissions(permissionsArray!!)) {
            currentLocation
        } else {
            runTimePermission!!.requestPermission()
        }

        // Back button
        val rL_back_btn: RelativeLayout
        val rL_product_category: RelativeLayout
        val rL_conditions: RelativeLayout
        val rL_currency: RelativeLayout
        rL_back_btn = findViewById<View>(R.id.rL_back_btn) as RelativeLayout
        rL_back_btn.setOnClickListener(this)

        // title
        eT_title = findViewById<View>(R.id.eT_title) as EditText
        eT_title!!.requestFocus()

        // Description
        eT_description = findViewById<View>(R.id.eT_description) as EditText

        // price
        eT_price = findViewById<View>(R.id.eT_price) as EditText

        //cancel button
        btn_cancel = findViewById<View>(R.id.btn_cancel) as TextView
        btn_cancel!!.setOnClickListener(this)

        // switch negotiable
        val switch_negotiable: SwitchCompat = findViewById<View>(R.id.switch_negotiable) as SwitchCompat
        switch_negotiable.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { buttonView, isChecked -> negotiable = if (isChecked) "1" else "0" })

        // category
        rL_product_category = findViewById<View>(R.id.rL_product_category) as RelativeLayout
        rL_product_category.setOnClickListener(this)
        tV_category = findViewById<View>(R.id.tV_category) as TextView

        // condition
        rL_conditions = findViewById<View>(R.id.rL_conditions) as RelativeLayout
        rL_conditions.setOnClickListener(this)
        tV_condition = findViewById<View>(R.id.tV_condition) as TextView

        // currency
        rL_currency = findViewById<View>(R.id.rL_currency) as RelativeLayout
        rL_currency.setOnClickListener(this)
        tV_currency = findViewById<View>(R.id.tV_currency) as TextView
        tV_currency_symbol = findViewById<View>(R.id.tV_currency_symbol) as TextView

        // for exchanges
        iWantRv = findViewById<View>(R.id.iWantRv) as RecyclerView
        iwantAdapter = IwantAdapter(iWantArrayList, this)
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        iWantRv!!.layoutManager = layoutManager
        iWantRv!!.adapter = iwantAdapter
        willingtoExchangeLl = findViewById<View>(R.id.willingtoExchangeLl) as RelativeLayout

        // set default currency
        setDefaultCurrency()

        // post product
        val rL_postProduct = findViewById<View>(R.id.rL_postProduct) as RelativeLayout
        rL_postProduct.setOnClickListener(this)

        // share on twitter
        val switch_twitter: SwitchCompat = findViewById<View>(R.id.switch_twitter) as SwitchCompat
        switch_twitter.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                if (checkAppinstall("com.twitter.android")) {
                    isTwitterSharingOn = true
                } else {
                    CommonClass.showSnackbarMessage(rL_rootElement, "Twitter not installed.")
                    buttonView.isChecked = false
                    //System.out.println(TAG+ " Instagram not install");
                    isTwitterSharingOn = false
                }
                /*if(TweetManger.getInstance().isUserLoggedIn())
                    {
                        isTwitterSharingOn = true;
                    }else
                    {
                        isTwitterSharingOn = false;
                        TweetManger.getInstance().doLogin(VariableConstants.TWEETER_REQUEST_CODE,mActivity,null);
                    }*/
            } else isTwitterSharingOn = false
        })

        // Share on Facebook
        val switch_fb: SwitchCompat = findViewById<View>(R.id.switch_fb) as SwitchCompat
        switch_fb.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->
            isFacebookSharingOn = if (isChecked) {
                true
                //checkForPermission();
            } else false
        })

        // Share on Instagram
        isInstagramSharingOn = false
        val switch_instagram: SwitchCompat = findViewById<View>(R.id.switch_instagram) as SwitchCompat
        switch_instagram.setOnCheckedChangeListener(
                CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->
                    if (isChecked) {
                        if (checkAppinstall("com.instagram.android")) {
                            isInstagramSharingOn = true
                        } else {
                            CommonClass.showSnackbarMessage(rL_rootElement, "Instagram not installed.")
                            buttonView.isChecked = false
                            //System.out.println(TAG+ " Instagram not install");
                            isInstagramSharingOn = false
                        }
                    } else isInstagramSharingOn = false
                })

        // switch exchange
        val switch_exchange: SwitchCompat = findViewById<View>(R.id.switch_exchange) as SwitchCompat
        switch_exchange.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                startWillingtoExchageActivity()
                willingtoExchangeLl!!.visibility = View.VISIBLE
                isSwap = 1
            } else {
                willingtoExchangeLl!!.visibility = View.GONE
                isSwap = 0
            }
        })
    }

    private fun checkAppinstall(uri: String): Boolean {
        val pm = packageManager
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES)
            return true
        } catch (e: PackageManager.NameNotFoundException) {
        }
        return false
    }

    private fun checkForPermission() {
        val permission = arrayOf("publish_actions")
        facebook_login!!.ask_PublishPermission(callbackManager!!, permission, object : Facebook_callback {
            override fun success(id: String?) {
                isFacebookSharingOn = true
            }

            override fun error(error: String?) {
                isFacebookSharingOn = false
            }

            override fun cancel(cancel: String?) {
                isFacebookSharingOn = false
            }
        })
    }

    override fun onResume() {
        super.onResume()

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.REGISTRATION_COMPLETE))

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.PUSH_NOTIFICATION))

        // clear the notification area when the app is opened
        clearNotifications(applicationContext)
    }

    override fun onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!)
        super.onPause()
    }

    /**
     * <h>SetDefaultCurrency</h>
     *
     *
     * In this method we used to find the current country currency.
     *
     */
    private fun setDefaultCurrency() {
//        val map = CommonClass.getCurrencyLocaleMap()
//        val countryIsoCode = Locale.getDefault().country
//        val locale = Locale("EN", countryIsoCode)
//        val currency = Currency.getInstance(locale)
//        println("$TAG currency=$currency")
//        val symbol = currency.getSymbol(map[currency])
//        if (symbol != null && !symbol.isEmpty()) {
//            tV_currency!!.text = currency.toString()
//            tV_currency_symbol!!.text = symbol
//        }
        tV_currency!!.text = "USD"
        tV_currency_symbol!!.text = "US$"
    }// call google map api

    /*if (!address.isEmpty())
    {
        progress_bar_location.setVisibility(View.GONE);
        tV_current_location.setVisibility(View.VISIBLE);
        tV_current_location.setText(address);
    }*/

    /**
     * In this method we find current location using FusedLocationApi.
     * in this we have onUpdateLocation() method in which we check if
     * its not null then We call guest user api.
     */
    private val currentLocation: Unit
        private get() {
            progress_bar_location!!.visibility = View.VISIBLE
            locationService = FusedLocationService(mActivity!!, object : FusedLocationReceiver {
                override fun onUpdateLocation() {
                    val currentLocation = locationService!!.receiveLocation()
                    if (currentLocation != null) {
                        lat = currentLocation.latitude.toString()
                        lng = currentLocation.longitude.toString()
                        if (isLocationFound(lat, lng)) {
                            println("$TAG lat=$lat lng=$lng")
                            mSessionManager!!.currentLat = lat
                            mSessionManager!!.currentLng = lng
                            val address = CommonClass.getCompleteAddressString(mActivity, currentLocation.latitude, currentLocation.longitude)
                            println("$TAG complete address=$address")
                            val url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=+" + lat + "+,+" + lng + "+&location_type=ROOFTOP&result_type=street_address&key=" + resources.getString(R.string.google_maps_key)
                            city = CommonClass.getCityName(mActivity!!, currentLocation.latitude, currentLocation.longitude)
                            countryCode = CommonClass.getCountryCode(mActivity!!, currentLocation.latitude, currentLocation.longitude)
                            val countryName = CommonClass.getCountryName(mActivity!!, currentLocation.latitude, currentLocation.longitude)
                            var location = ""
                            location = if (city == null || city!!.isEmpty()) CommonClass.getFirstCaps(countryName) else if (countryName == null || countryName.isEmpty()) CommonClass.getFirstCaps(city) else CommonClass.getFirstCaps(city) + ", " + CommonClass.getFirstCaps(countryName)

                            // call google map api
                            if (location.isEmpty()) RequestTask().execute(url)
                            if (!location.isEmpty()) {
                                progress_bar_location!!.visibility = View.GONE
                                tV_current_location!!.visibility = View.VISIBLE
                                tV_current_location!!.text = location
                            }

                            /*if (!address.isEmpty())
                            {
                                progress_bar_location.setVisibility(View.GONE);
                                tV_current_location.setVisibility(View.VISIBLE);
                                tV_current_location.setText(address);
                            }*/
                        }
                    }
                }
            }
            )
        }

    override fun onNoteListChanged(capturedImageList: List<CapturedImage?>?) {
        arrayListImgPath!!.clear()
        for (i in capturedImageList!!.indices) {
            arrayListImgPath!!.add(capturedImageList[i]!!.imagePath!!)
            println(TAG + " " + "position:" + i + " " + capturedImageList[i]!!.imagePath)
        }
    }

    override fun onUpdateImagesListChanged(productImageDatasList: List<ProductImageDatas?>?) {

    }

    override fun onStartDrag(viewHolder: RecyclerView.ViewHolder?) {
        mItemTouchHelper!!.startDrag(viewHolder!!)
    }


    /**
     * In this method we used to check whether current lat and
     * long has been received or not.
     *
     * @param lat The current latitude
     * @param lng The current longitude
     * @return boolean flag true or false
     */
    private fun isLocationFound(lat: String?, lng: String?): Boolean {
        return !(lat == null || lat.isEmpty()) && !(lng == null || lng.isEmpty())
    }

    /**
     * validation either all the mandatory fields are filled or not
     * if it is filled then call api method to post the product.
     */
    private fun checkPostProduct() {
        // title
        if (eT_title!!.text.toString().isEmpty()) {
            CommonClass.showSnackbarMessage(rL_rootElement, resources.getString(R.string.please_add_title))
        } else if (eT_description!!.text.toString().isEmpty()) {
            CommonClass.showSnackbarMessage(rL_rootElement, resources.getString(R.string.please_add_description))
        } else if (tV_category!!.text.toString().isEmpty()) {
            CommonClass.showSnackbarMessage(rL_rootElement, resources.getString(R.string.please_select_product_cate))
        } else if (tV_condition!!.text.toString().isEmpty()) {
            CommonClass.showSnackbarMessage(rL_rootElement, resources.getString(R.string.please_select_condition))
        } else if (tV_currency!!.text.toString().isEmpty()) {
            CommonClass.showSnackbarMessage(rL_rootElement, resources.getString(R.string.please_select_currency))
        } else if (eT_price!!.text.toString().isEmpty()) {
            CommonClass.showSnackbarMessage(rL_rootElement, resources.getString(R.string.please_enter_price))
        } else if (eT_price!!.text.toString().toFloat() < 5) {
            CommonClass.showSnackbarMessage(rL_rootElement, resources.getString(R.string.please_enter_price_greater))
        } else if (tV_current_location!!.text.toString().isEmpty()) {
            CommonClass.showSnackbarMessage(rL_rootElement, resources.getString(R.string.please_enter_location))
        } else if (arrayListImgPath!!.size == 0) {
            CommonClass.showSnackbarMessage(rL_rootElement, resources.getString(R.string.at_least_one_image_is))
        } else {
            println("$TAG post clicked 1")
            if (isToPostItem) {
                //progress_bar_post.setVisibility(View.VISIBLE);
                mDialogBox!!.showProgressDialog(mActivity!!.resources.getString(R.string.posting))
                //tV_post.setVisibility(View.GONE);
                println("$TAG post clicked 2")
                uploadImages(arrayListImgPath, rotationAngles)
                isToPostItem = false
            }
        }
    }

    /*
     *Uploading the image in cloudinary. */
    private fun uploadImages(list: ArrayList<String>?, rotationAnagles: ArrayList<Int>?) {
        // Reduce the Bitmap size
        try {
            var bitmap: Bitmap?
            var outputStream: ByteArrayOutputStream
            for (i in list!!.indices) {
                bitmap = BitmapFactory.decodeFile(list[i])
                outputStream = ByteArrayOutputStream()

                //..for samsung device handle angle
                /* if(Build.MANUFACTURER.equals("samsung")){
                    bitmap = rotate(bitmap,90);
                }else {
                    bitmap = rotate(bitmap, rotationAnagles.get(i));
                }*/bitmap = rotate(bitmap, rotationAnagles!![i])
                println(TAG + " " + "rotate angle==" + rotationAnagles[i])
                bitmap?.compress(Bitmap.CompressFormat.JPEG, 40, outputStream)
                try {
                    val file_out = FileOutputStream(File(list[i]))
                    file_out.write(outputStream.toByteArray())
                    file_out.close()
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
            println("$TAG post clicked 4")
            // Upload Images to cloudinary
            FileUploader.getFileUploader(applicationContext)!!.UploadMultiple(list, object : UploadedCallback {
                override fun onSuccess(data_list: List<ProductImageDatas>?, failed_list: List<ProductImageDatas>?) {
                    println("$TAG post clicked 5")
                    for (productImageDatas in data_list!!) {
                        println(TAG + " " + "main url=" + productImageDatas.mainUrl + " " + "thumb nail url=" + productImageDatas.thumbnailUrl + " " + "width=" + productImageDatas.width + " " + "height=" + productImageDatas.height + " " + "message=" + productImageDatas.message)
                    }
                    if (data_list.size > 0) {
                        //sort by image index to maintain the order of image capture sequence
                        Collections.sort(data_list) { s1, s2 -> // notice the cast to (Integer) to invoke compareTo
                            s1.index.compareTo(s2.index)
                        }
                        postProductApi(data_list)
                    }
                }

                override fun onError(error: String?) {
                    if (mDialogBox!!.progressBarDialog != null) mDialogBox!!.progressBarDialog?.dismiss()
                    Log.d("fdsf324", " $error")
                }
            })
        } catch (e: OutOfMemoryError) {
            if (mDialogBox!!.progressBarDialog != null) mDialogBox!!.progressBarDialog?.dismiss()
            CommonClass.showSnackbarMessage(rL_rootElement, getString(R.string.failedToUpload))
        }
    }
    /* @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString("title");
        super.onSaveInstanceState(outState);
    }*/
    /**
     * <h>PostProductApi</h>
     *
     *
     * In this method we used to do api call to post the product to our product.
     *
     *
     * @param data_list The list containing the image description like main image, thumbnail image, width and height
     */
    private fun postProductApi(data_list: List<ProductImageDatas>) {
        //token, type(0 : photo, 1 : video), mainUrl, thumbnailUrl, containerHeight, containerWidth, title,description, hashTags, location, latitude, longitude, userCoordinates, category, subCategory, price, 
        // currency, productName, thumbnailUrl1, imageUrl1, containerHeight1, containerWidth1, imageUrl2, thumbnailUrl2, containerHeight2, containerWidth2, imageUrl3, thumbnailUrl3, containerHeight3,
        // containerWidth3, imageUrl4, thumbnailUrl4, containerHeight4, containerWidth4, tagProduct, tagProductCoordinates, negotiable, condition

        /*{
    category = Other;
    city = Bengaluru;
    cloudinaryPublicId = t1rtjzy9dasxgq0cw0de;
    cloudinaryPublicId1 = joswipn5eryjehlvygcl;
    cloudinaryPublicId2 = wnes7xbnepd6ym4ljjxo;
    cloudinaryPublicId3 = aeutu4hs7m4ej9dmqx7y;
    cloudinaryPublicId4 = fzfwp6q1bsemtif2ywmy;
    condition = "New(Never Used)";
    containerHeight = 388;
    containerHeight1 = 326;
    containerHeight2 = 400;
    containerHeight3 = 384;
    containerHeight4 = 1136;
    containerWidth = 379;
    containerWidth1 = 451;
    containerWidth2 = 368;
    containerWidth3 = 384;
    containerWidth4 = 640;
    countrySname = IN;
    currency = INR;
    description = Testing;
    imageCount = 5;
    imageUrl1 = "https://res.cloudinary.com/dxaxmyifi/image/upload/fl_progressive:steep/v1509468002/joswipn5eryjehlvygcl.jpg";
    imageUrl2 = "https://res.cloudinary.com/dxaxmyifi/image/upload/fl_progressive:steep/v1509468005/wnes7xbnepd6ym4ljjxo.jpg";
    imageUrl3 = "https://res.cloudinary.com/dxaxmyifi/image/upload/fl_progressive:steep/v1509468008/aeutu4hs7m4ej9dmqx7y.jpg";
    imageUrl4 = "https://res.cloudinary.com/dxaxmyifi/image/upload/fl_progressive:steep/v1509468011/fzfwp6q1bsemtif2ywmy.jpg";
    latitude = "13.028609";
    location = "10th Cross Road, RT Nagar, Bengaluru, Karnataka 560024, India";
    longitude = "77.589495";
    mainUrl = "https://res.cloudinary.com/dxaxmyifi/image/upload/fl_progressive:steep/v1509468002/t1rtjzy9dasxgq0cw0de.jpg";
    negotiable = 1;
    postId = "";
    price = 5400;
    productName = Hello;
    subCategory = "";
    tagProduct = "";
    tagProductCoordinates = "";
    thumbnailImageUrl = "https://res.cloudinary.com/dxaxmyifi/image/upload/w_150,h_150/v1509468002/t1rtjzy9dasxgq0cw0de.jpg";
    thumbnailUrl1 = "";
    thumbnailUrl2 = "";
    thumbnailUrl3 = "";
    thumbnailUrl4 = "";
    token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MjAxMiwibmFtZSI6Im1haWwiLCJhY2Nlc3NLZXkiOiI5MDc5IiwiaWF0IjoxNTA5NDYzOTg3LCJleHAiOjE1MTQ2NDc5ODd9.s_gkTcXZ7496Aw0Dr1VrfedyTNFSmVCyy2c6-tL1nA0";
    type = 0;
}*/
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            val requestDatas = JSONObject()
            try {
                // token
                requestDatas.put("token", mSessionManager!!.authToken)

                // type 0 for image & 1 for video
                requestDatas.put("type", "0")

                // main url
                requestDatas.put("mainUrl", data_list[0].mainUrl)
                requestDatas.put("thumbnailUrl", data_list[0].thumbnailUrl)
                requestDatas.put("thumbnailImageUrl", data_list[0].thumbnailUrl)
                requestDatas.put("containerHeight", data_list[0].height)
                requestDatas.put("containerWidth", data_list[0].width)
                requestDatas.put("cloudinaryPublicId", data_list[0].public_id)

                // title
                requestDatas.put("title", eT_title!!.text.toString())

                //productName
                requestDatas.put("productName", eT_title!!.text.toString())

                // description
                requestDatas.put("description", eT_description!!.text.toString().trim { it <= ' ' })

                // hash tag
                requestDatas.put("hashTags", "")

                // location
                requestDatas.put("city", city)
                requestDatas.put("countrySname", countryCode)
                requestDatas.put("location", tV_current_location!!.text.toString())
                requestDatas.put("latitude", lat)
                requestDatas.put("longitude", lng)

                // user co-ordinate
                requestDatas.put("userCoordinates", "")

                //category
                requestDatas.put("category", tV_category!!.text.toString())

                // sub category
                requestDatas.put("subCategory", subCategory)

                //filter data
                requestDatas.put("filter", createFilterObject(filterKeyValues).toString())

                // price
                requestDatas.put("price", eT_price!!.text.toString())

                // currency
                requestDatas.put("currency", tV_currency!!.text.toString())

                // total number of images
                requestDatas.put("imageCount", data_list.size)

                // if swap post
                if (iWantArrayList.size > 0) {
                    isSwap = 1
                    requestDatas.put("isSwap", isSwap)
                    requestDatas.put("swapingPost", createSwappingArray(iWantArrayList).toString())
                } else {
                    isSwap = 0
                    requestDatas.put("swapingPost", "")
                    requestDatas.put("isSwap", isSwap)
                }


                // Second Image
                if (data_list.size > 1) {
                    requestDatas.put("thumbnailUrl1", data_list[1].thumbnailUrl)
                    requestDatas.put("imageUrl1", data_list[1].mainUrl)
                    requestDatas.put("containerHeight1", data_list[1].height)
                    requestDatas.put("containerWidth1", data_list[1].width)
                    requestDatas.put("cloudinaryPublicId1", data_list[1].public_id)
                } else {
                    requestDatas.put("thumbnailUrl1", "")
                    requestDatas.put("imageUrl1", "")
                    requestDatas.put("containerHeight1", "")
                    requestDatas.put("containerWidth1", "")
                    requestDatas.put("cloudinarydPublicId1", "")
                }

                // Third Image
                if (data_list.size > 2) {
                    requestDatas.put("imageUrl2", data_list[2].mainUrl)
                    requestDatas.put("thumbnailUrl2", data_list[2].thumbnailUrl)
                    requestDatas.put("containerHeight2", data_list[2].height)
                    requestDatas.put("containerWidth2", data_list[2].width)
                    requestDatas.put("cloudinaryPublicId2", data_list[2].public_id)
                } else {
                    requestDatas.put("imageUrl2", "")
                    requestDatas.put("thumbnailUrl2", "")
                    requestDatas.put("containerHeight2", "")
                    requestDatas.put("containerWidth2", "")
                    requestDatas.put("cloudinarydPublicId2", "")
                }

                // Fourth Image
                if (data_list.size > 3) {
                    requestDatas.put("imageUrl3", data_list[3].mainUrl)
                    requestDatas.put("thumbnailUrl3", data_list[3].thumbnailUrl)
                    requestDatas.put("containerHeight3", data_list[3].height)
                    requestDatas.put("containerWidth3", data_list[3].width)
                    requestDatas.put("cloudinaryPublicId3", data_list[3].public_id)
                } else {
                    requestDatas.put("imageUrl3", "")
                    requestDatas.put("thumbnailUrl3", "")
                    requestDatas.put("containerHeight3", "")
                    requestDatas.put("containerWidth3", "")
                    requestDatas.put("cloudinarydPublicId3", "")
                }

                // Fifth Image
                if (data_list.size > 4) {
                    requestDatas.put("imageUrl4", data_list[4].mainUrl)
                    requestDatas.put("thumbnailUrl4", data_list[4].thumbnailUrl)
                    requestDatas.put("containerHeight4", data_list[4].height)
                    requestDatas.put("containerWidth4", data_list[4].width)
                    requestDatas.put("cloudinaryPublicId4", data_list[4].public_id)
                } else {
                    requestDatas.put("imageUrl4", "")
                    requestDatas.put("thumbnailUrl4", "")
                    requestDatas.put("containerHeight4", "")
                    requestDatas.put("containerWidth4", "")
                    requestDatas.put("cloudinarydPublicId4", "")
                }

                // tag product
                requestDatas.put("tagProduct", "")

                // tag product co-ordinate
                requestDatas.put("tagProductCoordinates", "")

                // negotiable
                requestDatas.put("negotiable", negotiable)

                // condition
                requestDatas.put("condition", tV_condition!!.text.toString())
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            OkHttp3Connection.doOkHttp3Connection(TAG, ApiUrl.PRODUCT, OkHttp3Connection.Request_type.POST, requestDatas, object : OkHttp3RequestCallback {
                 override fun onSuccess(result: String?, user_tag: String?) {
                    //progress_bar_post.setVisibility(View.GONE);
                    if (mDialogBox!!.progressBarDialog != null) mDialogBox!!.progressBarDialog?.dismiss()
                    //tV_post.setVisibility(View.VISIBLE);
                    isToPostItem = true
                    val gson = Gson()
                    val postProductMainPojo = gson.fromJson(result, PostProductMainPojo::class.java)
                    when (postProductMainPojo.code) {
                        "200" -> {
                            val postProductDatas = postProductMainPojo.data?.get(0)
                            if (postProductDatas != null) {
                                val postId = postProductDatas.postId
                                val productName = postProductDatas.productName
                                if (isTwitterSharingOn) {

                                    //share_On_Twitter(postId, productName);
                                    val deepLink = resources.getString(R.string.share_item_base_url) + postId
                                    FirebaseDynamicLinks.getInstance().createDynamicLink()
                                            .setLink(Uri.parse(deepLink))
                                            .setDynamicLinkDomain(mActivity!!.resources.getString(R.string.deep_link))
                                            .setSocialMetaTagParameters(
                                                    SocialMetaTagParameters.Builder()
                                                            .setTitle(productName)
                                                            .setDescription(postProductDatas.description)
                                                            .setImageUrl(Uri.parse(postProductDatas.mainUrl))
                                                            .build())
                                            .setAndroidParameters(AndroidParameters.Builder().build())
                                            .buildShortDynamicLink()
                                            .addOnCompleteListener(mActivity!!) { task ->
                                                if (task.isSuccessful) {
                                                    val shortLink = task.result!!.shortLink
                                                    sharingOnTwitterIntent(shortLink)
                                                } else {
                                                    Toast.makeText(mActivity, "Sharing failed", Toast.LENGTH_SHORT).show()
                                                }
                                            }
                                }
                                if (isFacebookSharingOn) {
                                    val deepLink = resources.getString(R.string.share_item_base_url) + postId
                                    val shortLinkTask = FirebaseDynamicLinks.getInstance().createDynamicLink()
                                            .setLink(Uri.parse(deepLink))
                                            .setDynamicLinkDomain(mActivity!!.resources.getString(R.string.deep_link))
                                            .setSocialMetaTagParameters(
                                                    SocialMetaTagParameters.Builder()
                                                            .setTitle(productName)
                                                            .setDescription(postProductDatas.description)
                                                            .setImageUrl(Uri.parse(postProductDatas.mainUrl))
                                                            .build())
                                            .setAndroidParameters(AndroidParameters.Builder().build())
                                            .buildShortDynamicLink()
                                            .addOnCompleteListener(mActivity!!) { task ->
                                                if (task.isSuccessful) {
                                                    // Short link created
                                                    val shortLink = task.result!!.shortLink
                                                    val shareDialog: ShareDialog
                                                    FacebookSdk.sdkInitialize(mActivity)
                                                    shareDialog = ShareDialog(mActivity)
                                                    val linkContent = ShareLinkContent.Builder()
                                                            .setContentUrl(shortLink)
                                                            .setImageUrl(Uri.parse(postProductDatas.mainUrl))
                                                            .setContentTitle(productName)
                                                            .setContentDescription(postProductDatas.description).build()
                                                    shareDialog.show(linkContent)
                                                    //shareOnFacebook(shortLink,post_url, postProductDatas.getMainUrl(), productName, postProductDatas.getDescription());
                                                } else {
                                                    Toast.makeText(mActivity, "Sharing failed, Try again", Toast.LENGTH_SHORT).show()
                                                }
                                            }
                                }
                                if (isInstagramSharingOn) {
                                    shareOnInsta(arrayListImgPath!![0], productName)
                                }
                            }
                            DialogBox(mActivity!!).postedSuccessDialog()
                        }
                        "401" -> CommonClass.sessionExpired(mActivity!!)
                        else -> CommonClass.showSnackbarMessage(rL_rootElement, postProductMainPojo.message)
                    }
                }

                 override fun onError(error: String?, user_tag: String?) {
                    isToPostItem = true
                    //progress_bar_post.setVisibility(View.GONE);
                    if (mDialogBox!!.progressBarDialog != null) mDialogBox!!.progressBarDialog?.dismiss()
                    //tV_post.setVisibility(View.VISIBLE);
                    CommonClass.showSnackbarMessage(rL_rootElement, error)
                }
            })
        } else CommonClass.showSnackbarMessage(rL_rootElement, resources.getString(R.string.NoInternetAccess))
    }

    private fun createSwappingArray(list: ArrayList<SwapPost>): JSONArray {
        val jsonArray = JSONArray()
        var obj: JSONObject? = null
        for (i in list.indices) {
            val iwantItemPojo = list[i]
            if (iwantItemPojo.itemType) {
                list.remove(iwantItemPojo)
                continue
            }
            try {
                obj = JSONObject()
                obj.put("swapDescription", "")
                obj.put("swapPostId", iwantItemPojo.swapPostId)
                obj.put("swapTitle", iwantItemPojo.swapTitle)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            jsonArray.put(obj)
        }
        return jsonArray
    }

    private fun shareOnFacebook(dynamicLinkUri: Uri, link: String, thumbnail: String, name: String, description: String) {
        facebook_share_mamager!!.shareImage_Link(link, thumbnail, "NAME:$name\nDESCRIPTION:$description\n$dynamicLinkUri", object : Share_callback {
            override fun onSucess_share() {
                Toast.makeText(mActivity, "facebook successfully shared", Toast.LENGTH_SHORT).show()
                println("$TAG facebook successfully shared")
            }

            override fun onError(error: String?) {
                println("$TAG facebook successfully failed")
                println("$TAG error$error")
                Toast.makeText(mActivity, "facebook successfully failed", Toast.LENGTH_SHORT).show()
            }
        })
        /* facebook_share_mamager.shareLinkOnFacebook(link, thumbnail, name, description, new Facebook_share_mamager.Share_callback() {
            @Override
            public void onSucess_share() {
                System.out.println(TAG+" "+"facebook successfully shared");
            }

            @Override
            public void onError(String error) {
                System.out.println(TAG+" "+"facebook successfully failed");
            }
        });*/
    }

    /**
     * <h2>share_On_Twitter</h2>
     *
     *
     * Sharing the data in twitter by crating the
     * link.
     *
     */
    fun share_On_Twitter(postId: String, caption: String?) {
        var caption = caption
        caption = if (caption == null) {
            ""
        } else {
            "@$caption"
        }
        val post_url = resources.getString(R.string.share_item_base_url) + postId
        Log.d("daste", "" + post_url)
        TweetManger.instance?.updateStatus(caption, post_url, object : TweetSuccess {
            override fun onSuccess() {
                println("$TAG twitter successfully shared")
                //Toast.makeText(Home_container.this, R.string.share_text, Toast.LENGTH_SHORT).show();
            }

            override fun onFailed() {
                println("$TAG twitter successfully failed")
            }
        })
    }

    fun sharingOnTwitterIntent(link: Uri) {
        val tweetIntent = Intent(Intent.ACTION_SEND)
        tweetIntent.putExtra(Intent.EXTRA_TEXT, link.toString())
        tweetIntent.type = "text/plain"
        val packManager = packageManager
        val resolvedInfoList = packManager.queryIntentActivities(tweetIntent, PackageManager.MATCH_DEFAULT_ONLY)
        var resolved = false
        for (resolveInfo in resolvedInfoList) {
            if (resolveInfo.activityInfo.packageName.startsWith("com.twitter.android")) {
                tweetIntent.setClassName(
                        resolveInfo.activityInfo.packageName,
                        resolveInfo.activityInfo.name)
                resolved = true
                break
            }
        }
        if (resolved) {
            startActivity(tweetIntent)
        }
    }

    /*sharing on twitter via intent*/ /*
    Sharign the data in instagram. */
    private fun shareOnInsta(mediaPath: String, capsion: String) {
        Log.d("insta image path", mediaPath)
        val type = "image/*"
        // Create the new Intent using the 'Send' action.
        val share = Intent(Intent.ACTION_SEND)

        // Set the MIME type
        share.type = type
        try {
            /* // Create the URI from the media
            File media = new File(mediaPath);
            Uri uri;
            if ((android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)) {
                uri = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID + ".provider", media);
            } else {
                uri = Uri.fromFile(media);
            }

            // Add the URI to the Intent.
            share.putExtra(Intent.EXTRA_STREAM, uri);
            share.putExtra(Intent.EXTRA_TITLE, capsion);
            share.setPackage("com.instagram.android");

            // Broadcast the Intent.
            startActivity(Intent.createChooser(share, "Share to"));*/
            val media = File(mediaPath)
            val uri: Uri
            uri = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID + ".provider", media)
            } else {
                Uri.fromFile(media)
            }
            val shareIntent = Intent()
            shareIntent.action = Intent.ACTION_SEND
            shareIntent.setPackage("com.instagram.android")
            try {
                shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(MediaStore.Images.Media.insertImage(contentResolver, mediaPath, capsion, "")))
            } catch (e: FileNotFoundException) {
                // TODO Auto-generated catch block
                e.printStackTrace()
            }
            shareIntent.putExtra(Intent.EXTRA_TITLE, capsion)
            shareIntent.type = "image/*"
            startActivity(shareIntent)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onBackPressed() {
        val intent = Intent()
        intent.putExtra("arrayListImgPath", mCapturedImageData)
        setResult(VariableConstants.CAMERA_IMAGE_LIST_REQ, intent)
        finish()
        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate)
    }

    /**
     * <h>deleteAllCapturedImages</h>
     *
     *
     * In this method we used to delete all the captured images which is taken by the camera.
     *
     *
     * @param mList The list containing the images
     */
    fun deleteAllCapturedImages(mList: ArrayList<String?>) {
        try {
            var f: File
            var deleted = false
            for (i in mList.indices) {
                f = File(mList[i])
                if (f.exists()) {
                    f.delete()
                    deleted = true
                }
            }
            if (deleted) {
                MediaScannerConnection.scanFile(this, arrayOf(Environment.getExternalStorageDirectory().toString()), null) { path, uri -> }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onClick(v: View) {
        val intent: Intent
        when (v.id) {
            R.id.rL_back_btn -> if (isToPostItem) onBackPressed()
            R.id.rL_product_category -> if (isToPostItem) {
                intent = Intent(mActivity, ProductCategoryActivity::class.java)
                intent.putExtra("selectedCategory", tV_category!!.text.toString())
                intent.putExtra("selectedSubCategory", subCategory)
                intent.putExtra("selectedField", filterKeyValues)
                startActivityForResult(intent, VariableConstants.CATEGORY_REQUEST_CODE)
            }
            R.id.rL_conditions -> if (isToPostItem) {
                intent = Intent(mActivity, PostConditionsActivity::class.java)
                startActivityForResult(intent, VariableConstants.CONDITION_REQUEST_CODE)
            }
//            R.id.rL_currency -> if (isToPostItem) {
//                intent = Intent(mActivity, CurrencyListActivity::class.java)
//                startActivityForResult(intent, VariableConstants.CURRENCY_REQUEST_CODE)
//            }
            R.id.tV_change_loc -> if (isToPostItem) {
                intent = Intent(mActivity, ChangeLocationActivity::class.java)
                startActivityForResult(intent, VariableConstants.CHANGE_LOC_REQ_CODE)
            }
            R.id.rL_postProduct -> checkPostProduct()
            R.id.btn_cancel -> {
                val intent1 = Intent(mActivity, HomePageActivity::class.java)
                intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                mActivity!!.startActivity(intent1)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        // for facebook
        if (callbackManager!!.onActivityResult(requestCode, resultCode, data)) {
        } else if (requestCode == VariableConstants.TWEETER_REQUEST_CODE) {
            // for twitter
            if (resultCode == Activity.RESULT_OK) {
                if (TweetManger.instance!!.isUserLoggedIn) {
                    isTwitterSharingOn = true
                }
            }
        } else if (data != null) {
            try {
            when (requestCode) {

                    VariableConstants.CATEGORY_REQUEST_CODE -> {
                        val categoryName = data.getStringExtra("categoryName")
                        if(data.hasExtra("subCategoryName")) {
                            subCategory = data.getStringExtra("subCategoryName")
                        }
                        filterKeyValues = data.getSerializableExtra("filterKeyValues") as ArrayList<FilterKeyValue>
                        println("$TAG category data: $categoryName")
                        println("$TAG category data: $subCategory")
                        for (f in filterKeyValues!!) {
                            println(TAG + " category data: " + f.key + "," + f.value)
                        }
                        if (categoryName != null) {
                            tV_category!!.text = categoryName
                            eT_description!!.clearFocus()
                        }
                    }
                    VariableConstants.CONDITION_REQUEST_CODE -> {
                        val condition = data.getStringExtra("condition")
                        if (condition != null) {
                            tV_condition!!.text = condition
                            eT_price!!.requestFocus()
                        }
                    }
                    VariableConstants.CURRENCY_REQUEST_CODE -> {
                        val cuurency_code = data.getStringExtra("cuurency_code")
                        val currency_symbol = data.getStringExtra("currency_symbol")

                        // set currency cde eg. Inr
                        if (cuurency_code != null) tV_currency!!.text = cuurency_code

                        // set currency symbol eg. $
                        if (currency_symbol != null) tV_currency_symbol!!.text = currency_symbol
                    }
                    VariableConstants.CHANGE_LOC_REQ_CODE -> {
                        var placeName = data.getStringExtra("address")
                        lat = data.getStringExtra("lat")
                        lng = data.getStringExtra("lng")
                        println("$TAG place name=$placeName lat=$lat lng=$lng")

                        //..update city and countrycode on location changed.
                        city = CommonClass.getCityName(mActivity!!, lat.toDouble(), lng.toDouble())
                        countryCode = CommonClass.getCountryCode(mActivity!!, lat.toDouble(), lng.toDouble())
                        val countryName = CommonClass.getCountryName(mActivity!!, lat.toDouble(), lng.toDouble())
                        placeName = if (city == null || city!!.isEmpty()) CommonClass.getFirstCaps(countryName) else if (countryName == null || countryName.isEmpty()) CommonClass.getFirstCaps(city) else CommonClass.getFirstCaps(city) + ", " + CommonClass.getFirstCaps(countryName)
                        if (placeName != null && !placeName.isEmpty()) tV_current_location!!.text = placeName else {
                            val url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=+" + lat + "+,+" + lng + "+&location_type=ROOFTOP&result_type=street_address&key=" + resources.getString(R.string.google_maps_key)
                            // call google map api
                            RequestTask().execute(url)
                        }
                    }
                    VariableConstants.Willing_TO_EXCHANGE -> {
                        iWantArrayList.clear()
                        iWantArrayList.addAll((data.getSerializableExtra("iWantArrayList") as ArrayList<SwapPost>))
                        if (iWantArrayList.size > 0) {
                            val iwantItemPojo = SwapPost()
                            iwantItemPojo.itemType = true
                            iWantArrayList.add(iwantItemPojo)
                            iwantAdapter!!.notifyDataSetChanged()
                        }
                    }
                    VariableConstants.CAMERA_IMAGE_LIST_REQ -> {
                        //Intent intent=getIntent();
                        arrayListImgPath = data.getStringArrayListExtra("arrayListImgPath")
                        rotationAngles = data.getIntegerArrayListExtra("rotationAngles")
                        var image: CapturedImage
                        mCapturedImageData.clear()
                        var i = 0
                        while (i < rotationAngles!!.size) {
                            image = CapturedImage()
                            image.imagePath = arrayListImgPath?.get(i)
                            image.rotateAngle = rotationAngles!!.get(i)
                            mCapturedImageData.add(image)
                            i++
                        }
                        if (arrayListImgPath != null && arrayListImgPath!!.size > 0) {
                            when (arrayListImgPath!!.size) {
                                1 -> tV_add_more_image!!.text = resources.getString(R.string.add_upto_4_more_img)
                                2 -> tV_add_more_image!!.text = resources.getString(R.string.add_upto_3_more_img)
                                3 -> tV_add_more_image!!.text = resources.getString(R.string.add_upto_2_more_img)
                                4 -> tV_add_more_image!!.text = resources.getString(R.string.add_upto_1_more_img)
                                5 -> tV_add_more_image!!.visibility = View.GONE
                            }
                        }
                        imagesHorizontalRvAdap!!.notifyDataSetChanged()
                    }

            } }catch(e:Exception){e.printStackTrace()}
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            VariableConstants.PERMISSION_REQUEST_CODE -> {
                println("grant result=" + grantResults.size)
                if (grantResults.size > 0) {
                    var count = 0
                    while (count < grantResults.size) {
                        if (grantResults[count] != PackageManager.PERMISSION_GRANTED) runTimePermission!!.allowPermissionAlert(permissions[count])
                        count++
                    }
                    println("isAllPermissionGranted=" + runTimePermission!!.checkPermissions(permissionsArray!!))
                    if (runTimePermission!!.checkPermissions(permissionsArray!!)) {
                        currentLocation
                    }
                }
            }
        }
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(View(this).windowToken, 0)
        return true
    }

    fun rotate(bitmap: Bitmap?, degree: Int): Bitmap {
        val w = bitmap!!.width
        val h = bitmap.height
        val mtx = Matrix()
        mtx.postRotate(degree.toFloat())
        return Bitmap.createBitmap(bitmap, 0, 0, w, h, mtx, true)
    }

    // create filter data json object, which is need to send.
    fun createFilterObject(list: ArrayList<FilterKeyValue>?): JSONObject {
        val jsonObject = JSONObject()
        try {
            for (filterKeyValue in list!!) {
                if (filterKeyValue.type != null && filterKeyValue.type == "1") {
                    val i = filterKeyValue.value.toInt()
                    jsonObject.put(filterKeyValue.key, i)
                } else {
                    jsonObject.put(filterKeyValue.key, filterKeyValue.value)
                }
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        return jsonObject
    }

    fun iWantAdapterClick(postId: String, position: Int) {
        arrayListPostIds.remove(postId)
        iWantArrayList.removeAt(position)
        iwantAdapter!!.notifyDataSetChanged()
    }

    fun addMoreWillingSwapItems() {
        startWillingtoExchageActivity()
    }

    private fun startWillingtoExchageActivity() {
        val intent = Intent(this, WillingToExchangeActivity::class.java)
        intent.putExtra("iWantArrayList", iWantArrayList)
        startActivityForResult(intent, VariableConstants.Willing_TO_EXCHANGE)
    }

    override fun onRestart() {
        super.onRestart()
        println("$TAG onRestart")
        /*if (iWantArrayList.size()==0){
            System.out.println(TAG + " " + "inside the onRestart");
            SwapPost iwantItemPojo = new SwapPost();
            iwantItemPojo.setItemType(true);
            iWantArrayList.add(iwantItemPojo);
            iwantAdapter.notifyDataSetChanged();
        }*/
    }

    fun removedNewAddedPhotos(path: String) {
        for (i in arrayListImgPath!!.indices) {
            if (arrayListImgPath!![i] == path) {
                arrayListImgPath!!.removeAt(i)
            }
        }
    }

    private inner class RequestTask : AsyncTask<String?, String?, String?>() {
         override fun doInBackground(vararg uri: String?): String? {
            val httpclient: HttpClient = DefaultHttpClient()
            val response: HttpResponse
            var responseString: String? = null
            try {
                response = httpclient.execute(HttpGet(uri[0]))
                val statusLine = response.statusLine
                if (statusLine.statusCode == HttpStatus.SC_OK) {
                    val out = ByteArrayOutputStream()
                    response.entity.writeTo(out)
                    responseString = out.toString()
                    out.close()
                } else {
                    //Closes the connection.
                    response.entity.content.close()
                    throw IOException(statusLine.reasonPhrase)
                }
            } catch (e: ClientProtocolException) {
                //TODO Handle problems..
            } catch (e: IOException) {
                e.printStackTrace()
                //TODO Handle problems..
            }
            return responseString
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            //Do anything with response..
            progress_bar_location!!.visibility = View.GONE
            tV_current_location!!.visibility = View.VISIBLE
            println("$TAG address res=$result")
            if (result != null) {
                try {
                    val reader = JSONObject(result)
                    val sys = reader.getJSONArray("results")
                    val formatted_address = sys.getJSONObject(0).getString("formatted_address")
                    println("$TAG formatted_address=$formatted_address")
                    if (formatted_address != null && !formatted_address.isEmpty()) tV_current_location!!.text = formatted_address
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }
        }
    }

    companion object {
        private val TAG = PostProductActivity::class.java.simpleName
    }
}