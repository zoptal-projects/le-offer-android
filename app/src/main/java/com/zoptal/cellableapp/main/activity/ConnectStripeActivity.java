//package com.zoptal.cellableapp.main.activity;
//
//import android.app.Activity;
//import android.content.Intent;
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import androidx.appcompat.app.AppCompatActivity;
//import android.view.View;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//
//import com.zoptal.cellableapp.R;
//
///**
// * <h>ConnectPaypalActivity</h>
// *
// * @since 20-Jul-17
// */
//public class ConnectStripeActivity extends AppCompatActivity implements View.OnClickListener {
//    private static final String TAG = ConnectStripeActivity.class.getSimpleName();
//    TextView tv_getStripe, tvAddAddress,tvCreateToken;
//
//    @Override
//    protected void onCreate(@Nullable Bundle savedInstanceState) {
//         super.onCreate(savedInstanceState?: Bundle());
//        setContentView(R.layout.activity_connect_stripe);
//        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
//        initVariables();
//    }
//
//    /**
//     * <h>InitVariables</h>
//     * <p>
//     * In this method we used to initialize all variables.
//     * </p>
//     */
//    private void initVariables() {
//        // back button
//        RelativeLayout rL_back_btn = (RelativeLayout) findViewById(R.id.rL_back_btn);
//        rL_back_btn.setOnClickListener(this);
//
//        // get paypal
//        tv_getStripe = (TextView) findViewById(R.id.tv_getStripe);
//        tv_getStripe.setOnClickListener(this);
//        tvAddAddress = (TextView) findViewById(R.id.tvAddAddress);
//        tvAddAddress.setOnClickListener(this);
//        tvCreateToken = (TextView) findViewById(R.id.tvCreateToken);
//        tvCreateToken.setOnClickListener(this);
//
//    }
//
//
//    @Override
//    public void onClick(View v) {
//        switch (v.getId()) {
//            // back button
//            case R.id.rL_back_btn:
//                onBackPressed();
//                break;
//
//            // get paypal
//            case R.id.tv_getStripe:
//                if (tv_getStripe.getText().toString().trim().equals(getResources().getString(R.string.connect_stripe))) {
//                    Intent intent = new Intent(this, PaymentAddAccount.class);
//                    startActivityForResult(intent, 0);
//                } else {
////            showDialog();
////            authViewModel.disconnectStripeAccount()
//                }
//                break;
//            case R.id.tvAddAddress:
//                Intent intent = new Intent(this, AddAddressActivity.class);
//
//                startActivity(intent);
//                break;
//            case R.id.tvCreateToken:
//                Intent tokenIntent = new Intent(this,
//                .class);
//                startActivity(tokenIntent);
//                break;
//
//        }
//    }
//
//
//    @Override
//    public void onBackPressed() {
//        finish();
//        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
//    }
//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//
//        if (requestCode == 0 && resultCode == RESULT_OK) {
////            showDialog();
////            authViewModel.fetchBeauticationDetail(fetchBeauticationForm())
//
//        }
//    }
//}
