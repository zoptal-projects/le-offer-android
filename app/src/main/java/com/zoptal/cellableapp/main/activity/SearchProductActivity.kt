package com.zoptal.cellableapp.main.activity

import android.app.Activity
import android.content.IntentFilter
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.RelativeLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.viewpager.widget.ViewPager
import androidx.viewpager.widget.ViewPager.OnPageChangeListener
import com.google.android.material.tabs.TabLayout
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.adapter.ViewPagerAdapter
import com.zoptal.cellableapp.fcm_push_notification.Config
import com.zoptal.cellableapp.fcm_push_notification.NotificationMessageDialog
import com.zoptal.cellableapp.fcm_push_notification.NotificationUtils.Companion.clearNotifications
import com.zoptal.cellableapp.main.activity.SearchProductActivity
import com.zoptal.cellableapp.main.view_pager.search_product.PeoplesFrag
import com.zoptal.cellableapp.main.view_pager.search_product.PostsFrag
import com.zoptal.cellableapp.utility.CommonClass

/**
 * <h>SearchProductActivity</h>
 *
 *
 * This class is called from HomePage Frag class. In this class we used to set two
 * tab first one is Posts Tab to search a product from the given post from data base
 * and second tab is to search the registered user.
 *
 * @since 18-May-17
 */
class SearchProductActivity : AppCompatActivity(), View.OnClickListener {
    var eT_search_users: EditText? = null
    var postText = ""
    var peopleText = ""
    private var mNotificationMessageDialog: NotificationMessageDialog? = null
    override fun onCreate(savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_search_product)
        overridePendingTransition(R.anim.slide_up, R.anim.stay)
        initVariables()
    }

    /**
     * <h>initVariables</h>
     *
     *
     * This method is being called from onCreate() of the same class. In this
     * method we used to initliaze all variables.
     *
     */
    private fun initVariables() {
        val mActivity: Activity = this@SearchProductActivity
        mNotificationMessageDialog = NotificationMessageDialog(mActivity)
        CommonClass.statusBarColor(mActivity)
        val rL_close = findViewById<View>(R.id.rL_close) as RelativeLayout
        rL_close.setOnClickListener(this)
        eT_search_users = findViewById<View>(R.id.eT_search_users) as EditText
        eT_search_users!!.hint = resources.getString(R.string.search_post)
        val viewpager = findViewById<View>(R.id.viewpager) as ViewPager
        val tabs = findViewById<View>(R.id.tabs) as TabLayout
        setupViewPager(viewpager)
        tabs.setupWithViewPager(viewpager)
        viewpager.addOnPageChangeListener(object : OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
            override fun onPageSelected(position: Int) {
                println("$TAG position=$position")
                if (position == 0) {
                    eT_search_users!!.hint = resources.getString(R.string.search_post)
                    eT_search_users!!.setText(postText)
                } else {
                    eT_search_users!!.hint = resources.getString(R.string.search_people)
                    eT_search_users!!.setText(peopleText)
                }
            }

            override fun onPageScrollStateChanged(state: Int) {}
        })
    }

    override fun onResume() {
        super.onResume()

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.REGISTRATION_COMPLETE))

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.PUSH_NOTIFICATION))

        // clear the notification area when the app is opened
        clearNotifications(applicationContext)
    }

    override fun onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!)
        super.onPause()
    }

    private fun setupViewPager(viewPager: ViewPager) {
        val adapter = ViewPagerAdapter(supportFragmentManager)
        adapter.addFragment(PostsFrag(), resources.getString(R.string.posts))
        adapter.addFragment(PeoplesFrag(), resources.getString(R.string.people))
        viewPager.adapter = adapter
    }

    override fun onBackPressed() {
        finish()
        overridePendingTransition(R.anim.stay, R.anim.slide_down)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.rL_close -> onBackPressed()
        }
    }

    companion object {
        private val TAG = SearchProductActivity::class.java.simpleName
    }
}