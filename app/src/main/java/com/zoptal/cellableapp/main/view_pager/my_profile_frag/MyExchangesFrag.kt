package com.zoptal.cellableapp.main.view_pager.my_profile_frag

import android.app.Activity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.annotation.Nullable
import androidx.fragment.app.Fragment

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.gson.Gson
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.adapter.MyExchangesAdapter
import com.zoptal.cellableapp.event_bus.BusProvider.instance
import com.zoptal.cellableapp.main.activity.HomePageActivity
import com.zoptal.cellableapp.pojo_class.profile_myexchanges_pojo.ProfileMyExchangesData
import com.zoptal.cellableapp.pojo_class.profile_myexchanges_pojo.ProfileMyExchangesMainPojo
import com.zoptal.cellableapp.utility.ApiUrl
import com.zoptal.cellableapp.utility.CommonClass
import com.zoptal.cellableapp.utility.OkHttp3Connection
import com.zoptal.cellableapp.utility.OkHttp3Connection.OkHttp3RequestCallback
import com.zoptal.cellableapp.utility.SessionManager
import org.json.JSONObject
import java.util.*

/**
 * Created by ${3embed} on ${27-10-2017}.
 * Banglore
 */
class MyExchangesFrag : Fragment() {
    private var mActivity: Activity? = null
    private var rV_myprofile_exchange: RecyclerView? = null
    private var layoutManager: LinearLayoutManager? = null
    private var myExchangesAdapter: MyExchangesAdapter? = null
    private var progress_bar_profile: ProgressBar? = null
    private var mSessionManager: SessionManager? = null
    private var mSwipeRefreshLayout: SwipeRefreshLayout? = null
    private var pageIndex = 0
    private val profileMyExchangesData = ArrayList<ProfileMyExchangesData>()
    private var visibleItemCount = 0
    private var totalItemCount = 0
    private val visibleThreshold = 5
    private var isLoadingRequired = false
    private val isToCallFirstTime = false
    private var rL_noProductFound: RelativeLayout? = null
    private var rL_start_selling: RelativeLayout? = null
    private var tV_snapNpost: TextView? = null
    private var tV_no_ads: TextView? = null
    private var iV_default_icon: ImageView? = null
    override fun onCreate(@Nullable savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        mActivity = getActivity()
        mSessionManager = SessionManager(mActivity!!)
    }

    @Nullable
    override fun onCreateView(inflater: LayoutInflater, @Nullable container: ViewGroup?, @Nullable savedInstanceState: Bundle?): View {
        val view = inflater.inflate(R.layout.frag_profile_buying, container, false)
        rV_myprofile_exchange = view.findViewById<View>(R.id.rV_myprofile_selling) as RecyclerView
        progress_bar_profile = view.findViewById<View>(R.id.progress_bar_profile) as ProgressBar
        mSwipeRefreshLayout = view.findViewById<View>(R.id.swipeRefreshLayout) as SwipeRefreshLayout
        rL_noProductFound = view.findViewById<View>(R.id.rL_noProductFound) as RelativeLayout
        rL_start_selling = view.findViewById<View>(R.id.rL_start_selling) as RelativeLayout
        rL_start_selling!!.visibility = View.GONE
        tV_snapNpost = view.findViewById<View>(R.id.tV_snapNpost) as TextView
        tV_no_ads = view.findViewById<View>(R.id.tV_no_ads) as TextView
        iV_default_icon = view.findViewById<View>(R.id.iV_default_icon) as ImageView
        tV_snapNpost!!.visibility = View.GONE
        val txtMsg: String = getResources().getString(R.string.no_exchanges_yet).toString() + "\n" + getResources().getString(R.string.you_have_no_exchange_yet)
        tV_no_ads!!.text = txtMsg
        iV_default_icon!!.setImageResource(R.drawable.empty_selling_icon)
        myExchangesAdapter = MyExchangesAdapter(profileMyExchangesData, mActivity!!)
        layoutManager = LinearLayoutManager(mActivity)
        rV_myprofile_exchange?.setLayoutManager(layoutManager)
        rV_myprofile_exchange?.setHasFixedSize(true)
        rV_myprofile_exchange?.setAdapter(myExchangesAdapter)
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            progress_bar_profile!!.visibility = View.VISIBLE
            if (mSessionManager!!.isUserLoggedIn) {
                myExchangesPosts(pageIndex)
            }
        } else {
            CommonClass.showSnackbarMessage((mActivity as HomePageActivity?)!!.rL_rootElement, mActivity!!.resources.getString(R.string.NoInternetAccess))
        }
        mSwipeRefreshLayout?.setColorSchemeResources(R.color.pink_color)
        mSwipeRefreshLayout?.setOnRefreshListener(object : SwipeRefreshLayout.OnRefreshListener {
            override fun onRefresh() {
                pageIndex = 0
                profileMyExchangesData.clear()
                myExchangesAdapter!!.notifyDataSetChanged()
                if (mSessionManager!!.isUserLoggedIn) {
                    myExchangesPosts(pageIndex)
                }
            }
        })
        return view
    }

    private fun myExchangesPosts(offset: Int) {
        var offset = offset
        val limit = 20
        offset = limit * offset
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            println(TAG + " " + "token=" + mSessionManager!!.authToken)
            val url = ApiUrl.MY_EXCHANGES + "?token=" + mSessionManager!!.authToken + "&offset=" + offset + "&limit=" + limit
            OkHttp3Connection.doOkHttp3Connection(TAG, url, OkHttp3Connection.Request_type.GET, JSONObject(), object : OkHttp3RequestCallback {
                 override fun onSuccess(result: String?, user_tag: String?) {
                    progress_bar_profile!!.visibility = View.GONE
                    println("$TAG profile selling res=$result")
                    mSwipeRefreshLayout!!.setRefreshing(false)
                    val profileMyExchangesMainPojo: ProfileMyExchangesMainPojo
                    val gson = Gson()
                    profileMyExchangesMainPojo = gson.fromJson(result, ProfileMyExchangesMainPojo::class.java)
                    when (profileMyExchangesMainPojo.code) {
                        "200" -> if (profileMyExchangesMainPojo.data != null && profileMyExchangesMainPojo.data!!.size > 0) {
                            rL_noProductFound!!.visibility = View.GONE
                            profileMyExchangesData.addAll(profileMyExchangesMainPojo.data!!)
                            //   isLoadingRequired = arrayListSellingDatas.size() > 14;
                            myExchangesAdapter!!.notifyDataSetChanged()

                            // Load more
                            rV_myprofile_exchange?.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                               override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                                    super.onScrolled(recyclerView, dx, dy)

                                    //int[] firstVisibleItemPositions = new int[2];
                                    totalItemCount = layoutManager!!.getItemCount()
                                    visibleItemCount = layoutManager!!.findLastCompletelyVisibleItemPosition()
                                    if (isLoadingRequired && totalItemCount <= visibleItemCount + visibleThreshold) {
                                        isLoadingRequired = false
                                        pageIndex = pageIndex + 1
                                        mSwipeRefreshLayout!!.setRefreshing(true)
                                        myExchangesPosts(pageIndex)
                                    }
                                }
                            })
                        }
                        "204" -> {
                            mSwipeRefreshLayout!!.setRefreshing(false)
                            if (profileMyExchangesData.size == 0) {
                                rL_noProductFound!!.visibility = View.VISIBLE
                            }
                        }
                        "401" -> {
                            mSwipeRefreshLayout!!.setRefreshing(false)
                            CommonClass.sessionExpired(mActivity!!)
                        }
                        else -> {
                            mSwipeRefreshLayout?.setRefreshing(false)
                            CommonClass.showTopSnackBar((mActivity as HomePageActivity?)!!.rL_rootElement, profileMyExchangesMainPojo.message)
                        }
                    }
                }

                 override fun onError(error: String?, user_tag: String?) {
                    mSwipeRefreshLayout!!.setRefreshing(false)
                    progress_bar_profile!!.visibility = View.GONE
                    CommonClass.showSnackbarMessage((mActivity as HomePageActivity?)!!.rL_rootElement, error)
                }
            })
        } else CommonClass.showSnackbarMessage((mActivity as HomePageActivity?)!!.rL_rootElement, getResources().getString(R.string.NoInternetAccess))
    }

    override fun onViewCreated(view: View, @Nullable savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        instance.register(this)
    }

    override fun onDestroy() {
        instance.unregister(this)
        super.onDestroy()
    }

    companion object {
        private val TAG = MyExchangesFrag::class.java.simpleName
        @JvmStatic
        fun newInstance(memberName: String?): MyExchangesFrag {
            val bundle = Bundle()
            bundle.putString("memberName", memberName)
            val myExchangesFrag = MyExchangesFrag()
            myExchangesFrag.setArguments(bundle)
            return myExchangesFrag
        }
    }
}