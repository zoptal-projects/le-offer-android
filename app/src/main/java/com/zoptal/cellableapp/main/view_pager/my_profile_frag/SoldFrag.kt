package com.zoptal.cellableapp.main.view_pager.my_profile_frag

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.annotation.Nullable
import androidx.core.app.ActivityOptionsCompat
import androidx.core.view.ViewCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout

import com.google.gson.Gson
import com.squareup.otto.Subscribe
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.adapter.ProfileSoldRvAdapter
import com.zoptal.cellableapp.event_bus.BusProvider.instance
import com.zoptal.cellableapp.main.activity.HomePageActivity
import com.zoptal.cellableapp.main.activity.products.ProductDetailsActivity
import com.zoptal.cellableapp.pojo_class.home_explore_pojo.ExploreResponseDatas
import com.zoptal.cellableapp.pojo_class.profile_selling_pojo.ProfileSellingData
import com.zoptal.cellableapp.pojo_class.profile_sold_pojo.ProfileSoldDatas
import com.zoptal.cellableapp.pojo_class.profile_sold_pojo.ProfileSoldMainPojo
import com.zoptal.cellableapp.pojo_class.social_frag_pojo.SocialDatas
import com.zoptal.cellableapp.utility.*
import com.zoptal.cellableapp.utility.OkHttp3Connection.OkHttp3RequestCallback
import org.json.JSONException
import org.json.JSONObject
import java.util.*

/**
 * <h>SoldFrag</h>
 *
 *
 * In this class we used to show to products which has been sold out.
 *
 * @since 4/7/2017
 */
class SoldFrag : Fragment(), ProductItemClickListener {
    private var mActivity: Activity? = null
    private var mSessionManager: SessionManager? = null
    private var progress_bar_profile: ProgressBar? = null
    private var rL_rootview: RelativeLayout? = null
    private var rL_noProductFound: RelativeLayout? = null
    private var arrayListSoldDatas: ArrayList<ProfileSoldDatas>? = null
    private var soldRvAdapter: ProfileSoldRvAdapter? = null
    private var rV_selling: RecyclerView? = null
    private var gridLayoutManager: StaggeredGridLayoutManager? = null
    private var memberName: String? = null
    private var mSwipeRefreshLayout: SwipeRefreshLayout? = null
    private var pageIndex = 0
    private var apiCall: ApiCall? = null
    private var isFromMyProfile = false

    // Load more var
    private var isLoadingRequired = false
    private var isToCallFirstTime = false
    private var visibleItemCount = 0
    private var totalItemCount = 0
    private val visibleThreshold = 5
    override fun onCreate(@Nullable savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        mActivity = getActivity()
        mSessionManager = SessionManager(mActivity!!)
        memberName = getArguments()!!.getString("memberName")
        isFromMyProfile = getArguments()!!.getBoolean("isFromMyProfileFlag", false)
        apiCall = ApiCall(mActivity!!)
        isToCallFirstTime = true
        println("$TAG fragment sold called...")
    }

    @Nullable
    override fun onCreateView(inflater: LayoutInflater, @Nullable container: ViewGroup?, @Nullable savedInstanceState: Bundle?): View {
        val view = inflater.inflate(R.layout.frag_profile_buying, container, false)
        pageIndex = 0
        rV_selling = view.findViewById<View>(R.id.rV_myprofile_selling) as RecyclerView
        mSwipeRefreshLayout = view.findViewById<View>(R.id.swipeRefreshLayout) as SwipeRefreshLayout
        mSwipeRefreshLayout!!.setColorSchemeResources(R.color.pink_color)

        // set space equility between recycler view items
        val spanCount = 2 // 2 columns
        val spacing = CommonClass.dpToPx(mActivity!!, 12) // convert 12dp to pixel
        rV_selling!!.addItemDecoration(SpacesItemDecoration(spanCount, spacing))
        progress_bar_profile = view.findViewById<View>(R.id.progress_bar_profile) as ProgressBar
        rL_rootview = view.findViewById<View>(R.id.rL_rootview) as RelativeLayout
        arrayListSoldDatas = ArrayList()
        soldRvAdapter = ProfileSoldRvAdapter(mActivity!!!!, arrayListSoldDatas!!, this)
        gridLayoutManager = StaggeredGridLayoutManager(2, 1)
        rV_selling!!.setLayoutManager(gridLayoutManager)
        rV_selling!!.setAdapter(soldRvAdapter)

        // set empty favourite icon
        rL_noProductFound = view.findViewById<View>(R.id.rL_noProductFound) as RelativeLayout
        rL_noProductFound!!.visibility = View.GONE
        val iV_default_icon = view.findViewById<View>(R.id.iV_default_icon) as ImageView
        iV_default_icon.setImageResource(R.drawable.empty_selling_icon)
        val tV_no_ads = view.findViewById<View>(R.id.tV_no_ads) as TextView
        val txtMsg: String = getResources().getString(R.string.no_sold_yet).toString() + "\n" + getResources().getString(R.string.you_have_no_sold_yet)
        tV_no_ads.text = txtMsg
        val tV_snapNpost = view.findViewById<View>(R.id.tV_snapNpost) as TextView
        tV_snapNpost.visibility = View.GONE
        val tV_start_discovering = view.findViewById<View>(R.id.tV_start_discovering) as TextView
        tV_start_discovering.visibility = View.GONE
        val rL_start_selling = view.findViewById<View>(R.id.rL_start_selling) as RelativeLayout
        rL_start_selling.visibility = View.GONE

        // call api call method
        if (CommonClass.isNetworkAvailable(mActivity!!) && isToCallFirstTime) {
            progress_bar_profile!!.visibility = View.VISIBLE
            profilePosts(pageIndex)
        } else CommonClass.showSnackbarMessage((mActivity!! as HomePageActivity?)!!.rL_rootElement, getResources().getString(R.string.NoInternetAccess))

        // pull to refresh
        mSwipeRefreshLayout!!.setColorSchemeResources(R.color.pink_color)
        mSwipeRefreshLayout!!.setOnRefreshListener(object : SwipeRefreshLayout.OnRefreshListener {
           override fun onRefresh() {
                pageIndex = 0
                arrayListSoldDatas!!.clear()
                soldRvAdapter!!.notifyDataSetChanged()
                profilePosts(pageIndex)
            }
        })
        return view
    }

   override fun onViewCreated(view: View, @Nullable savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        instance.register(this)
    }

    override fun onDestroyView() {
        instance.unregister(this)
        super.onDestroyView()
    }

    private fun profilePosts(offset: Int) {
        var offset = offset
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            val limit = 20
            offset = limit * offset
            val request_datas = JSONObject()
            try {
                request_datas.put("token", mSessionManager!!.authToken)
                request_datas.put("limit", limit)
                request_datas.put("offset", offset)
                request_datas.put("sold", "1")
                request_datas.put("membername", memberName)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            val url = ApiUrl.PROFILE_POST + memberName
            OkHttp3Connection.doOkHttp3Connection(TAG, url, OkHttp3Connection.Request_type.POST, request_datas, object : OkHttp3RequestCallback {
                 override fun onSuccess(result: String?, user_tag: String?) {
                    progress_bar_profile!!.visibility = View.GONE
                    println("$TAG profile sold res=$result")
                    mSwipeRefreshLayout!!.setRefreshing(false)

                    /*ExplorePojoMain explorePojoMain;
                    Gson gson = new Gson();
                    explorePojoMain = gson.fromJson(result, ExplorePojoMain.class);*/
                    val profileSoldMainPojo: ProfileSoldMainPojo
                    val gson = Gson()
                    profileSoldMainPojo = gson.fromJson(result, ProfileSoldMainPojo::class.java)
                    when (profileSoldMainPojo.code) {
                        "200" -> {
                            isToCallFirstTime = false
                            rL_noProductFound!!.visibility = View.GONE
                            if (profileSoldMainPojo.data != null && profileSoldMainPojo.data!!.size > 0) {
                                rL_noProductFound!!.visibility = View.GONE
                                arrayListSoldDatas!!.addAll(profileSoldMainPojo.data!!)
                                isLoadingRequired = arrayListSoldDatas!!.size > 14
                                soldRvAdapter!!.notifyDataSetChanged()

                                // Load more
                                rV_selling!!.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                                    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                                        super.onScrolled(recyclerView, dx, dy)
                                        val firstVisibleItemPositions = IntArray(2)
                                        totalItemCount = gridLayoutManager!!.getItemCount()
                                        visibleItemCount = gridLayoutManager!!.findLastVisibleItemPositions(firstVisibleItemPositions).get(0)
                                        if (isLoadingRequired && totalItemCount <= visibleItemCount + visibleThreshold) {
                                            isLoadingRequired = false
                                            pageIndex = pageIndex + 1
                                            mSwipeRefreshLayout!!.setRefreshing(true)
                                            profilePosts(pageIndex)
                                        }
                                    }
                                })
//                                break
                            }
                            mSwipeRefreshLayout!!.setRefreshing(false)
                            if (arrayListSoldDatas!!.size == 0) rL_noProductFound!!.visibility = View.VISIBLE
                        }
                        "204" -> {
                            mSwipeRefreshLayout!!.setRefreshing(false)
                            if (arrayListSoldDatas!!.size == 0) rL_noProductFound!!.visibility = View.VISIBLE
                        }
                        "401" -> {
                            mSwipeRefreshLayout!!.setRefreshing(false)
                            CommonClass.sessionExpired(mActivity!!)
                        }
                        else -> {
                            mSwipeRefreshLayout!!.setRefreshing(false)
                            println(TAG + " " + "sold frag error=" + profileSoldMainPojo.message)
                        }
                    }
                }

                 override fun onError(error: String?, user_tag: String?) {
                    mSwipeRefreshLayout!!.setRefreshing(false)
                    progress_bar_profile!!.visibility = View.GONE
                    CommonClass.showSnackbarMessage((mActivity!! as HomePageActivity?)!!.rL_rootElement, error)
                }
            })
        }
    }

    /**
     * <h>SellItAgainDialog</h>
     *
     *
     * In this method we used to open a dialog to alert the user to sell the item again.
     *
     * @param position the position of the item in the list
     */
    fun sellItAgainDialog(position: Int) {
        val errorMessageDialog = Dialog(mActivity!!!!)
        errorMessageDialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        errorMessageDialog.setContentView(R.layout.dialog_sell_it_again)
        errorMessageDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        errorMessageDialog.window!!.setLayout((CommonClass.getDeviceWidth(mActivity!!) * 0.8).toInt(), RelativeLayout.LayoutParams.WRAP_CONTENT)

        // dismiss
        val tV_no = errorMessageDialog.findViewById<View>(R.id.tV_no) as TextView
        tV_no.setOnClickListener { errorMessageDialog.dismiss() }

        // yes
        val tV_yes = errorMessageDialog.findViewById<View>(R.id.tV_yes) as TextView
        tV_yes.setOnClickListener {
            if (CommonClass.isNetworkAvailable(mActivity!!)) {
                apiCall!!.markSellingApi(rL_rootview, arrayListSoldDatas!![position].postId)
                addSellingDatas(arrayListSoldDatas!![position])
                addHomePageDatas(arrayListSoldDatas!![position])
                setSocialDatas(arrayListSoldDatas!![position])
                arrayListSoldDatas!!.removeAt(position)
                soldRvAdapter!!.notifyDataSetChanged()
                if (arrayListSoldDatas!!.size == 0) rL_noProductFound!!.visibility = View.VISIBLE
                errorMessageDialog.dismiss()
            } else {
                CommonClass.showSnackbarMessage((mActivity!! as HomePageActivity?)!!.rL_rootElement, getResources().getString(R.string.NoInternetAccess))
            }
        }
        errorMessageDialog.show()
    }

    /**
     * <h>AddSellingDatas</h>
     *
     *
     * In this method we used to make obj of Selling Frag datas set all values of that and
     * send that object to the Selling via event bus.
     *
     * @param mProfileSoldDatas The reference variables of ProfileSellingData.
     */
    private fun addSellingDatas(mProfileSoldDatas: ProfileSoldDatas) {
        val profileSellingData = ProfileSellingData()
        profileSellingData.postNodeId = mProfileSoldDatas.postNodeId
        profileSellingData.isPromoted = mProfileSoldDatas.isPromoted
        profileSellingData.planId = mProfileSoldDatas.planId
        profileSellingData.likes = mProfileSoldDatas.likes
        profileSellingData.mainUrl = mProfileSoldDatas.mainUrl
        profileSellingData.postLikedBy = mProfileSoldDatas.postLikedBy
        profileSellingData.place = mProfileSoldDatas.place
        profileSellingData.thumbnailImageUrl = mProfileSoldDatas.thumbnailImageUrl
        profileSellingData.postId = mProfileSoldDatas.postId
        profileSellingData.productsTagged = mProfileSoldDatas.productsTagged
        profileSellingData.productsTaggedCoordinates = mProfileSoldDatas.productsTaggedCoordinates
        profileSellingData.hasAudio = mProfileSoldDatas.hasAudio
        profileSellingData.containerHeight = mProfileSoldDatas.containerHeight
        profileSellingData.containerWidth = mProfileSoldDatas.containerWidth
        profileSellingData.hashTags = mProfileSoldDatas.hashTags
        profileSellingData.postCaption = mProfileSoldDatas.postCaption
        profileSellingData.latitude = mProfileSoldDatas.latitude
        profileSellingData.longitude = mProfileSoldDatas.longitude
        profileSellingData.thumbnailUrl1 = mProfileSoldDatas.thumbnailUrl1
        profileSellingData.imageUrl1 = mProfileSoldDatas.imageUrl1
        profileSellingData.thumbnailImageUrl = mProfileSoldDatas.thumbnailImageUrl
        profileSellingData.containerWidth1 = mProfileSoldDatas.containerWidth1
        profileSellingData.containerHeight1 = mProfileSoldDatas.containerHeight1
        profileSellingData.imageUrl2 = mProfileSoldDatas.imageUrl2
        profileSellingData.thumbnailUrl2 = mProfileSoldDatas.thumbnailUrl2
        profileSellingData.containerHeight2 = mProfileSoldDatas.containerHeight2
        profileSellingData.containerWidth2 = mProfileSoldDatas.containerWidth2
        profileSellingData.thumbnailUrl3 = mProfileSoldDatas.thumbnailUrl3
        profileSellingData.imageUrl3 = mProfileSoldDatas.imageUrl3
        profileSellingData.containerHeight3 = mProfileSoldDatas.containerHeight3
        profileSellingData.containerWidth3 = mProfileSoldDatas.containerWidth3
        profileSellingData.thumbnailUrl4 = mProfileSoldDatas.thumbnailUrl4
        profileSellingData.imageUrl4 = mProfileSoldDatas.imageUrl4
        profileSellingData.containerHeight4 = mProfileSoldDatas.containerHeight4
        profileSellingData.containerWidth4 = mProfileSoldDatas.containerWidth4
        profileSellingData.postsType = mProfileSoldDatas.postsType
        profileSellingData.postedOn = mProfileSoldDatas.postedOn
        profileSellingData.likeStatus = mProfileSoldDatas.likeStatus
        profileSellingData.sold = mProfileSoldDatas.sold
        profileSellingData.productUrl = mProfileSoldDatas.productUrl
        profileSellingData.description = mProfileSoldDatas.description
        profileSellingData.negotiable = mProfileSoldDatas.negotiable
        profileSellingData.condition = mProfileSoldDatas.condition
        profileSellingData.price = mProfileSoldDatas.price
        profileSellingData.currency = mProfileSoldDatas.currency
        profileSellingData.productName = mProfileSoldDatas.productName
        profileSellingData.totalComments = mProfileSoldDatas.totalComments
        profileSellingData.categoryData = mProfileSoldDatas.categoryData
        instance.post(profileSellingData)
    }

    /**
     * <h>AddHomePageDatas</h>
     *
     *
     * In this method we used to make obj of Home Frag datas set all values of that and
     * send that object to the Selling via event bus.
     *
     * @param mProfileSoldDatas The reference variables of ProfileSellingData.
     */
    private fun addHomePageDatas(mProfileSoldDatas: ProfileSoldDatas) {
        val mExploreResponseDatas = ExploreResponseDatas()
        mExploreResponseDatas.postedByUserName = mSessionManager!!.userName!!
        mExploreResponseDatas.postNodeId = mProfileSoldDatas.postNodeId
        mExploreResponseDatas.isPromoted = mProfileSoldDatas.isPromoted
        mExploreResponseDatas.likes = mProfileSoldDatas.likes
        mExploreResponseDatas.mainUrl = mProfileSoldDatas.mainUrl
        mExploreResponseDatas.place = mProfileSoldDatas.place
        mExploreResponseDatas.thumbnailImageUrl = mProfileSoldDatas.thumbnailImageUrl
        mExploreResponseDatas.postId = mProfileSoldDatas.postId
        mExploreResponseDatas.hasAudio = mProfileSoldDatas.hasAudio
        mExploreResponseDatas.containerHeight = mProfileSoldDatas.containerHeight
        mExploreResponseDatas.containerWidth = mProfileSoldDatas.containerWidth
        mExploreResponseDatas.hashTags = mProfileSoldDatas.hashTags
        mExploreResponseDatas.postCaption = mProfileSoldDatas.postCaption
        mExploreResponseDatas.latitude = mProfileSoldDatas.latitude
        mExploreResponseDatas.longitude = mProfileSoldDatas.longitude
        mExploreResponseDatas.thumbnailUrl1 = mProfileSoldDatas.thumbnailUrl1
        mExploreResponseDatas.imageUrl1 = mProfileSoldDatas.imageUrl1
        mExploreResponseDatas.thumbnailImageUrl = mProfileSoldDatas.thumbnailImageUrl
        mExploreResponseDatas.containerWidth1 = mProfileSoldDatas.containerWidth1
        mExploreResponseDatas.containerHeight1 = mProfileSoldDatas.containerHeight1
        mExploreResponseDatas.imageUrl2 = mProfileSoldDatas.imageUrl2
        mExploreResponseDatas.thumbnailUrl2 = mProfileSoldDatas.thumbnailUrl2
        mExploreResponseDatas.containerHeight2 = mProfileSoldDatas.containerHeight2
        mExploreResponseDatas.containerWidth2 = mProfileSoldDatas.containerWidth2
        mExploreResponseDatas.thumbnailUrl3 = mProfileSoldDatas.thumbnailUrl3
        mExploreResponseDatas.imageUrl3 = mProfileSoldDatas.imageUrl3
        mExploreResponseDatas.containerHeight3 = mProfileSoldDatas.containerHeight3
        mExploreResponseDatas.containerWidth3 = mProfileSoldDatas.containerWidth3
        mExploreResponseDatas.thumbnailUrl4 = mProfileSoldDatas.thumbnailUrl4
        mExploreResponseDatas.imageUrl4 = mProfileSoldDatas.imageUrl4
        mExploreResponseDatas.containerHeight4 = mProfileSoldDatas.containerHeight4
        mExploreResponseDatas.containerWidth4 = mProfileSoldDatas.containerWidth4
        mExploreResponseDatas.postsType = mProfileSoldDatas.postsType
        mExploreResponseDatas.postedOn = mProfileSoldDatas.postedOn
        mExploreResponseDatas.likeStatus = mProfileSoldDatas.likeStatus
        mExploreResponseDatas.productUrl = mProfileSoldDatas.productUrl
        mExploreResponseDatas.description = mProfileSoldDatas.description
        mExploreResponseDatas.negotiable = mProfileSoldDatas.negotiable
        mExploreResponseDatas.condition = mProfileSoldDatas.condition
        mExploreResponseDatas.price = mProfileSoldDatas.price
        mExploreResponseDatas.currency = mProfileSoldDatas.currency
        mExploreResponseDatas.productName = mProfileSoldDatas.productName
        mExploreResponseDatas.totalComments = mProfileSoldDatas.totalComments
        mExploreResponseDatas.categoryData = mProfileSoldDatas.categoryData
        instance.post(mExploreResponseDatas)
    }

    /**
     * <h>SetSocialDatas</h>
     *
     *
     * In this method we used to send the one complete object of a followed product
     * to the socail screen through screen.
     *
     */
    fun setSocialDatas(mProfileSoldDatas: ProfileSoldDatas) {
        try {
            val socialDatas = SocialDatas()
            socialDatas.isToAddSocialData = true
            socialDatas.postedOn = mProfileSoldDatas.postedOn
            socialDatas.productName = mProfileSoldDatas.productName
            socialDatas.categoryData = mProfileSoldDatas.categoryData
            socialDatas.currency = mProfileSoldDatas.currency
            socialDatas.price = mProfileSoldDatas.price
            //socialDatas.setMemberProfilePicUrl(mProfileSoldDatas.getMemberProfilePicUrl());
            socialDatas.mainUrl = mProfileSoldDatas.mainUrl
            socialDatas.likes = mProfileSoldDatas.likes
            //socialDatas.setClickCount(mProfileSoldDatas.getClickCount());
            socialDatas.likeStatus = mProfileSoldDatas.likeStatus
            socialDatas.postId = mProfileSoldDatas.postId
            socialDatas.membername = mSessionManager!!.userName!!
            instance.post(socialDatas)
        } catch (e: Exception) {
            println(TAG + " " + "social event bus error=" + e.message)
        }
    }

    override fun onDestroy() {
        try {
            instance.unregister(this)
            super.onDestroy()
        } catch (e: Exception) {
            super.onDestroy()
        }
    }

    /*
     * Updating the comment list data.*/
    @Subscribe
    fun getMessage(soldResDetails: ProfileSoldDatas?) {
        if (soldResDetails != null && !isContainsId(soldResDetails.postId)) {
            arrayListSoldDatas!!.add(0, soldResDetails)
            if (arrayListSoldDatas!!.size > 0) rL_noProductFound!!.visibility = View.GONE else rL_noProductFound!!.visibility = View.VISIBLE
            soldRvAdapter!!.notifyDataSetChanged()
            println("$TAG get message called...")
        }
    }

    /**
     * <h>IsContainsId</h>
     *
     *
     * In this method we used to check whether the given post id is
     * present or not in the current list.
     *
     * @param postId the given post id of product
     * @return the boolean value
     */
    fun isContainsId(postId: String): Boolean {
        var flag = false
        for (`object` in arrayListSoldDatas!!) {
            println(TAG + " " + "given post id=" + postId + " " + "current post id=" + `object`.postId)
            if (postId == `object`.postId) {
                flag = true
            }
        }
        return flag
    }

    override fun onItemClick(pos: Int, imageView: ImageView?) {

        println("$TAG sold item clicked")
        //        if (isFromMyProfile)
//            sellItAgainDialog(pos);
//        else
//        {
        val intent = Intent(mActivity!!, ProductDetailsActivity::class.java)
        intent.putExtra("productName", arrayListSoldDatas!![pos].productName)
        if (arrayListSoldDatas!![pos].categoryData != null) intent.putExtra("category", arrayListSoldDatas!![pos].categoryData!![0].category)
        intent.putExtra("likes", arrayListSoldDatas!![pos].likes)
        intent.putExtra("likeStatus", arrayListSoldDatas!![pos].likeStatus)
        intent.putExtra("currency", arrayListSoldDatas!![pos].currency)
        intent.putExtra("price", arrayListSoldDatas!![pos].price)
        intent.putExtra("postedOn", arrayListSoldDatas!![pos].postedOn)
        intent.putExtra("image", arrayListSoldDatas!![pos].mainUrl)
        intent.putExtra("thumbnailImageUrl", arrayListSoldDatas!![pos].thumbnailImageUrl)
        //intent.putExtra("likedByUsersArr",arrayListSoldDatas.get(pos).getLikedByUsers());
        intent.putExtra("description", arrayListSoldDatas!![pos].description)
        intent.putExtra("condition", arrayListSoldDatas!![pos].condition)
        intent.putExtra("place", arrayListSoldDatas!![pos].place)
        intent.putExtra("latitude", arrayListSoldDatas!![pos].latitude)
        intent.putExtra("longitude", arrayListSoldDatas!![pos].longitude)
        //intent.putExtra("postedByUserName",arrayListSoldDatas.get(pos).getPostedByUserName());
        intent.putExtra("postId", arrayListSoldDatas!![pos].postId)
        intent.putExtra("postsType", arrayListSoldDatas!![pos].postsType)
        intent.putExtra("followRequestStatus", "")
        intent.putExtra("clickCount", "")
        intent.putExtra("memberProfilePicUrl", "")
        intent.putExtra(VariableConstants.EXTRA_ANIMAL_IMAGE_TRANSITION_NAME, ViewCompat.getTransitionName(imageView!!))
        val options = ActivityOptionsCompat.makeSceneTransitionAnimation(mActivity!!!!, imageView, ViewCompat.getTransitionName(imageView)!!)
        startActivity(intent, options.toBundle())
        //        }
    }

    companion object {
        private val TAG = SoldFrag::class.java.simpleName
        @JvmStatic
        fun newInstance(memberName: String?, isFromMyProfile: Boolean): SoldFrag {
            val bundle = Bundle()
            bundle.putString("memberName", memberName)
            bundle.putBoolean("isFromMyProfileFlag", isFromMyProfile)
            val soldFrag = SoldFrag()
            soldFrag.setArguments(bundle)
            return soldFrag
        }
    }
}