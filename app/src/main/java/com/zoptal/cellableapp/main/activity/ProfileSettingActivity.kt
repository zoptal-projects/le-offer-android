package com.zoptal.cellableapp.main.activity

import android.app.Activity
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.fcm_push_notification.Config
import com.zoptal.cellableapp.fcm_push_notification.NotificationMessageDialog
import com.zoptal.cellableapp.fcm_push_notification.NotificationUtils.Companion.clearNotifications
import com.zoptal.cellableapp.main.activity.AddAddressActivity.Companion.callActivity
import com.zoptal.cellableapp.main.activity.ConnectPaypalActivity
import com.zoptal.cellableapp.main.activity.EditProfileActivity
import com.zoptal.cellableapp.main.activity.FacebookFriendsActivity
import com.zoptal.cellableapp.main.activity.GetAllCardsActivity.Companion.callActivity
import com.zoptal.cellableapp.main.activity.PhoneContactsActivity
import com.zoptal.cellableapp.utility.CommonClass
import com.zoptal.cellableapp.utility.DialogBox
import com.zoptal.cellableapp.utility.SessionManager
import com.zoptal.cellableapp.utility.VariableConstants

/**
 * <h>ProfileSettingActivity</h>
 *
 *
 * In this class we used to show severeal options to the user like finding friends,
 * edit profile, logout etc
 *
 *
 * @since 4/11/2017
 */
class ProfileSettingActivity : Activity(), View.OnClickListener {
    private var dialogBox: DialogBox? = null
    private var mActivity: Activity? = null
    private var mSessionManager: SessionManager? = null
    private var mNotificationMessageDialog: NotificationMessageDialog? = null
    private var payPalLink = ""
    private var isPaypalVerified = false
    private var isToStartActivity = false
    override fun onCreate(savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_profile_setting)
        overridePendingTransition(R.anim.slide_up, R.anim.stay)
        mActivity = this@ProfileSettingActivity
        isToStartActivity = true

        // receive datas from last class
        val intent = intent
        if (intent != null) payPalLink = intent.getStringExtra("payPalLink")
        mNotificationMessageDialog = NotificationMessageDialog(mActivity)
        CommonClass.statusBarColor(mActivity!!)
        mSessionManager = SessionManager(mActivity!!)
        dialogBox = DialogBox(mActivity!!)
        val rL_back_btn = findViewById<View>(R.id.rL_back_btn) as RelativeLayout
        rL_back_btn.setOnClickListener(this)
        val tV_logout = findViewById<View>(R.id.tV_logout) as TextView
        tV_logout.setOnClickListener(this)

        // edit profile
        val rL_edit_people = findViewById<View>(R.id.rL_edit_people) as RelativeLayout
        rL_edit_people.setOnClickListener(this)

        // find facebook friends
        val rL_fb_friends = findViewById<View>(R.id.rL_fb_friends) as RelativeLayout
        rL_fb_friends.setOnClickListener(this)

        // find contect friends
        val rL_contact_friends = findViewById<View>(R.id.rL_contact_friends) as RelativeLayout
        rL_contact_friends.setOnClickListener(this)

        // My payemnt
        val rL_my_payment = findViewById<View>(R.id.rL_my_payment) as RelativeLayout
        rL_my_payment.setOnClickListener(this)

        // Report problem
        val rL_report_problem = findViewById<View>(R.id.rL_report_problem) as RelativeLayout
        rL_report_problem.setOnClickListener(this)

        // privacy policy
        val rL_privacy = findViewById<View>(R.id.rL_privacy) as RelativeLayout
        rL_privacy.setOnClickListener(this)

        // Terms and condition
        val rL_termsNcondition = findViewById<View>(R.id.rL_termsNcondition) as RelativeLayout
        rL_termsNcondition.setOnClickListener(this)
        val rL_add_address = findViewById<View>(R.id.rL_add_address) as RelativeLayout
        rL_add_address.setOnClickListener(this)
        val rL_add_card = findViewById<View>(R.id.rL_add_card) as RelativeLayout
        rL_add_card.setOnClickListener(this)

        // set app version
        val tV_version = findViewById<View>(R.id.tV_version) as TextView
        try {
            val versionName = resources.getString(R.string.version) + " " + mActivity!!.getPackageManager().getPackageInfo(mActivity!!.getPackageName(), 0).versionName
            if (!versionName.isEmpty()) tV_version.text = versionName
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }
    }

    override fun onResume() {
        super.onResume()
        isToStartActivity = true
        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.REGISTRATION_COMPLETE))

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.PUSH_NOTIFICATION))

        // clear the notification area when the app is opened
        clearNotifications(applicationContext)
    }

    override fun onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!)
        super.onPause()
    }

    override fun onBackPressed() {
        val intent = Intent()
        intent.putExtra("isPaypalVerified", isPaypalVerified)
        intent.putExtra("payPalLink", payPalLink)
        setResult(VariableConstants.PAYPAL_REQ_CODE, intent)
        finish()
        overridePendingTransition(R.anim.stay, R.anim.slide_down)
    }

    override fun onClick(v: View) {
        val intent: Intent
        when (v.id) {
            R.id.tV_logout -> dialogBox!!.showLogoutAlert()
            R.id.rL_back_btn -> onBackPressed()
            R.id.rL_edit_people -> if (isToStartActivity) {
                startActivity(Intent(mActivity, EditProfileActivity::class.java))
                isToStartActivity = false
            }
            R.id.rL_fb_friends -> if (isToStartActivity) {
                intent = Intent(mActivity, FacebookFriendsActivity::class.java)
                startActivity(intent)
                isToStartActivity = false
            }
            R.id.rL_contact_friends -> if (isToStartActivity) {
                intent = Intent(mActivity, PhoneContactsActivity::class.java)
                startActivity(intent)
                isToStartActivity = false
            }
            R.id.rL_my_payment -> if (isToStartActivity) {
                intent = Intent(mActivity, ConnectPaypalActivity::class.java)
                intent.putExtra("payPalLink", payPalLink)
                startActivityForResult(intent, VariableConstants.PAYPAL_REQ_CODE)
                isToStartActivity = false
            }
            R.id.rL_report_problem -> CommonClass.sendEmail(mActivity!!, resources.getString(R.string.report_email), mSessionManager!!.userFullName!!, resources.getString(R.string.problem_on_yelo))
            R.id.rL_privacy -> if (isToStartActivity) {
                val privacyLink = resources.getString(R.string.privacyPolicyUrl)
                if (!privacyLink.isEmpty()) {
                    val privacyIntent = Intent(this, PrivacyAndTermsActivity::class.java)
                    privacyIntent.putExtra("url", resources.getString(R.string.privacyPolicyUrl))
                    privacyIntent.putExtra("title", resources.getString(R.string.privacyPolicy))
                    startActivity(privacyIntent)
                    isToStartActivity = false
                }
            }
            R.id.rL_termsNcondition -> if (isToStartActivity) {
                val termsNcondition = resources.getString(R.string.termsNconditionsUrl)
                if (!termsNcondition.isEmpty()) {
                    val termsNconIntent = Intent(this, PrivacyAndTermsActivity::class.java)
                    termsNconIntent.putExtra("url", resources.getString(R.string.termsNconditionsUrl))
                    termsNconIntent.putExtra("title", resources.getString(R.string.termsNcondition))
                    startActivity(termsNconIntent)
                    isToStartActivity = false
                }
            }
            R.id.rL_add_address -> if (isToStartActivity) {
                callActivity(this@ProfileSettingActivity, 1, "", "", "", "")
                isToStartActivity = false
            }
            R.id.rL_add_card -> callActivity(mActivity!!, 1, "", "", "", "", "", "", "", "", "", "")
            R.id.rL_purchased_items -> {
                val purchasedIntent = Intent(mActivity, PurchasedListActivity::class.java)
                startActivity(purchasedIntent)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)
        if (data != null) {
            when (requestCode) {
                VariableConstants.PAYPAL_REQ_CODE -> {
                    isPaypalVerified = data.getBooleanExtra("isPaypalVerified", false)
                    payPalLink = data.getStringExtra("payPalLink")
                }
            }
        }
    }
}