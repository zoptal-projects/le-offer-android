package com.zoptal.cellableapp.main.view_pager.search_product

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import androidx.annotation.Nullable
import androidx.core.app.ActivityOptionsCompat
import androidx.core.view.ViewCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.gson.Gson
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.adapter.SearchPostsRvAdap
import com.zoptal.cellableapp.main.activity.SearchProductActivity
import com.zoptal.cellableapp.main.activity.products.ProductDetailsActivity
import com.zoptal.cellableapp.pojo_class.search_post_pojo.SearchPostMainPojo
import com.zoptal.cellableapp.utility.*
import com.zoptal.cellableapp.utility.OkHttp3Connection.OkHttp3RequestCallback
import org.json.JSONObject
import java.util.*

/**
 * <h>PostsFrag</h>
 *
 *
 * In this class we used to show the list of searched products.
 *
 * @since 18-May-17
 */
class PostsFrag : Fragment() {
    private var mActivity: Activity? = null
    private var mSessionManager: SessionManager? = null
    private var rL_rootElement: RelativeLayout? = null
    private var rV_search_post: RecyclerView? = null
    private var isPostFrag = false
    private var page_index = 0
    private var mSwipeRefreshLayout: SwipeRefreshLayout? = null
    override fun onCreate(@Nullable savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        mActivity = getActivity()
        mSessionManager = SessionManager(mActivity!!)
    }

    @Nullable
    override fun onCreateView(inflater: LayoutInflater, @Nullable container: ViewGroup?, @Nullable savedInstanceState: Bundle?): View {
        val view = inflater.inflate(R.layout.frag_people, container, false)
        rL_rootElement = view.findViewById<View>(R.id.rL_rootElement) as RelativeLayout
        rV_search_post = view.findViewById<View>(R.id.rV_search_people) as RecyclerView
        mSwipeRefreshLayout = view.findViewById<View>(R.id.swipeRefreshLayout) as SwipeRefreshLayout
        mSwipeRefreshLayout!!.setColorSchemeResources(R.color.pink_color)

        // pull to refresh
        mSwipeRefreshLayout!!.setOnRefreshListener(object : SwipeRefreshLayout.OnRefreshListener {
            override fun onRefresh() {
                page_index = 0
                searchProductApi((mActivity as SearchProductActivity?)!!.postText, page_index)
            }
        })
        searchPost()
        return view
    }

    /**
     * <h>SearchPost</h>
     *
     *
     * In this method we used to do api call for each text changes.
     *
     */
    private fun searchPost() {
        (mActivity as SearchProductActivity?)!!.eT_search_users!!.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (isPostFrag) {
                    println(TAG + " " + "text entered=" + (mActivity as SearchProductActivity?)!!.eT_search_users!!.text.toString() + " " + "s=" + s)
                    (mActivity as SearchProductActivity?)!!.postText = (mActivity as SearchProductActivity?)!!.eT_search_users!!.text.toString()
                    page_index = 0
                    searchProductApi((mActivity as SearchProductActivity?)!!.eT_search_users!!.text.toString(), page_index)
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })
    }

    /**
     * <h>SearchProductApi</h>
     *
     *
     * In this method we used to do api call(using method Http get method) and get all post.
     *
     * @param searchText The character
     * @param offset The page index
     */
    private fun searchProductApi(searchText: String, offset: Int) {
        var offset = offset
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            val limit = 20
            offset = limit * offset
            println("$TAG offset=$offset searched text=$searchText")
            val URL = ApiUrl.SEARCH_POST + searchText + "?offset=" + offset + "?limit=" + limit + "?token=" + mSessionManager!!.authToken
            //System.out.println(TAG+" "+"url="+URL);
            OkHttp3Connection.doOkHttp3Connection(TAG, URL, OkHttp3Connection.Request_type.GET, JSONObject(), object : OkHttp3RequestCallback {
                 override fun onSuccess(result: String?, user_tag: String?) {
                    mSwipeRefreshLayout!!.setRefreshing(false)
                    println("$TAG search product api res=$result")
                    val searchPostMainPojo: SearchPostMainPojo
                    val gson = Gson()
                    searchPostMainPojo = gson.fromJson(result, SearchPostMainPojo::class.java)
                    when (searchPostMainPojo.code) {
                        "200" -> if (searchPostMainPojo.data != null && !searchPostMainPojo.data!!.isEmpty()) {
                            val aL_searchedPosts = searchPostMainPojo.data
                            // set post adapter
                            val searchPostsRvAdap = SearchPostsRvAdap(mActivity!!, aL_searchedPosts!!)
                            val mLinearLayoutManager = LinearLayoutManager(mActivity)
                            rV_search_post!!.setLayoutManager(mLinearLayoutManager)
                            rV_search_post!!.setAdapter(searchPostsRvAdap)
                            searchPostsRvAdap.notifyDataSetChanged()

                            // listen item click
                            searchPostsRvAdap.setOnItemClick(object : ProductItemClickListener {
                                override fun onItemClick(pos: Int, imageView: ImageView?) {
                                    val intent = Intent(mActivity, ProductDetailsActivity::class.java)
                                    intent.putExtra("productName", aL_searchedPosts[pos].productName)
                                    intent.putExtra("category", aL_searchedPosts[pos].category)
                                    intent.putExtra("likes", aL_searchedPosts[pos].likes)
                                    intent.putExtra("likeStatus", "")
                                    intent.putExtra("currency", aL_searchedPosts[pos].currency)
                                    intent.putExtra("price", aL_searchedPosts[pos].price)
                                    intent.putExtra("postedOn", aL_searchedPosts[pos].postedOn)
                                    intent.putExtra("image", aL_searchedPosts[pos].mainUrl)
                                    intent.putExtra("thumbnailImageUrl", aL_searchedPosts[pos].thumbnailImageUrl)
                                    intent.putExtra("likedByUsersArr", aL_searchedPosts[pos].likedByUsers)
                                    intent.putExtra("description", "")
                                    intent.putExtra("condition", "")
                                    intent.putExtra("place", aL_searchedPosts[pos].place)
                                    intent.putExtra("latitude", aL_searchedPosts[pos].latitude)
                                    intent.putExtra("longitude", aL_searchedPosts[pos].longitude)
                                    intent.putExtra("postedByUserName", aL_searchedPosts[pos].username)
                                    intent.putExtra("postId", aL_searchedPosts[pos].postId)
                                    intent.putExtra("postsType", aL_searchedPosts[pos].postsType)
                                    intent.putExtra("followRequestStatus", "")
                                    intent.putExtra("clickCount", "")
                                    intent.putExtra("memberProfilePicUrl", "")
                                    intent.putExtra(VariableConstants.EXTRA_ANIMAL_IMAGE_TRANSITION_NAME, ViewCompat.getTransitionName(imageView!!))
                                    val options = ActivityOptionsCompat.makeSceneTransitionAnimation(mActivity!!, imageView, ViewCompat.getTransitionName(imageView)!!)
                                    startActivity(intent, options.toBundle())
                                }
                            })
                        }
                        "401" -> CommonClass.sessionExpired(mActivity!!)
                        "204" -> {
                            val searchPostsRvAdap = SearchPostsRvAdap(mActivity!!, ArrayList())
                            val mLinearLayoutManager = LinearLayoutManager(mActivity)
                            rV_search_post!!.setLayoutManager(mLinearLayoutManager)
                            rV_search_post!!.setAdapter(searchPostsRvAdap)
                            searchPostsRvAdap.notifyDataSetChanged()
                        }
                    }
                }

                 override fun onError(error: String?, user_tag: String?) {
                    mSwipeRefreshLayout!!.setRefreshing(false)
                    CommonClass.showSnackbarMessage(rL_rootElement, error)
                }
            })
        } else CommonClass.showTopSnackBar(rL_rootElement, getResources().getString(R.string.NoInternetAccess))
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        isPostFrag = isVisibleToUser
    }

    companion object {
        private val TAG = PostsFrag::class.java.simpleName
    }
}