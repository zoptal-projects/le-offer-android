package com.zoptal.cellableapp.main.view_type_activity

import android.app.Activity
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.text.InputType
import android.view.View
import android.widget.EditText
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.fcm_push_notification.Config
import com.zoptal.cellableapp.fcm_push_notification.NotificationMessageDialog
import com.zoptal.cellableapp.fcm_push_notification.NotificationUtils.Companion.clearNotifications
import com.zoptal.cellableapp.main.view_type_activity.EditTextTypeActivity
import com.zoptal.cellableapp.utility.VariableConstants

class EditTextTypeActivity : AppCompatActivity(), View.OnClickListener {
    private var mNotificationMessageDialog: NotificationMessageDialog? = null
    private var rL_done: RelativeLayout? = null
    private var tV_fieldName: TextView? = null
    private var eT_fieldValue: EditText? = null
    private var fieldName: String? = null
    private var tV_title: TextView? = null
    private var title: String? = null
    private var type: String? = null
    private var selectedValue: String? = ""
    private var mActivity: Activity? = null
    protected override fun onCreate(savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_edit_text_view)
        mActivity = this@EditTextTypeActivity
        mNotificationMessageDialog = NotificationMessageDialog(mActivity)
        title = getIntent().getStringExtra("title")
        fieldName = getIntent().getStringExtra("fieldName")
        if (getIntent().hasExtra("isNumber")) {
            type = getIntent().getStringExtra("isNumber")
        }
        if (getIntent().hasExtra("selectedValue")) {
            selectedValue = getIntent().getStringExtra("selectedValue")
        }
        tV_title = findViewById(R.id.tV_title) as TextView?
        tV_title!!.text = title
        tV_fieldName = findViewById(R.id.tV_fieldName) as TextView?
        tV_fieldName!!.text = fieldName
        eT_fieldValue = findViewById(R.id.eT_fieldValue) as EditText?
        //eT_fieldValue.setHint(fieldName);
        if (selectedValue != null && !selectedValue!!.isEmpty()) eT_fieldValue!!.setText(selectedValue)
        if (type == "1") {
            eT_fieldValue!!.inputType = InputType.TYPE_CLASS_NUMBER
        }
        rL_done = findViewById(R.id.rL_done) as RelativeLayout?
        rL_done!!.setOnClickListener(this)
        val rL_back_btn = findViewById(R.id.rL_back_btn) as RelativeLayout
        rL_back_btn.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.rL_back_btn -> onBackPressed()
            R.id.rL_done -> {
                val intent: Intent = getIntent()
                intent.putExtra("selectedValue", eT_fieldValue!!.text.toString().trim { it <= ' ' })
                setResult(VariableConstants.SELECT_VALUE_REQ_CODE, intent)
                onBackPressed()
            }
        }
    }

    override fun onResume() {
        super.onResume()

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.REGISTRATION_COMPLETE))

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.PUSH_NOTIFICATION))

        // clear the notification area when the app is opened
        clearNotifications(getApplicationContext())
    }

   override fun onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!)
        super.onPause()
    }
}