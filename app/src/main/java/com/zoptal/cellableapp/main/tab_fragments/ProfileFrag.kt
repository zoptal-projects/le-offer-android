package com.zoptal.cellableapp.main.tab_fragments

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.RatingBar
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager

import com.bumptech.glide.Glide
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.Scopes
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.Scope
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.tabs.TabLayout
import com.google.gson.Gson
import com.squareup.otto.Subscribe
import com.squareup.picasso.Picasso
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.adapter.ViewPagerAdapter
import com.zoptal.cellableapp.event_bus.BusProvider
import com.zoptal.cellableapp.main.activity.ConnectPaypalActivity
import com.zoptal.cellableapp.main.activity.DiscoverPeopleActivity
import com.zoptal.cellableapp.main.activity.EditProfileActivity
import com.zoptal.cellableapp.main.activity.HomePageActivity
import com.zoptal.cellableapp.main.activity.PaymentAddAccount
import com.zoptal.cellableapp.main.activity.ProfileSettingActivity
import com.zoptal.cellableapp.main.activity.SelfFollowingActivity
import com.zoptal.cellableapp.main.activity.VerifyEmailIdActivity
import com.zoptal.cellableapp.main.activity.VerifyLoginWithFacebook
import com.zoptal.cellableapp.main.activity.VerifyLoginWithGplus
import com.zoptal.cellableapp.main.view_pager.my_profile_frag.FavouriteFrag
import com.zoptal.cellableapp.main.view_pager.my_profile_frag.MyExchangesFrag
import com.zoptal.cellableapp.main.view_pager.my_profile_frag.SellingFrag
import com.zoptal.cellableapp.main.view_pager.my_profile_frag.SoldFrag
import com.zoptal.cellableapp.pojo_class.ProfileFollowingCount
import com.zoptal.cellableapp.pojo_class.payment.PaymentPojoMain
import com.zoptal.cellableapp.pojo_class.profile_pojo.ProfilePojoMain
import com.zoptal.cellableapp.pojo_class.profile_pojo.ProfileResultDatas
import com.zoptal.cellableapp.utility.ApiUrl
import com.zoptal.cellableapp.utility.CircleTransform
import com.zoptal.cellableapp.utility.CommonClass
import com.zoptal.cellableapp.utility.OkHttp3Connection
import com.zoptal.cellableapp.utility.PositionedCropTransformation
import com.zoptal.cellableapp.utility.SessionManager
import com.zoptal.cellableapp.utility.VariableConstants

import org.json.JSONException
import org.json.JSONObject

import com.zoptal.cellableapp.utility.VariableConstants.STRIPE_ACCOUNT_ADDED_PROFILE

/**
 * <h>ProfileFrag</h>
 *
 *
 * In this class we show the user profile description like user name, user profile
 * users total posts, following count, follower count etc
 *
 *
 * @since 3/31/2017
 */
class ProfileFrag : Fragment(), View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {
    private var mSessionManager: SessionManager? = null
    private var mActivity: Activity? = null
    private var profilePicUrl: String? = ""
    private var fullName: String? = ""
    private var userName: String? = ""
    private var email = ""
    private var phoneNumber = ""
    private var website: String? = ""
    private var bio: String? = ""
    private var googleVerified: String? = ""
    private var facebookVerified: String? = ""
    private var emailVerified: String? = ""
    private var paypalUrl = ""
    private var followingCount: String? = ""
    private var verifyLoginWithGplus: VerifyLoginWithGplus? = null
    private var sellingFrag: SellingFrag? = null
    private var soldFrag: SoldFrag? = null
    private var favouriteFrag: FavouriteFrag? = null
    private var myExchangesFrag: MyExchangesFrag? = null
    private var linear_load_profile: LinearLayout? = null
    var btn_stripe_connect: Button? = null
    // Declare xml variables
    private var progress_bar_profile: ProgressBar? = null
    private var iV_profile_pic: ImageView? = null
    private var iv_background: ImageView? = null
    private var iV_email_icon: ImageView? = null
    private var iV_google_icon: ImageView? = null
    private var iV_fbicon: ImageView? = null
    private var iV_paypal_icon: ImageView? = null
    private var tV_posts_count: TextView? = null
    private var tV_follower_count: TextView? = null
    private var tV_following_count: TextView? = null
    private var tV_fullName: TextView? = null
    private var tV_bio: TextView? = null
    private var tV_website: TextView? = null
    private var tV_postsText: TextView? = null
    private var profileViewPager: ViewPager? = null
    private var rL_edit_people: RelativeLayout? = null
    private val rL_verify_paypal: RelativeLayout? = null
    private var isProfileFragVisible: Boolean = false
    private var mGoogleApiClient: GoogleApiClient? = null
    private var appBarLayout: AppBarLayout? = null
    internal var accountId = ""
    internal var isstripeConnected = 0
    private var ratingBar: RatingBar? = null
    private var avgRating: String? = ""
    private var ratedBy = ""
    private var tv_ratedBy: TextView? = null
    //private SwipeRefreshLayout mSwipeRefreshLayout;
    private var isRefresh = false

    override fun onCreate(savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        mActivity = activity
        mSessionManager = SessionManager(mActivity!!)

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestScopes(Scope(Scopes.DRIVE_APPFOLDER))
                .requestServerAuthCode(mActivity!!.resources.getString(R.string.servers_client_id))
                .requestEmail()
                .build()

        try {
            mGoogleApiClient = GoogleApiClient.Builder(mActivity!!)
                    .enableAutoManage(activity!!, this)
                    .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                    .build()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        BusProvider.instance.register(this)
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

            val view = inflater.inflate(R.layout.frag_profile, container, false)
        try {
            progress_bar_profile = view.findViewById(R.id.progress_bar_profile)
            appBarLayout = view.findViewById(R.id.appBarLayout) as AppBarLayout
            appBarLayout!!.setVisibility(View.GONE)

            // mSwipeRefreshLayout=(SwipeRefreshLayout) view.findViewById(R.id.mSwipeRefreshLayout);
            // mSwipeRefreshLayout.setColorSchemeResources(R.color.pink_color);
            // profile pic
            iV_profile_pic = view.findViewById(R.id.iV_profile_pic)
            iv_background = view.findViewById(R.id.iv_background)
            iV_profile_pic!!.layoutParams.width = CommonClass.getDeviceWidth(mActivity!!) / 4
            iV_profile_pic!!.layoutParams.height = CommonClass.getDeviceWidth(mActivity!!) / 4
            val coordinate_rootView = view.findViewById(R.id.coordinate_rootView) as CoordinatorLayout

            // User name and full name
            tV_fullName = view.findViewById(R.id.tV_fullName)
            tV_website = view.findViewById(R.id.tV_website)
            tV_website!!.setOnClickListener(this)
            tV_bio = view.findViewById(R.id.tV_bio)
            btn_stripe_connect = view.findViewById(R.id.btn_stripe_connect)
            ratingBar = view.findViewById(R.id.ratingBar)
            tv_ratedBy = view.findViewById(R.id.tV_ratedBy)

            // verification ioon like email, google plus , facebook etc

            // Facebook verification
            val pBar_fbVerify = view.findViewById(R.id.pBar_fbVerify) as ProgressBar
            iV_fbicon = view.findViewById(R.id.iV_fbicon)
            iV_paypal_icon = view.findViewById(R.id.iV_paypal_icon)
            iV_paypal_icon!!.setOnClickListener(this)
            (mActivity as HomePageActivity).verifyLoginWithFacebook = VerifyLoginWithFacebook(
                    mActivity!!,
                    coordinate_rootView,
                    pBar_fbVerify,
                    iV_fbicon!!,
                    facebookVerified!!
            )
            iV_fbicon!!.setOnClickListener {
                if (facebookVerified == null || facebookVerified != "1")
                    (mActivity as HomePageActivity).verifyLoginWithFacebook!!.loginWithFbWithSdk()
            }

            // Google plus verification
            val pBar_gPlusVerify = view.findViewById(R.id.pBar_gPlusVerify) as ProgressBar
            iV_google_icon = view.findViewById(R.id.iV_google_icon)
            if(mGoogleApiClient!=null) {
                verifyLoginWithGplus = VerifyLoginWithGplus(
                        mActivity!!,
                        mGoogleApiClient!!,
                        coordinate_rootView,
                        pBar_gPlusVerify,
                        iV_google_icon!!,
                        googleVerified!!
                )
            }
            iV_google_icon!!.setOnClickListener {
                if (googleVerified == null || googleVerified != "1"){
                    verifyLoginWithGplus?.signInWithGoogle(
                            this@ProfileFrag,
                            mActivity!!
                    )
            }
            }

            // Email verification
            iV_email_icon = view.findViewById(R.id.iV_email_icon)
            iV_email_icon!!.setOnClickListener {
                if (emailVerified == null || emailVerified != "1"){
                    startActivity(
                            Intent(
                                    mActivity,
                                    VerifyEmailIdActivity::class.java
                            )
                    )
            }
            }

            // set profile pic
            val profilePic = mSessionManager!!.userImage
            Log.d("fb profile pic trace", profilePic!!)
            if (profilePic != null && !profilePic!!.isEmpty()) {
                Picasso.with(mActivity)
                        .load(profilePic)
                        .resize(
                                CommonClass.getDeviceWidth(mActivity!!) / 4,
                                CommonClass.getDeviceWidth(mActivity!!) / 4
                        )
                        .placeholder(R.drawable.default_profile_image)
                        .error(R.drawable.default_profile_image)
                        .transform(CircleTransform())
                        .into(iV_profile_pic)

                Glide.with(mActivity!!)
                        .load(profilePic)
                        .transform(PositionedCropTransformation(mActivity, 0f, 0f))
                        .placeholder(R.drawable.default_profile_cover_pic)
                        .error(R.drawable.default_profile_cover_pic)
                        .into(iv_background)
            }

            // Full name
            fullName = mSessionManager!!.userName
            if (fullName != null)
                tV_fullName!!.text = fullName

            // number of posts
            tV_posts_count = view.findViewById(R.id.tV_posts_count)
            tV_postsText = view.findViewById(R.id.tV_postsText)
            tV_follower_count = view.findViewById(R.id.tV_follower_count)
            tV_following_count = view.findViewById(R.id.tV_following_count)

            // Following
            val rL_following = view.findViewById(R.id.rL_following) as RelativeLayout
            rL_following.setOnClickListener(this)

            // Follower
            val rL_follower = view.findViewById(R.id.rL_follower) as RelativeLayout
            rL_follower.setOnClickListener(this)

            // Edit profile
            rL_edit_people = view.findViewById(R.id.rL_edit_people)

            // verify paypal
            //        rL_verify_paypal = (RelativeLayout) view.findViewById(R.id.rL_verify_paypal);
            //        rL_verify_paypal.setOnClickListener(this);

            // setting
            val rL_setting = view.findViewById(R.id.rL_setting) as RelativeLayout
            rL_setting.setOnClickListener(this)

            // Discover people
            val rL_discovery_people = view.findViewById(R.id.rL_discovery_people) as RelativeLayout
            rL_discovery_people.setOnClickListener(this)

            // view pager
            profileViewPager = view.findViewById(R.id.viewpager)
            profileViewPager!!.offscreenPageLimit = 3
            val tabs = view.findViewById(R.id.tabs) as TabLayout
            tabs.setupWithViewPager(profileViewPager)

            println("$TAG frag visible profile=$isProfileFragVisible")

            // Re-Load profile datas
            linear_load_profile = view.findViewById(R.id.linear_load_profile)
            linear_load_profile!!.visibility = View.GONE
            linear_load_profile!!.setOnClickListener(this)

            // Load profile datas
            getUserProfileDatas()

            /*mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Load profile datas
                isRefresh=true;
                getUserProfileDatas();
            }
        });*/
        }catch (e:Exception){e.printStackTrace()}
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        println("$TAG onActivityCreated profile")
    }

    /**
     * <h>GetUserProfileDatas</h>
     *
     *
     * In this method we do api call to get users profile complete description
     * Once we receive all datas then set all values to the respective fields.
     *
     */
    private fun getUserProfileDatas() {
        // token, limit, offset, membername
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            linear_load_profile!!.visibility = View.GONE
            progress_bar_profile!!.visibility = View.VISIBLE
            val request_datas = JSONObject()
            try {
                request_datas.put("token", mSessionManager!!.authToken)
                request_datas.put("limit", "20")
                request_datas.put("offset", "0")
                request_datas.put("membername", mSessionManager!!.userName)

            } catch (e: JSONException) {
                e.printStackTrace()
            }

            OkHttp3Connection.doOkHttp3Connection(
                    TAG,
                    ApiUrl.USER_PROFILE,
                    OkHttp3Connection.Request_type.POST,
                    request_datas,
                    object : OkHttp3Connection.OkHttp3RequestCallback {
                        override fun onSuccess(result: String?, user_tag: String?) {
                            // mSwipeRefreshLayout.setRefreshing(false);
                            linear_load_profile!!.visibility = View.GONE
                            progress_bar_profile!!.visibility = View.GONE
                            println("$TAG my profile res=$result")

                            val profilePojoMain: ProfilePojoMain
                            val gson = Gson()
                            profilePojoMain = gson.fromJson(result, ProfilePojoMain::class.java)

                            when (profilePojoMain.code) {
                                // Success
                                "200" -> {
                                    appBarLayout!!.setVisibility(View.VISIBLE)
                                    // Set my profile fields like image, name etc
                                    val profileResponseDatas = profilePojoMain.data
                                    if (profileResponseDatas != null && profileResponseDatas!!.size > 0) {
                                        val profileResultDatas = profileResponseDatas!!.get(0)
                                        val noOfPost: String?
                                        val followersCount: String?
                                        var int_postCount = 0
                                        profilePicUrl = profileResultDatas.profilePicUrl
                                        noOfPost = profileResultDatas.posts
                                        if (noOfPost != null && !noOfPost!!.isEmpty())
                                            int_postCount = Integer.parseInt(noOfPost!!)

                                        if (int_postCount > 1)
                                            tV_postsText!!.text = resources.getString(R.string.posts)
                                        else
                                            tV_postsText!!.text = resources.getString(R.string.post)

                                        isstripeConnected = profileResultDatas.is_strip_connected
                                        accountId = profileResultDatas.accountId
                                        if (isstripeConnected == 0) {
                                            btn_stripe_connect!!.text =
                                                    resources.getString(R.string.connect_stripe)
                                        } else {
                                            btn_stripe_connect!!.text =
                                                    resources.getString(R.string.disconnect_stripe)
                                        }
                                        btn_stripe_connect!!.setOnClickListener {
                                            if (isstripeConnected == 0) {
                                                PaymentAddAccount.callActivity(
                                                        context!!,
                                                        profileResultDatas.weburl,
                                                        STRIPE_ACCOUNT_ADDED_PROFILE
                                                )
                                            } else {
                                                btn_stripe_connect!!.text =
                                                        resources.getString(R.string.disconnect_stripe)
                                                disconnectStripeAccount()
                                            }
                                        }
                                        followersCount = profileResultDatas.followers
                                        followingCount = profileResultDatas.following
                                        fullName = profileResultDatas.fullName
                                        email = profileResultDatas.email
                                        userName = profileResultDatas.username
                                        phoneNumber = profileResultDatas.phoneNumber
                                        website = profileResultDatas.website
                                        bio = profileResultDatas.bio
                                        googleVerified = profileResultDatas.googleVerified
                                        facebookVerified = profileResultDatas.facebookVerified
                                        emailVerified = profileResultDatas.emailVerified
                                        paypalUrl = profileResultDatas.paypalUrl
                                        avgRating = profileResultDatas.avgRating
                                        ratedBy = profileResponseDatas!!.get(0).ratedBy

                                        // Set profile pic
                                        if (profilePicUrl != null && !profilePicUrl!!.isEmpty()) {
                                            Picasso.with(mActivity)
                                                    .load(profilePicUrl)
                                                    .placeholder(R.drawable.default_profile_image)
                                                    .error(R.drawable.default_profile_image)
                                                    .transform(CircleTransform())
                                                    .into(iV_profile_pic)

                                            // set profile pic as cover pic
                                            Glide.with(mActivity)
                                                    .load(profilePicUrl)
                                                    .transform(
                                                            PositionedCropTransformation(
                                                                    mActivity,
                                                                    0f,
                                                                    0f
                                                            )
                                                    )
                                                    .placeholder(R.drawable.default_profile_cover_pic)
                                                    .error(R.drawable.default_profile_cover_pic)
                                                    .into(iv_background)
                                        }
                                        ratingBar!!.visibility = View.VISIBLE// set rating
                                        tv_ratedBy!!.visibility =
                                                View.VISIBLE //..visible while get rating

                                        if (avgRating != null && !avgRating!!.isEmpty()) {
                                            //..visible while get rating
                                            ratingBar!!.rating = java.lang.Float.parseFloat(avgRating!!)

                                            ratedBy = "($avgRating)"
                                            tv_ratedBy!!.text = avgRating
                                        } else {
                                            ratingBar!!.rating = java.lang.Float.parseFloat("0")
                                            tv_ratedBy!!.text = "(0)"
                                        }
                                        //                                if (ratedBy != null && !ratedBy.isEmpty()) {
                                        //                                    ratingBar.setVisibility(View.VISIBLE); //..visible while get rating
                                        //                                    tv_ratedBy.setVisibility(View.VISIBLE); //..visible while get rating
                                        //                                    ratedBy = "(" + ratedBy + ")";
                                        //                                    tv_ratedBy.setText(ratedBy);
                                        //                                }

                                        // set number of posts
                                        if (noOfPost != null)
                                            tV_posts_count!!.text = noOfPost

                                        // Followers count
                                        if (followersCount != null)
                                            tV_follower_count!!.text = followersCount

                                        // Following count
                                        if (followingCount != null)
                                            tV_following_count!!.text = followingCount

                                        // Full name
                                        if (fullName != null)
                                            tV_fullName!!.text = fullName

                                        // Edit profile
                                        rL_edit_people!!.setOnClickListener {
                                            val editProfileIntent =
                                                    Intent(mActivity, EditProfileActivity::class.java)
                                            editProfileIntent.putExtra("profilePicUrl", profilePicUrl)
                                            editProfileIntent.putExtra("username", userName)
                                            editProfileIntent.putExtra("fullName", fullName)
                                            editProfileIntent.putExtra("email", email)
                                            editProfileIntent.putExtra("phoneNumber", phoneNumber)
                                            editProfileIntent.putExtra("website", website)
                                            editProfileIntent.putExtra("bio", bio)
                                            this@ProfileFrag.startActivityForResult(
                                                    editProfileIntent,
                                                    VariableConstants.PROFILE_REQUEST_CODE
                                            )
                                        }

                                        // set google verified
                                        if (googleVerified != null && googleVerified == "1")
                                            iV_google_icon!!.setImageResource(R.drawable.gplus_verified_icon)

                                        // set facebook verified
                                        if (facebookVerified != null && facebookVerified == "1")
                                            iV_fbicon!!.setImageResource(R.drawable.facebook_verified_icon)

                                        // set email verified
                                        if (emailVerified != null && emailVerified == "1")
                                            iV_email_icon!!.setImageResource(R.drawable.email_verified_icon)

                                        // set payal verified
                                        //                                if (paypalUrl != null && !paypalUrl.equals("0") && !paypalUrl.equals("")) {
                                        //                                    iV_paypal_icon.setVisibility(View.VISIBLE);
                                        //                                    rL_verify_paypal.setVisibility(View.GONE);
                                        //                                } else {
                                        //                                    iV_paypal_icon.setVisibility(View.GONE);
                                        //                                    rL_verify_paypal.setVisibility(View.VISIBLE);
                                        //                                }

                                        // set bio
                                        if (bio != null && !bio!!.isEmpty()) {
                                            tV_bio!!.visibility = View.VISIBLE
                                            tV_bio!!.text = bio
                                        } else
                                            tV_bio!!.visibility = View.GONE

                                        // set website
                                        if (website != null && !website!!.isEmpty()) {
                                            tV_website!!.visibility = View.VISIBLE
                                            tV_website!!.text = website
                                        } else
                                            tV_website!!.visibility = View.GONE
                                    }

                                    if (!isRefresh) {
                                        sellingFrag = SellingFrag.newInstance(
                                                mSessionManager!!.userName,
                                                true
                                        )
                                        soldFrag =
                                                SoldFrag.newInstance(mSessionManager!!.userName, true)
                                        favouriteFrag =
                                                FavouriteFrag.newInstance(mSessionManager!!.userName)
                                        myExchangesFrag =
                                                MyExchangesFrag.newInstance(mSessionManager!!.userName)
                                        setupViewPager()
                                    }
                                }

                                // auth token expired
                                "401" -> CommonClass.sessionExpired(mActivity!!)

                                // Error
                                else -> CommonClass.showSnackbarMessage(
                                        (mActivity as HomePageActivity).rL_rootElement,
                                        profilePojoMain.message
                                )
                            }
                        }

                        override fun onError(error: String?, user_tag: String?) {
                            //mSwipeRefreshLayout.setRefreshing(false);
                            linear_load_profile!!.visibility = View.VISIBLE
                            progress_bar_profile!!.visibility = View.GONE
                            CommonClass.showSnackbarMessage(
                                    (mActivity as HomePageActivity).rL_rootElement,
                                    error
                            )
                        }
                    })
        } else {
            linear_load_profile!!.visibility = View.VISIBLE
            progress_bar_profile!!.visibility = View.GONE
            CommonClass.showSnackbarMessage(
                    (mActivity as HomePageActivity).rL_rootElement,
                    resources.getString(R.string.NoInternetAccess)
            )
        }
    }

    private fun disconnectStripeAccount() {
        // token, limit, offset, membername
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            linear_load_profile!!.visibility = View.GONE
            progress_bar_profile!!.visibility = View.VISIBLE
            val request_datas = JSONObject()
            try {
                request_datas.put("token", mSessionManager!!.authToken)
                request_datas.put("accountId", accountId)

            } catch (e: JSONException) {
                e.printStackTrace()
            }

            OkHttp3Connection.doOkHttp3Connection(
                    TAG,
                    ApiUrl.STRIPE_DISCONNECT,
                    OkHttp3Connection.Request_type.POST,
                    request_datas,
                    object : OkHttp3Connection.OkHttp3RequestCallback {
                        override fun onSuccess(result: String?, user_tag: String?) {
                            // mSwipeRefreshLayout.setRefreshing(false);
                            linear_load_profile!!.visibility = View.GONE
                            progress_bar_profile!!.visibility = View.GONE
                            println("$TAG my profile res=$result")

                            val profilePojoMain: PaymentPojoMain
                            val gson = Gson()
                            profilePojoMain = gson.fromJson(result, PaymentPojoMain::class.java)

                            when (profilePojoMain.code) {
                                // Success
                                "200" -> {
                                    appBarLayout!!.setVisibility(View.VISIBLE)
                                    btn_stripe_connect!!.text =
                                            resources.getString(R.string.connect_stripe)
                                    isstripeConnected = 0
                                    accountId = ""
                                }

                                // auth token expired
                                "401" -> CommonClass.sessionExpired(mActivity!!)

                                // Error
                                else -> CommonClass.showSnackbarMessage(
                                        (mActivity as HomePageActivity).rL_rootElement,
                                        profilePojoMain.message
                                )
                            }
                        }

                        override fun onError(error: String?, user_tag: String?) {
                            //mSwipeRefreshLayout.setRefreshing(false);
                            linear_load_profile!!.visibility = View.VISIBLE
                            progress_bar_profile!!.visibility = View.GONE
                            CommonClass.showSnackbarMessage(
                                    (mActivity as HomePageActivity).rL_rootElement,
                                    error
                            )
                        }
                    })
        } else {
            linear_load_profile!!.visibility = View.VISIBLE
            progress_bar_profile!!.visibility = View.GONE
            CommonClass.showSnackbarMessage(
                    (mActivity as HomePageActivity).rL_rootElement,
                    resources.getString(R.string.NoInternetAccess)
            )
        }
    }

    /**
     * <h>SetupViewPager</h>
     *
     *
     * In this method we used to set viewpager with respective fragments.
     *
     */
    private fun setupViewPager() {
        if (isAdded) {
            val adapter = ViewPagerAdapter(childFragmentManager)
            adapter.addFragment(sellingFrag!!, resources.getString(R.string.selling))
            adapter.addFragment(myExchangesFrag!!, resources.getString(R.string.my_exchanges))
            adapter.addFragment(soldFrag!!, resources.getString(R.string.sold))
            adapter.addFragment(favouriteFrag!!, resources.getString(R.string.favourites))
            profileViewPager!!.adapter = adapter
        }
    }

    override fun onClick(v: View) {
        var mFollowingCount = 0
        val intent: Intent
        when (v.id) {
            // Profile setting
            R.id.rL_setting -> {
                intent = Intent(mActivity, ProfileSettingActivity::class.java)
                intent.putExtra("payPalLink", paypalUrl)
                this@ProfileFrag.startActivityForResult(intent, VariableConstants.PAYPAL_REQ_CODE)
            }

            // Discover people
            R.id.rL_discovery_people -> {
                if (followingCount != null && !followingCount!!.isEmpty())
                    mFollowingCount = Integer.parseInt(followingCount!!)
                intent = Intent(mActivity, DiscoverPeopleActivity::class.java)
                intent.putExtra("followingCount", mFollowingCount)
                startActivityForResult(intent, VariableConstants.FOLLOW_COUNT_REQ_CODE)
            }

            // Following
            R.id.rL_following -> {
                if (followingCount != null && !followingCount!!.isEmpty())
                    mFollowingCount = Integer.parseInt(followingCount!!)
                intent = Intent(mActivity, SelfFollowingActivity::class.java)
                intent.putExtra("title", resources.getString(R.string.Following))
                intent.putExtra("isFollower", false)
                intent.putExtra("followingCount", mFollowingCount)
                startActivityForResult(intent, VariableConstants.FOLLOW_COUNT_REQ_CODE)
            }

            // Follower
            R.id.rL_follower -> {
                if (followingCount != null && !followingCount!!.isEmpty())
                    mFollowingCount = Integer.parseInt(followingCount!!)
                intent = Intent(mActivity, SelfFollowingActivity::class.java)
                intent.putExtra("title", resources.getString(R.string.Followers))
                intent.putExtra("isFollower", true)
                intent.putExtra("followingCount", mFollowingCount)
                startActivityForResult(intent, VariableConstants.FOLLOW_COUNT_REQ_CODE)
            }

            // verify paypal
            //            case R.id.rL_verify_paypal:
            //                intent = new Intent(mActivity, ConnectPaypalActivity.class);
            //                ProfileFrag.this.startActivityForResult(intent, VariableConstants.PAYPAL_REQ_CODE);
            //                break;

            // open website link
            R.id.tV_website -> {
                println("$TAG website=$website")

                if (website != null && !website!!.isEmpty()) {
                    val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("http://" + website!!))
                    startActivity(browserIntent)
                }
            }

            // re open paypal for update
            R.id.iV_paypal_icon -> {
                println("$TAG send paypal url=$paypalUrl")
                intent = Intent(mActivity, ConnectPaypalActivity::class.java)
                intent.putExtra("payPalLink", paypalUrl)
                this@ProfileFrag.startActivityForResult(intent, VariableConstants.PAYPAL_REQ_CODE)
            }

            // Reload profile data again
            R.id.linear_load_profile -> getUserProfileDatas()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        println("$TAG profile onactivity result...==$requestCode")
        super.onActivityResult(requestCode, resultCode, data)
        if (data != null) {
            when (requestCode) {
                VariableConstants.PROFILE_REQUEST_CODE -> {
                    profilePicUrl = data.getStringExtra("profilePicUrl")
                    userName = data.getStringExtra("username")
                    fullName = data.getStringExtra("fullName")
                    bio = data.getStringExtra("bio")
                    website = data.getStringExtra("website")
                    println("$TAG profile pic=$profilePicUrl user name=$userName full name=$fullName")

                    // Set profile pic
                    if (profilePicUrl != null && !profilePicUrl!!.isEmpty()) {
                        // profile pic
                        Picasso.with(mActivity)
                                .load(profilePicUrl)
                                .placeholder(R.drawable.default_profile_image)
                                .error(R.drawable.default_profile_image)
                                .transform(CircleTransform())
                                .into(iV_profile_pic)

                        // set profile pic as cover pic
                        Glide.with(mActivity)
                                .load(profilePicUrl)
                                .transform(PositionedCropTransformation(mActivity, 0f, 0f))
                                .placeholder(R.drawable.default_profile_cover_pic)
                                .error(R.drawable.default_profile_cover_pic)
                                .into(iv_background)
                    } else {
                        iV_profile_pic!!.setImageResource(R.drawable.default_profile_image)
                        iv_background!!.setImageResource(R.drawable.default_profile_cover_pic)
                    }

                    // Full name
                    if (fullName != null)
                        tV_fullName!!.text = fullName

                    // set bio
                    if (bio != null && !bio!!.isEmpty()) {
                        tV_bio!!.visibility = View.VISIBLE
                        tV_bio!!.text = bio
                    }

                    // set website
                    if (website != null && !website!!.isEmpty()) {
                        tV_website!!.visibility = View.VISIBLE
                        tV_website!!.text = website
                    }
                }

                VariableConstants.GOOGLE_LOGIN_REQ_CODE -> {
                    verifyLoginWithGplus!!.onActivityResult(data)
                    if (googleVerified != null && googleVerified == "1")
                        iV_google_icon!!.setImageResource(R.drawable.gplus_verified_icon)

                    // set facebook verified
                    if (facebookVerified != null && facebookVerified == "1")
                        iV_fbicon!!.setImageResource(R.drawable.facebook_verified_icon)
                }

                // Paypal verified link
                //                case VariableConstants.PAYPAL_REQ_CODE:
                //                    boolean isPaypalVerified = data.getBooleanExtra("isPaypalVerified", false);
                //                    paypalUrl = data.getStringExtra("payPalLink");
                //                    if (isPaypalVerified) {
                //                        iV_paypal_icon.setVisibility(View.VISIBLE);
                //                        rL_verify_paypal.setVisibility(View.GONE);
                //                    }
                //                    break;

                // following count
                VariableConstants.FOLLOW_COUNT_REQ_CODE -> {
                    val intFollowCount = data.getIntExtra("followingCount", 0)
                    println("$TAG followingCount=$intFollowCount")
                    if (intFollowCount >= 0) {
                        followingCount = intFollowCount.toString()
                        tV_following_count!!.text = followingCount
                    }
                }
                STRIPE_ACCOUNT_ADDED_PROFILE -> {
                    isRefresh = true
                    getUserProfileDatas()
                }
            }//            showDialog();
            //            authViewModel.fetchBeauticationDetail(fetchBeauticationForm())
        }
    }

    // This methos is notified when any following count changes from Social Frag
    @Subscribe
    fun getMessage(follingCountObj: ProfileFollowingCount?) {
        if (follingCountObj != null && follingCountObj!!.followingCount >= 0) {
            println(TAG + " " + "followingCount=" + follingCountObj!!.followingCount)
            followingCount = (follingCountObj!!.followingCount).toString()
            tV_following_count!!.text = followingCount
        }
    }

    // This method is notified when paypal link verified from Connect paypal screen
    @Subscribe
    fun getMessage(getPaypalUrl: String) {
        //        System.out.println(TAG + " " + "get paypal url=" + getPaypalUrl);
        //        if (getPaypalUrl != null) {
        //            paypalUrl = getPaypalUrl;
        //            iV_paypal_icon.setVisibility(View.VISIBLE);
        //            rL_verify_paypal.setVisibility(View.GONE);
        //        }
    }

    @Subscribe
    fun getMessage(getProfileResultDatas: ProfileResultDatas?) {
        if (getProfileResultDatas != null) {
            profilePicUrl = getProfileResultDatas!!.profilePicUrl
            userName = getProfileResultDatas!!.username
            fullName = getProfileResultDatas!!.fullName
            bio = getProfileResultDatas!!.bio
            website = getProfileResultDatas!!.website
            println("$TAG profile pic=$profilePicUrl user name=$userName full name=$fullName")

            // Set profile pic
            if (profilePicUrl != null && !profilePicUrl!!.isEmpty()) {
                // profile pic
                Picasso.with(mActivity)
                        .load(profilePicUrl)
                        .placeholder(R.drawable.default_profile_image)
                        .error(R.drawable.default_profile_image)
                        .transform(CircleTransform())
                        .into(iV_profile_pic)

                // set profile pic as cover pic
                Glide.with(mActivity)
                        .load(profilePicUrl)
                        .transform(PositionedCropTransformation(mActivity, 0f, 0f))
                        .placeholder(R.drawable.default_profile_cover_pic)
                        .error(R.drawable.default_profile_cover_pic)
                        .into(iv_background)
            } else {
                iV_profile_pic!!.setImageResource(R.drawable.default_profile_image)
                iv_background!!.setImageResource(R.drawable.default_profile_cover_pic)
            }

            // Full name
            if (fullName != null)
                tV_fullName!!.text = fullName

            // set bio
            if (bio != null) {
                tV_bio!!.visibility = View.VISIBLE
                tV_bio!!.text = bio
            }

            // set website
            if (website != null) {
                tV_website!!.visibility = View.VISIBLE
                tV_website!!.text = website
            }
        }
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)

        isProfileFragVisible = isVisibleToUser
    }

    override fun onConnectionFailed(@NonNull connectionResult: ConnectionResult) {

    }

    override fun onDestroy() {
        super.onDestroy()
        BusProvider.instance.unregister(this)
        mGoogleApiClient!!.stopAutoManage(activity!!)
        mGoogleApiClient!!.disconnect()
    }

    companion object {
        private val TAG = ProfileFrag::class.java.simpleName
    }
}
