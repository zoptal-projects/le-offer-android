package com.zoptal.cellableapp.main.activity.products

import android.app.Activity
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.widget.*
import androidx.annotation.Nullable
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.adapter.ReportItemRvAdapter
import com.zoptal.cellableapp.fcm_push_notification.Config
import com.zoptal.cellableapp.fcm_push_notification.NotificationMessageDialog
import com.zoptal.cellableapp.fcm_push_notification.NotificationUtils.Companion.clearNotifications
import com.zoptal.cellableapp.main.activity.products.ReportProductActivity
import com.zoptal.cellableapp.pojo_class.PostItemReportPojo
import com.zoptal.cellableapp.pojo_class.report_product_pojo.ReportProductDatas
import com.zoptal.cellableapp.pojo_class.report_product_pojo.ReportProductMain
import com.zoptal.cellableapp.utility.*
import com.zoptal.cellableapp.utility.OkHttp3Connection.OkHttp3RequestCallback
import org.json.JSONException
import org.json.JSONObject
import java.util.*

/**
 * <h>ReportProductActivity</h>
 *
 *
 * In this class we used to show the list of report reason. Once
 * user will click on any item we show one optional box to write
 * his own comment. then once we click on submit. then it will
 * be submitted and current screen i.e activity will be finished.
 *
 * @since 06-Jun-17
 * @version 1.0
 * @author 3Embed
 */
class ReportProductActivity : AppCompatActivity(), View.OnClickListener {
    private var mActivity: Activity? = null
    private var mSessionManager: SessionManager? = null
    private var rL_rootElement: RelativeLayout? = null
    private var mProgressBar: ProgressBar? = null
    private var pBar_postReport: ProgressBar? = null
    private var rV_reportProduct: RecyclerView? = null
    private var postId = ""
    private var rL_submit: RelativeLayout? = null
    private var arrayListReportItem: ArrayList<ReportProductDatas>? = null
    private var tV_submit: TextView? = null
    private var mNotificationMessageDialog: NotificationMessageDialog? = null
    private var timer: Timer? = null
     override fun onCreate(@Nullable savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_report_product)
        overridePendingTransition(R.anim.slide_up, R.anim.stay)
        initVar()
    }

    /**
     * <h>initVar</h>
     *
     *
     * In this method we used to initialize all variables
     *
     */
    private fun initVar() {
        mActivity = this@ReportProductActivity
        timer = Timer()
        mNotificationMessageDialog = NotificationMessageDialog(mActivity)
        arrayListReportItem = ArrayList()
        rV_reportProduct = findViewById(R.id.rV_reportProduct) as RecyclerView?
        mSessionManager = SessionManager(mActivity!!)
        rL_rootElement = findViewById(R.id.rL_rootElement) as RelativeLayout?
        mProgressBar = findViewById(R.id.progress_bar_reportItem) as ProgressBar?
        pBar_postReport = findViewById(R.id.pBar_postReport) as ProgressBar?
        rL_submit = findViewById(R.id.rL_submit) as RelativeLayout?
        rL_submit!!.visibility = View.GONE
        rL_submit!!.setOnClickListener(this)
        tV_submit = findViewById(R.id.tV_submit) as TextView?

        // receiving data from last class
        val intent: Intent = getIntent()
        val product_image: String?
        val product_name: String?
        val sold_by_name: String?
        product_image = intent.getStringExtra("product_image")
        product_name = intent.getStringExtra("product_name")
        sold_by_name = intent.getStringExtra("sold_by_name")
        postId = intent.getStringExtra("postId")

        // Product image
        val iV_productImage = findViewById(R.id.iV_productImage) as ImageView
        iV_productImage.layoutParams.width = CommonClass.getDeviceWidth(mActivity!!) / 5
        iV_productImage.layoutParams.height = CommonClass.getDeviceWidth(mActivity!!) / 5
        if (product_image != null && !product_image.isEmpty()) Picasso.with(mActivity)
                .load(product_image)
                .transform(RoundedCornersTransform())
                .placeholder(R.drawable.default_image)
                .error(R.drawable.default_image)
                .into(iV_productImage)

        // product name
        val tV_productname = findViewById(R.id.tV_productname) as TextView
        if (product_name != null) tV_productname.text = product_name

        // sold by name
        val tV_posted_by = findViewById(R.id.tV_posted_by) as TextView
        if (sold_by_name != null) tV_posted_by.text = sold_by_name
        val rL_back_btn = findViewById(R.id.rL_back_btn) as RelativeLayout
        rL_back_btn.setOnClickListener(this)
        reportReasonAPi
    }

    override fun onResume() {
        super.onResume()

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.REGISTRATION_COMPLETE))

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.PUSH_NOTIFICATION))

        // clear the notification area when the app is opened
        clearNotifications(getApplicationContext())
    }

   override fun onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!)
        super.onPause()
    }

    /**
     * <h>GetReportReasonAPi</h>
     *
     *
     * In this method we used to do api call to get all report reasons. Once
     * we get than show that in recyclerview.
     *
     */
    private val reportReasonAPi: Unit
        private get() {
            if (CommonClass.isNetworkAvailable(mActivity!!)) {
                mProgressBar!!.visibility = View.VISIBLE
                val postReportReasonUrl = "" + ApiUrl.REPORT_POST_REASON + "?token=" + mSessionManager!!.authToken
                OkHttp3Connection.doOkHttp3Connection(TAG, postReportReasonUrl, OkHttp3Connection.Request_type.GET, JSONObject(), object : OkHttp3RequestCallback {
                     override fun onSuccess(result: String?, user_tag: String?) {
                        mProgressBar!!.visibility = View.GONE
                        println("$TAG report post reason res=$result")
                        val gson = Gson()
                        val reportProductMain = gson.fromJson(result, ReportProductMain::class.java)
                        when (reportProductMain.code) {
                            "200" -> {
                                arrayListReportItem!!.addAll(reportProductMain.data!!)
                                if (arrayListReportItem != null && arrayListReportItem!!.size > 0) {
                                    rL_submit!!.visibility = View.VISIBLE
                                    val reportItemRvAdapter = ReportItemRvAdapter(mActivity!!, arrayListReportItem!!)
                                    val linearLayoutManager = LinearLayoutManager(mActivity)
                                    rV_reportProduct!!.setLayoutManager(linearLayoutManager)
                                    rV_reportProduct!!.setAdapter(reportItemRvAdapter)
                                }
                            }
                            "401" -> CommonClass.sessionExpired(mActivity!!)
                            else -> {
                            }
                        }
                    }

                     override fun onError(error: String?, user_tag: String?) {
                        mProgressBar!!.visibility = View.GONE
                    }
                })
            } else CommonClass.showSnackbarMessage(rL_rootElement, getResources().getString(R.string.NoInternetAccess))
        }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.rL_back_btn -> onBackPressed()
            R.id.rL_submit -> {
                var isSelected = false
                if (arrayListReportItem!!.size > 0) {
                    for (reportProductDatas in arrayListReportItem!!) {
                        if (reportProductDatas.isItemSelected) {
                            isSelected = true
                            println(TAG + " " + "user report reason=" + reportProductDatas.reportReasonByUser)
                            reportPostApi(reportProductDatas._id, reportProductDatas.reportReasonByUser)
                        }
                    }
                } else {
                    Toast.makeText(this, getResources().getString(R.string.err_reason), Toast.LENGTH_SHORT).show()
                }
                if (!isSelected) {
                    Toast.makeText(this, getResources().getString(R.string.err_reason), Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    /**
     * <h>reportPostApi</h>
     *
     *
     * In this method we used to post the report to the server.
     *
     * @param reasonId The reasonId which we get in getReason api.
     * @param reasonByUser The reason written by user
     */
    private fun reportPostApi(reasonId: String, reasonByUser: String) {
        //token, postId, reason
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            tV_submit!!.visibility = View.GONE
            pBar_postReport!!.visibility = View.VISIBLE
            val requestDatas = JSONObject()
            try {
                requestDatas.put("token", mSessionManager!!.authToken)
                requestDatas.put("postId", postId)
                requestDatas.put("reasonId", reasonId)
                requestDatas.put("membername", mSessionManager!!.userName)
                requestDatas.put("description", reasonByUser)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            OkHttp3Connection.doOkHttp3Connection(TAG, ApiUrl.REPORT_POST, OkHttp3Connection.Request_type.POST, requestDatas, object : OkHttp3RequestCallback {
                 override fun onSuccess(result: String?, user_tag: String?) {
                    println("$TAG report reason=$result")
                    tV_submit!!.visibility = View.VISIBLE
                    pBar_postReport!!.visibility = View.GONE
                    val gson = Gson()
                    val itemReportPojo = gson.fromJson(result, PostItemReportPojo::class.java)
                    when (itemReportPojo.code) {
                        "200" -> {
                            CommonClass.showSuccessSnackbarMsg(rL_rootElement, getResources().getString(R.string.the_product_report_has_been))
                            timer!!.schedule(object : TimerTask() {
                                override fun run() {
                                    // when the task active then close the activity
                                    timer!!.cancel()
                                    onBackPressed()
                                }
                            }, 3000)
                        }
                        "401" -> CommonClass.sessionExpired(mActivity!!)
                        "409" -> {
                            CommonClass.showSnackbarMessage(rL_rootElement, getResources().getString(R.string.the_report_is_already_reported))
                            timer!!.schedule(object : TimerTask() {
                                override fun run() {
                                    // when the task active then close the activity
                                    timer!!.cancel()
                                    onBackPressed()
                                }
                            }, 3000)
                        }
                        else -> CommonClass.showSnackbarMessage(rL_rootElement, itemReportPojo.message)
                    }
                }

                 override fun onError(error: String?, user_tag: String?) {}
            })
        } else CommonClass.showSnackbarMessage(rL_rootElement, getResources().getString(R.string.NoInternetAccess))
    }

 override fun onBackPressed() {
        finish()
        overridePendingTransition(R.anim.stay, R.anim.slide_down)
    }

    companion object {
        private val TAG = ReportProductActivity::class.java.simpleName
    }
}