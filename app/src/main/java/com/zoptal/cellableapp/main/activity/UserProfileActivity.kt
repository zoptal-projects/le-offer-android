package com.zoptal.cellableapp.main.activity

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.view.Window
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.tabs.TabLayout
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.adapter.ViewPagerAdapter
import com.zoptal.cellableapp.fcm_push_notification.Config
import com.zoptal.cellableapp.fcm_push_notification.NotificationMessageDialog
import com.zoptal.cellableapp.fcm_push_notification.NotificationUtils.Companion.clearNotifications
import com.zoptal.cellableapp.main.activity.LandingActivity
import com.zoptal.cellableapp.main.activity.ReportUserActivity
import com.zoptal.cellableapp.main.activity.UserFollowingActivity
import com.zoptal.cellableapp.main.activity.UserProfileActivity
import com.zoptal.cellableapp.main.view_pager.my_profile_frag.SellingFrag
import com.zoptal.cellableapp.main.view_pager.my_profile_frag.SoldFrag
import com.zoptal.cellableapp.pojo_class.profile_pojo.ProfilePojoMain
import com.zoptal.cellableapp.utility.*
import com.zoptal.cellableapp.utility.OkHttp3Connection.OkHttp3RequestCallback
import org.json.JSONException
import org.json.JSONObject

/**
 * <h>ProfileFrag</h>
 *
 *
 * In this class we show the user profile description like user name, user profile
 * users total posts, following count, follower count etc
 *
 * @since 11-May-17
 */
class UserProfileActivity : AppCompatActivity(), View.OnClickListener {
    private var mActivity: Activity? = null
    private var profilePicUrl: String? = ""
    private var fullName: String? = ""
    private var membername: String? = ""
    private var followStatus: String? = ""
    private var phoneNumber = ""
    private var email = ""
    private var mApiCall: ApiCall? = null
    private var runTimePermission: RunTimePermission? = null
    private var  permissionsArray = arrayOf(Manifest.permission.CALL_PHONE)
    private var mSessionManager: SessionManager? = null
    private var isOwnUserFlag = false
    private var intFollowerCount = 0

    // Declare xml variables
    private var progress_bar_profile: ProgressBar? = null
    private var iV_profile_pic: ImageView? = null
    private var iv_background: ImageView? = null
    private var iV_fbicon: ImageView? = null
    private var iV_google_icon: ImageView? = null
    private var iV_email_icon: ImageView? = null
    private var iV_follow: ImageView? = null
//    private var iV_paypal_icon: ImageView? = null
    private var tV_posts_count: TextView? = null
    private var tV_follower_count: TextView? = null
    private var tV_following_count: TextView? = null
    private var tV_fullName: TextView? = null
    private var chatViewPager: ViewPager? = null
    private var coordinate_rootView: CoordinatorLayout? = null
    private var appBarLayout: AppBarLayout? = null
    private var rL_report_user: RelativeLayout? = null
//    private var rL_verify_paypal: RelativeLayout? = null
    private var mNotificationMessageDialog: NotificationMessageDialog? = null
    private var avgRating: String? = ""
    private var ratedBy: String? = ""
    private var ratingBar: RatingBar? = null
    private var tv_ratedBy: TextView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_user_profile)
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale)
        initVariables()
    }

    /**
     * <h>InitVariables</h>
     *
     *
     * In this method we used to initialize all variables.
     *
     */
    private fun initVariables() {
        mActivity = this@UserProfileActivity
        mNotificationMessageDialog = NotificationMessageDialog(mActivity)
        mSessionManager = SessionManager(mActivity!!)
        mApiCall = ApiCall(mActivity!!)
        coordinate_rootView = findViewById<View>(R.id.coordinate_rootView) as CoordinatorLayout
        appBarLayout = findViewById<View>(R.id.appBarLayout) as AppBarLayout
        appBarLayout!!.visibility = View.GONE
        ratingBar = findViewById<View>(R.id.ratingBar) as RatingBar
        tv_ratedBy = findViewById<View>(R.id.tV_ratedBy) as TextView

        runTimePermission = RunTimePermission(mActivity!!, permissionsArray, false)

        // Receiving datas from last class
        val intent = intent
        membername = intent.getStringExtra("membername")
        isOwnUserFlag = membername == mSessionManager!!.userName

        // Back button
        val rL_back_btn = findViewById<View>(R.id.rL_back_btn) as RelativeLayout
        rL_back_btn.setOnClickListener(this)
        progress_bar_profile = findViewById<View>(R.id.progress_bar_profile) as ProgressBar

        // profile pic
        iV_profile_pic = findViewById<View>(R.id.iV_profile_pic) as ImageView
        iV_profile_pic!!.layoutParams.width = CommonClass.getDeviceWidth(mActivity!!) / 4
        iV_profile_pic!!.layoutParams.height = CommonClass.getDeviceWidth(mActivity!!) / 4

        // background pic
        iv_background = findViewById<View>(R.id.iv_background) as ImageView
        iV_fbicon = findViewById<View>(R.id.iV_fbicon) as ImageView
        iV_google_icon = findViewById<View>(R.id.iV_google_icon) as ImageView
        iV_email_icon = findViewById<View>(R.id.iV_email_icon) as ImageView
        iV_follow = findViewById<View>(R.id.iV_follow) as ImageView
        iV_follow!!.setOnClickListener(this)
//        iV_paypal_icon = findViewById<View>(R.id.iV_paypal_icon) as ImageView
        val iV_call_mail = findViewById<View>(R.id.iV_call_mail) as ImageView
        iV_call_mail.setOnClickListener(this)

        // number of posts
        tV_posts_count = findViewById<View>(R.id.tV_posts_count) as TextView
        tV_follower_count = findViewById<View>(R.id.tV_follower_count) as TextView
        tV_following_count = findViewById<View>(R.id.tV_following_count) as TextView

        // Following
        val rL_following = findViewById<View>(R.id.rL_following) as RelativeLayout
        rL_following.setOnClickListener(this)

        // Follower
        val rL_follower = findViewById<View>(R.id.rL_follower) as RelativeLayout
        rL_follower.setOnClickListener(this)

        // User name and full name
        tV_fullName = findViewById<View>(R.id.tV_fullName) as TextView

        // report user
        rL_report_user = findViewById<View>(R.id.rL_report_user) as RelativeLayout
        rL_report_user!!.setOnClickListener(this)
//        rL_verify_paypal = findViewById<View>(R.id.rL_verify_paypal) as RelativeLayout
        // view pager
        chatViewPager = findViewById<View>(R.id.viewpager) as ViewPager
        val tabs = findViewById<View>(R.id.tabs) as TabLayout
        tabs.setupWithViewPager(chatViewPager)
        if (mSessionManager!!.isUserLoggedIn) getUserProfileDatas(ApiUrl.USER_PROFILE) else getUserProfileDatas(ApiUrl.GUEST_PROFILE)
    }

    override fun onResume() {
        super.onResume()

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.REGISTRATION_COMPLETE))

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.PUSH_NOTIFICATION))

        // clear the notification area when the app is opened
        clearNotifications(applicationContext)
    }

    override fun onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!)
        super.onPause()
    }

    /**
     * <h>GetUserProfileDatas</h>
     *
     *
     * In this method we do api call to get users profile complete description
     * Once we receive all datas then set all values to the respective fields.
     *
     */
    private fun getUserProfileDatas(apiUrl: String) {
        // token, limit, offset, membername
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            progress_bar_profile!!.visibility = View.VISIBLE
            val request_datas = JSONObject()
            try {
                request_datas.put("token", mSessionManager!!.authToken)
                request_datas.put("limit", "20")
                request_datas.put("offset", "0")
                request_datas.put("membername", membername)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            OkHttp3Connection.doOkHttp3Connection(TAG, apiUrl, OkHttp3Connection.Request_type.POST, request_datas, object : OkHttp3RequestCallback {
                 override fun onSuccess(result: String?, user_tag: String?) {
                    progress_bar_profile!!.visibility = View.GONE
                    println("$TAG  profile res=$result")
                    val profilePojoMain: ProfilePojoMain
                    val gson = Gson()
                    profilePojoMain = gson.fromJson(result, ProfilePojoMain::class.java)
                    when (profilePojoMain.code) {
                        "200" -> {

                            // Set my profile fields like image, name etc
                            val profileResponseDatas = profilePojoMain.data
                            if (profileResponseDatas != null && profileResponseDatas.size > 0) {
                                appBarLayout!!.visibility = View.VISIBLE
                                val noOfPost: String?
                                val followersCount: String?
                                val followingCount: String?
                                val googleVerified: String?
                                val facebookVerified: String?
                                val emailVerified: String?
                                profilePicUrl = profileResponseDatas[0].profilePicUrl
                                noOfPost = profileResponseDatas[0].posts
                                followersCount = profileResponseDatas[0].followers
                                followingCount = profileResponseDatas[0].following
                                if (followersCount != null && !followersCount.isEmpty()) intFollowerCount = followersCount.toInt()
                                googleVerified = profileResponseDatas[0].googleVerified
                                facebookVerified = profileResponseDatas[0].facebookVerified
                                emailVerified = profileResponseDatas[0].emailVerified
                                fullName = profileResponseDatas[0].fullName
                                followStatus = profileResponseDatas[0].followStatus
                                phoneNumber = profileResponseDatas[0].phoneNumber
                                email = profileResponseDatas[0].email
                                val paypalUrl = profileResponseDatas[0].paypalUrl
                                avgRating = profileResponseDatas[0].avgRating
                                ratedBy = profileResponseDatas[0].ratedBy
                                if (avgRating != null && !avgRating!!.isEmpty()) {
                                    ratingBar!!.visibility = View.VISIBLE //..visible while get rating
                                    ratingBar!!.rating = avgRating!!.toFloat()
                                }
                                if (ratedBy != null && !ratedBy!!.isEmpty()) {
                                    ratingBar!!.visibility = View.VISIBLE //..visible while get rating
                                    tv_ratedBy!!.visibility = View.VISIBLE //..visible while get rating
                                    ratedBy = "($ratedBy)"
                                    tv_ratedBy!!.text = ratedBy
                                }
                                if (membername == mSessionManager!!.userName) {
                                    rL_report_user!!.visibility = View.GONE
                                }
                                println("$TAG followStatus=$followStatus")

                                // Set profile pic
                                if (profilePicUrl != null && !profilePicUrl!!.isEmpty()) {
                                    Picasso.with(mActivity)
                                            .load(profilePicUrl)
                                            .placeholder(R.drawable.default_profile_image)
                                            .error(R.drawable.default_profile_image)
                                            .transform(CircleTransform())
                                            .into(iV_profile_pic)
                                    Glide.with(mActivity)
                                            .load(profilePicUrl)
                                            .transform(PositionedCropTransformation(mActivity, 0f, 0f))
                                            .placeholder(R.drawable.default_profile_cover_pic)
                                            .error(R.drawable.default_profile_cover_pic)
                                            .into(iv_background)
                                }

                                // set facebook verified icon
                                if (facebookVerified != null && facebookVerified == "1") iV_fbicon!!.setImageResource(R.drawable.facebook_verified_icon)

                                // set google plus verified icon
                                if (googleVerified != null && googleVerified == "1") iV_google_icon!!.setImageResource(R.drawable.gplus_verified_icon)

                                // set email verified iocn
                                if (emailVerified != null && emailVerified == "1") iV_email_icon!!.setImageResource(R.drawable.email_verified_icon)

                                // set follow or following icon
                                if (followStatus != null && followStatus == "1") iV_follow!!.setImageResource(R.drawable.followed_icon) else iV_follow!!.setImageResource(R.drawable.follow_icon)

                                // set number of posts
                                if (noOfPost != null) tV_posts_count!!.text = noOfPost

                                // Followers count
                                if (followersCount != null) tV_follower_count!!.text = followersCount

                                // Following count
                                if (followingCount != null) tV_following_count!!.text = followingCount

                                // Full name
                                if (fullName != null) tV_fullName!!.text = fullName

                                // set payal verified
//                                if (paypalUrl != null && paypalUrl != "") {
//                                    iV_paypal_icon!!.visibility = View.VISIBLE
//                                    rL_verify_paypal!!.visibility = View.GONE
//                                } else {
//                                    iV_paypal_icon!!.visibility = View.GONE
//                                    rL_verify_paypal!!.visibility = View.VISIBLE
//                                }
                            }
                            setupViewPager()
                        }
                        "401" -> CommonClass.sessionExpired(mActivity!!)
                        else -> CommonClass.showTopSnackBar(coordinate_rootView, profilePojoMain.message)
                    }
                }

                 override fun onError(error: String?, user_tag: String?) {
                    progress_bar_profile!!.visibility = View.GONE
                    CommonClass.showTopSnackBar(coordinate_rootView, error)
                }
            })
        } else CommonClass.showTopSnackBar(coordinate_rootView, resources.getString(R.string.NoInternetAccess))
    }

    /**
     * <h>SetupViewPager</h>
     *
     *
     * In this method we used to set viewpager with respective fragments.
     *
     */
    private fun setupViewPager() {
        val adapter = ViewPagerAdapter(supportFragmentManager)
        adapter.addFragment(SellingFrag.newInstance(membername, isOwnUserFlag), resources.getString(R.string.selling))
        adapter.addFragment(SoldFrag.newInstance(membername, isOwnUserFlag), resources.getString(R.string.sold))
        chatViewPager!!.adapter = adapter
    }

    override fun onBackPressed() {
        val intent = Intent()
        intent.putExtra("followStatus", followStatus)
        setResult(VariableConstants.USER_FOLLOW_REQ_CODE, intent)
        finish()
        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate)
    }

    override fun onClick(v: View) {
        val intent: Intent
        when (v.id) {
            R.id.rL_back_btn -> onBackPressed()
            R.id.rL_report_user -> if (mSessionManager!!.isUserLoggedIn) {
                intent = Intent(mActivity, ReportUserActivity::class.java)
                intent.putExtra("userImage", profilePicUrl)
                intent.putExtra("userName", membername)
                intent.putExtra("userFullName", fullName)
                startActivity(intent)
            } else startActivityForResult(Intent(mActivity, LandingActivity::class.java), VariableConstants.LANDING_REQ_CODE)
            R.id.rL_following -> {
                intent = Intent(mActivity, UserFollowingActivity::class.java)
                intent.putExtra("title", resources.getString(R.string.Following))
                intent.putExtra("isFollower", false)
                intent.putExtra("membername", membername)
                startActivity(intent)
            }
            R.id.rL_follower -> {
                intent = Intent(mActivity, UserFollowingActivity::class.java)
                intent.putExtra("title", resources.getString(R.string.Followers))
                intent.putExtra("isFollower", true)
                intent.putExtra("membername", membername)
                intent.putExtra("followerCount", intFollowerCount)
                startActivityForResult(intent, VariableConstants.FOLLOW_COUNT_REQ_CODE)
            }
            R.id.iV_call_mail -> callOrEmailDialog()
            R.id.iV_follow -> if (mSessionManager!!.isUserLoggedIn) {
                if (CommonClass.isNetworkAvailable(mActivity!!)) {
                    val url: String
                    if (followStatus != null && followStatus == "1") {
                        url = ApiUrl.UNFOLLOW + membername
                        unfollowUserAlert(url)
                    } else {
                        url = ApiUrl.FOLLOW + membername
                        mApiCall!!.followUserApi(url)
                        iV_follow!!.setImageResource(R.drawable.followed_icon)
                        followStatus = "1"
                        intFollowerCount = intFollowerCount + 1
                        if (intFollowerCount >= 0) tV_follower_count!!.text = intFollowerCount.toString()
                    }
                } else {
                    CommonClass.showSnackbarMessage(coordinate_rootView, resources.getString(R.string.NoInternetAccess))
                }
            } else startActivityForResult(Intent(mActivity, LandingActivity::class.java), VariableConstants.LANDING_REQ_CODE)
        }
    }

    /**
     * <h>unfollowUserAlert</h>
     *
     *
     * In this method we used to open a simple dialog pop-up to show
     * alert to unfollow
     *
     */
    fun unfollowUserAlert(url: String?) {
        val unfollowUserDialog = Dialog(mActivity!!)
        unfollowUserDialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        unfollowUserDialog.setContentView(R.layout.dialog_unfollow_user)
        unfollowUserDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        unfollowUserDialog.window!!.setLayout((CommonClass.getDeviceWidth(mActivity!!) * 0.9).toInt(), RelativeLayout.LayoutParams.WRAP_CONTENT)

        // set user pic
        val imageViewPic = unfollowUserDialog.findViewById<View>(R.id.iV_userPic) as ImageView
        imageViewPic.layoutParams.width = CommonClass.getDeviceWidth(mActivity!!) / 5
        imageViewPic.layoutParams.height = CommonClass.getDeviceWidth(mActivity!!) / 5

        // posted by pic
        if (profilePicUrl != null && !profilePicUrl!!.isEmpty()) Picasso.with(mActivity)
                .load(profilePicUrl)
                .transform(CircleTransform())
                .placeholder(R.drawable.default_profile_image)
                .error(R.drawable.default_profile_image)
                .into(imageViewPic)

        // set user name
        val tV_userName = unfollowUserDialog.findViewById<View>(R.id.tV_userName) as TextView
        if (membername != null && !membername!!.isEmpty()) {
            val setUserName = resources.getString(R.string.at_the_rate) + membername + resources.getString(R.string.question_mark)
            tV_userName.text = setUserName
        }

        // set cancel button
        val tV_cancel = unfollowUserDialog.findViewById<View>(R.id.tV_cancel) as TextView
        tV_cancel.setOnClickListener { unfollowUserDialog.dismiss() }

        // set done button
        val tV_unfollow = unfollowUserDialog.findViewById<View>(R.id.tV_unfollow) as TextView
        tV_unfollow.setOnClickListener {
            mApiCall!!.followUserApi(url!!)
            iV_follow!!.setImageResource(R.drawable.follow_icon)
            followStatus = "0"
            intFollowerCount = intFollowerCount - 1
            tV_follower_count!!.text = intFollowerCount.toString()
            unfollowUserDialog.dismiss()
        }
        unfollowUserDialog.show()
    }

    /**
     * In this method we open one simple Dialog pop-up to show
     * option like call or mail to the the user
     */
    fun callOrEmailDialog() {
        val showOptionDialog = Dialog(mActivity!!)
        showOptionDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        showOptionDialog.window!!.setGravity(Gravity.BOTTOM)
        showOptionDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        showOptionDialog.setContentView(R.layout.dialog_call_mail)
        showOptionDialog.window!!.attributes.windowAnimations = R.style.DialogAnimation
        showOptionDialog.window!!.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT)

        // call
        val rL_call = showOptionDialog.findViewById<View>(R.id.rL_call) as RelativeLayout
        rL_call.setOnClickListener {
            callUser()
            showOptionDialog.dismiss()
        }

        // mail
        val rL_mail = showOptionDialog.findViewById<View>(R.id.rL_mail) as RelativeLayout
        rL_mail.setOnClickListener { CommonClass.sendEmail(mActivity!!, email, mSessionManager!!.userName!!, "") }

        // cancel button
        val cancel_button = showOptionDialog.findViewById<View>(R.id.cancel_button) as TextView
        cancel_button.setOnClickListener { showOptionDialog.dismiss() }
        showOptionDialog.show()
    }

    private fun callUser() {
        val callIntent = Intent(Intent.ACTION_CALL)
        val mobileNo = "tel:$phoneNumber"
        println("$TAG mob no=$mobileNo")
        callIntent.data = Uri.parse(mobileNo)
        if (runTimePermission!!.checkPermissions(permissionsArray)) startActivity(callIntent!!) else runTimePermission!!.requestPermission()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            VariableConstants.PERMISSION_REQUEST_CODE -> {
                println("grant result=" + grantResults.size)
                if (grantResults.size > 0) {
                    var count = 0
                    while (count < grantResults.size) {
                        if (grantResults[count] != PackageManager.PERMISSION_GRANTED) runTimePermission!!.allowPermissionAlert(permissions[count])
                        count++
                    }
                    if (runTimePermission!!.checkPermissions(permissionsArray)) {
                        callUser()
                    }
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (data != null) {
            when (requestCode) {
                VariableConstants.LANDING_REQ_CODE -> {
                    val isToRefreshHomePage = data.getBooleanExtra("isToRefreshHomePage", true)
                    println("$TAG isToRefreshHomePage=$isToRefreshHomePage")
                    if (isToRefreshHomePage && mSessionManager!!.userName == membername) {
                        val intent = Intent(mActivity, SelfProfileActivity::class.java)
                        intent.putExtra("membername", membername)
                        startActivity(intent)
                        finish()
                    }
                    val isFromSignup = data.getBooleanExtra("isFromSignup", false)

                    // open start browsering screen
                    if (isFromSignup) DialogBox(mActivity!!).startBrowsingDialog()
                }
                VariableConstants.FOLLOW_COUNT_REQ_CODE -> println(TAG + " " + "followerCount=" + data.getIntExtra("followerCount", 0))
            }
        }
    }

    companion object {
        private val TAG = UserProfileActivity::class.java.simpleName
    }
}