package com.zoptal.cellableapp.main.view_pager.notification_frag

import android.app.Activity
import android.content.Intent
import android.os.Bundle

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.RelativeLayout
import androidx.annotation.Nullable
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout

import com.google.gson.Gson
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.adapter.YouFragRvAdapter
import com.zoptal.cellableapp.main.activity.NotificationActivity
import com.zoptal.cellableapp.main.activity.NotificationActivity.FragmentRefreshListener
import com.zoptal.cellableapp.main.camera_kit.CameraKitActivity
import com.zoptal.cellableapp.pojo_class.explore_for_you_pojo.ForYouMainPojo
import com.zoptal.cellableapp.pojo_class.explore_for_you_pojo.ForYouResposeDatas
import com.zoptal.cellableapp.utility.ApiUrl
import com.zoptal.cellableapp.utility.CommonClass
import com.zoptal.cellableapp.utility.OkHttp3Connection
import com.zoptal.cellableapp.utility.OkHttp3Connection.OkHttp3RequestCallback
import com.zoptal.cellableapp.utility.SessionManager
import org.json.JSONException
import org.json.JSONObject
import java.util.*

/**
 * <h>YouFrag</h>
 *
 *
 * This class is called from ExploreNotificationActivity class. In this
 * Fragment class we used we used to show self notifications like abc
 * started following you.
 *
 * @since 4/15/2017
 * @author 3Embed
 * @version 1.0
 */
class YouFrag : Fragment(), View.OnClickListener {
    private var mActivity: Activity? = null
    private var mSessionManager: SessionManager? = null
    private var followingFragRvAdap: YouFragRvAdapter? = null
    private var al_selfActivity_data: ArrayList<ForYouResposeDatas>? = null

    // xml variables
    private var progress_bar_notification: ProgressBar? = null
    private var rV_notification: RecyclerView? = null
    private var swipe_refresh_layout: SwipeRefreshLayout? = null
    private var rL_rootview: RelativeLayout? = null
    private var linear_no_activity: LinearLayout? = null

    // Load more variables
    private var index = 0
    private var totalVisibleItem = 0
    private var totalItemCount = 0
    private val visibleThresold = 5
    private var isLoaded = false
    override fun onCreate(@Nullable savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        mActivity = getActivity()
        mSessionManager = SessionManager(mActivity!!)
        index = 0
    }

    @Nullable
    override fun onCreateView(inflater: LayoutInflater, @Nullable container: ViewGroup?, @Nullable savedInstanceState: Bundle?): View {
        val view = inflater.inflate(R.layout.frag_for_you_notification, container, false)
        initVariables(view)
        (getActivity() as NotificationActivity).fragmentRefreshListener = object : FragmentRefreshListener {
            override fun onRefresh() {
                al_selfActivity_data!!.clear()
                selfActivityApi(0)
                // Refresh Your Fragment
            }
        }
        return view
    }

    /**
     * <h>initVariables</h>
     *
     *
     * In this method we initialize the xml all variables.
     *
     * @param view The parent view
     */
    private fun initVariables(view: View) {
        al_selfActivity_data = ArrayList()
        rL_rootview = view.findViewById<View>(R.id.rL_rootview) as RelativeLayout
        val rL_start_selling = view.findViewById<View>(R.id.rL_start_selling) as RelativeLayout
        rL_start_selling.setOnClickListener(this)
        swipe_refresh_layout = view.findViewById<View>(R.id.swipe_refresh_layout) as SwipeRefreshLayout
        swipe_refresh_layout!!.setColorSchemeResources(R.color.pink_color)
        progress_bar_notification = view.findViewById<View>(R.id.progress_bar_notification) as ProgressBar
        rV_notification = view.findViewById<View>(R.id.rV_notification) as RecyclerView
        followingFragRvAdap = YouFragRvAdapter(mActivity!!!!, al_selfActivity_data!!)
        val layoutManager = LinearLayoutManager(mActivity!!)
        rV_notification!!.setLayoutManager(layoutManager)
        rV_notification!!.setAdapter(followingFragRvAdap!!)
        linear_no_activity = view.findViewById<View>(R.id.linear_no_activity) as LinearLayout
        linear_no_activity!!.visibility = View.GONE

        // call self activity api
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            progress_bar_notification!!.visibility = View.VISIBLE
            selfActivityApi(index)
        }

        // pull to refresh
        swipe_refresh_layout!!.setOnRefreshListener(object : SwipeRefreshLayout.OnRefreshListener {
           override fun onRefresh() {
                index = 0
                al_selfActivity_data!!.clear()
                selfActivityApi(index)
            }
        })
    }

    /**
     * <h>SelfActivityApi</h>
     *
     *
     * In this method we do api selfActivityApi call to get  all
     * self notifications.
     *
     * @param offsetValue The page index
     */
    private fun selfActivityApi(offsetValue: Int) {
        var offsetValue = offsetValue
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            val limit = 20
            offsetValue = offsetValue * limit
            val request_data = JSONObject()
            try {
                request_data.put("token", mSessionManager!!.authToken)
                request_data.put("offset", offsetValue)
                request_data.put("limit", limit)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            println("$TAG index in self activity=$offsetValue")
            OkHttp3Connection.doOkHttp3Connection(TAG, ApiUrl.SELF_ACTIVITY, OkHttp3Connection.Request_type.POST, request_data, object : OkHttp3RequestCallback {
                 override fun onSuccess(result: String?, user_tag: String?) {
                    progress_bar_notification!!.visibility = View.GONE
                    println("$TAG self Activity res=$result")
                    val forYouPojo: ForYouMainPojo
                    val gson = Gson()
                    forYouPojo = gson.fromJson(result, ForYouMainPojo::class.java)
                    when (forYouPojo.code) {
                        "200" -> {
                            (mActivity!! as NotificationActivity?)!!.isNotificationSeen = true
                            swipe_refresh_layout!!.setRefreshing(false)
                            al_selfActivity_data!!.addAll(forYouPojo.data!!)
                            if (al_selfActivity_data != null && al_selfActivity_data!!.size > 0) {
                                followingFragRvAdap!!.notifyDataSetChanged()
                                //isLoaded=false;
                                isLoaded = al_selfActivity_data!!.size < 15
                                val layoutManager: LinearLayoutManager = rV_notification!!.getLayoutManager() as LinearLayoutManager
                                // Load more
                                rV_notification!!.addOnScrollListener(object : RecyclerView.OnScrollListener() {

                                   override  fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                                        super.onScrolled(recyclerView, dx, dy)
                                        totalItemCount = layoutManager.getItemCount()
                                        totalVisibleItem = layoutManager.findLastVisibleItemPosition()
                                        println("$TAG you frag totalItemCount=$totalItemCount lastVisibleItem=$totalVisibleItem visibleThreshold=$visibleThresold is load more=$isLoaded")
                                        if (!isLoaded && totalItemCount <= visibleThresold + totalVisibleItem) {
                                            println("$TAG on load more called")
                                            swipe_refresh_layout!!.setRefreshing(true)
                                            isLoaded = true
                                            index = index + 1
                                            selfActivityApi(index)
                                        }
                                    }
                                })
                            } else linear_no_activity!!.visibility = View.VISIBLE
                        }
                        "401" -> CommonClass.sessionExpired(mActivity!!)
                        else -> swipe_refresh_layout!!.setRefreshing(false)
                    }
                }

                 override fun onError(error: String?, user_tag: String?) {
                    progress_bar_notification!!.visibility = View.GONE
                    CommonClass.showSnackbarMessage(rL_rootview, error)
                }
            })
        } else CommonClass.showSnackbarMessage(rL_rootview, getResources().getString(R.string.NoInternetAccess))
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.rL_start_selling ->                 /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                    startActivity(new Intent(mActivity!!, Camera2Activity.class));
                else*/startActivity(Intent(mActivity!!, CameraKitActivity::class.java))
        }
    }

    companion object {
        private val TAG = FollowingFrag::class.java.simpleName
    }
}