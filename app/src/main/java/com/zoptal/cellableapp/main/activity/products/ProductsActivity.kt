package com.zoptal.cellableapp.main.activity.products

import android.app.Activity
import android.content.IntentFilter
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.viewpager.widget.ViewPager
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.adapter.ProductDetailPagerAdap
import com.zoptal.cellableapp.fcm_push_notification.Config
import com.zoptal.cellableapp.fcm_push_notification.NotificationMessageDialog
import com.zoptal.cellableapp.fcm_push_notification.NotificationUtils.Companion.clearNotifications
import com.zoptal.cellableapp.pojo_class.home_explore_pojo.ExploreResponseDatas
import java.util.*

/*
* In this activity product detail page showing
* Using View Pager For swipe to go next product
* */
class ProductsActivity : AppCompatActivity() {
    private var arrayListExploreDatas: ArrayList<ExploreResponseDatas>? = null
    private var viewPagerDetail: ViewPager? = null
    var productDetailPagerAdap: ProductDetailPagerAdap? = null
    private var position = 0
    private var mActivity: Activity? = null
    private var mNotificationMessageDialog: NotificationMessageDialog? = null
    override fun onCreate(savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_products)
        if (getIntent() != null) {
            arrayListExploreDatas = getIntent().getSerializableExtra("arrayListExploreDatas") as ArrayList<ExploreResponseDatas>?
            position = getIntent().getIntExtra("position", 0)
        }
        mActivity = this@ProductsActivity
        mNotificationMessageDialog = NotificationMessageDialog(mActivity)
        viewPagerDetail = findViewById(R.id.viewPager_detail) as ViewPager?
        productDetailPagerAdap = ProductDetailPagerAdap(getSupportFragmentManager(), arrayListExploreDatas!!, this)
        viewPagerDetail!!.setAdapter(productDetailPagerAdap)
        viewPagerDetail!!.setCurrentItem(position, true)
    }

    override fun onResume() {
        super.onResume()

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.REGISTRATION_COMPLETE))

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.PUSH_NOTIFICATION))

        // clear the notification area when the app is opened
        clearNotifications(getApplicationContext())
    }
}