package com.zoptal.cellableapp.main.activity

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.MediaScannerConnection
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.View
import android.widget.*
import androidx.annotation.Nullable
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SwitchCompat
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.Uploader.FileUploader
import com.zoptal.cellableapp.Uploader.ProductImageDatas
import com.zoptal.cellableapp.Uploader.UploadedCallback
import com.zoptal.cellableapp.adapter.UpdatePostIWantAdapter
import com.zoptal.cellableapp.adapter.UpdatePostRvAdapter
import com.zoptal.cellableapp.main.activity.ChangeLocationActivity
import com.zoptal.cellableapp.main.activity.CurrencyListActivity
import com.zoptal.cellableapp.main.activity.HomePageActivity
import com.zoptal.cellableapp.main.activity.ProductCategoryActivity
import com.zoptal.cellableapp.main.activity.UpdatePostActivity
import com.zoptal.cellableapp.mqttchat.AppController
import com.zoptal.cellableapp.pojo_class.product_category.FilterKeyValue
import com.zoptal.cellableapp.pojo_class.product_details_pojo.PostFilter
import com.zoptal.cellableapp.pojo_class.product_details_pojo.SwapPost
import com.zoptal.cellableapp.pojo_class.update_product_pojo.UpdateProductMainPojo
import com.zoptal.cellableapp.recyleview_drag_drop.OnCustomerListChangedListener
import com.zoptal.cellableapp.recyleview_drag_drop.OnStartDragListener
import com.zoptal.cellableapp.recyleview_drag_drop.SimpleItemTouchHelperCallback
import com.zoptal.cellableapp.utility.*
import com.zoptal.cellableapp.utility.OkHttp3Connection.OkHttp3RequestCallback
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList

/**
 * <h>UpdatePostActivity</h>
 *
 *
 * In this class we used to provide options to user for modifications for
 * the posted products like Title, description or prices etc.
 *
 *
 * @author 3Embed.
 * @since 28-Aug-17.
 */
class UpdatePostActivity : AppCompatActivity(), View.OnClickListener, OnCustomerListChangedListener, OnStartDragListener {
    var arrayListImgPath: ArrayList<String>? = null
    var isNewUpload = false
    var postFilters: ArrayList<PostFilter>? = ArrayList()
    private var mActivity: Activity? = null
    private var eT_title: EditText? = null
    private var eT_description: EditText? = null
    private var eT_price: EditText? = null
    private var tV_category: TextView? = null
    private var tV_condition: TextView? = null
    private var tV_currency: TextView? = null
    private var tV_currency_symbol: TextView? = null
    private var tV_current_location: TextView? = null
    private var btn_cancel: TextView? = null
    private var postId: String? = ""
    private var productName: String? = ""
    private var description: String? = ""
    private var category: String? = ""
    private var condition: String? = ""
    private var price: String? = ""
    private var currency: String? = ""
    private var negotiable: String? = ""
    private var place: String? = ""
    private var latitude: String? = ""
    private var longitude: String? = ""
    private var city: String? = ""
    private var countrySname = ""
    private var aLProductImageDatases: ArrayList<ProductImageDatas?>? = null
    private var aLUpdateProductImage: ArrayList<ProductImageDatas?>? = null
    private var imagesHorizontalRvAdap: UpdatePostRvAdapter? = null

    //private ProgressBar progress_bar_post;
    private var rL_rootElement: RelativeLayout? = null
    private var mSessionManager: SessionManager? = null
    private var rotationAngles: ArrayList<Int>? = null
    private var mDialogBox: DialogBox? = null
    private var subCategory: String? = ""
    private var filterKeyValues: ArrayList<FilterKeyValue>? = null

    // exchanges elements
    private var willingtoExchangeLl: RelativeLayout? = null
    private var iWantRv: RecyclerView? = null
    private var iwantAdapter: UpdatePostIWantAdapter? = null
    private val arrayListPostIds = ArrayList<String>()
    private var isSwap = 0
    private var swapPostArrayList: ArrayList<SwapPost>? = ArrayList()
    private var layoutManager: LinearLayoutManager? = null
    private var mItemTouchHelper: ItemTouchHelper? = null
    override fun onCreate(@Nullable savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_post_product)
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale)
        initVariables()
    }

    /**
     * <h>InitVariables</h>
     *
     *
     * In this method we used to initialize all variables.
     *
     */
    private fun initVariables() {
        mActivity = this@UpdatePostActivity
        mDialogBox = DialogBox(mActivity!!)
        mSessionManager = SessionManager(mActivity!!)
        arrayListImgPath = ArrayList()
        rotationAngles = ArrayList()
        aLUpdateProductImage = ArrayList()
        filterKeyValues = ArrayList()
        rL_rootElement = findViewById(R.id.rL_rootElement) as RelativeLayout?
        val tV_change_loc = findViewById(R.id.tV_change_loc) as TextView
        tV_change_loc.setOnClickListener(this)
        val linear_share = findViewById(R.id.linear_share) as LinearLayout
        linear_share.visibility = View.GONE
        val tV_actionBarTitle = findViewById(R.id.tV_actionBarTitle) as TextView
        tV_actionBarTitle.setText(getResources().getString(R.string.update_post))

        // receive datas
        val bundlePostItem: Bundle = getIntent().getExtras()!!
        if (bundlePostItem != null) {
            postId = bundlePostItem.getString("postId")
            productName = bundlePostItem.getString("productName")
            description = bundlePostItem.getString("description")
            category = bundlePostItem.getString("category")
            subCategory = bundlePostItem.getString("subCategory")
            postFilters = bundlePostItem.getSerializable("information") as ArrayList<PostFilter>?
            condition = bundlePostItem.getString("condition")
            price = bundlePostItem.getString("price")
            currency = bundlePostItem.getString("currency")
            negotiable = bundlePostItem.getString("negotiable")
            place = bundlePostItem.getString("place")
            latitude = bundlePostItem.getString("latitude")
            longitude = bundlePostItem.getString("longitude")
            isSwap = bundlePostItem.getInt("isSwap", 0)
            if (isSwap == 1) {
                swapPostArrayList = bundlePostItem["swapPostList"] as ArrayList<SwapPost>?
                val swapPost = SwapPost()
                swapPost.itemType = true
                swapPostArrayList!!.add(swapPost)
            }
            if (isLocationFound(latitude, longitude)) {
                city = CommonClass.getCityName(mActivity!!, latitude!!.toDouble(), longitude!!.toDouble())
                countrySname = CommonClass.getCountryCode(mActivity!!, latitude!!.toDouble(), longitude!!.toDouble())
            }
            println("$TAG place=$place city=$city countrySname=$countrySname")
        }

        // exchanges elements
        iWantRv = findViewById(R.id.iWantRv) as RecyclerView?
        willingtoExchangeLl = findViewById(R.id.willingtoExchangeLl) as RelativeLayout?
        iwantAdapter = UpdatePostIWantAdapter(swapPostArrayList!!, this)
        layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        iWantRv!!.setLayoutManager(layoutManager)
        iWantRv!!.setAdapter(iwantAdapter)

        //cancel button
        btn_cancel = findViewById(R.id.btn_cancel) as TextView?
        btn_cancel!!.setOnClickListener(this)

        //add filter data to keyValue list which is need to send while updatepost
        if (postFilters != null && postFilters!!.size > 0) {
            for (p in postFilters!!) {
                if(p.fieldName!=null) {
                    filterKeyValues!!.add(FilterKeyValue(p.fieldName, p.values))
                }
            }
        }

        // title
        eT_title = findViewById(R.id.eT_title) as EditText?
        if (productName != null && !productName!!.isEmpty()) eT_title!!.setText(productName)

        // Description
        eT_description = findViewById(R.id.eT_description) as EditText?
        if (description != null && !description!!.isEmpty()) eT_description!!.setText(description)

        // price
        eT_price = findViewById(R.id.eT_price) as EditText?
        if (price != null && !price!!.isEmpty()) eT_price!!.setText(price)

        // Back button
        val rL_back_btn: RelativeLayout
        val rL_product_category: RelativeLayout
        val rL_conditions: RelativeLayout
        val rL_currency: RelativeLayout
        rL_back_btn = findViewById(R.id.rL_back_btn) as RelativeLayout
        rL_back_btn.setOnClickListener(this)

        // category
        rL_product_category = findViewById(R.id.rL_product_category) as RelativeLayout
        rL_product_category.setOnClickListener(this)
        println("$TAG category=$category")
        tV_category = findViewById(R.id.tV_category) as TextView?
        if (category != null && !category!!.isEmpty()) tV_category!!.text = category

        // condition
        rL_conditions = findViewById(R.id.rL_conditions) as RelativeLayout
        rL_conditions.setOnClickListener(this)
        tV_condition = findViewById(R.id.tV_condition) as TextView?
        if (condition != null && !condition!!.isEmpty()) tV_condition!!.text = condition

        // currency
        rL_currency = findViewById(R.id.rL_currency) as RelativeLayout
        rL_currency.setOnClickListener(this)
        tV_currency = findViewById(R.id.tV_currency) as TextView?
        if (currency != null && !currency!!.isEmpty()) tV_currency!!.text = currency

        // currency symbol
        tV_currency_symbol = findViewById(R.id.tV_currency_symbol) as TextView?

        // set place
        tV_current_location = findViewById(R.id.tV_current_location) as TextView?
        if (place != null && !place!!.isEmpty()) {
            println("$TAG place 2 =$place")
            tV_current_location!!.visibility = View.VISIBLE
            tV_current_location!!.text = place
        }

        // disable fetching location progress bar
        val progress_bar_location = findViewById(R.id.progress_bar_location) as ProgressBar
        progress_bar_location.visibility = View.GONE

        // switch negotiable
        val switch_negotiable: SwitchCompat = findViewById(R.id.switch_negotiable) as SwitchCompat
        if (negotiable != null && negotiable == "1") switch_negotiable.setChecked(true) else switch_negotiable.setChecked(false)
        switch_negotiable.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { buttonView, isChecked -> negotiable = if (isChecked) "1" else "0" })

        // switch exchange
        val switch_exchange: SwitchCompat = findViewById(R.id.switch_exchange) as SwitchCompat
        if (isSwap == 1) {
            switch_exchange.setChecked(true)
            willingtoExchangeLl!!.visibility = View.VISIBLE
        } else {
            switch_exchange.setChecked(false)
            willingtoExchangeLl!!.visibility = View.GONE
        }
        switch_exchange.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                willingtoExchangeLl!!.visibility = View.VISIBLE
                startWillingtoExchageActivity()
                isSwap = 1
            } else {
                swapPostArrayList!!.clear()
                iwantAdapter!!.notifyDataSetChanged()
                willingtoExchangeLl!!.visibility = View.GONE
                isSwap = 0
            }
        })
        aLProductImageDatases = bundlePostItem.getSerializable("imageDatas") as ArrayList<ProductImageDatas?>?
        if (aLProductImageDatases != null && aLProductImageDatases!!.size > 0) aLUpdateProductImage!!.addAll(aLProductImageDatases!!)

        // add more images text
        val tV_add_more_image = findViewById(R.id.tV_add_more_image) as TextView

        // set adpter for horizontal images
        if (aLProductImageDatases!!.size > 0) {
            when (aLProductImageDatases!!.size) {
                1 -> tV_add_more_image.setText(getResources().getString(R.string.add_upto_4_more_img))
                2 -> tV_add_more_image.setText(getResources().getString(R.string.add_upto_3_more_img))
                3 -> tV_add_more_image.setText(getResources().getString(R.string.add_upto_2_more_img))
                4 -> tV_add_more_image.setText(getResources().getString(R.string.add_upto_1_more_img))
                5 -> tV_add_more_image.visibility = View.GONE
            }

            // set image recycler view adapter
            imagesHorizontalRvAdap = UpdatePostRvAdapter(mActivity!!, aLProductImageDatases!!, aLUpdateProductImage!!, tV_add_more_image, this, this)
            val rV_cameraImages: RecyclerView = findViewById(R.id.rV_cameraImages) as RecyclerView
            val linearLayoutManager = LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false)
            rV_cameraImages.setLayoutManager(linearLayoutManager)
            val callback: ItemTouchHelper.Callback = SimpleItemTouchHelperCallback(imagesHorizontalRvAdap!!)
            mItemTouchHelper = ItemTouchHelper(callback)
            mItemTouchHelper!!.attachToRecyclerView(rV_cameraImages)
            rV_cameraImages.setAdapter(imagesHorizontalRvAdap)
            imagesHorizontalRvAdap!!.notifyDataSetChanged()
        }

        // post
        val tV_post = findViewById(R.id.tV_post) as TextView
        tV_post.setText(getResources().getString(R.string.update))
        tV_post.setOnClickListener(this)
        //progress_bar_post= (ProgressBar) findViewById(R.id.progress_bar_post);
        currencySymbol
    }

    /**
     * In this method we used to check whether current lat and
     * long has been received or not.
     *
     * @param lat The current latitude
     * @param lng The current longitude
     * @return boolean flag true or false
     */
    private fun isLocationFound(lat: String?, lng: String?): Boolean {
        return !(lat == null || lat.isEmpty()) && !(lng == null || lng.isEmpty())
    }

    /**
     * <h>GetCurrencySymbol</h>
     *
     *
     * In this method we used to get the country currency symbol e.g $ from given Currency code e.g USD
     *
     */
    private val currencySymbol: Unit
        private get() {
            val arrayCurrency: Array<String> = getResources().getStringArray(R.array.currency_picker)
            if (arrayCurrency.size > 0) {
                var getCurrencyArr: Array<String>
                for (setCurrency in arrayCurrency) {
                    getCurrencyArr = setCurrency.split(",".toRegex()).toTypedArray()
                    val currency_code = getCurrencyArr[1]
                    val currency_symbol = getCurrencyArr[2]
                    println("$TAG given currency=$currency my currency=$currency_code")
                    if (currency == currency_code) {
                        tV_currency_symbol!!.text = currency_symbol
                    }
                }
            }
        }

    override fun onClick(v: View) {
        val intent: Intent
        when (v.id) {
            R.id.tV_post -> if (arrayListImgPath!!.size == 0 && aLProductImageDatases!!.size == 0) {
                CommonClass.showSnackbarMessage(rL_rootElement, getResources().getString(R.string.at_least_one_image_is))
            } else {
                mDialogBox!!.showProgressDialog(mActivity!!.resources.getString(R.string.updating))
                if (arrayListImgPath!!.size > 0) {
                    uploadImages(arrayListImgPath, rotationAngles)
                } else {
                    for (productImageDatas in aLProductImageDatases!!) {
                        println(TAG + " " + "main url=" + productImageDatas!!.mainUrl + " " + "thumb nail url=" + productImageDatas.thumbnailUrl + " " + "width=" + productImageDatas.width + " " + "height=" + productImageDatas.height + " " + "message=" + productImageDatas.message + " " + "public id=" + productImageDatas.public_id)
                    }
                    if (aLProductImageDatases!!.size > 0) updatePostApi(aLProductImageDatases)
                }
            }
            R.id.rL_back_btn -> onBackPressed()
            R.id.rL_product_category -> {
                intent = Intent(mActivity, ProductCategoryActivity::class.java)
                intent.putExtra("selectedCategory", category)
                intent.putExtra("selectedSubCategory", subCategory)
                intent.putExtra("selectedField", filterKeyValues)
                startActivityForResult(intent, VariableConstants.CATEGORY_REQUEST_CODE)
            }
            R.id.rL_conditions -> {
                intent = Intent(mActivity, PostConditionsActivity::class.java)
                startActivityForResult(intent, VariableConstants.CONDITION_REQUEST_CODE)
            }
//            R.id.rL_currency -> {
//                intent = Intent(mActivity, CurrencyListActivity::class.java)
//                startActivityForResult(intent, VariableConstants.CURRENCY_REQUEST_CODE)
//            }
            R.id.tV_change_loc -> {
                intent = Intent(mActivity, ChangeLocationActivity::class.java)
                startActivityForResult(intent, VariableConstants.CHANGE_LOC_REQ_CODE)
            }
            R.id.btn_cancel -> onBackPressed()
        }
    }

    /*
     *Uploading the image in cloudinary. */
    private fun uploadImages(list: ArrayList<String>?, rotationAnagles: ArrayList<Int>?) {
        println("$TAG post clicked 3")
        // Reduce the Bitmap size
        try {
            var bitmap: Bitmap?
            var outputStream: ByteArrayOutputStream
            for (i in list!!.indices) {
                bitmap = BitmapFactory.decodeFile(list[i])
                outputStream = ByteArrayOutputStream()
                //..for samsung device handle angle
                bitmap = if (Build.MANUFACTURER == "samsung") {
                    rotate(bitmap, 90)
                } else {
                    rotate(bitmap, rotationAnagles!![i])
                }
                bitmap?.compress(Bitmap.CompressFormat.JPEG, 30, outputStream)
                try {
                    val file_out = FileOutputStream(File(list[i]))
                    file_out.write(outputStream.toByteArray())
                    file_out.close()
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
            println("$TAG post clicked 4")
            // Upload Images to cloudinary
            FileUploader.getFileUploader(getApplicationContext())!!.UploadMultiple(list, object : UploadedCallback {
                override fun onSuccess(data_list: List<ProductImageDatas>?, failed_list: List<ProductImageDatas>?) {
                    //sort by image index to maintain the order of image capture sequence
                    if (data_list != null) {
                        Collections.sort(data_list) { s1, s2 -> // notice the cast to (Integer) to invoke compareTo
                            s1.index.compareTo(s2.index)
                        }
                        for (productImageDatas in aLUpdateProductImage!!) {
                            println(TAG + " " + "main url 1=" + productImageDatas!!.mainUrl + " " + "thumb nail url=" + productImageDatas.thumbnailUrl + " " + "width=" + productImageDatas.width + " " + "height=" + productImageDatas.height + " " + "message=" + productImageDatas.message)
                        }


                        // handle for image drag & drop
                        var j = 0
                        val pos = ArrayList<Int>()
                        for (i in aLProductImageDatases!!.indices) {
                            if (!aLProductImageDatases!![i]!!.isImageUrl) {
                                pos.add(i)
                            }
                        }
                        ///////////////////////////////
                        for (productImageDatas in data_list) {
                            println(TAG + " " + "main url 2=" + productImageDatas.mainUrl + " " + "thumb nail url=" + productImageDatas.thumbnailUrl + " " + "width=" + productImageDatas.width + " " + "height=" + productImageDatas.height + " " + "message=" + productImageDatas.message)
                            val productImage = ProductImageDatas()
                            productImage.mainUrl = productImageDatas.mainUrl
                            productImage.thumbnailUrl = productImageDatas.thumbnailUrl
                            productImage.width = productImageDatas.width
                            productImage.height = productImageDatas.height
                            productImage.public_id = productImageDatas.public_id
                            //aLUpdateProductImage.remove((int)pos.get(j));
                            aLUpdateProductImage!!.add(pos[j], productImage)
                            println(TAG + " new image upload at " + pos[j])
                            j++
                        }
                        if (aLUpdateProductImage!!.size > 0) {
                            for (productImageDatas in aLUpdateProductImage!!) {
                                println(TAG + " " + "main url 3=" + productImageDatas!!.mainUrl + " " + "thumb nail url=" + productImageDatas.thumbnailUrl + " " + "width=" + productImageDatas.width + " " + "height=" + productImageDatas.height + " " + "message=" + productImageDatas.message + " " + "public id=" + productImageDatas.public_id)
                            }
                            updatePostApi(aLUpdateProductImage)
                        }
                    }
                }

                override fun onError(error: String?) {
                    if (mDialogBox!!.progressBarDialog != null) mDialogBox!!.progressBarDialog?.dismiss()
                    Log.d("fdsf324", " $error")
                }
            })
        } catch (e: OutOfMemoryError) {
            if (mDialogBox!!.progressBarDialog != null) mDialogBox!!.progressBarDialog?.dismiss()
            CommonClass.showSnackbarMessage(rL_rootElement, getString(R.string.failedToUpload))
        }
    }

    /**
     * <h>UpdatePostApi</h>
     *
     *
     * In this method we used we used to call update post api for modification
     * in the existing products.
     *
     *
     * @param aLProductImageDatases The list containing the image description like main image, thumbnail image, width and height
     */
    private fun updatePostApi(aLProductImageDatases: List<ProductImageDatas?>?) {
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            category = tV_category!!.text.toString()
            if (!category!!.isEmpty()) category = category!!.trim { it <= ' ' }
            val request_datas = JSONObject()
            try {
                request_datas.put("token", mSessionManager!!.authToken)
                request_datas.put("postId", postId)
                request_datas.put("type", "0")
                request_datas.put("subCategory", "")
                request_datas.put("category", tV_category!!.text.toString())
                println(TAG + " " + "category name=" + tV_category!!.text.toString())

                // product main image, thumbnail image, width and height
                request_datas.put("mainUrl", aLProductImageDatases!![0]!!.mainUrl)
                request_datas.put("thumbnailImageUrl", aLProductImageDatases[0]!!.thumbnailUrl)
                request_datas.put("containerHeight", aLProductImageDatases[0]!!.height)
                request_datas.put("containerWidth", aLProductImageDatases[0]!!.width)
                request_datas.put("imageCount", aLProductImageDatases.size)
                request_datas.put("cloudinaryPublicId", aLProductImageDatases[0]!!.public_id)
                request_datas.put("price", eT_price!!.text.toString())
                request_datas.put("currency", tV_currency!!.text.toString())

                // sub category
                request_datas.put("subCategory", subCategory)

                //filter data
                request_datas.put("filter", createFilterObject(filterKeyValues).toString())
                request_datas.put("productName", eT_title!!.text.toString())
                request_datas.put("condition", tV_condition!!.text.toString())
                request_datas.put("description", eT_description!!.text.toString())
                request_datas.put("negotiable", negotiable)
                request_datas.put("location", tV_current_location!!.text.toString())
                request_datas.put("latitude", latitude)
                request_datas.put("longitude", longitude)
                request_datas.put("city", city)
                request_datas.put("countrySname", countrySname)
                request_datas.put("tagProduct", "")
                request_datas.put("tagProductCoordinates", "")

                // if swap post
                if (swapPostArrayList != null && swapPostArrayList!!.size > 0) {
                    isSwap = 1
                    request_datas.put("isSwap", isSwap)
                    request_datas.put("swapingPost", createSwappingArray(swapPostArrayList!!).toString())
                } else {
                    isSwap = 0
                    request_datas.put("isSwap", isSwap)
                    request_datas.put("swapingPost", "")
                }

                // Second Image
                if (aLProductImageDatases.size > 1) {
                    request_datas.put("thumbnailUrl1", aLProductImageDatases[1]!!.thumbnailUrl)
                    request_datas.put("imageUrl1", aLProductImageDatases[1]!!.mainUrl)
                    request_datas.put("containerHeight1", aLProductImageDatases[1]!!.height)
                    request_datas.put("containerWidth1", aLProductImageDatases[1]!!.width)
                    request_datas.put("cloudinaryPublicId1", aLProductImageDatases[1]!!.public_id)

//                    Log.d("cloudinaryPublicId111",aLProductImageDatases.get(1).getPublic_id());
                } else {
                    request_datas.put("thumbnailUrl1", "")
                    request_datas.put("imageUrl1", "")
                    request_datas.put("containerHeight1", "")
                    request_datas.put("containerWidth1", "")
                    request_datas.put("cloudinaryPublicId1", "")
                    //                   Log.d("cloudinaryPublicId111",aLProductImageDatases.get(1).getPublic_id());
                }

                // Third Image
                if (aLProductImageDatases.size > 2) {
                    request_datas.put("imageUrl2", aLProductImageDatases[2]!!.mainUrl)
                    request_datas.put("thumbnailUrl2", aLProductImageDatases[2]!!.thumbnailUrl)
                    request_datas.put("containerHeight2", aLProductImageDatases[2]!!.height)
                    request_datas.put("containerWidth2", aLProductImageDatases[2]!!.width)
                    request_datas.put("cloudinaryPublicId2", aLProductImageDatases[2]!!.public_id)
                    //                    Log.d("cloudinaryPublicId112",aLProductImageDatases.get(2).getPublic_id());
                } else {
                    request_datas.put("imageUrl2", "")
                    request_datas.put("thumbnailUrl2", "")
                    request_datas.put("containerHeight2", "")
                    request_datas.put("containerWidth2", "")
                    request_datas.put("cloudinaryPublicId2", "")
                    //                    Log.d("cloudinaryPublicId112",aLProductImageDatases.get(2).getPublic_id());
                }

                // Fourth Image
                if (aLProductImageDatases.size > 3) {
                    request_datas.put("imageUrl3", aLProductImageDatases[3]!!.mainUrl)
                    request_datas.put("thumbnailUrl3", aLProductImageDatases[3]!!.thumbnailUrl)
                    request_datas.put("containerHeight3", aLProductImageDatases[3]!!.height)
                    request_datas.put("containerWidth3", aLProductImageDatases[3]!!.width)
                    request_datas.put("cloudinaryPublicId3", aLProductImageDatases[3]!!.public_id)

//                    Log.d("cloudinaryPublicId113",aLProductImageDatases.get(3).getPublic_id());
                } else {
                    request_datas.put("imageUrl3", "")
                    request_datas.put("thumbnailUrl3", "")
                    request_datas.put("containerHeight3", "")
                    request_datas.put("containerWidth3", "")
                    request_datas.put("cloudinaryPublicId3", "")
                    //                    Log.d("cloudinaryPublicId113",aLProductImageDatases.get(3).getPublic_id());
                }

                // Fifth Image
                if (aLProductImageDatases.size > 4) {
                    request_datas.put("imageUrl4", aLProductImageDatases[4]!!.mainUrl)
                    request_datas.put("thumbnailUrl4", aLProductImageDatases[4]!!.thumbnailUrl)
                    request_datas.put("containerHeight4", aLProductImageDatases[4]!!.height)
                    request_datas.put("containerWidth4", aLProductImageDatases[4]!!.width)
                    request_datas.put("cloudinaryPublicId4", aLProductImageDatases[4]!!.public_id)

//                    Log.d("cloudinaryPublicId114",aLProductImageDatases.get(4).getPublic_id());
                } else {
                    request_datas.put("imageUrl4", "")
                    request_datas.put("thumbnailUrl4", "")
                    request_datas.put("containerHeight4", "")
                    request_datas.put("containerWidth4", "")
                    request_datas.put("cloudinaryPublicId4", "")
                    //                    Log.d("cloudinaryPublicId114",aLProductImageDatases.get(4).getPublic_id());
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            OkHttp3Connection.doOkHttp3Connection(TAG, ApiUrl.PRODUCT, OkHttp3Connection.Request_type.PUT, request_datas, object : OkHttp3RequestCallback {
                 override fun onSuccess(result: String?, user_tag: String?) {
                    println("$TAG update product res=$result")
                    if (mDialogBox!!.progressBarDialog != null) mDialogBox!!.progressBarDialog?.dismiss()
                    //tV_post.setVisibility(View.VISIBLE);
                    val updateProductMainPojo: UpdateProductMainPojo
                    val gson = Gson()
                    updateProductMainPojo = gson.fromJson(result, UpdateProductMainPojo::class.java)
                    when (updateProductMainPojo.code) {
                        "200" -> {
                            val data = updateProductMainPojo.data?.get(0)
                            AppController.instance?.dbController?.updateAllProduct(data!!.postId, data.mainUrl, data.productName, data.price, data.negotiable, data.currency)
                            val intent = Intent(mActivity, HomePageActivity::class.java)
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                            mActivity!!.startActivity(intent)
                        }
                        else -> CommonClass.showSnackbarMessage(rL_rootElement, result)
                    }
                }

                 override fun onError(error: String?, user_tag: String?) {
                    if (mDialogBox!!.progressBarDialog != null) mDialogBox!!.progressBarDialog!!.dismiss()
                    //tV_post.setVisibility(View.VISIBLE);
                    CommonClass.showSnackbarMessage(rL_rootElement, error)
                }
            })
        } else CommonClass.showSnackbarMessage(rL_rootElement, getResources().getString(R.string.NoInternetAccess))
    }

    private fun createSwappingArray(list: ArrayList<SwapPost>): JSONArray {
        val jsonArray = JSONArray()
        var obj: JSONObject? = null
        for (i in list.indices) {
            val iwantItemPojo = list[i]
            if (iwantItemPojo.itemType) {
                list.remove(iwantItemPojo)
                continue
            }
            try {
                obj = JSONObject()
                obj.put("swapDescription", "")
                obj.put("swapPostId", iwantItemPojo.swapPostId)
                obj.put("swapTitle", iwantItemPojo.swapTitle)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            jsonArray.put(obj)
        }
        return jsonArray
    }

   override  protected fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (data != null) {
            when (requestCode) {
                VariableConstants.UPDATE_IMAGE_REQ_CODE -> {
                    arrayListImgPath = data.getStringArrayListExtra("arrayListImgPath")
                    rotationAngles = data.getIntegerArrayListExtra("rotationAngles")
                    var i = 0
                    while (i < rotationAngles!!.size) {
                        Log.d(TAG, "onActivityResult: rotationAngles " + rotationAngles!!.get(i))
                        i++
                    }
                    println(TAG + " " + "receive image count=" + arrayListImgPath?.size)
                    if (arrayListImgPath!!.size > 0) {
                        isNewUpload = true
                        //for (String imagePath : arrayListImgPath)
                        var imgPathCount = 0
                        while (imgPathCount < arrayListImgPath!!.size) {
                            val productImageDatas = ProductImageDatas()
                            productImageDatas.mainUrl = arrayListImgPath!!.get(imgPathCount)
                            productImageDatas.isImageUrl = false
                            productImageDatas.rotationAngle = rotationAngles?.get(imgPathCount)!!
                            aLProductImageDatases!!.add(productImageDatas)
                            imgPathCount++
                        }
                        imagesHorizontalRvAdap?.notifyDataSetChanged()
                    } else {
                        isNewUpload = false
                    }
                }
                VariableConstants.CATEGORY_REQUEST_CODE -> {
                    val categoryName = data.getStringExtra("categoryName")
                    subCategory = data.getStringExtra("subCategoryName")
                    filterKeyValues = data.getSerializableExtra("filterKeyValues") as ArrayList<FilterKeyValue>
                    println("$TAG category data: $categoryName")
                    println("$TAG category data: $subCategory")
                    for (f in filterKeyValues!!) {
                        println(TAG + " category data: " + f.key + "," + f.value)
                    }
                    if (categoryName != null) {
                        tV_category!!.text = categoryName
                        eT_price!!.requestFocus()
                    }
                }
                VariableConstants.CONDITION_REQUEST_CODE -> {
                    val condition = data.getStringExtra("condition")
                    if (condition != null) tV_condition!!.text = condition
                }
                VariableConstants.CURRENCY_REQUEST_CODE -> {
                    val cuurency_code = data.getStringExtra("cuurency_code")
                    val currency_symbol = data.getStringExtra("currency_symbol")

                    // set currency cde eg. Inr
                    if (cuurency_code != null) tV_currency!!.text = cuurency_code

                    // set currency symbol eg. $
                    if (currency_symbol != null) tV_currency_symbol!!.text = currency_symbol
                }
                VariableConstants.CHANGE_LOC_REQ_CODE -> {
                    var placeName = data.getStringExtra("address")
                    latitude = data.getStringExtra("lat")
                    longitude = data.getStringExtra("lng")

                    //..find city and countrysname from chnaged lat long
                    city = CommonClass.getCityName(mActivity!!, latitude!!.toDouble(), longitude!!.toDouble())
                    countrySname = CommonClass.getCountryCode(mActivity!!, latitude!!.toDouble(), longitude!!.toDouble())
                    val countryName = CommonClass.getCountryName(mActivity!!, latitude!!.toDouble(), longitude!!.toDouble())
                    placeName = if (city == null || city!!.isEmpty()) CommonClass.getFirstCaps(countryName) else if (countryName == null || countryName.isEmpty()) CommonClass.getFirstCaps(city) else CommonClass.getFirstCaps(city) + ", " + CommonClass.getFirstCaps(countryName)
                    println("$TAG place name=$placeName $latitude $longitude")
                    if (placeName != null && !placeName.isEmpty()) tV_current_location!!.text = placeName
                }
                VariableConstants.Willing_TO_EXCHANGE -> {
                    if (!swapPostArrayList!!.isEmpty()) {
                        layoutManager?.removeAllViews()
                        swapPostArrayList!!.clear()
                        iwantAdapter!!.notifyDataSetChanged()
                    }
                    swapPostArrayList!!.addAll((data.getSerializableExtra("iWantArrayList") as ArrayList<SwapPost>))
                    val iwantItemPojo = SwapPost()
                    iwantItemPojo.itemType = true
                    swapPostArrayList!!.add(iwantItemPojo)
                    // iWantRv.getRecycledViewPool().clear();
                    iwantAdapter!!.notifyDataSetChanged()
                }
            }
        }
    }

    /**
     * <h>deleteAllCapturedImages</h>
     *
     *
     * In this method we used to delete all the captured images which is taken by the camera.
     *
     *
     * @param mList The list containing the images
     */
    fun deleteAllCapturedImages(mList: ArrayList<String?>) {
        try {
            var f: File
            var deleted = false
            for (i in mList.indices) {
                f = File(mList[i])
                if (f.exists()) {
                    f.delete()
                    deleted = true
                }
            }
            if (deleted) {
                MediaScannerConnection.scanFile(this, arrayOf(Environment.getExternalStorageDirectory().toString()), null, /*
                     *   (non-Javadoc)
                     * @see android.media.MediaScannerConnection.OnScanCompletedListener#onScanCompleted(java.lang.String, android.net.Uri)
                     */MediaScannerConnection.OnScanCompletedListener { path, uri -> })
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun rotate(bitmap: Bitmap?, degree: Int): Bitmap {
        val w = bitmap!!.width
        val h = bitmap.height
        val mtx = Matrix()
        mtx.postRotate(degree.toFloat())
        return Bitmap.createBitmap(bitmap, 0, 0, w, h, mtx, true)
    }

 override fun onBackPressed() {
        finish()
        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate)
    }

    // create filter data json object, which is need to send.
    fun createFilterObject(list: ArrayList<FilterKeyValue>?): JSONObject {
        val jsonObject = JSONObject()
        try {
            for (filterKeyValue in list!!) {
                if (filterKeyValue.type != null && filterKeyValue.type == "1") {
                    val i = filterKeyValue.value.toInt()
                    jsonObject.put(filterKeyValue.key, i)
                } else {
                    jsonObject.put(filterKeyValue.key, filterKeyValue.value)
                }
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        return jsonObject
    }

    fun iWantAdapterClick(postId: String, position: Int) {
        arrayListPostIds.remove(postId)
        //  swapPostArrayList.remove(position);
        //iwantAdapter.notifyItemRangeRemoved(0, swapPostArrayList.size());
        swapPostArrayList!!.removeAt(position)
        iwantAdapter!!.notifyDataSetChanged()
    }

    fun addMoreView() {
        startWillingtoExchageActivity()
    }

    private fun startWillingtoExchageActivity() {
        val intent = Intent(this, WillingToExchangeActivity::class.java)
        intent.putExtra("iWantArrayList", swapPostArrayList)
        startActivityForResult(intent, VariableConstants.Willing_TO_EXCHANGE)
    }

    override fun onNoteListChanged(capturedImageList: List<CapturedImage?>?) {

    }


    override fun onUpdateImagesListChanged(productImageDatasList: List<ProductImageDatas?>?) {
        aLUpdateProductImage!!.clear()
        arrayListImgPath!!.clear()
        for (i in productImageDatasList!!.indices) {
            aLUpdateProductImage!!.add(productImageDatasList[i])
            println(TAG + " " + "position:" + i + " " + productImageDatasList[i]!!.mainUrl)
            if (!productImageDatasList[i]!!.isImageUrl) {
                arrayListImgPath!!.add(productImageDatasList[i]!!.mainUrl!!)
            }
        }
    }

    override fun onStartDrag(viewHolder: RecyclerView.ViewHolder?) {}
    fun removedNewAddedPhotos(path: String) {
        for (i in arrayListImgPath!!.indices) {
            if (arrayListImgPath!![i] == path) {
                arrayListImgPath!!.removeAt(i)
            }
        }
    }

    companion object {
        private val TAG = UpdatePostActivity::class.java.simpleName
    }
}