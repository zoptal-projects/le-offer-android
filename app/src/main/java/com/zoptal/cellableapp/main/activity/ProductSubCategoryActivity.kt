package com.zoptal.cellableapp.main.activity

import android.app.Activity
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle

import android.view.View
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.annotation.Nullable
import androidx.appcompat.app.AppCompatActivity
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.LinearLayoutManager

import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.adapter.ProductSubCategoryRvAdapter
import com.zoptal.cellableapp.fcm_push_notification.Config
import com.zoptal.cellableapp.fcm_push_notification.NotificationMessageDialog
import com.zoptal.cellableapp.fcm_push_notification.NotificationUtils.Companion.clearNotifications
import com.zoptal.cellableapp.main.activity.ProductSubCategoryActivity
import com.zoptal.cellableapp.main.view_type_activity.AddDetailActivity
import com.zoptal.cellableapp.pojo_class.product_category.FilterKeyValue
import com.zoptal.cellableapp.pojo_class.product_category.SubCategoryData
import com.zoptal.cellableapp.pojo_class.product_category.SubCategoryMainPojo
import com.zoptal.cellableapp.utility.*
import com.zoptal.cellableapp.utility.OkHttp3Connection.OkHttp3RequestCallback
import org.json.JSONObject
import java.util.*

class ProductSubCategoryActivity : AppCompatActivity(), View.OnClickListener {
    private var progress_bar: ProgressBar? = null
    private var mActivity: Activity? = null
    private var rL_rootview: RelativeLayout? = null
    private var rV_subCategory: RecyclerView? = null
    private var mNotificationMessageDialog: NotificationMessageDialog? = null
    private var aL_subCategoryDatas: ArrayList<SubCategoryData>? = null
    private var tV_title: TextView? = null
    private var categoryName = ""
    private var subCategoryRvAdapter: ProductSubCategoryRvAdapter? = null
    private var subCategoryName = ""
    private val selectedCatgory = ""
    private var selectedSubCategory: String? = ""
    private var selectedFileds: ArrayList<FilterKeyValue>? = null
     override fun onCreate(@Nullable savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_product_sub_category)
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale)
        mActivity = this@ProductSubCategoryActivity
        mNotificationMessageDialog = NotificationMessageDialog(mActivity)
        CommonClass.statusBarColor(mActivity!!)

        // get data which is selected by user.
        if (getIntent() != null) {
            selectedSubCategory = getIntent().getStringExtra("selectedSubCategory")
            selectedFileds = getIntent().getSerializableExtra("selectedField") as ArrayList<FilterKeyValue>?
        }
        tV_title = findViewById(R.id.tV_title) as TextView?
        progress_bar = findViewById(R.id.progress_bar) as ProgressBar?
        rL_rootview = findViewById(R.id.rL_rootview) as RelativeLayout?
        rV_subCategory = findViewById(R.id.rV_category) as RecyclerView?
        val rL_back_btn = findViewById(R.id.rL_back_btn) as RelativeLayout
        rL_back_btn.setOnClickListener(this)

        //aL_subCategoryDatas= (ArrayList<ProductCategoryResDatas>) getIntent().getSerializableExtra("categoryDatas");
        categoryName = getIntent().getStringExtra("categoryName")
        val title = categoryName.substring(0, 1).toUpperCase() + "" + categoryName.substring(1)
        tV_title!!.text = categoryName
        val layoutManager = LinearLayoutManager(mActivity)
        rV_subCategory!!.setLayoutManager(layoutManager)
        categoriesService
    }

    override fun onResume() {
        super.onResume()

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.REGISTRATION_COMPLETE))

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.PUSH_NOTIFICATION))

        // clear the notification area when the app is opened
        clearNotifications(getApplicationContext())
    }

   override fun onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!)
        super.onPause()
    }// isForFilter used while view type is open from filter screen// when user select another subcatgory then remove previously selected data// set previously selected category by user
    /////////////////////////////////////////////
/* try {
                request_datas.put("token", "20");
                request_datas.put("offset", "0");

            } catch (JSONException e) {
                e.printStackTrace();
            }*/

    /**
     * <h>GetCategoriesService</h>
     *
     *
     * This method is called from onCreate() method of the current class.
     * In this method we used to call the getCategories api using okHttp3.
     * Once we get the data we show that list in recyclerview.
     *
     */
    private val categoriesService: Unit
        private get() {
            if (CommonClass.isNetworkAvailable(mActivity!!)) {
                progress_bar!!.visibility = View.VISIBLE
                val request_datas = JSONObject()

                /* try {
                    request_datas.put("token", "20");
                    request_datas.put("offset", "0");

                } catch (JSONException e) {
                    e.printStackTrace();
                }*/
                val Url = ApiUrl.GET_SUB_CATEGORIES + "?categoryName=" + categoryName
                OkHttp3Connection.doOkHttp3Connection(TAG, Url, OkHttp3Connection.Request_type.GET, request_datas, object : OkHttp3RequestCallback {
                     override fun onSuccess(result: String?, user_tag: String?) {
                        progress_bar!!.visibility = View.GONE
                        println("$TAG get subcategory res=$result")
                        val subCategoryMainPojo: SubCategoryMainPojo
                        val gson = Gson()
                        subCategoryMainPojo = gson.fromJson(result, SubCategoryMainPojo::class.java)
                        when (subCategoryMainPojo.code) {
                            "200" -> {
                                aL_subCategoryDatas = subCategoryMainPojo.data
                                subCategoryRvAdapter = ProductSubCategoryRvAdapter(mActivity!!, aL_subCategoryDatas!!)
                                rV_subCategory!!.setAdapter(subCategoryRvAdapter)
                                if (aL_subCategoryDatas != null && aL_subCategoryDatas!!.size > 0) {
                                    // set previously selected category by user
                                    var i = 0
                                    while (i < aL_subCategoryDatas!!.size) {
                                        if (selectedSubCategory != null && !selectedSubCategory!!.isEmpty() && selectedSubCategory.equals(aL_subCategoryDatas!![i].subCategoryName, ignoreCase = true)) subCategoryRvAdapter!!.mSelected = i
                                        i++
                                    }
                                    /////////////////////////////////////////////
                                    subCategoryRvAdapter!!.setOnItemClick(object : ClickListener {
                                        override fun onItemClick(view: View?, position: Int) {
                                            subCategoryName = aL_subCategoryDatas!![position].subCategoryName!!

                                            // when user select another subcatgory then remove previously selected data
                                            if (!subCategoryName.equals(selectedSubCategory, ignoreCase = true) && selectedFileds != null) selectedFileds!!.clear()
                                            if (aL_subCategoryDatas!![position].fieldCount > 0) {
                                                val intent = Intent(mActivity, AddDetailActivity::class.java)
                                                // isForFilter used while view type is open from filter screen
                                                intent.putExtra("isForFilter", getIntent().getBooleanExtra("isForFilter", false))
                                                intent.putExtra("title", "$categoryName -> $subCategoryName")
                                                intent.putExtra("filterData", aL_subCategoryDatas!![position].filter)
                                                intent.putExtra("selectedField", selectedFileds)
                                                startActivityForResult(intent, VariableConstants.ADD_DETAIL_DATA_REQ_CODE)
                                            } else {
                                                val data = Intent()
                                                data.putExtra("categoryName", categoryName)
                                                data.putExtra("subCategoryName", subCategoryName)
                                                data.putExtra("filterKeyValues", ArrayList<FilterKeyValue>())
                                                setResult(VariableConstants.SUB_CATEGORY_REQ_CODE, data)
                                                onBackPressed()
                                            }
                                        }
                                    })
                                }
                            }
                            else -> CommonClass.showSnackbarMessage(rL_rootview, subCategoryMainPojo.message)
                        }
                    }

                     override fun onError(error: String?, user_tag: String?) {
                        progress_bar!!.visibility = View.GONE
                        CommonClass.showSnackbarMessage(rL_rootview, error)
                    }
                })
            } else CommonClass.showSnackbarMessage(rL_rootview, getResources().getString(R.string.NoInternetAccess))
        }

 override fun onBackPressed() {
        finish()
        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.rL_back_btn -> onBackPressed()
        }
    }

   override  protected fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (data != null) {
            when (resultCode) {
                VariableConstants.ADD_DETAIL_DATA_REQ_CODE -> {
                    data.putExtra("categoryName", categoryName)
                    data.putExtra("subCategoryName", subCategoryName)
                    setResult(VariableConstants.SUB_CATEGORY_REQ_CODE, data)
                    onBackPressed()
                }
            }
        }
    }

    companion object {
        private val TAG = ProductSubCategoryActivity::class.java.simpleName
    }
}