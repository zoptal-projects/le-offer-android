package com.zoptal.cellableapp.main.view_type_activity

import android.app.Activity
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle

import androidx.appcompat.app.AppCompatActivity
import android.view.View
import android.widget.RelativeLayout
import android.widget.SeekBar
import android.widget.TextView
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.android.material.snackbar.Snackbar
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.fcm_push_notification.Config
import com.zoptal.cellableapp.fcm_push_notification.NotificationMessageDialog
import com.zoptal.cellableapp.fcm_push_notification.NotificationUtils.Companion.clearNotifications
import com.zoptal.cellableapp.main.view_type_activity.SeekBarViewActivity
import com.zoptal.cellableapp.utility.VariableConstants
import java.util.*

class SeekBarViewActivity : AppCompatActivity(), View.OnClickListener {
    private var mNotificationMessageDialog: NotificationMessageDialog? = null
    private val tV_value: TextView? = null
    private var tV_title: TextView? = null
    private var tV_min: TextView? = null
    private var tV_max: TextView? = null
    private var mActivity: Activity? = null
    private var title: String? = null
    private var selectedValue = ""
    private var fieldName = ""
    private var values: String? = null
    private var arrayListValue: ArrayList<String>? = null
    private var seekBar: SeekBar? = null
    private var rootElement: RelativeLayout? = null
    protected override fun onCreate(savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_seek_bar_view)
        mActivity = this@SeekBarViewActivity
        mNotificationMessageDialog = NotificationMessageDialog(mActivity)
        rootElement = findViewById(R.id.rootElement) as RelativeLayout?

        //get data from intent
        title = getIntent().getStringExtra("title")
        fieldName = getIntent().getStringExtra("fieldName")
        values = getIntent().getStringExtra("values")
        if (getIntent().hasExtra("selectedValue")) {
            selectedValue = getIntent().getStringExtra("selectedValue")
        }

        // set activity title
        tV_title = findViewById(R.id.tV_title) as TextView?
        if (title != null && !title!!.isEmpty()) tV_title!!.text = title

        // get min and max values
        arrayListValue = getArrayListFromValues(values)
        val min = arrayListValue!![0]
        val max = arrayListValue!![arrayListValue!!.size - 1]

        // set min and max values
        tV_min = findViewById(R.id.tV_min) as TextView?
        tV_max = findViewById(R.id.tV_max) as TextView?
        if (min != null && !min.isEmpty()) tV_min!!.text = min
        if (max != null && !max.isEmpty()) tV_max!!.text = max
        seekBar = findViewById(R.id.seekBar) as SeekBar?
        seekBar!!.max = min.toInt()
        seekBar!!.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, i: Int, b: Boolean) {
                tV_value!!.setText(i)
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {}
            override fun onStopTrackingTouch(seekBar: SeekBar) {}
        })

        // intialize back and done button
        val rL_apply = findViewById(R.id.rL_apply) as RelativeLayout
        rL_apply.setOnClickListener(this)
        val rL_back_btn = findViewById(R.id.rL_back_btn) as RelativeLayout
        rL_back_btn.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.rL_apply -> {
                Snackbar.make(rootElement!!, tV_value!!.text.toString(), Snackbar.LENGTH_SHORT).show()
                val intent: Intent = getIntent()
                intent.putExtra("selectedValue", tV_value.text.toString().trim { it <= ' ' })
                setResult(VariableConstants.SELECT_VALUE_REQ_CODE, intent)
                onBackPressed()
            }
            R.id.rL_back_btn -> onBackPressed()
        }
    }

    fun getArrayListFromValues(values: String?): ArrayList<String> {
        val arrayList = ArrayList<String>()
        val `val` = values!!.split(",".toRegex()).toTypedArray()
        arrayList.addAll(Arrays.asList(*`val`))
        return arrayList
    }

    override fun onResume() {
        super.onResume()

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.REGISTRATION_COMPLETE))

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.PUSH_NOTIFICATION))

        // clear the notification area when the app is opened
        clearNotifications(getApplicationContext())
    }

   override fun onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!)
        super.onPause()
    }

    companion object {
        private val TAG = SeekBarViewActivity::class.java.simpleName
    }
}