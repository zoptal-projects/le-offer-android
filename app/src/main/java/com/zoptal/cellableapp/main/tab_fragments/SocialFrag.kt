package com.zoptal.cellableapp.main.tab_fragments

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.Nullable
import androidx.core.app.ActivityOptionsCompat
import androidx.core.view.ViewCompat
import androidx.fragment.app.Fragment
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.gson.Gson
import com.squareup.otto.Subscribe
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.adapter.SocialFragRvAdapter
import com.zoptal.cellableapp.event_bus.BusProvider.instance
import com.zoptal.cellableapp.main.activity.DiscoverPeopleActivity
import com.zoptal.cellableapp.main.activity.HomePageActivity
import com.zoptal.cellableapp.main.activity.LandingActivity
import com.zoptal.cellableapp.main.activity.NotificationActivity
import com.zoptal.cellableapp.main.activity.products.ProductDetailsActivity
import com.zoptal.cellableapp.main.tab_fragments.SocialFrag
import com.zoptal.cellableapp.pojo_class.ProfileFollowingCount
import com.zoptal.cellableapp.pojo_class.UnseenNotifiactionCountPojo
import com.zoptal.cellableapp.pojo_class.home_explore_pojo.ExploreLikedByUsersDatas
import com.zoptal.cellableapp.pojo_class.social_frag_pojo.SocialDatas
import com.zoptal.cellableapp.pojo_class.social_frag_pojo.SocialMainPojo
import com.zoptal.cellableapp.utility.*
import com.zoptal.cellableapp.utility.OkHttp3Connection.OkHttp3RequestCallback
import org.json.JSONException
import org.json.JSONObject
import java.util.*

/**
 * <h>SocialFrag</h>
 *
 *
 * In this class we used to show product list of the user those who has been followed by
 * the logged-in user.
 *
 * @since 3/31/2017
 */
class SocialFrag : Fragment(), View.OnClickListener {
    private var mActivity: Activity? = null
    private var notificationCount = 0
    private var mProgress_bar: ProgressBar? = null
    private var sessionManager: SessionManager? = null
    private var linear_no_friends: LinearLayout? = null
    private var mSwipeRefreshLayout: SwipeRefreshLayout? = null
    private var arrayListNewsFeedDatas: ArrayList<SocialDatas>? = null
    private var newsFeedRvAdapter: SocialFragRvAdapter? = null
    private var pageIndex = 0
    private var rV_newsFeed: RecyclerView? = null
    private var layoutManager: LinearLayoutManager? = null
    private var itemPosition = 0
    private var rL_notification: RelativeLayout? = null

    // Load more variables
    private val visibleThreshold = 5
    private var totalVisibleItem = 0
    private var totalItemCount = 0
    private var isLoadingRequired = false
    private var isNewsFeedFragVisible = false
    private var tV_notification_count: TextView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        mActivity = getActivity()
        sessionManager = SessionManager(mActivity!!)
        arrayListNewsFeedDatas = ArrayList()
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        val view = inflater.inflate(R.layout.frag_social, container, false)
        val rL_rootview = view.findViewById<View>(R.id.rL_rootview) as RelativeLayout
        mProgress_bar = view.findViewById<View>(R.id.progress_bar_news_feed) as ProgressBar
        rV_newsFeed = view.findViewById<View>(R.id.rV_newsFeed) as RecyclerView
        linear_no_friends = view.findViewById<View>(R.id.linear_no_friends) as LinearLayout
        linear_no_friends!!.visibility = View.GONE
        val rL_find_friends = view.findViewById<View>(R.id.rL_find_friends) as RelativeLayout
        rL_find_friends.setOnClickListener(this)
        tV_notification_count = view.findViewById<View>(R.id.tV_notification_count) as TextView
        tV_notification_count!!.visibility = View.GONE
        // swipe refresh
        mSwipeRefreshLayout = view.findViewById<View>(R.id.mSwipeRefreshLayout) as SwipeRefreshLayout
        mSwipeRefreshLayout!!.setColorSchemeResources(R.color.pink_color)

        // set Recyclerview Adapter
        newsFeedRvAdapter = SocialFragRvAdapter(mActivity!!!!, arrayListNewsFeedDatas!!, rL_rootview)
        layoutManager = LinearLayoutManager(mActivity!!)
        rV_newsFeed!!.setLayoutManager(layoutManager)
        rV_newsFeed!!.setAdapter(newsFeedRvAdapter)
        rL_notification = view.findViewById<View>(R.id.rL_notification) as RelativeLayout
        rL_notification!!.setOnClickListener(this)
        // pull to refresh
        mSwipeRefreshLayout!!.setOnRefreshListener(object : SwipeRefreshLayout.OnRefreshListener {
            override fun onRefresh() {
                pageIndex = 0
                arrayListNewsFeedDatas!!.clear()
                newsFeedRvAdapter!!.notifyDataSetChanged()
                getNewsFeedDatas(pageIndex)
            }
        })

        // call news feed api to get datas
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            println("$TAG frag visible newsfeed=$isNewsFeedFragVisible")
            pageIndex = 0
            arrayListNewsFeedDatas!!.clear()
            newsFeedRvAdapter!!.notifyDataSetChanged()
            mProgress_bar!!.visibility = View.VISIBLE
            getNewsFeedDatas(pageIndex)
        } else CommonClass.showSnackbarMessage((mActivity!! as HomePageActivity?)!!.rL_rootElement, getResources().getString(R.string.NoInternetAccess))
        notificationCount = 0
        // to see notification count
        unseenNotificationCountApi()
        return view
    }

    /**
     * <h>UnseenNotificationCountApi</h>
     *
     *
     * In this method we used to do api call to get total unseen notification count.
     *
     */
    private fun unseenNotificationCountApi() {
        if (CommonClass.isNetworkAvailable(mActivity!!) && sessionManager!!.isUserLoggedIn) {
            val request_datas = JSONObject()
            try {
                request_datas.put("token", sessionManager!!.authToken)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            val unseenNotificationCountUrl = ApiUrl.UNSEEN_NOTIFICATION_COUNT + "?token=" + sessionManager!!.authToken
            OkHttp3Connection.doOkHttp3Connection(TAG, unseenNotificationCountUrl, OkHttp3Connection.Request_type.GET, JSONObject(), object : OkHttp3RequestCallback {
                 override fun onSuccess(result: String?, user_tag: String?) {
                    println("$TAG unseen notification count res=$result")
                    val unseenNotifiactionCountPojo: UnseenNotifiactionCountPojo
                    val gson = Gson()
                    unseenNotifiactionCountPojo = gson.fromJson(result, UnseenNotifiactionCountPojo::class.java)
                    when (unseenNotifiactionCountPojo.code) {
                        "200" -> {
                            println(TAG + " " + "Notification count=" + unseenNotifiactionCountPojo.data)
                            notificationCount = unseenNotifiactionCountPojo.data
                            if (notificationCount > 0) {
                                tV_notification_count!!.visibility = View.VISIBLE
                                tV_notification_count!!.text = notificationCount.toString()
                            } else tV_notification_count!!.visibility = View.GONE
                        }
                    }
                }

                 override fun onError(error: String?, user_tag: String?) {}
            })
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        instance.register(this)
    }

    /**
     * <h>GetNewsFeedDatas</h>
     *
     *
     * In this method we used to do api call to get all the social product.
     *
     * @param offset The page index
     */
    private fun getNewsFeedDatas(offset: Int) {
        var offset = offset
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            val request_datas = JSONObject()
            val limit = 20
            offset = limit * offset
            try {
                request_datas.put("token", sessionManager!!.authToken)
                request_datas.put("offset", offset)
                request_datas.put("limit", offset)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            OkHttp3Connection.doOkHttp3Connection(TAG, ApiUrl.HOME, OkHttp3Connection.Request_type.POST, request_datas, object : OkHttp3RequestCallback {
                 override fun onSuccess(result: String?, user_tag: String?) {
                    mProgress_bar!!.visibility = View.GONE
                    mSwipeRefreshLayout!!.setRefreshing(false)
                    println("$TAG news feed res=$result")
                    if (result != null && !result.isEmpty()) {
                        val socialMainPojo: SocialMainPojo
                        val gson = Gson()
                        socialMainPojo = gson.fromJson(result, SocialMainPojo::class.java)
                        when (socialMainPojo.code) {
                            "200" -> {
                                mSwipeRefreshLayout!!.setRefreshing(false)
                                arrayListNewsFeedDatas!!.addAll(socialMainPojo.data!!)
                                if (arrayListNewsFeedDatas != null && arrayListNewsFeedDatas!!.size > 0) {
                                    linear_no_friends!!.visibility = View.GONE
                                    isLoadingRequired = arrayListNewsFeedDatas!!.size > 14
                                    newsFeedRvAdapter!!.notifyDataSetChanged()

                                    // Load more
                                    rV_newsFeed!!.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                                        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                                            super.onScrolled(recyclerView, dx, dy)
                                            totalItemCount = layoutManager!!.getItemCount()
                                            totalVisibleItem = layoutManager!!.findLastVisibleItemPosition()
                                            if (isLoadingRequired && totalItemCount <= totalVisibleItem + visibleThreshold) {
                                                pageIndex = pageIndex + 1
                                                getNewsFeedDatas(pageIndex)
                                                isLoadingRequired = false
                                            }
                                        }
                                    })

                                    // Item click
                                    newsFeedRvAdapter!!.setItemClick(object : ProductItemClickListener {
                                        override fun onItemClick(pos: Int, imageView: ImageView?) {
                                            if (arrayListNewsFeedDatas!!.size > pos) {
                                                itemPosition = pos
                                                val intent = Intent(mActivity!!, ProductDetailsActivity::class.java)
                                                intent.putExtra("productName", arrayListNewsFeedDatas!![pos].productName)
                                                //if (arrayListNewsFeedDatas.get(pos).getCategoryData()!=null)
                                                intent.putExtra("category", arrayListNewsFeedDatas!![pos].category)
                                                intent.putExtra("subCategory", arrayListNewsFeedDatas!![pos].subCategory)
                                                intent.putExtra("likes", arrayListNewsFeedDatas!![pos].likes)
                                                intent.putExtra("likeStatus", arrayListNewsFeedDatas!![pos].likeStatus)
                                                intent.putExtra("currency", arrayListNewsFeedDatas!![pos].currency)
                                                intent.putExtra("price", arrayListNewsFeedDatas!![pos].price)
                                                intent.putExtra("postedOn", arrayListNewsFeedDatas!![pos].postedOn)
                                                intent.putExtra("image", arrayListNewsFeedDatas!![pos].mainUrl)
                                                intent.putExtra("thumbnailImageUrl", arrayListNewsFeedDatas!![pos].thumbnailImageUrl)
                                                intent.putExtra("likedByUsersArr", arrayListNewsFeedDatas!![pos].likedByUsers)
                                                //intent.putExtra("description", arrayListNewsFeedDatas.get(pos).getDescription());
                                                intent.putExtra("condition", arrayListNewsFeedDatas!![pos].condition)
                                                intent.putExtra("place", arrayListNewsFeedDatas!![pos].place)
                                                intent.putExtra("latitude", arrayListNewsFeedDatas!![pos].latitude)
                                                intent.putExtra("longitude", arrayListNewsFeedDatas!![pos].longitude)
                                                intent.putExtra("postedByUserName", arrayListNewsFeedDatas!![pos].membername)
                                                intent.putExtra("postId", arrayListNewsFeedDatas!![pos].postId)
                                                intent.putExtra("postsType", arrayListNewsFeedDatas!![pos].postsType)
                                                intent.putExtra("clickCount", arrayListNewsFeedDatas!![pos].clickCount)
                                                intent.putExtra(VariableConstants.EXTRA_ANIMAL_IMAGE_TRANSITION_NAME, ViewCompat.getTransitionName(imageView!!))
                                                val options = ActivityOptionsCompat.makeSceneTransitionAnimation(mActivity!!!!, imageView!!, ViewCompat.getTransitionName(imageView)!!)
                                                this@SocialFrag.startActivityForResult(intent, VariableConstants.PRODUCT_DETAILS_REQ_CODE, options.toBundle())
                                            }
                                        }
                                    })
                                }
                            }
                            "204" -> {
                                mSwipeRefreshLayout!!.setRefreshing(false)
                                if (arrayListNewsFeedDatas!!.size == 0) linear_no_friends!!.visibility = View.VISIBLE
                            }
                            "401" -> {
                                mSwipeRefreshLayout!!.setRefreshing(false)
                                CommonClass.sessionExpired(mActivity!!)
                            }
                            else -> {
                                mSwipeRefreshLayout!!.setRefreshing(false)
                                CommonClass.showSnackbarMessage((mActivity!! as HomePageActivity?)!!.rL_rootElement, socialMainPojo.message)
                            }
                        }
                    }
                }

                 override fun onError(error: String?, user_tag: String?) {
                    mProgress_bar!!.visibility = View.GONE
                    mSwipeRefreshLayout!!.setRefreshing(false)
                    CommonClass.showSnackbarMessage((mActivity!! as HomePageActivity?)!!.rL_rootElement, error)
                }
            })
        } else CommonClass.showSnackbarMessage((mActivity!! as HomePageActivity?)!!.rL_rootElement, getResources().getString(R.string.NoInternetAccess))
    }

    @Subscribe
    fun getMessage(socialDatas: SocialDatas?) {
        if (socialDatas != null && socialDatas.postId != null) {
            if (socialDatas.isToAddSocialData) {
                if (!isContainsId(socialDatas.postId)) {
                    arrayListNewsFeedDatas!!.add(0, socialDatas)
                    newsFeedRvAdapter!!.notifyDataSetChanged()
                }
            } else {
                for (socialCount in arrayListNewsFeedDatas!!.indices) {
                    if (arrayListNewsFeedDatas!![socialCount].postId == socialDatas.postId) {
                        arrayListNewsFeedDatas!!.removeAt(socialCount)
                        newsFeedRvAdapter!!.notifyDataSetChanged()
                        break
                    }
                }
            }
        }
        if (arrayListNewsFeedDatas!!.size > 0) linear_no_friends!!.visibility = View.GONE else linear_no_friends!!.visibility = View.VISIBLE
        setProfileFollingCount()
    }

    /**
     * <h>setProfileFollingCount</h>
     *
     *
     * In this method we used to find out the following count from the list.
     *
     */
    private fun setProfileFollingCount() {
        //int count = 0;
        if (arrayListNewsFeedDatas!!.size > 0) {
            val uniqueSet = HashSet<String>()
            for (outerSocialData in arrayListNewsFeedDatas!!) {
                val membername = outerSocialData.membername
                println(TAG + " " + "membername=" + membername + " " + "logged in member name=" + sessionManager!!.userName)
                if (membername != sessionManager!!.userName) {
                    uniqueSet.add(membername)
                }
            }
            println(TAG + " " + "unique member count=" + uniqueSet.size)
            val follingCountObj = ProfileFollowingCount()
            follingCountObj.followingCount = uniqueSet.size
            instance.post(follingCountObj)
        }
    }

    /**
     * <h>IsContainsId</h>
     *
     *
     * In this method we used to check whether the given post id is
     * present or not in the current list.
     *
     * @param postId the given post id of product
     * @return the boolean value
     */
    fun isContainsId(postId: String): Boolean {
        var flag = false
        for (`object` in arrayListNewsFeedDatas!!) {
            println(TAG + " " + "given post id=" + postId + " " + "current post id=" + `object`.postId)
            if (postId == `object`.postId) {
                flag = true
            }
        }
        return flag
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.rL_notification -> if (sessionManager!!.isUserLoggedIn) {
                val intent = Intent(mActivity!!, NotificationActivity::class.java)
                startActivityForResult(intent, VariableConstants.IS_NOTIFICATION_SEEN_REQ_CODE)
            } else this@SocialFrag.startActivityForResult(Intent(mActivity!!, LandingActivity::class.java), VariableConstants.LANDING_REQ_CODE)
            R.id.rL_find_friends -> startActivity(Intent(mActivity!!, DiscoverPeopleActivity::class.java))
        }
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        isNewsFeedFragVisible = isVisibleToUser
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        println("$TAG requestCode=$requestCode resultCode=$resultCode data=$data")
        if (data != null) {
            when (requestCode) {
                VariableConstants.PRODUCT_DETAILS_REQ_CODE -> if (arrayListNewsFeedDatas!!.size > itemPosition) {
                    val likesCount = data.getStringExtra("likesCount")
                    val likeStatus = data.getStringExtra("likeStatus")
                    Log.d("likes product detail:", "$likesCount $likeStatus")
                    val aL_likedByUsers = data.getSerializableExtra("aL_likedByUsers") as ArrayList<ExploreLikedByUsersDatas>
                    arrayListNewsFeedDatas!![itemPosition].likes = likesCount
                    arrayListNewsFeedDatas!![itemPosition].likeStatus = likeStatus
                    arrayListNewsFeedDatas!![itemPosition].likedByUsers = aL_likedByUsers
                    //newsFeedRvAdapter.notifyItemChanged(itemPosition);
                    newsFeedRvAdapter!!.notifyDataSetChanged()
                }
                VariableConstants.IS_NOTIFICATION_SEEN_REQ_CODE -> {
                    val isNotificationSeen = data.getBooleanExtra("isNotificationSeen", false)
                    if (isNotificationSeen) {
                        notificationCount = 0
                        tV_notification_count!!.visibility = View.GONE
                    }
                }
            }
        }
    }

    override fun onDestroy() {
        instance.unregister(this)
        super.onDestroy()
    }

    companion object {
        private val TAG = SocialFrag::class.java.simpleName
    }
}