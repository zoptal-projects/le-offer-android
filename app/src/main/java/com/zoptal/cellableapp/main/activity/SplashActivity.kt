package com.zoptal.cellableapp.main.activity

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.os.PersistableBundle
import android.telephony.TelephonyManager
import android.util.Base64
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.crashlytics.android.Crashlytics
import com.google.firebase.iid.FirebaseInstanceId
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.get_current_location.FusedLocationReceiver
import com.zoptal.cellableapp.get_current_location.FusedLocationService
import com.zoptal.cellableapp.main.activity.HomePageActivity
import com.zoptal.cellableapp.main.activity.SplashActivity
import com.zoptal.cellableapp.utility.*
import com.zoptal.cellableapp.utility.OkHttp3Connection.OkHttp3RequestCallback
import io.fabric.sdk.android.Fabric
import org.json.JSONException
import org.json.JSONObject
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.util.*

/**
 * <h>SplashActivity</h>
 *
 *
 * This is launch screen i.e open first when user launch the app. It stays for
 * 3second then check if user is logged-in then go to HomeActivity or else go
 * to Landing Screen where we have option for login or signup.
 *
 *
 * @author 3Embed
 * @version 1.0
 * @since 3/29/2017.
 */
class SplashActivity : AppCompatActivity() {
    var lat = ""
    var lng = ""
    private var mActivity: Activity? = null
    private var mSessionManager: SessionManager? = null
    private var postResultData = ""
    private var locationService: FusedLocationService? = null
    private var permissionsArray: Array<String> ? =null
    private var runTimePermission: RunTimePermission? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState?: Bundle())
        Fabric.with(this, Crashlytics())
        setContentView(R.layout.activity_splash)
        mActivity = this@SplashActivity
        val tz = TimeZone.getDefault()
        val timeZoneId = tz.id
        val tm = this.getSystemService(TELEPHONY_SERVICE) as TelephonyManager
        val countryCodeValue = tm.networkCountryIso
        Log.e("=====", Calendar.getInstance().timeZone.getDisplayName(false, TimeZone.SHORT) + "==" + countryCodeValue + "===" + Locale.getDefault().country)
        mSessionManager = SessionManager(mActivity!!)
        permissionsArray = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
        runTimePermission = RunTimePermission(mActivity!!, permissionsArray!!, false)


        // change status bar color
        CommonClass.statusBarColor(mActivity!!)

        // generating unique id from FCM
        val serialNumber = FirebaseInstanceId.getInstance().id
        println("$TAG serial number=$serialNumber")
        if (serialNumber != null && !serialNumber.isEmpty()) {
            mSessionManager!!.deviceId = serialNumber
        }

        //Getting registration token
        val refreshedToken = FirebaseInstanceId.getInstance().token
        //Displaying token on logcat
        println("$TAG My Refreshed token: $refreshedToken")
        if (refreshedToken != null && !refreshedToken.isEmpty()) mSessionManager!!.pushToken = refreshedToken
        println(TAG + "get push token=" + mSessionManager!!.pushToken)

        // get post data in this activity and pass it for reducing the load in home frag where its need to be show
        postApiGetOnlyData()

        // get bundle datas if notification comes in background
        if(getIntent()!=null &&  getIntent().getExtras()!=null) {
            val bundle: Bundle = getIntent().getExtras()!!
            println("$TAG bundle=$bundle")
            var notificationDatas: String? = ""
            if (bundle != null) {
                notificationDatas = bundle.getString("body")
                println("$TAG bundle notification datas=$notificationDatas")
            }


            // Go to notification screen if any notification msg is there else Home Page
            if (notificationDatas != null && !notificationDatas.isEmpty()) callNotificationClass(notificationDatas)
        }
        // setTimerForScreen(3000);
        val info: PackageInfo
        try {
            info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES)
            for (signature in info.signatures) {
                var md: MessageDigest
                md = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                val something = String(Base64.encode(md.digest(), 0))
                //String something = new String(Base64.encodeBytes(md.digest()));
                Log.e("hash key", something)
            }
        } catch (e1: PackageManager.NameNotFoundException) {
            Log.e("name not found", e1.toString())
        } catch (e: NoSuchAlgorithmException) {
            Log.e("no such an algorithm", e.toString())
        } catch (e: Exception) {
            Log.e("exception", e.toString())
        }
    }


    private fun postApiGetOnlyData() {
        // Call all posted api
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            // check is user logged in or not if its logged in then show according to location if not then show all posts.
            if (mSessionManager!!.isUserLoggedIn) {
                if (runTimePermission!!.checkPermissions(permissionsArray!!)) {
                    currentLocation
                } else {
                    setTimerForScreen(3000)
                }
            } else {
                if (runTimePermission!!.checkPermissions(permissionsArray!!)) {
                    currentLocation
                } else {
                    setTimerForScreen(3000)
                }
            }
        } else {
            setTimerForScreen(3000)
        }
    }

    fun forceCrash(view: View?) {
        throw RuntimeException("This is a crash")
    }

    /**
     * <h>CallNotificationClass</h>
     *
     *
     * In this method we used to receive the bundle datas when notification comes
     * from background. After that we used to send datas to Notification activity
     * class.
     *
     *
     * @param notificationDatas The notification datas.
     */
    private fun callNotificationClass(notificationDatas: String?) {
        if (notificationDatas != null && !notificationDatas.isEmpty()) {
            println("$TAG bundle=$notificationDatas")
            val intent = Intent(mActivity, NotificationActivity::class.java)
            intent.putExtra("notificationDatas", notificationDatas)
            intent.putExtra("isFromNotification", true)
            startActivity(intent)
            finish()
        }
    }

    /**
     * <h>SetTimerForScreen</h>
     *
     *
     * In this method we used to sleep screen for three second.
     *
     */
    private fun setTimerForScreen(msec: Int) {
        Handler().postDelayed({
            val intent: Intent
            intent = if (mSessionManager!!.privacyAccept) Intent(mActivity, HomePageActivity::class.java) else Intent(mActivity, PermissionAcceptActivity::class.java)
            intent.putExtra("postResultData", postResultData)
            startActivity(intent)
            finish()
        }, msec.toLong())
    }

    /**
     * In this method we find current location using FusedLocationApi.
     * in this we have onUpdateLocation() method in which we check if
     * its not null then We call guest user api.
     */
    private val currentLocation: Unit
        private get() {
            locationService = FusedLocationService(mActivity!!, object : FusedLocationReceiver {
                override fun onUpdateLocation() {
                    val currentLocation = locationService!!.receiveLocation()
                    if (currentLocation != null) {
                        lat = currentLocation.latitude.toString()
                        lng = currentLocation.longitude.toString()
                        println("$TAG lat=$lat lng=$lng")
                        if (isLocationFound(lat, lng)) {
                            mSessionManager!!.currentLat = lat
                            mSessionManager!!.currentLng = lng
                            if (mSessionManager!!.isUserLoggedIn) getUserPosts(0) else getGuestPosts(0)
                        }
                    }
                }
            }
            )
        }

    private fun isLocationFound(lat: String?, lng: String?): Boolean {
        return !(lat == null || lat.isEmpty()) && !(lng == null || lng.isEmpty())
    }

    /**
     * <h>GetGuestPosts</h>
     *
     *
     * In this method we used to call guest user api to get all posts.
     *
     *
     * @param offset The page index
     */
    private fun getGuestPosts(offset: Int) {
        var offset = offset
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            val requestDatas = JSONObject()
            val limit = 20
            offset = limit * offset
            try {
                requestDatas.put("offset", offset)
                requestDatas.put("limit", limit)
                requestDatas.put("latitude", lat)
                requestDatas.put("longitude", lng)
                requestDatas.put("pushToken", mSessionManager!!.pushToken)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            OkHttp3Connection.doOkHttp3Connection(TAG, ApiUrl.GET_GUEST_ALL_POSTS, OkHttp3Connection.Request_type.POST, requestDatas, object : OkHttp3RequestCallback {
                override fun onSuccess(result: String?, user_tag: String?) {
                    var jsonObject: JSONObject? = null
                    try {
                        jsonObject = JSONObject(result)
                        val code = jsonObject.getString("code")
                        val msg = jsonObject.getString("message")
                        when (code) {
                            "200" -> {
                                postResultData = result!!
                                setTimerForScreen(500)
                            }
                            "401" -> CommonClass.sessionExpired(mActivity!!)
                            else -> setTimerForScreen(500)
                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }

                override fun onError(error: String?, user_tag: String?) {
                    setTimerForScreen(500)
                }

            })
        } else {
            setTimerForScreen(3000)
        }
    }

    /**
     * <h>GetUserPosts</h>
     *
     *
     * In this method we used to do call getUserPosts api. And get all posts
     * in response. Once we get all post then show that in recyclerview.
     *
     *
     * @param offset The pagination
     */
    private fun getUserPosts(offset: Int) {
        var offset = offset
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            val requestDatas = JSONObject()
            val limit = 20
            offset = limit * offset
            try {
                requestDatas.put("offset", offset)
                requestDatas.put("limit", limit)
                requestDatas.put("token", mSessionManager!!.authToken)
                requestDatas.put("latitude", lat)
                requestDatas.put("longitude", lng)
                requestDatas.put("pushToken", mSessionManager!!.pushToken)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            OkHttp3Connection.doOkHttp3Connection(TAG, ApiUrl.GET_USER_ALL_POSTS, OkHttp3Connection.Request_type.POST, requestDatas, object : OkHttp3RequestCallback {
                override fun onSuccess(result: String?, user_tag: String?) {
                    var jsonObject: JSONObject? = null
                    try {
                        jsonObject = JSONObject(result)
                        val code = jsonObject.getString("code")
                        val msg = jsonObject.getString("message")
                        when (code) {
                            "200" -> {
                                postResultData = result!!
                                setTimerForScreen(500)
                            }
                            "401" -> CommonClass.sessionExpired(mActivity!!)
                            else -> setTimerForScreen(500)
                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }

                override fun onError(error: String?, user_tag: String?) {
                    setTimerForScreen(500)
                }
            })
        } else {
            setTimerForScreen(3000)
        }
    }

    companion object {
        private val TAG = SplashActivity::class.java.simpleName
    }
}