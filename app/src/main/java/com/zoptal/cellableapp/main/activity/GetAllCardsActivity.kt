package com.zoptal.cellableapp.main.activity

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.annotation.Nullable
import com.google.gson.Gson
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.adapter.StripeListRvAdap
import com.zoptal.cellableapp.main.activity.CreateTokenActivity
import com.zoptal.cellableapp.main.activity.GetAllCardsActivity
import com.zoptal.cellableapp.pojo_class.card.CardList
import com.zoptal.cellableapp.pojo_class.card.CardMainPojo
import com.zoptal.cellableapp.utility.*
import com.zoptal.cellableapp.utility.OkHttp3Connection.OkHttp3RequestCallback
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class GetAllCardsActivity : AppCompatActivity(), View.OnClickListener {
    override fun onClick(v: View) {
        try {
            when (v.id) {
                R.id.fab_add_card -> {
                    val tokenIntent = Intent(this, CreateTokenActivity::class.java)
                    startActivity(tokenIntent)
                }
                R.id.rL_back_btn -> onBackPressed()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    var rv_payments: RecyclerView? = null
    var progress_bar_profile: ProgressBar? = null
    private var mSessionManager: SessionManager? = null
    var rL_rootElement: RelativeLayout? = null
    var tv_no_data: TextView? = null
    var currencyRvAdap: StripeListRvAdap? = null
    var alist = ArrayList<CardList>()
     override fun onCreate(@Nullable savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_get_cards)
        progress_bar_profile = findViewById(R.id.progress_bar_profile)
        rv_payments = findViewById(R.id.rv_payments) as RecyclerView?
        rL_rootElement = findViewById(R.id.rL_rootElement) as RelativeLayout?
        tv_no_data = findViewById(R.id.tv_no_data)
        mSessionManager = SessionManager(this)
    }

    override fun onResume() {
        super.onResume()
        cardList
    }

    //                    CommonClass.showSnackbarMessage(rL_rootElement,error);
    private val cardList: Unit
        private get() {
            progress_bar_profile!!.visibility = View.VISIBLE
            if (CommonClass.isNetworkAvailable(this)) {
                val request_datas = JSONObject()
                try {
                    request_datas.put("token", mSessionManager!!.authToken)
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
                val url = ApiUrl.GET_CARD
                OkHttp3Connection.doOkHttp3Connection(TAG, url, OkHttp3Connection.Request_type.POST, request_datas, object : OkHttp3RequestCallback {
                     override fun onSuccess(result: String?, user_tag: String?) {
                        progress_bar_profile!!.visibility = View.GONE
                        val cardMainPojo: CardMainPojo
                        val gson = Gson()
                        cardMainPojo = gson.fromJson(result, CardMainPojo::class.java)
                        if (cardMainPojo.code == "200") {
                            CommonClass.showToast(this@GetAllCardsActivity, cardMainPojo.message)
                            tv_no_data!!.visibility = View.GONE
                            alist = cardMainPojo.data!!
                            if (alist.size > 0) {
                                currencyRvAdap = StripeListRvAdap(this@GetAllCardsActivity, alist, object : CardItemClickListener {
                                    override fun onItemClick(pos: Int, bookmarkOrDelete: Int) {
                                        if (bookmarkOrDelete == 1) {
                                            setCardAsDefault(cardMainPojo.data!![pos]._id, pos)
                                        } else if (bookmarkOrDelete == 2) {
                                            createDialog(cardMainPojo.data!![pos]._id, pos)
                                        } else if (bookmarkOrDelete == 3 && getIntent().getIntExtra("screen_type", 0) === 2) {
                                            callPaymentApi(cardMainPojo.data!![pos]._id, pos)
                                        }
                                    }
                                })
                                val layoutManager = LinearLayoutManager(this@GetAllCardsActivity)
                                rv_payments?.setLayoutManager(layoutManager)
                                rv_payments?.setAdapter(currencyRvAdap)
                            }
                        } else {
                            onError(cardMainPojo.message, "")
                        }
                    }

                     override fun onError(error: String?, user_tag: String?) {
                        tv_no_data!!.visibility = View.VISIBLE
                        tv_no_data!!.text = error
                        progress_bar_profile!!.visibility = View.GONE
                        //                    CommonClass.showSnackbarMessage(rL_rootElement,error);
                    }
                })
            }
        }

    private fun createDialog(deleteId: String, pos: Int) {
        try {
            val builder: android.app.AlertDialog.Builder = android.app.AlertDialog.Builder(this@GetAllCardsActivity)
            builder.setTitle(getResources().getString(R.string.hint_confirm_delete))
            builder.setPositiveButton(R.string.ok, DialogInterface.OnClickListener { dialog, id -> deleteCard(deleteId, pos) })
            builder.setNegativeButton(R.string.cancel, DialogInterface.OnClickListener { dialog, id -> dialog.dismiss() })
            val dialog: android.app.AlertDialog? = builder.create()
            dialog?.show()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun deleteCard(id: String, pos: Int) {
        try {
            progress_bar_profile!!.visibility = View.VISIBLE
            if (CommonClass.isNetworkAvailable(this)) {
                val request_datas = JSONObject()
                try {
                    request_datas.put("token", mSessionManager!!.authToken)
                    request_datas.put("card_id", id)
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
                val url = ApiUrl.DELETE_CARD
                OkHttp3Connection.doOkHttp3Connection(TAG, url, OkHttp3Connection.Request_type.POST, request_datas, object : OkHttp3RequestCallback {
                     override fun onSuccess(result: String?, user_tag: String?) {
                        try {
                            progress_bar_profile!!.visibility = View.GONE
                            val cardMainPojo: CardMainPojo
                            val gson = Gson()
                            cardMainPojo = gson.fromJson(result, CardMainPojo::class.java)
                            CommonClass.showToast(this@GetAllCardsActivity, cardMainPojo.message)
                            tv_no_data!!.visibility = View.GONE
                            if (cardMainPojo.code == "200") {
                                if (currencyRvAdap != null) {
                                    alist.removeAt(pos)
                                    currencyRvAdap?.notifyItemRemoved(pos)
                                    rv_payments?.setAdapter(currencyRvAdap)
                                    if (alist.size == 0) {
                                        tv_no_data!!.visibility = View.VISIBLE
                                        tv_no_data?.setText(getResources().getString(R.string.err_stripe_no_card))
                                    } else {
                                        tv_no_data!!.visibility = View.GONE
                                    }
                                }
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                     override fun onError(error: String?, user_tag: String?) {
                        tv_no_data!!.visibility = View.VISIBLE
                        tv_no_data!!.text = error
                        progress_bar_profile!!.visibility = View.GONE
                        CommonClass.showSnackbarMessage(rL_rootElement, error)
                    }
                })
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun callPaymentApi(id: String, pos: Int) {
        try {
            progress_bar_profile!!.visibility = View.VISIBLE
            if (CommonClass.isNetworkAvailable(this)) {
                val request_datas = JSONObject()
                try {
                    request_datas.put("token", mSessionManager!!.authToken)
                    request_datas.put("postId", getIntent().getStringExtra("postId"))
                    //                    request_datas.put("price", getIntent().getStringExtra("productSelPrice"));
                    request_datas.put("card_id", id)
                    request_datas.put("type", getIntent().getStringExtra("postType"))
                    request_datas.put("pin_code", getIntent().getStringExtra("pinCode"))
                    request_datas.put("address_one", getIntent().getStringExtra("addressOne"))
                    request_datas.put("address_two", getIntent().getStringExtra("addressTwo"))
                    request_datas.put("country", getIntent().getStringExtra("country"))
                    request_datas.put("state", getIntent().getStringExtra("state"))
                    request_datas.put("town", getIntent().getStringExtra("city"))
                    request_datas.put("currency", getIntent().getStringExtra("currency"))
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
                val url = ApiUrl.MAKE_PAYMENT
                OkHttp3Connection.doOkHttp3Connection(TAG, url, OkHttp3Connection.Request_type.POST, request_datas, object : OkHttp3RequestCallback {
                     override fun onSuccess(result: String?, user_tag: String?) {
                        try {
                            progress_bar_profile!!.visibility = View.GONE
                            val cardMainPojo: CardMainPojo
                            val gson = Gson()
                            cardMainPojo = gson.fromJson(result, CardMainPojo::class.java)
                            CommonClass.showToast(this@GetAllCardsActivity, cardMainPojo.message)
                            tv_no_data!!.visibility = View.GONE
                            if (cardMainPojo.code == "200") {
                                val intent1 = Intent(this@GetAllCardsActivity, HomePageActivity::class.java)
                                intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                                startActivity(intent1)
                                finish()
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                     override fun onError(error: String?, user_tag: String?) {
                        tv_no_data!!.visibility = View.VISIBLE
                        tv_no_data!!.text = error
                        progress_bar_profile!!.visibility = View.GONE
                        CommonClass.showSnackbarMessage(rL_rootElement, error)
                    }
                })
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun setCardAsDefault(id: String, pos: Int) {
        try {
            progress_bar_profile!!.visibility = View.VISIBLE
            if (CommonClass.isNetworkAvailable(this)) {
                val request_datas = JSONObject()
                try {
                    request_datas.put("token", mSessionManager!!.authToken)
                    request_datas.put("card_id", id)
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
                val url = ApiUrl.SET_CARD_AS_DEFAULT
                OkHttp3Connection.doOkHttp3Connection(TAG, url, OkHttp3Connection.Request_type.POST, request_datas, object : OkHttp3RequestCallback {
                     override fun onSuccess(result: String?, user_tag: String?) {
                        try {
                            progress_bar_profile!!.visibility = View.GONE
                            val cardMainPojo: CardMainPojo
                            val gson = Gson()
                            cardMainPojo = gson.fromJson(result, CardMainPojo::class.java)
                            tv_no_data!!.visibility = View.GONE
                            if (cardMainPojo.code == "200") {
                                cardList
                            } else {
                                CommonClass.showToast(this@GetAllCardsActivity, cardMainPojo.message)
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                     override fun onError(error: String?, user_tag: String?) {
                        tv_no_data!!.visibility = View.VISIBLE
                        tv_no_data!!.text = error
                        progress_bar_profile!!.visibility = View.GONE
                        CommonClass.showSnackbarMessage(rL_rootElement, error)
                    }
                })
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    companion object {
        @JvmStatic
        fun callActivity(mContext: Context, screen_type: Int, addressOne: String?, addressTwo: String?, state: String?, city: String?, country: String?, pinCode: String?
                         , postId: String?, currency: String?, productSelPrice: String?, postType: String?) {
            val intent = Intent(mContext, GetAllCardsActivity::class.java)
            intent.putExtra("screen_type", screen_type) //1 means profile // 2means chat
            intent.putExtra("addressOne", addressOne)
            intent.putExtra("addressTwo", addressTwo)
            intent.putExtra("state", state)
            intent.putExtra("city", city)
            intent.putExtra("country", country)
            intent.putExtra("pinCode", pinCode)
            intent.putExtra("postId", postId)
            intent.putExtra("currency", currency)
            intent.putExtra("productSelPrice", productSelPrice)
            intent.putExtra("postType", postType)
            mContext.startActivity(intent)
        }

        private val TAG = GetAllCardsActivity::class.java.simpleName
    }
}