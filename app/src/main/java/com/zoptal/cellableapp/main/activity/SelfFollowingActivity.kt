package com.zoptal.cellableapp.main.activity

import android.app.Activity
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.localbroadcastmanager.content.LocalBroadcastManager

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.gson.Gson
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.adapter.UserFollowRvAdapter
import com.zoptal.cellableapp.fcm_push_notification.Config
import com.zoptal.cellableapp.fcm_push_notification.NotificationMessageDialog
import com.zoptal.cellableapp.fcm_push_notification.NotificationUtils.Companion.clearNotifications
import com.zoptal.cellableapp.main.activity.SelfFollowingActivity
import com.zoptal.cellableapp.pojo_class.user_follow_pojo.FollowMainPojo
import com.zoptal.cellableapp.pojo_class.user_follow_pojo.FollowResponseDatas
import com.zoptal.cellableapp.utility.*
import com.zoptal.cellableapp.utility.OkHttp3Connection.OkHttp3RequestCallback
import org.json.JSONException
import org.json.JSONObject
import java.util.*

/**
 * <h>SelfFollowingActivity</h>
 * @since 4/21/2017
 */
class SelfFollowingActivity : Activity(), View.OnClickListener {
    private var mProgressBar: ProgressBar? = null
    private var mActivity: Activity? = null
    private var mSessionManager: SessionManager? = null
    private var rL_rootview: RelativeLayout? = null
    private var rL_no_following: RelativeLayout? = null
    private var isFollower = false
    private var mSwipeRefreshLayout: SwipeRefreshLayout? = null
    private var arrayListFollow: ArrayList<FollowResponseDatas>? = null
    private var followRvAdapter: UserFollowRvAdapter? = null
    private var index = 0
    private var mNotificationMessageDialog: NotificationMessageDialog? = null
    override fun onCreate(savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_user_following)
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale)
        initVariables()
    }

    private fun initVariables() {
        // receiving values from last class
        val intent = intent
        val title = intent.getStringExtra("title")
        isFollower = intent.getBooleanExtra("isFollower", false)
        val followingCount = intent.getIntExtra("followingCount", 0)


        // initialize variables
        index = 0
        mActivity = this@SelfFollowingActivity
        mNotificationMessageDialog = NotificationMessageDialog(mActivity)
        CommonClass.statusBarColor(mActivity!!)
        arrayListFollow = ArrayList()
        mSessionManager = SessionManager(mActivity!!)
        val tV_title = findViewById<View>(R.id.tV_title) as TextView
        if (title != null) tV_title.text = title
        rL_rootview = findViewById<View>(R.id.rL_rootview) as RelativeLayout
        val rV_follow: RecyclerView = findViewById<View>(R.id.rV_follow) as RecyclerView
        mSwipeRefreshLayout = findViewById<View>(R.id.swipeRefreshLayout) as SwipeRefreshLayout
        mSwipeRefreshLayout!!.setColorSchemeResources(R.color.pink_color)
        mProgressBar = findViewById<View>(R.id.progress_bar_follow) as ProgressBar
        mProgressBar!!.visibility = View.GONE
        val rL_back_btn = findViewById<View>(R.id.rL_back_btn) as RelativeLayout
        rL_back_btn.setOnClickListener(this)

        // set recycler view adapter
        followRvAdapter = UserFollowRvAdapter(mActivity!!, arrayListFollow!!, followingCount)
        val layoutManager = LinearLayoutManager(mActivity)
        rV_follow.setLayoutManager(layoutManager)
        rV_follow.setAdapter(followRvAdapter)

        // No Following or follower default var
        val iV_following_icon = findViewById<View>(R.id.iV_following_icon) as ImageView
        val tV_no_following = findViewById<View>(R.id.tV_no_following) as TextView
        rL_no_following = findViewById<View>(R.id.rL_no_following) as RelativeLayout
        rL_no_following!!.visibility = View.GONE

        // set api url according to condition like follower or following
        val apiUrl: String
        if (isFollower) {
            apiUrl = ApiUrl.GET_FOLLOWER
            iV_following_icon.setImageResource(R.drawable.empty_follower)
            tV_no_following.text = resources.getString(R.string.no_follower_yet)
        } else {
            apiUrl = ApiUrl.GET_FOLLOWING
            iV_following_icon.setImageResource(R.drawable.empty_following)
            tV_no_following.text = resources.getString(R.string.no_following_yet)
        }
        mProgressBar!!.visibility = View.VISIBLE
        getFollowingService(index, apiUrl)

        // pull to refresh
        mSwipeRefreshLayout!!.setOnRefreshListener(object : SwipeRefreshLayout.OnRefreshListener {
            override fun onRefresh() {
                arrayListFollow!!.clear()
                getFollowingService(index, apiUrl)
            }
        })
    }

    override fun onResume() {
        super.onResume()

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.REGISTRATION_COMPLETE))

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.PUSH_NOTIFICATION))

        // clear the notification area when the app is opened
        clearNotifications(applicationContext)
    }

    override fun onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!)
        super.onPause()
    }

    private fun getFollowingService(offsetValue: Int, ApiUrl: String) {
        var offsetValue = offsetValue
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            val limit = 20
            offsetValue = offsetValue * limit
            val request_datas = JSONObject()
            try {
                request_datas.put("token", mSessionManager!!.authToken)
                request_datas.put("offset", offsetValue)
                request_datas.put("limit", limit)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            OkHttp3Connection.doOkHttp3Connection(TAG, ApiUrl, OkHttp3Connection.Request_type.POST, request_datas, object : OkHttp3RequestCallback {
                 override fun onSuccess(result: String?, user_tag: String?) {
                    mProgressBar!!.visibility = View.GONE
                    mSwipeRefreshLayout!!.setRefreshing(false)
                    val followMainPojo: FollowMainPojo
                    val gson = Gson()
                    followMainPojo = gson.fromJson(result, FollowMainPojo::class.java)
                    when (followMainPojo.code) {
                        "200" -> {
                            if (isFollower) arrayListFollow!!.addAll(followMainPojo.followers!!) else arrayListFollow!!.addAll(followMainPojo.result!!)
                            if (arrayListFollow != null && arrayListFollow!!.size > 0) {
                                followRvAdapter!!.notifyDataSetChanged()
                            }
                        }
                        "201321" -> rL_no_following!!.visibility = View.VISIBLE
                        "201322" -> rL_no_following!!.visibility = View.VISIBLE
                        "401" -> CommonClass.sessionExpired(mActivity!!)
                        else -> CommonClass.showSnackbarMessage(rL_rootview, followMainPojo.message)
                    }
                }

                 override fun onError(error: String?, user_tag: String?) {
                    mProgressBar!!.visibility = View.GONE
                }
            })
        } else CommonClass.showSnackbarMessage(rL_rootview, resources.getString(R.string.NoInternetAccess))
    }

    override fun onBackPressed() {
        println(TAG + " " + "followingCount back press=" + followRvAdapter!!.followingCount)
        val intent = Intent()
        intent.putExtra("followingCount", followRvAdapter!!.followingCount)
        setResult(VariableConstants.FOLLOW_COUNT_REQ_CODE, intent)
        finish()
        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.rL_back_btn -> onBackPressed()
        }
    }

    companion object {
        private val TAG = SelfFollowingActivity::class.java.simpleName
    }
}