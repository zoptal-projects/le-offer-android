package com.zoptal.cellableapp.main.activity

import android.app.Activity
import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import androidx.fragment.app.Fragment

import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignInResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.gson.Gson
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.pojo_class.verify_gplus_pojo.VerifyLoginWithGooglePojo
import com.zoptal.cellableapp.utility.*
import com.zoptal.cellableapp.utility.OkHttp3Connection.OkHttp3RequestCallback
import org.json.JSONException
import org.json.JSONObject

/**
 * <h>VerifyLoginWithGplus</h>
 *
 *
 * In this class we used to do login with google plus and verification with g plus.
 *
 * @since 12-Jul-17.
 */
class VerifyLoginWithGplus(private val mActivity: Activity, private val mGoogleApiClient: GoogleApiClient, private val rootView: View, private val pBar_gPlusVerify: ProgressBar, private val iV_gPlusIcon: ImageView, private var gPlusVerified: String) {
    private val mSessionManager: SessionManager
    fun signInWithGoogle(fragment: Fragment?, mActivity: Activity) {
        val signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient)
        if (fragment != null) fragment.startActivityForResult(signInIntent, VariableConstants.GOOGLE_LOGIN_REQ_CODE) else mActivity.startActivityForResult(signInIntent, VariableConstants.GOOGLE_LOGIN_REQ_CODE)
    }

    fun onActivityResult(data: Intent?) {
        val result = Auth.GoogleSignInApi.getSignInResultFromIntent(data)
        handleSignInResult(result)
    }

    private fun handleSignInResult(result: GoogleSignInResult?) {
        Log.d(TAG, "handleSignInResult:" + result!!.isSuccess)
        if (result.isSuccess) {
            // Signed in successfully, show authenticated UI.
            val acct = result.signInAccount!!
            Log.e(TAG, "display name: " + acct.displayName)
            val googleId = acct.id
            val idToken = acct.idToken
            val serverAuthCode = acct.serverAuthCode
            emailVerification(serverAuthCode, googleId)
        }
    }

    private fun emailVerification(accessToken: String?, googleId: String?) {
        if (CommonClass.isNetworkAvailable(mActivity)) {
            pBar_gPlusVerify.visibility = View.VISIBLE
            iV_gPlusIcon.visibility = View.GONE
            // token,accessToken,googleId
            val request_datas = JSONObject()
            try {
                request_datas.put("token", mSessionManager.authToken)
                request_datas.put("accessToken", accessToken)
                request_datas.put("googleId", googleId)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            OkHttp3Connection.doOkHttp3Connection(TAG, ApiUrl.VERIFY_GOOGLE_PLUS, OkHttp3Connection.Request_type.POST, request_datas, object : OkHttp3RequestCallback {
                 override fun onSuccess(result: String?, user_tag: String?) {
                    println("$TAG email verified res=$result")
                    pBar_gPlusVerify.visibility = View.GONE
                    iV_gPlusIcon.visibility = View.VISIBLE
                    val loginWithGooglePojo: VerifyLoginWithGooglePojo
                    val gson = Gson()
                    loginWithGooglePojo = gson.fromJson(result, VerifyLoginWithGooglePojo::class.java)
                    when (loginWithGooglePojo.code) {
                        "200" -> {
                            gPlusVerified = "1"
                            iV_gPlusIcon.setImageResource(R.drawable.gplus_verified_icon)
                        }
                        "401" -> CommonClass.sessionExpired(mActivity)
                        else -> {
                            gPlusVerified = "0"
                            CommonClass.showSnackbarMessage(rootView, loginWithGooglePojo.message)
                        }
                    }
                }

                 override fun onError(error: String?, user_tag: String?) {
                    pBar_gPlusVerify.visibility = View.GONE
                    iV_gPlusIcon.visibility = View.VISIBLE
                }
            })
        } else CommonClass.showSnackbarMessage(rootView, mActivity.resources.getString(R.string.NoInternetAccess))
    }

    companion object {
        private val TAG = VerifyLoginWithGplus::class.java.simpleName
    }

    init {
        mSessionManager = SessionManager(mActivity)
    }
}