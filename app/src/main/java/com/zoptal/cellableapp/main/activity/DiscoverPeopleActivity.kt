package com.zoptal.cellableapp.main.activity

import android.app.Activity
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.widget.ProgressBar
import android.widget.RelativeLayout
import androidx.annotation.Nullable
import androidx.core.content.ContextCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout

import com.google.gson.Gson
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.adapter.DiscoveryPeopleRvAdapter
import com.zoptal.cellableapp.fcm_push_notification.Config
import com.zoptal.cellableapp.fcm_push_notification.NotificationMessageDialog
import com.zoptal.cellableapp.fcm_push_notification.NotificationUtils.Companion.clearNotifications
import com.zoptal.cellableapp.main.activity.DiscoverPeopleActivity
import com.zoptal.cellableapp.pojo_class.discovery_people_pojo.DiscoverPeopleMainPojo
import com.zoptal.cellableapp.pojo_class.discovery_people_pojo.DiscoverPeopleResponse
import com.zoptal.cellableapp.utility.*
import com.zoptal.cellableapp.utility.OkHttp3Connection.OkHttp3RequestCallback
import org.json.JSONException
import org.json.JSONObject
import java.util.*

/**
 * <h>DiscoverPeopleActivity</h>
 *
 *
 * In this class we used to show the users and its posts.
 *
 * @since 4/26/2017
 */
class DiscoverPeopleActivity  : AppCompatActivity(), View.OnClickListener {
    private var mActivity: Activity? = null
    private var mSessionManager: SessionManager? = null
    private var mProgressBar: ProgressBar? = null
    private var rL_rootview: RelativeLayout? = null
    private var rV_discover_people: RecyclerView? = null
    private var mSwipeRefreshLayout: SwipeRefreshLayout? = null
    private var arrayListDiscoverData: ArrayList<DiscoverPeopleResponse>? = null
    private var peopleRvAdapter: DiscoveryPeopleRvAdapter? = null
    private var mLayoutManager: LinearLayoutManager? = null
    private var pageIndex: Int = 0

    // Load more
    private var totalItemCount: Int = 0
    private var totalVisibleItem: Int = 0
    private val visibleTheshold: Int = 5
    private var isLoadMoreNeeded: Boolean = false
    private var mNotificationMessageDialog: NotificationMessageDialog? = null
     override fun onCreate(@Nullable savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_discover_people)
        overridePendingTransition(R.anim.slide_up, R.anim.stay)
        pageIndex = 0
        mActivity = this@DiscoverPeopleActivity
        mNotificationMessageDialog = NotificationMessageDialog(mActivity)

        // receiving datas from last activty
        val intent: Intent = getIntent()
        val followingCount: Int = intent.getIntExtra("followingCount", 0)
        println(TAG + " " + "followingCount=" + followingCount)

        // Initialize data member
        arrayListDiscoverData = ArrayList()
        CommonClass.statusBarColor(mActivity!!)
        mSessionManager = SessionManager(mActivity!!)
        mProgressBar = findViewById(R.id.progress_bar) as ProgressBar?
        rL_rootview = findViewById(R.id.rL_rootview) as RelativeLayout?
        val rL_back_btn: RelativeLayout = findViewById(R.id.rL_back_btn) as RelativeLayout
        rL_back_btn.setOnClickListener(this)
        rV_discover_people = findViewById(R.id.rV_discover_people) as RecyclerView?
        mSwipeRefreshLayout = findViewById(R.id.swipeRefreshLayout) as SwipeRefreshLayout?
        mSwipeRefreshLayout!!.setColorSchemeResources(R.color.pink_color)

        // adapter
        peopleRvAdapter = DiscoveryPeopleRvAdapter(mActivity!!, arrayListDiscoverData!!, followingCount)
        mLayoutManager = LinearLayoutManager(mActivity)
        rV_discover_people!!.setLayoutManager(mLayoutManager)
        rV_discover_people!!.setAdapter(peopleRvAdapter)

        // pull to refresh
        mSwipeRefreshLayout!!.setOnRefreshListener(object : SwipeRefreshLayout.OnRefreshListener {
            override fun onRefresh() {
                arrayListDiscoverData!!.clear()
                pageIndex = 0
                discoverPeopleApi(pageIndex)
            }
        })

        // call api when open the screen
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            mProgressBar!!.setVisibility(View.VISIBLE)
            discoverPeopleApi(pageIndex)
        }
    }

    override fun onResume() {
        super.onResume()

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.REGISTRATION_COMPLETE))

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.PUSH_NOTIFICATION))

        // clear the notification area when the app is opened
        clearNotifications(getApplicationContext())
    }

   override fun onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!)
        super.onPause()
    }

    /**
     * <h>DiscoverPeopleApi</h>
     *
     *
     * In this method we used to do api call to get list of registered users and there posts.
     *
     * @param offset The page index
     */
    private fun discoverPeopleApi(offset: Int) {
        var offset: Int = offset
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            val limit: Int = 20
            offset = limit * offset
            val request_datas: JSONObject = JSONObject()
            try {
                request_datas.put("token", mSessionManager!!.authToken)
                request_datas.put("offset", offset)
                request_datas.put("limit", limit)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            println(TAG + " " + "offset=" + offset)
            OkHttp3Connection.doOkHttp3Connection(TAG, ApiUrl.DISCOVER_PEOPLE, OkHttp3Connection.Request_type.POST, request_datas, object : OkHttp3RequestCallback {
                public  override fun onSuccess(result: String?, user_tag: String?) {
                    mProgressBar!!.setVisibility(View.GONE)
                    println(TAG + " " + "discovery people res=" + result)
                    mSwipeRefreshLayout!!.setRefreshing(false)
                    val gson: Gson = Gson()
                    val discoverPeoplePojo: DiscoverPeopleMainPojo = gson.fromJson(result, DiscoverPeopleMainPojo::class.java)
                    when (discoverPeoplePojo.code) {
                        "200" -> if (discoverPeoplePojo.discoverData != null && discoverPeoplePojo.discoverData!!.size > 0) {
                            arrayListDiscoverData!!.addAll(discoverPeoplePojo.discoverData!!)
                            isLoadMoreNeeded = arrayListDiscoverData!!.size > 15
                            peopleRvAdapter!!.notifyDataSetChanged()

                            // set facebook and contact count
                            val contactCount: Int = mSessionManager!!.contectFriendCount
                            if (contactCount > 0) {
                                peopleRvAdapter!!.tV_contact_friend_count!!.setTextColor(ContextCompat.getColor((mActivity)!!, R.color.pink_color))
                                val setContactFrdCount: String
                                if (contactCount > 1) setContactFrdCount = contactCount.toString() + " " + getResources().getString(R.string.friends) else setContactFrdCount = contactCount.toString() + " " + getResources().getString(R.string.friend)
                                peopleRvAdapter!!.tV_contact_friend_count!!.setText(setContactFrdCount)
                            }
                            val fb_friend_count: Int = mSessionManager!!.fbFriendCount
                            println(TAG + " " + "fb_friend_count=" + fb_friend_count)
                            if (fb_friend_count > 0) {
                                peopleRvAdapter!!.tV_fb_friends_count!!.setTextColor(ContextCompat.getColor((mActivity)!!, R.color.pink_color))
                                val setContactFrdCount: String
                                if (fb_friend_count > 1) setContactFrdCount = fb_friend_count.toString() + " " + getResources().getString(R.string.friends) else setContactFrdCount = fb_friend_count.toString() + " " + getResources().getString(R.string.friend)
                                peopleRvAdapter!!.tV_fb_friends_count!!.setText(setContactFrdCount)
                            }

                            // Load more
                            rV_discover_people!!.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                                    super.onScrolled(recyclerView, dx, dy)
                                    totalItemCount = mLayoutManager!!.getItemCount()
                                    totalVisibleItem = mLayoutManager!!.findLastVisibleItemPosition()
                                    println(TAG + " " + "isLoadMoreNeeded=" + isLoadMoreNeeded + " " + "total item count=" + totalItemCount + " " + "total visible=" + totalVisibleItem + " " + "visible threshold=" + visibleTheshold)
                                    if (isLoadMoreNeeded && totalItemCount <= (visibleTheshold + totalVisibleItem)) {
                                        println(TAG + " " + "loading more...")
                                        isLoadMoreNeeded = false
                                        pageIndex = pageIndex + 1
                                        mSwipeRefreshLayout!!.setRefreshing(true)
                                        discoverPeopleApi(pageIndex)
                                    }
                                }
                            })
                        }
                        "401" -> CommonClass.sessionExpired(mActivity!!)
                        else -> CommonClass.showSnackbarMessage(rL_rootview, discoverPeoplePojo.message)
                    }
                }

                public  override fun onError(error: String?, user_tag: String?) {
                    mSwipeRefreshLayout!!.setRefreshing(false)
                    mProgressBar!!.setVisibility(View.GONE)
                    CommonClass.showSnackbarMessage(rL_rootview, error)
                }
            })
        } else CommonClass.showSnackbarMessage(rL_rootview, getResources().getString(R.string.NoInternetAccess))
    }

 override fun onBackPressed() {
        println(TAG + " " + "followingCount back press=" + peopleRvAdapter!!.followingCount)
        val intent: Intent = Intent()
        intent.putExtra("followingCount", peopleRvAdapter!!.followingCount)
        setResult(VariableConstants.FOLLOW_COUNT_REQ_CODE, intent)
        finish()
        overridePendingTransition(R.anim.stay, R.anim.slide_down)
    }

    public override fun onClick(v: View) {
        when (v.getId()) {
            R.id.rL_back_btn -> onBackPressed()
        }
    }

   override  protected fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (data != null) {
            val intFollowingCount: Int
            when (requestCode) {
                VariableConstants.CONTACT_FRIEND_REQ_CODE -> {
                    val contact_friend_count: Int = data.getIntExtra("contact_count", 0)
                    intFollowingCount = data.getIntExtra("followingCount", 0)
                    peopleRvAdapter!!.followingCount = intFollowingCount
                    println(TAG + " " + "contact_friend_count=" + contact_friend_count + " " + "following count=" + intFollowingCount)
                    if (contact_friend_count > 0) {
                        peopleRvAdapter!!.tV_contact_friend_count!!.setTextColor(ContextCompat.getColor((mActivity)!!, R.color.pink_color))
                        val setContactFrdCount: String
                        if (contact_friend_count > 1) setContactFrdCount = contact_friend_count.toString() + " " + getResources().getString(R.string.friends) else setContactFrdCount = contact_friend_count.toString() + " " + getResources().getString(R.string.friend)
                        peopleRvAdapter!!.tV_contact_friend_count!!.setText(setContactFrdCount)
                    }
                }
                VariableConstants.FB_FRIEND_REQ_CODE -> {
                    val fb_friend_count: Int = data.getIntExtra("fb_count", 0)
                    intFollowingCount = data.getIntExtra("followingCount", 0)
                    peopleRvAdapter!!.followingCount = intFollowingCount
                    println(TAG + " " + "fb_friend_count=" + fb_friend_count + " " + "following count=" + intFollowingCount)
                    if (fb_friend_count > 0) {
                        peopleRvAdapter!!.tV_fb_friends_count!!.setTextColor(ContextCompat.getColor((mActivity)!!, R.color.pink_color))
                        val setContactFrdCount: String
                        if (fb_friend_count > 1) setContactFrdCount = fb_friend_count.toString() + " " + getResources().getString(R.string.friends) else setContactFrdCount = fb_friend_count.toString() + " " + getResources().getString(R.string.friend)
                        peopleRvAdapter!!.tV_fb_friends_count!!.setText(setContactFrdCount)
                    }
                }
            }
        }
    }

    companion object {
        private val TAG: String = DiscoverPeopleActivity::class.java.getSimpleName()
    }
}