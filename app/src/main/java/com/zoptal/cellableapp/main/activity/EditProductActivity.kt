package com.zoptal.cellableapp.main.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.annotation.Nullable
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout

import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.adapter.AcceptedOfferRvAdap
import com.zoptal.cellableapp.main.activity.EditProductActivity
import com.zoptal.cellableapp.main.activity.products.ProductDetailsActivity
import com.zoptal.cellableapp.pojo_class.accepted_offer.AcceptedOfferDatas
import com.zoptal.cellableapp.pojo_class.accepted_offer.AcceptedOfferMainPojo
import com.zoptal.cellableapp.utility.*
import com.zoptal.cellableapp.utility.OkHttp3Connection.OkHttp3RequestCallback
import org.json.JSONException
import org.json.JSONObject
import java.util.*

/**
 * <h>EditProductActivity</h>
 *
 *
 * In this class we show the all accepted offer for particular product. We also have option
 * to edit the product structure and can make product as mark as sold.
 *
 * @since 13-Jul-17
 */
class EditProductActivity : AppCompatActivity(), View.OnClickListener {
    private var mActivity: Activity? = null
    private var rL_rootElement: RelativeLayout? = null
    private var rL_empty_offer: RelativeLayout? = null
    private var mProgressBar: ProgressBar? = null
    private var arrayListAcceptedOffer: ArrayList<AcceptedOfferDatas>? = null
    private var mSessionManager: SessionManager? = null
    private var rV_acceptedOffer: RecyclerView? = null
    private var mLinearLayoutManager: LinearLayoutManager? = null
    private var acceptedOfferRvAdap: AcceptedOfferRvAdap? = null
    private var postId: String? = null
    private var pageIndex = 0
    private var mSwipeRefreshLayout: SwipeRefreshLayout? = null
    private var view2: View? = null
    private var mDialogBox: DialogBox? = null
    private var bundlePostDatas: Bundle? = null
    private var isFromProductDetail = false

    // Load more
    private var isLoadMoreReq = false
    private var totalItemCount = 0
    private val visibleThreshold = 5
    private var visibleCount = 0
     override fun onCreate(@Nullable savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_edit_product)
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale)
        initVariables()
    }

    /**
     * <h>InitVariables</h>
     *
     *
     * In this method we used to assign all the xml variable and data member.
     *
     */
    private fun initVariables() {
        mActivity = this@EditProductActivity
        mDialogBox = DialogBox(mActivity!!)

        //Get the bundlePostDatas
        var productName: String? = ""
        var productImage: String? = ""
        var category: String? = ""
        bundlePostDatas = getIntent().getExtras()
        if (bundlePostDatas != null) {
            postId = bundlePostDatas!!.getString("postId")
            productImage = bundlePostDatas!!.getString("productImage")
            productName = bundlePostDatas!!.getString("productName")
            category = bundlePostDatas!!.getString("category")
            isFromProductDetail = bundlePostDatas!!.getBoolean("isFromProductDetail", false)
        }
        println("$TAG product image=$productImage")
        pageIndex = 0
        view2 = findViewById(R.id.view2)
        mSessionManager = SessionManager(mActivity!!)
        rL_rootElement = findViewById(R.id.rL_rootElement) as RelativeLayout?
        rL_empty_offer = findViewById(R.id.rL_empty_offer) as RelativeLayout?
        mProgressBar = findViewById(R.id.progress_bar) as ProgressBar?
        rV_acceptedOffer = findViewById(R.id.rV_acceptedOffer) as RecyclerView?
        mSwipeRefreshLayout = findViewById(R.id.swipeRefreshLayout) as SwipeRefreshLayout?
        mSwipeRefreshLayout!!.setColorSchemeResources(R.color.pink_color)
        arrayListAcceptedOffer = ArrayList()

        // promote item
        val tV_promote_item = findViewById(R.id.tV_promote_item) as TextView
        tV_promote_item.setOnClickListener(this)

        // Back button
        val rL_back_btn = findViewById(R.id.rL_back_btn) as RelativeLayout
        rL_back_btn.setOnClickListener(this)

        // set values to product info
        val iV_productImage = findViewById(R.id.iV_productImage) as ImageView
        iV_productImage.setOnClickListener(this)
        if (productImage != null && !productImage.isEmpty()) Glide.with(mActivity!!)
                .load(productImage)
                .centerCrop()
                .placeholder(R.drawable.default_image)
                .error(R.drawable.default_image)
                .into(iV_productImage)

        // set product name
        val tV_productname = findViewById(R.id.tV_productname) as TextView
        tV_productname.setOnClickListener(this)
        if (productName != null && !productName.isEmpty()) {
            productName = productName.substring(0, 1).toUpperCase() + productName.substring(1).toLowerCase()
            tV_productname.text = productName
        }

        // category
        val tV_category = findViewById(R.id.tV_category) as TextView
        if (category != null && !category.isEmpty()) {
            category = category.substring(0, 1).toUpperCase() + category.substring(1).toLowerCase()
            tV_category.text = category
        }

        // Accepted offer recycler view adapter
        acceptedOfferRvAdap = AcceptedOfferRvAdap(mActivity!!, arrayListAcceptedOffer!!)
        mLinearLayoutManager = LinearLayoutManager(mActivity)
        rV_acceptedOffer!!.setLayoutManager(mLinearLayoutManager)
        rV_acceptedOffer!!.setAdapter(acceptedOfferRvAdap)

        // call api call method to get all offer price
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            mProgressBar!!.visibility = View.VISIBLE
            acceptedOffersApi(pageIndex)
        } else CommonClass.showSnackbarMessage(rL_rootElement, getResources().getString(R.string.NoInternetAccess))

        // pull to refresh
        mSwipeRefreshLayout!!.setOnRefreshListener(object : SwipeRefreshLayout.OnRefreshListener {
           override fun onRefresh() {
                mSwipeRefreshLayout!!.setRefreshing(true)
                arrayListAcceptedOffer!!.clear()
                pageIndex = 0
                acceptedOffersApi(pageIndex)
            }
        })

        // Mark as sold
//        RelativeLayout rL_markAsSold= (RelativeLayout) findViewById(R.id.rL_markAsSold);
//        rL_markAsSold.setOnClickListener(this);

        // insight
        val tV_insight = findViewById(R.id.tV_insight) as TextView
        tV_insight.setOnClickListener {
            val insightIntent = Intent(mActivity, InsightActivity::class.java)
            insightIntent.putExtra("postId", postId)
            startActivity(insightIntent)
        }

        // Update product
        val rL_update_product = findViewById(R.id.rL_update_product) as RelativeLayout
        rL_update_product.setOnClickListener(this)
    }

    /**
     * <h>AcceptedOffersApi</h>
     *
     *
     * In this method we used to do api call to get all accepted offer for a
     * product.
     *
     * @param offset The page index
     */
    private fun acceptedOffersApi(offset: Int) {
        var offset = offset
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            val limit = 20
            offset = offset * limit
            val request_datas = JSONObject()
            try {
                request_datas.put("token", mSessionManager!!.authToken)
                request_datas.put("postId", postId)
                request_datas.put("postType", "0")
                request_datas.put("limit", limit)
                request_datas.put("offset", offset)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            OkHttp3Connection.doOkHttp3Connection(TAG, ApiUrl.ACCEPTED_OFFER, OkHttp3Connection.Request_type.POST, request_datas, object : OkHttp3RequestCallback {
                 override fun onSuccess(result: String?, user_tag: String?) {
                    mProgressBar!!.visibility = View.GONE
                    mSwipeRefreshLayout!!.setRefreshing(false)
                    println("$TAG accepted offer=$result")
                    val acceptedOfferMainPojo: AcceptedOfferMainPojo
                    val gson = Gson()
                    acceptedOfferMainPojo = gson.fromJson(result, AcceptedOfferMainPojo::class.java)
                    when (acceptedOfferMainPojo.code) {
                        "200" -> if (acceptedOfferMainPojo.data != null && acceptedOfferMainPojo.data!!.size > 0) {
                            arrayListAcceptedOffer!!.addAll(acceptedOfferMainPojo.data!!)
                            isLoadMoreReq = arrayListAcceptedOffer!!.size > 14
                            acceptedOfferRvAdap!!.notifyDataSetChanged()

                            // Load more
                            rV_acceptedOffer!!.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                                    super.onScrolled(recyclerView, dx, dy)
                                    totalItemCount = mLinearLayoutManager!!.getItemCount()
                                    visibleCount = mLinearLayoutManager!!.findLastVisibleItemPosition()
                                    if (isLoadMoreReq && totalItemCount <= visibleCount + visibleThreshold) {
                                        isLoadMoreReq = false
                                        pageIndex = pageIndex + 1
                                        mSwipeRefreshLayout!!.setRefreshing(true)
                                        acceptedOffersApi(pageIndex)
                                    }
                                }
                            })
                        }
                        "204" -> {
                            view2!!.visibility = View.GONE
                            rL_empty_offer!!.visibility = View.VISIBLE
                        }
                        "401" -> CommonClass.sessionExpired(mActivity!!)
                        else -> CommonClass.showSnackbarMessage(rL_rootElement, acceptedOfferMainPojo.messgae)
                    }
                }

                 override fun onError(error: String?, user_tag: String?) {
                    mSwipeRefreshLayout!!.setRefreshing(false)
                    mProgressBar!!.visibility = View.GONE
                    CommonClass.showSnackbarMessage(rL_rootElement, error)
                }
            })
        } else CommonClass.showSnackbarMessage(rL_rootElement, getResources().getString(R.string.NoInternetAccess))
    }

 override fun onBackPressed() {
        finish()
        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.rL_back_btn -> onBackPressed()
            R.id.rL_update_product -> mDialogBox!!.openUpdateProductDialog(postId, rL_rootElement, bundlePostDatas)
            R.id.tV_promote_item -> {
                val intent = Intent(mActivity, PromoteItemActivity::class.java)
                intent.putExtra("price", bundlePostDatas!!.getString("price"))
                intent.putExtra("currency", bundlePostDatas!!.getString("currency"))
                intent.putExtra("productName", bundlePostDatas!!.getString("productName"))
                intent.putExtra("productImage", bundlePostDatas!!.getString("productImage"))
                intent.putExtra("productId", postId)
                intent.putExtra("isPromoted", bundlePostDatas!!.getInt("isPromoted"))
                startActivity(intent)
            }
            R.id.iV_productImage -> if (isFromProductDetail) {
                onBackPressed()
            } else {
                val i = Intent(mActivity, ProductDetailsActivity::class.java)
                i.putExtras(bundlePostDatas!!)
                startActivity(i)
                finish()
            }
            R.id.tV_productname -> if (isFromProductDetail) {
                onBackPressed()
            } else {
                val i = Intent(mActivity, ProductDetailsActivity::class.java)
                i.putExtras(bundlePostDatas!!)
                startActivity(i)
                finish()
            }
        }
    }

   override  protected fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        println("$TAG requestCode=$requestCode resultCode=$resultCode data=$data")
        if (data != null) {
            when (requestCode) {
                VariableConstants.SELLING_REQ_CODE -> {
                    val isToSellItAgain = data.getBooleanExtra("isToSellItAgain", false)
                    if (isToSellItAgain) {
                        val intent = Intent()
                        intent.putExtra("isToSellItAgain", true)
                        setResult(VariableConstants.SELLING_REQ_CODE, intent)
                        finish()
                    }
                    val isToFinishEditPost = data.getBooleanExtra("isToFinishEditPost", false)
                    if (isToFinishEditPost) finish()
                }
            }
        }
    }

    companion object {
        private val TAG = EditProductActivity::class.java.simpleName
    }
}