package com.zoptal.cellableapp.main.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.View
import androidx.annotation.Nullable
import androidx.fragment.app.FragmentTransaction

import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.main.view_pager.my_profile_frag.PurchasedFrag

class PurchasedListActivity : AppCompatActivity(), View.OnClickListener {
    override fun onClick(v: View) {
        try {
            when (v.id) {
                R.id.rL_back_btn -> onBackPressed()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

     override fun onCreate(@Nullable savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_purchased_list)
        val transaction: FragmentTransaction = getSupportFragmentManager().beginTransaction()
        val myProfileFrag = PurchasedFrag()
        if (myProfileFrag.isAdded()) {
            transaction.hide(myProfileFrag)
        }
        transaction.add(R.id.frame_layout, myProfileFrag)
        transaction.commit()
    }
}