package com.zoptal.cellableapp.main.view_pager.my_profile_frag

import android.app.Activity
import android.content.Intent
import android.os.Bundle

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.annotation.Nullable
import androidx.core.app.ActivityOptionsCompat
import androidx.core.view.ViewCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout

import com.google.gson.Gson
import com.squareup.otto.Subscribe
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.Uploader.ProductImageDatas
import com.zoptal.cellableapp.adapter.ProfileSellingFragRvAdap
import com.zoptal.cellableapp.event_bus.BusProvider.instance
import com.zoptal.cellableapp.main.activity.EditProductActivity
import com.zoptal.cellableapp.main.activity.HomePageActivity
import com.zoptal.cellableapp.main.activity.products.ProductDetailsActivity
import com.zoptal.cellableapp.main.camera_kit.CameraKitActivity
import com.zoptal.cellableapp.pojo_class.profile_selling_pojo.ProfileSellingData
import com.zoptal.cellableapp.pojo_class.profile_selling_pojo.ProfileSellingMainPojo
import com.zoptal.cellableapp.utility.*
import com.zoptal.cellableapp.utility.OkHttp3Connection.OkHttp3RequestCallback
import org.json.JSONException
import org.json.JSONObject
import java.util.*

/**
 * <h>SellingFrag</h>
 *
 *
 * In this class we used to show all the product which is posted by user.
 *
 * @since 4/7/2017
 * @author 3Embed
 */
class SellingFrag : Fragment(), ProductItemClickListener {
    private var mActivity: Activity? = null
    private var mSessionManager: SessionManager? = null
    private var progress_bar_profile: ProgressBar? = null
    private var rL_noProductFound: RelativeLayout? = null
    private var arrayListSellingDatas: ArrayList<ProfileSellingData>? = null
    private var sellingRvAdapter: ProfileSellingFragRvAdap? = null
    private var rV_selling: RecyclerView? = null
    private var gridLayoutManager: StaggeredGridLayoutManager? = null
    private var memberName: String? = null
    private var mSwipeRefreshLayout: SwipeRefreshLayout? = null
    private var pageIndex = 0

    // Load more var
    private var isLoadingRequired = false
    private var isToCallFirstTime = false
    private var isFromMyProfile = false
    private var visibleItemCount = 0
    private var totalItemCount = 0
    private val visibleThreshold = 5
    override fun onCreate(@Nullable savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState ?: Bundle())
        mActivity = getActivity()
        mSessionManager = SessionManager(mActivity!!)
        memberName = getArguments()!!.getString("memberName")
        isFromMyProfile = getArguments()!!.getBoolean("isFromMyProfileFlag", false)
        isToCallFirstTime = true
        println("$TAG isFromMyProfile=$isFromMyProfile")
    }

    @Nullable
    override fun onCreateView(inflater: LayoutInflater, @Nullable container: ViewGroup?, @Nullable savedInstanceState: Bundle?): View {
        val view = inflater.inflate(R.layout.frag_profile_buying, container, false)
        rV_selling = view.findViewById<View>(R.id.rV_myprofile_selling) as RecyclerView
        pageIndex = 0
        // set space equility between recycler view items
        val spanCount = 2 // 2 columns
        val spacing = CommonClass.dpToPx(mActivity!!, 12) // convert 12dp to pixel
        rV_selling!!.addItemDecoration(SpacesItemDecoration(spanCount, spacing))
        mSwipeRefreshLayout = view.findViewById<View>(R.id.swipeRefreshLayout) as SwipeRefreshLayout
        progress_bar_profile = view.findViewById<View>(R.id.progress_bar_profile) as ProgressBar
        arrayListSellingDatas = ArrayList()
        sellingRvAdapter = ProfileSellingFragRvAdap(mActivity!!, arrayListSellingDatas!!, this)
        gridLayoutManager = StaggeredGridLayoutManager(2, 1)
        rV_selling!!.setLayoutManager(gridLayoutManager)
        rV_selling!!.setAdapter(sellingRvAdapter)

        // set empty favourite icon
        rL_noProductFound = view.findViewById<View>(R.id.rL_noProductFound) as RelativeLayout
        rL_noProductFound!!.visibility = View.GONE
        val iV_default_icon = view.findViewById<View>(R.id.iV_default_icon) as ImageView
        iV_default_icon.setImageResource(R.drawable.empty_selling_icon)
        val tV_no_ads = view.findViewById<View>(R.id.tV_no_ads) as TextView
        tV_no_ads.setText(getResources().getString(R.string.no_ads_yet))
        val tV_snapNpost = view.findViewById<View>(R.id.tV_snapNpost) as TextView
        tV_snapNpost.setText(getResources().getString(R.string.snapNpostIn))
        val tV_start_discovering = view.findViewById<View>(R.id.tV_start_discovering) as TextView
        tV_start_discovering.setText(getResources().getString(R.string.start_selling))
        val rL_start_selling = view.findViewById<View>(R.id.rL_start_selling) as RelativeLayout
        if (!isFromMyProfile) rL_start_selling.visibility = View.GONE else rL_start_selling.visibility = View.VISIBLE
        rL_start_selling.setOnClickListener { //startActivity(new Intent(mActivity, CameraActivity.class));

            /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                    startActivity(new Intent(mActivity, Camera2Activity.class));
                else*/
            startActivity(Intent(mActivity, CameraKitActivity::class.java))
        }

        // call api call method
        if (CommonClass.isNetworkAvailable(mActivity!!) && isToCallFirstTime) {
            progress_bar_profile!!.visibility = View.VISIBLE
            if (mSessionManager!!.isUserLoggedIn) {
                profilePosts(pageIndex)
            } else {
                guestProfilePosts(pageIndex)
            }
        } else CommonClass.showSnackbarMessage((mActivity as HomePageActivity?)!!.rL_rootElement, getResources().getString(R.string.NoInternetAccess))

        // pull to refresh
        mSwipeRefreshLayout!!.setColorSchemeResources(R.color.pink_color)
        mSwipeRefreshLayout!!.setOnRefreshListener(object : SwipeRefreshLayout.OnRefreshListener {
            override fun onRefresh() {
                pageIndex = 0
                arrayListSellingDatas!!.clear()
                sellingRvAdapter!!.notifyDataSetChanged()
                if (mSessionManager!!.isUserLoggedIn) {
                    profilePosts(pageIndex)
                } else {
                    guestProfilePosts(pageIndex)
                }
            }
        })
        return view
    }

    override fun onViewCreated(view: View, @Nullable savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        instance.register(this)
    }

    private fun profilePosts(offset: Int) {
        var offset = offset
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            val limit = 20
            offset = limit * offset
            val request_datas = JSONObject()
            try {
                request_datas.put("token", mSessionManager!!.authToken)
                request_datas.put("limit", limit)
                request_datas.put("offset", offset)
                request_datas.put("sold", "0")
                request_datas.put("membername", memberName)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            val url: String
            Log.d("username111", memberName)
            Log.d("username112", mSessionManager!!.userName)
            if (mSessionManager!!.userName == memberName) {
                url = ApiUrl.PROFILE_POST
                Log.d("username113", url)
            } else {
                url = ApiUrl.PROFILE_POST + memberName
                Log.d("username114", url)
            }
            OkHttp3Connection.doOkHttp3Connection(TAG, url, OkHttp3Connection.Request_type.POST, request_datas, object : OkHttp3RequestCallback {
                override fun onSuccess(result: String?, user_tag: String?) {
                    progress_bar_profile!!.visibility = View.GONE
                    println("$TAG profile selling res=$result")
                    mSwipeRefreshLayout!!.setRefreshing(false)

                    /*ExplorePojoMain explorePojoMain;
                    Gson gson = new Gson();
                    explorePojoMain = gson.fromJson(result, ExplorePojoMain.class);*/
                    val profileSellingMainPojo: ProfileSellingMainPojo
                    val gson = Gson()
                    profileSellingMainPojo = gson.fromJson(result, ProfileSellingMainPojo::class.java)
                    when (profileSellingMainPojo.code) {
                        "200" -> if (profileSellingMainPojo.data != null && profileSellingMainPojo.data!!.size > 0) {
                            isToCallFirstTime = false
                            rL_noProductFound!!.visibility = View.GONE
                            arrayListSellingDatas!!.addAll(profileSellingMainPojo.data!!)
                            isLoadingRequired = arrayListSellingDatas!!.size > 14
                            sellingRvAdapter!!.notifyDataSetChanged()

                            // Load more
                            rV_selling!!.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                                    super.onScrolled(recyclerView, dx, dy)
                                    val firstVisibleItemPositions = IntArray(2)
                                    totalItemCount = gridLayoutManager!!.getItemCount()
                                    visibleItemCount = gridLayoutManager!!.findLastVisibleItemPositions(firstVisibleItemPositions).get(0)
                                    if (isLoadingRequired && totalItemCount <= visibleItemCount + visibleThreshold) {
                                        isLoadingRequired = false
                                        pageIndex = pageIndex + 1
                                        mSwipeRefreshLayout!!.setRefreshing(true)
                                        profilePosts(pageIndex)
                                    }
                                }
                            })
                        }
                        "204" -> {
                            mSwipeRefreshLayout!!.setRefreshing(false)
                            if (arrayListSellingDatas!!.size == 0) {
                                rL_noProductFound!!.visibility = View.VISIBLE
                            }
                        }
                        "401" -> {
                            mSwipeRefreshLayout!!.setRefreshing(false)
                            CommonClass.sessionExpired(mActivity!!)
                        }
                        else -> {
                            mSwipeRefreshLayout!!.setRefreshing(false)
                            CommonClass.showTopSnackBar((mActivity as HomePageActivity?)!!.rL_rootElement, profileSellingMainPojo.message)
                        }
                    }
                }

                override fun onError(error: String?, user_tag: String?) {
                    mSwipeRefreshLayout!!.setRefreshing(false)
                    progress_bar_profile!!.visibility = View.GONE
                    CommonClass.showSnackbarMessage((mActivity as HomePageActivity?)!!.rL_rootElement, error)
                }
            })
        } else CommonClass.showSnackbarMessage((mActivity as HomePageActivity?)!!.rL_rootElement, getResources().getString(R.string.NoInternetAccess))
    }

    /*
* Updating the comment list data.*/
    @Subscribe
    fun getMessage(sellingDatas: ProfileSellingData?) {
        println("$TAG get message called... sellingDatas=$sellingDatas")
        if (sellingDatas != null) {
            if (sellingDatas.isToRemoveSellingItem) {
                if (arrayListSellingDatas!!.size > 0) {
                    for (sellingItemCount in arrayListSellingDatas!!.indices) {
                        val postId = sellingDatas.postId
                        if (postId == arrayListSellingDatas!![sellingItemCount].postId) {
                            arrayListSellingDatas!!.removeAt(sellingItemCount)
                            sellingRvAdapter!!.notifyDataSetChanged()
                            break
                        }
                    }
                }
            } else {
                if (!isContainsId(sellingDatas.postId)) {
                    rL_noProductFound!!.visibility = View.GONE
                    arrayListSellingDatas!!.add(0, sellingDatas)
                    println(TAG + " " + "arrayListSellingDatas size=" + arrayListSellingDatas!!.size)
                    sellingRvAdapter!!.notifyDataSetChanged()
                }
            }
        }
        println(TAG + " " + "selling item size=" + arrayListSellingDatas!!.size)
        if (arrayListSellingDatas!!.size > 0) rL_noProductFound!!.visibility = View.GONE else rL_noProductFound!!.visibility = View.VISIBLE

        /*ProfileFrag profileFrag = new ProfileFrag();
        profileFrag.updatePostcount();*/
    }

    /**
     * <h>IsContainsId</h>
     *
     *
     * In this method we used to check whether the given post id is
     * present or not in the current list.
     *
     * @param postId the given post id of product
     * @return the boolean value
     */
    fun isContainsId(postId: String): Boolean {
        var flag = false
        for (`object` in arrayListSellingDatas!!) {
            println(TAG + " " + "given post id=" + postId + " " + "current post id=" + `object`.postId)
            if (postId == `object`.postId) {
                flag = true
            }
        }
        return flag
    }

    override fun onDestroy() {
        instance.unregister(this)
        super.onDestroy()
    }

    override fun onItemClick(pos: Int, imageView: ImageView?) {
        try {
            println("$TAG selling item clicked")
            if (isFromMyProfile) {
                val intent = Intent(mActivity, EditProductActivity::class.java)
                val bundle = Bundle()
                bundle.putString("postId", arrayListSellingDatas!![pos].postId)
                bundle.putString("postsType", arrayListSellingDatas!![pos].postsType)
                bundle.putString("postedByUserName", memberName)
                bundle.putString("productImage", arrayListSellingDatas!![pos].mainUrl)
                bundle.putString("productName", arrayListSellingDatas!![pos].productName)
                bundle.putString("category", arrayListSellingDatas!![pos].category)
                bundle.putString("subCategory", arrayListSellingDatas!![pos].subCategory)
                bundle.putSerializable("information", arrayListSellingDatas!![pos].postFilter)
                bundle.putString("description", arrayListSellingDatas!![pos].description)
                bundle.putString("condition", arrayListSellingDatas!![pos].condition)
                bundle.putString("price", arrayListSellingDatas!![pos].price)
                bundle.putString("negotiable", arrayListSellingDatas!![pos].negotiable)
                bundle.putString("place", arrayListSellingDatas!![pos].place)
                bundle.putString("latitude", arrayListSellingDatas!![pos].latitude)
                bundle.putString("longitude", arrayListSellingDatas!![pos].longitude)
                bundle.putString("currency", arrayListSellingDatas!![pos].currency)
                bundle.putInt("isSwap", arrayListSellingDatas!![pos].isSwap)
                bundle.putInt("isSold", arrayListSellingDatas!![pos].sold.toInt())
                if (arrayListSellingDatas!![pos].isPromoted.isEmpty()) bundle.putInt("isPromoted", 0) else bundle.putInt("isPromoted", arrayListSellingDatas!![pos].isPromoted.toInt())
                //bundle.putInt("isPromoted",(arrayListSellingDatas.get(pos).getIsPromoted().isEmpty())? 0:Integer.parseInt(arrayListSellingDatas.get(pos).getIsPromoted()));
                bundle.putSerializable("swapPostList", arrayListSellingDatas!![pos].swapPost)
                if (arrayListSellingDatas != null && arrayListSellingDatas?.size!! > 0 && arrayListSellingDatas!![pos].categoryData != null && arrayListSellingDatas!![pos].categoryData?.size!!>0) {
                    bundle.putString("category", arrayListSellingDatas!![pos].categoryData!![0].category)
                }
                //           System.out.println(TAG+" "+"category="+arrayListSellingDatas.get(pos).getCategoryData().get(0).getCategory());
                val aLProductImageDatases = ArrayList<ProductImageDatas>()

                // first image
                val mainUrl = arrayListSellingDatas!![pos].mainUrl
                if (mainUrl != null && !mainUrl.isEmpty()) {
                    val productImageDatas1 = ProductImageDatas()
                    productImageDatas1.mainUrl = arrayListSellingDatas!![pos].mainUrl
                    productImageDatas1.thumbnailUrl = arrayListSellingDatas!![pos].thumbnailImageUrl
                    productImageDatas1.public_id = arrayListSellingDatas!![pos].cloudinaryPublicId

                    // set width
                    val width = arrayListSellingDatas!![pos].containerWidth
                    if (width != null && !width.isEmpty()) productImageDatas1.width = width.toInt()

                    // set height
                    val height = arrayListSellingDatas!![pos].containerHeight
                    if (height != null && !height.isEmpty()) productImageDatas1.height = height.toInt()
                    productImageDatas1.isImageUrl = true
                    aLProductImageDatases.add(productImageDatas1)
                }

                // second image
                val imageUrl1 = arrayListSellingDatas!![pos].imageUrl1
                if (imageUrl1 != null && !imageUrl1.isEmpty()) {
                    val productImageDatas2 = ProductImageDatas()
                    productImageDatas2.mainUrl = arrayListSellingDatas!![pos].imageUrl1
                    productImageDatas2.thumbnailUrl = arrayListSellingDatas!![pos].thumbnailUrl1
                    productImageDatas2.public_id = arrayListSellingDatas!![pos].cloudinaryPublicId1

                    // set width
                    val width = arrayListSellingDatas!![pos].containerWidth1
                    if (width != null && !width.isEmpty()) productImageDatas2.width = width.toInt()

                    // set height
                    val height = arrayListSellingDatas!![pos].containerHeight1
                    if (height != null && !height.isEmpty()) productImageDatas2.height = height.toInt()
                    productImageDatas2.isImageUrl = true
                    aLProductImageDatases.add(productImageDatas2)
                }

                // Third Image
                val imageUrl2 = arrayListSellingDatas!![pos].imageUrl2
                if (imageUrl2 != null && !imageUrl2.isEmpty()) {
                    val productImageDatas3 = ProductImageDatas()
                    productImageDatas3.mainUrl = arrayListSellingDatas!![pos].imageUrl2
                    productImageDatas3.thumbnailUrl = arrayListSellingDatas!![pos].thumbnailUrl2
                    productImageDatas3.public_id = arrayListSellingDatas!![pos].cloudinaryPublicId2
                    // set width
                    val width = arrayListSellingDatas!![pos].containerWidth2
                    if (width != null && !width.isEmpty()) productImageDatas3.width = width.toInt()

                    // set height
                    val height = arrayListSellingDatas!![pos].containerHeight2
                    if (height != null && !height.isEmpty()) productImageDatas3.height = height.toInt()
                    productImageDatas3.isImageUrl = true
                    aLProductImageDatases.add(productImageDatas3)
                }

                // Fourth Image
                val imageUrl3 = arrayListSellingDatas!![pos].imageUrl3
                if (imageUrl3 != null && !imageUrl3.isEmpty()) {
                    val productImageDatas4 = ProductImageDatas()
                    productImageDatas4.mainUrl = arrayListSellingDatas!![pos].imageUrl3
                    productImageDatas4.thumbnailUrl = arrayListSellingDatas!![pos].thumbnailUrl3
                    productImageDatas4.public_id = arrayListSellingDatas!![pos].cloudinaryPublicId3
                    // set width
                    val width = arrayListSellingDatas!![pos].containerWidth3
                    if (width != null && !width.isEmpty()) productImageDatas4.width = width.toInt()

                    // set height
                    val height = arrayListSellingDatas!![pos].containerHeight3
                    if (height != null && !height.isEmpty()) productImageDatas4.height = height.toInt()
                    productImageDatas4.isImageUrl = true
                    aLProductImageDatases.add(productImageDatas4)
                }

                // Fifth Image
                val imageUrl4 = arrayListSellingDatas!![pos].imageUrl4
                if (imageUrl4 != null && !imageUrl4.isEmpty()) {
                    val productImageDatas5 = ProductImageDatas()
                    productImageDatas5.mainUrl = arrayListSellingDatas!![pos].imageUrl4
                    productImageDatas5.thumbnailUrl = arrayListSellingDatas!![pos].thumbnailUrl4
                    productImageDatas5.public_id = arrayListSellingDatas!![pos].cloudinaryPublicId4
                    // set width
                    val width = arrayListSellingDatas!![pos].containerWidth4
                    if (width != null && !width.isEmpty()) productImageDatas5.width = width.toInt()

                    // set height
                    val height = arrayListSellingDatas!![pos].containerHeight4
                    if (height != null && !height.isEmpty()) productImageDatas5.height = height.toInt()
                    productImageDatas5.isImageUrl = true
                    aLProductImageDatases.add(productImageDatas5)
                }
                bundle.putSerializable("imageDatas", aLProductImageDatases)
                intent.putExtras(bundle)
                startActivity(intent)
            } else {
                val intent = Intent(mActivity, ProductDetailsActivity::class.java)
                intent.putExtra("productName", arrayListSellingDatas!![pos].productName)
                if (arrayListSellingDatas!![pos].categoryData != null) intent.putExtra("category", arrayListSellingDatas!![pos].categoryData!![0].category)
                println(TAG + " " + "category=" + arrayListSellingDatas!![pos].categoryData!![0]!!.category)
                intent.putExtra("likes", arrayListSellingDatas!![pos].likes)
                intent.putExtra("likeStatus", arrayListSellingDatas!![pos].likeStatus)
                intent.putExtra("currency", arrayListSellingDatas!![pos].currency)
                intent.putExtra("price", arrayListSellingDatas!![pos].price)
                intent.putExtra("postedOn", arrayListSellingDatas!![pos].postedOn)
                intent.putExtra("image", arrayListSellingDatas!![pos].mainUrl)
                intent.putExtra("thumbnailImageUrl", arrayListSellingDatas!![pos].thumbnailImageUrl)
                intent.putExtra("description", arrayListSellingDatas!![pos].description)
                intent.putExtra("condition", arrayListSellingDatas!![pos].condition)
                intent.putExtra("place", arrayListSellingDatas!![pos].place)
                intent.putExtra("latitude", arrayListSellingDatas!![pos].latitude)
                intent.putExtra("longitude", arrayListSellingDatas!![pos].longitude)
                intent.putExtra("postId", arrayListSellingDatas!![pos].postId)
                intent.putExtra("postsType", arrayListSellingDatas!![pos].postsType)
                intent.putExtra("followRequestStatus", "")
                intent.putExtra("clickCount", "")
                intent.putExtra("memberProfilePicUrl", "")
                intent.putExtra(VariableConstants.EXTRA_ANIMAL_IMAGE_TRANSITION_NAME, ViewCompat.getTransitionName(imageView!!))
                val options = ActivityOptionsCompat.makeSceneTransitionAnimation(mActivity!!, imageView, ViewCompat.getTransitionName(imageView)!!)
                startActivity(intent, options.toBundle())
            }
        }catch (e:Exception){e.printStackTrace()}
    }

    private fun guestProfilePosts(offset: Int) {
        var offset = offset
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            val limit = 20
            offset = limit * offset
            val request_datas = JSONObject()
            try {
                request_datas.put("token", mSessionManager!!.authToken)
                request_datas.put("limit", limit)
                request_datas.put("offset", offset)
                request_datas.put("sold", "0")
                request_datas.put("membername", memberName)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            val url = ApiUrl.GUEST_PROFILE_POST
            OkHttp3Connection.doOkHttp3Connection(TAG, url, OkHttp3Connection.Request_type.POST, request_datas, object : OkHttp3RequestCallback {
                override fun onSuccess(result: String?, user_tag: String?) {
                    progress_bar_profile!!.visibility = View.GONE
                    println("$TAG profile selling res=$result")
                    mSwipeRefreshLayout!!.setRefreshing(false)

                    /*ExplorePojoMain explorePojoMain;
                    Gson gson = new Gson();
                    explorePojoMain = gson.fromJson(result, ExplorePojoMain.class);*/
                    val profileSellingMainPojo: ProfileSellingMainPojo
                    val gson = Gson()
                    profileSellingMainPojo = gson.fromJson(result, ProfileSellingMainPojo::class.java)
                    when (profileSellingMainPojo.code) {
                        "200" -> if (profileSellingMainPojo.data != null && profileSellingMainPojo.data!!.size > 0) {
                            isToCallFirstTime = false
                            rL_noProductFound!!.visibility = View.GONE
                            arrayListSellingDatas!!.addAll(profileSellingMainPojo.data!!)
                            isLoadingRequired = arrayListSellingDatas!!.size > 14
                            sellingRvAdapter!!.notifyDataSetChanged()

                            // Load more
                            rV_selling!!.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                                    super.onScrolled(recyclerView, dx, dy)
                                    val firstVisibleItemPositions = IntArray(2)
                                    totalItemCount = gridLayoutManager!!.getItemCount()
                                    visibleItemCount = gridLayoutManager!!.findLastVisibleItemPositions(firstVisibleItemPositions).get(0)
                                    if (isLoadingRequired && totalItemCount <= visibleItemCount + visibleThreshold) {
                                        isLoadingRequired = false
                                        pageIndex = pageIndex + 1
                                        mSwipeRefreshLayout!!.setRefreshing(true)
                                        guestProfilePosts(pageIndex)
                                    }
                                }
                            })
                        }
                        "204" -> {
                            mSwipeRefreshLayout!!.setRefreshing(false)
                            if (arrayListSellingDatas!!.size == 0) {
                                rL_noProductFound!!.visibility = View.VISIBLE
                            }
                        }
                        "401" -> {
                            mSwipeRefreshLayout!!.setRefreshing(false)
                            CommonClass.sessionExpired(mActivity!!)
                        }
                        else -> {
                            mSwipeRefreshLayout!!.setRefreshing(false)
                            CommonClass.showTopSnackBar((mActivity as HomePageActivity?)!!.rL_rootElement, profileSellingMainPojo.message)
                        }
                    }
                }

                override fun onError(error: String?, user_tag: String?) {
                    mSwipeRefreshLayout!!.setRefreshing(false)
                    progress_bar_profile!!.visibility = View.GONE
                    CommonClass.showSnackbarMessage((mActivity as HomePageActivity?)!!.rL_rootElement, error)
                }
            })
        } else CommonClass.showSnackbarMessage((mActivity as HomePageActivity?)!!.rL_rootElement, getResources().getString(R.string.NoInternetAccess))
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (data != null) {
            if (requestCode == VariableConstants.SELLING_REQ_CODE) {
                Log.d(TAG, "onActivityResult: dfskldjfs")
            }
        }
    }

    companion object {
        private val TAG = SellingFrag::class.java.simpleName

        @JvmStatic
        fun newInstance(memberName: String?, isFromMyProfile: Boolean): SellingFrag {
            val bundle = Bundle()
            bundle.putString("memberName", memberName)
            bundle.putBoolean("isFromMyProfileFlag", isFromMyProfile)
            val sellingFrag = SellingFrag()
            sellingFrag.setArguments(bundle)
            return sellingFrag
        }
    }
}