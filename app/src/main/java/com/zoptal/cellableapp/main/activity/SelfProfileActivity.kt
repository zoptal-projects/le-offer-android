package com.zoptal.cellableapp.main.activity

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.Scopes
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener
import com.google.android.gms.common.api.Scope
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.tabs.TabLayout
import com.google.gson.Gson
import com.squareup.otto.Subscribe
import com.squareup.picasso.Picasso
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.adapter.ViewPagerAdapter
import com.zoptal.cellableapp.event_bus.BusProvider
import com.zoptal.cellableapp.main.view_pager.my_profile_frag.FavouriteFrag
import com.zoptal.cellableapp.main.view_pager.my_profile_frag.SellingFrag
import com.zoptal.cellableapp.main.view_pager.my_profile_frag.SoldFrag
import com.zoptal.cellableapp.pojo_class.ProfileFollowingCount
import com.zoptal.cellableapp.pojo_class.profile_pojo.ProfilePojoMain
import com.zoptal.cellableapp.pojo_class.profile_pojo.ProfileResultDatas
import com.zoptal.cellableapp.utility.ApiUrl
import com.zoptal.cellableapp.utility.CircleTransform
import com.zoptal.cellableapp.utility.CommonClass
import com.zoptal.cellableapp.utility.OkHttp3Connection
import com.zoptal.cellableapp.utility.PositionedCropTransformation
import com.zoptal.cellableapp.utility.SessionManager
import com.zoptal.cellableapp.utility.VariableConstants
import org.json.JSONException
import org.json.JSONObject
import java.util.*

/**
 * <h>ProfileFrag</h>
 *
 *
 * In this class we show the own profile description like user name, user profile
 * users total posts, following count, follower count etc
 *
 * @since 30-Oct-17
 */
class SelfProfileActivity : AppCompatActivity(), View.OnClickListener,
        OnConnectionFailedListener {
    private var mSessionManager: SessionManager? = null
    private var mActivity: Activity? = null
    private var profilePicUrl: String? = ""
    private var fullName: String? = ""
    private var userName = ""
    private var email = ""
    private var phoneNumber = ""
    private var website: String? = ""
    private var bio: String? = ""
    private var googleVerified: String? = ""
    private var facebookVerified: String? = ""
    private var emailVerified: String? = ""
    private var paypalUrl: String? = ""
    private var followingCount: String? = ""
    private var verifyLoginWithGplus: VerifyLoginWithGplus? = null
    private var sellingFrag: SellingFrag? = null
    private var soldFrag: SoldFrag? = null
    private var favouriteFrag: FavouriteFrag? = null
    private var verifyLoginWithFacebook: VerifyLoginWithFacebook? = null
    private var coordinate_rootView: CoordinatorLayout? = null

    // Declare xml variables
    private var progress_bar_profile: ProgressBar? = null
    private var iV_profile_pic: ImageView? = null
    private var iv_background: ImageView? = null
    private var iV_email_icon: ImageView? = null
    private var iV_google_icon: ImageView? = null
    private var iV_fbicon: ImageView? = null
//    private var iV_paypal_icon: ImageView? = null
    private var tV_posts_count: TextView? = null
    private var tV_follower_count: TextView? = null
    private var tV_following_count: TextView? = null
    private var tV_fullName: TextView? = null
    private var tV_bio: TextView? = null
    private var tV_website: TextView? = null
    private var profileViewPager: ViewPager? = null
    private var rL_edit_people: RelativeLayout? = null
//    private var rL_verify_paypal: RelativeLayout? = null
    private var appBarLayout: AppBarLayout? = null

    //..rating
    private var ratingBar: RatingBar? = null
    private var avgRating: String? = ""
    private var ratedBy: String? = ""
    private var tv_ratedBy: TextView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_own_profile)
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale)
        initVariables()
    }

    private fun initVariables() {
        mActivity = this@SelfProfileActivity
        mSessionManager = SessionManager(mActivity!!)
        BusProvider.instance.register(this)
        val gso =
                GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                        .requestScopes(Scope(Scopes.DRIVE_APPFOLDER))
                        .requestServerAuthCode(
                                mActivity!!.getResources().getString(R.string.servers_client_id)
                        )
                        .requestEmail()
                        .build()
        val mGoogleApiClient = GoogleApiClient.Builder(mActivity!!)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build()
        coordinate_rootView =
                findViewById<View>(R.id.coordinate_rootView) as CoordinatorLayout
        progress_bar_profile =
                findViewById<View>(R.id.progress_bar_profile) as ProgressBar
        appBarLayout = findViewById<View>(R.id.appBarLayout) as AppBarLayout
        appBarLayout!!.visibility = View.GONE

        // profile pic
        iV_profile_pic =
                findViewById<View>(R.id.iV_profile_pic) as ImageView
        iv_background =
                findViewById<View>(R.id.iv_background) as ImageView
        iV_profile_pic!!.layoutParams.width = CommonClass.getDeviceWidth(mActivity!!) / 4
        iV_profile_pic!!.layoutParams.height = CommonClass.getDeviceWidth(mActivity!!) / 4
        val coordinate_rootView =
                findViewById<View>(R.id.coordinate_rootView) as CoordinatorLayout

        // User name and full name
        tV_fullName = findViewById<View>(R.id.tV_fullName) as TextView
        tV_website = findViewById<View>(R.id.tV_website) as TextView
        tV_website!!.setOnClickListener(this)
        tV_bio = findViewById<View>(R.id.tV_bio) as TextView
        ratingBar = findViewById<View>(R.id.ratingBar) as RatingBar
        tv_ratedBy = findViewById<View>(R.id.tV_ratedBy) as TextView

        // verification ioon like email, google plus , facebook etc

        // Facebook verification
        val pBar_fbVerify =
                findViewById<View>(R.id.pBar_fbVerify) as ProgressBar
        iV_fbicon = findViewById<View>(R.id.iV_fbicon) as ImageView
//        iV_paypal_icon =
//                findViewById<View>(R.id.iV_paypal_icon) as ImageView
//        iV_paypal_icon!!.setOnClickListener(this)
        verifyLoginWithFacebook = VerifyLoginWithFacebook(
                mActivity!!,
                coordinate_rootView,
                pBar_fbVerify,
                iV_fbicon!!,
                facebookVerified!!
        )
        iV_fbicon!!.setOnClickListener { if (facebookVerified == null || facebookVerified != "1") verifyLoginWithFacebook!!.loginWithFbWithSdk() }

        // Google plus verification
        val pBar_gPlusVerify =
                findViewById<View>(R.id.pBar_gPlusVerify) as ProgressBar
        iV_google_icon =
                findViewById<View>(R.id.iV_google_icon) as ImageView
        verifyLoginWithGplus = VerifyLoginWithGplus(
                mActivity!!,
                mGoogleApiClient,
                coordinate_rootView,
                pBar_gPlusVerify,
                iV_google_icon!!,
                googleVerified!!
        )
        iV_google_icon!!.setOnClickListener {
            verifyLoginWithGplus!!.signInWithGoogle(
                    null,
                    mActivity!!
            )
        }

        // Email verification
        iV_email_icon =
                findViewById<View>(R.id.iV_email_icon) as ImageView
        iV_email_icon!!.setOnClickListener {
            startActivity(
                    Intent(
                            mActivity,
                            VerifyEmailIdActivity::class.java
                    )
            )
        }

        // set profile pic
        val profilePic: String = mSessionManager!!.userImage!!
        if (profilePic != null && !profilePic.isEmpty()) {
            Picasso.with(mActivity)
                    .load(profilePic)
                    .placeholder(R.drawable.default_profile_image)
                    .error(R.drawable.default_profile_image)
                    .transform(CircleTransform())
                    .into(iV_profile_pic)
            Glide.with(mActivity!!)
                    .load(profilePic)
                    .transform(PositionedCropTransformation(mActivity, 0f, 0f))
                    .placeholder(R.drawable.default_profile_cover_pic)
                    .error(R.drawable.default_profile_cover_pic)
                    .into(iv_background)
        }

        // Full name
        fullName = mSessionManager!!.userName
        if (fullName != null) tV_fullName!!.text = fullName

        // number of posts
        tV_posts_count = findViewById<View>(R.id.tV_posts_count) as TextView
        tV_follower_count = findViewById<View>(R.id.tV_follower_count) as TextView
        tV_following_count = findViewById<View>(R.id.tV_following_count) as TextView

        // Following
        val rL_following =
                findViewById<View>(R.id.rL_following) as RelativeLayout
        rL_following.setOnClickListener(this)

        // Follower
        val rL_follower =
                findViewById<View>(R.id.rL_follower) as RelativeLayout
        rL_follower.setOnClickListener(this)

        // Edit profile
        rL_edit_people = findViewById<View>(R.id.rL_edit_people) as RelativeLayout

        // verify paypal
//        rL_verify_paypal = findViewById<View>(R.id.rL_verify_paypal) as RelativeLayout
//        rL_verify_paypal!!.setOnClickListener(this)

        // back button
        val rL_back_btn =
                findViewById<View>(R.id.rL_back_btn) as RelativeLayout
        rL_back_btn.setOnClickListener(this)

        // setting
        val rL_setting =
                findViewById<View>(R.id.rL_setting) as RelativeLayout
        rL_setting.setOnClickListener(this)

        // Discover people
        val rL_discovery_people =
                findViewById<View>(R.id.rL_discovery_people) as RelativeLayout
        rL_discovery_people.setOnClickListener(this)

        // view pager
        profileViewPager = findViewById<View>(R.id.viewpager) as ViewPager
        profileViewPager!!.offscreenPageLimit = 2
        val tabs = findViewById<View>(R.id.tabs) as TabLayout
        tabs.setupWithViewPager(profileViewPager)
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            userProfileDatas
        } else CommonClass.showSnackbarMessage(
                coordinate_rootView,
                resources.getString(R.string.NoInternetAccess)
        )
    }//..visible while get rating
    //..visible while get rating

    // set number of posts

    // Followers count

    // Following count

    // Full name

    // Edit profile

    // set google verified

    // set facebook verified

    // set email verified

    // set payal verified

    // set bio

    // set website
//..visible while get rating// set profile pic as cover pic

    // set rating
// Set profile pic// Set my profile fields like image, name etc// token, limit, offset, membername

    /**
     * <h>GetUserProfileDatas</h>
     *
     *
     * In this method we do api call to get users profile complete description
     * Once we receive all datas then set all values to the respective fields.
     *
     */
    private val userProfileDatas: Unit
        private get() {
            // token, limit, offset, membername
            if (CommonClass.isNetworkAvailable(mActivity!!)) {
                progress_bar_profile!!.visibility = View.VISIBLE
                val request_datas = JSONObject()
                try {
                    request_datas.put("token", mSessionManager!!.authToken)
                    request_datas.put("limit", "20")
                    request_datas.put("offset", "0")
                    request_datas.put("membername", mSessionManager!!.userName)
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
                OkHttp3Connection.doOkHttp3Connection(
                        TAG,
                        ApiUrl.USER_PROFILE,
                        OkHttp3Connection.Request_type.POST,
                        request_datas,
                        object : OkHttp3Connection.OkHttp3RequestCallback {
                            override fun onSuccess(result: String?, user_tag: String?) {
                                progress_bar_profile!!.visibility = View.GONE
                                println("$TAG my profile res=$result")
                                val profilePojoMain: ProfilePojoMain
                                val gson = Gson()
                                profilePojoMain = gson.fromJson(result, ProfilePojoMain::class.java)
                                when (profilePojoMain.code) {
                                    "200" -> {
                                        appBarLayout!!.visibility = View.VISIBLE
                                        // Set my profile fields like image, name etc
                                        val profileResponseDatas: ArrayList<ProfileResultDatas> =
                                                profilePojoMain.data!!
                                        if (profileResponseDatas != null && profileResponseDatas.size > 0) {
                                            val profileResultDatas: ProfileResultDatas =
                                                    profileResponseDatas[0]
                                            val noOfPost: String
                                            val followersCount: String
                                            profilePicUrl = profileResultDatas.profilePicUrl
                                            noOfPost = profileResultDatas.posts
                                            followersCount = profileResultDatas.followers
                                            followingCount = profileResultDatas.following
                                            fullName = profileResultDatas.fullName
                                            email = profileResultDatas.email
                                            userName = profileResultDatas.username
                                            phoneNumber = profileResultDatas.phoneNumber
                                            website = profileResultDatas.website
                                            bio = profileResultDatas.bio
                                            googleVerified = profileResultDatas.googleVerified
                                            facebookVerified = profileResultDatas.facebookVerified
                                            emailVerified = profileResultDatas.emailVerified
                                            paypalUrl = profileResultDatas.paypalUrl
                                            avgRating = profileResultDatas.avgRating
                                            ratedBy = profileResponseDatas[0].ratedBy

                                            // Set profile pic
                                            if (profilePicUrl != null && !profilePicUrl!!.isEmpty()) {
                                                Picasso.with(mActivity)
                                                        .load(profilePicUrl)
                                                        .placeholder(R.drawable.default_profile_image)
                                                        .error(R.drawable.default_profile_image)
                                                        .transform(CircleTransform())
                                                        .into(iV_profile_pic)

                                                // set profile pic as cover pic
                                                Glide.with(applicationContext)
                                                        .load(profilePicUrl)
                                                        .transform(
                                                                PositionedCropTransformation(
                                                                        mActivity,
                                                                        0f,
                                                                        0f
                                                                )
                                                        )
                                                        .placeholder(R.drawable.default_profile_cover_pic)
                                                        .error(R.drawable.default_profile_cover_pic)
                                                        .into(iv_background)
                                            }

                                            // set rating
                                            if (avgRating != null && !avgRating!!.isEmpty()) {
                                                ratingBar!!.visibility =
                                                        View.VISIBLE //..visible while get rating
                                                ratingBar!!.rating = avgRating!!.toFloat()
                                            }
                                            if (ratedBy != null && !ratedBy!!.isEmpty()) {
                                                ratingBar!!.visibility =
                                                        View.VISIBLE //..visible while get rating
                                                tv_ratedBy!!.visibility =
                                                        View.VISIBLE //..visible while get rating
                                                ratedBy = "($ratedBy)"
                                                tv_ratedBy!!.text = ratedBy
                                            }

                                            // set number of posts
                                            if (noOfPost != null) tV_posts_count!!.text = noOfPost

                                            // Followers count
                                            if (followersCount != null) tV_follower_count!!.text =
                                                    followersCount

                                            // Following count
                                            if (followingCount != null) tV_following_count!!.text =
                                                    followingCount

                                            // Full name
                                            if (fullName != null) tV_fullName!!.text = fullName

                                            // Edit profile
                                            rL_edit_people!!.setOnClickListener {
                                                val editProfileIntent = Intent(
                                                        mActivity,
                                                        EditProfileActivity::class.java
                                                )
                                                editProfileIntent.putExtra(
                                                        "profilePicUrl",
                                                        profilePicUrl
                                                )
                                                editProfileIntent.putExtra("username", userName)
                                                editProfileIntent.putExtra("fullName", fullName)
                                                editProfileIntent.putExtra("email", email)
                                                editProfileIntent.putExtra("phoneNumber", phoneNumber)
                                                editProfileIntent.putExtra("website", website)
                                                editProfileIntent.putExtra("bio", bio)
                                                startActivityForResult(
                                                        editProfileIntent,
                                                        VariableConstants.PROFILE_REQUEST_CODE
                                                )
                                            }

                                            // set google verified
                                            if (googleVerified != null && googleVerified == "1") iV_google_icon!!.setImageResource(
                                                    R.drawable.gplus_verified_icon
                                            )

                                            // set facebook verified
                                            if (facebookVerified != null && facebookVerified == "1") iV_fbicon!!.setImageResource(
                                                    R.drawable.facebook_verified_icon
                                            )

                                            // set email verified
                                            if (emailVerified != null && emailVerified == "1") iV_email_icon!!.setImageResource(
                                                    R.drawable.email_verified_icon
                                            )

                                            // set payal verified
//                                            if (paypalUrl != null && paypalUrl != "") {
//                                                iV_paypal_icon!!.visibility = View.VISIBLE
//                                                rL_verify_paypal!!.visibility = View.GONE
//                                            } else {
//                                                iV_paypal_icon!!.visibility = View.GONE
//                                                rL_verify_paypal!!.visibility = View.VISIBLE
//                                            }

                                            // set bio
                                            if (bio != null && !bio!!.isEmpty()) {
                                                tV_bio!!.visibility = View.VISIBLE
                                                tV_bio!!.text = bio
                                            } else tV_bio!!.visibility = View.GONE

                                            // set website
                                            if (website != null && !website!!.isEmpty()) {
                                                tV_website!!.visibility = View.VISIBLE
                                                tV_website!!.text = website
                                            } else tV_website!!.visibility = View.GONE
                                        }
                                        sellingFrag =
                                                SellingFrag.newInstance(mSessionManager!!.userName, true)
                                        soldFrag =
                                                SoldFrag.newInstance(mSessionManager!!.userName, true)
                                        favouriteFrag =
                                                FavouriteFrag.newInstance(mSessionManager!!.userName)
                                        setupViewPager()
                                    }
                                    "401" -> CommonClass.sessionExpired(mActivity!!)
                                    else -> CommonClass.showSnackbarMessage(
                                            coordinate_rootView,
                                            profilePojoMain.message
                                    )
                                }
                            }

                            override fun onError(error: String?, user_tag: String?) {
                                progress_bar_profile!!.visibility = View.GONE
                                CommonClass.showSnackbarMessage(coordinate_rootView, error)
                            }
                        })
            } else CommonClass.showSnackbarMessage(
                    coordinate_rootView,
                    resources.getString(R.string.NoInternetAccess)
            )
        }

    /**
     * <h>SetupViewPager</h>
     *
     *
     * In this method we used to set viewpager with respective fragments.
     *
     */
    private fun setupViewPager() {
        val adapter = ViewPagerAdapter(supportFragmentManager)
        sellingFrag?.let { adapter.addFragment(it, resources.getString(R.string.selling)) }
        soldFrag?.let { adapter.addFragment(it, resources.getString(R.string.sold)) }
        favouriteFrag?.let { adapter.addFragment(it, resources.getString(R.string.favourites)) }
        profileViewPager!!.adapter = adapter
    }

    override fun onClick(v: View) {
        var mFollowingCount = 0
        val intent: Intent
        when (v.id) {
            R.id.rL_back_btn -> onBackPressed()
            R.id.rL_setting -> {
                intent = Intent(mActivity, ProfileSettingActivity::class.java)
                intent.putExtra("payPalLink", paypalUrl)
                startActivityForResult(intent, VariableConstants.PAYPAL_REQ_CODE)
            }
            R.id.rL_discovery_people -> {
                if (followingCount != null && !followingCount!!.isEmpty()) mFollowingCount =
                        followingCount!!.toInt()
                intent = Intent(mActivity, DiscoverPeopleActivity::class.java)
                intent.putExtra("followingCount", mFollowingCount)
                startActivityForResult(intent, VariableConstants.FOLLOW_COUNT_REQ_CODE)
            }
            R.id.rL_following -> {
                if (followingCount != null && !followingCount!!.isEmpty()) mFollowingCount =
                        followingCount!!.toInt()
                intent = Intent(mActivity, SelfFollowingActivity::class.java)
                intent.putExtra("title", resources.getString(R.string.Following))
                intent.putExtra("isFollower", false)
                intent.putExtra("followingCount", mFollowingCount)
                startActivityForResult(intent, VariableConstants.FOLLOW_COUNT_REQ_CODE)
            }
            R.id.rL_follower -> {
                if (followingCount != null && !followingCount!!.isEmpty()) mFollowingCount =
                        followingCount!!.toInt()
                intent = Intent(mActivity, SelfFollowingActivity::class.java)
                intent.putExtra("title", resources.getString(R.string.Followers))
                intent.putExtra("isFollower", true)
                intent.putExtra("followingCount", mFollowingCount)
                startActivityForResult(intent, VariableConstants.FOLLOW_COUNT_REQ_CODE)
            }
//            R.id.rL_verify_paypal -> {
//                intent = Intent(mActivity, ConnectPaypalActivity::class.java)
//                startActivityForResult(intent, VariableConstants.PAYPAL_REQ_CODE)
//            }
            R.id.tV_website -> {
                println("$TAG website=$website")
                if (website != null && !website!!.isEmpty()) {
                    val browserIntent =
                            Intent(Intent.ACTION_VIEW, Uri.parse("http://$website"))
                    startActivity(browserIntent)
                }
            }
//            R.id.iV_paypal_icon -> {
//                println("$TAG send paypal url=$paypalUrl")
//                intent = Intent(mActivity, ConnectPaypalActivity::class.java)
//                intent.putExtra("payPalLink", paypalUrl)
//                startActivityForResult(intent, VariableConstants.PAYPAL_REQ_CODE)
//            }
        }
    }

    public override fun onActivityResult(
            requestCode: Int,
            resultCode: Int,
            data: Intent?
    ) {
        println("$TAG profile onactivity result...")
        super.onActivityResult(requestCode, resultCode, data)
        if (data != null) {
            when (requestCode) {
                VariableConstants.PROFILE_REQUEST_CODE -> {
                    profilePicUrl = data.getStringExtra("profilePicUrl")
                    userName = data.getStringExtra("username")
                    fullName = data.getStringExtra("fullName")
                    bio = data.getStringExtra("bio")
                    website = data.getStringExtra("website")
                    println("$TAG profile pic=$profilePicUrl user name=$userName full name=$fullName")

                    // Set profile pic
                    if (profilePicUrl != null && !profilePicUrl!!.isEmpty()) {
                        // profile pic
                        Picasso.with(mActivity)
                                .load(profilePicUrl)
                                .placeholder(R.drawable.default_profile_image)
                                .error(R.drawable.default_profile_image)
                                .transform(CircleTransform())
                                .into(iV_profile_pic)

                        // set profile pic as cover pic
                        Glide.with(mActivity!!)
                                .load(profilePicUrl)
                                .transform(PositionedCropTransformation(mActivity, 0f, 0f))
                                .placeholder(R.drawable.default_profile_cover_pic)
                                .error(R.drawable.default_profile_cover_pic)
                                .into(iv_background)
                    } else {
                        iV_profile_pic!!.setImageResource(R.drawable.default_profile_image)
                        iv_background!!.setImageResource(R.drawable.default_profile_cover_pic)
                    }

                    // Full name
                    if (fullName != null) tV_fullName!!.text = fullName

                    // set bio
                    if (bio != null && !bio!!.isEmpty()) {
                        tV_bio!!.visibility = View.VISIBLE
                        tV_bio!!.text = bio
                    }

                    // set website
                    if (website != null && !website!!.isEmpty()) {
                        tV_website!!.visibility = View.VISIBLE
                        tV_website!!.text = website
                    }
                }
                VariableConstants.GOOGLE_LOGIN_REQ_CODE -> verifyLoginWithGplus!!.onActivityResult(
                        data
                )
//                VariableConstants.PAYPAL_REQ_CODE -> {
//                    val isPaypalVerified =
//                            data.getBooleanExtra("isPaypalVerified", false)
//                    paypalUrl = data.getStringExtra("payPalLink")
//                    if (isPaypalVerified) {
//                        iV_paypal_icon!!.visibility = View.VISIBLE
////                        rL_verify_paypal!!.visibility = View.GONE
//                    }
//                }
                VariableConstants.FOLLOW_COUNT_REQ_CODE -> {
                    val intFollowCount = data.getIntExtra("followingCount", 0)
                    println("$TAG followingCount=$intFollowCount")
                    if (intFollowCount >= 0) {
                        followingCount = intFollowCount.toString()
                        tV_following_count!!.text = followingCount
                    }
                }
            }
            if (verifyLoginWithFacebook != null) verifyLoginWithFacebook!!.fbOnActivityResult(
                    requestCode,
                    resultCode,
                    data
            )
        }
    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {}

    // This methos is notified when any following count changes from Social Frag
    @Subscribe
    fun getMessage(follingCountObj: ProfileFollowingCount?) {
        if (follingCountObj != null && follingCountObj.followingCount >= 0) {
            println(TAG + " " + "followingCount=" + follingCountObj.followingCount)
            followingCount = java.lang.String.valueOf(follingCountObj.followingCount)
            tV_following_count!!.text = followingCount
        }
    }

    // This method is notified when paypal link verified from Connect paypal screen
    @Subscribe
    fun getMessage(getPaypalUrl: String?) {
//        println("$TAG get paypal url=$getPaypalUrl")
//        if (getPaypalUrl != null) {
//            paypalUrl = getPaypalUrl
//            iV_paypal_icon!!.visibility = View.VISIBLE
//            rL_verify_paypal!!.visibility = View.GONE
//        }
    }

    @Subscribe
    fun getMessage(getProfileResultDatas: ProfileResultDatas?) {
        if (getProfileResultDatas != null) {
            profilePicUrl = getProfileResultDatas.profilePicUrl
            userName = getProfileResultDatas.username
            fullName = getProfileResultDatas.fullName
            bio = getProfileResultDatas.bio
            website = getProfileResultDatas.website
            println("$TAG profile pic=$profilePicUrl user name=$userName full name=$fullName")

            // Set profile pic
            if (profilePicUrl != null && !profilePicUrl!!.isEmpty()) {
                // profile pic
                Picasso.with(mActivity)
                        .load(profilePicUrl)
                        .placeholder(R.drawable.default_profile_image)
                        .error(R.drawable.default_profile_image)
                        .transform(CircleTransform())
                        .into(iV_profile_pic)

                // set profile pic as cover pic
                Glide.with(mActivity!!)
                        .load(profilePicUrl)
                        .transform(PositionedCropTransformation(mActivity, 0f, 0f))
                        .placeholder(R.drawable.default_profile_cover_pic)
                        .error(R.drawable.default_profile_cover_pic)
                        .into(iv_background)
            } else {
                iV_profile_pic!!.setImageResource(R.drawable.default_profile_image)
                iv_background!!.setImageResource(R.drawable.default_profile_cover_pic)
            }

            // Full name
            if (fullName != null) tV_fullName!!.text = fullName

            // set bio
            if (bio != null && !bio!!.isEmpty()) {
                tV_bio!!.visibility = View.VISIBLE
                tV_bio!!.text = bio
            }

            // set website
            if (website != null && !website!!.isEmpty()) {
                tV_website!!.visibility = View.VISIBLE
                tV_website!!.text = website
            }
        }
    }

    public override fun onDestroy() {
        super.onDestroy()
        BusProvider.instance.unregister(this)
    }

    override fun onBackPressed() {
        finish()
        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate)
    }

    companion object {
        private val TAG: String = com.zoptal.cellableapp.main.tab_fragments.ProfileFrag::class.java.getSimpleName()
    }
}