package com.zoptal.cellableapp.main.camera_kit

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.Nullable
import butterknife.BindView
import butterknife.ButterKnife
import com.zoptal.cellableapp.R
import kotlinx.android.synthetic.main.activity_preview.*

class PreviewActivity : AppCompatActivity() {
//    @BindView(R.id.image)
//    var imageView: ImageView? = null

//    @BindView(R.id.video)
//    var videoView: VideoView? = null

//    @BindView(R.id.actualResolution)
//    var actualResolution: TextView? = null

//    @BindView(R.id.approxUncompressedSize)
//    var approxUncompressedSize: TextView? = null

//    @BindView(R.id.captureLatency)
//    var captureLatency: TextView? = null
     override fun onCreate(@Nullable savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_preview)
        ButterKnife.bind(this)

        //setupToolbar();
        val jpeg = ResultHolder.image
        val video1 = ResultHolder.video
        if (jpeg != null) {
            image!!.visibility = View.VISIBLE
            val bitmap = BitmapFactory.decodeByteArray(jpeg, 0, jpeg.size)
            if (bitmap == null) {
                finish()
                return
            }
            image!!.setImageBitmap(bitmap)
            actualResolution!!.text = bitmap.width.toString() + " x " + bitmap.height
            approxUncompressedSize!!.text = getApproximateFileMegabytes(bitmap).toString() + "MB"
            captureLatency!!.setText(ResultHolder.timeToCallback.toString() + " milliseconds")
        } else if (video != null) {
            video!!.visibility = View.VISIBLE
            video!!.setVideoURI(Uri.parse(video1?.absolutePath))
            val mediaController = MediaController(this)
            mediaController.visibility = View.GONE
            video!!.setMediaController(mediaController)
            video!!.setOnPreparedListener { mp ->
                mp.isLooping = true
                mp.start()
                val multiplier = video!!.width.toFloat() / mp.videoWidth.toFloat()
                video!!.layoutParams = FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (mp.videoHeight * multiplier).toInt())
            }
            //videoView.start();
        } else {
            finish()
            return
        }
    }

    companion object {
        /*private void setupToolbar() {
        if (getSupportActionBar() != null) {
            View toolbarView = getLayoutInflater().inflate(R.layout.action_bar, null, false);
            TextView titleView = toolbarView.findViewById(R.id.toolbar_title);
            titleView.setText(Html.fromHtml("<b>Camera</b>Kit"));

            getSupportActionBar().setCustomView(toolbarView, new ActionBar.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
    }*/
        private fun getApproximateFileMegabytes(bitmap: Bitmap): Float {
            return (bitmap.rowBytes * bitmap.height / 1024 / 1024).toFloat()
        }
    }
}