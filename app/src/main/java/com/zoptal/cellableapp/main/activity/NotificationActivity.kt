package com.zoptal.cellableapp.main.activity

import android.app.Activity
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.View
import android.widget.RelativeLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.google.gson.Gson
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.adapter.ViewPagerAdapter
import com.zoptal.cellableapp.fcm_push_notification.Config
import com.zoptal.cellableapp.fcm_push_notification.NotificationMessageDialog
import com.zoptal.cellableapp.fcm_push_notification.NotificationUtils.Companion.clearNotifications
import com.zoptal.cellableapp.main.activity.NotificationActivity
import com.zoptal.cellableapp.main.view_pager.notification_frag.FollowingFrag
import com.zoptal.cellableapp.main.view_pager.notification_frag.YouFrag
import com.zoptal.cellableapp.pojo_class.fcm_notification_pojo.FcmNotificationBody
import com.zoptal.cellableapp.utility.CommonClass
import com.zoptal.cellableapp.utility.DialogBox
import com.zoptal.cellableapp.utility.VariableConstants

/**
 * <h>NotificationActivity</h>
 *
 *
 * In this class we used to show the all notification for friends and
 * logged-in user.
 *
 * @since 4/15/2017
 * @version 1.0
 * @author 3Embed
 */
class NotificationActivity : AppCompatActivity(), View.OnClickListener {
    private var isFromNotification = false
    private var mNotificationMessageDialog: NotificationMessageDialog? = null
    private var mDialogBox: DialogBox? = null
    var isNotificationSeen = false
    var fragmentRefreshListener: FragmentRefreshListener? = null
    override fun onCreate(savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_explore_notification)
        overridePendingTransition(R.anim.slide_up, R.anim.stay)
        val mActivity: Activity = this@NotificationActivity
        isNotificationSeen = false
        mDialogBox = DialogBox(mActivity)
        mNotificationMessageDialog = NotificationMessageDialog(mActivity)
        CommonClass.statusBarColor(mActivity)

        // Receive datas from last activity
        val intent = intent
        isFromNotification = intent.getBooleanExtra("isFromNotification", false)
        println("$TAG isFromNotification=$isFromNotification")
        val notificationDatas = intent.getStringExtra("notificationDatas")
        handleNotificationDatas(notificationDatas)
        val rL_back_btn = findViewById<View>(R.id.rL_back_btn) as RelativeLayout
        rL_back_btn.setOnClickListener(this)
        val chatViewPager = findViewById<View>(R.id.viewpager) as ViewPager
        val tabs_notification = findViewById<View>(R.id.tabs_notification) as TabLayout
        setupViewPager(chatViewPager)
        tabs_notification.setupWithViewPager(chatViewPager)
    }

    /**
     * <h>HandleNotificationDatas</h>
     *
     *
     * In this method we used to check the type of push notification.
     * if it is of campaign type i.e type 73. then open a dialog.
     *
     * @param notificationDatas consisting the json type String which contains notification complete description.
     */
    private fun handleNotificationDatas(notificationDatas: String?) {
        if (notificationDatas != null && !notificationDatas.isEmpty()) {
            val fcmNotificationBody: FcmNotificationBody
            val gson = Gson()
            fcmNotificationBody = gson.fromJson(notificationDatas, FcmNotificationBody::class.java)
            if (fcmNotificationBody != null) {
                val campaignId: String
                val imageUrl: String
                val title: String
                val message: String
                val type: String
                val userId: String
                val username: String
                val url: String
                campaignId = fcmNotificationBody.campaignId
                imageUrl = fcmNotificationBody.imageUrl
                title = fcmNotificationBody.title
                message = fcmNotificationBody.message
                type = fcmNotificationBody.type
                userId = fcmNotificationBody.userId
                username = fcmNotificationBody.username
                url = fcmNotificationBody.url
                if (type == "73") {
                    mDialogBox!!.localCampaignDialog(username, userId, campaignId, imageUrl, title, message, url)
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.REGISTRATION_COMPLETE))

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.PUSH_NOTIFICATION))

        // clear the notification area when the app is opened
        clearNotifications(applicationContext)
    }

    override fun onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!)
        super.onPause()
    }

    /**
     * <h>SetupViewPager</h>
     *
     *
     * In this method we used to set-up viewpager with fragments.
     *
     * @param viewPager The reference of Viewpager
     */
    private fun setupViewPager(viewPager: ViewPager) {
        val adapter = ViewPagerAdapter(supportFragmentManager)
        adapter.addFragment(FollowingFrag(), resources.getString(R.string.followings))
        adapter.addFragment(YouFrag(), resources.getString(R.string.you))
        viewPager.adapter = adapter
        //..remove for redirect you tab
        //if (isFromNotification)
        viewPager.currentItem = 1
    }

    override fun onBackPressed() {
        val intent = Intent()
        intent.putExtra("isNotificationSeen", isNotificationSeen)
        setResult(VariableConstants.IS_NOTIFICATION_SEEN_REQ_CODE, intent)
        finish()
        overridePendingTransition(R.anim.stay, R.anim.slide_down)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.rL_back_btn -> onBackPressed()
        }
    }

    interface FragmentRefreshListener {
        fun onRefresh()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == VariableConstants.RATE_USER_REQ_CODE) {
            if (fragmentRefreshListener != null) {
                fragmentRefreshListener!!.onRefresh()
            }
        }
    }

    companion object {
        private val TAG = NotificationActivity::class.java.simpleName
    }
}