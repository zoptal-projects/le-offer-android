package com.zoptal.cellableapp.main.activity

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.View
import android.webkit.*
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.TextView
import butterknife.BindView
//import butterknife.ButterKnife
//import butterknife.OnClick
//import butterknife.Unbinder
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.main.activity.HomePageActivity
import com.zoptal.cellableapp.utility.SessionManager
import com.zoptal.cellableapp.utility.VariableConstants
import kotlinx.android.synthetic.main.activity_web.*

/**
 * <h>WebActivity class</h>
 *
 *
 * This activity mainly load the url to webView, url passed via intent form other activity.
 *
 * @author 3Embed
 * @since 30/01/18.
 */
class PrivacyPolicyActivity : AppCompatActivity() {
    /* @Inject
    TypeFaceManager typeFaceManager;*/
//    @BindView(R.id.webView)
//    var webView: WebView? = null

//    @BindView(R.id.pbLoadProgress)
//    var pbLoadProgress: ProgressBar? = null

//    @BindView(R.id.tvError)
//    var tvError: TextView? = null

//    @BindView(R.id.page_title_tv)
//    var tvPageTitle: TextView? = null

//    @BindView(R.id.rL_accept)
//    var rL_accept: RelativeLayout? = null
//    private var unbinder: Unbinder? = null
    val activity: Activity = this
    private var url: String? = null
    private val pageTitle: String? = ""
    override fun onCreate(savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_web)
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
//        unbinder = ButterKnife.bind(this)
        url = getResources().getString(R.string.privacyPolicyUrl)
        //pageTitle = getIntent().getStringExtra("title");
        initView()
        initWebview()
    }

    /**
     * set the page title and typeface.
     */
    private fun initView() {
        if (pageTitle != null) page_title_tv!!.setText(getResources().getString(R.string.privacyPolicy))
        //rL_accept.setVisibility(View.VISIBLE);
        //tvPageTitle.setTypeface(typeFaceManager.getCircularAirBold());
    }

    /**
     * <P>
     * Enable javaScript and set WebChromeClient and webViewClient
     * to webView.
    </P> *
     *
     */
    @SuppressLint("SetJavaScriptEnabled")
    private fun initWebview() {
        webView!!.settings.javaScriptEnabled = true
        webView!!.settings.builtInZoomControls = true
        webView!!.setWebChromeClient(object : WebChromeClient() {
            override fun onProgressChanged(view: WebView, newProgress: Int) {
                super.onProgressChanged(view, newProgress)
                activity.setProgress(newProgress)
                if (newProgress >= 50) {
                    activity.setTitle(R.string.app_name)
                    //if(pbLoadProgress != null)
                    //pbLoadProgress.setVisibility(View.INVISIBLE);
                }
            }
        })
        webView!!.webViewClient = object : WebViewClient() {
            override fun onReceivedError(view: WebView, request: WebResourceRequest, error: WebResourceError) {
                super.onReceivedError(view, request, error)
                showError("loading error")
            }

            override fun shouldOverrideUrlLoading(view: WebView, request: WebResourceRequest): Boolean {
                return super.shouldOverrideUrlLoading(view, request)
            }

            override fun onPageFinished(view: WebView, url: String) {
                pbLoadProgress!!.visibility = View.GONE
                rL_accept!!.visibility = View.VISIBLE
            }
        }
        pbLoadProgress!!.visibility = View.VISIBLE
        if (url != null) webView!!.loadUrl(url)
    }

    /**
     * set the error msg to TextView tvError.
     * @param errorMsg
     */
    private fun showError(errorMsg: String) {
        if (tvError != null) {
            tvError!!.text = errorMsg
            tvError!!.visibility = View.VISIBLE
        }
    }

 override fun onBackPressed() {
//        if (webView!!.canGoBack()) webView!!.goBack() else {
//            onBack()
     super.onBackPressed()
     activity.overridePendingTransition(R.anim.slide_up, R.anim.slide_down)
     val intent = Intent(this, HomePageActivity::class.java)
     intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
     startActivity(intent)
//        }
    }

//    @OnClick(R.id.close_button)
    fun onBack(v:View) {
    onBackPressed()
//        super.onBackPressed()
//        activity.overridePendingTransition(R.anim.slide_up, R.anim.slide_down)
//        val intent = Intent(this, HomePageActivity::class.java)
//        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
//        startActivity(intent)
    }

    override fun onDestroy() {
        super.onDestroy()
        webView!!.destroy()
//        unbinder!!.unbind()
    }

//    @OnClick(R.id.rL_accept)
    fun accept(v:View) {
        val intent = Intent()
        intent.putExtra("accept", true)
        setResult(VariableConstants.PRIVACY_POLICY, intent)
        val sessionManager = SessionManager(activity)
        sessionManager.signUpPrivacyAccept = true
        finish()
    }
}