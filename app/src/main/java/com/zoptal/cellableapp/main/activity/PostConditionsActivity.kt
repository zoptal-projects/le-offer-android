package com.zoptal.cellableapp.main.activity

import android.app.Activity
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.annotation.Nullable
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.fcm_push_notification.Config
import com.zoptal.cellableapp.fcm_push_notification.NotificationMessageDialog
import com.zoptal.cellableapp.fcm_push_notification.NotificationUtils.Companion.clearNotifications
import com.zoptal.cellableapp.utility.CommonClass
import com.zoptal.cellableapp.utility.VariableConstants

/**
 * <h>PostConditionsActivity</h>
 *
 *
 * In this class we used to show the listing of conditions.
 *
 * @since 2017-05-04
 */
class PostConditionsActivity : AppCompatActivity(), View.OnClickListener {
    private var mNotificationMessageDialog: NotificationMessageDialog? = null
     override fun onCreate(@Nullable savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_conditions)
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale)
        initVariables()
    }

    /**
     * <h>InitVariables</h>
     *
     *
     * In this method we used to initlialize all variables.
     *
     */
    private fun initVariables() {
        val mActivity: Activity = this@PostConditionsActivity
        mNotificationMessageDialog = NotificationMessageDialog(mActivity)
        CommonClass.statusBarColor(mActivity)
        val tV_description: TextView
        val tV_for_parts: TextView
        val tV_normal_wear: TextView
        val tV_open_box: TextView
        val tV_reconditioned: TextView
        val tV_never_used: TextView
        tV_description = findViewById(R.id.tV_description) as TextView
        tV_description.setOnClickListener(this)
        tV_for_parts = findViewById(R.id.tV_for_parts) as TextView
        tV_for_parts.setOnClickListener(this)
        tV_normal_wear = findViewById(R.id.tV_normal_wear) as TextView
        tV_normal_wear.setOnClickListener(this)
        tV_open_box = findViewById(R.id.tV_open_box) as TextView
        tV_open_box.setOnClickListener(this)
        tV_reconditioned = findViewById(R.id.tV_reconditioned) as TextView
        tV_reconditioned.setOnClickListener(this)
        tV_never_used = findViewById(R.id.tV_never_used) as TextView
        tV_never_used.setOnClickListener(this)
        val rL_back_btn = findViewById(R.id.rL_back_btn) as RelativeLayout
        rL_back_btn.setOnClickListener(this)
    }

    override fun onResume() {
        super.onResume()

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.REGISTRATION_COMPLETE))

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.PUSH_NOTIFICATION))

        // clear the notification area when the app is opened
        clearNotifications(getApplicationContext())
    }

   override fun onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!)
        super.onPause()
    }

    override fun onClick(v: View) {
        val conditions: String
        when (v.id) {
            R.id.tV_description -> {
                conditions = getResources().getString(R.string.other_see_descriptions)
                setConditionsValues(conditions)
            }
            R.id.tV_for_parts -> {
                conditions = getResources().getString(R.string.for_parts)
                setConditionsValues(conditions)
            }
            R.id.tV_normal_wear -> {
                conditions = getResources().getString(R.string.used_normal_wear)
                setConditionsValues(conditions)
            }
            R.id.tV_open_box -> {
                conditions = getResources().getString(R.string.open_box)
                setConditionsValues(conditions)
            }
            R.id.tV_reconditioned -> {
                conditions = getResources().getString(R.string.reconditioned)
                setConditionsValues(conditions)
            }
            R.id.tV_never_used -> {
                conditions = getResources().getString(R.string.new_never_used)
                setConditionsValues(conditions)
            }
            R.id.rL_back_btn -> onBackPressed()
        }
    }

    private fun setConditionsValues(conditions: String) {
        val intent = Intent()
        intent.putExtra("condition", conditions)
        setResult(VariableConstants.CONDITION_REQUEST_CODE, intent)
        onBackPressed()
    }

 override fun onBackPressed() {
        finish()
        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate)
    }
}