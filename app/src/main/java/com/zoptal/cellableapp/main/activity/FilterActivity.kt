package com.zoptal.cellableapp.main.activity

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.WindowManager
import android.widget.*
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.GridLayoutManager
import com.google.gson.Gson
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.adapter.FilterCategoryRvAdapter
import com.zoptal.cellableapp.adapter.filter.FilterAddDetailAdap
import com.zoptal.cellableapp.adapter.filter.FilterSubCategoryRvAdapter
import com.zoptal.cellableapp.fcm_push_notification.Config
import com.zoptal.cellableapp.fcm_push_notification.NotificationMessageDialog
import com.zoptal.cellableapp.fcm_push_notification.NotificationUtils.Companion.clearNotifications
import com.zoptal.cellableapp.get_current_location.FusedLocationReceiver
import com.zoptal.cellableapp.get_current_location.FusedLocationService
import com.zoptal.cellableapp.main.activity.ChangeLocationActivity
import com.zoptal.cellableapp.main.activity.CurrencyListActivity
import com.zoptal.cellableapp.main.activity.FilterActivity
import com.zoptal.cellableapp.pojo_class.filter.SendFilter
import com.zoptal.cellableapp.pojo_class.product_category.*
import com.zoptal.cellableapp.utility.*
import com.zoptal.cellableapp.utility.OkHttp3Connection.OkHttp3RequestCallback
import org.json.JSONObject
import java.util.*

/**
 * <h>FilterActivity</h>
 *
 *
 * This class is called from HomePageFrag class. In this class we used to save the selected
 * filter datas like address, category etc and send back to the HomePageFrag class.
 *
 *
 * @author 3Embed
 * @version 1.0
 * @since 4/17/2017
 */
class FilterActivity : AppCompatActivity(), View.OnClickListener, ProductItemClickListener, ClickListener {
    private var distance_seekbar: SeekBar? = null
    private var radio_NewestFirst: RadioButton? = null
    private var radio_ClosestFirst: RadioButton? = null
    private var radio_highToLow: RadioButton? = null
    private var radio_lowToHigh: RadioButton? = null
    private var radio_last24hr: RadioButton? = null
    private var radio_last7day: RadioButton? = null
    private var radio_last30day: RadioButton? = null
    private var radio_allProduct: RadioButton? = null
    private var locationService: FusedLocationService? = null
    private var  permissionsArray = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
    private var runTimePermission: RunTimePermission? = null
    private var mActivity: Activity? = null
    private var progress_bar_save: ProgressBar? = null
    private var rL_rootElement: RelativeLayout? = null
    private var tV_currency: TextView? = null
    private var tV_setLocation: TextView? = null
    private var tV_starting_dis: TextView? = null
    private var tV_save: TextView? = null
    private var iV_dropDown_currency: ImageView? = null
    private var categoryRvAdapter: FilterCategoryRvAdapter? = null
    private var eT_minprice: EditText? = null
    private var eT_maxprice: EditText? = null
    private var currency_symbol: String? = ""
    private var currency_code: String? = ""
    private var userLat = ""
    private var cityName = ""
    private var userLng = ""
    private var sortByText = ""
    private var postedWithinText = ""
    private var address: String? = ""
    private var sortBy = ""
    private var setDistanceValue: String? = ""
    private var postedWithin = ""
    private var minPrice: String? = ""
    private var maxPrice: String? = ""
    private var aL_categoryDatas: ArrayList<ProductCategoryResDatas>? = null
    private var isToApplyFilter = false
    private var mNotificationMessageDialog: NotificationMessageDialog? = null
    private var mSessionManager: SessionManager? = null
    private var isToStartActivity = false
    var event = ""
    var housing = ""
    var categoryName = ""
    private var subCategoryName: String? = null
    var filterKeyValues = ArrayList<FilterKeyValue>()
    private var linear_filterField: LinearLayout? = null
    private var linear_subCategory: LinearLayout? = null
    private var rV_subCategory: RecyclerView? = null
    private var rV_filterField: RecyclerView? = null
    private var filtersubCategoryRvAdapter: FilterSubCategoryRvAdapter? = null
    private var aL_subCategoryDatas: ArrayList<SubCategoryData>? = null
    private var filterData: ArrayList<FilterData>? = null
    private var filterAddDetailAdap: FilterAddDetailAdap? = null

    //used for send filter array
    var sendFilters = ArrayList<SendFilter>()
    private val isShow = false
    private var progress_bar: ProgressBar? = null
    private val initialSeekBar = true
    private var locationchange = 0
     override fun onCreate(@Nullable savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_filter)
        overridePendingTransition(R.anim.slide_up, R.anim.stay)
        initializeVariables()
    }

    /**
     * <h>InitializeVariables</h>
     *
     *
     * In this method we used to initialize The data member
     * and xml all variable.
     *
     */
    private fun initializeVariables() {
        mActivity = this@FilterActivity
        mSessionManager = SessionManager(mActivity!!)
        mNotificationMessageDialog = NotificationMessageDialog(mActivity)
        isToApplyFilter = false
        isToStartActivity = true

        // Receive datas from HomePage Frag
        val intent: Intent = getIntent()
        aL_categoryDatas = intent.getSerializableExtra("aL_categoryDatas") as ArrayList<ProductCategoryResDatas>
        address = intent.getStringExtra("address")
        setDistanceValue = intent.getStringExtra("distance")
        sortBy = intent.getStringExtra("sortBy")
        postedWithin = intent.getStringExtra("postedWithin")
        currency_code = intent.getStringExtra("currency_code")
        currency_symbol = intent.getStringExtra("currency_symbol")
        minPrice = intent.getStringExtra("minPrice")
        maxPrice = intent.getStringExtra("maxPrice")
        userLat = intent.getStringExtra("userLat")
        userLng = intent.getStringExtra("userLng")
        subCategoryName = intent.getStringExtra("subCategoryName")
        sendFilters = intent.getSerializableExtra("sendFilters") as ArrayList<SendFilter>

        runTimePermission = RunTimePermission(mActivity!!, permissionsArray, false)

        //center progress bar
        progress_bar = findViewById(R.id.progress_bar) as ProgressBar?

        //sub category list
        linear_subCategory = findViewById(R.id.linear_subCategory) as LinearLayout?
        rV_subCategory = findViewById(R.id.rV_subCategory) as RecyclerView?
        rV_subCategory!!.setNestedScrollingEnabled(false)
        rV_subCategory!!.setLayoutManager(LinearLayoutManager(this))

        //add detail list
        linear_filterField = findViewById(R.id.linear_filterField) as LinearLayout?
        rV_filterField = findViewById(R.id.rV_filterField) as RecyclerView?
        rV_filterField!!.setNestedScrollingEnabled(false)
        rV_filterField!!.setLayoutManager(LinearLayoutManager(this))

        // set category recyclerview adapter
        val rV_category: RecyclerView = findViewById(R.id.rV_category) as RecyclerView
        categoryRvAdapter = FilterCategoryRvAdapter(mActivity!!, aL_categoryDatas!!, this)
        val layoutManager = GridLayoutManager(mActivity, 3)
        // Set spacing the recycler view items
        val spanCount = 3 // 5 columns
        val spacing = 20 // 200px
        rV_category.addItemDecoration(ItemDecorationRvItems(spanCount, spacing, true))
        rV_category.setLayoutManager(layoutManager)
        rV_category.setAdapter(categoryRvAdapter)

        // Initialize xml variables
        CommonClass.statusBarColor(mActivity!!)
        rL_rootElement = findViewById(R.id.rL_rootElement) as RelativeLayout?
        tV_save = findViewById(R.id.tV_save) as TextView?
        val tV_reset = findViewById(R.id.tV_reset) as TextView
        tV_reset.setOnClickListener(this)
        progress_bar_save = findViewById(R.id.progress_bar_save) as ProgressBar?
        progress_bar_save!!.visibility = View.GONE
        val rL_save: RelativeLayout
        val rL_back_btn: RelativeLayout
        rL_back_btn = findViewById(R.id.rL_back_btn) as RelativeLayout
        rL_back_btn.setOnClickListener(this)
        rL_save = findViewById(R.id.rL_apply) as RelativeLayout
        rL_save.setOnClickListener(this)
        eT_minprice = findViewById(R.id.eT_minprice) as EditText?
        eT_maxprice = findViewById(R.id.eT_maxprice) as EditText?

        // Distance seekbar
        tV_starting_dis = findViewById(R.id.tV_starting_dis) as TextView?
        distance_seekbar = findViewById(R.id.distance_seekbar) as SeekBar?
        tV_starting_dis!!.text = 25.toString() + " " + getResources().getString(R.string.km)
        distance_seekbar!!.max = 3000
        distance_seekbar!!.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                val setValue = progress.toString() + " " + getResources().getString(R.string.km)
                tV_starting_dis!!.text = setValue
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {}
            override fun onStopTrackingTouch(seekBar: SeekBar) {}
        })
        distance_seekbar!!.progress = 25

        // sorted by value
        radio_NewestFirst = findViewById(R.id.radio_NewestFirst) as RadioButton?
        radio_ClosestFirst = findViewById(R.id.radio_ClosestFirst) as RadioButton?
        radio_highToLow = findViewById(R.id.radio_highToLow) as RadioButton?
        radio_lowToHigh = findViewById(R.id.radio_lowToHigh) as RadioButton?

        // posted within
        radio_last24hr = findViewById(R.id.radio_last24hr) as RadioButton?
        radio_last7day = findViewById(R.id.radio_last7day) as RadioButton?
        radio_last30day = findViewById(R.id.radio_last30day) as RadioButton?
        radio_allProduct = findViewById(R.id.radio_allProduct) as RadioButton?

        // Currency
        val rL_cuurency = findViewById(R.id.rL_currency) as RelativeLayout
        rL_cuurency.setOnClickListener(this)
        tV_currency = findViewById(R.id.tV_currency) as TextView?
        tV_setLocation = findViewById(R.id.tV_setLocation) as TextView?
        iV_dropDown_currency = findViewById(R.id.iV_dropDown_currency) as ImageView?
        setDefaultCurrency()

        // Change location
        val rL_changeLoc = findViewById(R.id.rL_changeLoc) as RelativeLayout
        rL_changeLoc.setOnClickListener(this)
        if (runTimePermission!!.checkPermissions(permissionsArray)) {
            if (!isLocationFound(userLat, userLng)) {
                println(TAG + " " + "is location found=" + isLocationFound(userLat, userLng))
                isToApplyFilter = false
                currentLocation
            }
        } else {
            runTimePermission!!.requestPermission()
        }
        setFilterDatas()

        //intialize fragments of categories
        /*eventFrag = new EventFrag();
        housingFrag = new HousingFrag();
        textbookFrag = new TextbookFrag();
        transportationFrag = new TransportationFrag();
        tutorFrag = new TutorFrag();*/
    }

    /**
     * <h>setFilterDatas</h>
     *
     *
     * In this method we used set the xml variables
     *
     */
    private fun setFilterDatas() {
        // set address
        if (address != null && !address!!.isEmpty()) tV_setLocation!!.text = address

        // set distance
        if (setDistanceValue != null && !setDistanceValue!!.isEmpty()) {
            distance_seekbar!!.progress = setDistanceValue!!.toInt()
            setDistanceValue = setDistanceValue + " " + getResources().getString(R.string.km)
            tV_starting_dis!!.text = setDistanceValue
            Log.d(TAG, "setFilterDatas: sdfsdf")
        }
        println("$TAG sortBy value=$sortBy")
        when (sortBy) {
            "newFirst" -> radio_NewestFirst!!.isChecked = true
            "closestFirst" -> radio_ClosestFirst!!.isChecked = true
            "heighToLow" -> radio_highToLow!!.isChecked = true
            "lowToHigh" -> radio_lowToHigh!!.isChecked = true
        }
        when (postedWithin) {
            "1" -> radio_last24hr!!.isChecked = true
            "7" -> radio_last7day!!.isChecked = true
            "30" -> radio_last30day!!.isChecked = true
        }

        // set currency code
        if (currency_code != null && !currency_code!!.isEmpty()) tV_currency!!.text = currency_code

        // min price
        if (minPrice != null && !minPrice!!.isEmpty()) eT_minprice!!.setText(minPrice)
        if (maxPrice != null && !maxPrice!!.isEmpty()) eT_maxprice!!.setText(maxPrice)
    }

    /**
     * <h>SetDefaultCurrency</h>
     *
     *
     * In this method we used to find the current country currency.
     *
     */
    private fun setDefaultCurrency() {
        val map = CommonClass.getCurrencyLocaleMap()
        val countryIsoCode = Locale.getDefault().country
        val locale = Locale("EN", countryIsoCode)
        val currency = Currency.getInstance(locale)
        currency_symbol = currency.getSymbol(map[currency])
        if (currency_symbol != null && !currency_symbol!!.isEmpty()) {
            tV_currency!!.text = currency.toString()
            currency_code = currency.toString()
        }
    }

 override fun onBackPressed() {
        finish()
        overridePendingTransition(R.anim.stay, R.anim.slide_down)
    }

    override fun onClick(v: View) {
        val intent: Intent
        when (v.id) {
            R.id.rL_back_btn -> onBackPressed()
            R.id.rL_apply -> {
                var minValue = 0.0
                var maxValue = 0.0
                if (!eT_minprice!!.text.toString().isEmpty()) minValue = eT_minprice!!.text.toString().toDouble()
                if (!eT_maxprice!!.text.toString().isEmpty()) maxValue = eT_maxprice!!.text.toString().toDouble()
                if (maxValue >= minValue) {
                    if (tV_setLocation!!.text.toString() != getResources().getString(R.string.change_Location)) {
                        if (!isLocationFound(userLat, userLng)) {
                            if (runTimePermission!!.checkPermissions(permissionsArray)) {
                                isToApplyFilter = true
                                tV_save!!.visibility = View.GONE
                                progress_bar_save!!.visibility = View.VISIBLE
                                currentLocation
                            } else {
                                runTimePermission!!.requestPermission()
                            }
                        } else {
                            allSavedValues
                        }
                    } else CommonClass.showSnackbarMessage(rL_rootElement, getResources().getString(R.string.please_select_your_loc))
                } else CommonClass.showSnackbarMessage(rL_rootElement, getResources().getString(R.string.min_price_cant_be_higher))
            }
            R.id.tV_reset -> {
                isToApplyFilter = false
                currentLocation
                resetFilterDatas()
            }
//            R.id.rL_currency -> if (isToStartActivity) {
//                isToStartActivity = false
//                intent = Intent(mActivity, CurrencyListActivity::class.java)
//                startActivityForResult(intent, VariableConstants.CURRENCY_REQUEST_CODE)
//            }
            R.id.rL_changeLoc -> if (isToStartActivity) {
                isToStartActivity = false
                intent = Intent(mActivity, ChangeLocationActivity::class.java)
                intent.putExtra("location", tV_setLocation!!.text.toString())
                startActivityForResult(intent, VariableConstants.CHANGE_LOC_REQ_CODE)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        isToStartActivity = true
        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.REGISTRATION_COMPLETE))

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.PUSH_NOTIFICATION))

        // clear the notification area when the app is opened
        clearNotifications(getApplicationContext())
    }

   override fun onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!)
        super.onPause()
    }

    /**
     * In this method we find current location using FusedLocationApi.
     * in this we have onUpdateLocation() method in which we check if
     * its not null then We call guest user api.
     */
    private val currentLocation: Unit
        private get() {
            locationService = FusedLocationService(mActivity!!, object : FusedLocationReceiver {
                override fun onUpdateLocation() {
                    val currentLocation = locationService!!.receiveLocation()
                    if (currentLocation != null) {
                        userLat = currentLocation.latitude.toString()
                        userLng = currentLocation.longitude.toString()
                        progress_bar_save!!.visibility = View.GONE
                        if (isLocationFound(userLat, userLng)) {
                            println("$TAG lat=$userLat lng=$userLng")
                            mSessionManager!!.currentLat = userLat
                            mSessionManager!!.currentLng = userLng
                            if (isToApplyFilter) allSavedValues
                            val address = CommonClass.getCompleteAddressString(mActivity, currentLocation.latitude, currentLocation.longitude)
                            if (!address.isEmpty()) tV_setLocation!!.text = address else GetCompleteAddress(mActivity!!, userLat, userLng, tV_setLocation!!, null)
                        }
                    }
                }
            }
            )
        }

    /**
     * <h>GetAllSavedValues</h>
     *
     *
     * In this method we fetch all saved values like categories, distance, price etc
     * once we receive all those values we send back all those values to previous
     * activity and do filter operation.
     *
     */
    private val allSavedValues: Unit
        private get() {
            var distance: String
            val postedWithin: String
            val minPrice = eT_minprice!!.text.toString()
            val maxPrice = eT_maxprice!!.text.toString()
            distance = distance_seekbar!!.progress.toString() + ""
            postedWithin = postedWithinValue
            getSortBy()
            if (distance == "0") distance = ""
            println(TAG + " " + "currency code=" + tV_currency!!.text.toString() + " " + " " + "distance=" + distance + " " + "latitude=" + userLat + " " + "long=" + userLng + " " + "maxPrice=" + maxPrice + " " + "min price=" + minPrice + " " + "postedWithin=" + postedWithin + " " + "sortBy=" + sortBy + " " + "location=" + tV_setLocation!!.text.toString() + " ")
            val filterIntent = Intent()
            filterIntent.putExtra("aL_categoryDatas", aL_categoryDatas)
            if (setDistanceValue!!.isEmpty() && distance == "25") filterIntent.putExtra("distance", "") else filterIntent.putExtra("distance", distance)
            filterIntent.putExtra("locationChange", locationchange.toString())
            filterIntent.putExtra("currentLatitude", userLat)
            filterIntent.putExtra("currentLongitude", userLng)
            filterIntent.putExtra("address", tV_setLocation!!.text.toString())
            filterIntent.putExtra("sortBy", sortBy)
            filterIntent.putExtra("postedWithin", postedWithin)
            filterIntent.putExtra("minPrice", minPrice)
            filterIntent.putExtra("maxPrice", maxPrice)
            filterIntent.putExtra("currency_code", currency_code)
            filterIntent.putExtra("currency", currency_symbol)
            filterIntent.putExtra("postedWithinText", postedWithinText)
            filterIntent.putExtra("sortByText", sortByText)
            filterIntent.putExtra("categoryName", categoryName)
            filterIntent.putExtra("subCategoryName", subCategoryName)
            filterIntent.putExtra("sendFilters", sendFilters)
            filterIntent.putExtra("cityName", cityName)
            setResult(VariableConstants.FILTER_REQUEST_CODE, filterIntent)
            progress_bar_save!!.visibility = View.GONE
            onBackPressed()
        }

    /**
     * <h>ResetFilterDatas</h>
     *
     *
     * In this method we used to reset all the selected filtered values.
     *
     */
    private fun resetFilterDatas() {
        // clear selected category
        if (aL_categoryDatas != null && aL_categoryDatas!!.size > 0) {
            for (productCategoryResDatas in aL_categoryDatas!!) {
                println(TAG + " " + "name=" + productCategoryResDatas.name + " " + "selected value=" + productCategoryResDatas.isSelected)
                productCategoryResDatas.isSelected = false
            }
        }
        categoryRvAdapter!!.notifyDataSetChanged()
        categoryName = ""
        subCategoryName = ""
        sendFilters.clear()
        linear_subCategory!!.visibility = View.GONE
        linear_filterField!!.visibility = View.GONE

        // location
        address = getResources().getString(R.string.change_Location)
        userLat = ""
        userLng = ""

        // set distance
        setDistanceValue = getResources().getString(R.string.zero)

        // sort by
        sortBy = ""
        radio_NewestFirst!!.isChecked = false
        radio_ClosestFirst!!.isChecked = false
        radio_highToLow!!.isChecked = false
        radio_lowToHigh!!.isChecked = false

        // posted within
        postedWithin = ""
        radio_last24hr!!.isChecked = false
        radio_last7day!!.isChecked = false
        radio_last30day!!.isChecked = false
        radio_allProduct!!.isChecked = false

        // currency
        setDefaultCurrency()

        // price
        eT_minprice!!.text.clear()
        eT_maxprice!!.text.clear()
        maxPrice = ""
        minPrice = ""
        println(TAG + " " + "min price=" + eT_minprice!!.text.toString() + " " + "max price=" + eT_maxprice!!.text.toString())

        //eT_minprice.setHint(getResources().getString(R.string.default_price));
        //eT_maxprice.setHint(getResources().getString(R.string.default_price));
        setFilterDatas()
    }

    /**
     * <h>GetSortBy</h>
     *
     *
     * In this method we used to set sort by value according to checked sort by checked radio button.
     *
     */
    private fun getSortBy() {
        if (radio_NewestFirst!!.isChecked) {
            sortBy = getResources().getString(R.string.postedOnDesc)
            sortByText = getResources().getString(R.string.newest_first)
        } else if (radio_ClosestFirst!!.isChecked) {
            sortBy = getResources().getString(R.string.distanceAsc)
            sortByText = getResources().getString(R.string.closest_first)
        } else if (radio_highToLow!!.isChecked) {
            sortBy = getResources().getString(R.string.priceDsc)
            sortByText = getResources().getString(R.string.price_high_to_low)
        } else if (radio_lowToHigh!!.isChecked) {
            sortBy = getResources().getString(R.string.priceAsc)
            sortByText = getResources().getString(R.string.price_low_to_high)
        } else sortBy = ""
    }// i.e by default all product// i.e 30 day// i.e seven day// i.e one day

    /**
     * <h>GetPostedWithinValue</h>
     *
     *
     * In this method we used to find any one selected Posted Within Value since
     * it contains radiobutton.
     *
     *
     * @return The selected sort value
     */
    private val postedWithinValue: String
        private get() {
            var postedWithin = ""
            if (radio_last24hr!!.isChecked) {
                postedWithin = "1" // i.e one day
                postedWithinText = getResources().getString(R.string.the_last_24hr)
            } else if (radio_last7day!!.isChecked) {
                postedWithin = "7" // i.e seven day
                postedWithinText = getResources().getString(R.string.the_last_7day)
            } else if (radio_last30day!!.isChecked) {
                postedWithin = "30" // i.e 30 day
                postedWithinText = getResources().getString(R.string.the_last_30day)
            } else if (radio_allProduct!!.isChecked) {
                postedWithin = "" // i.e by default all product
                postedWithinText = getResources().getString(R.string.all_producs)
            }
            return postedWithin
        }

    /**
     * In this method we used to check whether current lat and
     * long has been received or not.
     *
     * @param lat The current latitude
     * @param lng The current longitude
     * @return boolean flag true or false
     */
    private fun isLocationFound(lat: String?, lng: String?): Boolean {
        return !(lat == null || lat.isEmpty()) && !(lng == null || lng.isEmpty())
    }

    override fun onRequestPermissionsResult(requestCode: Int, @NonNull permissions: Array<String?>, @NonNull grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            VariableConstants.PERMISSION_REQUEST_CODE -> {
                println("grant result=" + grantResults.size)
                if (grantResults.size > 0) {
                    var count = 0
                    while (count < grantResults.size) {
                        if (grantResults[count] != PackageManager.PERMISSION_GRANTED) runTimePermission!!.allowPermissionAlert(permissions[count]!!)
                        count++
                    }
                    println("isAllPermissionGranted=" + runTimePermission!!.checkPermissions(permissionsArray))
                    if (runTimePermission!!.checkPermissions(permissionsArray)) {
                        currentLocation
                    }
                }
            }
        }
    }

   override  protected fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        println("$TAG on activity result called.... req code=$requestCode res code=$resultCode data=$data")
        if (data != null) {
            when (requestCode) {
                VariableConstants.CURRENCY_REQUEST_CODE -> {
                    currency_code = data.getStringExtra("cuurency_code")
                    currency_symbol = data.getStringExtra("currency_symbol")

                    // set currency cde eg. Inr
                    if (currency_code != null) tV_currency!!.text = currency_code

                    // change dropdown icon
                    iV_dropDown_currency!!.setImageResource(R.drawable.drop_down_black_color_icon)
                }
                VariableConstants.CHANGE_LOC_REQ_CODE -> {
                    userLat = data.getStringExtra("lat")
                    userLng = data.getStringExtra("lng")
                    address = data.getStringExtra("address")
                    tV_setLocation!!.text = address
                    cityName = data.getStringExtra("cityName")
                    locationchange = 1
                    println("$TAG lat=$userLat lng=$userLng address=$address")
                }
            }
        }
    }

    override fun onItemClick(pos: Int, imageView: ImageView?) {

        // hide while selected other category
        linear_filterField!!.visibility = View.GONE
        categoryName = if (aL_categoryDatas!![pos!!].isSelected) aL_categoryDatas!![pos].name else ""
        sendFilters.clear()
        subCategoryName = ""
        println(TAG + "selected categoryName:" + categoryName)
        if (aL_categoryDatas!![pos].subCategoryCount > 0 && !categoryName.isEmpty()) {
            subCategoriesService
        } else if (aL_categoryDatas!![pos].filterCount > 0) {
            setCategoryFilterData(pos)
        } else {
            linear_subCategory!!.visibility = View.GONE
            linear_filterField!!.visibility = View.GONE
        }
    }

    fun setCategoryFilterData(pos: Int) {
        linear_filterField!!.visibility = View.VISIBLE
        linear_subCategory!!.visibility = View.GONE
        filterData = aL_categoryDatas!![pos].filter
        filterAddDetailAdap = FilterAddDetailAdap(mActivity!!, filterData!!)
        rV_filterField!!.setAdapter(filterAddDetailAdap)
    }//handle for unselected//  if already selected once

    /**
     * <h>GetCategoriesService</h>
     *
     *
     * This method is called from onCreate() method of the current class.
     * In this method we used to call the getCategories api using okHttp3.
     * Once we get the data we show that list in recyclerview.
     *
     */
    val subCategoriesService: Unit
        get() {
            if (CommonClass.isNetworkAvailable(mActivity!!)) {
                progress_bar!!.visibility = View.VISIBLE
                val request_datas = JSONObject()
                val Url = ApiUrl.GET_SUB_CATEGORIES + "?categoryName=" + categoryName
                OkHttp3Connection.doOkHttp3Connection(TAG, Url, OkHttp3Connection.Request_type.GET, request_datas, object : OkHttp3RequestCallback {
                     override fun onSuccess(result: String?, user_tag: String?) {
                        println("$TAG get sub category res=$result")
                        val subCategoryMainPojo: SubCategoryMainPojo
                        val gson = Gson()
                        subCategoryMainPojo = gson.fromJson(result, SubCategoryMainPojo::class.java)
                        when (subCategoryMainPojo.code) {
                            "200" -> {
                                progress_bar!!.visibility = View.GONE
                                linear_subCategory!!.visibility = View.VISIBLE
                                aL_subCategoryDatas = subCategoryMainPojo.data
                                filtersubCategoryRvAdapter = FilterSubCategoryRvAdapter(mActivity!!, aL_subCategoryDatas!!)
                                rV_subCategory?.setAdapter(filtersubCategoryRvAdapter)

                                //  if already selected once
                                setSubCategoryData()
                                if (aL_subCategoryDatas != null && aL_subCategoryDatas!!.size > 0) {
                                    filtersubCategoryRvAdapter!!.setOnItemClick(object : ClickListener {
                                        override fun onItemClick(view: View?, position: Int) {
                                            sendFilters.clear()
                                            if (filtersubCategoryRvAdapter!!.mSelected >= 0) {
                                                subCategoryName = aL_subCategoryDatas!![position].subCategoryName
                                                linear_filterField!!.visibility = View.VISIBLE
                                            } else { //handle for unselected
                                                subCategoryName = ""
                                                linear_filterField!!.visibility = View.GONE
                                            }
                                            println(TAG + "selected subCategoryName:" + subCategoryName)
                                            if (aL_subCategoryDatas!![position].fieldCount > 0) {
                                                linear_filterField!!.visibility = View.VISIBLE
                                                filterData = aL_subCategoryDatas!![position].filter
                                                filterAddDetailAdap = FilterAddDetailAdap(mActivity!!, filterData!!)
                                                rV_filterField?.setAdapter(filterAddDetailAdap)
                                            } else {
                                                linear_filterField!!.visibility = View.GONE
                                            }
                                        }
                                    })
                                }
                            }
                            else -> progress_bar!!.visibility = View.GONE
                        }
                    }

                     override fun onError(error: String?, user_tag: String?) {
                        progress_bar!!.visibility = View.GONE
                        CommonClass.showSnackbarMessage(rL_rootElement, error)
                    }
                })
            } else CommonClass.showSnackbarMessage(rL_rootElement, getResources().getString(R.string.NoInternetAccess))
        }

    override fun onItemClick(view: View?, position: Int) {
        TODO("Not yet implemented")
    }
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        return true
    }

    fun setSubCategoryData() {
        if (subCategoryName != null && !subCategoryName!!.isEmpty()) {
            for (i in aL_subCategoryDatas!!.indices) {
                // set selected subcat name
                if (aL_subCategoryDatas!![i].subCategoryName.equals(subCategoryName, ignoreCase = true)) {
                    filtersubCategoryRvAdapter!!.mSelected = i

                    //shows selected subcat fields
                    if (aL_subCategoryDatas!![i].fieldCount > 0) {
                        filterData = aL_subCategoryDatas!![i].filter
                        filterAddDetailAdap = FilterAddDetailAdap(mActivity!!, filterData!!)
                        rV_filterField!!.setAdapter(filterAddDetailAdap)
                        linear_filterField!!.visibility = View.VISIBLE
                    }
                }
            }
        }
    }

    companion object {
        private val TAG = FilterActivity::class.java.simpleName
    }
}