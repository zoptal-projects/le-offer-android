package com.zoptal.cellableapp.main

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import com.facebook.*
import com.facebook.appevents.AppEventsLogger
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.gson.Gson
import com.zoptal.cellableapp.BuildConfig
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.get_current_location.FusedLocationReceiver
import com.zoptal.cellableapp.get_current_location.FusedLocationService
import com.zoptal.cellableapp.main.activity.LoginOrSignupActivity
import com.zoptal.cellableapp.mqttchat.AppController
import com.zoptal.cellableapp.pojo_class.LogDevicePojo
import com.zoptal.cellableapp.pojo_class.LoginResponsePojo
import com.zoptal.cellableapp.utility.*
import com.zoptal.cellableapp.utility.OkHttp3Connection.OkHttp3RequestCallback
import org.json.JSONException
import org.json.JSONObject
import java.util.*

import kotlin.collections.HashMap
/**
 * <h>LoginWithFacebook</h>
 *
 *
 * This class is getting called when user Click on Login with facebook from Login screen(LoginActivity).
 * In this class we do all operation regarding facebook. First of all we retrieve user all information
 * using facebook graph api 2.0 once we get after that we call Facebook login api and pass user facebook
 * id. if we get code 200 that means success then move to HomePageActivity or else call signUp api.
 *
 * @since 04/04/17
 * @author 3Embed
 * @version 1.0
 */
class LoginWithFacebook(private val mActivity: Activity, private val rL_rootview: RelativeLayout, private val pBar_fbLogin: ProgressBar, private val iV_fbicon: ImageView, private val tV_facebook: TextView) {
    //facebook variables
    private val callbackManager: CallbackManager
    private var Fb_emailId = ""
    private var Fb_firstName = ""
    private var fb_lastName = ""
    private var fb_pic = ""
    private var facebookId: String? = ""
    private var fullName = ""
    private var fb_accessToken = ""
    private var currentLat = ""
    private var currentLng = ""
    private var address = ""
    private var city = ""
    private var countryCode = ""
    private val mProgressBar: ProgressBarHandler
    private val mSessionManager: SessionManager
    private var locationService: FusedLocationService? = null
    private val runTimePermission: RunTimePermission
    private val permissionsArray: Array<String>

    /**
     * <h>LoginFacebookSdk</h>
     *
     *
     * This method is being called from onCreate method. this method is called
     * when user click on LoginWithFacebook button. it contains three method onSuccess,
     * onCancel and onError. if login will be successfull then success method will be
     * called and in that method we obtain user all details like id, email, name, profile pic
     * etc. onCancel method will be called if user click on facebook with login button and
     * suddenly click back button. onError method will be called if any problem occurs like
     * internet issue.
     *
     */
    private fun initializeFacebookSdk() {
        LoginManager.getInstance().registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(loginResult: LoginResult) {
                val request = GraphRequest.newMeRequest(loginResult.accessToken) { `object`, response ->
                    Log.d("LoginActivity", response.toString())
                    if (response.error == null) {
                        facebookId = `object`.optString("id")
                        Fb_emailId = `object`.optString("email")
                        Fb_firstName = `object`.optString("first_name")
                        fb_lastName = `object`.optString("last_name")
                        fb_pic = "https://graph.facebook.com/$facebookId/picture?type=large"
                        fullName = "$Fb_firstName $fb_lastName"
                        val token = AccessToken.getCurrentAccessToken()
                        if (token != null) fb_accessToken = token.token
                        println(TAG + " " + "facebookId=" + facebookId + " " + "accessToken=" + token!!.token)
                        if (facebookId != null && !facebookId!!.isEmpty()) {
                            val lm = mActivity.getSystemService(Context.LOCATION_SERVICE) as LocationManager
                            val isLocationEnabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER)
                            println(TAG + " " + "is location enabled=" + isLocationEnabled + " " + "is permission allowed=" + runTimePermission.checkPermissions(permissionsArray))
                            facebookLoginProgress(true)
                            if (isLocationEnabled && runTimePermission.checkPermissions(permissionsArray)) currentLocation else loginWithFbApi()
                        }
                    }
                }
                val parameters = Bundle()
                parameters.putString("fields", "id,email,gender, birthday,first_name,last_name")
                request.parameters = parameters
                request.executeAsync()
            }

            override fun onCancel() {
                CommonClass.showSnackbarMessage(rL_rootview, mActivity.resources.getString(R.string.Loginfailed))
            }

            override fun onError(error: FacebookException) {
                CommonClass.showSnackbarMessage(rL_rootview, error.toString())
                if (error is FacebookAuthorizationException) {
                    if (AccessToken.getCurrentAccessToken() != null) {
                        LoginManager.getInstance().logOut()
                    }
                }
            }
        })
    }

    /**
     * In this method we find current location using FusedLocationApi.
     * in this we have onUpdateLocation() method in which we check if
     * its not null then We call guest user api.
     */
    val currentLocation: Unit
        get() {
            if (CommonClass.isNetworkAvailable(mActivity)) {
                locationService = FusedLocationService(mActivity, object : FusedLocationReceiver {
                    override fun onUpdateLocation() {
                        val currentLocation = locationService!!.receiveLocation()
                        if (currentLocation != null) {
                            currentLat = currentLocation.latitude.toString()
                            currentLng = currentLocation.longitude.toString()
                            if (isLocationFound(currentLat, currentLng)) {
                                mSessionManager.currentLat = currentLat
                                mSessionManager.currentLng = currentLng
                                address = CommonClass.getCompleteAddressString(mActivity, currentLocation.latitude, currentLocation.longitude)
                                city = CommonClass.getCityName(mActivity, currentLocation.latitude, currentLocation.longitude)
                                countryCode = CommonClass.getCountryCode(mActivity, currentLocation.latitude, currentLocation.longitude)
                                loginWithFbApi()
                            }
                        }
                    }
                }
                )
            } else CommonClass.showSnackbarMessage(rL_rootview, mActivity.resources.getString(R.string.NoInternetAccess))
        }

    /**
     * In this method we used to check whether current currentLat and
     * long has been received or not.
     * @param lat The current latitude
     * @param lng The current longitude
     * @return boolean flag true or false
     */
    private fun isLocationFound(lat: String?, lng: String?): Boolean {
        return !(lat == null || lat.isEmpty()) && !(lng == null || lng.isEmpty())
    }

    /**
     * <h>LoginWithFbApi</h>
     *
     *
     * This method is called from initializeFacebookSdk() after getting user complete
     * information like facebook_id,name,email etc from Facebook login. In this method
     * we call login api and pass facebook id which we got from facebook api. After
     * login response we check whether This facebook id is register or not. if not
     * then call facebook signup api.
     *
     */
    private fun loginWithFbApi() {
        if (CommonClass.isNetworkAvailable(mActivity)) {
            //mProgressBar.show();
            val requestDatas = JSONObject()
            try {
                // loginType, pushToken, place, city, countrySname, latitude, longitude,facebookId, email
                requestDatas.put("loginType", VariableConstants.TYPE_FACEBOOK)
                requestDatas.put("pushToken", mSessionManager.pushToken)
                requestDatas.put("place", address)
                requestDatas.put("city", city)
                requestDatas.put("countrySname", countryCode)
                requestDatas.put("latitude", currentLat)
                requestDatas.put("longitude", currentLng)
                requestDatas.put("facebookId", facebookId)
                requestDatas.put("email", Fb_emailId)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            OkHttp3Connection.doOkHttp3Connection(TAG, ApiUrl.LOGIN, OkHttp3Connection.Request_type.POST, requestDatas, object : OkHttp3RequestCallback {
                override fun onSuccess(result: String?, user_tag: String?) {
                    println("$TAG facebook login res=$result")
                    val loginResponsePojo: LoginResponsePojo
                    val gson = Gson()
                    loginResponsePojo = gson.fromJson(result, LoginResponsePojo::class.java)
                    when (loginResponsePojo.code) {
                        "200" -> {
                            mProgressBar.hide()
                            mSessionManager.setmqttId(loginResponsePojo.mqttId)
                            mSessionManager.isUserLoggedIn = true
                            mSessionManager.authToken = loginResponsePojo.token
                            mSessionManager.userName = loginResponsePojo.username
                            mSessionManager.userImage = loginResponsePojo.profilePicUrl
                            mSessionManager.userId = loginResponsePojo.userId
                            mSessionManager.loginWith = "facebookLogin"
                            initUserDetails(loginResponsePojo.profilePicUrl, loginResponsePojo.mqttId, Fb_emailId, "$Fb_firstName $fb_lastName", loginResponsePojo.token)
                            logDeviceInfo(loginResponsePojo.token)
                        }
                        "204" -> {
                            //signUpWithFb();
                            println("$TAG fb_pic=$fb_pic")
                            val intent = Intent(mActivity, LoginOrSignupActivity::class.java)
                            intent.putExtra("type", "fbSignUp")
                            intent.putExtra("userFullName", fullName)
                            intent.putExtra("userImageUrl", fb_pic)
                            intent.putExtra("email", Fb_emailId)
                            intent.putExtra("id", facebookId)
                            intent.putExtra("fb_accessToken", fb_accessToken)
                            mActivity.startActivityForResult(intent, VariableConstants.LOGIN_SIGNUP_REQ_CODE)
                            facebookLoginProgress(false)
                        }
                        else -> {
                            CommonClass.showSnackbarMessage(rL_rootview, loginResponsePojo.message)
                            facebookLoginProgress(false)
                        }
                    }
                }

                override fun onError(error: String?, user_tag: String?) {
                    mProgressBar.hide()
                    facebookLoginProgress(false)
                    CommonClass.showSnackbarMessage(rL_rootview, error)
                }
            })
        } else CommonClass.showSnackbarMessage(rL_rootview, mActivity.resources.getString(R.string.NoInternetAccess))
    }

    /**
     * <h>LogDeviceInfo</h>
     *
     *
     * In this method we used to do api call to send device information like device name
     * model number, device id etc to server to log the the user with specific device.
     *
     * @param token The auth token for particular user
     */
    private fun logDeviceInfo(token: String) {
        if (CommonClass.isNetworkAvailable(mActivity)) {
            //deviceName, deviceId, deviceOs, modelNumber, appVersion
            val request_datas = JSONObject()
            try {
                request_datas.put("deviceName", Build.BRAND)
                request_datas.put("deviceId", mSessionManager.deviceId)
                request_datas.put("deviceOs", Build.VERSION.RELEASE)
                request_datas.put("modelNumber", Build.MODEL)
                request_datas.put("appVersion", BuildConfig.VERSION_NAME)
                request_datas.put("token", token)
                request_datas.put("deviceType", VariableConstants.DEVICE_TYPE)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            OkHttp3Connection.doOkHttp3Connection(TAG, ApiUrl.LOG_DEVICE, OkHttp3Connection.Request_type.POST, request_datas, object : OkHttp3RequestCallback {
                override fun onSuccess(result: String?, user_tag: String?) {
                    facebookLoginProgress(false)
                    println("$TAG log device info=$result")
                    val logDevicePojo: LogDevicePojo
                    val gson = Gson()
                    logDevicePojo = gson.fromJson(result, LogDevicePojo::class.java)
                    when (logDevicePojo.code) {
                        "200" -> {
                            // Open Home page screen
                            facebookLoginProgress(false)
                            //mActivity.finish();
                            val intent = Intent()
                            intent.putExtra("isToRefreshHomePage", true)
                            mActivity.setResult(VariableConstants.LANDING_REQ_CODE, intent)
                            mActivity.finish()
                        }
                        else -> CommonClass.showSnackbarMessage(rL_rootview, logDevicePojo.message)
                    }
                }


                 override fun onError(error: String?, user_tag: String?) {
                    facebookLoginProgress(false)
                    CommonClass.showSnackbarMessage(rL_rootview, error)
                }
            })
        } else CommonClass.showSnackbarMessage(rL_rootview, mActivity.resources.getString(R.string.NoInternetAccess))
    }

    fun fbOnActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        callbackManager.onActivityResult(requestCode, resultCode, data)
    }

    fun loginWithFbWithSdk() {
        if (CommonClass.isNetworkAvailable(mActivity)) LoginManager.getInstance().logInWithReadPermissions(mActivity, Arrays.asList("public_profile", "email")) //"user_friends",
        else CommonClass.showSnackbarMessage(rL_rootview, mActivity.resources.getString(R.string.NoInternetAccess))
    }

    private fun facebookLoginProgress(isVisible: Boolean) {
        if (isVisible) {
            pBar_fbLogin.visibility = View.VISIBLE
            iV_fbicon.visibility = View.GONE
            tV_facebook.visibility = View.GONE
        } else {
            pBar_fbLogin.visibility = View.GONE
            iV_fbicon.visibility = View.VISIBLE
            tV_facebook.visibility = View.VISIBLE
        }
    }

    /*
    * Initialization of the user details .*/
    private fun initUserDetails(profile_Url: String?, userId: String?, email: String, userName: String, token: String) {
        val db = AppController.instance?.dbController
        val map: HashMap<String, Any> = HashMap()
        if (profile_Url != null && !profile_Url.isEmpty()) {
            map["userImageUrl"] = profile_Url
        } else {
            map["userImageUrl"] = ""
        }
        map["userIdentifier"] = email
        map["userId"] = userId!!
        map["userName"] = userName
        map["apiToken"] = token
        if (userId == null || userId.isEmpty()) {
            Toast.makeText(mActivity, R.string.userIdtext, Toast.LENGTH_SHORT).show()
            return
        }
        if (!db!!.checkUserDocExists(AppController.instance?.indexDocId, userId)) {
            val userDocId = db.createUserInformationDocument(map)
            db.addToIndexDocument(AppController.instance?.indexDocId, userId, userDocId)
        } else {
            db.updateUserDetails(db.getUserDocId(userId, AppController.instance?.indexDocId!!), map)
        }
        db.updateIndexDocumentOnSignIn(AppController.instance?.indexDocId, userId)
        AppController.instance?.setSignedIn(true, userId, userName, email)
        AppController.instance?.isSignStatusChanged = true
    }

    companion object {
        private val TAG = LoginWithFacebook::class.java.simpleName
    }

    init {
        mSessionManager = SessionManager(mActivity)
        currentLat = mSessionManager.currentLat!!
        currentLng = mSessionManager.currentLng!!
        permissionsArray = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
        runTimePermission = RunTimePermission(mActivity, permissionsArray, false)
        if (isLocationFound(currentLat, currentLng)) {
            address = CommonClass.getCompleteAddressString(mActivity, currentLat.toDouble(), currentLng.toDouble())
            city = CommonClass.getCityName(mActivity, currentLat.toDouble(), currentLng.toDouble())
            countryCode = CommonClass.getCountryCode(mActivity, currentLat.toDouble(), currentLng.toDouble())
            //loginRequestApi();
        }
        Log.e("==============",mActivity.toString())
        mProgressBar = ProgressBarHandler(mActivity)

//        FacebookSdk.sdkInitialize(getApplicationContext());
//        AppEventsLogger.activateApp(mActivity);
        callbackManager = CallbackManager.Factory.create()
        initializeFacebookSdk()
    }
}