package com.zoptal.cellableapp.main.activity

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.annotation.Nullable
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.fcm_push_notification.Config
import com.zoptal.cellableapp.fcm_push_notification.NotificationMessageDialog
import com.zoptal.cellableapp.fcm_push_notification.NotificationUtils.Companion.clearNotifications
import com.zoptal.cellableapp.main.activity.GetTotalClicksActivity
import com.zoptal.cellableapp.pojo_class.get_total_clicks_pojo.TotalClicksDatas
import com.zoptal.cellableapp.pojo_class.get_total_clicks_pojo.TotalClicksMainPojo
import com.zoptal.cellableapp.utility.ApiUrl
import com.zoptal.cellableapp.utility.CommonClass
import com.zoptal.cellableapp.utility.OkHttp3Connection
import com.zoptal.cellableapp.utility.OkHttp3Connection.OkHttp3RequestCallback
import com.zoptal.cellableapp.utility.SessionManager
import org.json.JSONException
import org.json.JSONObject
import java.util.*

/**
 * <h>GetTotalClicksActivity</h>
 *
 *
 * This class is called from InsightActivity. In this class we used to show the list of city and its clicks from
 * the particular county.
 *
 * @since 22-Aug-17
 * @version 1.0
 * @author 3Embed
 */
class GetTotalClicksActivity : AppCompatActivity(), View.OnClickListener {
    private var mActivity: Activity? = null
    private var mSessionManager: SessionManager? = null
    private var postId: String? = null
    private var countrySname: String? = null
    private var linear_rootElement: LinearLayout? = null
    private var linear_location: LinearLayout? = null
    private var mNotificationMessageDialog: NotificationMessageDialog? = null
     override fun onCreate(@Nullable savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_total_clicks)
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale)

        // Receiving datas from last class
        val intent: Intent = getIntent()
        postId = intent.getStringExtra("postId")
        countrySname = intent.getStringExtra("countrySname")
        mActivity = this@GetTotalClicksActivity
        mNotificationMessageDialog = NotificationMessageDialog(mActivity)
        mSessionManager = SessionManager(mActivity!!)
        initVariables()
        countriesTotalClick
    }

    /**
     * <h>initVariables</h>
     *
     *
     * In this method we used to assign the xml variables
     *
     */
    private fun initVariables() {
        // set country name
        val tV_country_name = findViewById(R.id.tV_country_name) as TextView
        if (countrySname != null && !countrySname!!.isEmpty()) {
            val loc = Locale("", countrySname)
            val setCountryName: String = getResources().getString(R.string.clicks_from).toString() + " " + loc.displayCountry
            tV_country_name.text = setCountryName
        }
        val rL_back_btn = findViewById(R.id.rL_back_btn) as RelativeLayout
        rL_back_btn.setOnClickListener(this)
        linear_rootElement = findViewById(R.id.linear_rootElement) as LinearLayout?
        linear_location = findViewById(R.id.linear_location) as LinearLayout?
        //linear_location.setClickable(false);
    }

    override fun onResume() {
        super.onResume()

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.REGISTRATION_COMPLETE))

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.PUSH_NOTIFICATION))

        // clear the notification area when the app is opened
        clearNotifications(getApplicationContext())
    }

   override fun onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!)
        super.onPause()
    }

    /**
     * <h>getCountriesTotalClick</h>
     *
     *
     * In this method we used to do api call to get list of cities and its clicks
     * for any particular county.
     *
     */
    private val countriesTotalClick: Unit
        private get() {
            if (mSessionManager!!.isUserLoggedIn) {
                val request_datas = JSONObject()
                try {
                    request_datas.put("token", mSessionManager!!.authToken)
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
                val URL = ApiUrl.INSIGHT + "/" + postId + "/" + countrySname
                OkHttp3Connection.doOkHttp3Connection(TAG, URL, OkHttp3Connection.Request_type.POST, request_datas, object : OkHttp3RequestCallback {
                     override fun onSuccess(result: String?, user_tag: String?) {
                        println("$TAG total click res=$result")
                        val gson = Gson()
                        val listType = object : TypeToken<ArrayList<TotalClicksMainPojo?>?>() {}.type
                        val arrayListTotalClicks = gson.fromJson<ArrayList<TotalClicksMainPojo>>(result, listType)
                        println(TAG + " " + "posts code=" + arrayListTotalClicks[0].code)
                        if (arrayListTotalClicks.size > 0) {
                            val arrayListClicksData = arrayListTotalClicks[0].data
                            if (arrayListClicksData != null && arrayListClicksData.size > 0) {
                                inflateLocationInsight(arrayListClicksData)
                            }
                        }
                    }

                     override fun onError(error: String?, user_tag: String?) {
                        CommonClass.showSnackbarMessage(linear_rootElement, error)
                    }
                })
            } else CommonClass.showSnackbarMessage(linear_rootElement, getResources().getString(R.string.NoInternetAccess))
        }

    /**
     * <h>InflateLocationInsight</h>
     *
     *
     * In this method we used to inflate the list of city and its total clicks.
     *
     * @param arrayListLocation The list containing the cities and its click
     */
    private fun inflateLocationInsight(arrayListLocation: ArrayList<TotalClicksDatas>) {
        val inflater = this.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        linear_location!!.removeAllViews()
        for (locCount in arrayListLocation.indices) {
            val view = inflater.inflate(R.layout.single_row_insight_location, null)
            val tV_country_name = view.findViewById<View>(R.id.tV_country_name) as TextView
            val tV_totalViews = view.findViewById<View>(R.id.tV_totalViews) as TextView
            val cityName = arrayListLocation[locCount].city
            val totalViews = arrayListLocation[locCount].count
            val iV_array = view.findViewById<View>(R.id.iV_array) as ImageView
            iV_array.visibility = View.GONE

            // set country name
            if (cityName != null && !cityName.isEmpty()) {
                tV_country_name.text = cityName
            }

            // set total reviews
            if (totalViews != null && !totalViews.isEmpty()) tV_totalViews.text = totalViews

            // click on view
            val finalLocCount = locCount
            /*view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    System.out.println(TAG+" "+"pos="+ finalLocCount);
                    Intent intent=new Intent(mActivity,GetTotalClicksActivity.class);
                    intent.putExtra("postId",postId);
                    intent.putExtra("countrySname",countrySname);
                    startActivity(intent);
                }
            });*/linear_location!!.addView(view)
        }
    }

 override fun onBackPressed() {
        finish()
        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.rL_back_btn -> onBackPressed()
        }
    }

    companion object {
        private val TAG = GetTotalClicksActivity::class.java.simpleName
    }
}