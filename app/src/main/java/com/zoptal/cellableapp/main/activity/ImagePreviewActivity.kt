package com.zoptal.cellableapp.main.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.widget.RelativeLayout
import com.bumptech.glide.Glide
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.utility.TouchImageView

class ImagePreviewActivity : AppCompatActivity() {
    private var imageView: TouchImageView? = null
    private var rL_close: RelativeLayout? = null
    override fun onCreate(savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_image_preview)
        val url: String = getIntent().getStringExtra("url")
        imageView = findViewById(R.id.iV_preview) as TouchImageView
        rL_close = findViewById(R.id.rL_close) as RelativeLayout?
        rL_close!!.setOnClickListener { onBackPressed() }
        Glide.with(this)
                .load(url)
                .placeholder(R.drawable.default_image)
                .error(R.drawable.default_image)
                .into(imageView)
    }

 override fun onBackPressed() {
        super.onBackPressed()
    }
}