package com.zoptal.cellableapp.main.activity

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import android.util.Log
import android.view.Gravity
import android.view.View
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.annotation.Nullable
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.main.activity.PaymentAddAccount
import com.zoptal.cellableapp.utility.VariableConstants

class PaymentAddAccount : AppCompatActivity(), View.OnClickListener {
    private var doubleBackToExitPressedOnce = false
    var mWebView: WebView? = null
    var progress_bar: ProgressBar? = null
     override fun onCreate(@Nullable savedInstanceState: Bundle?) {
        try {
             super.onCreate(savedInstanceState?: Bundle())
            setContentView(R.layout.activity_payment_add_account)
            progress_bar = findViewById(R.id.progress_bar) as ProgressBar?
            // back button
            val rL_back_btn = findViewById(R.id.rL_back_btn) as RelativeLayout
            rL_back_btn.setOnClickListener(this)
            mWebView = findViewById(R.id.wVPaymentDetail)
            val webSettings = mWebView!!.settings
            //        webSettings?.domStorageEnabled = true
            webSettings.javaScriptEnabled = true
            webSettings.javaScriptCanOpenWindowsAutomatically = true
            mWebView!!.webViewClient = object : WebViewClient() {
                //If you will not use this method url links are opeen in new brower not in webview
                override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                    view.loadUrl(url)
                    Log.e("=====", "$url===")
                    if (url.contains("failure")) {
                        if (getIntent().getIntExtra("from", 0) === VariableConstants.STRIPE_ACCOUNT_ADDED_HOME) {
                            val intent = Intent()
                            setResult(getIntent().getIntExtra("from", 0), intent)
                            finish()
                        }
                        Toast.makeText(this@PaymentAddAccount, getResources().getString(R.string.stripe_link_unsuccessfull), Toast.LENGTH_SHORT).show()
                    } else if (url.contains("success")) {
                        if (getIntent().getIntExtra("from", 0) === VariableConstants.STRIPE_ACCOUNT_ADDED_HOME) {
                            finish()
                        } else {
                            val intent = Intent()
                            setResult(getIntent().getIntExtra("from", 0), intent)
                            finish()
                        }
                        overridePendingTransition(R.anim.stay, R.anim.slide_down)
                        Toast.makeText(this@PaymentAddAccount, getResources().getString(R.string.stripe_link_successfull), Toast.LENGTH_SHORT).show()
                    }
                    return true
                }

                override fun onPageStarted(view: WebView, url: String, favicon: Bitmap?) {
                    super.onPageStarted(view, url, favicon)
                }
            }
            mWebView!!.setWebChromeClient(object : WebChromeClient() {
                override fun onProgressChanged(view: WebView, progress: Int) {


                    // Return the app name after finish loading
                    if (progress >= 90) progress_bar!!.visibility = View.GONE
                }
            })
            mWebView!!.loadUrl(getIntent().getStringExtra("weburl"))
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.rL_back_btn -> onBackPressed()
        }
    }

 override fun onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            if (getIntent().getIntExtra("from", 0) === VariableConstants.STRIPE_ACCOUNT_ADDED_HOME) {
                val intent = Intent()
                setResult(getIntent().getIntExtra("from", 0), intent)
                finish()
            } else {
                super.onBackPressed()
            }
            return
        }
        doubleBackToExitPressedOnce = true
        val toast: Toast = Toast.makeText(this, "Cancel Transaction? Please press again to exit.", Toast.LENGTH_LONG)
        toast.setGravity(Gravity.CENTER, 0, 0)
        toast.show()
        Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 2000)
    }

    companion object {
        @JvmStatic
        fun callActivity(context: Context, weburl: String?, from: Int) {
            val intent = Intent(context, PaymentAddAccount::class.java)
            intent.putExtra("weburl", weburl)
            intent.putExtra("from", from) //101 means from profile and 102 means from home
            (context as Activity).startActivityForResult(intent, from)
        }
    }
}