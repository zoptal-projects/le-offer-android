package com.zoptal.cellableapp.main.view_type_activity

import android.app.Activity
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.adapter.view_type_adapter.AddDetailAdap
import com.zoptal.cellableapp.fcm_push_notification.Config
import com.zoptal.cellableapp.fcm_push_notification.NotificationMessageDialog
import com.zoptal.cellableapp.fcm_push_notification.NotificationUtils.Companion.clearNotifications
import com.zoptal.cellableapp.pojo_class.product_category.FilterData
import com.zoptal.cellableapp.pojo_class.product_category.FilterKeyValue
import com.zoptal.cellableapp.utility.ClickListener
import com.zoptal.cellableapp.utility.CommonClass
import com.zoptal.cellableapp.utility.VariableConstants
import java.util.*

class AddDetailActivity : AppCompatActivity(), ClickListener, View.OnClickListener {
    private var mNotificationMessageDialog: NotificationMessageDialog? = null
    private var rV_fieldList: RecyclerView? = null
    private var filterData: ArrayList<FilterData>? = null
    private var addDetailAdap: AddDetailAdap? = null
    private var mActivity: Activity? = null
    private var rL_done: RelativeLayout? = null
    private var root_addDetail: RelativeLayout? = null
    private var filterKeyValues: ArrayList<FilterKeyValue>? = null
    private var title: String? = null
    private var tV_title: TextView? = null

    // isForFilter used while view type is open from filter screen
    private var isForFilter = false
    private var selectedFileds: ArrayList<FilterKeyValue>? = null
    protected override fun onCreate(savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_add_detail)
        mActivity = this@AddDetailActivity
        mNotificationMessageDialog = NotificationMessageDialog(mActivity)
        filterKeyValues = ArrayList()
        filterData = getIntent().getSerializableExtra("filterData") as ArrayList<FilterData>?
        title = getIntent().getStringExtra("title")
        isForFilter = getIntent().getBooleanExtra("isForFilter", false)
        if (getIntent() != null) {
            selectedFileds = getIntent().getSerializableExtra("selectedField") as ArrayList<FilterKeyValue>?
            if (selectedFileds != null && selectedFileds!!.size > 0) filterKeyValues = selectedFileds
        }


        //set again all values which is filled by user
        if (selectedFileds != null && selectedFileds!!.size > 0) {
            for (i in filterData!!.indices) {
                for (f in selectedFileds!!) {
                    if (f.key.equals(filterData!![i].fieldName, ignoreCase = true)) {
                        if (filterData!![i].values!!.isEmpty()) filterData!![i].isSelected = false else filterData!![i].isSelected = true
                        filterData!![i].selectedValues = f.value
                    }
                }
            }
        }
        tV_title = findViewById(R.id.tV_title) as TextView?
        tV_title!!.text = title
        rV_fieldList = findViewById(R.id.rV_fieldList) as RecyclerView?
        rV_fieldList?.setLayoutManager(LinearLayoutManager(this))
        addDetailAdap = AddDetailAdap(mActivity!!, filterData!!)
        rV_fieldList?.setAdapter(addDetailAdap)
        rL_done = findViewById(R.id.rL_done) as RelativeLayout?
        rL_done!!.setOnClickListener(this)
        root_addDetail = findViewById(R.id.root_addDetail) as RelativeLayout?
        val rL_back_btn = findViewById(R.id.rL_back_btn) as RelativeLayout
        rL_back_btn.setOnClickListener(this)
    }

    override fun onItemClick(view: View?, position: Int) {
        val filter = filterData!![position]
        val intent: Intent
        when (filter.type) {
            1 -> {
                intent = Intent(mActivity, EditTextTypeActivity::class.java)
                intent.putExtra("title", filter.fieldName)
                intent.putExtra("fieldName", filter.fieldName)
                intent.putExtra("selectedValue", filter.selectedValues)
                intent.putExtra("position", position)
                intent.putExtra("isNumber", "0")
                startActivityForResult(intent, VariableConstants.SELECT_VALUE_REQ_CODE)
            }
            2 -> {
                intent = Intent(mActivity, MultipleSelectionActivity::class.java)
                intent.putExtra("values", filter.values)
                intent.putExtra("title", filter.fieldName)
                intent.putExtra("fieldName", filter.fieldName)
                intent.putExtra("position", position)
                startActivityForResult(intent, VariableConstants.SELECT_VALUE_REQ_CODE)
            }
            3 -> {
                intent = Intent(mActivity, SeekBarViewActivity::class.java)
                intent.putExtra("values", filter.values)
                intent.putExtra("title", filter.fieldName)
                intent.putExtra("fieldName", filter.fieldName)
                intent.putExtra("position", position)
                startActivityForResult(intent, VariableConstants.SELECT_VALUE_REQ_CODE)
            }
            4 -> {
                intent = Intent(mActivity, SingleSelectionActivity::class.java)
                intent.putExtra("selectedValue", filter.selectedValues)
                intent.putExtra("values", filter.values)
                intent.putExtra("title", filter.fieldName)
                intent.putExtra("fieldName", filter.fieldName)
                intent.putExtra("position", position)
                startActivityForResult(intent, VariableConstants.SELECT_VALUE_REQ_CODE)
            }
            5 -> {
                intent = Intent(mActivity, EditTextTypeActivity::class.java)
                intent.putExtra("title", filter.fieldName)
                intent.putExtra("fieldName", filter.fieldName)
                intent.putExtra("selectedValue", filter.selectedValues)
                intent.putExtra("position", position)
                intent.putExtra("isNumber", "1")
                startActivityForResult(intent, VariableConstants.SELECT_VALUE_REQ_CODE)
            }
            6 -> {
                intent = Intent(mActivity, SingleSelectionActivity::class.java)
                intent.putExtra("selectedValue", filter.selectedValues)
                intent.putExtra("values", filter.values)
                intent.putExtra("title", filter.fieldName)
                intent.putExtra("fieldName", filter.fieldName)
                intent.putExtra("position", position)
                startActivityForResult(intent, VariableConstants.SELECT_VALUE_REQ_CODE)
            }
            7 -> {
                intent = Intent(mActivity, DatePickerActivity::class.java)
                intent.putExtra("title", filter.fieldName)
                intent.putExtra("fieldName", filter.fieldName)
                intent.putExtra("selectedValue", filter.selectedValues)
                intent.putExtra("position", position)
                intent.putExtra("isNumber", "0")
                startActivityForResult(intent, VariableConstants.SELECT_VALUE_REQ_CODE)
            }
        }
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.rL_back_btn -> onBackPressed()
            R.id.rL_done -> if (isMendatoryFill) {
                val intent = Intent()
                intent.putExtra("filterKeyValues", filterKeyValues)
                setResult(VariableConstants.ADD_DETAIL_DATA_REQ_CODE, intent)
                onBackPressed()
            }
        }
    }

    val isMendatoryFill: Boolean
        get() {
            for (fData in filterData!!) {
                if (fData.isMandatory) {
                    if (!fData.isSelected) {
                        CommonClass.showSnackbarMessage(root_addDetail, fData.fieldName + " is not selected.")
                        return false
                    }
                }
            }
            return true
        }

   override  protected fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (data != null) {
            when (resultCode) {
                VariableConstants.SELECT_VALUE_REQ_CODE -> {
                    val value = data.getStringExtra("selectedValue")
                    val pos = data.getIntExtra("position", -1)
                    if (filterData!![pos].values!!.isEmpty()) filterData!![pos].isSelected = false else filterData!![pos].isSelected = true
                    filterData!![pos].selectedValues = value
                    if (filterData!![pos].type == 5 || filterData!![pos].type == 3) {
                        // user change the value then first remove and add.
                        var i = 0
                        while (i < filterKeyValues!!.size) {
                            if (filterKeyValues!![i].key.equals(filterData!![pos].fieldName, ignoreCase = true)) filterKeyValues!!.removeAt(i)
                            i++
                        }
                        filterKeyValues!!.add(FilterKeyValue("1", filterData!![pos].fieldName!!, value))
                    } else {
                        // user change the value then first remove and add.
                        var i = 0
                        while (i < filterKeyValues!!.size) {
                            if (filterKeyValues!![i].key.equals(filterData!![pos].fieldName, ignoreCase = true)) filterKeyValues!!.removeAt(i)
                            i++
                        }
                        filterKeyValues!!.add(FilterKeyValue(filterData!![pos].fieldName!!, value))
                    }
                    addDetailAdap?.notifyItemChanged(pos)
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.REGISTRATION_COMPLETE))

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.PUSH_NOTIFICATION))

        // clear the notification area when the app is opened
        clearNotifications(getApplicationContext())
    }

   override fun onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!)
        super.onPause()
    }
}