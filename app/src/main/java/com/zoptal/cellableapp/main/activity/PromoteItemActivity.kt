package com.zoptal.cellableapp.main.activity

import android.app.Activity
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.View
import android.widget.*
import androidx.annotation.Nullable
import com.squareup.picasso.Picasso
import com.suresh.innapp_purches.InAppConstants
import com.suresh.innapp_purches.InAppConstants.Purchase_item
import com.suresh.innapp_purches.Inn_App_billing.BillingProcessor
import com.suresh.innapp_purches.Inn_App_billing.BillingProcessor.IBillingHandler
import com.suresh.innapp_purches.Inn_App_billing.SkuDetails
import com.suresh.innapp_purches.Inn_App_billing.TransactionDetails
import com.zoptal.cellableapp.BusEventManager.FutureUpdated
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.adapter.PromoteItemRvAdapter
import com.zoptal.cellableapp.aleret.CircleProgressDialog.Companion.instance
import com.zoptal.cellableapp.main.activity.HomePageActivity
import com.zoptal.cellableapp.main.activity.PromoteItemActivity
import com.zoptal.cellableapp.mqttchat.AppController
import com.zoptal.cellableapp.pojo_class.promote_item_pojo.PromoteItemData
import com.zoptal.cellableapp.utility.*
import com.zoptal.cellableapp.utility.OkHttp3Connection.OkHttp3RequestCallback
import org.json.JSONException
import org.json.JSONObject
import java.util.*

/**
 * <h>PromoteItemActivity</h>
 *
 *
 * In this class we used to show the list of promote item.
 *
 * @since 30-Aug-17
 */
class PromoteItemActivity : AppCompatActivity(), View.OnClickListener, IBillingHandler {
    private var mActivity: Activity? = null
    private var mSessionManager: SessionManager? = null
    private var rL_rootElement: RelativeLayout? = null
    private var mProgressBar: ProgressBar? = null
    private var rV_promotePlan: RecyclerView? = null
    private var progressDialog: ProgressDialog? = null
    private var mpurchase_item: Purchase_item? = null
    private var arrayListPromoteItem: ArrayList<PromoteItemData>? = null
    private var circleDialog: Dialog? = null
    private var postId: String? = null
    private var uniqueView: Int? = 0
    private var title: String? = null
    private var isPromoted = 0
    private var rl_apply: RelativeLayout? = null
     override fun onCreate(@Nullable savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_promote_item)
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale)
        initVariables()
    }

    private fun initVariables() {
        circleDialog = instance!!.get_Circle_Progress_bar(this)
        progressDialog = ProgressDialog(this)
        progressDialog!!.setMessage("Wait..")
        // receiving datas from last class
        val price: String
        val productName: String
        val productImage: String
        val intent: Intent = getIntent()
        price = intent.getStringExtra("price")
        productName = intent.getStringExtra("productName")
        productImage = intent.getStringExtra("productImage")
        postId = intent.getStringExtra("productId")
        isPromoted = intent.getIntExtra("isPromoted", 0)
        mActivity = this@PromoteItemActivity
        mProgressBar = findViewById(R.id.progress_bar) as ProgressBar?
        mSessionManager = SessionManager(mActivity!!)
        rL_rootElement = findViewById(R.id.rL_rootElement) as RelativeLayout?
        rV_promotePlan = findViewById(R.id.rV_promotePlan) as RecyclerView?
        rl_apply = findViewById(R.id.rL_apply)
        rl_apply!!.setOnClickListener(this)
        if (isPromoted == 1) rl_apply!!.visibility = View.GONE else rl_apply!!.visibility = View.VISIBLE
        bp = BillingProcessor(this, InAppConstants.base64EncodedPublicKey, mSessionManager!!.userId, this)

        // set product desc like image, name and price
        val iV_productImage = findViewById(R.id.iV_productImage) as ImageView
        if (productImage != null && !productImage.isEmpty()) Picasso.with(mActivity)
                .load(productImage)
                .placeholder(R.drawable.default_image)
                .error(R.drawable.default_image)
                .into(iV_productImage)

        // set price
        val tV_productprice = findViewById(R.id.tV_productprice) as TextView
        if (price != null && !price.isEmpty()) tV_productprice.text = "$"+price

        // set name
        val tV_productName = findViewById(R.id.tV_productName) as TextView
        if (productName != null && !productName.isEmpty()) tV_productName.text = productName

        // Back button
        val rL_back_btn = findViewById(R.id.rL_back_btn) as RelativeLayout
        rL_back_btn.setOnClickListener(this)
        promotionDetail()
        // promotionPlansApi();
    }

    fun promotionDetail() {
        val controller = getApplicationContext() as AppController
        val promotionItemList = controller.inappItemList
        if (promotionItemList == null) {
            controller.collectIN_AppDetails(object : SukoCallBack {
                override fun onSucess(data: List<SkuDetails?>?) {
                    updateUi(data!!)
                }

                override fun onError(message: String?) {}
            })
        } else {
            updateUi(promotionItemList)
        }
    }

    private fun updateUi(data: List<SkuDetails?>) {
        arrayListPromoteItem = ArrayList()
        for (temp_data in data) {
            val temp = PromoteItemData()
            temp.planId = temp_data!!.productId!!
            temp.price = "" + temp_data!!.priceText
            temp.name = temp_data.title
            temp.uniqueViews = temp_data.description
            arrayListPromoteItem!!.add(temp)
        }
        if (arrayListPromoteItem != null && arrayListPromoteItem!!.size > 0) {
            val promoteItemRvAdapter = PromoteItemRvAdapter(mActivity!!, arrayListPromoteItem!!)
            val linearLayoutManager = LinearLayoutManager(mActivity)
            rV_promotePlan!!.setLayoutManager(linearLayoutManager)
            rV_promotePlan!!.setAdapter(promoteItemRvAdapter)
        }
    }

 override fun onBackPressed() {
        finish()
        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.rL_back_btn -> onBackPressed()
            R.id.rL_apply ->{
//                buy_Item(Purchase_item.getPurchaseItem("times_more_buyers_100"))}
                collectSeletedItem()}
        }
    }

    /*
     *buying the data details. */
    private fun buy_Item(purchase_item: Purchase_item) {
        mpurchase_item = purchase_item
        progressDialog!!.show()
        bp = BillingProcessor(this, InAppConstants.base64EncodedPublicKey, mSessionManager!!.userId, this)
    }

   override  protected fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (!(bp != null && bp!!.handleActivityResult(requestCode, resultCode, data))) {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun onProductPurchased(productId: String?, details: TransactionDetails?) {
        Log.d("Item_purchesed", "String postId$productId $details")
        if (bp!!.consumePurchase(productId!!)) {
            purchase_Item(productId)
            bp!!.release()
        }
    }

    override fun onPurchaseHistoryRestored() {
        bp!!.release()
    }

    override fun onBillingError(errorCode: Int, error: Throwable?) {
error?.printStackTrace()
        Toast.makeText(this, R.string.purchaset_error, Toast.LENGTH_SHORT).show()
        /*
         * Releasing the service.*/bp!!.release()
    }

    override fun onBillingInitialized() {
        if (progressDialog != null) {
            progressDialog!!.dismiss()
        }
        if (mpurchase_item != null) {
            /*
            *For testing purpose only.*/
            bp!!.purchase(this, mpurchase_item!!.key)
        }
    }

    /*
     * current selected item*/
    private fun collectSeletedItem() {
        var planId = ""
        for (data in arrayListPromoteItem!!) {
            if (data.isItemSelected) {
                planId = data.planId


                uniqueView = if (data.planId.equals("android.test.purchased", ignoreCase = true)) 100 else data.uniqueViews.toInt()
                title = data.name
                //Toast.makeText(mActivity,uniqueView,Toast.LENGTH_SHORT).show();
                break
            }
        }
        if (planId.isEmpty()) {
            Toast.makeText(this, R.string.Error_on_plandetaisl, Toast.LENGTH_SHORT).show()
            return
        }
        /*
         *buying the item. */buy_Item(Purchase_item.getPurchaseItem(planId))
//        buy_Item(Purchase_item.getPurchaseItem("times_more_buyers_100"))
    }

    /*
     *Purchase item.*/
    private fun purchase_Item(playItemId: String) {
        val item = Purchase_item.getPlanId(playItemId)
        if (item == null) {
            Toast.makeText(this, R.string.Stored_item_error, Toast.LENGTH_SHORT).show()
            return
        }
        val request_datas = JSONObject()
        try {
            request_datas.put("token", mSessionManager!!.authToken)
            request_datas.put("postId", postId)
            request_datas.put("postType", "0")
            request_datas.put("purchaseId", item.id)
            request_datas.put("noOfViews", uniqueView)
            request_datas.put("promotionTitle", title)//"title")
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            circleDialog!!.show()
            val promotionPlansUrl = ApiUrl.PURCHASED_PLAN //+"/"+item.getId()+"/"+ postId;
            OkHttp3Connection.doOkHttp3Connection(TAG, promotionPlansUrl, OkHttp3Connection.Request_type.POST, request_datas, object : OkHttp3RequestCallback {
                 override fun onSuccess(result: String?, user_tag: String?) {
                    if (circleDialog != null) {
                        circleDialog!!.dismiss()
                    }
                    try {
                        parseResult(result)
                    } catch (e: Exception) {
                        e.printStackTrace()
                        CommonClass.showSnackbarMessage(rL_rootElement, getString(R.string.parse_exception_text))
                    }
                }

                 override fun onError(error: String?, user_tag: String?) {
                    if (circleDialog != null) {
                        circleDialog!!.dismiss()
                    }
                    CommonClass.showSnackbarMessage(rL_rootElement, error)
                }
            })
        } else {
            CommonClass.showSnackbarMessage(rL_rootElement, getResources().getString(R.string.NoInternetAccess))
        }
    }

    /*
     *Parsing the result data.*/
    @Throws(JSONException::class)
    fun parseResult(response: String?) {
        val jsonObject = JSONObject(response)
        val code = jsonObject.getString("code")
        if (code == "200") {
            isPromoted = 1
            AppController.bus.post(FutureUpdated(postId!!, true))
            val intent = Intent(mActivity, HomePageActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            mActivity!!.startActivity(intent)
            //PromoteItemActivity.this.finish();
        } else {
            val message = jsonObject.getString("message")
            CommonClass.showSnackbarMessage(rL_rootElement, message)
        }
    }

    companion object {
        private val TAG = PromoteItemActivity::class.java.simpleName
        private var bp: BillingProcessor? = null
    }
}