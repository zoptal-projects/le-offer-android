package com.zoptal.cellableapp.main.activity

import android.app.Activity
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.widget.RelativeLayout
import androidx.annotation.Nullable
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.adapter.SelectBuyerRvAdap
import com.zoptal.cellableapp.fcm_push_notification.Config
import com.zoptal.cellableapp.fcm_push_notification.NotificationMessageDialog
import com.zoptal.cellableapp.fcm_push_notification.NotificationUtils.Companion.clearNotifications
import com.zoptal.cellableapp.main.activity.RateUserActivity
import com.zoptal.cellableapp.pojo_class.accepted_offer.AcceptedOfferDatas
import com.zoptal.cellableapp.utility.ClickListener
import com.zoptal.cellableapp.utility.VariableConstants
import java.util.*

/**
 * <h>SelectBuyerActivity</h>
 *
 *
 * This class is called from EditProductActivity class. In this class we get all
 * accepted offer from last class and show in this.
 *
 *
 * @since 13-Jul-17
 */
class SelectBuyerActivity : AppCompatActivity(), View.OnClickListener {
    private var mNotificationMessageDialog: NotificationMessageDialog? = null
    var postId = ""
    var rL_rootElement: RelativeLayout? = null
     override fun onCreate(@Nullable savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_select_buyer)
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale)
        initVariables()
    }

    private fun initVariables() {
        val mActivity: Activity = this@SelectBuyerActivity
        mNotificationMessageDialog = NotificationMessageDialog(mActivity)

        // accept datas from last class
        val intent: Intent = getIntent()
        val arrayListAcceptedOffer = intent.getSerializableExtra("acceptedOffer") as ArrayList<AcceptedOfferDatas>
        postId = intent.getStringExtra("postId")

        // Accepted offer recycler view adapter
        val rV_acceptedOffer: RecyclerView = findViewById(R.id.rV_acceptedOffer) as RecyclerView
        rL_rootElement = findViewById(R.id.rL_rootElement) as RelativeLayout?
        val selectBuyerRvAdap = SelectBuyerRvAdap(mActivity, arrayListAcceptedOffer)
        val mLinearLayoutManager = LinearLayoutManager(mActivity)
        rV_acceptedOffer.setLayoutManager(mLinearLayoutManager)
        rV_acceptedOffer.setAdapter(selectBuyerRvAdap)

        // item click
        selectBuyerRvAdap.setItemClick(object : ClickListener {
            override fun onItemClick(view: View?, position: Int) {
                val intent = Intent(mActivity, RateUserActivity::class.java)
                intent.putExtra("userName", arrayListAcceptedOffer[position].buyername)
                intent.putExtra("userImage", arrayListAcceptedOffer[position].buyerProfilePicUrl)
                intent.putExtra("postId", arrayListAcceptedOffer[position].postId)
                startActivityForResult(intent, VariableConstants.RATE_USER_REQ_CODE)
            }
        })
        val rL_back_btn = findViewById(R.id.rL_back_btn) as RelativeLayout
        rL_back_btn.setOnClickListener(this)
    }

    override fun onResume() {
        super.onResume()

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.REGISTRATION_COMPLETE))

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.PUSH_NOTIFICATION))

        // clear the notification area when the app is opened
        clearNotifications(getApplicationContext())
    }

   override fun onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!)
        super.onPause()
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.rL_back_btn -> onBackPressed()
        }
    }

 override fun onBackPressed() {
        finish()
        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate)
    }

   override  protected fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (data != null) when (requestCode) {
            VariableConstants.RATE_USER_REQ_CODE -> {
                val isToFinishSelectBuyer = data.getBooleanExtra("isToFinishSelectBuyer", false)
                if (isToFinishSelectBuyer) {
                    val intent = Intent()
                    intent.putExtra("isToFinishEditPost", true)
                    intent.putExtra("isToSellItAgain", true)
                    setResult(VariableConstants.SELLING_REQ_CODE, intent)
                    finish()
                }
            }
        }
    }
}