package com.zoptal.cellableapp.main.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle

import android.util.Log
import android.view.View
import android.widget.*
import androidx.annotation.Nullable
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.main.activity.AddAddressActivity
import com.zoptal.cellableapp.pojo_class.address.AddressMainPojo
import com.zoptal.cellableapp.utility.ApiUrl
import com.zoptal.cellableapp.utility.CommonClass
import com.zoptal.cellableapp.utility.OkHttp3Connection
import com.zoptal.cellableapp.utility.OkHttp3Connection.OkHttp3RequestCallback
import com.zoptal.cellableapp.utility.SessionManager
import org.json.JSONException
import org.json.JSONObject

class AddAddressActivity : AppCompatActivity(), View.OnClickListener {
    var et_address_line_one: EditText? = null
    var et_address_line_two: EditText? = null
    var et_state: EditText? = null
    var et_city: EditText? = null
    var et_country: EditText? = null
    var et_pin_code: EditText? = null
    var tv_save: TextView? = null
    var rL_rootview: LinearLayout? = null
    var progress_bar_profile: ProgressBar? = null
    private var mSessionManager: SessionManager? = null
    var id = ""
    override fun onCreate(@Nullable savedInstanceState: Bundle?) {
        try {
             super.onCreate(savedInstanceState?: Bundle())
            setContentView(R.layout.activity_address)
            // back button
            val rL_back_btn = findViewById(R.id.rL_back_btn) as RelativeLayout
            rL_back_btn.setOnClickListener(this)
            mSessionManager = SessionManager(this)
            progress_bar_profile = findViewById(R.id.progress_bar_profile)
            et_address_line_one = findViewById(R.id.et_address_line_one) as EditText?
            et_address_line_two = findViewById(R.id.et_address_line_two) as EditText?
            et_state = findViewById(R.id.et_state) as EditText?
            et_city = findViewById(R.id.et_city) as EditText?
            et_country = findViewById(R.id.et_country) as EditText?
            et_pin_code = findViewById(R.id.et_pin_code) as EditText?
            rL_rootview = findViewById(R.id.rL_rootview) as LinearLayout?
            tv_save = findViewById(R.id.tv_save) as TextView?
            tv_save!!.setOnClickListener(this)
            if (getIntent().getIntExtra("screen_type", 0) === 2) {
                tv_save!!.setText(getResources().getString(R.string.hint_deliver_to_address))
            } else {
                tv_save!!.setText(getResources().getString(R.string.submit))
            }
            addressApi
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onClick(v: View) {
        try {
            when (v.id) {
                R.id.rL_back_btn -> onBackPressed()
                R.id.tv_save -> {
                    Log.e("=====", "hiii")
                    if (validations()) {
                        if (getIntent().getIntExtra("screen_type", 0) === 1) {
                            addAddressApi()
                        } else if (getIntent().getIntExtra("screen_type", 0) === 2) {
                            GetAllCardsActivity.callActivity(this@AddAddressActivity, 2, et_address_line_one!!.text.toString().trim { it <= ' ' },
                                    et_address_line_two!!.text.toString().trim { it <= ' ' }, et_state!!.text.toString().trim { it <= ' ' }, et_city!!.text.toString().trim { it <= ' ' },
                                    et_country!!.text.toString().trim { it <= ' ' }, et_pin_code!!.text.toString().trim { it <= ' ' },
                                    getIntent().getStringExtra("postId"), getIntent().getStringExtra("currency"),
                                    getIntent().getStringExtra("productSelPrice"), getIntent().getStringExtra("postType"))
                        }
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * <h>UpdateProfileApi</h>
     *
     *
     * In this method we used to send the modified datas to the server.
     *
     */
    private fun addAddressApi() {
        progress_bar_profile!!.visibility = View.VISIBLE
        var url = ApiUrl.ADD_ADDRESS
        if (CommonClass.isNetworkAvailable(this)) {
            val request_datas = JSONObject()
            try {
                request_datas.put("token", mSessionManager!!.authToken)
                request_datas.put("full_name", "")
                request_datas.put("mobile_num", "")
                request_datas.put("pin_code", et_pin_code!!.text.toString())
                request_datas.put("address_one", et_address_line_one!!.text.toString())
                request_datas.put("address_two", et_address_line_two!!.text.toString())
                request_datas.put("landmark", "")
                request_datas.put("country", et_country!!.text.toString())
                request_datas.put("state", et_state!!.text.toString())
                request_datas.put("town", et_city!!.text.toString())
                if (id != "") {
                    request_datas.put("address_id", id)
                    url = ApiUrl.UPDATE_ADDRESS
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            OkHttp3Connection.doOkHttp3Connection(TAG, url, OkHttp3Connection.Request_type.POST, request_datas, object : OkHttp3RequestCallback {
                 override fun onSuccess(result: String?, user_tag: String?) {
                    println("$TAG update profile res=$result")
                    progress_bar_profile!!.visibility = View.GONE
                    val editProfilePojo: AddressMainPojo
                    val gson = Gson()
                    editProfilePojo = gson.fromJson(result, AddressMainPojo::class.java)
                    CommonClass.showToast(this@AddAddressActivity, editProfilePojo.message)
                    when (editProfilePojo.code) {
                        "200" -> finish()
                        "401" -> CommonClass.sessionExpired(this@AddAddressActivity)
                    }
                }

                 override fun onError(error: String?, user_tag: String?) {
                    progress_bar_profile!!.visibility = View.GONE
                    CommonClass.showSnackbarMessage(rL_rootview, error)
                }
            })
        } else CommonClass.showSnackbarMessage(rL_rootview, getResources().getString(R.string.NoInternetAccess))
    }

    /**
     * <h>UpdateProfileApi</h>
     *
     *
     * In this method we used to send the modified datas to the server.
     *
     */
    private val addressApi: Unit
        private get() {
            progress_bar_profile!!.visibility = View.VISIBLE
            if (CommonClass.isNetworkAvailable(this)) {
                val request_datas = JSONObject()
                try {
                    request_datas.put("token", mSessionManager!!.authToken)
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
                OkHttp3Connection.doOkHttp3Connection(TAG, ApiUrl.GET_ADDRESS, OkHttp3Connection.Request_type.POST, request_datas, object : OkHttp3RequestCallback {
                     override fun onSuccess(result: String?, user_tag: String?) {
                        try {
                            println("$TAG update profile res=$result")
                            progress_bar_profile!!.visibility = View.GONE
                            val editProfilePojo: AddressMainPojo
                            val gson = Gson()
                            editProfilePojo = gson.fromJson(result, AddressMainPojo::class.java)
                            when (editProfilePojo.code) {
                                "200" -> {
                                    id = editProfilePojo.data!!._id!!
                                    et_address_line_one!!.setText(editProfilePojo.data!!.address_one)
                                    et_address_line_two!!.setText(editProfilePojo.data!!.address_two)
                                    et_country!!.setText(editProfilePojo.data!!.country)
                                    et_state!!.setText(editProfilePojo.data!!.state)
                                    et_city!!.setText(editProfilePojo.data!!.town)
                                    et_pin_code!!.setText(editProfilePojo.data!!.pin_code)
                                }
                                "401" -> CommonClass.sessionExpired(this@AddAddressActivity)
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                     override fun onError(error: String?, user_tag: String?) {
                        progress_bar_profile!!.visibility = View.GONE
                        CommonClass.showSnackbarMessage(rL_rootview, error)
                    }
                })
            } else CommonClass.showSnackbarMessage(rL_rootview, getResources().getString(R.string.NoInternetAccess))
        }

    private fun validations(): Boolean {
        try {
            if (checkEditTextEmpty(et_address_line_one, getResources().getString(R.string.err_address_line_one))) {
                return false
            }
            if (checkEditTextEmpty(et_address_line_two, getResources().getString(R.string.err_address_line_two))) {
                return false
            }
            if (checkEditTextEmpty(et_country, getResources().getString(R.string.err_country))) {
                return false
            }
            if (checkEditTextEmpty(et_state, getResources().getString(R.string.err_state))) {
                return false
            }
            if (checkEditTextEmpty(et_city, getResources().getString(R.string.err_city))) {
                return false
            }
            if (checkEditTextEmpty(et_pin_code, getResources().getString(R.string.err_pin_code))) {
                return false
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return true
    }

    private fun checkEditTextEmpty(et_address_line_one: EditText?, msg: String): Boolean {
        try {
            if (isNullOrEmpty(et_address_line_one)) {
                Toast.makeText(this@AddAddressActivity, msg, Toast.LENGTH_SHORT).show()
                et_address_line_one!!.requestFocus()
                return true
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return false
    }

    private fun isNullOrEmpty(editText: EditText?): Boolean {
        return editText!!.text == null || editText.text.toString().trim { it <= ' ' }.length == 0
    }

    companion object {
        private val TAG = AddAddressActivity::class.java.simpleName
        @JvmStatic
        fun callActivity(mContext: Context, screen_type: Int, postId: String?, currency: String?,
                         productSelPrice: String?, postType: String?) {
            val intent = Intent(mContext, AddAddressActivity::class.java)
            intent.putExtra("screen_type", screen_type) //1 means profile // 2means chat
            intent.putExtra("postId", postId)
            intent.putExtra("currency", currency)
            intent.putExtra("productSelPrice", productSelPrice)
            intent.putExtra("postType", postType)
            mContext.startActivity(intent)
        }
    }
}