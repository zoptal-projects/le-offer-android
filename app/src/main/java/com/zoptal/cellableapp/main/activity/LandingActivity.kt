package com.zoptal.cellableapp.main.activity

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import androidx.appcompat.app.AppCompatActivity
import com.facebook.login.LoginManager
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.auth.api.signin.GoogleSignInResult
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import com.zoptal.cellableapp.BuildConfig
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.get_current_location.FusedLocationReceiver
import com.zoptal.cellableapp.get_current_location.FusedLocationService
import com.zoptal.cellableapp.main.LoginWithFacebook
import com.zoptal.cellableapp.main.activity.LandingActivity
import com.zoptal.cellableapp.mqttchat.AppController
import com.zoptal.cellableapp.pojo_class.LogDevicePojo
import com.zoptal.cellableapp.pojo_class.LoginResponsePojo
import com.zoptal.cellableapp.utility.*
import com.zoptal.cellableapp.utility.OkHttp3Connection.OkHttp3RequestCallback
import org.json.JSONException
import org.json.JSONObject
import java.util.*

/**
 * <h>LandingActivity</h>
 *
 *
 * This class is called when user is not logged in. In this screen
 * we can do login with facebook or google. And we have option for
 * login or signup.
 *
 *
 * @since 13-May-17
 */
class LandingActivity : AppCompatActivity(), View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {
    private var mActivity: Activity? = null
    private var loginWithFacebook: LoginWithFacebook? = null
    private var mGoogleApiClient: GoogleApiClient? = null
    private var rL_rootview: RelativeLayout? = null
    private var mSessionManager: SessionManager? = null
    private var iV_google_icon: ImageView? = null
    private var tV_googleLogin: TextView? = null
    private var pBar_googleLogin: ProgressBar? = null
    private var locationService: FusedLocationService? = null
    private var currentLat: String? = ""
    private var currentLng: String? = ""
    private var address: String? = ""
    private var city: String? = ""
    private var countryCode: String? = ""
    private var fullName: String? = ""
    private var email: String? = ""
    private var id: String? = ""
    private var serverAuthCode: String? = ""
    private var personPhotoUrl: String? = ""
    private var runTimePermission: RunTimePermission? = null
    private var permissionsArray: Array<String>? =null
    private var isFromGplusLocation = false
    private var isToStartActivity = false
    private var fromLandingAccept = false
   override  fun onCreate(@Nullable savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_landing)
        overridePendingTransition(R.anim.slide_up, R.anim.stay)
        initVariables()
    }

    /**
     * In this method we used to initialize the data member and xml variables.
     */
    private fun initVariables() {
        mActivity = this@LandingActivity
        mSessionManager = SessionManager(mActivity!!)
        isToStartActivity = true
        currentLat = mSessionManager!!.currentLat
        currentLng = mSessionManager!!.currentLng
        if (isLocationFound(currentLat, currentLng)) {
            address = CommonClass.getCompleteAddressString(mActivity, currentLat!!.toDouble(), currentLng!!.toDouble())
            city = CommonClass.getCityName(mActivity!!, currentLat!!.toDouble(), currentLng!!.toDouble())
            countryCode = CommonClass.getCountryCode(mActivity!!, currentLat!!.toDouble(), currentLng!!.toDouble())
        }

        //Getting registration token
        if (mSessionManager!!.pushToken == null || mSessionManager!!.pushToken!!.isEmpty()) {
            val refreshedToken = FirebaseInstanceId.getInstance().token
            //Displaying token on logcat
            println("$TAG My Refreshed token: $refreshedToken")
            if (refreshedToken != null && !refreshedToken.isEmpty()) mSessionManager!!.pushToken = refreshedToken
        }
        permissionsArray = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
        runTimePermission = RunTimePermission(mActivity!!, permissionsArray!!, false)
        CommonClass.statusBarColor(mActivity!!)
        CommonClass.generateHashKey(mActivity!!)
        rL_rootview = findViewById(R.id.rL_rootview) as RelativeLayout?
        iV_google_icon = findViewById(R.id.iV_google_icon) as ImageView?
        val iV_fbicon = findViewById(R.id.iV_fbicon) as ImageView?
        tV_googleLogin = findViewById(R.id.tV_googleLogin) as TextView?
        val tV_facebook = findViewById(R.id.tV_facebook) as TextView?
        pBar_googleLogin = findViewById(R.id.pBar_googleLogin) as ProgressBar?
        val pBar_fbLogin = findViewById(R.id.pBar_fbLogin) as ProgressBar?
        // initialize google signin variables

        /*GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestScopes(new Scope(Scopes.DRIVE_APPFOLDER))
                .requestServerAuthCode(getResources().getString(R.string.servers_client_id),false)
                .requestEmail()
                .build();*/
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build()
        mGoogleApiClient = GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build()
        loginWithFacebook = LoginWithFacebook(mActivity!!, rL_rootview!!, pBar_fbLogin!!, iV_fbicon!!, tV_facebook!!)
        val tV_login: TextView?
        val tV_signup: TextView?
        // Login
        tV_login = findViewById(R.id.tV_login) as TextView?
        tV_login!!.setOnClickListener(this)

        // Sign up
        tV_signup = findViewById(R.id.tV_signup) as TextView?
        tV_signup!!.setOnClickListener(this)

        // fb login
        val rL_fb_login = findViewById(R.id.rL_fb_login) as RelativeLayout?
        rL_fb_login!!.setOnClickListener(this)

        // Google login
        val rL_google_login = findViewById(R.id.rL_google_login) as RelativeLayout?
        rL_google_login!!.setOnClickListener(this)

        // close
        val rL_skip = findViewById(R.id.rL_skip) as RelativeLayout?
        rL_skip!!.setOnClickListener(this)

        // Terms and conditions
        val tV_termsNcondition = findViewById(R.id.tV_termsNcondition) as TextView?
        tV_termsNcondition!!.setOnClickListener(this)

        // privacy policy
        val tV_privacy = findViewById(R.id.tV_privacy) as TextView?
        tV_privacy!!.setOnClickListener(this)
    }

    override fun onResume() {
        super.onResume()
        isToStartActivity = true
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.tV_login -> openLoginSignupScreen("login")
            R.id.tV_signup ->                 /*if (mSessionManager.getSignUpPrivacyAccept()) {*/openLoginSignupScreen("normalSignup")
            R.id.rL_fb_login -> {
                LoginManager.getInstance().logOut()
                isFromGplusLocation = false
                loginWithFacebook!!.loginWithFbWithSdk()
            }
            R.id.rL_google_login -> {
                isFromGplusLocation = true
                signInWithGoogle()
            }
            R.id.rL_skip -> onBackPressed()
            R.id.tV_termsNcondition -> if (isToStartActivity) {
                isToStartActivity = false
                val termsNconIntent = Intent(this, PrivacyAndTermsActivity::class.java)
                termsNconIntent.putExtra("url", getResources().getString(R.string.termsNconditionsUrl))
                termsNconIntent.putExtra("title", getResources().getString(R.string.termsNcondition))
                startActivity(termsNconIntent)
            }
            R.id.tV_privacy -> if (isToStartActivity) {
                isToStartActivity = false
                val privacyIntent = Intent(this, PrivacyAndTermsActivity::class.java)
                privacyIntent.putExtra("url", getResources().getString(R.string.privacyPolicyUrl))
                privacyIntent.putExtra("title", getResources().getString(R.string.privacyPolicy))
                startActivity(privacyIntent)
            }
        }
    }

    private fun signInWithGoogle() {
        val signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient)
        startActivityForResult(signInIntent, VariableConstants.GOOGLE_LOGIN_REQ_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (data != null) {
            loginWithFacebook!!.fbOnActivityResult(requestCode, resultCode, data)
            when (requestCode) {
                VariableConstants.GOOGLE_LOGIN_REQ_CODE -> {
                    val result = Auth.GoogleSignInApi.getSignInResultFromIntent(data)
                    val statusCode = result!!.status.statusCode
                    Log.e("====", statusCode.toString() + "")
                    handleSignInResult(result)
                }
                VariableConstants.LOGIN_SIGNUP_REQ_CODE -> {
                    val isToFinishLandingScreen = data.getBooleanExtra("isToFinishLandingScreen", false)
                    val isFromSignup = data.getBooleanExtra("isFromSignup", false)
                    println(TAG + "isToFinishLandingScreen=" + isToFinishLandingScreen + " " + "isFromSignup=" + isFromSignup)
                    if (isToFinishLandingScreen) {
                        val intent = Intent()
                        intent.putExtra("isToRefreshHomePage", true)
                        intent.putExtra("isFromSignup", isFromSignup)
                        setResult(VariableConstants.LANDING_REQ_CODE, intent)
                        onBackPressed()
                    }
                }
                VariableConstants.PRIVACY_POLICY -> {
                    val accept = data.getBooleanExtra("accept", false)
                    if (accept) {
                        fromLandingAccept = true
                        openLoginSignupScreen("normalSignup")
                    }
                }
            }
        }
    }

    private fun handleSignInResult(result: GoogleSignInResult?) {
        Log.d(TAG, "handleSignInResult:" + result!!.isSuccess)
        if (result.isSuccess) {
            // Signed in successfully, show authenticated UI.
            val acct = result.signInAccount!!
            Log.e(TAG, "display name: " + acct.displayName)
            fullName = acct.displayName
            val imageUri = acct.photoUrl
            if (imageUri != null) personPhotoUrl = imageUri.toString()
            email = acct.email
            id = acct.id
            val familyName = acct.familyName
            val givenName = acct.givenName
            val idToken = acct.idToken
            serverAuthCode = acct.serverAuthCode
            Log.e(TAG, "Name: " + fullName + ", email: " + email
                    + ", Image: " + personPhotoUrl + ", id: " + id + ", familyName: " + familyName + ", givenName: " + givenName + ", idToken: " + idToken + ", serverAuthCode: " + serverAuthCode)
            val lm = getSystemService(Context.LOCATION_SERVICE) as LocationManager?
            val isLocationEnabled = lm!!.isProviderEnabled(LocationManager.GPS_PROVIDER)
            println(TAG + " " + "is location enabled=" + isLocationEnabled + " " + "is permission allowed=" + runTimePermission!!.checkPermissions(permissionsArray!!))
            googleLoginProgress(true)
            if (isLocationEnabled && runTimePermission!!.checkPermissions(permissionsArray!!)) currentLocation else googleLoginApi()
        }
    }

    /**
     * In this method we find current location using FusedLocationApi.
     * in this we have onUpdateLocation() method in which we check if
     * its not null then We call guest user api.
     */
    private val currentLocation: Unit
        private get() {
            if (CommonClass.isNetworkAvailable(mActivity!!)) {
                locationService = FusedLocationService(mActivity!!, object : FusedLocationReceiver {
                    override fun onUpdateLocation() {
                        val currentLocation = locationService!!.receiveLocation()
                        if (currentLocation != null) {
                            currentLat = currentLocation.latitude.toString()
                            currentLng = currentLocation.longitude.toString()
                            if (isLocationFound(currentLat, currentLng)) {
                                println("$TAG currentLat=$currentLat currentLng=$currentLng")
                                mSessionManager!!.currentLat = currentLat
                                mSessionManager!!.currentLng = currentLng
                                address = CommonClass.getCompleteAddressString(mActivity, currentLocation.latitude, currentLocation.longitude)
                                city = CommonClass.getCityName(mActivity!!, currentLocation.latitude, currentLocation.longitude)
                                countryCode = CommonClass.getCountryCode(mActivity!!, currentLocation.latitude, currentLocation.longitude)
                                googleLoginApi()
                            }
                        }
                    }
                }
                )
            } else CommonClass.showSnackbarMessage(rL_rootview, getResources().getString(R.string.NoInternetAccess))
        }

    /**
     * In this method we used to check whether current currentLat and
     * long has been received or not.
     *
     * @param lat The current latitude
     * @param lng The current longitude
     * @return boolean flag true or false
     */
    private fun isLocationFound(lat: String?, lng: String?): Boolean {
        return !(lat == null || lat.isEmpty()) && !(lng == null || lng.isEmpty())
    }

    private fun googleLoginApi() {
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            val request_datas = JSONObject()
            try {
                request_datas.put("loginType", VariableConstants.TYPE_GOOGLE)
                request_datas.put("pushToken", mSessionManager!!.pushToken)
                request_datas.put("place", address)
                request_datas.put("city", city)
                request_datas.put("countrySname", countryCode)
                request_datas.put("latitude", currentLat)
                request_datas.put("longitude", currentLng)
                request_datas.put("googleId", id)
                request_datas.put("email", email)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            OkHttp3Connection.doOkHttp3Connection(TAG, ApiUrl.LOGIN, OkHttp3Connection.Request_type.POST, request_datas, object : OkHttp3RequestCallback {
                override fun onSuccess(result: String?, user_tag: String?) {
                    try{
                    println("$TAG google login res=$result")
                    //progress_bar_login.setVisibility(View.GONE);
                    val loginResponse: LoginResponsePojo
                    val gson = Gson()
                    loginResponse = gson.fromJson(result, LoginResponsePojo::class.java)
                    when (loginResponse.code) {
                        "200" -> {
                            mSessionManager!!.isUserLoggedIn = true
                            mSessionManager!!.setmqttId(loginResponse.mqttId)
                            mSessionManager!!.authToken = loginResponse.token
                            mSessionManager!!.userName = loginResponse.username
                            mSessionManager!!.userImage = loginResponse.profilePicUrl
                            mSessionManager!!.userId = loginResponse.userId
                            mSessionManager!!.loginWith = "googleLogin"
                            initUserDetails(loginResponse.profilePicUrl, loginResponse.mqttId!!, email!!, loginResponse.username, loginResponse.token)
                            logDeviceInfo(loginResponse.token)
                        }
                        "204" -> if (isToStartActivity) {
                            isToStartActivity = false
                            googleLoginProgress(false)
                            val intent = Intent(mActivity, LoginOrSignupActivity::class.java)
                            intent.putExtra("type", "googleSignUp")
                            intent.putExtra("userFullName", fullName)
                            intent.putExtra("userImageUrl", personPhotoUrl)
                            intent.putExtra("email", email)
                            intent.putExtra("id", id)
                            intent.putExtra("serverAuthCode", serverAuthCode)
                            startActivityForResult(intent, VariableConstants.LOGIN_SIGNUP_REQ_CODE)
                        }
                        "401" -> CommonClass.sessionExpired(mActivity!!)
                        else ->                             //tV_do_login.setVisibility(View.VISIBLE);
                            CommonClass.showTopSnackBar(rL_rootview, loginResponse.message)
                    } } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }

                override fun onError(error: String?, user_tag: String?) {
                    googleLoginProgress(false)
                    CommonClass.showTopSnackBar(rL_rootview, error)
                }
            })
        } else CommonClass.showSnackbarMessage(rL_rootview, getResources().getString(R.string.NoInternetAccess))
    }

    /**
     * <h>LogDeviceInfo</h>
     *
     *
     * In this method we used to do api call to send device information like device name
     * model number, device id etc to server to log the the user with specific device.
     *
     *
     * @param token The auth token for particular user
     */
    private fun logDeviceInfo(token: String?) {
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            //deviceName, deviceId, deviceOs, modelNumber, appVersion
            val request_datas = JSONObject()
            try {
                request_datas.put("deviceName", Build.BRAND)
                request_datas.put("deviceId", mSessionManager!!.deviceId)
                request_datas.put("deviceOs", Build.VERSION.RELEASE)
                request_datas.put("modelNumber", Build.MODEL)
                request_datas.put("appVersion", BuildConfig.VERSION_NAME)
                request_datas.put("token", token)
                request_datas.put("deviceType", VariableConstants.DEVICE_TYPE)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            OkHttp3Connection.doOkHttp3Connection(TAG, ApiUrl.LOG_DEVICE, OkHttp3Connection.Request_type.POST, request_datas, object : OkHttp3RequestCallback {
                override fun onSuccess(result: String?, user_tag: String?) {
                    googleLoginProgress(false)
                    println("$TAG log device info=$result")
                    val logDevicePojo: LogDevicePojo
                    val gson = Gson()
                    logDevicePojo = gson.fromJson(result, LogDevicePojo::class.java)
                    when (logDevicePojo.code) {
                        "200" -> {
                            // Open Home page screen
                            //onBackPressed();
                            val intent = Intent()
                            intent.putExtra("isToRefreshHomePage", true)
                            setResult(VariableConstants.LANDING_REQ_CODE, intent)
                            onBackPressed()
                        }
                        "401" -> CommonClass.sessionExpired(mActivity!!)
                        else -> CommonClass.showSnackbarMessage(rL_rootview, logDevicePojo.message)
                    }
                }

                override fun onError(error: String?, user_tag: String?) {
                    googleLoginProgress(false)
                    CommonClass.showSnackbarMessage(rL_rootview, error)
                }
            })
        } else CommonClass.showSnackbarMessage(rL_rootview, getResources().getString(R.string.NoInternetAccess))
    }

    override fun onConnectionFailed(@NonNull connectionResult: ConnectionResult) {
        println("$TAG onConnectionFailed...")
    }

    /**
     * <h>OpenLoginSignupScreen</h>
     *
     *
     * In this method we used to launch the LoginOrSignupActivity class.
     *
     *
     * @param type The string value.
     */
    private fun openLoginSignupScreen(type: String?) {
        if (isToStartActivity) {
            isToStartActivity = false
            val intent = Intent(mActivity, LoginOrSignupActivity::class.java)
            intent.putExtra("type", type)
            intent.putExtra("fromLandingAccept", fromLandingAccept)
            startActivityForResult(intent, VariableConstants.LOGIN_SIGNUP_REQ_CODE)
        }
    }

    private fun googleLoginProgress(isVisible: Boolean) {
        if (isVisible) {
            pBar_googleLogin!!.visibility = View.VISIBLE
            iV_google_icon!!.visibility = View.GONE
            tV_googleLogin!!.visibility = View.GONE
        } else {
            pBar_googleLogin!!.visibility = View.GONE
            iV_google_icon!!.visibility = View.VISIBLE
            tV_googleLogin!!.visibility = View.VISIBLE
        }
    }

override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            VariableConstants.PERMISSION_REQUEST_CODE -> {
                println("grant result=" + grantResults!!.size)
                if (grantResults.size > 0) {
                    var count = 0
                    while (count < grantResults.size) {
                        if (grantResults[count] != PackageManager.PERMISSION_GRANTED) runTimePermission!!.allowPermissionAlert(permissions!![count])
                        count++
                    }
                    if (runTimePermission!!.checkPermissions(permissionsArray!!)) {
                        if (isFromGplusLocation) currentLocation else loginWithFacebook!!.currentLocation
                    }
                }
            }
        }
    }

  override  fun onBackPressed() {
        finish()
        overridePendingTransition(R.anim.stay, R.anim.slide_down)
    }

    /*
     * Initialization of the user details .*/
    private fun initUserDetails(profile_Url: String?, userId: String, email: String, userName: String, token: String) {
     try {
         if (AppController.instance != null) {
             val db = AppController.instance?.dbController
             val map: HashMap<String, Any> = HashMap()
             if (profile_Url != null && !profile_Url.isEmpty()) {
                 map["userImageUrl"] = profile_Url
             } else {
                 map["userImageUrl"] = ""
             }
             map["userIdentifier"] = email
             map["userId"] = userId
             map["userName"] = userName
             map["apiToken"] = token
             if (!db!!.checkUserDocExists(AppController.instance?.indexDocId, userId!!)) {
                 val userDocId = db.createUserInformationDocument(map)
                 db.addToIndexDocument(AppController.instance?.indexDocId, userId, userDocId)
             } else {
                 db.updateUserDetails(db.getUserDocId(userId, AppController.instance?.indexDocId!!), map)
             }
             db.updateIndexDocumentOnSignIn(AppController.instance?.indexDocId, userId)
             AppController.instance?.setSignedIn(true, userId, userName, email)
             AppController.instance?.isSignStatusChanged = true
         }
     }catch (e:Exception){e.printStackTrace()}
    }

    companion object {
        private val TAG: String? = LandingActivity::class.java.simpleName
    }
}