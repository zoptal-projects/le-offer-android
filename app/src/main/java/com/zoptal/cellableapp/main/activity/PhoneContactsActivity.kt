package com.zoptal.cellableapp.main.activity

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.provider.ContactsContract
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.Gravity
import android.view.View
import android.view.Window
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.gson.Gson
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.adapter.PhoneContactRvAdapter
import com.zoptal.cellableapp.fcm_push_notification.Config
import com.zoptal.cellableapp.fcm_push_notification.NotificationMessageDialog
import com.zoptal.cellableapp.fcm_push_notification.NotificationUtils.Companion.clearNotifications
import com.zoptal.cellableapp.main.activity.PhoneContactsActivity
import com.zoptal.cellableapp.pojo_class.phone_contact_pojo.PhoneContactData
import com.zoptal.cellableapp.pojo_class.phone_contact_pojo.PhoneContactMainPojo
import com.zoptal.cellableapp.utility.*
import com.zoptal.cellableapp.utility.OkHttp3Connection.OkHttp3RequestCallback
import org.json.JSONException
import org.json.JSONObject
import java.util.*

/**
 * <h>PhoneContactsActivity</h>
 *
 *
 * In this class we used to find friends from our phone contects.
 *
 * @since 04-Jul-17
 * @version 1.0
 * @author 3Embed
 */
class PhoneContactsActivity : AppCompatActivity(), View.OnClickListener {
    private var permissionsArray = arrayOf(Manifest.permission.READ_CONTACTS)
    private var runTimePermission: RunTimePermission? = null
    private var mProgress_bar: ProgressBar? = null
    private var mSessionManager: SessionManager? = null
    private var mActivity: Activity? = null
    private var rL_rootElement: RelativeLayout? = null
    private var rL_friend_count: RelativeLayout? = null
    private var tV_friend_count: TextView? = null
    private var tv_no_found: TextView? = null
    private var arrayListContacts: ArrayList<PhoneContactData>? = null
    private var phoneContactRvAdapter: PhoneContactRvAdapter? = null
    private var mNotificationMessageDialog: NotificationMessageDialog? = null
     override fun onCreate(@Nullable savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_phone_contact)
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale)
        initVariables()
    }

    /**
     * <h>initVariables</h>
     *
     *
     * In this method we used to initialize all variables.
     *
     */
    private fun initVariables() {
        mActivity = this@PhoneContactsActivity
        mNotificationMessageDialog = NotificationMessageDialog(mActivity)
        mSessionManager = SessionManager(mActivity!!)
        arrayListContacts = ArrayList()
        CommonClass.statusBarColor(mActivity!!)

        // receiving datas from last class
        val intent: Intent = getIntent()
        val followingCount = intent.getIntExtra("followingCount", 0)
        println("$TAG followingCount=$followingCount")

        // Initialize xml values
        rL_rootElement = findViewById(R.id.rL_rootElement) as RelativeLayout?
        rL_friend_count = findViewById(R.id.rL_friend_count) as RelativeLayout?
        val rV_phoneContacts: RecyclerView = findViewById(R.id.rV_phoneContacts) as RecyclerView
        tV_friend_count = findViewById(R.id.tV_friend_count) as TextView?
        mProgress_bar = findViewById(R.id.progress_bar) as ProgressBar?
        tv_no_found = findViewById(R.id.tv_no_found) as TextView?


        // set status bar color
        CommonClass.statusBarColor(mActivity!!)

        runTimePermission = RunTimePermission(mActivity!!, permissionsArray, true)

        // back button
        val rL_back_btn = findViewById(R.id.rL_back_btn) as RelativeLayout
        rL_back_btn.setOnClickListener(this)

        // set recyclerview adapter
        phoneContactRvAdapter = PhoneContactRvAdapter(mActivity!!, arrayListContacts!!, followingCount)
        val linearLayoutManager = LinearLayoutManager(mActivity)
        rV_phoneContacts.setLayoutManager(linearLayoutManager)
        rV_phoneContacts.setAdapter(phoneContactRvAdapter)

        // retrieve all contacts from phone
        if (runTimePermission!!.checkPermissions(permissionsArray)) {
            contactsIntoArrayList
        } else permissionAllow()
    }

    private fun permissionAllow() {
        val privacyDialog = Dialog(mActivity!!)
        privacyDialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        privacyDialog.setContentView(R.layout.allow_contact_permission)
        privacyDialog.window!!.setGravity(Gravity.CENTER)
        privacyDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        privacyDialog.window!!.attributes.windowAnimations = R.style.DialogAnimation
        privacyDialog.window!!.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT)
        privacyDialog.setCancelable(false)
        val rL_allow = privacyDialog.findViewById<RelativeLayout>(R.id.rL_allow)
        rL_allow.setOnClickListener {
            runTimePermission!!.requestPermission()
            privacyDialog.dismiss()
        }
        val rL_skip = privacyDialog.findViewById<RelativeLayout>(R.id.rL_skip)
        rL_skip.setOnClickListener {
            privacyDialog.dismiss()
            finish()
        }
        val et_privacy = privacyDialog.findViewById<EditText>(R.id.et_privacy)
        et_privacy.setOnClickListener {
            val intent = Intent(mActivity, PrivacyAndTermsActivity::class.java)
            intent.putExtra("url", mActivity!!.resources.getString(R.string.privacyPolicyUrl))
            intent.putExtra("title", mActivity!!.resources.getString(R.string.privacyPolicy))
            mActivity!!.startActivity(intent)
        }
        privacyDialog.show()
    }

    override fun onResume() {
        super.onResume()

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.REGISTRATION_COMPLETE))

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.PUSH_NOTIFICATION))

        // clear the notification area when the app is opened
        clearNotifications(getApplicationContext())
    }

   override fun onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!)
        super.onPause()
    }

    /**
     * <h>getContactsIntoArrayList</h>
     *
     *
     * In this method we used to retrieve the all contacts from our phone memory.
     * Once we get then save all contects into a string seperated by comma.
     *
     */
    val contactsIntoArrayList: Unit
        get() {
            var saveContact = ""
            mProgress_bar!!.visibility = View.VISIBLE
            val cursor: Cursor = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null)!!
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    val phonenumber = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))
                    if (phonenumber != null && !phonenumber.isEmpty()) saveContact = "$saveContact,$phonenumber"
                }
                cursor.close()
            }
            if (saveContact.startsWith(",")) {
                saveContact = saveContact.replaceFirst(",".toRegex(), "")
            }
            if (!saveContact.isEmpty()) {
                phoneContactsApi(saveContact)
            } else {
                tv_no_found!!.visibility = View.VISIBLE
                mProgress_bar!!.visibility = View.GONE
            }
            println("$TAG save contact=$saveContact")
        }

    /**
     * <h>PhoneContactsApi</h>
     *
     *
     * In this method we used to do api call to get complete infomation
     * about a user who registered from this app.
     *
     * @param contacts The String consisting the all contects.
     */
    private fun phoneContactsApi(contacts: String) {
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            val request_datas = JSONObject()
            try {
                request_datas.put("token", mSessionManager!!.authToken)
                request_datas.put("contactNumbers", contacts)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            OkHttp3Connection.doOkHttp3Connection(TAG, ApiUrl.PHONE_CONTACTS, OkHttp3Connection.Request_type.POST, request_datas, object : OkHttp3RequestCallback {
                 override fun onSuccess(result: String?, user_tag: String?) {
                    mProgress_bar!!.visibility = View.GONE
                    println("$TAG phone contacts res=$result")
                    val phoneContactMainPojo: PhoneContactMainPojo
                    val gson = Gson()
                    phoneContactMainPojo = gson.fromJson(result, PhoneContactMainPojo::class.java)
                    when (phoneContactMainPojo.code) {
                        "200" -> if (phoneContactMainPojo.data != null && phoneContactMainPojo.data!!.size > 0) {
                            arrayListContacts!!.addAll(phoneContactMainPojo.data!!)
                            mSessionManager!!.contectFriendCount = arrayListContacts!!.size

                            // set count
                            rL_friend_count!!.visibility = View.VISIBLE
                            val setCountText: String
                            setCountText = if (arrayListContacts!!.size > 1) arrayListContacts!!.size.toString() + " " + getResources().getString(R.string.friends_on) + " " + getResources().getString(R.string.app_name) else arrayListContacts!!.size.toString() + " " + getResources().getString(R.string.friend_on) + " " + getResources().getString(R.string.app_name)
                            tV_friend_count!!.text = setCountText

                            // notify adapter
                            phoneContactRvAdapter!!.notifyDataSetChanged()
                        }
                        "204" -> tv_no_found!!.visibility = View.VISIBLE
                        "401" -> CommonClass.sessionExpired(mActivity!!)
                    }
                }

                 override fun onError(error: String?, user_tag: String?) {
                    mProgress_bar!!.visibility = View.GONE
                    CommonClass.showSnackbarMessage(rL_rootElement, error)
                }
            })
        } else CommonClass.showSnackbarMessage(rL_rootElement, getResources().getString(R.string.NoInternetAccess))
    }

    override fun onRequestPermissionsResult(requestCode: Int, @NonNull permissions: Array<String?>, @NonNull grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            VariableConstants.PERMISSION_REQUEST_CODE -> {
                println("grant result=" + grantResults.size)
                if (grantResults.size > 0) {
                    var count = 0
                    while (count < grantResults.size) {
                        if (grantResults[count] != PackageManager.PERMISSION_GRANTED) runTimePermission!!.allowPermissionAlert(permissions[count]!!)
                        count++
                    }
                    println("isAllPermissionGranted=" + runTimePermission!!.checkPermissions(permissionsArray))
                    if (runTimePermission!!.checkPermissions(permissionsArray)) {
                        contactsIntoArrayList
                    }
                }
            }
        }
    }

 override fun onBackPressed() {
        val intent = Intent()
        intent.putExtra("contact_count", arrayListContacts!!.size)
        intent.putExtra("followingCount", phoneContactRvAdapter!!.followingCount)
        setResult(VariableConstants.CONTACT_FRIEND_REQ_CODE, intent)
        finish()
        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.rL_back_btn -> onBackPressed()
        }
    }

    companion object {
        private val TAG = PhoneContactsActivity::class.java.simpleName
    }
}