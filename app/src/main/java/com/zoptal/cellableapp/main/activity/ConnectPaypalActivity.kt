package com.zoptal.cellableapp.main.activity

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.annotation.Nullable
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.gson.Gson
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.event_bus.BusProvider.instance
import com.zoptal.cellableapp.fcm_push_notification.Config
import com.zoptal.cellableapp.fcm_push_notification.NotificationMessageDialog
import com.zoptal.cellableapp.fcm_push_notification.NotificationUtils.Companion.clearNotifications
import com.zoptal.cellableapp.main.activity.ConnectPaypalActivity
import com.zoptal.cellableapp.pojo_class.verify_paypal_link.VerifyPaypalMainPojo
import com.zoptal.cellableapp.utility.*
import com.zoptal.cellableapp.utility.OkHttp3Connection.OkHttp3RequestCallback
import org.json.JSONException
import org.json.JSONObject
import java.util.*

/**
 * <h>ConnectPaypalActivity</h>
 *
 * @since 20-Jul-17
 */
class ConnectPaypalActivity constructor() : AppCompatActivity(), View.OnClickListener {
    private var mActivity: Activity? = null
    private var mSessionManager: SessionManager? = null
    private var rL_rootview: RelativeLayout? = null
    private var rL_apply: RelativeLayout? = null
    private var eT_yourName: EditText? = null
    private var isToSave: Boolean = false
    private var progress_bar_save: ProgressBar? = null
    private var tV_save: TextView? = null
    private var mNotificationMessageDialog: NotificationMessageDialog? = null
    private var unlinkPaypalLink: Boolean = false
     override fun onCreate(@Nullable savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_connect_paypal)
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale)
        initVariables()
    }

    /**
     * <h>InitVariables</h>
     *
     *
     * In this method we used to initialize all variables.
     *
     */
    private fun initVariables() {
        mActivity = this@ConnectPaypalActivity
        mNotificationMessageDialog = NotificationMessageDialog(mActivity)
        mSessionManager = SessionManager(mActivity!!)
        isToSave = false
        instance.register(this)

        // root element
        rL_rootview = findViewById(R.id.rL_rootview) as RelativeLayout?
        rL_apply = findViewById(R.id.rL_apply) as RelativeLayout?
        rL_apply!!.setOnClickListener(this)
        progress_bar_save = findViewById(R.id.progress_bar_save) as ProgressBar?
        tV_save = findViewById(R.id.tV_save) as TextView?
        CommonClass.setViewOpacity(mActivity, rL_apply!!, 102, R.color.status_bar_color)

        // set name
        eT_yourName = findViewById(R.id.eT_yourName) as EditText?
        eT_yourName!!.addTextChangedListener(object : TextWatcher {
            public override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            public override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (!TextUtils.isEmpty(eT_yourName!!.getText())) {
                    CommonClass.setViewOpacity(mActivity, rL_apply!!, 255, R.color.status_bar_color)
                    isToSave = true
                } else {
                    CommonClass.setViewOpacity(mActivity, rL_apply!!, 102, R.color.status_bar_color)
                    isToSave = false
                }
            }

            public override fun afterTextChanged(s: Editable) {}
        })

        // set paypal verified name
        val intent: Intent? = getIntent()
        if (intent != null) {
            val payPalLink: String? = intent.getStringExtra("payPalLink")
            println(TAG + " " + "paypalUrl=" + payPalLink)
            if (payPalLink != null && !payPalLink.isEmpty()) {
                val index: Int = payPalLink.lastIndexOf('/')
                println(TAG + " " + "paypal verified name=" + payPalLink.substring(index + 1, payPalLink.length))
                val paypalVerifiedName: String = payPalLink.substring(index + 1, payPalLink.length)
                if (!paypalVerifiedName.isEmpty()) {
                    eT_yourName!!.setText(paypalVerifiedName)
                    tV_save!!.setText("Unlink paypal link")
                    unlinkPaypalLink = true
                }
            }
        }

        // back button
        val rL_back_btn: RelativeLayout = findViewById(R.id.rL_back_btn) as RelativeLayout
        rL_back_btn.setOnClickListener(this)

        // get paypal
        val rL_getPaypal: RelativeLayout = findViewById(R.id.rL_getPaypal) as RelativeLayout
        rL_getPaypal.setOnClickListener(this)
    }

    override fun onResume() {
        super.onResume()

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.REGISTRATION_COMPLETE))

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.PUSH_NOTIFICATION))

        // clear the notification area when the app is opened
        clearNotifications(getApplicationContext())
    }

   override fun onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!)
        super.onPause()
    }

    public override fun onClick(v: View) {
        when (v.getId()) {
            R.id.rL_back_btn -> onBackPressed()
            R.id.rL_getPaypal -> {
                val intent: Intent = Intent(mActivity, WebViewActivity::class.java)
                intent.putExtra("browserLink", VariableConstants.GET_PAYPAL_LINK)
                intent.putExtra("actionBarTitle", getResources().getString(R.string.get_paypal))
                startActivity(intent)
            }
            R.id.rL_apply -> if (isToSave) linkPaypalApi()
        }
    }

    private fun linkPaypalApi() {
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            progress_bar_save!!.setVisibility(View.VISIBLE)
            tV_save!!.setVisibility(View.GONE)
            val paypalUrl: String
            if (!unlinkPaypalLink) paypalUrl = getResources().getString(R.string.connect_paypal_link).toString() + eT_yourName!!.getText().toString() else paypalUrl = ""
            val request_datas: JSONObject = JSONObject()
            try {
                request_datas.put("token", mSessionManager!!.authToken)
                request_datas.put("paypalUrl", paypalUrl)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            OkHttp3Connection.doOkHttp3Connection(TAG, ApiUrl.LINK_PAYPAL, OkHttp3Connection.Request_type.PUT, request_datas, object : OkHttp3RequestCallback {
                public  override fun onSuccess(result: String?, user_tag: String?) {
                    progress_bar_save!!.setVisibility(View.GONE)
                    tV_save!!.setVisibility(View.VISIBLE)
                    Log.d(TAG, "onSuccess: res= " + result)
                    val verifyPaypalMainPojo: VerifyPaypalMainPojo
                    val gson: Gson = Gson()
                    verifyPaypalMainPojo = gson.fromJson(result, VerifyPaypalMainPojo::class.java)
                    when (verifyPaypalMainPojo.code) {
                        "200" -> {
                            CommonClass.showSuccessSnackbarMsg(rL_rootview, verifyPaypalMainPojo.message)
                            mSessionManager!!.userPayPal = paypalUrl
                            instance.post(paypalUrl)
                            val t: Timer = Timer()
                            t.schedule(object : TimerTask() {
                                public override fun run() {
                                    // when the task active then close the activity
                                    t.cancel()
                                    val intent: Intent = Intent()
                                    intent.putExtra("isPaypalVerified", true)
                                    intent.putExtra("payPalLink", paypalUrl)
                                    setResult(VariableConstants.PAYPAL_REQ_CODE, intent)
                                    onBackPressed()
                                }
                            }, 3000)
                        }
                        "401" -> CommonClass.sessionExpired(mActivity!!)
                        else -> CommonClass.showSnackbarMessage(rL_rootview, verifyPaypalMainPojo.message)
                    }
                }

                public  override fun onError(error: String?, user_tag: String?) {
                    progress_bar_save!!.setVisibility(View.GONE)
                    tV_save!!.setVisibility(View.VISIBLE)
                }
            })
        } else CommonClass.showSnackbarMessage(rL_rootview, getResources().getString(R.string.NoInternetAccess))
    }

 override fun onBackPressed() {
        hideKeyboard()
        finish()
        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate)
    }

    fun hideKeyboard() {
        try {
            val inputManager: InputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputManager.hideSoftInputFromWindow(tV_save!!.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        val imm: InputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(View(this).getWindowToken(), 0)
        return true
    }

    /**
     * <h>ShowKeyboard</h>
     *
     *
     * In this method we used to open device keypad when user
     * click on search iocn and close when user click close
     * search button.
     *
     *
     * @param flag This is integer valuew to open or close keypad
     */
    private fun showKeyboard(flag: Int) {
        val imm: InputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.toggleSoftInput(flag, 0)
    }

    override fun onDestroy() {
        super.onDestroy()
        instance.unregister(this)
    }

    companion object {
        private val TAG: String = ConnectPaypalActivity::class.java.getSimpleName()
    }
}