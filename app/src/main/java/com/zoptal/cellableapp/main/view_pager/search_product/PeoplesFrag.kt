package com.zoptal.cellableapp.main.view_pager.search_product

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import androidx.annotation.Nullable
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.gson.Gson
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.adapter.SearchPeopleRvAdap
import com.zoptal.cellableapp.main.activity.SearchProductActivity
import com.zoptal.cellableapp.main.activity.SelfProfileActivity
import com.zoptal.cellableapp.main.activity.UserProfileActivity
import com.zoptal.cellableapp.pojo_class.search_people_pojo.SearchPeopleMainPojo
import com.zoptal.cellableapp.pojo_class.search_people_pojo.SearchPeopleUsers
import com.zoptal.cellableapp.utility.*
import com.zoptal.cellableapp.utility.OkHttp3Connection.OkHttp3RequestCallback
import org.json.JSONException
import org.json.JSONObject
import java.util.*

/**
 * <h>PeoplesFrag</h>
 *
 *
 * This class is getting called from SearchProductActivity class.
 * In this class we used to search the user using search user api.
 *
 * @since 18-May-17
 * @version 1.0
 * @author 3Embed
 */
class PeoplesFrag : Fragment() {
    private var mActivity: Activity? = null
    private var mSessionManager: SessionManager? = null
    private var rL_rootElement: RelativeLayout? = null
    private var rV_search_people: RecyclerView? = null
    private var isPeopleFrag = false
    private var mSwipeRefreshLayout: SwipeRefreshLayout? = null
    private var pageIndex = 0

    // Load more variables
    private val totalVisibleItem = 0
    private val totalItemCount = 0
    private val visibleThreshold = 5
    private val isToLoadData = false
    override fun onCreate(@Nullable savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        pageIndex = 0
        mActivity = getActivity()
        mSessionManager = SessionManager(mActivity!!)
    }

    @Nullable
    override fun onCreateView(inflater: LayoutInflater, @Nullable container: ViewGroup?, @Nullable savedInstanceState: Bundle?): View {
        val view = inflater.inflate(R.layout.frag_people, container, false)
        rL_rootElement = view.findViewById<View>(R.id.rL_rootElement) as RelativeLayout
        rV_search_people = view.findViewById<View>(R.id.rV_search_people) as RecyclerView
        mSwipeRefreshLayout = view.findViewById<View>(R.id.swipeRefreshLayout) as SwipeRefreshLayout
        mSwipeRefreshLayout!!.setColorSchemeResources(R.color.pink_color)

        // search user
        searchUser()

        // pull to refresh
        mSwipeRefreshLayout!!.setOnRefreshListener(object : SwipeRefreshLayout.OnRefreshListener {
            override fun onRefresh() {
                pageIndex = 0
                if (mSessionManager!!.isUserLoggedIn) searchUsersApi((mActivity as SearchProductActivity?)!!.peopleText, pageIndex, ApiUrl.SEARCH_USER) else searchUsersApi((mActivity as SearchProductActivity?)!!.peopleText, pageIndex, ApiUrl.SEARCH_GUEST_USER)
            }
        })
        return view
    }

    /**
     * <h>SearchUser</h>
     *
     *
     * In this method we used to do search people api call
     * on every text changes.
     *
     */
    private fun searchUser() {
        (mActivity as SearchProductActivity?)!!.eT_search_users!!.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (isPeopleFrag) {
                    println(TAG + " " + "text entered=" + (mActivity as SearchProductActivity?)!!.eT_search_users!!.text.toString())
                    (mActivity as SearchProductActivity?)!!.peopleText = (mActivity as SearchProductActivity?)!!.eT_search_users!!.text.toString()
                    pageIndex = 0
                    //searchUsersApi(((SearchProductActivity) mActivity).eT_search_users.getText().toString(), pageIndex);
                    if (mSessionManager!!.isUserLoggedIn) searchUsersApi((mActivity as SearchProductActivity?)!!.eT_search_users!!.text.toString(), pageIndex, ApiUrl.SEARCH_USER) else searchUsersApi((mActivity as SearchProductActivity?)!!.eT_search_users!!.text.toString(), pageIndex, ApiUrl.SEARCH_GUEST_USER)
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })
    }

    private fun searchGuestMemberApi(value: String, offset: Int) {}

    /**
     * <h>SearchUsersApi</h>
     *
     *
     * In this method we used to do search user api.
     *
     * @param value The list we get from server
     * @param offset The index
     */
    private fun searchUsersApi(value: String, offset: Int, apiUrl: String) {
        var offset = offset
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            val limit = 20
            offset = limit * offset
            val request_datas = JSONObject()
            try {
                request_datas.put("userNameToSearch", value)
                request_datas.put("member", value)
                request_datas.put("offset", offset)
                request_datas.put("limit", limit)
                request_datas.put("token", mSessionManager!!.authToken)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            println("$TAG offset=$offset searched text=$value")
            OkHttp3Connection.doOkHttp3Connection(TAG, apiUrl, OkHttp3Connection.Request_type.POST, request_datas, object : OkHttp3RequestCallback {
                 override fun onSuccess(result: String?, user_tag: String?) {
                    mSwipeRefreshLayout!!.setRefreshing(false)
                    println("$TAG search user res=$result")
                    val searchPeopleMainPojo: SearchPeopleMainPojo
                    val gson = Gson()
                    searchPeopleMainPojo = gson.fromJson(result, SearchPeopleMainPojo::class.java)
                    when (searchPeopleMainPojo.code) {
                        "200" -> if (searchPeopleMainPojo.data != null && searchPeopleMainPojo.data!!.size > 0) {
                            val aL_users = searchPeopleMainPojo.data
                            val searchPeopleRvAdap = SearchPeopleRvAdap(mActivity!!, aL_users!!)
                            val mLinearLayoutManager = LinearLayoutManager(mActivity)
                            rV_search_people!!.setLayoutManager(mLinearLayoutManager)
                            rV_search_people!!.setAdapter(searchPeopleRvAdap)
                            searchPeopleRvAdap.notifyDataSetChanged()

                            // Item click
                            searchPeopleRvAdap.setOnItemClick(object : ClickListener {
                                override fun onItemClick(view: View?, position: Int) {
                                    val intent: Intent
                                    if (mSessionManager!!.isUserLoggedIn && mSessionManager!!.userName == aL_users[position].username) {
                                        intent = Intent(mActivity, SelfProfileActivity::class.java)
                                        intent.putExtra("membername", aL_users[position].username)
                                    } else {
                                        intent = Intent(mActivity, UserProfileActivity::class.java)
                                        intent.putExtra("membername", aL_users[position].username)
                                    }
                                    mActivity!!.startActivity(intent)
                                }
                            })
                        } else {
                            val aL_users = ArrayList<SearchPeopleUsers>()
                            val searchPeopleRvAdap = SearchPeopleRvAdap(mActivity!!, aL_users)
                            val mLinearLayoutManager = LinearLayoutManager(mActivity)
                            rV_search_people!!.setLayoutManager(mLinearLayoutManager)
                            rV_search_people!!.setAdapter(searchPeopleRvAdap)
                            searchPeopleRvAdap.notifyDataSetChanged()
                        }
                        "401" -> CommonClass.sessionExpired(mActivity!!)
                        "56714" -> {
                            val aL_users = ArrayList<SearchPeopleUsers>()
                            val searchPeopleRvAdap = SearchPeopleRvAdap(mActivity!!, aL_users)
                            val mLinearLayoutManager = LinearLayoutManager(mActivity)
                            rV_search_people!!.setLayoutManager(mLinearLayoutManager)
                            rV_search_people!!.setAdapter(searchPeopleRvAdap)
                            searchPeopleRvAdap.notifyDataSetChanged()
                        }
                    }
                }

                 override fun onError(error: String?, user_tag: String?) {
                    mSwipeRefreshLayout!!.setRefreshing(false)
                    CommonClass.showTopSnackBar(rL_rootElement, error)
                }
            })
        } else CommonClass.showTopSnackBar(rL_rootElement, getResources().getString(R.string.NoInternetAccess))
    }

    private fun setRvAdapterDatas() {}
    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        isPeopleFrag = isVisibleToUser
    }

    companion object {
        private val TAG = PeoplesFrag::class.java.simpleName
    }
}