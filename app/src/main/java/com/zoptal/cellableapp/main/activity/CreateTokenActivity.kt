package com.zoptal.cellableapp.main.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.View
import android.widget.*
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import com.google.gson.Gson
import com.stripe.android.ApiResultCallback
import com.stripe.android.Stripe
import com.stripe.android.model.Token
import com.stripe.android.view.CardInputWidget
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.main.activity.CreateTokenActivity
import com.zoptal.cellableapp.pojo_class.card.CardMainPojo
import com.zoptal.cellableapp.utility.ApiUrl
import com.zoptal.cellableapp.utility.CommonClass
import com.zoptal.cellableapp.utility.OkHttp3Connection
import com.zoptal.cellableapp.utility.OkHttp3Connection.OkHttp3RequestCallback
import com.zoptal.cellableapp.utility.SessionManager
import org.json.JSONException
import org.json.JSONObject

class CreateTokenActivity() : AppCompatActivity(), View.OnClickListener {
    private var mSessionManager: SessionManager? = null
    var confirmPaymentIntent: Button? = null
    var cardInputWidget: CardInputWidget? = null
    var stripe: Stripe? = null
    var STRIPE_KEY =  "pk_live_LUnXQzvwDZXEUdyaEsDteOmE00DxCm2Q3k";//test account key
//            "pk_live_LUnXQzvwDZXEUdyaEsDteOmE00DxCm2Q3k" //live key
    var sw_default: Switch? = null
    var et_name: EditText? = null
    var progress_bar_profile: ProgressBar? = null
    var rL_rootElement: RelativeLayout? = null
     override fun onCreate(@Nullable savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_create_token)
        stripe = Stripe(this, STRIPE_KEY)
        sw_default = findViewById(R.id.sw_default) as Switch?
        mSessionManager = SessionManager(this)
        cardInputWidget = findViewById(R.id.card_input_widget)
        confirmPaymentIntent = findViewById(R.id.btn_confirm_payment_intent)
        progress_bar_profile = findViewById(R.id.progress_bar_profile)
        et_name = findViewById(R.id.et_name)
        rL_rootElement = findViewById(R.id.rL_rootElement)
        confirmPaymentIntent!!.setOnClickListener(View.OnClickListener {
            CommonClass.hideKeyboard(this@CreateTokenActivity, et_name!!)
            if (cardInputWidget!!.card == null) {
                Toast.makeText(this@CreateTokenActivity, getResources().getString(R.string.err_card), Toast.LENGTH_SHORT).show()
                return@OnClickListener
            } else if ((et_name!!.text.toString().trim { it <= ' ' } == "") || (et_name!!.text.toString().trim { it <= ' ' } == null)) {
                Toast.makeText(this@CreateTokenActivity, getResources().getString(R.string.name_empty), Toast.LENGTH_SHORT).show()
                return@OnClickListener
            } else {
                confirmPaymentIntent!!.isClickable = false
                stripe!!.createToken(cardInputWidget!!.card!!, object : ApiResultCallback<Token> {
                    override fun onSuccess(@NonNull result: Token) {
                        val tokenID = result.id
                        Log.e("====", tokenID)
                        addCard(tokenID)
                    }

                    override fun onError(@NonNull e: Exception) {
                        e.printStackTrace()
                        confirmPaymentIntent!!.isClickable = true
                        CommonClass.showToast(this@CreateTokenActivity, e.message)
                    }
                })
            }
        })
    }

    private fun addCard(token: String) {
        try {
            progress_bar_profile!!.visibility = View.VISIBLE
            if (CommonClass.isNetworkAvailable(this)) {
                var i = 0
                if (sw_default!!.isChecked) {
                    i = 1
                } else {
                    i = 0
                }
                val request_datas = JSONObject()
                try {
                    request_datas.put("token", mSessionManager!!.authToken)
                    request_datas.put("is_default", i)
                    request_datas.put("name", et_name!!.text.toString().trim { it <= ' ' })
                    request_datas.put("stripetoken", token)
                    request_datas.put("country", "")
                    request_datas.put("zip_code", "")
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
                val url = ApiUrl.ADD_CARD
                OkHttp3Connection.doOkHttp3Connection(TAG, url, OkHttp3Connection.Request_type.POST, request_datas, object : OkHttp3RequestCallback {
                     override fun onSuccess(result: String?, user_tag: String?) {
                        progress_bar_profile!!.visibility = View.GONE
                        val cardMainPojo: CardMainPojo
                        val gson = Gson()
                        cardMainPojo = gson.fromJson(result, CardMainPojo::class.java)
                        CommonClass.showToast(this@CreateTokenActivity, cardMainPojo.message)
                        if ((cardMainPojo.code == "200")) {
                            onBackPressed()
                        }
                        confirmPaymentIntent!!.isClickable = true
                    }

                     override fun onError(error: String?, user_tag: String?) {
                        confirmPaymentIntent!!.isClickable = true
                        progress_bar_profile!!.visibility = View.GONE
                        CommonClass.showSnackbarMessage(rL_rootElement, error)
                    }
                })
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onClick(v: View) {
        try {
            when (v.id) {
                R.id.rL_back_btn -> onBackPressed()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    companion object {
        private val TAG = CreateTokenActivity::class.java.simpleName
    }
}