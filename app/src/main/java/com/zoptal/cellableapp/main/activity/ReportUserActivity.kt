package com.zoptal.cellableapp.main.activity

import android.app.Activity
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.annotation.Nullable
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.gson.Gson
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.adapter.ReportItemRvAdapter
import com.zoptal.cellableapp.fcm_push_notification.Config
import com.zoptal.cellableapp.fcm_push_notification.NotificationMessageDialog
import com.zoptal.cellableapp.fcm_push_notification.NotificationUtils.Companion.clearNotifications
import com.zoptal.cellableapp.main.activity.ReportUserActivity
import com.zoptal.cellableapp.pojo_class.PostItemReportPojo
import com.zoptal.cellableapp.pojo_class.report_product_pojo.ReportProductDatas
import com.zoptal.cellableapp.pojo_class.report_product_pojo.ReportProductMain
import com.zoptal.cellableapp.utility.ApiUrl
import com.zoptal.cellableapp.utility.CommonClass
import com.zoptal.cellableapp.utility.OkHttp3Connection
import com.zoptal.cellableapp.utility.OkHttp3Connection.OkHttp3RequestCallback
import com.zoptal.cellableapp.utility.SessionManager
import org.json.JSONException
import org.json.JSONObject
import java.util.*

/**
 * <h>ReportUserActivity</h>
 *
 *
 * In this class we used to report for any illegal user.
 *
 * @since 27-Jul-17
 */
class ReportUserActivity : AppCompatActivity(), View.OnClickListener {
    private var mActivity: Activity? = null
    private var mSessionManager: SessionManager? = null
    private var rL_rootElement: RelativeLayout? = null
    private var arrayListReportItem: ArrayList<ReportProductDatas>? = null
    private var mProgressBar: ProgressBar? = null
    private var pBar_postReport: ProgressBar? = null
    private var userImage = ""
    private var userName = ""
    private var userFullName = ""
    private var rV_reportUser: RecyclerView? = null
    private var rL_submit: RelativeLayout? = null
    private var tV_submit: TextView? = null
    private var mNotificationMessageDialog: NotificationMessageDialog? = null
     override fun onCreate(@Nullable savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_report_user)
        overridePendingTransition(R.anim.slide_up, R.anim.stay)
        initVariables()
    }

    /**
     * <h>InitVariables</h>
     *
     *
     * In this method we used to initialize all variables
     *
     */
    private fun initVariables() {
        mActivity = this@ReportUserActivity
        mNotificationMessageDialog = NotificationMessageDialog(mActivity)
        arrayListReportItem = ArrayList()
        rV_reportUser = findViewById(R.id.rV_reportUser) as RecyclerView?
        mSessionManager = SessionManager(mActivity!!)
        rL_rootElement = findViewById(R.id.rL_rootElement) as RelativeLayout?
        mProgressBar = findViewById(R.id.progress_bar_reportUser) as ProgressBar?
        pBar_postReport = findViewById(R.id.pBar_postReport) as ProgressBar?
        rL_submit = findViewById(R.id.rL_submit) as RelativeLayout?
        rL_submit!!.visibility = View.GONE
        rL_submit!!.setOnClickListener(this)
        tV_submit = findViewById(R.id.tV_submit) as TextView?

        // receiving data from last class
        val intent: Intent = getIntent()
        userImage = intent.getStringExtra("userImage")
        userName = intent.getStringExtra("userName")
        userFullName = intent.getStringExtra("userFullName")
        val rL_back_btn = findViewById(R.id.rL_back_btn) as RelativeLayout
        rL_back_btn.setOnClickListener(this)
        reportReasonApi
    }

    override fun onResume() {
        super.onResume()

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.REGISTRATION_COMPLETE))

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.PUSH_NOTIFICATION))

        // clear the notification area when the app is opened
        clearNotifications(getApplicationContext())
    }

   override fun onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!)
        super.onPause()
    }

    /**
     * <h>GetReportReasonApi</h>
     *
     *
     * In this method we used to do api call to get the reasons to report user.
     *
     */
    private val reportReasonApi: Unit
        private get() {
            if (CommonClass.isNetworkAvailable(mActivity!!)) {
                val url = ApiUrl.REPORT_USER_REASON + "?token=" + mSessionManager!!.authToken
                OkHttp3Connection.doOkHttp3Connection(TAG, url, OkHttp3Connection.Request_type.GET, JSONObject(), object : OkHttp3RequestCallback {
                     override fun onSuccess(result: String?, user_tag: String?) {
                        println("$TAG get report reason res=$result")
                        mProgressBar!!.visibility = View.GONE
                        println("$TAG report post reason res=$result")
                        val gson = Gson()
                        val reportProductMain = gson.fromJson(result, ReportProductMain::class.java)
                        when (reportProductMain.code) {
                            "200" -> {
                                arrayListReportItem!!.addAll(reportProductMain.data!!)
                                if (arrayListReportItem != null && arrayListReportItem!!.size > 0) {
                                    rL_submit!!.visibility = View.VISIBLE
                                    val reportItemRvAdapter = ReportItemRvAdapter(mActivity!!, arrayListReportItem!!)
                                    val linearLayoutManager = LinearLayoutManager(mActivity)
                                    rV_reportUser!!.setLayoutManager(linearLayoutManager)
                                    rV_reportUser!!.setAdapter(reportItemRvAdapter)
                                }
                            }
                            "401" -> CommonClass.sessionExpired(mActivity!!)
                            else -> {
                            }
                        }
                    }

                     override fun onError(error: String?, user_tag: String?) {}
                })
            } else CommonClass.showSnackbarMessage(rL_rootElement, getResources().getString(R.string.NoInternetAccess))
        }

    /**
     * <h>ReportUserApi</h>
     *
     *
     * In this method we used to do api call to report user and pass the report id and description
     * to the server.
     *
     * @param reason The reason id
     * @param description The description.
     */
    private fun reportUserApi(reason: String, description: String) {
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            tV_submit!!.visibility = View.GONE
            pBar_postReport!!.visibility = View.VISIBLE

            // token, reportedUser, reason
            val request_datas = JSONObject()
            try {
                request_datas.put("token", mSessionManager!!.authToken)
                //request_datas.put("reportedUser", userName);
                request_datas.put("reasonId", reason)
                request_datas.put("description", description)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            val url = ApiUrl.REPORT_USER + userName
            OkHttp3Connection.doOkHttp3Connection(TAG, url, OkHttp3Connection.Request_type.POST, request_datas, object : OkHttp3RequestCallback {
                 override fun onSuccess(result: String?, user_tag: String?) {
                    println(TAG + "report user res=" + result)
                    tV_submit!!.visibility = View.VISIBLE
                    pBar_postReport!!.visibility = View.GONE
                    val gson = Gson()
                    val itemReportPojo = gson.fromJson(result, PostItemReportPojo::class.java)
                    when (itemReportPojo.code) {
                        "200" -> {
                            CommonClass.showSuccessSnackbarMsg(rL_rootElement, getResources().getString(R.string.the_user_report_has_been))
                            val t = Timer()
                            t.schedule(object : TimerTask() {
                                override fun run() {
                                    // when the task active then close the activity
                                    t.cancel()
                                    onBackPressed()
                                }
                            }, 3000)
                        }
                        "401" -> CommonClass.sessionExpired(mActivity!!)
                        "9477" -> CommonClass.showSnackbarMessage(rL_rootElement, itemReportPojo.message)
                        else -> CommonClass.showSnackbarMessage(rL_rootElement, itemReportPojo.message)
                    }
                }

                 override fun onError(error: String?, user_tag: String?) {
                    CommonClass.showSnackbarMessage(rL_rootElement, error)
                    tV_submit!!.visibility = View.VISIBLE
                    pBar_postReport!!.visibility = View.GONE
                }
            })
        } else CommonClass.showSnackbarMessage(rL_rootElement, getResources().getString(R.string.NoInternetAccess))
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.rL_back_btn -> onBackPressed()
            R.id.rL_submit -> if (arrayListReportItem!!.size > 0) {
                for (reportProductDatas in arrayListReportItem!!) {
                    if (reportProductDatas.isItemSelected) {
                        println(TAG + " " + "user report reason=" + reportProductDatas.reportReasonByUser)
                        reportUserApi(reportProductDatas._id, reportProductDatas.reportReasonByUser)
                    }
                }
            }
        }
    }

 override fun onBackPressed() {
        finish()
        overridePendingTransition(R.anim.stay, R.anim.slide_down)
    }

    companion object {
        private val TAG = ReportUserActivity::class.java.simpleName
    }
}