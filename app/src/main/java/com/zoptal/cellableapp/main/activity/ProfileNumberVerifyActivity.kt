package com.zoptal.cellableapp.main.activity

import android.app.Activity
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.annotation.Nullable
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.gson.Gson
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.fcm_push_notification.Config
import com.zoptal.cellableapp.fcm_push_notification.NotificationMessageDialog
import com.zoptal.cellableapp.fcm_push_notification.NotificationUtils.Companion.clearNotifications
import com.zoptal.cellableapp.main.activity.EditProfileActivity
import com.zoptal.cellableapp.main.activity.ProfileNumberVerifyActivity
import com.zoptal.cellableapp.pojo_class.profile_mobile_otp.ProfileNumberOtpMain
import com.zoptal.cellableapp.pojo_class.update_no_pojo.UpdatePhoneNoPojo
import com.zoptal.cellableapp.utility.ApiUrl
import com.zoptal.cellableapp.utility.CommonClass
import com.zoptal.cellableapp.utility.OkHttp3Connection
import com.zoptal.cellableapp.utility.OkHttp3Connection.OkHttp3RequestCallback
import com.zoptal.cellableapp.utility.SessionManager
import org.json.JSONException
import org.json.JSONObject

/**
 * <h>ProfileNumberVerifyActivity</h>
 *
 *
 * This class is getting called from VerifyPhoneNoActivity class. In this class we used
 * to insert otp which is being sent to our mobile number. If the inserted otp is correct
 * then call update mobile number api to update the obile number.
 *
 * @since 11-Jul-17
 */
class ProfileNumberVerifyActivity : AppCompatActivity(), View.OnClickListener {
    private var mActivity: Activity? = null
    private var rL_rootElement: RelativeLayout? = null
    private var eT_first_digit: EditText? = null
    private var eT_second_digit: EditText? = null
    private var eT_third_digit: EditText? = null
    private var eT_fourth_digit: EditText? = null
    private var rL_submit: RelativeLayout? = null
    private var isVerifyButtonVisible = false
    private var tV_sumit: TextView? = null
    private var tV_reSend: TextView? = null
    private var pBar_resend: ProgressBar? = null
    private var pBar_submit: ProgressBar? = null
    private var phoneNumber: String? = null
    private var otpCode: String? = null
    private var mSessionManager: SessionManager? = null
    private var mNotificationMessageDialog: NotificationMessageDialog? = null
    private var isToStartActivity = false
     override fun onCreate(@Nullable savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_number_verification)
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale)
        initVariable()
    }

    /**
     * In this method we used to initialize the all xml and data memeber.
     */
    private fun initVariable() {
        mActivity = this@ProfileNumberVerifyActivity
        isToStartActivity = true
        mNotificationMessageDialog = NotificationMessageDialog(mActivity)
        mSessionManager = SessionManager(mActivity!!)

        // receiving datas from last activity
        val intent: Intent = getIntent()
        phoneNumber = intent.getStringExtra("phoneNumber")
        otpCode = intent.getStringExtra("otpCode")

        // root element
        rL_rootElement = findViewById(R.id.rL_rootElement) as RelativeLayout?

        // mobile number
        val tV_mobile_no = findViewById(R.id.tV_mobile_no) as TextView
        if (phoneNumber != null && !phoneNumber!!.isEmpty()) tV_mobile_no.text = phoneNumber

        // back button
        val rL_back_btn = findViewById(R.id.rL_back_btn) as RelativeLayout
        rL_back_btn.setOnClickListener(this)

        // submit
        tV_sumit = findViewById(R.id.tV_sumit) as TextView?
        rL_submit = findViewById(R.id.rL_submit) as RelativeLayout?
        rL_submit!!.setOnClickListener(this)
        CommonClass.setViewOpacity(mActivity, rL_submit!!, 102, R.drawable.rect_purple_color_with_solid_shape)

        // resend
        tV_reSend = findViewById(R.id.tV_reSend) as TextView?
        tV_reSend!!.setOnClickListener(this)

        // progress bar
        pBar_resend = findViewById(R.id.pBar_resend) as ProgressBar?
        pBar_submit = findViewById(R.id.pBar_submit) as ProgressBar?

        // otp
        eT_first_digit = findViewById(R.id.eT_first_digit) as EditText?
        eT_second_digit = findViewById(R.id.eT_second_digit) as EditText?
        eT_third_digit = findViewById(R.id.eT_third_digit) as EditText?
        eT_fourth_digit = findViewById(R.id.eT_fourth_digit) as EditText?

        // visible submit button
        eT_first_digit!!.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (eT_first_digit!!.text.toString().length == 1) {
                    eT_second_digit!!.requestFocus()
                }

                // set verification button opacity
                if (isToShowVerifyButton) {
                    isVerifyButtonVisible = true
                    CommonClass.setViewOpacity(mActivity, rL_submit!!, 204, R.drawable.rect_purple_color_with_solid_shape)
                } else {
                    isVerifyButtonVisible = false
                    CommonClass.setViewOpacity(mActivity, rL_submit!!, 102, R.drawable.rect_purple_color_with_solid_shape)
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })
        eT_second_digit!!.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (eT_second_digit!!.text.toString().length == 1) {
                    eT_third_digit!!.requestFocus()
                }

                // set verification button opacity
                if (isToShowVerifyButton) {
                    isVerifyButtonVisible = true
                    CommonClass.setViewOpacity(mActivity, rL_submit!!, 204, R.drawable.rect_purple_color_with_solid_shape)
                } else {
                    isVerifyButtonVisible = false
                    CommonClass.setViewOpacity(mActivity, rL_submit!!, 102, R.drawable.rect_purple_color_with_solid_shape)
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })
        eT_third_digit!!.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (eT_third_digit!!.text.toString().length == 1) {
                    eT_fourth_digit!!.requestFocus()
                }

                // set verification button opacity
                if (isToShowVerifyButton) {
                    isVerifyButtonVisible = true
                    CommonClass.setViewOpacity(mActivity, rL_submit!!, 204, R.drawable.rect_purple_color_with_solid_shape)
                } else {
                    isVerifyButtonVisible = false
                    CommonClass.setViewOpacity(mActivity, rL_submit!!, 102, R.drawable.rect_purple_color_with_solid_shape)
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })
        eT_fourth_digit!!.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                // set verification button opacity
                if (isToShowVerifyButton) {
                    isVerifyButtonVisible = true
                    CommonClass.setViewOpacity(mActivity, rL_submit!!, 204, R.drawable.rect_purple_color_with_solid_shape)
                } else {
                    isVerifyButtonVisible = false
                    CommonClass.setViewOpacity(mActivity, rL_submit!!, 102, R.drawable.rect_purple_color_with_solid_shape)
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })
    }

    override fun onResume() {
        super.onResume()
        isToStartActivity = true
        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.REGISTRATION_COMPLETE))

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.PUSH_NOTIFICATION))

        // clear the notification area when the app is opened
        clearNotifications(getApplicationContext())
    }

   override fun onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!)
        super.onPause()
    }

    private val isToShowVerifyButton: Boolean
        private get() = !eT_first_digit!!.text.toString().isEmpty() && !eT_second_digit!!.text.toString().isEmpty() && !eT_third_digit!!.text.toString().isEmpty() && !eT_fourth_digit!!.text.toString().isEmpty()

 override fun onBackPressed() {
        finish()
        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.rL_back_btn -> onBackPressed()
            R.id.tV_reSend -> generateProfileMobOtp()
            R.id.rL_submit -> {
                val enteredCode = eT_first_digit!!.text.toString() + eT_second_digit!!.text.toString() + eT_third_digit!!.text.toString() + eT_fourth_digit!!.text.toString()
                if (isVerifyButtonVisible) {
                    if (enteredCode == otpCode) {
                        updatePhoneNumberApi()
                    } else CommonClass.showTopSnackBar(rL_rootElement, getResources().getString(R.string.invalid_otp_code))
                }
            }
        }
    }

    /**
     * <h>GenerateProfileMobOtp</h>
     *
     *
     * In this method we used to call generate otp api to get new otp
     * when used click on resend button.
     *
     */
    private fun generateProfileMobOtp() {
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            pBar_resend!!.visibility = View.VISIBLE
            tV_reSend!!.visibility = View.GONE
            val request_datas = JSONObject()
            try {
                request_datas.put("token", mSessionManager!!.authToken)
                request_datas.put("phoneNumber", phoneNumber)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            OkHttp3Connection.doOkHttp3Connection(TAG, ApiUrl.OTP_PROFILE_NUMBER, OkHttp3Connection.Request_type.POST, request_datas, object : OkHttp3RequestCallback {
                 override fun onSuccess(result: String?, user_tag: String?) {
                    pBar_resend!!.visibility = View.GONE
                    tV_reSend!!.visibility = View.VISIBLE
                    println("$TAG generate profile re otp res=$result")
                    val profileNumberOtpMain: ProfileNumberOtpMain
                    val gson = Gson()
                    profileNumberOtpMain = gson.fromJson(result, ProfileNumberOtpMain::class.java)
                    when (profileNumberOtpMain.code) {
                        "200" -> otpCode = CommonClass.extractNumberFromString(profileNumberOtpMain.data!!.body)
                        "401" -> CommonClass.sessionExpired(mActivity!!)
                        else -> CommonClass.showSnackbarMessage(rL_rootElement, profileNumberOtpMain.message)
                    }
                }

                 override fun onError(error: String?, user_tag: String?) {
                    pBar_resend!!.visibility = View.GONE
                    tV_reSend!!.visibility = View.VISIBLE
                }
            })
        } else CommonClass.showSnackbarMessage(rL_rootElement, getResources().getString(R.string.NoInternetAccess))
    }

    /**
     * <h>updatePhoneNumberApi</h>
     *
     *
     * In this method we used to call update mobile number api to update
     * new mobile number.
     *
     */
    private fun updatePhoneNumberApi() {
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            pBar_submit!!.visibility = View.VISIBLE
            tV_sumit!!.visibility = View.GONE
            val requestDatas = JSONObject()
            try {
                requestDatas.put("token", mSessionManager!!.authToken)
                requestDatas.put("phoneNumber", phoneNumber)
                requestDatas.put("otp", otpCode)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            OkHttp3Connection.doOkHttp3Connection(TAG, ApiUrl.OTP_PROFILE_NUMBER, OkHttp3Connection.Request_type.PUT, requestDatas, object : OkHttp3RequestCallback {
                 override fun onSuccess(result: String?, user_tag: String?) {
                    pBar_submit!!.visibility = View.GONE
                    tV_sumit!!.visibility = View.VISIBLE
                    println("$TAG update number res=$result")
                    val updatePhoneNoPojo: UpdatePhoneNoPojo
                    val gson = Gson()
                    updatePhoneNoPojo = gson.fromJson(result, UpdatePhoneNoPojo::class.java)
                    when (updatePhoneNoPojo.code) {
                        "200" -> if (isToStartActivity) {
                            isToStartActivity = false
                            startActivity(Intent(mActivity, EditProfileActivity::class.java))
                            EditProfileActivity.editProfileInstance!!.finish()
                            VerifyPhoneNoActivity.verifyNumberInstance!!.finish()
                            mActivity!!.finish()
                        }
                        "401" -> CommonClass.sessionExpired(mActivity!!)
                        else -> CommonClass.showSnackbarMessage(rL_rootElement, updatePhoneNoPojo.message)
                    }
                }

                 override fun onError(error: String?, user_tag: String?) {
                    pBar_submit!!.visibility = View.GONE
                    tV_sumit!!.visibility = View.VISIBLE
                }
            })
        } else CommonClass.showSnackbarMessage(rL_rootElement, getResources().getString(R.string.NoInternetAccess))
    }

    companion object {
        private val TAG = ProfileNumberVerifyActivity::class.java.simpleName
    }
}