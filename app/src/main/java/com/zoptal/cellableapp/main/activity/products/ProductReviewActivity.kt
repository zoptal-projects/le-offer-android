package com.zoptal.cellableapp.main.activity.products

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.IntentFilter
import android.graphics.*
import android.os.Bundle

import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.annotation.Nullable
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager

import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.gson.Gson
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.adapter.ProductReviewRvAdap
import com.zoptal.cellableapp.fcm_push_notification.Config
import com.zoptal.cellableapp.fcm_push_notification.NotificationMessageDialog
import com.zoptal.cellableapp.fcm_push_notification.NotificationUtils.Companion.clearNotifications
import com.zoptal.cellableapp.main.activity.products.ProductReviewActivity
import com.zoptal.cellableapp.pojo_class.add_review_pojo.AddReviewMainPojo
import com.zoptal.cellableapp.pojo_class.product_review.ProductReviewMainPojo
import com.zoptal.cellableapp.pojo_class.product_review.ProductReviewResult
import com.zoptal.cellableapp.utility.*
import com.zoptal.cellableapp.utility.OkHttp3Connection.OkHttp3RequestCallback
import org.json.JSONException
import org.json.JSONObject
import java.util.*

/**
 * <h>ProductReviewActivity</h>
 *
 *
 * This class is called from Product details class. In this class we used to retrive the
 * all reviews of the given product from server. we show that in a list. Apart from that
 * we have option to add our new comment and delete my review by sliding the row from right
 * to left.
 *
 * @since 08-Jun-17
 * @version 1.0
 * @author 3Embed
 */
class ProductReviewActivity : AppCompatActivity(), View.OnClickListener {
    private var mActivity: Activity? = null
    private var mSessionManager: SessionManager? = null
    private var postId: String? = null
    private var rL_rootview: RelativeLayout? = null
    private var rL_noReviews: RelativeLayout? = null
    private var progress_bar: ProgressBar? = null
    private var progress_bar_send: ProgressBar? = null
    private var arrayListReview: ArrayList<ProductReviewResult>? = null
    private var reviewRvAdap: ProductReviewRvAdap? = null
    private var eT_add_review: EditText? = null
    private var rV_reviews: RecyclerView? = null
    private var p: Paint? = null
    private var mApiCall: ApiCall? = null
    private var pageIndex = 0
    private var mSwipeRefreshLayout: SwipeRefreshLayout? = null
    private var linearLayoutManager: LinearLayoutManager? = null
    private var tV_send: TextView? = null

    // load more variables
    private var totalItemCount = 0
    private var totalVisibleItem = 0
    private val visibleThreshold = 5
    private var isLoading = false
    private var mNotificationMessageDialog: NotificationMessageDialog? = null
     override fun onCreate(@Nullable savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_product_review)
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale)
        initVariables()
    }

    /**
     * <h>InitVariables</h>
     *
     *
     * In this method we used to set the xml variable and data member.
     *
     */
    private fun initVariables() {
        // receiving datas from last activity
        val intent: Intent = getIntent()
        postId = intent.getStringExtra("postId")
        val activity: Activity = this@ProductReviewActivity
        mNotificationMessageDialog = NotificationMessageDialog(activity)
        pageIndex = 0
        mActivity = this@ProductReviewActivity
        mApiCall = ApiCall(mActivity!!)
        mSessionManager = SessionManager(mActivity!!)
        p = Paint()

        // initialize xml variables
        mSwipeRefreshLayout = findViewById(R.id.swipe_refresh_layout) as SwipeRefreshLayout?
        mSwipeRefreshLayout!!.setColorSchemeResources(R.color.pink_color)
        rL_rootview = findViewById(R.id.rL_rootview) as RelativeLayout?
        progress_bar = findViewById(R.id.progress_bar) as ProgressBar?
        progress_bar_send = findViewById(R.id.progress_bar_send) as ProgressBar?
        tV_send = findViewById(R.id.tV_send) as TextView?
        rL_noReviews = findViewById(R.id.rL_noReviews) as RelativeLayout?
        eT_add_review = findViewById(R.id.eT_add_review) as EditText?
        val rL_send_message = findViewById(R.id.rL_send_message) as RelativeLayout
        val rL_send = findViewById(R.id.rL_send) as RelativeLayout
        val productMemberName: String = getIntent().getStringExtra("userName")
        if (mSessionManager!!.userName == productMemberName) rL_send.visibility = View.GONE
        rL_send_message.setOnClickListener(this)

        // change send button background color to purple when text is entered
        eT_add_review!!.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (TextUtils.isEmpty(eT_add_review!!.text)) rL_send_message.setBackgroundColor(ContextCompat.getColor(mActivity!!, R.color.sub_heading_color)) else rL_send_message.setBackgroundColor(ContextCompat.getColor(mActivity!!, R.color.purple_color))
            }

            override fun afterTextChanged(s: Editable) {}
        })
        val rL_back_btn = findViewById(R.id.rL_back_btn) as RelativeLayout
        rL_back_btn.setOnClickListener(this)

        // reviews result
        arrayListReview = ArrayList()
        rV_reviews = findViewById(R.id.rV_reviews) as RecyclerView?
        linearLayoutManager = LinearLayoutManager(mActivity)
        rV_reviews!!.setLayoutManager(linearLayoutManager)
        reviewRvAdap = ProductReviewRvAdap(mActivity!!, arrayListReview!!)
        rV_reviews!!.setAdapter(reviewRvAdap)

        // pull to refresh
        mSwipeRefreshLayout!!.setOnRefreshListener(object : SwipeRefreshLayout.OnRefreshListener {
            override fun onRefresh() {
                pageIndex = 0
                arrayListReview!!.clear()
                getPostCommentsApi(pageIndex)
            }
        })

        // initialize swipe method for recycler view
        initItemSwipe()

        // call post comment method to get all comments
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            progress_bar!!.visibility = View.VISIBLE
            getPostCommentsApi(pageIndex)
        } else CommonClass.showSnackbarMessage(rL_rootview, getResources().getString(R.string.NoInternetAccess))
    }

    override fun onResume() {
        super.onResume()

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.REGISTRATION_COMPLETE))

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.PUSH_NOTIFICATION))

        // clear the notification area when the app is opened
        clearNotifications(getApplicationContext())
    }

   override fun onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!)
        super.onPause()
    }

    /**
     * <h>initItemSwipe</h>
     *
     *
     * In this method we used to do swiping of particular item of recyclerview
     * from right to left direction for the purpose of deleting that rows.
     *
     */
    private fun initItemSwipe() {
        val simpleItemTouchCallback: ItemTouchHelper.SimpleCallback = object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {
              return false
            }


          override  fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val position: Int = viewHolder.getAdapterPosition()
                val itemUserId = arrayListReview!![position]!!.userId
                val userId = mSessionManager!!.userId
                if (direction == ItemTouchHelper.LEFT && userId == itemUserId) {
                    //adapter.removeItem(position);
                    val builder: AlertDialog.Builder = AlertDialog.Builder(mActivity!!) //alert for confirm to delete
                    builder.setCancelable(false)
                    builder.setMessage("Are you sure to delete?") //set message
                    builder.setPositiveButton("REMOVE", DialogInterface.OnClickListener { dialog, which ->

                        //when click on DELETE
                        mApiCall!!.deletePostComment(arrayListReview!![position]!!.commentId, postId)
                        reviewRvAdap!!.removeItem(position, rL_noReviews!!)
                    }).setNegativeButton("CANCEL", DialogInterface.OnClickListener { dialog, which ->

                        //not removing items if cancel is done
                        reviewRvAdap!!.notifyItemRemoved(position + 1) //notifies the RecyclerView Adapter that data in adapter has been removed at a particular position.
                        reviewRvAdap!!.notifyItemRangeChanged(position, reviewRvAdap!!.itemCount) //notifies the RecyclerView Adapter that positions of element in adapter has been changed from position(removed element index to end of list), please update it.
                        //return;
                    }).show() //show alert dialog
                } else {
                    reviewRvAdap!!.notifyItemRemoved(position + 1) //notifies the RecyclerView Adapter that data in adapter has been removed at a particular position.
                    reviewRvAdap!!.notifyItemRangeChanged(position, reviewRvAdap!!.itemCount) //notifies the RecyclerView Adapter that positions of element in adapter has been changed from position(removed element index to end of list), please update it.
                }
            }

            override fun getSwipeDirs(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder): Int {
                val position: Int = viewHolder.getAdapterPosition()
                val itemUserId = arrayListReview!![position]!!.userId
                val userId = mSessionManager!!.userId
                return if (userId != itemUserId) 0 else super.getSwipeDirs(recyclerView, viewHolder)
            }

            override fun onChildDraw(c: Canvas, recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, dX: Float, dY: Float, actionState: Int, isCurrentlyActive: Boolean) {
                val icon: Bitmap
                if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
                    val itemView: View = viewHolder.itemView
                    val height = itemView.bottom.toFloat() - itemView.top.toFloat()
                    val width = height / 3
                    if (dX > 0) {
                        p!!.color = ContextCompat.getColor(mActivity!!, R.color.red_color)
                        val background = RectF(itemView.left.toFloat(), itemView.top.toFloat(), dX, itemView.bottom.toFloat())
                        c.drawRect(background, p!!)
                        icon = BitmapFactory.decodeResource(getResources(), R.drawable.white_color_cross_icon)
                        val icon_dest = RectF(itemView.left.toFloat() + width, itemView.top.toFloat() + width, itemView.left.toFloat() + 2 * width, itemView.bottom.toFloat() - width)
                        c.drawBitmap(icon, null, icon_dest, p)
                    } else {
                        p!!.color = Color.parseColor("#D32F2F")
                        val background = RectF(itemView.right.toFloat() + dX, itemView.top.toFloat(), itemView.right.toFloat(), itemView.bottom.toFloat())
                        c.drawRect(background, p!!)
                        icon = BitmapFactory.decodeResource(getResources(), R.drawable.white_color_cross_icon)
                        val icon_dest = RectF(itemView.right.toFloat() - 2 * width, itemView.top.toFloat() + width, itemView.right.toFloat() - width, itemView.bottom.toFloat() - width)
                        c.drawBitmap(icon, null, icon_dest, p)
                    }
                }
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
            }
        }
        val itemTouchHelper = ItemTouchHelper(simpleItemTouchCallback)
        itemTouchHelper.attachToRecyclerView(rV_reviews)
    }

    /**
     * <h>GetPostCommentsApi</h>
     *
     *
     * In this method we used to do getPostComments api to get all comments of the product.
     *
     */
    private fun getPostCommentsApi(offset: Int) {
        var offset = offset
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            val limit = 20
            offset = offset * limit
            val requestDatas = JSONObject()
            try {
                requestDatas.put("token", mSessionManager!!.authToken)
                requestDatas.put("postId", postId)
                requestDatas.put("limit", limit)
                requestDatas.put("offset", offset)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            OkHttp3Connection.doOkHttp3Connection(TAG, ApiUrl.GET_POST_COMMENTS, OkHttp3Connection.Request_type.POST, requestDatas, object : OkHttp3RequestCallback {
                 override fun onSuccess(result: String?, user_tag: String?) {
                    mSwipeRefreshLayout!!.setRefreshing(false)
                    progress_bar!!.visibility = View.GONE
                    println("$TAG get post comment res=$result")
                    val productReviewMainPojo: ProductReviewMainPojo
                    val gson = Gson()
                    productReviewMainPojo = gson.fromJson(result, ProductReviewMainPojo::class.java)
                    when (productReviewMainPojo.code) {
                        "200" -> {
                            mSwipeRefreshLayout!!.setRefreshing(false)
                            arrayListReview!!.addAll(productReviewMainPojo.result!!)
                            Collections.reverse(arrayListReview)
                            if (arrayListReview != null && arrayListReview!!.size > 0) {
                                rL_noReviews!!.visibility = View.GONE
                                if (arrayListReview!!.size < 15) {
                                    isLoading = true
                                }
                                reviewRvAdap!!.notifyDataSetChanged()

                                // Load more
                                rV_reviews!!.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                                   override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                                        super.onScrolled(recyclerView, dx, dy)
                                        totalItemCount = linearLayoutManager!!.getItemCount()
                                        totalVisibleItem = linearLayoutManager!!.findLastVisibleItemPosition()
                                        if (!isLoading && totalItemCount <= visibleThreshold + totalVisibleItem) {
                                            println("$TAG on load more called")
                                            mSwipeRefreshLayout!!.setRefreshing(true)
                                            isLoading = true
                                            pageIndex = pageIndex + 1
                                            getPostCommentsApi(pageIndex)
                                        }
                                    }
                                })
                            } else rL_noReviews!!.visibility = View.VISIBLE
                        }
                        "401" -> CommonClass.sessionExpired(mActivity!!)
                        else -> {
                            progress_bar!!.visibility = View.GONE
                            if (arrayListReview!!.size == 0) rL_noReviews!!.visibility = View.VISIBLE
                            println(TAG + " " + "get post comment error=" + productReviewMainPojo.message)
                        }
                    }
                }

                 override fun onError(error: String?, user_tag: String?) {
                    mSwipeRefreshLayout!!.setRefreshing(false)
                    progress_bar!!.visibility = View.GONE
                }
            })
        } else CommonClass.showSnackbarMessage(rL_rootview, getResources().getString(R.string.NoInternetAccess))
    }//2014/08/06 15:59:48

    // to get current time in long value
    private val currentDateTime: String
        private get() {
            val date = Date()
            return date.time.toString() + "" //2014/08/06 15:59:48
        }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.rL_back_btn -> onBackPressed()
            R.id.rL_send_message -> addReviewComment(eT_add_review!!.text.toString(), postId)
        }
    }

    /**
     * <h>AddReviewComment</h>
     *
     *
     * In this method we used to do api call add review for particular product.
     *
     * @param message The message body to add as review
     * @param postId The post id of a product
     */
    fun addReviewComment(message: String?, postId: String?) {
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            progress_bar_send!!.visibility = View.VISIBLE
            tV_send!!.visibility = View.GONE
            val request_datas = JSONObject()
            try {
                request_datas.put("token", mSessionManager!!.authToken)
                request_datas.put("comment", message)
                request_datas.put("postId", postId)
                request_datas.put("type", "0")
                request_datas.put("hashTags", "")
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            OkHttp3Connection.doOkHttp3Connection(TAG, ApiUrl.ADD_COMMENTS, OkHttp3Connection.Request_type.POST, request_datas, object : OkHttp3RequestCallback {
                 override fun onSuccess(result: String?, user_tag: String?) {
                    println("$TAG add review res=$result")
                    progress_bar_send!!.visibility = View.GONE
                    tV_send!!.visibility = View.VISIBLE
                    val addReviewMainPojo: AddReviewMainPojo
                    val gson = Gson()
                    addReviewMainPojo = gson.fromJson(result, AddReviewMainPojo::class.java)
                    when (addReviewMainPojo.code) {
                        "200" -> {
                            val arrayListReviewData = addReviewMainPojo.data
                            if (arrayListReviewData != null && arrayListReviewData.size > 0) {
                                val arrayListCommentData = arrayListReviewData[0].commentData
                                if (arrayListCommentData != null && arrayListCommentData.size > 0) {
                                    rL_noReviews!!.visibility = View.GONE
                                    val commentId = arrayListCommentData[0].commentId
                                    println("$TAG commentId=$commentId")
                                    val productReviewResult = ProductReviewResult()
                                    productReviewResult.commentBody = arrayListCommentData[0].commentBody
                                    productReviewResult.username = mSessionManager!!.userName!!
                                    productReviewResult.userId = mSessionManager!!.userId!!
                                    productReviewResult.commentId = commentId
                                    productReviewResult.profilePicUrl = mSessionManager!!.userImage!!
                                    productReviewResult.commentedOn = currentDateTime
                                    eT_add_review!!.setText("")
                                    arrayListReview!!.add(productReviewResult)
                                    rV_reviews!!.smoothScrollToPosition(arrayListReview!!.size)
                                    reviewRvAdap!!.notifyDataSetChanged()
                                }
                            }
                        }
                        "401" -> CommonClass.sessionExpired(mActivity!!)
                    }
                }

                 override fun onError(error: String?, user_tag: String?) {
                    progress_bar_send!!.visibility = View.GONE
                    tV_send!!.visibility = View.VISIBLE
                    println("$TAG add review error=$error")
                }
            })
        }
    }

 override fun onBackPressed() {
        try {
            if (arrayListReview != null) {
                val intent = Intent()
                intent.putExtra("count", arrayListReview!!.size.toString() + "")
                setResult(VariableConstants.REVIEW_COUNT_UPDATE, intent)
            }
            finish()
            overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(getCurrentFocus()!!.getWindowToken(), 0)
        return true
    } /*    @Override
    public boolean dispatchTouchEvent(final MotionEvent ev) {
        // all touch events close the keyboard before they are processed except EditText instances.
        // if focus is an EditText we need to check, if the touchevent was inside the focus editTexts
        final View currentFocus = getCurrentFocus();
        if (!(currentFocus instanceof EditText) || !isTouchInsideView(ev, currentFocus)) {
            ((InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE))
                    .hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
        return super.dispatchTouchEvent(ev);
    }

    */

    /**
     * determine if the given motionevent is inside the given view.
     *
     * @param ev
     * the given view
     * @param currentFocus
     * the motion event.
     * @return if the given motionevent is inside the given view
     */
    /*
    private boolean isTouchInsideView(final MotionEvent ev, final View currentFocus) {
        final int[] loc = new int[2];
        currentFocus.getLocationOnScreen(loc);
        return ev.getRawX() > loc[0] && ev.getRawY() > loc[1] && ev.getRawX() < (loc[0] + currentFocus.getWidth())
                && ev.getRawY() < (loc[1] + currentFocus.getHeight());
    }*/
    companion object {
        private val TAG = ProductReviewActivity::class.java.simpleName
    }
}