package com.zoptal.cellableapp.main.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.annotation.Nullable
import com.zoptal.cellableapp.R

/**
 * <h>WebViewActivity</h>
 *
 *
 * In this class we used to open the browser using webview from our custom activity.
 *
 * @since 21-Jul-17
 */
class WebViewActivity : AppCompatActivity(), View.OnClickListener {
    private var mWebView: WebView? = null
    private var mProgressBar: ProgressBar? = null
     override fun onCreate(@Nullable savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_get_paypal)
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale)
        val mActivity: Activity = this@WebViewActivity
        mWebView = findViewById(R.id.webView) as WebView?
        mProgressBar = findViewById(R.id.progress_bar) as ProgressBar?

        // receiving datas from last activity
        val intent: Intent = getIntent()
        val url = intent.getStringExtra("browserLink")
        val actionBarTitle = intent.getStringExtra("actionBarTitle")

        // action bar title
        val tV_actionBarTitle = findViewById(R.id.tV_actionBarTitle) as TextView
        if (actionBarTitle != null) tV_actionBarTitle.text = actionBarTitle
        val rL_back_btn = findViewById(R.id.rL_back_btn) as RelativeLayout
        rL_back_btn.setOnClickListener(this)
        if (url != null && !url.isEmpty()) startWebView(url)
    }

    private fun startWebView(url: String) {

        //Create new webview Client to show progress dialog
        //When opening a url or click on link
        mWebView!!.webViewClient = object : WebViewClient() {
            //If you will not use this method url links are opeen in new brower not in webview
            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                view.loadUrl(url)
                return true
            }

            //Show loader on url load
            override fun onLoadResource(view: WebView, url: String) {
                mProgressBar!!.visibility = View.VISIBLE
            }

            override fun onPageFinished(view: WebView, url: String) {
                mProgressBar!!.visibility = View.GONE
            }
        }

        // Javascript inabled on webview
        mWebView!!.settings.javaScriptEnabled = true

        //Load url in webview
        mWebView!!.loadUrl(url)
    }

 override fun onBackPressed() {
        finish()
        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.rL_back_btn -> onBackPressed()
        }
    }
}