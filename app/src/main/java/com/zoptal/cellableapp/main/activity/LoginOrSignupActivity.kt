package com.zoptal.cellableapp.main.activity

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.os.Environment

import android.telephony.TelephonyManager
import android.text.*
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.util.Log
import android.view.KeyEvent
import android.view.MotionEvent
import android.view.View
import android.view.Window
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import com.zoptal.cellableapp.BuildConfig
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.county_code_picker.Country
import com.zoptal.cellableapp.county_code_picker.DialogCountryList
import com.zoptal.cellableapp.county_code_picker.SetCountryCodeListener
import com.zoptal.cellableapp.device_camera.HandleCameraEvents
import com.zoptal.cellableapp.get_current_location.FusedLocationReceiver
import com.zoptal.cellableapp.get_current_location.FusedLocationService
import com.zoptal.cellableapp.main.activity.ForgotPasswordActivity
import com.zoptal.cellableapp.main.activity.LoginOrSignupActivity
import com.zoptal.cellableapp.mqttchat.AppController
import com.zoptal.cellableapp.mqttchat.ImageCropper.CropImage
import com.zoptal.cellableapp.pojo_class.*
import com.zoptal.cellableapp.pojo_class.cloudinary_details_pojo.Cloudinary_Details_reponse
import com.zoptal.cellableapp.pojo_class.phone_otp_pojo.PhoneOtpMainPojo
import com.zoptal.cellableapp.pojo_class.sign_up_pojo.SignUpMainPojo
import com.zoptal.cellableapp.utility.*
import com.zoptal.cellableapp.utility.OkHttp3Connection.OkHttp3RequestCallback
import org.json.JSONException
import org.json.JSONObject
import java.io.File
import java.io.FileOutputStream
import java.io.InputStream
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


/**
 * <h>LoginOrSignupActivity</h>
 *
 *
 * This activity class has been called from LandingActivity class.
 * In this class two button(Login and signup) is there on the top
 * of the screen.
 *
 * @since 15-May-17
 */
class LoginOrSignupActivity : AppCompatActivity(), View.OnClickListener {
    private var mActivity: Activity? = null
    private var login_views: View? = null
    private var signup_views: View? = null
    private var runTimePermission: RunTimePermission? = null
    private var permissionsArray: Array<String>? = null
    private var mSessionManager: SessionManager? = null
    private var rL_rootElement: RelativeLayout? = null
    private var isPictureTaken = false
    private var isFromLocation = false
    private var locationService: FusedLocationService? = null
    private var signUpType: String? = ""
    private var googleToken: String? = ""
    private var googleId: String? = ""
    private var fbId: String? = null
    private var type: String? = ""
    private var google_userImageUrl: String? = ""
    private var fb_userImageUrl: String? = ""
    private var fb_accessToken: String? = ""
    private var currentLat: String? = ""
    private var currentLng: String? = ""
    private var address: String? = ""
    private var otpCode: String? = ""
    private var city: String? = ""
    private var countryShortName: String? = ""
    private var isUserRegistered = false
    private var isPhoneRegistered = false
    private var isEmailRegistered = false
    private var isToStartActivity = false

    // Login xml variables
    private var eT_loginUserName: EditText? = null
    private var eT_loginPassword: EditText? = null
    private var rL_do_login: RelativeLayout? = null
    private var rL_signup: RelativeLayout? = null
    private var iV_login_userName_error: ImageView? = null
    private var iV_login_password_error: ImageView? = null
    private var profilePicUrl: String? = ""

    // signup xml variables
    private var eT_userName: EditText? = null
    private var eT_fullName: EditText? = null
    private var eT_password: EditText? = null
    private var eT_mobileNo: EditText? = null
    private var eT_emailId: EditText? = null
    private var isLoginButtonEnabled = false
    private var isSignUpButtonEnabled = false
    private var isAskLocationPermission = false
    private var progress_bar_login: ProgressBar? = null
    private var progress_bar_signup: ProgressBar? = null
    private var progress_bar_ph: ProgressBar? = null
    private var pBar_userName: ProgressBar? = null
    private var pBar_email: ProgressBar? = null
    private var tV_do_login: TextView? = null
    private var tV_signup: TextView? = null
    private var tV_by_signing_up: TextView? = null
    private var iV_profile_pic: ImageView? = null
    private var iV_userName_error: ImageView? = null
    private var iV_password_error: ImageView? = null
    private var iV_error_ph: ImageView? = null
    private var iV_error_email: ImageView? = null
    private var arrayListCountry: ArrayList<Country>? = null
    private var dialogCountryList: DialogCountryList? = null
    private var tV_country_iso_no: TextView? = null
    private var tV_country_code: TextView? = null
    private var iV_full_name: ImageView? = null
    private var iV_user_name: ImageView? = null
    private var iV_password: ImageView? = null
    private var iV_phone_icon: ImageView? = null
    private var iV_email: ImageView? = null
    private var iv_edit_icon: ImageView? = null
    private var checkboxSignUp: CheckBox? = null
    private var countryIsoNumber: String? = ""
    private var mFile: File? = null
    private var mHandleCameraEvents: HandleCameraEvents? = null
   override   fun onCreate(@Nullable savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_login_signup)
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale)
        mActivity = this@LoginOrSignupActivity
        isToStartActivity = true
        mSessionManager = SessionManager(mActivity!!)
        permissionsArray = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
        runTimePermission = RunTimePermission(mActivity!!, permissionsArray!!, false)
        if (!runTimePermission!!.checkPermissions(permissionsArray!!)) {
            isAskLocationPermission = true
            runTimePermission!!.requestPermission()
        }
        currentLat = mSessionManager!!.currentLat
        currentLng = mSessionManager!!.currentLng
        if (isLocationFound(currentLat, currentLng)) {
            address = CommonClass.getCompleteAddressString(mActivity, currentLat!!.toDouble(), currentLng!!.toDouble())
            city = CommonClass.getCityName(mActivity!!, currentLat!!.toDouble(), currentLng!!.toDouble())
            countryShortName = CommonClass.getCountryCode(mActivity!!, currentLat!!.toDouble(), currentLng!!.toDouble())
        }

        // hide status bar
        CommonClass.statusBarColor(mActivity!!)
        initVariables()
    }

    private fun initVariables() {
        isPictureTaken = false
        arrayListCountry = ArrayList()
        rL_rootElement = findViewById(R.id.rL_rootElement) as RelativeLayout?
        login_views = findViewById(R.id.login_views)
        signup_views = findViewById(R.id.signup_views)
        val radioGroup = findViewById(R.id.radioGroup) as RadioGroup?
        val radio_login: RadioButton?
        val radio_signup: RadioButton?
        radio_login = findViewById(R.id.radio_login) as RadioButton?
        radio_signup = findViewById(R.id.radio_signup) as RadioButton?

        // receiving flag value from last class
        val intent: Intent = getIntent()
        type = intent.getStringExtra("type")
        //        fromLandingAccpet=intent.getBooleanExtra("fromLandingAccept",false);
        println("$TAG type=$type")

        // initialize sigup xml variables
        eT_userName = signup_views!!.findViewById<View?>(R.id.eT_userName) as EditText
        eT_fullName = signup_views!!.findViewById<View?>(R.id.eT_fullName) as EditText
        eT_fullName!!.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_FLAG_CAP_WORDS
        eT_emailId = signup_views!!.findViewById<View?>(R.id.eT_emailId) as EditText
        iV_profile_pic = signup_views!!.findViewById<View?>(R.id.iV_profile_pic) as ImageView
        iV_profile_pic!!.layoutParams.width = CommonClass.getDeviceWidth(mActivity!!) / 4
        iV_profile_pic!!.layoutParams.height = CommonClass.getDeviceWidth(mActivity!!) / 4
        iV_profile_pic!!.setOnClickListener(this)
        progress_bar_ph = signup_views!!.findViewById<View?>(R.id.pBar_ph) as ProgressBar
        progress_bar_ph!!.visibility = View.GONE
        pBar_userName = findViewById(R.id.pBar_userName) as ProgressBar?
        pBar_userName!!.visibility = View.GONE
        pBar_email = findViewById(R.id.pBar_email) as ProgressBar?
        pBar_email!!.visibility = View.GONE

        // sign up icons
        iV_full_name = signup_views!!.findViewById<View?>(R.id.iV_full_name) as ImageView
        iV_user_name = signup_views!!.findViewById<View?>(R.id.iV_user_name) as ImageView
        iV_password = signup_views!!.findViewById<View?>(R.id.iV_password) as ImageView
        iV_phone_icon = signup_views!!.findViewById<View?>(R.id.iV_phone_icon) as ImageView
        iV_email = signup_views!!.findViewById<View?>(R.id.iV_email) as ImageView
        iv_edit_icon = signup_views!!.findViewById<View?>(R.id.iv_edit_icon) as ImageView
        iv_edit_icon!!.visibility = View.GONE

        // sign up error icon
        iV_userName_error = signup_views!!.findViewById<View?>(R.id.iV_userName_error) as ImageView
        iV_password_error = signup_views!!.findViewById<View?>(R.id.iV_password_error) as ImageView
        iV_error_ph = signup_views!!.findViewById<View?>(R.id.iV_error_ph) as ImageView
        iV_error_email = signup_views!!.findViewById<View?>(R.id.iV_error_email) as ImageView
        when (type) {
            "login" -> {
                radio_login!!.isChecked = true
                radio_signup!!.isChecked = false
                login_views!!.visibility = View.VISIBLE
                signup_views!!.visibility = View.GONE
            }
            "normalSignup" -> {
                if (mSessionManager!!.signUpPrivacyAccept) {
                    if (type == "login") type = "normalSignup"
                    login_views!!.visibility = View.GONE
                    signup_views!!.visibility = View.VISIBLE
                } else {
                    val intent1 = Intent(mActivity, PrivacyPolicyActivity::class.java)
                    startActivityForResult(intent1, VariableConstants.PRIVACY_POLICY)
                }
                radio_login!!.isChecked = false
                radio_signup!!.isChecked = true
                login_views!!.visibility = View.GONE
                signup_views!!.visibility = View.VISIBLE
            }
            "googleSignUp" -> {
                radio_login!!.isChecked = false
                radio_signup!!.isChecked = true
                login_views!!.visibility = View.GONE
                signup_views!!.visibility = View.VISIBLE

                // Google login
                val userFullName: String?
                val email: String?
                userFullName = intent.getStringExtra("userFullName")
                google_userImageUrl = intent.getStringExtra("userImageUrl")
                email = intent.getStringExtra("email")
                googleId = intent.getStringExtra("id")
                googleToken = intent.getStringExtra("serverAuthCode")

                // set profile pic
                setSignUpXmlVar(google_userImageUrl, userFullName, email)
                signUpType = "3"
            }
            "fbSignUp" -> {
                radio_login!!.isChecked = false
                radio_signup!!.isChecked = true
                login_views!!.visibility = View.GONE
                signup_views!!.visibility = View.VISIBLE

                // facebook login
                val fbUserFullName: String?
                val fbEmail: String?
                val fbUserName: String?
                fbUserFullName = intent.getStringExtra("userFullName")
                fb_userImageUrl = intent.getStringExtra("userImageUrl")
                fbEmail = intent.getStringExtra("email")
                fbId = intent.getStringExtra("id")
                fbUserName = intent.getStringExtra("userName")
                fb_accessToken = intent.getStringExtra("fb_accessToken")

                // set values
                setSignUpXmlVar(fb_userImageUrl, fbUserFullName, fbEmail)
                signUpType = "1"
                Log.e(TAG, "Facebook Name: " + fbUserFullName + ", email: " + fbEmail
                        + ", Image: " + fb_userImageUrl + ", fb id: " + fbId + ", user name: " + fbUserName)
            }
        }

        // Here we set login or sign up views visibility
        radioGroup!!.setOnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                R.id.radio_login -> {
                    type = "login"
                    login_views!!.visibility = View.VISIBLE
                    signup_views!!.visibility = View.GONE
                }
                R.id.radio_signup -> if (mSessionManager!!.signUpPrivacyAccept) {
                    if (type == "login") type = "normalSignup"
                    login_views!!.visibility = View.GONE
                    signup_views!!.visibility = View.VISIBLE
                } else {
                    /* if(fromLandingAccpet) {
                                if (type.equals("login"))
                                    type = "normalSignup";
                                login_views.setVisibility(View.GONE);
                                signup_views.setVisibility(View.VISIBLE);*/
                    /*} else {*/
                    val intent = Intent(mActivity, PrivacyPolicyActivity::class.java)
                    startActivityForResult(intent, VariableConstants.PRIVACY_POLICY)
                    //}
                }
            }
        }

        // Back button
        val rL_back_btn = findViewById(R.id.rL_back_btn) as RelativeLayout?
        rL_back_btn!!.setOnClickListener(this)

        // Login button
        iV_login_userName_error = login_views!!.findViewById<View?>(R.id.iV_login_userName_error) as ImageView
        iV_login_password_error = login_views!!.findViewById<View?>(R.id.iV_login_password_error) as ImageView
        rL_do_login = login_views!!.findViewById<View?>(R.id.rL_do_login) as RelativeLayout
        rL_do_login!!.setOnClickListener(this)

        // login progress bar
        progress_bar_login = login_views!!.findViewById<View?>(R.id.progress_bar_login) as ProgressBar
        tV_do_login = login_views!!.findViewById<View?>(R.id.tV_do_login) as TextView

        // Forgot password
        val rL_forgot_password = findViewById(R.id.rL_forgot_password) as RelativeLayout?
        rL_forgot_password!!.setOnClickListener(this)
        loginButtonValidation()

        //////////////// sign up ////////////////////
        tV_country_iso_no = signup_views!!.findViewById<View?>(R.id.tV_country_iso_no) as TextView
        tV_country_code = signup_views!!.findViewById<View?>(R.id.tV_country_code) as TextView
        progress_bar_signup = signup_views!!.findViewById<View?>(R.id.progress_bar_signup) as ProgressBar
        progress_bar_signup!!.visibility = View.GONE
        rL_signup = signup_views!!.findViewById<View?>(R.id.rL_signup) as RelativeLayout
        tV_signup = signup_views!!.findViewById<View?>(R.id.tV_signup) as TextView
        tV_by_signing_up = signup_views!!.findViewById<View?>(R.id.tV_by_signing_up) as TextView
        checkboxSignUp = signup_views!!.findViewById<View?>(R.id.checkboxSignUp) as CheckBox
        val rL_createAcc = signup_views!!.findViewById<View?>(R.id.rL_createAcc) as RelativeLayout
        rL_createAcc.setOnClickListener(this)
        signUpButtonValidation()

        // call method to get all country iso code
        countryCodeList
        val rL_country_picker = signup_views!!.findViewById<View?>(R.id.rL_country_picker) as RelativeLayout
        rL_country_picker.setOnClickListener(this)
        setTermsNconditions()
        val state = Environment.getExternalStorageState()
        if (Environment.MEDIA_MOUNTED == state) {
            mFile = File(Environment.getExternalStorageDirectory(), VariableConstants.TEMP_PHOTO_FILE_NAME)
        } else {
            mFile = File(getFilesDir(), VariableConstants.TEMP_PHOTO_FILE_NAME)
        }
        mHandleCameraEvents = HandleCameraEvents(mActivity!!, mFile!!)
    }

    override  fun onResume() {
        super.onResume()
        isToStartActivity = true
    }

    /**
     * <h>SetSignUpXmlVar</h>
     *
     *
     * In this method we used to set the xml datas which we get
     * from the last activity like LandingACtivity class.
     *
     * @param imageUrl the profile image url
     * @param fullName The user full name
     * @param emailId The user eamil-Id
     */
    private fun setSignUpXmlVar(imageUrl: String?, fullName: String?, emailId: String?) {
        // set profile pic
        if (imageUrl != null && !imageUrl.isEmpty()) {
            println("$TAG fb_userImageUrl=$fb_userImageUrl")
            Picasso.with(mActivity)
                    .load(imageUrl)
                    .transform(CircleTransform())
                    .placeholder(R.drawable.add_photo)
                    .error(R.drawable.add_photo)
                    .into(iV_profile_pic)
        }

        // set full name
        if (fullName != null && !fullName.isEmpty()) {
            iV_full_name!!.setImageResource(R.drawable.name_on)
            eT_fullName!!.setText(fullName)
            eT_userName!!.requestFocus()
        }

        // email
        if (emailId != null && !emailId.isEmpty()) {
            iV_email!!.setImageResource(R.drawable.email_on)
            eT_emailId!!.setText(emailId)
            // email address validation api
            emailCheckApi()
        }
    }

    /**
     * <h>SetTermsNconditions</h>
     *
     *
     * In this method we used to set the terms and condition & privacy policy text at
     * bottom of the screen along with checkbox.
     *
     */
    private fun setTermsNconditions() {
        val terms: String
        val privacy: String
        val s1: String = getResources().getString(R.string.by_signing_up)
        val s2: String = getResources().getString(R.string.termscondition)
        val s3: String = getResources().getString(R.string.and)
        val s4: String = getResources().getString(R.string.privacyPolicy)
        terms = getResources().getString(R.string.termsNconditionsUrl)
        privacy = getResources().getString(R.string.privacyPolicyUrl)
        val privacyTitle: String = getResources().getString(R.string.privacyPolicy)
        val termsConditions: String = getResources().getString(R.string.termsNcondition)
        val message = "$s1 $s2 $s3 $s4"
        val spannableString = SpannableString(message)
        spannableString.setSpan(MyClickableSpan(true, terms, termsConditions), s1.length + 1, s1.length + s2.length + 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        spannableString.setSpan(MyClickableSpan(true, privacy, privacyTitle), s1.length + 1 + s2.length + 1 + s3.length + 1, s1.length + 1 + s2.length + 1 + s3.length + 1 + s4.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        tV_by_signing_up!!.text = spannableString
        tV_by_signing_up!!.movementMethod = LinkMovementMethod.getInstance()
        tV_by_signing_up!!.highlightColor = Color.WHITE
    }

    /**
     * <h>MyClickableSpan</h>
     *
     *
     * In this method we used to set the text clickable part by part seperately
     * like "Terms and Conditions" seperate and "Privacy Policy" seperate.
     *
     */
    private inner class MyClickableSpan internal constructor(private val isUnderLine: Boolean, private val setUrl: String?, private val title: String?) : ClickableSpan() {

        override fun onClick(widget: View) {
            val privacyIntent = Intent(mActivity, PrivacyAndTermsActivity::class.java)
            privacyIntent.putExtra("url", setUrl)
            privacyIntent.putExtra("title", title)
            startActivity(privacyIntent)
        }

        override fun updateDrawState(ds: TextPaint) {
            super.updateDrawState(ds!!)
            ds.isUnderlineText = isUnderLine
            ds.color = ContextCompat.getColor(mActivity!!, R.color.white)
            ds.isFakeBoldText = false
        }

    }

    /**
     * <h>loginButtonValidation</h>
     *
     *
     * In this method we used to show login button more visible and clickable
     * when all the mandatory fields are filled.
     *
     */
    private fun loginButtonValidation() {
        // set Opacity to login button
        CommonClass.setViewOpacity(mActivity, rL_do_login!!, 102, R.drawable.rect_purple_color_with_solid_shape)

        // user name validation
        eT_loginUserName = login_views!!.findViewById<View?>(R.id.eT_loginUserName) as EditText
        eT_loginUserName!!.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                //..user enter spcce then remove
                if (eT_loginUserName!!.text.toString().contains(" ")) {
                    eT_loginUserName!!.setText(eT_loginUserName!!.text.toString().replace(" ", ""))
                    eT_loginUserName!!.setSelection(eT_loginUserName!!.text.toString().length)
                }
                if (isLoginParamFilled) {
                    isLoginButtonEnabled = true
                    // set Opacity to login button
                    CommonClass.setViewOpacity(mActivity, rL_do_login!!, 204, R.drawable.rect_purple_color_with_solid_shape)
                } else {
                    isLoginButtonEnabled = false
                    // set Opacity to login button
                    CommonClass.setViewOpacity(mActivity, rL_do_login!!, 102, R.drawable.rect_purple_color_with_solid_shape)
                }
            }

            override fun afterTextChanged(s: Editable?) {}
        })

        // Password
        eT_loginPassword = findViewById(R.id.eT_loginPassword) as EditText?
        eT_loginPassword!!.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (isLoginParamFilled) {
                    isLoginButtonEnabled = true
                    CommonClass.setViewOpacity(mActivity, rL_do_login!!, 204, R.drawable.rect_purple_color_with_solid_shape)
                } else {
                    isLoginButtonEnabled = false
                    CommonClass.setViewOpacity(mActivity, rL_do_login!!, 102, R.drawable.rect_purple_color_with_solid_shape)
                }
            }

            override fun afterTextChanged(s: Editable?) {}
        })

        // Login Edit text next click event
        eT_loginUserName!!.setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_NEXT) {
                if (eT_loginUserName!!.text.toString().isEmpty()) {
                    iV_login_userName_error!!.visibility = View.VISIBLE
                    iV_login_userName_error!!.setImageResource(R.drawable.error_icon)
                    return@OnEditorActionListener true
                } else {
                    iV_login_userName_error!!.visibility = View.VISIBLE
                    iV_login_userName_error!!.setImageResource(R.drawable.rightusername)
                    return@OnEditorActionListener false
                }
            }
            false
        })

        // password
        eT_loginPassword!!.setOnKeyListener(View.OnKeyListener { v, keyCode, event -> // If the event is a key-down event on the "enter" button
            if (event!!.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                // Perform action on key press
                if (eT_loginPassword!!.text.toString().isEmpty()) {
                    iV_login_password_error!!.visibility = View.VISIBLE
                    iV_login_password_error!!.setImageResource(R.drawable.error_icon)
                } else {
                    iV_login_password_error!!.visibility = View.VISIBLE
                    iV_login_password_error!!.setImageResource(R.drawable.rightusername)
                }

                // if all the mandatory are filled then do login api call
                if (isLoginButtonEnabled) {
                    showKeyboard(InputMethodManager.HIDE_IMPLICIT_ONLY)
                    isFromLocation = true
                    permissionsArray = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
                    runTimePermission = RunTimePermission(mActivity!!, permissionsArray!!, false)
                    val lm = mActivity!!.getSystemService(Context.LOCATION_SERVICE) as LocationManager
                    val isLocationEnabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER)
                    println(TAG + " " + "is location enabled=" + isLocationEnabled + " " + "is permission allowed=" + runTimePermission!!.checkPermissions(permissionsArray!!))
                    progress_bar_login!!.visibility = View.VISIBLE
                    tV_do_login!!.visibility = View.GONE
                    if (isLocationEnabled && runTimePermission!!.checkPermissions(permissionsArray!!)) currentLocation else loginRequestApi()
                }
                return@OnKeyListener true
            }
            false
        })
    }

    /**
     * <h>loginButtonValidation</h>
     *
     *
     * In this method we used to show sign up button more visible and clickable
     * when all the mandatory fields are filled.
     *
     */
    private fun signUpButtonValidation() {
        // set Opacity to login button
        CommonClass.setViewOpacity(mActivity, rL_signup!!, 102, R.drawable.rect_purple_color_with_solid_shape)
        // user name
        eT_userName!!.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                println("$TAG isSignUpButtonEnabled=$isSignUpButtonEnabled")

                //..user enter space then remove
                if (eT_userName!!.text.toString().contains(" ")) {
                    eT_userName!!.setText(eT_userName!!.text.toString().replace(" ", ""))
                    eT_userName!!.setSelection(eT_userName!!.text.toString().length)
                }

                //check vadidate username or not
                if (CommonClass.isValidUserName(eT_userName!!.text.toString())) {
                    // call this method to verify user name
                    userNameCheckApi()
                } else {
                    iV_userName_error!!.visibility = View.VISIBLE
                    iV_userName_error!!.setImageResource(R.drawable.error_icon)
                }
                if (isSignUpParamFilled) {
                    isSignUpButtonEnabled = true
                    // set Opacity to login button
                    CommonClass.setViewOpacity(mActivity, rL_signup!!, 204, R.drawable.rect_purple_color_with_solid_shape)
                } else {
                    isSignUpButtonEnabled = false
                    // set Opacity to login button
                    CommonClass.setViewOpacity(mActivity, rL_signup!!, 102, R.drawable.rect_purple_color_with_solid_shape)
                }

                // change user icon
                val userName = eT_userName!!.text.toString()
                if (userName.isEmpty()) iV_user_name!!.setImageResource(R.drawable.username_off) else iV_user_name!!.setImageResource(R.drawable.username_on)
            }

            override fun afterTextChanged(s: Editable?) {}
        })

        // User Full name
        eT_fullName!!.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                // change user name icon
                val fullName = eT_fullName!!.text.toString()
                if (fullName.isEmpty()) iV_full_name!!.setImageResource(R.drawable.name_off) else iV_full_name!!.setImageResource(R.drawable.name_on)
                if (isSignUpParamFilled) {
                    isSignUpButtonEnabled = true
                    // set Opacity to login button
                    CommonClass.setViewOpacity(mActivity, rL_signup!!, 204, R.drawable.rect_purple_color_with_solid_shape)
                } else {
                    isSignUpButtonEnabled = false
                    // set Opacity to login button
                    CommonClass.setViewOpacity(mActivity, rL_signup!!, 102, R.drawable.rect_purple_color_with_solid_shape)
                }
            }

            override fun afterTextChanged(s: Editable?) {}
        })

        // Password
        eT_password = signup_views!!.findViewById<View?>(R.id.eT_password) as EditText
        eT_password!!.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                println("$TAG isSignUpButtonEnabled=$isSignUpButtonEnabled")
                if (isSignUpParamFilled) {
                    isSignUpButtonEnabled = true
                    // set Opacity to login button
                    CommonClass.setViewOpacity(mActivity, rL_signup!!, 204, R.drawable.rect_purple_color_with_solid_shape)
                } else {
                    isSignUpButtonEnabled = false
                    // set Opacity to login button
                    CommonClass.setViewOpacity(mActivity, rL_signup!!, 102, R.drawable.rect_purple_color_with_solid_shape)
                }

                // change user password icon
                val password = eT_password!!.text.toString()
                if (password.isEmpty()) iV_password!!.setImageResource(R.drawable.password_off) else iV_password!!.setImageResource(R.drawable.password_on)
            }

            override fun afterTextChanged(s: Editable?) {}
        })

        // Mobile number
        eT_mobileNo = signup_views!!.findViewById<View?>(R.id.eT_mobileNo) as EditText
        eT_mobileNo!!.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                println("$TAG isSignUpButtonEnabled=$isSignUpButtonEnabled")
                if (CommonClass.isValidPhoneNumber(eT_mobileNo!!.text.toString())) {
                    // call this method to verify mobile number
                    phoneNumberCheckApi()
                } else {
                    iV_error_ph!!.visibility = View.VISIBLE
                    iV_error_ph!!.setImageResource(R.drawable.error_icon)
                }
                if (isSignUpParamFilled) {
                    isSignUpButtonEnabled = true
                    // set Opacity to login button
                    CommonClass.setViewOpacity(mActivity, rL_signup!!, 204, R.drawable.rect_purple_color_with_solid_shape)
                } else {
                    isSignUpButtonEnabled = false
                    // set Opacity to login button
                    CommonClass.setViewOpacity(mActivity, rL_signup!!, 102, R.drawable.rect_purple_color_with_solid_shape)
                }

                // change user mobile icon
                val mobileNo = eT_mobileNo!!.text.toString()
                if (mobileNo.isEmpty()) iV_phone_icon!!.setImageResource(R.drawable.mobileniumber_off) else iV_phone_icon!!.setImageResource(R.drawable.mobilenumber_on)
            }

            override fun afterTextChanged(s: Editable?) {}
        })

        // Email Id
        eT_emailId!!.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                println("$TAG isSignUpButtonEnabled=$isSignUpButtonEnabled")

                // check mobile number to verify email address
                if (CommonClass.isValidEmail(eT_emailId!!.text.toString())) {
                    emailCheckApi()
                } else {
                    iV_error_email!!.visibility = View.VISIBLE
                    iV_error_email!!.setImageResource(R.drawable.error_icon)
                }
                if (isSignUpParamFilled) {
                    isSignUpButtonEnabled = true
                    // set Opacity to login button
                    CommonClass.setViewOpacity(mActivity, rL_signup!!, 204, R.drawable.rect_purple_color_with_solid_shape)
                } else {
                    isSignUpButtonEnabled = false
                    // set Opacity to login button
                    CommonClass.setViewOpacity(mActivity, rL_signup!!, 102, R.drawable.rect_purple_color_with_solid_shape)
                }

                // change user mobile icon
                val emailId = eT_emailId!!.text.toString()
                if (emailId.isEmpty()) iV_email!!.setImageResource(R.drawable.email_off) else iV_email!!.setImageResource(R.drawable.email_on)
            }

            override fun afterTextChanged(s: Editable?) {}
        })

        // password
        eT_password!!.setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_NEXT) {
                if (eT_password!!.text.toString().isEmpty()) {
                    iV_password_error!!.visibility = View.VISIBLE
                    iV_password_error!!.setImageResource(R.drawable.error_icon)
                    CommonClass.showSnackbarMessage(rL_rootElement, getResources().getString(R.string.please_enter_password))
                    return@OnEditorActionListener true
                } else {
                    //iV_password_error.setVisibility(View.VISIBLE);
                    //iV_password_error.setImageResource(R.drawable.rightusername);
                    return@OnEditorActionListener false
                }
            }
            false
        })

        // set password validation
        eT_password!!.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            if (!hasFocus) {
                if (!TextUtils.isEmpty(eT_password!!.text)) {
                    iV_password_error!!.visibility = View.GONE
                    //iV_password_error.setImageResource(R.drawable.rightusername);
                } else {
                    iV_password_error!!.visibility = View.VISIBLE
                    iV_password_error!!.setImageResource(R.drawable.error_icon)
                }
            }
        }
    }//                countryIsoCode=checkCountry(countryIsoCode);

    /**
     * <h>GetCountryCodeList</h>
     *
     *
     * In this method we used to get the all country iso code and number into
     * list. And find the user current country iso code and number and set
     * before the mobile number.
     *
     */
    private val countryCodeList: Unit
        private get() {
            val country_array: Array<String?> = getResources().getStringArray(R.array.countryCodes)
            if (country_array.size > 0) {
                for (aCountry_array in country_array) {
                    var getCountryList: Array<String?>
                    getCountryList = aCountry_array!!.split(",".toRegex()).toTypedArray()
                    var countryCode: String?
                    var countryName: String?
                    countryCode = getCountryList[0]
                    countryName = getCountryList[1]
                    val country = Country()
                    country.code = countryCode!!.trim { it <= ' ' }
                    country.name = countryName!!.trim { it <= ' ' }
                    arrayListCountry!!.add(country)
                }
                if (arrayListCountry!!.size > 0) {
                    dialogCountryList = DialogCountryList(mActivity!!, arrayListCountry!!)
                    var countryIsoCode = Locale.getDefault().country
                    Log.e("====1", countryIsoCode)
                    //                countryIsoCode=checkCountry(countryIsoCode);
                    countryIsoCode = getDetectedCountry(countryIsoCode)
                    Log.e("====2", countryIsoCode)
                    countryIsoCode = countryIsoCode.toUpperCase()
                    if (countryIsoCode != null && !countryIsoCode.isEmpty()) {
                        val countryIsoNo = setCurrentCountryCode(countryIsoCode.toUpperCase())
                        countryIsoNumber = getResources().getString(R.string.plus).toString() + countryIsoNo
                        println("$TAG countryIsoNumber=$countryIsoNumber")
                        tV_country_iso_no!!.text = countryIsoNumber
                        tV_country_code!!.text = countryIsoCode
                    }
                }
            }
        }

    fun getDetectedCountry(defaultCountryIsoCode: String?): String? {
        var countryIsoCode = defaultCountryIsoCode
        val telMgr = getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager?
        if (telMgr != null) {
            Log.e("====3", telMgr.simCountryIso + "=====" + telMgr.networkCountryIso)
            if (telMgr.simCountryIso != null && telMgr.simCountryIso != "") {
                Log.e("====3", telMgr.simCountryIso)
                return telMgr.simCountryIso.also { countryIsoCode = it }
            } else if (telMgr.networkCountryIso != null && telMgr.networkCountryIso != "") {
                Log.e("====4", telMgr.networkCountryIso)
                return telMgr.networkCountryIso.also { countryIsoCode = it }
            }
        }
        return countryIsoCode
    }

    private fun checkCountry(countryIsoCode: String?): String? {
        var countryIsoCodeVal = countryIsoCode
        val telMgr = getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager?
                ?: return countryIsoCodeVal
        val simState = telMgr.simState
        when (simState) {
            TelephonyManager.SIM_STATE_ABSENT -> {
                val tz = TimeZone.getDefault()
                Log.e("====3", tz!!.id + "===" + tz.displayName)
                return tz.id.also { countryIsoCodeVal = it }
            }
            TelephonyManager.SIM_STATE_READY -> if (telMgr != null) {
                Log.e("====4", telMgr.networkCountryIso)
                return telMgr.networkCountryIso.also { countryIsoCodeVal = it }
            }
        }
        return countryIsoCodeVal
    }

    /**
     * <h>SetCurrentCountryCode</h>
     *
     *
     * In this method we used to find the country iso number by giving its
     * iso code.
     *
     * @param isoCode The iso code of the country
     * @return it returns the country iso number e.g +91
     */
    private fun setCurrentCountryCode(isoCode: String?): String? {
        var countryCode = ""
        for (country in arrayListCountry!!) {
            println(TAG + " " + "isoCode=" + isoCode + " " + "country.getName()=" + country!!.name)
            if (country.name == isoCode) {
                countryCode = country.code
                return countryCode
            }
        }
        return countryCode
    }

    /**
     * <h>checkValidation</h>
     *
     *
     * In this method we used to check the mandatory field whether
     * it has been filled or not.
     *
     */
    private val isLoginParamFilled: Boolean
        private get() {
            val isValid: Boolean
            isValid = !eT_loginUserName!!.text.toString().isEmpty() && !eT_loginPassword!!.text.toString().isEmpty()
            return isValid
        }

    /**
     * <h>checkValidation</h>
     *
     *
     * In this method we used to check the mandatory field whether
     * it has been filled or not else call registation api.
     *
     */
    private val isSignUpParamFilled: Boolean
        private get() {
            val isValid: Boolean
            isValid = !eT_userName!!.text.toString().trim { it <= ' ' }.isEmpty() && !eT_password!!.text.toString().trim { it <= ' ' }.isEmpty() && !eT_mobileNo!!.text.toString().trim { it <= ' ' }.isEmpty() && !eT_emailId!!.text.toString().trim { it <= ' ' }.isEmpty() && CommonClass.isValidEmail(eT_emailId!!.text.toString().trim { it <= ' ' }) && !eT_fullName!!.text.toString().trim { it <= ' ' }.isEmpty() && CommonClass.isValidUserName(eT_userName!!.text.toString().trim { it <= ' ' }) && CommonClass.isValidPhoneNumber(eT_mobileNo!!.text.toString().trim { it <= ' ' })
            return isValid
        }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.rL_back_btn -> onBackPressed()
            R.id.rL_do_login -> if (isLoginButtonEnabled) {
                isFromLocation = true
                permissionsArray = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
                runTimePermission = RunTimePermission(mActivity!!, permissionsArray!!, false)
                showKeyboard(InputMethodManager.HIDE_IMPLICIT_ONLY)
                val lm = mActivity!!.getSystemService(Context.LOCATION_SERVICE) as LocationManager
                val isLocationEnabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER)
                println(TAG + " " + "is location enabled=" + isLocationEnabled + " " + "is permission allowed=" + runTimePermission!!.checkPermissions(permissionsArray!!))
                progress_bar_login!!.visibility = View.VISIBLE
                tV_do_login!!.visibility = View.GONE
                if (isLocationEnabled && runTimePermission!!.checkPermissions(permissionsArray!!)) currentLocation else loginRequestApi()
                // loginRequestApi();
            }
            R.id.iV_profile_pic -> {
                println("$TAG profile pic clicked..")
                isFromLocation = false
                permissionsArray = arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                runTimePermission = RunTimePermission(mActivity!!, permissionsArray!!, false)
                if (runTimePermission!!.checkPermissions(permissionsArray!!)) chooseImage() else {
                    runTimePermission!!.requestPermission()
                }
            }
            R.id.rL_createAcc -> if (isSignUpButtonEnabled) {
                if (checkboxSignUp!!.isChecked) {
                    if (isUserRegistered) {
                        CommonClass.showSnackbarMessage(rL_rootElement, getResources().getString(R.string.username_already_registered))
                    } else if (isPhoneRegistered) {
                        CommonClass.showSnackbarMessage(rL_rootElement, getResources().getString(R.string.phone_already_registered))
                    } else if (isEmailRegistered) {
                        CommonClass.showSnackbarMessage(rL_rootElement, getResources().getString(R.string.email_already_registered))
                    } else {
                        //generateOtp();
                        println("$TAG type=$type")
                        when (type) {
                            "normalSignup" -> {
                                signUpType = VariableConstants.TYPE_MANUAL
                                if (isPictureTaken) {
                                    progress_bar_signup!!.visibility = View.VISIBLE
                                    tV_signup!!.visibility = View.GONE
                                    cloudinaryDetailsApi
                                } else {
                                    generateOtp("")
                                    //                                        submitRegistration();
//                                        Intent intent= new Intent(mActivity,HomePageActivity.class);
//                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                                        startActivity(intent);
                                }
                            }
                            "googleSignUp" -> {
                                signUpType = VariableConstants.TYPE_GOOGLE
                                if (isPictureTaken) {
                                    progress_bar_signup!!.visibility = View.VISIBLE
                                    tV_signup!!.visibility = View.GONE
                                    cloudinaryDetailsApi
                                } else {
//                                        submitRegistration();
                                    generateOtp(google_userImageUrl)

//                                        Intent intent= new Intent(mActivity,HomePageActivity.class);
//                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                                        startActivity(intent);
                                }
                            }
                            "fbSignUp" -> {
                                signUpType = VariableConstants.TYPE_FACEBOOK
                                if (isPictureTaken) {
                                    progress_bar_signup!!.visibility = View.VISIBLE
                                    tV_signup!!.visibility = View.GONE
                                    cloudinaryDetailsApi
                                } else {
                                    generateOtp(fb_userImageUrl)
                                    //                                        submitRegistration();
//                                        Intent intent= new Intent(mActivity,HomePageActivity.class);
//                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                                        startActivity(intent);
                                }
                            }
                        }
                    }
                } else CommonClass.showTopSnackBar(rL_rootElement, getResources().getString(R.string.please_accept_termsNconditions))
            } else {
                CommonClass.showTopSnackBar(rL_rootElement, getResources().getString(R.string.please_check_filled))
            }
            R.id.rL_country_picker -> if (dialogCountryList != null) {
                showKeyboard(InputMethodManager.SHOW_FORCED)
                dialogCountryList!!.showCountryCodePicker(object : SetCountryCodeListener {
                    override fun getCode(code: String?, name: String?) {
                        var code = code
                        showKeyboard(InputMethodManager.HIDE_IMPLICIT_ONLY)
                        countryIsoNumber = getResources().getString(R.string.plus).toString() + code
                        code = getResources().getString(R.string.plus).toString() + code
                        tV_country_iso_no!!.text = code
                        tV_country_code!!.text = name
                        eT_mobileNo!!.requestFocus()
                    }
                })
            }
            R.id.rL_forgot_password -> if (isToStartActivity) {
                startActivity(Intent(mActivity, ForgotPasswordActivity::class.java))
                isToStartActivity = false
            }
        }
    }
    /**
     * <h>GenerateOtp</h>
     */
    //    private void generateOtp()
    //    {
    //        if (CommonClass.isNetworkAvailable(mActivity)) {
    //            progress_bar_signup.setVisibility(View.VISIBLE);
    //            tV_signup.setVisibility(View.GONE);
    //
    //            System.out.println(TAG+" "+"country code="+tV_country_iso_no.getText().toString());
    //
    //            JSONObject request_datas = new JSONObject();
    //            try {
    //                request_datas.put("deviceId",mSessionManager.getDeviceId());
    //                request_datas.put("phoneNumber", countryIsoNumber+eT_mobileNo.getText().toString());
    //            }
    //            catch (JSONException e) {
    //                e.printStackTrace();
    //            }
    //
    //            OkHttp3Connection.doOkHttp3Connection(TAG, ApiUrl.OTP, OkHttp3Connection.Request_type.POST, request_datas, new OkHttp3Connection.OkHttp3RequestCallback() {
    //                @Override
    //                public void onSuccess(String result, String user_tag)
    //                {
    //                    System.out.println(TAG+" "+"otp response="+result);
    //                    progress_bar_signup.setVisibility(View.GONE);
    //                    tV_signup.setVisibility(View.VISIBLE);
    //                    Gson gson = new Gson();
    //                    PhoneOtpMainPojo otpMainPojo = gson.fromJson(result, PhoneOtpMainPojo.class);
    //
    //                    // success
    //                    switch (otpMainPojo.getCode())
    //                    {
    //                        // success
    //                        case "200":
    //                            otpCode=otpMainPojo.getData();
    //                            System.out.println(TAG+" "+"type="+type);
    //
    //                            switch (type) {
    //                                // Normal sign up
    //                                case "normalSignup":
    //                                    signUpType = VariableConstants.TYPE_MANUAL;
    //                                    if (isPictureTaken) {
    //                                        progress_bar_signup.setVisibility(View.VISIBLE);
    //                                        tV_signup.setVisibility(View.GONE);
    //                                        getCloudinaryDetailsApi();
    //                                    } else {
    //                                       // openOtpScreen("");
    //                                        submitRegistration();
    //                                        Intent intent= new Intent(mActivity,HomePageActivity.class);
    //                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
    //                                        startActivity(intent);
    //                                    }
    //                                    break;
    //
    //                                // Google sign up
    //                                case "googleSignUp":
    //                                    signUpType = VariableConstants.TYPE_GOOGLE;
    //                                    if (isPictureTaken) {
    //                                        progress_bar_signup.setVisibility(View.VISIBLE);
    //                                        tV_signup.setVisibility(View.GONE);
    //                                        getCloudinaryDetailsApi();
    //                                    } else {
    //                                      //  openOtpScreen(google_userImageUrl);
    //                                        submitRegistration();
    ////                                        Intent intent= new Intent(mActivity,HomePageActivity.class);
    ////                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
    ////                                        startActivity(intent);
    //                                    }
    //                                    break;
    //
    //                                // Facebook signup
    //                                case "fbSignUp":
    //                                    signUpType = VariableConstants.TYPE_FACEBOOK;
    //                                    if (isPictureTaken) {
    //                                        progress_bar_signup.setVisibility(View.VISIBLE);
    //                                        tV_signup.setVisibility(View.GONE);
    //                                        getCloudinaryDetailsApi();
    //                                    } else {
    //
    //                                       // openOtpScreen(fb_userImageUrl);
    //                                        submitRegistration();
    ////                                        Intent intent= new Intent(mActivity,HomePageActivity.class);
    ////                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
    ////                                        startActivity(intent);
    //                                    }
    //                                    break;
    //                            }
    //                            break;
    //
    //                        // auth token expired
    //                        case "401" :
    //                            CommonClass.sessionExpired(mActivity);
    //                            break;
    //
    //                        // error like invalid phone number
    //                        case "500":
    //                            CommonClass.showSnackbarMessage(rL_rootElement, otpMainPojo.getError().getMessage());
    //                            break;
    //
    //                        // other error
    //                        default:
    //                            CommonClass.showSnackbarMessage(rL_rootElement, otpMainPojo.getMessage());
    //                            break;
    //                    }
    //                }
    //
    //                @Override
    //                public void onError(String error, String user_tag) {
    //                    progress_bar_signup.setVisibility(View.GONE);
    //                    tV_signup.setVisibility(View.VISIBLE);
    //                    CommonClass.showSnackbarMessage(rL_rootElement, error);
    //                }
    //            });
    //        }
    //        else CommonClass.showSnackbarMessage(rL_rootElement,getResources().getString(R.string.NoInternetAccess));
    //    }
    /**
     * <h>GenerateOtp</h>
     */
    private fun generateOtp(profilePicUrl: String?) {
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            val request_datas = JSONObject()
            try {
                request_datas.put("deviceId", mSessionManager!!.deviceId)
                request_datas.put("phoneNumber", tV_country_iso_no!!.text.toString().trim { it <= ' ' } + eT_mobileNo!!.text.toString().trim { it <= ' ' })
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            OkHttp3Connection.doOkHttp3Connection(TAG, ApiUrl.OTP, OkHttp3Connection.Request_type.POST, request_datas, object : OkHttp3RequestCallback {
                override fun onSuccess(result: String?, user_tag: String?) {
                    val gson = Gson()
                    val otpMainPojo = gson.fromJson(result, PhoneOtpMainPojo::class.java)
                    when (otpMainPojo!!.code) {
                        "200" -> {
                            otpCode = otpMainPojo.data
                            openOtpScreen(profilePicUrl)
                        }
                        "401" -> CommonClass.sessionExpired(mActivity!!)
                        "500" -> CommonClass.showSnackbarMessage(rL_rootElement, otpMainPojo.error!!.message)
                        else -> CommonClass.showSnackbarMessage(rL_rootElement, otpMainPojo.message)
                    }
                }

                override fun onError(error: String?, user_tag: String?) {
                    // stop progress bar
                    progress_bar_signup!!.visibility = View.GONE
                    tV_signup!!.visibility = View.VISIBLE
                    CommonClass.showSnackbarMessage(rL_rootElement, error)
                }
            })
        } else CommonClass.showSnackbarMessage(rL_rootElement, getResources().getString(R.string.NoInternetAccess))
    }

    private fun openOtpScreen(profilePicUrl: String?) {
        // stop progress bar
        progress_bar_signup!!.visibility = View.GONE
        tV_signup!!.visibility = View.VISIBLE

        // Hide Keypad
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
        val view: View = this.getCurrentFocus()!!
        if (view != null && imm != null) {
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }


        // call Number verification screen
        val intent = Intent(mActivity, NumberVerificationActivity::class.java)
        intent.putExtra("type", type)
        intent.putExtra("signupType", signUpType)
        intent.putExtra("username", eT_userName!!.text.toString())
        intent.putExtra("profilePicUrl", profilePicUrl)
        intent.putExtra("fullName", eT_fullName!!.text.toString())
        intent.putExtra("password", eT_password!!.text.toString())
        intent.putExtra("phoneNumber", countryIsoNumber + eT_mobileNo!!.text.toString())
        intent.putExtra("email", eT_emailId!!.text.toString())
        intent.putExtra("googleToken", googleToken)
        intent.putExtra("googleId", googleId)
        intent.putExtra("facebookId", fbId)
        intent.putExtra("accessToken", fb_accessToken)
        intent.putExtra("otpCode", otpCode)
        if (isToStartActivity) {
            startActivityForResult(intent, VariableConstants.NUMBER_VERIFICATION_REQ_CODE)
            isToStartActivity = false
            println(TAG + " " + "sending mob no=" + countryIsoNumber + eT_mobileNo!!.text.toString())
        }
    }

    /**
     * In this method we find current location using FusedLocationApi.
     * in this we have onUpdateLocation() method in which we check if
     * its not null then We call guest user api.
     */
    private val currentLocation: Unit
        private get() {
            if (CommonClass.isNetworkAvailable(mActivity!!)) {
                locationService = FusedLocationService(mActivity!!, object : FusedLocationReceiver {
                    override fun onUpdateLocation() {
                        val currentLocation = locationService!!.receiveLocation()
                        if (currentLocation != null) {
                            currentLat = currentLocation.latitude.toString()
                            currentLng = currentLocation.longitude.toString()
                            println("$TAG currentLat=$currentLat currentLng=$currentLng")
                            if (isLocationFound(currentLat, currentLng)) {
                                mSessionManager!!.currentLat = currentLat
                                mSessionManager!!.currentLng = currentLng
                                address = CommonClass.getCompleteAddressString(mActivity, currentLocation.latitude, currentLocation.longitude)
                                city = CommonClass.getCityName(mActivity!!, currentLocation.latitude, currentLocation.longitude)
                                countryShortName = CommonClass.getCountryCode(mActivity!!, currentLocation.latitude, currentLocation.longitude)
                                val country = CommonClass.getCountryName(mActivity!!, currentLocation.latitude, currentLocation.longitude)
                                if (address!!.isEmpty() || address == null) address = CommonClass.makeAddressFromCityCountry(city, country)
                                loginRequestApi()
                            }
                        }
                    }
                }
                )
            } else {
                progress_bar_login!!.visibility = View.GONE
                tV_do_login!!.visibility = View.VISIBLE
                CommonClass.showSnackbarMessage(rL_rootElement, getResources().getString(R.string.NoInternetAccess))
            }
        }

    /**
     * In this method we used to check whether current currentLat and
     * long has been received or not.
     * @param lat The current latitude
     * @param lng The current longitude
     * @return boolean flag true or false
     */
    private fun isLocationFound(lat: String?, lng: String?): Boolean {
        return !(lat == null || lat.isEmpty()) && !(lng == null || lng.isEmpty())
    }

    /**
     * <h>SelectImage</h>
     *
     *
     * Using this method we open a pop-up. when
     * user click on profile image. it contains
     * thee field Take Photo,Choose from Gallery,
     * Cancel.
     *
     */
    fun chooseImage() {
        val selectImgDialog = Dialog(mActivity!!)
        selectImgDialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        mActivity!!.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        selectImgDialog.setContentView(R.layout.select_image_layout)

        // Take picture
        val rL_take_pic = selectImgDialog.findViewById<View?>(R.id.rL_take_pic) as RelativeLayout
        rL_take_pic.setOnClickListener {
            selectImgDialog.dismiss()
            mHandleCameraEvents!!.takePicture()
        }

        // Choose Image from gallery
        val rL_select_pic = selectImgDialog.findViewById<View?>(R.id.rL_select_pic) as RelativeLayout
        rL_select_pic.setOnClickListener {
            selectImgDialog.dismiss()
            mHandleCameraEvents!!.openGallery()
        }

        // Remove profile pic
        val rL_remove_pic = selectImgDialog.findViewById<View?>(R.id.rL_remove_pic) as RelativeLayout
        if (!isPictureTaken) {
            iv_edit_icon!!.visibility = View.GONE
            rL_remove_pic.visibility = View.GONE
        } else {
            iv_edit_icon!!.visibility = View.VISIBLE
            rL_remove_pic.visibility = View.VISIBLE
        }
        rL_remove_pic.setOnClickListener {
            removeProfileImage()
            selectImgDialog.dismiss()
        }
        selectImgDialog.show()
    }

    private fun removeProfileImage() {
        isPictureTaken = false
        iv_edit_icon!!.visibility = View.GONE
        iV_profile_pic!!.setImageResource(R.drawable.add_photo)
    }

    /**
     * <h>LoginRequestApi</h>
     *
     *
     * This method is called when user click on Normal login button.
     * In this method we used to call login api through OkHttp3. After
     * getting response if the code is 200. Then we move to HomePageActivity.
     *
     */
    private fun loginRequestApi() {
        //username, password
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            val requestDatas = JSONObject()
            // loginType, pushToken, place, city, countrySname, latitude, longitude,username, password
            try {
                requestDatas.put("loginType", VariableConstants.TYPE_MANUAL)
                requestDatas.put("pushToken", mSessionManager!!.pushToken)
                requestDatas.put("place", address)
                requestDatas.put("city", city)
                requestDatas.put("countrySname", countryShortName)
                requestDatas.put("latitude", currentLat)
                requestDatas.put("longitude", currentLng)
                requestDatas.put("username", eT_loginUserName!!.text.toString())
                requestDatas.put("password", eT_loginPassword!!.text.toString())
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            OkHttp3Connection.doOkHttp3Connection(TAG, ApiUrl.LOGIN, OkHttp3Connection.Request_type.POST, requestDatas, object : OkHttp3RequestCallback {
                override fun onSuccess(result: String?, user_tag: String?) {
                    val loginResponse: LoginResponsePojo
                    val gson = Gson()
                    loginResponse = gson.fromJson(result, LoginResponsePojo::class.java)
                    when (loginResponse.code) {
                        "200" -> {
                            mSessionManager!!.isUserLoggedIn = true
                            mSessionManager!!.setmqttId(loginResponse.mqttId)
                            mSessionManager!!.authToken = loginResponse.token
                            mSessionManager!!.userName = loginResponse.username
                            mSessionManager!!.userImage = loginResponse.profilePicUrl
                            mSessionManager!!.userId = loginResponse.userId
                            mSessionManager!!.loginWith = "normalLogin"
                            CommonClass.isBackFromLogin = true
                            initUserDetails(loginResponse.profilePicUrl, loginResponse.mqttId!!, loginResponse.email, loginResponse.username, loginResponse.token)
                            logDeviceInfo(loginResponse.token)
                        }
                        else -> {
                            progress_bar_login!!.visibility = View.GONE
                            tV_do_login!!.visibility = View.VISIBLE
                            CommonClass.showTopSnackBar(rL_rootElement, loginResponse.message)
                        }
                    }
                }

                override fun onError(error: String?, user_tag: String?) {
                    progress_bar_login!!.visibility = View.GONE
                    tV_do_login!!.visibility = View.VISIBLE
                    CommonClass.showTopSnackBar(rL_rootElement, error)
                }
            })
        } else {
            progress_bar_login!!.visibility = View.GONE
            tV_do_login!!.visibility = View.VISIBLE
            CommonClass.showTopSnackBar(rL_rootElement, getResources().getString(R.string.NoInternetAccess))
        }
    }

    /**
     * <h>LogDeviceInfo</h>
     *
     *
     * In this method we used to do api call to send device information like device name
     * model number, device id etc to server to log the the user with specific device.
     *
     * @param token The auth token for particular user
     */
    private fun logDeviceInfo(token: String?) {
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            //deviceName, deviceId, deviceOs, modelNumber, appVersion
            val request_datas = JSONObject()
            try {
                request_datas.put("deviceName", Build.BRAND)
                request_datas.put("deviceId", mSessionManager!!.deviceId)
                request_datas.put("deviceOs", Build.VERSION.RELEASE)
                request_datas.put("modelNumber", Build.MODEL)
                request_datas.put("appVersion", BuildConfig.VERSION_NAME)
                request_datas.put("token", token)
                request_datas.put("deviceType", VariableConstants.DEVICE_TYPE)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            OkHttp3Connection.doOkHttp3Connection(TAG, ApiUrl.LOG_DEVICE, OkHttp3Connection.Request_type.POST, request_datas, object : OkHttp3RequestCallback {
                override fun onSuccess(result: String?, user_tag: String?) {
                    progress_bar_login!!.visibility = View.GONE
                    println("$TAG log device info=$result")
                    val logDevicePojo: LogDevicePojo
                    val gson = Gson()
                    logDevicePojo = gson.fromJson(result, LogDevicePojo::class.java)
                    when (logDevicePojo.code) {
                        "200" -> {
                            // Open Home page screen
                            //LandingActivity.mLandingActivity.finish();
                            //finish();
                            val intent = Intent()
                            intent.putExtra("isToFinishLandingScreen", true)
                            setResult(VariableConstants.LOGIN_SIGNUP_REQ_CODE, intent)
                            finish()
                        }
                        "401" -> CommonClass.sessionExpired(mActivity!!)
                        else -> {
                            tV_do_login!!.visibility = View.VISIBLE
                            CommonClass.showSnackbarMessage(rL_rootElement, logDevicePojo.message)
                        }
                    }
                }

                override fun onError(error: String?, user_tag: String?) {
                    progress_bar_login!!.visibility = View.GONE
                    tV_do_login!!.visibility = View.VISIBLE
                    CommonClass.showSnackbarMessage(rL_rootElement, error)
                }
            })
        } else CommonClass.showSnackbarMessage(rL_rootElement, getResources().getString(R.string.NoInternetAccess))
    }

    /**
     * <h>phoneNumberCheckApi</h>
     *
     *
     * In this method we used to do phone Number Check api
     * to find the given phone number is already exist or
     * not.
     *
     */
    private fun phoneNumberCheckApi() {
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            progress_bar_ph!!.visibility = View.VISIBLE
            iV_error_ph!!.visibility = View.GONE
            val request_param = JSONObject()
            try {
                request_param.put("phoneNumber", countryIsoNumber + eT_mobileNo!!.text.toString())
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            OkHttp3Connection.doOkHttp3Connection(TAG, ApiUrl.PHONE_NUMBER_CHECK, OkHttp3Connection.Request_type.POST, request_param, object : OkHttp3RequestCallback {
                override fun onSuccess(result: String?, user_tag: String?) {
                    progress_bar_ph!!.visibility = View.GONE
                    println("$TAG phoneNumber api res=$result")
                    val phoneNoCheckPojo: PhoneNoCheckPojo
                    val gson = Gson()
                    phoneNoCheckPojo = gson.fromJson(result, PhoneNoCheckPojo::class.java)
                    when (phoneNoCheckPojo.code) {
                        "200" -> if (eT_mobileNo!!.text.toString().isEmpty()) {
                            iV_error_ph!!.setImageResource(R.drawable.error_icon)
                        } else {
                            isPhoneRegistered = false
                            iV_error_ph!!.visibility = View.VISIBLE
                            iV_error_ph!!.setImageResource(R.drawable.rightusername)
                        }
                        "401" -> CommonClass.sessionExpired(mActivity!!)
                        else -> {
                            isPhoneRegistered = true
                            iV_error_ph!!.visibility = View.VISIBLE
                            iV_error_ph!!.setImageResource(R.drawable.error_icon)
                            CommonClass.showTopSnackBar(rL_rootElement, phoneNoCheckPojo.message)
                        }
                    }
                }

                override fun onError(error: String?, user_tag: String?) {
                    progress_bar_ph!!.visibility = View.GONE
                }
            })
        }
    }

    /**
     * <h>UserNameCheckApi</h>
     *
     *
     * In this method we used to do user name Check api
     * to find the given user name is already exist or
     * not.
     *
     */
    private fun userNameCheckApi() {
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            pBar_userName!!.visibility = View.VISIBLE
            iV_userName_error!!.visibility = View.GONE
            val request_param = JSONObject()
            try {
                request_param.put("username", eT_userName!!.text.toString())
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            OkHttp3Connection.doOkHttp3Connection(TAG, ApiUrl.USER_NAME_CHECK, OkHttp3Connection.Request_type.POST, request_param, object : OkHttp3RequestCallback {
                override fun onSuccess(result: String?, user_tag: String?) {
                    pBar_userName!!.visibility = View.GONE
                    println("$TAG user name check api res=$result")
                    val userNameCheckPojo: UserNameCheckPojo
                    val gson = Gson()
                    userNameCheckPojo = gson.fromJson(result, UserNameCheckPojo::class.java)
                    when (userNameCheckPojo.code) {
                        "200" -> if (eT_userName!!.text.toString().isEmpty()) {
                            iV_userName_error!!.visibility = View.VISIBLE
                            iV_userName_error!!.setImageResource(R.drawable.error_icon)
                        } else {
                            isUserRegistered = false
                            iV_userName_error!!.visibility = View.VISIBLE
                            iV_userName_error!!.setImageResource(R.drawable.rightusername)
                        }
                        "401" -> CommonClass.sessionExpired(mActivity!!)
                        else -> {
                            isUserRegistered = true
                            iV_userName_error!!.visibility = View.VISIBLE
                            iV_userName_error!!.setImageResource(R.drawable.error_icon)
                            CommonClass.showTopSnackBar(rL_rootElement, userNameCheckPojo.message)
                        }
                    }
                }

                override fun onError(error: String?, user_tag: String?) {
                    pBar_userName!!.visibility = View.GONE
                }
            })
        }
    }

    /**
     * <h>UserNameCheckApi</h>
     *
     *
     * In this method we used to do user name Check api
     * to find the given user name is already exist or
     * not.
     *
     */
    private fun emailCheckApi() {
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            pBar_email!!.visibility = View.VISIBLE
            iV_error_email!!.visibility = View.GONE
            val request_param = JSONObject()
            try {
                request_param.put("email", eT_emailId!!.text.toString())
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            OkHttp3Connection.doOkHttp3Connection(TAG, ApiUrl.EMAIL_ID_CHECK, OkHttp3Connection.Request_type.POST, request_param, object : OkHttp3RequestCallback {
                override fun onSuccess(result: String?, user_tag: String?) {
                    pBar_email!!.visibility = View.GONE
                    println("$TAG email is check api res=$result")
                    val emailCheckPojo: EmailCheckPojo
                    val gson = Gson()
                    emailCheckPojo = gson.fromJson(result, EmailCheckPojo::class.java)
                    when (emailCheckPojo.code) {
                        "200" -> if (eT_emailId!!.text.toString().isEmpty()) {
                            iV_error_email!!.visibility = View.VISIBLE
                            iV_error_email!!.setImageResource(R.drawable.error_icon)
                        } else {
                            isEmailRegistered = false
                            iV_error_email!!.visibility = View.VISIBLE
                            iV_error_email!!.setImageResource(R.drawable.rightusername)
                        }
                        "401" -> CommonClass.sessionExpired(mActivity!!)
                        else -> {
                            isEmailRegistered = true
                            iV_error_email!!.visibility = View.VISIBLE
                            iV_error_email!!.setImageResource(R.drawable.error_icon)
                            CommonClass.showTopSnackBar(rL_rootElement, emailCheckPojo.message)
                        }
                    }
                }

                override fun onError(error: String?, user_tag: String?) {
                    pBar_email!!.visibility = View.GONE
                }
            })
        }
    }

    /**
     * <h2>getCloud_details</h2>
     *
     *
     * Collecting the cloudinary details from the server..
     *
     */
    private val cloudinaryDetailsApi: Unit
        private get() {
            if (CommonClass.isNetworkAvailable(mActivity!!)) {
                val request_data = JSONObject()
                OkHttp3Connection.doOkHttp3Connection("", ApiUrl.GET_CLOUDINARY_DETAILS, OkHttp3Connection.Request_type.POST, request_data, object : OkHttp3RequestCallback {
                    override fun onSuccess(result: String?, tag: String?) {
                        val response = Gson().fromJson(result, Cloudinary_Details_reponse::class.java)
                        if (response!!.code == "200") {
                            val cloudName: String?
                            val timestamp: String?
                            val apiKey: String?
                            val signature: String?
                            cloudName = response.response!!.cloudName
                            timestamp = response.response!!.timestamp
                            apiKey = response.response!!.apiKey
                            signature = response.response!!.signature
                            val remaining_data = Bundle()
                            remaining_data.putString("signature", signature)
                            remaining_data.putString("timestamp", timestamp)
                            remaining_data.putString("apiKey", apiKey)
                            remaining_data.putString("cloudName", cloudName)
                            postMedia(remaining_data)
                        } else {
                            progress_bar_signup!!.visibility = View.GONE
                            tV_signup!!.visibility = View.VISIBLE
                            CommonClass.showTopSnackBar(rL_rootElement, response.message)
                        }
                    }

                    override fun onError(error: String?, tag: String?) {
                        progress_bar_signup!!.visibility = View.GONE
                        tV_signup!!.visibility = View.VISIBLE
                        CommonClass.showTopSnackBar(rL_rootElement, error)
                    }
                })
            } else CommonClass.showTopSnackBar(rL_rootElement, getResources().getString(R.string.NoInternetAccess))
        }

    /**
     * <h>PostMedia</h>
     *
     *
     * In this method we are uploading file(Image or video) to the cloudinary server.
     * in Respose we will get Url. Once we get Url of the image then call registration
     * api.
     *
     *
     * @param remaining_data The bundle of cloudinary details data
     */
    private fun postMedia(remaining_data: Bundle?) {
        /*
         * Creating the path holder.*/
        val cloudData = CloudData()
        //cloudData.setPath(handleCameraEvent.getRealPathFromURI());
        cloudData.path = mFile!!.absolutePath
        cloudData.isVideo = false
        object : UploadToCloudinary(remaining_data!!, cloudData) {
            override fun callBack(resultData: Map<*, *>?) {
                if (resultData != null) {
                    val main_url = resultData["url"] as String?
                    println("$TAG main url=$main_url")
                    profilePicUrl = main_url
                    //                    submitRegistration();
                    generateOtp(main_url)
                    //getCurrentLocation(main_url);
                }
            }

            override fun errorCallBack(error: String?) {}
        }
    }

    /**
     * <h>ShowKeyboard</h>
     *
     *
     * In this method we used to open device keypad when user
     * click on search iocn and close when user click close
     * search button.
     *
     * @param flag This is integer valuew to open or close keypad
     */
    private fun showKeyboard(flag: Int) {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
        imm!!.toggleSoftInput(flag, 0)
    }

    override fun onBackPressed() {
        finish()
        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate)
    }

   override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        println(TAG + " " + "on activity result..." + " " + "requestCode=" + requestCode + " " + "result code=" + requestCode + "data=" + data)
        val bitmap: Bitmap?
        when (requestCode) {
            VariableConstants.SELECT_GALLERY_IMG_REQ_CODE -> {
                if (resultCode != RESULT_OK) {
                    return
                }
                try {
                    val inputStream: InputStream = getContentResolver().openInputStream(data!!.data!!)!!
                    val fileOutputStream = FileOutputStream(mFile)
                    mHandleCameraEvents!!.copyStream(inputStream, fileOutputStream)
                    fileOutputStream.close()
                    assert(inputStream != null)
                    inputStream.close()
                    mHandleCameraEvents!!.startCropImage()
                } catch (e: Exception) {
                    Log.e(TAG, "Error while creating temp file", e)
                }
            }
            VariableConstants.CAMERA_CAPTURE -> {
                if (resultCode != RESULT_OK) {
                    return
                }
                mHandleCameraEvents!!.startCropImage()
            }
            VariableConstants.PIC_CROP -> {
                if (data == null) {
                    return
                }
//                val path = data.getStringExtra(IMAGE_PATH) ?: return
                isPictureTaken = true
                iv_edit_icon!!.visibility = View.VISIBLE
                bitmap = BitmapFactory.decodeFile(mFile!!.path)
                iV_profile_pic!!.setImageBitmap(mHandleCameraEvents!!.getCircleBitmap(bitmap))
            }
            VariableConstants.NUMBER_VERIFICATION_REQ_CODE -> if (data != null) {
                val isToFinishLoginSignup = data.getBooleanExtra("isToFinishLoginSignup", true)
                println(TAG + "isToFinishLoginSignup=" + isToFinishLoginSignup)
                if (isToFinishLoginSignup) {
                    val intent = Intent()
                    intent.putExtra("isToFinishLandingScreen", true)
                    intent.putExtra("isFromSignup", true)
                    setResult(VariableConstants.LOGIN_SIGNUP_REQ_CODE, intent)
                    finish()
                }
            }
            VariableConstants.PRIVACY_POLICY -> if (data != null) {
                val accept = data.getBooleanExtra("accept", false)
                if (accept) {
                    login_views!!.visibility = View.GONE
                    signup_views!!.visibility = View.VISIBLE
                }
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            VariableConstants.PERMISSION_REQUEST_CODE -> {
                println("grant result=" + grantResults!!.size)
                if (grantResults.size > 0) {
                    var count = 0
                    while (count < grantResults.size) {
                        if (grantResults[count] != PackageManager.PERMISSION_GRANTED) runTimePermission!!.allowPermissionAlert(permissions!![count])
                        count++
                    }
                    if (runTimePermission!!.checkPermissions(permissionsArray!!)) {
                        if (isFromLocation) {
                            currentLocation
                        } else if (isAskLocationPermission) {
                            Log.d("Location Permission", "Granted")
                        } else chooseImage()
                    }
                }
            }
        }
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
        if (imm != null && getCurrentFocus() != null) imm.hideSoftInputFromWindow(getCurrentFocus()!!.getWindowToken(), 0)
        return true
    }

    /*
     * Initialization of the user details .*/
    private fun initUserDetails(profile_Url: String?, userId: String, email: String, userName: String, token: String) {
      try {
          if (AppController.instance != null) {
              val db = AppController.instance?.dbController
              val map: HashMap<String, Any> = HashMap()
              if (profile_Url != null && !profile_Url.isEmpty()) {
                  map["userImageUrl"] = profile_Url
              } else {
                  map["userImageUrl"] = ""
              }
              map["userIdentifier"] = email
              map["userId"] = userId
              map["userName"] = userName
              map["apiToken"] = token
              if (userId == null || userId.isEmpty()) {
                  Toast.makeText(this, R.string.userIdtext, Toast.LENGTH_SHORT).show()
                  return
              }
              if (!db!!.checkUserDocExists(AppController.instance?.indexDocId, userId)) {
                  val userDocId = db.createUserInformationDocument(map)
                  db.addToIndexDocument(AppController.instance?.indexDocId, userId, userDocId)
              } else {
                  db.updateUserDetails(db.getUserDocId(userId, AppController.instance?.indexDocId!!), map)
              }
              db.updateIndexDocumentOnSignIn(AppController.instance?.indexDocId, userId)
              AppController.instance?.setSignedIn(true, userId, userName, email)
              AppController.instance?.isSignStatusChanged = true
          }
      }catch (e:Exception){e.printStackTrace()}
    }

    private fun submitRegistration() {
        registerFromEmailService()
        //            LocationManager lm = (LocationManager)mActivity.getSystemService(Context.LOCATION_SERVICE);
//            boolean isLocationEnabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
//            System.out.println(TAG+" "+"is location enabled="+ isLocationEnabled +" "+"is permission allowed="+runTimePermission.checkPermissions(permissionsArray));
//
//            if (isLocationEnabled && runTimePermission.checkPermissions(permissionsArray))
//                getCurrentLocation();
//            else
//                registerFromEmailService();
    }

    private fun registerFromEmailService() {
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            val request_Data = JSONObject()
            try {
                progress_bar_signup!!.visibility = View.VISIBLE
                request_Data.put("signupType", signUpType) // mandatory
                request_Data.put("username", eT_userName!!.text.toString()) // mandatory
                request_Data.put("deviceType", VariableConstants.DEVICE_TYPE) // mandatory
                request_Data.put("pushToken", mSessionManager!!.pushToken) // mandatory
                request_Data.put("deviceId", mSessionManager!!.deviceId) // mandatory
                request_Data.put("profilePicUrl", profilePicUrl)
                request_Data.put("fullName", eT_fullName!!.text.toString())
                request_Data.put("location", address)
                request_Data.put("latitude", currentLat)
                request_Data.put("longitude", currentLng)
                request_Data.put("city", city)
                request_Data.put("countrySname", countryShortName)
                request_Data.put("password", eT_password!!.text.toString()) // mandatory
                request_Data.put("phoneNumber", countryIsoNumber + eT_mobileNo!!.text.toString()) // mandatory
                request_Data.put("email", eT_emailId!!.text.toString()) // mandatory
                //request_Data.put("googleToken",googleToken); // mandatory when sign up with google
                request_Data.put("googleToken", "ya29.GlvvBPnlNioB1hN-F_aJIOhQKG8iy91yd0pt8M5z4E2ykapJbMlPgLHmPKwr2UHjtm54XPA2OgbYYVQqNy81hYphjMExFML4ampiMsGfM5rJ1jxBRCRgWEz5v-9V") // mandatory when sign up with google
                request_Data.put("googleId", googleId) // mandatory when sign up with google
                request_Data.put("facebookId", fbId)
                request_Data.put("accessToken", fb_accessToken)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            OkHttp3Connection.doOkHttp3Connection("", ApiUrl.SIGN_UP, OkHttp3Connection.Request_type.POST, request_Data, object : OkHttp3RequestCallback {
                override fun onSuccess(result: String?, tag: String?) {
                    println("$TAG sign up response=$result")
                    /*
                     * handel response.*/
                    val signUpMainPojo: SignUpMainPojo
                    val gson = Gson()
                    signUpMainPojo = gson.fromJson(result, SignUpMainPojo::class.java)
                    progress_bar_signup!!.visibility = View.GONE
                    when (signUpMainPojo.code) {
                        "200" -> {
                            mSessionManager!!.setmqttId(signUpMainPojo.response!!.mqttId)
                            mSessionManager!!.isUserLoggedIn = true
                            mSessionManager!!.authToken = signUpMainPojo.response!!.authToken
                            mSessionManager!!.userName = signUpMainPojo.response!!.username
                            mSessionManager!!.userImage = profilePicUrl
                            mSessionManager!!.userId = signUpMainPojo.response!!.userId
                            VariableConstants.IS_TO_SHOW_START_BROWSING = true
                            initUserDetails(profilePicUrl, signUpMainPojo.response!!.mqttId!!, eT_emailId!!.text.toString(), signUpMainPojo.response!!.username, signUpMainPojo.response!!.authToken)
                            // call this method to set device info to server
                            logDeviceInfo(signUpMainPojo.response!!.authToken)
                        }
                        else -> {
                            progress_bar_signup!!.visibility = View.GONE
                            CommonClass.showTopSnackBar(rL_rootElement, signUpMainPojo.message)
                        }
                    }
                }

                override fun onError(error: String?, tag: String?) {
                    progress_bar_signup!!.visibility = View.GONE
                    CommonClass.showTopSnackBar(rL_rootElement, error)
                }
            })
        } else {
            CommonClass.showTopSnackBar(rL_rootElement, getResources().getString(R.string.NoInternetAccess))
        }
    }

    companion object {
        private val TAG: String? = LoginOrSignupActivity::class.java.simpleName
    }
}