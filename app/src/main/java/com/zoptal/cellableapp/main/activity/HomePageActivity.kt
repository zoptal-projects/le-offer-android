package com.zoptal.cellableapp.main.activity

import android.app.Activity
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Typeface
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.android.material.bottomnavigation.BottomNavigationItemView
import com.google.android.material.bottomnavigation.BottomNavigationMenuView
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks
import com.squareup.otto.Subscribe
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.badgeView.Badge
import com.zoptal.cellableapp.badgeView.BadgeView
import com.zoptal.cellableapp.fcm_push_notification.Config
import com.zoptal.cellableapp.fcm_push_notification.NotificationMessageDialog
import com.zoptal.cellableapp.fcm_push_notification.NotificationUtils.Companion.clearNotifications
import com.zoptal.cellableapp.main.activity.products.ProductDetailsActivity
import com.zoptal.cellableapp.main.camera_kit.CameraKitActivity
import com.zoptal.cellableapp.main.tab_fragments.ChatFrag
import com.zoptal.cellableapp.main.tab_fragments.HomeFrag
import com.zoptal.cellableapp.main.tab_fragments.ProfileFrag
import com.zoptal.cellableapp.main.tab_fragments.SocialFrag
import com.zoptal.cellableapp.mqttchat.AppController
import com.zoptal.cellableapp.mqttchat.Utilities.MqttEvents
import com.zoptal.cellableapp.mqttchat.Utilities.TimestampSorter
import com.zoptal.cellableapp.utility.CommonClass
import com.zoptal.cellableapp.utility.DialogBox
import com.zoptal.cellableapp.utility.SessionManager
import com.zoptal.cellableapp.utility.VariableConstants
import org.json.JSONObject
import java.util.*

/**
 * <h>HomePageActivity</h>
 *
 *
 * In this class we have the bottom Navigation layout for various fragment transition.
 *
 * @since 3/31/2017
 */
class HomePageActivity : AppCompatActivity() {
    @JvmField
    var verifyLoginWithFacebook: VerifyLoginWithFacebook? = null
    private var mActivity: Activity? = null
    private var mSessionManager: SessionManager? = null
    private var homeFrag: Fragment? = null
    private var socialFarg: Fragment? = null
    private var chatFarg: Fragment? = null
    private var myProfileFrag: Fragment? = null
    private var isToHighLightTab = false
    private var mNotificationMessageDialog: NotificationMessageDialog? = null
    @JvmField
    var bottomNavigationView: BottomNavigationView? = null
    @JvmField
    var rL_rootElement: RelativeLayout? = null
    private var isToStartActivity = false
    @JvmField
    var postResultData = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        try {
            super.onCreate(savedInstanceState ?: Bundle())
            setContentView(R.layout.activity_homepage)
            overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale)
            mActivity = this@HomePageActivity
            isToStartActivity = true
            //FacebookSdk.sdkInitialize(mActivity);
            //share();
            //new RequestTask().execute("https://play.google.com/store/apps/details?id=com.yelo.com&hl=en");
            isToHighLightTab = false
            mSessionManager = SessionManager(mActivity!!)
            mNotificationMessageDialog = NotificationMessageDialog(mActivity)
            rL_rootElement = findViewById<View>(R.id.rL_rootElement) as RelativeLayout

            // receive datas
            val intent = intent
            if (intent != null) {
                if(intent.hasExtra("postResultData")) {
                    postResultData = intent.getStringExtra("postResultData")
                }
                if(intent.hasExtra("isFromSignup")) {
                    val isFromSignup = intent.getBooleanExtra("isFromSignup", false)

                    if (isFromSignup) DialogBox(mActivity!!).startBrowsingDialog()
                }
            }
            FirebaseDynamicLinks.getInstance()
                    .getDynamicLink(getIntent())
                    .addOnSuccessListener(this) { pendingDynamicLinkData -> // Get deep link from result (may be null if no link is found)
                        var deepLink: Uri? = null
                        if (pendingDynamicLinkData != null) {
                            deepLink = pendingDynamicLinkData.link
                            Log.d("exe", "deepLink$deepLink")
                            val postId = deepLink.lastPathSegment
                            Log.d("exe", "deepLinkpostId$postId")
                            callProductApi(postId)
                        }
                    }
                    .addOnFailureListener(this) { e -> Log.d("exe", "getDynamicLink$e") }
            initBottomNavView()
            /*
        * Updating the bagged count.*/setChatCount(countUnreadChat())
        }catch (e:Exception){e.printStackTrace()}
    }

    private fun callProductApi(postId: String?) {
        if (postId != null && !postId.isEmpty()) {
            val intent = Intent(this, ProductDetailsActivity::class.java)
            intent.putExtra("postId", postId)
            startActivity(intent)
        }
    }

    override fun onResume() {
        super.onResume()
        isToStartActivity = true
        AppController.bus.register(this)
        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.REGISTRATION_COMPLETE))
        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.PUSH_NOTIFICATION))
        // clear the notification area when the app is opened
        clearNotifications(applicationContext)
        val bundle = intent.extras
        if (bundle != null) {
            if (bundle.containsKey("openChat") && bundle.getBoolean("openChat")) {
                if (CommonClass.isBackFromChat) {
                    CommonClass.isBackFromChat = false
                    val view = bottomNavigationView!!.findViewById<View>(R.id.nav_chat)
                    view.performClick()
                }
            }
        }

        //In this we put bagded count when back after login
        if (CommonClass.isBackFromLogin) {
            if (mSessionManager!!.isUserLoggedIn) {
                val transaction = supportFragmentManager.beginTransaction()
                if (chatFarg!!.isAdded) transaction.show(chatFarg!!) else transaction.add(R.id.frame_layout, chatFarg!!)
                if (socialFarg!!.isAdded) transaction.hide(socialFarg!!)
                if (myProfileFrag!!.isAdded) transaction.hide(myProfileFrag!!)
                if (homeFrag!!.isAdded) transaction.hide(homeFrag!!)
                transaction.commit()
                val handler = Handler()
                handler.postDelayed({ //Do something after 100ms
                    val transaction1 = supportFragmentManager.beginTransaction()
                    if (chatFarg!!.isAdded) transaction1.hide(chatFarg!!)
                    if (socialFarg!!.isAdded) transaction1.hide(socialFarg!!)
                    if (myProfileFrag!!.isAdded) transaction1.hide(myProfileFrag!!)
                    if (homeFrag!!.isAdded) transaction1.show(homeFrag!!) else transaction1.add(R.id.frame_layout, homeFrag!!)
                    transaction1.commit()
                }, 100)
            }
        }

        /*
        * Updating the bagged count.*/setChatCount(countUnreadChat())
    }

    override fun onPause() {
        AppController.bus.unregister(this)
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!)
        super.onPause()
    }

    /**
     * <h>InitBottomNavView</h>
     *
     *
     * In this method we used to initialize BottomNavigationView variables.
     *
     */
    private fun initBottomNavView() {
        bottomNavigationView = findViewById<View>(R.id.navigation) as BottomNavigationView
        val font = Typeface.createFromAsset(assets, "fonts/roboto-regular.ttf")
        disableAllAnimation()
        //        bottomNavigationView.setTypeface(font);
//        bottomNavigationView.setIconSize(26,26);
//        bottomNavigationView.setTextSize(12);
        homeFrag = HomeFrag()
        socialFarg = SocialFrag()
        chatFarg = ChatFrag()
        myProfileFrag = ProfileFrag()
        val transaction = supportFragmentManager.beginTransaction()
        val nav_menu = bottomNavigationView!!.menu
        nav_menu.getItem(0).setIcon(R.drawable.tabbar_home_onn)
        bottomNavigationView!!.setOnNavigationItemSelectedListener { item ->
            val transaction = supportFragmentManager.beginTransaction()
            when (item.itemId) {
                R.id.nav_home -> {
                    setBottomTabIcon(nav_menu, R.drawable.tabbar_home_onn, R.drawable.tabbar_social_off, R.drawable.tab_bar_chat_off, R.drawable.tabbar_profile_off)
                    println(TAG + " " + "home frag=" + homeFrag!!.isAdded())
                    isToHighLightTab = true
                    if (homeFrag!!.isAdded()) transaction.show(homeFrag!!) else transaction.add(R.id.frame_layout, homeFrag!!, "HomeFragment")
                    if (socialFarg!!.isAdded()) transaction.hide(socialFarg!!)
                    if (chatFarg!!.isAdded()) transaction.hide(chatFarg!!)
                    if (myProfileFrag!!.isAdded()) transaction.hide(myProfileFrag!!)
                }
                R.id.nav_social -> if (mSessionManager!!.isUserLoggedIn) {
                    setBottomTabIcon(nav_menu, R.drawable.tabbar_home_off, R.drawable.tabbar_social_on, R.drawable.tab_bar_chat_off, R.drawable.tabbar_profile_off)
                    isToHighLightTab = true
                    if (socialFarg!!.isAdded()) transaction.show(socialFarg!!) else transaction.add(R.id.frame_layout, socialFarg!!, "SocialFrag")
                    if (chatFarg!!.isAdded()) transaction.hide(chatFarg!!)
                    if (myProfileFrag!!.isAdded()) transaction.hide(myProfileFrag!!)
                    if (homeFrag!!.isAdded()) transaction.hide(homeFrag!!)
                } else {
                    if (isToStartActivity) {
                        startActivityForResult(Intent(mActivity, LandingActivity::class.java), VariableConstants.LANDING_REQ_CODE)
                        isToStartActivity = false
                    }
                }
                R.id.nav_camera -> {
                    //item.setIcon(R.drawable.tab_bar_camera_on);
                    isToHighLightTab = false
                    if (mSessionManager!!.isUserLoggedIn) {
                        /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                                    {
                                        if (isToStartActivity) {
                                            startActivity(new Intent(mActivity, Camera2Activity.class));
                                            isToStartActivity = false;
                                        }
                                    }
                                    else
                                    {*/
                        if (isToStartActivity) {
                            isToStartActivity = false
                            startActivity(Intent(mActivity, CameraKitActivity::class.java))
                        }
                        //}
                    } else {
                        if (isToStartActivity) {
                            isToStartActivity = false
                            startActivityForResult(Intent(mActivity, LandingActivity::class.java), VariableConstants.LANDING_REQ_CODE)
                        }
                    }
                }
                R.id.nav_chat -> if (mSessionManager!!.isUserLoggedIn) {
                    setBottomTabIcon(nav_menu, R.drawable.tabbar_home_off, R.drawable.tabbar_social_off, R.drawable.tab_bar_chat_onn, R.drawable.tabbar_profile_off)
                    isToHighLightTab = true
                    if (chatFarg!!.isAdded()) transaction.show(chatFarg!!) else transaction.add(R.id.frame_layout, chatFarg!!)
                    if (socialFarg!!.isAdded()) transaction.hide(socialFarg!!)
                    if (myProfileFrag!!.isAdded()) transaction.hide(myProfileFrag!!)
                    if (homeFrag!!.isAdded()) transaction.hide(homeFrag!!)
                } else {
                    if (isToStartActivity) {
                        startActivityForResult(Intent(mActivity, LandingActivity::class.java), VariableConstants.LANDING_REQ_CODE)
                        isToStartActivity = false
                    }
                }
                R.id.nav_profile -> if (mSessionManager!!.isUserLoggedIn) {
                    setBottomTabIcon(nav_menu, R.drawable.tabbar_home_off, R.drawable.tabbar_social_off, R.drawable.tab_bar_chat_off, R.drawable.tabbar_profile_on)
                    isToHighLightTab = true
                    if (myProfileFrag!!.isAdded()) transaction.show(myProfileFrag!!) else transaction.add(R.id.frame_layout, myProfileFrag!!)
                    if (socialFarg!!.isAdded()) transaction.hide(socialFarg!!)
                    if (chatFarg!!.isAdded()) transaction.hide(chatFarg!!)
                    if (homeFrag!!.isAdded()) transaction.hide(homeFrag!!)
                } else {
                    if (isToStartActivity) {
                        isToStartActivity = false
                        startActivityForResult(Intent(mActivity, LandingActivity::class.java), VariableConstants.LANDING_REQ_CODE)
                    }
                }
            }
            transaction.commit()
            isToHighLightTab
        }
        transaction.add(R.id.frame_layout, homeFrag!!)
        transaction.commit()
    }

    /**
     * <h>DisableAllAnimation</h>
     *
     *
     * In this method we used to disable auto-animation from bottom navigation view like
     * shifting of icons and text.
     *
     */
    private fun disableAllAnimation() {
//        bottomNavigationView.enableAnimation(false);
//        bottomNavigationView.enableShiftingMode(false);
//        bottomNavigationView.enableItemShiftingMode(false);
    }

    /**
     * <h>SetChatCount</h>
     *
     *
     * In this method we used to set the badge count(unread chat count) on the
     * chat icon.
     *
     */
    var baage: Badge? = null
        private set

    fun setChatCount(count: Int) {
        try {
            if (mSessionManager!!.isUserLoggedIn) {
//                addBadgeView(countUnreadChat());
                Log.d("dfgt", "" + count)
                if (baage != null) {
                    baage!!.setBadgeNumber(count)
                } else {
                    val menuView = bottomNavigationView!!.getChildAt(0) as BottomNavigationMenuView
                    val itemView = menuView.getChildAt(3) as BottomNavigationItemView
                    baage = BadgeView(this)
                            .setBadgeNumber(count)
                            .setGravityOffset(14f, 4f, true)
                            .setBadgeTextSize(8f, true)
                            .setBadgeTextColor(ContextCompat.getColor(this, R.color.white))
                            .setBadgeBackgroundColor(ContextCompat.getColor(this, R.color.pink_color))
                            .bindTarget(itemView)
                    //.bindTarget(bottomNavigationView.getChildAt(3));
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private var notificationBadge: View? = null
    private fun addBadgeView(count: Int) {
        try {
            val menuView = bottomNavigationView!!.getChildAt(0) as BottomNavigationMenuView
            val itemView = menuView.getChildAt(3) as BottomNavigationItemView
            notificationBadge = LayoutInflater.from(this).inflate(R.layout.view_notification_badge, menuView, false)
            val textView = notificationBadge!!.findViewById<TextView>(R.id.badge)
            textView.text = "15"
            itemView.addView(notificationBadge)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * <h>SetBottomTabIcon</h>
     *
     *
     * In this method we used to set the selected and unselected icon of bottom navigation view.
     *
     * @param nav_menu The Navigation Menu
     * @param homeIcon First Tab i.e HomePage icon
     * @param socialIcon The Second Tab i.e SocialPage icon
     * @param chatIcon The Fourth Tab i.e Camera Page icon
     * @param profileIcon The Fifth Tab i.e Profile page icon
     */
    private fun setBottomTabIcon(nav_menu: Menu, homeIcon: Int, socialIcon: Int, chatIcon: Int, profileIcon: Int) {
        nav_menu.getItem(0).setIcon(homeIcon)
        nav_menu.getItem(1).setIcon(socialIcon)
        nav_menu.getItem(3).setIcon(chatIcon)
        nav_menu.getItem(4).setIcon(profileIcon)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        println("$TAG onactivity result res code=$resultCode req code=$requestCode data=$data")
        if (data != null) {
            when (requestCode) {
                VariableConstants.LANDING_REQ_CODE -> {
                    val isToRefreshHomePage = data.getBooleanExtra("isToRefreshHomePage", false)
                    val isFromSignup = data.getBooleanExtra("isFromSignup", false)
                    println("$TAG isToRefreshHomePage=$isToRefreshHomePage isFromSignup=$isFromSignup")
                    if (isToRefreshHomePage) {
                        isToStartActivity = false
                        val intent = intent
                        intent.putExtra("isFromSignup", isFromSignup)
                        startActivity(intent)
                        finish()
                    }
                }
            }
            if (verifyLoginWithFacebook != null) verifyLoginWithFacebook!!.fbOnActivityResult(requestCode, resultCode, data)
            for (fragment in supportFragmentManager.fragments) {
                fragment.onActivityResult(requestCode, resultCode, data)
            }
        }
    }

    /**
     * To fetch the chats from the couchdb, stored locally
     */
    fun countUnreadChat(): Int {
        var total_count = 0
        /*if(mSessionManager.getIsUserLoggedIn())
        {
            return total_count;
        }*/
        if(AppController.instance !=null) {
            val db = AppController.instance?.dbController
            val map = db!!.getAllChatDetails(AppController.instance?.chatDocId)
            if (map != null) {
                val receiverUidArray = map["receiverUidArray"] as ArrayList<String>?
                val receiverDocIdArray = map["receiverDocIdArray"] as ArrayList<String>?
                val chats = ArrayList<Map<String, Any>>()
                for (i in receiverUidArray!!.indices) {
                    chats.add(db.getParticularChatInfo(receiverDocIdArray!![i])!!)
                    Collections.sort(chats, TimestampSorter())
                }
                for (i in chats.indices) {
                    val temp_count = parseData(chats[i])
                    total_count = total_count + temp_count
                }
            }
        }


        return total_count
    }

    /*
    *Collect unread count*/
    private fun parseData(chat_item: Map<String, Any>): Int {
        var count = 0
        val hasNewMessage = (chat_item["hasNewMessage"] as Boolean?)!!
        if (hasNewMessage) {
            try {
                count = (chat_item["newMessageCount"] as String).toInt()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        return count
    }

    @Subscribe
    fun getMessage(`object`: JSONObject) {
        try {
            if (`object`.getString("eventName") == MqttEvents.Message.value + "/" + AppController.instance?.userId || `object`.getString("eventName") == MqttEvents.OfferMessage.value + "/" + AppController.instance?.userId) {
                var count = baage!!.badgeNumber
                count = count + 1
                setChatCount(count)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    var doubleBackToExitPressedOnce = false
    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed()
            return
        }
        doubleBackToExitPressedOnce = true
        Toast.makeText(this, resources.getString(R.string.err_double_back), Toast.LENGTH_SHORT).show()
        Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 2000)
    }

    companion object {
        private val TAG: String? = HomePageActivity::class.java.simpleName
    }
}