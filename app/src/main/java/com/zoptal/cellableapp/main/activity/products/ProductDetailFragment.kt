package com.zoptal.cellableapp.main.activity.products

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.text.Spannable
import android.text.SpannableString
import android.view.*
import android.widget.*
import androidx.annotation.NonNull
import androidx.annotation.Nullable

import androidx.core.content.ContextCompat

import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.facebook.CallbackManager
import com.facebook.FacebookSdk
import com.facebook.share.model.ShareLinkContent
import com.facebook.share.widget.ShareDialog
import com.google.android.gms.ads.AdView
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.dynamiclinks.DynamicLink.AndroidParameters
import com.google.firebase.dynamiclinks.DynamicLink.SocialMetaTagParameters
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import com.zoptal.cellableapp.Face_book_manger.Facebook_login
import com.zoptal.cellableapp.Face_book_manger.Facebook_share_mamager
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.Uploader.ProductImageDatas
import com.zoptal.cellableapp.adapter.ProductDetailImagePagerAdapter
import com.zoptal.cellableapp.adapter.ProductDetailsSwapAdapter
import com.zoptal.cellableapp.adapter.ProductInforRvAdap
import com.zoptal.cellableapp.custom_scroll_view.AlphaForeGroundColorSpan
import com.zoptal.cellableapp.event_bus.EventBusDatasHandler
import com.zoptal.cellableapp.fcm_push_notification.NotificationMessageDialog
import com.zoptal.cellableapp.get_current_location.FusedLocationReceiver
import com.zoptal.cellableapp.get_current_location.FusedLocationService
import com.zoptal.cellableapp.main.activity.*
import com.zoptal.cellableapp.mqttchat.Activities.ChatMessageScreen
import com.zoptal.cellableapp.mqttchat.AppController
import com.zoptal.cellableapp.pojo_class.home_explore_pojo.ExploreLikedByUsersDatas
import com.zoptal.cellableapp.pojo_class.home_explore_pojo.ExploreResponseDatas
import com.zoptal.cellableapp.pojo_class.product_details_pojo.PostFilter
import com.zoptal.cellableapp.pojo_class.product_details_pojo.ProductDetailsMain
import com.zoptal.cellableapp.pojo_class.product_details_pojo.ProductResponseDatas
import com.zoptal.cellableapp.pojo_class.product_details_pojo.SwapPost
import com.zoptal.cellableapp.pull_to_zoom.PullToZoomScrollFragment

import com.zoptal.cellableapp.utility.*
import com.zoptal.cellableapp.utility.OkHttp3Connection.OkHttp3RequestCallback
import org.json.JSONException
import org.json.JSONObject
import java.util.*

/**
 * Created by embed on 20/8/18.
 */
class ProductDetailFragment : Fragment(), View.OnClickListener, OnMapReadyCallback {
    private var mActivity: Activity? = null
    private var mSessionManager: SessionManager? = null
    private var apiCall: ApiCall? = null
    private var aL_multipleImages: ArrayList<String>? = null
    private var fromChatScreen = "0"
    private var receiverMqttId: String? = ""
    private var likeStatus: String? = ""
    private var postId = ""
    private var postsType = ""
    private var productImage: String? = ""
    private var thumbnailImageUrl = ""
    private var productName: String? = ""
    private var membername: String? = null
    private var followRequestStatus: String? = ""
    private var likesCount: String? = ""
    private var currency: String? = ""
    private var price: String? = ""
    private var postedOn: String? = ""
    private var description: String? = ""
    private var condition: String? = ""
    private var place: String? = ""
    private var latitude = ""
    private var longitude = ""
    private var cityName = ""
    private var countryCode = ""
    private var currentLat = ""
    private var currentLng = ""
    private var category: String? = ""
    private var subCategory: String? = ""
    private var containerWidth: String? = ""
    private var containerHeight = ""
    private var image1: String? = ""
    private var image1thumbnail = ""
    private var image2: String? = ""
    private var image2thumbnail = ""
    private var image3: String? = ""
    private var image3thumbnail = ""
    private var image4: String? = ""
    private var image4thumbnail = ""
    private var memberProfilePicUrl: String? = ""
    private var clickCount: String? = ""
    private var negotiable: String? = ""
    private var cloudinaryPublicId = ""
    private var cloudinaryPublicId1 = ""
    private var cloudinaryPublicId2 = ""
    private var cloudinaryPublicId3 = ""
    private var cloudinaryPublicId4 = ""
    private var getCityName: String? = ""
    private var getCountryName: String? = ""
    private var aL_likedByUsers: ArrayList<ExploreLikedByUsersDatas>? = null
    private var postFilters: ArrayList<PostFilter>? = null
    private var locationService: FusedLocationService? = null
    private var toolbar_shadow: View? = null
    private var tV_negotiable: TextView? = null
    private var iV_back_icon: ImageView? = null
    private var iV_option_menu: ImageView? = null
    private var rL_report_item: RelativeLayout? = null
    private var myClipboard: ClipboardManager? = null
    private var myClip: ClipData? = null
    private var isToMakeOffer = false
    private var  permissionsArray = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
    private var runTimePermission: RunTimePermission? = null

    // Pull To Zoom and fading action bar var
    private var scrollView_itemDetails: PullToZoomScrollFragment? = null
    @JvmField
    var rL_actionBar: RelativeLayout? = null
    var product_rootview: RelativeLayout? = null
    var tV_ProductName: TextView? = null
    private var mAlphaForegroundColorSpan: AlphaForeGroundColorSpan? = null
    private var mSpannableString: SpannableString? = null

    // Pull to zoom layout var
    var iV_productImage: ImageView? = null

    // product details content view layout var
    private var linear_like_product: LinearLayout? = null
    private var rL_follow: RelativeLayout? = null
    private var tV_productname: TextView? = null
    private var tV_category: TextView? = null
    private var tV_subCategory: TextView? = null
    private var tV_postedOn: TextView? = null
    private var tV_posted_by: TextView? = null
    private var tV_follow: TextView? = null
    private var tV_description: TextView? = null
    private var tV_condition: TextView? = null
    private var tV_location: TextView? = null
    private var tV_currency: TextView? = null
    private var tV_productprice: TextView? = null
    private var tV_like_count: TextView? = null
    private var tV_view_count: TextView? = null
    private var tV_makeoffer: TextView? = null
    private var iV_soldby: ImageView? = null
    private var like_item_icon: ImageView? = null
    private var iV_followed_list: ImageView? = null
    private var linear_followed_images: LinearLayout? = null
    private var mNotificationMessageDialog: NotificationMessageDialog? = null
    private var isToSellItAgain = false
    private var mDialogBox: DialogBox? = null
    private var productResponse: ProductResponseDatas? = null
    private var mEventBusDatasHandler: EventBusDatasHandler? = null
    private var mProgress_bar: ProgressBar? = null

    //..viewPager with dots component
    private var vP_productImage: ViewPager? = null

    private var dots: Array<ImageView?> ? = null
    var sliderDotspanel: LinearLayout? = null

    //..fb background sharing
    private val facebook_share_mamager: Facebook_share_mamager? = null
    private val facebook_login: Facebook_login? = null
    private val callbackManager: CallbackManager? = null
    private var mAdView: AdView? = null
    private var sold = 0
    private var rV_info: RecyclerView? = null
    private var linear_info: LinearLayout? = null
    private var productInforRvAdap: ProductInforRvAdap? = null
    private var iWantRv: RecyclerView? = null
    private val swapPostList = ArrayList<SwapPost>()
    private var swapItemAdapter: ProductDetailsSwapAdapter? = null
    private var isSwap = 0
    private val isFrom = 1
    private var rL_price: RelativeLayout? = null
    private var rL_swap: RelativeLayout? = null
    private var rL_price_content: RelativeLayout? = null
    private var rl_progressBar: RelativeLayout? = null
    private var tV_currency_content: TextView? = null
    private var tV_price_content: TextView? = null
    private var iwantTv: TextView? = null
    private var tV_negotiable_content: TextView? = null
    private var isPromoted = 0
    private var mapFragment: SupportMapFragment? = null
    override fun onCreate(@Nullable savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        bundleData
    }

    //retrieves the thumbnail data
    val bundleData: Unit
        get() {
            val exploreData: ExploreResponseDatas
            //retrieves the thumbnail data
            if (getArguments() != null) {
                exploreData = getArguments()!!.getSerializable("exploreData") as ExploreResponseDatas
                productName = exploreData.productName
                category = exploreData.category
                subCategory = exploreData.subCategory
                likesCount = exploreData.likes
                likeStatus = exploreData.likeStatus
                productImage = exploreData.mainUrl
                thumbnailImageUrl = exploreData.thumbnailImageUrl
                isPromoted = exploreData.isPromoted!!.toInt()
                postId = exploreData.postId
                postsType = exploreData.postsType
                currency = exploreData.currency
                price = exploreData.price
                postedOn = exploreData.postedOn
                description = exploreData.description
                condition = exploreData.condition
                place = exploreData.place
                latitude = exploreData.latitude
                longitude = exploreData.longitude
                membername = exploreData.membername
                clickCount = exploreData.clickCount
                negotiable = exploreData.negotiable
                memberProfilePicUrl = exploreData.memberProfilePicUrl
                followRequestStatus = exploreData.followRequestStatus
                aL_likedByUsers = exploreData.likedByUsers
            }
        }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = LayoutInflater.from(getContext()).inflate(R.layout.fragment_product_detail, container, false)
        initVariable(view)
        return view
    }

    /**
     * <h>InitVariable</h>
     *
     *
     * In this method we used to initialize the xml variables.
     *
     */
    @SuppressLint("ClickableViewAccessibility")
    private fun initVariable(view: View) {
        mActivity = getActivity()
        mEventBusDatasHandler = EventBusDatasHandler(mActivity)
        mDialogBox = DialogBox(mActivity!!)
        //mDialogBox.showProgressDialog(mActivity.getResources().getString(R.string.Loading));

        runTimePermission = RunTimePermission(mActivity!!, permissionsArray, false)
        isToMakeOffer = false
        mNotificationMessageDialog = NotificationMessageDialog(mActivity)
        myClipboard = mActivity!!.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        mSessionManager = SessionManager(mActivity!!)
        apiCall = ApiCall(mActivity!!)
        CommonClass.statusBarColor(mActivity!!)
        aL_multipleImages = ArrayList()
        rL_actionBar = view.findViewById<View>(R.id.rL_actionBar) as RelativeLayout
        product_rootview = view.findViewById<View>(R.id.product_rootview) as RelativeLayout
        tV_ProductName = view.findViewById<View>(R.id.tV_ProductName) as TextView
        mAlphaForegroundColorSpan = AlphaForeGroundColorSpan(ContextCompat.getColor(mActivity!!, R.color.colorPrimary))
        val cd = ColorDrawable(ContextCompat.getColor(mActivity!!, R.color.white))
        rL_actionBar!!.background = cd
        cd.alpha = 0

        // toolbar bottom shadow
        toolbar_shadow = view.findViewById(R.id.toolbar_shadow)

        // scroll view
        scrollView_itemDetails = view.findViewById<View>(R.id.scrollView_itemDetails) as PullToZoomScrollFragment
        scrollView_itemDetails!!.setFragment(this)

        // Back button
        val rL_back_btn = view.findViewById<View>(R.id.rL_back_btn) as RelativeLayout
        rL_back_btn.setOnClickListener(this)

        // report item
        rL_report_item = view.findViewById<View>(R.id.rL_report_item) as RelativeLayout
        rL_report_item!!.setOnClickListener(this)

        // back icon
        iV_back_icon = view.findViewById<View>(R.id.iV_back_icon) as ImageView

        // option menu icon
        iV_option_menu = view.findViewById<View>(R.id.iV_option_menu) as ImageView

        // make offer
        tV_makeoffer = view.findViewById<View>(R.id.tV_makeoffer) as TextView
        tV_makeoffer!!.setOnClickListener(this)

        // currency and price
        tV_currency = view.findViewById<View>(R.id.tV_currency) as TextView
        tV_productprice = view.findViewById<View>(R.id.tV_productprice) as TextView
        tV_negotiable = view.findViewById<View>(R.id.tV_negotiable) as TextView

        // price view
        rL_price = view.findViewById<View>(R.id.rL_price) as RelativeLayout

        // swap button
        rL_swap = view.findViewById<View>(R.id.rL_swap) as RelativeLayout
        rL_swap!!.setOnClickListener(this)

        // swap data progress bar
        rl_progressBar = view.findViewById<View>(R.id.rl_progressBar) as RelativeLayout

        //edit Product
        val rL_editProduct = view.findViewById<View>(R.id.rL_editProduct) as RelativeLayout
        rL_editProduct.setOnClickListener(this)

        // chat
        val rL_chat_icon = view.findViewById<View>(R.id.rL_chat_icon) as RelativeLayout
        rL_chat_icon.setOnClickListener(this)
        CommonClass.setMargins(rL_chat_icon, 0, (CommonClass.getDeviceWidth(mActivity!!) / 1.4).toInt(), 0, 0)
        CommonClass.setMargins(rL_editProduct, 0, (CommonClass.getDeviceWidth(mActivity!!) / 1.4).toInt(), 0, 0)
        //rL_chat_icon.setOnTouchListener(this);
        fromChatScreen = "0"
        mSpannableString = if (productName != null && !productName!!.isEmpty()) {
            SpannableString(productName)
        } else {
            SpannableString("")
        }

        // Load content layout
        loadViewForCode()

        // make offer
        tV_makeoffer = view.findViewById<View>(R.id.tV_makeoffer) as TextView
        tV_makeoffer!!.setOnClickListener(this)
        if (mSessionManager!!.isUserLoggedIn && membername != null && membername == mSessionManager!!.userName) {
            isToSellItAgain = false
            tV_makeoffer!!.text = mActivity!!.resources.getString(R.string.mark_as_sold)
            rL_chat_icon.visibility = View.GONE
            rL_editProduct.visibility = View.VISIBLE
        }
        try {
            Glide.with(mActivity!!)
                    .load(productImage)
                    .asBitmap()
                    .placeholder(R.color.add_title)
                    .error(R.color.add_title)
                    .into(iV_productImage)
        } catch (e: OutOfMemoryError) {
            e.printStackTrace()
        }

        // call this which initialize
        initializeResposeDatas()

        //Set the background color to white
        val colorDrawable = ColorDrawable(Color.WHITE)
        product_rootview!!.background = colorDrawable
        currentLat = mSessionManager!!.currentLat!!
        currentLng = mSessionManager!!.currentLng!!
        if (isLocationFound(latitude, longitude)) {
            if (mSessionManager!!.isUserLoggedIn) getProductDetailsService(ApiUrl.GET_POST_BY_ID_USER) else getProductDetailsService(ApiUrl.GET_POST_BY_ID_GUEST)
        } else {
            if (runTimePermission!!.checkPermissions(permissionsArray)) currentLocation else runTimePermission!!.requestPermission()
        }
    }

    /**
     * <h>LoadViewForCode</h>
     *
     *
     * In this method we used to set header(Product image view) and
     * body i.e content view to the activity custom scroll view.
     *
     */
    private fun loadViewForCode() {
        val nullParent: ViewGroup? = null
        val zoomView = LayoutInflater.from(mActivity).inflate(R.layout.product_zoom_image_view, nullParent)
        val contentView = LayoutInflater.from(mActivity).inflate(R.layout.product_details_content_view, nullParent)
        iV_productImage = zoomView.findViewById<View>(R.id.iv_zoom) as ImageView
        vP_productImage = zoomView.findViewById<View>(R.id.vp_product_image) as ViewPager
        sliderDotspanel = zoomView.findViewById<View>(R.id.slider_dot) as LinearLayout
        initializeContentVariables(contentView)
        scrollView_itemDetails!!.setHeaderViewSize(CommonClass.getDeviceWidth(mActivity!!), (CommonClass.getDeviceWidth(mActivity!!) / 1.25).toInt())
        scrollView_itemDetails!!.zoomView = zoomView
        scrollView_itemDetails!!.setScrollContentView(contentView)
    }

    /**
     * <h>InitializeContentVariables</h>
     *
     *
     * In this method we used to initialize the content of the product details.
     *
     * @param contentView the view of product details
     */
    private fun initializeContentVariables(contentView: View) {
        // google banner ad mob
        mAdView = contentView.findViewById<View>(R.id.adView) as AdView
        //        showBannerMobAd();
        // Initialize xml Variables
        linear_like_product = contentView.findViewById<View>(R.id.linear_like_product) as LinearLayout
        linear_like_product!!.setOnClickListener(this)
        rL_follow = contentView.findViewById<View>(R.id.relative_follow) as RelativeLayout
        rL_follow!!.setOnClickListener(this)
        mProgress_bar = contentView.findViewById<View>(R.id.progress_bar) as ProgressBar
        mProgress_bar!!.visibility = View.VISIBLE

        // sold by profile
        val rL_sold_by = contentView.findViewById<View>(R.id.rL_sold_by) as RelativeLayout
        rL_sold_by.setOnClickListener(this)

        // share item
        val rL_share = contentView.findViewById<View>(R.id.rL_share) as RelativeLayout
        rL_share.setOnClickListener(this)

        // add review
        val rL_addToReview = contentView.findViewById<View>(R.id.rL_addToReview) as RelativeLayout
        rL_addToReview.setOnClickListener(this)
        tV_productname = contentView.findViewById<View>(R.id.tV_productname) as TextView
        tV_category = contentView.findViewById<View>(R.id.tV_category) as TextView
        tV_subCategory = contentView.findViewById<View>(R.id.tV_subCategory) as TextView
        tV_postedOn = contentView.findViewById<View>(R.id.tV_postedOn) as TextView
        tV_like_count = contentView.findViewById<View>(R.id.tV_like_count) as TextView
        tV_view_count = contentView.findViewById<View>(R.id.tV_view_count) as TextView
        linear_followed_images = contentView.findViewById<View>(R.id.linear_followed_images) as LinearLayout
        tV_posted_by = contentView.findViewById<View>(R.id.tV_posted_by) as TextView
        tV_follow = contentView.findViewById<View>(R.id.tV_follow) as TextView
        tV_description = contentView.findViewById<View>(R.id.tV_description) as TextView
        tV_condition = contentView.findViewById<View>(R.id.tV_condition) as TextView
        tV_location = contentView.findViewById<View>(R.id.tV_location) as TextView
        iV_soldby = contentView.findViewById<View>(R.id.iV_soldby) as ImageView
        iV_soldby!!.layoutParams.width = CommonClass.getDeviceWidth(mActivity!!) / 9
        iV_soldby!!.layoutParams.height = CommonClass.getDeviceWidth(mActivity!!) / 9
        like_item_icon = contentView.findViewById<View>(R.id.like_item_icon) as ImageView
        iV_followed_list = contentView.findViewById<View>(R.id.iV_followed_list) as ImageView
        //iv_staticMap= (ImageView) contentView.findViewById(R.id.iV_static_map);
        mapFragment = fragmentManager!!.findFragmentById(R.id.map) as SupportMapFragment?
        linear_info = contentView.findViewById<View>(R.id.linear_info) as LinearLayout
        rV_info = contentView.findViewById<View>(R.id.rV_info) as RecyclerView
        tV_currency_content = contentView.findViewById<View>(R.id.tV_currency) as TextView
        rL_price_content = contentView.findViewById<View>(R.id.rL_price) as RelativeLayout
        tV_price_content = contentView.findViewById<View>(R.id.tV_productprice) as TextView
        tV_negotiable_content = contentView.findViewById<View>(R.id.tV_negotiable) as TextView
        iwantTv = contentView.findViewById<View>(R.id.iwantTv) as TextView
        iWantRv = contentView.findViewById<View>(R.id.iWantRv) as RecyclerView
        swapItemAdapter = ProductDetailsSwapAdapter(swapPostList, mActivity!!)
        val mlayoutManager: RecyclerView.LayoutManager = LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false)
        iWantRv!!.setLayoutManager(mlayoutManager)
        iWantRv!!.setAdapter(swapItemAdapter)
    }
    //    // add google mob ad
    //    private void showBannerMobAd()
    //    {
    //        MobileAds.initialize(getContext(), mActivity.getResources().getString(R.string.adMobAppId));
    //        AdRequest adRequest = new AdRequest.Builder().build();
    //        mAdView.loadAd(adRequest);
    //        mAdView.setAdListener(new AdListener() {
    //            @Override
    //            public void onAdFailedToLoad(int i) {
    //                super.onAdFailedToLoad(i);
    //                Log.d("BannerAd",i+"");
    //            }
    //
    //            @Override
    //            public void onAdLoaded() {
    //                super.onAdLoaded();
    //                Log.d("BannerAd","loaded");
    //                mAdView.setVisibility(View.VISIBLE);
    //            }
    //        });
    //
    //    }// call product details api call method
    /**
     * In this method we find current location using FusedLocationApi.
     * in this we have onUpdateLocation() method in which we check if
     * its not null then We call guest user api.
     */
    private val currentLocation: Unit
        private get() {
            if (CommonClass.isNetworkAvailable(mActivity!!)) {
                locationService = FusedLocationService(mActivity!!, object : FusedLocationReceiver {
                    override fun onUpdateLocation() {
                        val currentLocation = locationService!!.receiveLocation()
                        println("$TAG currentLocation=$currentLocation")
                        if (currentLocation != null) {
                            currentLat = currentLocation.latitude.toString()
                            currentLng = currentLocation.longitude.toString()
                            if (isLocationFound(currentLat, currentLng)) {
                                println("$TAG currentLat=$currentLat currentLng=$currentLng")
                                mSessionManager!!.currentLat = currentLat
                                mSessionManager!!.currentLng = currentLng
                                cityName = CommonClass.getCityName(mActivity!!, currentLocation.latitude, currentLocation.longitude)
                                countryCode = CommonClass.getCountryCode(mActivity!!, currentLocation.latitude, currentLocation.longitude)
                                println("$TAG post id=$postId")

                                // call product details api call method
                                if (mSessionManager!!.isUserLoggedIn) getProductDetailsService(ApiUrl.GET_POST_BY_ID_USER) else getProductDetailsService(ApiUrl.GET_POST_BY_ID_GUEST)
                            }
                        }
                    }
                }
                )
            } else CommonClass.showSnackbarMessage(product_rootview, mActivity!!.resources.getString(R.string.NoInternetAccess))
        }

    /**
     * In this method we used to check whether current lat and
     * long has been received or not.
     * @param lat The current latitude
     * @param lng The current longitude
     * @return boolean flag true or false
     */
    private fun isLocationFound(lat: String?, lng: String?): Boolean {
        return !(lat == null || lat.isEmpty()) && !(lng == null || lng.isEmpty())
    }

    /**
     * <h>GetProductDetailsService</h>
     *
     *
     * In this method we do api call to get product complete information
     * like name, image, description etc.
     *
     */
    private fun getProductDetailsService(url: String) {
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            // token, postId, latitude, longitude, city, countrySname
            val requestDats = JSONObject()
            try {
                requestDats.put("token", mSessionManager!!.authToken)
                requestDats.put("postId", postId)
                requestDats.put("latitude", currentLat)
                requestDats.put("longitude", currentLng)
                requestDats.put("city", cityName)
                requestDats.put("countrySname", countryCode)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            OkHttp3Connection.doOkHttp3Connection(TAG, url, OkHttp3Connection.Request_type.POST, requestDats, object : OkHttp3RequestCallback {
                 override fun onSuccess(result: String?, user_tag: String?) {
                    println("$TAG product details res=$result")
                    if (mDialogBox!!.progressBarDialog != null) mDialogBox!!.progressBarDialog!!.dismiss()
                    mProgress_bar!!.visibility = View.GONE
                    val productDetailsMain: ProductDetailsMain
                    val gson = Gson()
                    productDetailsMain = gson.fromJson(result, ProductDetailsMain::class.java)
                    when (productDetailsMain.code) {
                        "200" -> {
                            productResponse = productDetailsMain.data!![0]!!
                            isToMakeOffer = true
                            receiverMqttId = productResponse!!.memberMqttId
                            tV_makeoffer!!.setBackgroundColor(ContextCompat.getColor(mActivity!!, R.color.status_bar_color))
                            tV_makeoffer!!.setTextColor(ContextCompat.getColor(mActivity!!, R.color.white))
                            productName = productResponse!!.productName
                            productImage = productResponse!!.mainUrl
                            thumbnailImageUrl = productResponse!!.thumbnailImageUrl
                            likesCount = productResponse!!.likes
                            //category=productResponse.getCategoryData().get(0).getCategory();
                            category = productResponse!!.category
                            subCategory = productResponse!!.subCategory
                            postedOn = productResponse!!.postedOn
                            likesCount = productResponse!!.likes
                            clickCount = productResponse!!.clickCount
                            likeStatus = productResponse!!.likeStatus
                            followRequestStatus = productResponse!!.followRequestStatus
                            membername = productResponse!!.membername
                            memberProfilePicUrl = productResponse!!.memberProfilePicUrl
                            description = productResponse!!.description
                            condition = productResponse!!.condition
                            place = productResponse!!.place
                            price = productResponse!!.price
                            negotiable = productResponse!!.negotiable
                            currency = productResponse!!.currency
                            latitude = productResponse!!.latitude
                            longitude = productResponse!!.longitude
                            image1 = productResponse!!.imageUrl1
                            image1thumbnail = productResponse!!.thumbnailUrl1
                            image2 = productResponse!!.imageUrl2
                            image2thumbnail = productResponse!!.thumbnailUrl2
                            image3 = productResponse!!.imageUrl3
                            image3thumbnail = productResponse!!.thumbnailUrl3
                            image4 = productResponse!!.imageUrl4
                            image4thumbnail = productResponse!!.thumbnailUrl4
                            containerWidth = productResponse!!.containerWidth
                            containerHeight = productResponse!!.containerHeight
                            cloudinaryPublicId = productResponse!!.cloudinaryPublicId
                            cloudinaryPublicId1 = productResponse!!.cloudinaryPublicId1
                            cloudinaryPublicId2 = productResponse!!.cloudinaryPublicId2
                            cloudinaryPublicId3 = productResponse!!.cloudinaryPublicId3
                            cloudinaryPublicId4 = productResponse!!.cloudinaryPublicId4
                            sold = productResponse!!.getIsSold()
                            isPromoted = productResponse!!.isPromoted
                            getCityName = productResponse!!.city


                            // To show liked By Users
                            aL_likedByUsers =  productResponse!!.likedByUsers
                            linear_like_product!!.visibility = View.VISIBLE

                            // info list
                            postFilters =  productResponse!!.postFilter


                            // swap datas
                            isSwap =  productResponse!!.isSwap
                            if ( productResponse!!.swapPost != null) {
                                swapPostList.addAll( productResponse!!.swapPost!!)
                                initilizeSwapDatas()
                            }
                            initializeResposeDatas()
                        }
                        "401" -> CommonClass.sessionExpired(mActivity!!)
                        else -> {
                            rL_price!!.visibility = View.VISIBLE
                            rl_progressBar!!.visibility = View.GONE
                        }
                    }
                }

                 override fun onError(error: String?, user_tag: String?) {
                    mProgress_bar!!.visibility = View.GONE
                    if (mDialogBox!!.progressBarDialog != null) mDialogBox!!.progressBarDialog!!.dismiss()
                    CommonClass.showSnackbarMessage(product_rootview, error)
                    rL_price!!.visibility = View.VISIBLE
                    rl_progressBar!!.visibility = View.GONE
                }
            })
        } else {
            if (mDialogBox!!.progressBarDialog != null) mDialogBox!!.progressBarDialog!!.dismiss()
            CommonClass.showSnackbarMessage(product_rootview, mActivity!!.resources.getString(R.string.NoInternetAccess))
            rL_price!!.visibility = View.VISIBLE
            rl_progressBar!!.visibility = View.GONE
        }
    }

    private fun initilizeSwapDatas() {
        println(TAG + " " + "arrayList.size()=" + swapPostList.size)
        //set swapDatas
        if (swapPostList.size > 0 && isSwap == 1) {
            iwantTv!!.visibility = View.VISIBLE
            swapItemAdapter!!.notifyDataSetChanged()
        } else iwantTv!!.visibility = View.GONE
        if (isSwap == 1) {
            rL_swap!!.visibility = View.VISIBLE
            rL_price!!.visibility = View.GONE
            rL_price_content!!.visibility = View.VISIBLE
            rl_progressBar!!.visibility = View.GONE
        } else {
            rL_price!!.visibility = View.VISIBLE
            rL_swap!!.visibility = View.GONE
            rL_price_content!!.visibility = View.GONE
            rl_progressBar!!.visibility = View.GONE
        }
    }

    private fun initializeResposeDatas() {
        aL_multipleImages!!.clear()
        // set multiple image
        if (productImage != null && !productImage!!.isEmpty()) aL_multipleImages!!.add(productImage!!)
        if (image1 != null && !image1!!.isEmpty()) aL_multipleImages!!.add(image1!!)
        if (image2 != null && !image2!!.isEmpty()) aL_multipleImages!!.add(image2!!)
        if (image3 != null && !image3!!.isEmpty()) aL_multipleImages!!.add(image3!!)
        if (image4 != null && !image4!!.isEmpty()) aL_multipleImages!!.add(image4!!)
        if (productImage != null) viewPagerSetup()
        iV_productImage!!.setOnClickListener {
            val intent = Intent(mActivity, ProductImagesActivity::class.java)
            intent.putExtra("imagesArrayList", aL_multipleImages)
            startActivity(intent)
        }

        // product name
        if (productName != null && !productName!!.isEmpty()) {
            productName = productName!!.substring(0, 1).toUpperCase() + productName!!.substring(1).toLowerCase()
            tV_productname!!.text = productName
        }
        try {
            Glide.with(mActivity)
                    .load(productImage)
                    .asBitmap()
                    .placeholder(R.color.add_title)
                    .error(R.color.add_title)
                    .into(iV_productImage)
        } catch (e: OutOfMemoryError) {
            e.printStackTrace()
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        }

        // category
        if (category != null && !category!!.isEmpty()) {
            //category=category.substring(0,1).toUpperCase()+category.substring(1).toLowerCase();
            tV_category!!.text = capitalizeString(category!!)
        }
        if (subCategory != null && !subCategory!!.isEmpty()) {
            val subcat = " | " + capitalizeString(subCategory!!)
            tV_subCategory!!.text = subcat
        }


        // posted on
        if (postedOn != null) tV_postedOn!!.text = CommonClass.getTimeDifference(postedOn!!)

        // view count
        if (clickCount != null) tV_view_count!!.text = clickCount

        // like count
        if (likesCount != null) tV_like_count!!.text = likesCount
        println("$TAG like status=$likeStatus")
        // set like status
        if (likeStatus != null && likeStatus == "1") {
            like_item_icon!!.setImageResource(R.drawable.like_icon_on)
            tV_like_count!!.setTextColor(ContextCompat.getColor(mActivity!!, R.color.pink_color))
            linear_like_product!!.setBackgroundResource(R.drawable.rect_pink_color_with_stroke_shape)
        } else {
            like_item_icon!!.setImageResource(R.drawable.like_icon_off)
            tV_like_count!!.setTextColor(ContextCompat.getColor(mActivity!!, R.color.hide_button_border_color))
            linear_like_product!!.setBackgroundResource(R.drawable.rect_gray_color_with_with_stroke_shape)
        }

        // show total user likes horizontally
        if (isToMakeOffer) inflateUserLikes()

        // to see followed list
        iV_followed_list!!.setOnClickListener {
            if (mSessionManager!!.isUserLoggedIn) {
                val intent = Intent(mActivity, UserLikesActivity::class.java)
                intent.putExtra("postId", postId)
                intent.putExtra("postType", postsType)
                mActivity!!.startActivity(intent)
            } else startActivityForResult(Intent(mActivity, LandingActivity::class.java), VariableConstants.LANDING_REQ_CODE)
        }

        // sold by name
        tV_posted_by!!.text = membername

        // posted by pic
        if (memberProfilePicUrl != null && !memberProfilePicUrl!!.isEmpty()) Picasso.with(mActivity)
                .load(memberProfilePicUrl)
                .transform(CircleTransform())
                .placeholder(R.drawable.default_circle_img)
                .error(R.drawable.default_circle_img)
                .into(iV_soldby)

        // hide the follow option for the user who posted
        if (membername != null && membername == mSessionManager!!.userName) rL_follow!!.visibility = View.GONE else rL_follow!!.visibility = View.VISIBLE

        // hide report icon for own post
        if (membername != null && membername == mSessionManager!!.userName) rL_report_item!!.visibility = View.GONE

        // Check follow status
        if (followRequestStatus != null) {
            if (followRequestStatus == "1") {
                rL_follow!!.setBackgroundResource(R.drawable.rect_purple_color_with_solid_shape)
                tV_follow!!.setTextColor(ContextCompat.getColor(mActivity!!, R.color.white))
                tV_follow!!.text = mActivity!!.resources.getString(R.string.Following)
            } else {
                rL_follow!!.setBackgroundResource(R.drawable.rect_purple_color_with_stroke_shape)
                tV_follow!!.setTextColor(ContextCompat.getColor(mActivity!!, R.color.blue))
                tV_follow!!.text = mActivity!!.resources.getString(R.string.follow)
            }
        }

        // description
        if (description != null) tV_description!!.text = description

        // information
        if (postFilters != null && postFilters!!.size > 0 && postFilters!![0].fieldName != null) {
            linear_info!!.visibility = View.VISIBLE
            productInforRvAdap = ProductInforRvAdap(mActivity!!, postFilters!!)
            rV_info!!.setLayoutManager(GridLayoutManager(mActivity, 2))
            rV_info!!.setAdapter(productInforRvAdap)
        } else {
            linear_info!!.visibility = View.GONE
        }

        // condition
        if (condition != null) tV_condition!!.text = condition

        // mapFragment Sync with google map by calling onMapReadyCallback
        mapFragment!!.getMapAsync(this)
        // api call to get static map location of product
        /* apiCall.staticMapApi(iv_staticMap,latitude,longitude);

        // open map location
        iv_staticMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //latitude,longitude
                Intent intent=new Intent(mActivity,ProductsMapActivity.class);
                intent.putExtra("place",place);
                intent.putExtra("latitude",latitude);
                intent.putExtra("longitude",longitude);
                startActivity(intent);
            }
        });*/

        // location is only city and country
        if (getCityName != null && !getCityName!!.isEmpty()) {
            getCityName = CommonClass.getFirstCaps(getCityName)
            getCountryName = CommonClass.getCountryName(mActivity!!, latitude.toDouble(), longitude.toDouble())
            place = if (getCountryName != null && !getCountryName!!.isEmpty()) "$getCityName, $getCountryName" else getCityName
            tV_location!!.text = place
        }

        // set currency
        if (currency != null && !currency!!.isEmpty()) {
            /*Currency c  = Currency.getInstance(currency);
            currency=c.getSymbol();*/
            val arrayCurrency = mActivity!!.resources.getStringArray(R.array.currency_picker)
            if (arrayCurrency.size > 0) {
                var getCurrencyArr: Array<String>
                for (setCurrency in arrayCurrency) {
                    getCurrencyArr = setCurrency.split(",".toRegex()).toTypedArray()
                    val currency_code = getCurrencyArr[1]
                    val currency_symbol = getCurrencyArr[2]
                    println("$TAG given currency=$currency my currency=$currency_code")
                    if (currency == currency_code) {
                        tV_currency!!.text = currency_symbol
                        tV_currency_content!!.text = currency_symbol
                    }
                }
            }
        }

        // price
        if (price != null) {
            tV_productprice!!.text = price
            tV_price_content!!.text = price
        }

        // set whether price is negotiable
        if (negotiable != null && negotiable != "1") {
            tV_negotiable!!.text = mActivity!!.resources.getString(R.string.not_negotiable)
            tV_negotiable_content!!.text = mActivity!!.resources.getString(R.string.not_negotiable)
        } else {
            tV_negotiable!!.text = mActivity!!.resources.getString(R.string.negotiable)
            tV_negotiable_content!!.text = mActivity!!.resources.getString(R.string.negotiable)
        }

        //...if product already sold then show sold at make offer text
        if (sold == 2 && mSessionManager!!.isUserLoggedIn) {
            if (membername.equals(mSessionManager!!.userName, ignoreCase = true)) {
                isToSellItAgain = true
                tV_makeoffer!!.setText(getString(R.string.sell_it_again))
            } else {
                isToMakeOffer = false
                tV_makeoffer!!.text = mActivity!!.resources.getString(R.string.sold)
            }
        }

        // Make offer
        tV_makeoffer!!.setOnClickListener {
            if (isToMakeOffer) {
                if (membername != null && membername == mSessionManager!!.userName) {
                    if (isToSellItAgain) {
                        sellItAgainDialog()
                    } else openEditProductScreen()
                } else {
                    if (mSessionManager!!.isUserLoggedIn) {
                        val intent = Intent(mActivity, MakeOfferActivity::class.java)
                        intent.putExtra("productPicUrl", productImage)
                        intent.putExtra("productName", productName)
                        intent.putExtra("place", place)
                        intent.putExtra("latitude", latitude)
                        intent.putExtra("longitude", longitude)
                        intent.putExtra("currency", tV_currency!!.text.toString())
                        intent.putExtra("price", tV_productprice!!.text.toString())
                        intent.putExtra("membername", membername)
                        intent.putExtra("postId", postId)
                        intent.putExtra("memberPicUrl", memberProfilePicUrl)
                        intent.putExtra("receiverMqttId", receiverMqttId)
                        intent.putExtra("negotiable", negotiable)
                        intent.putExtra("fromChatScreen", fromChatScreen)
                        startActivity(intent)
                    } else startActivityForResult(Intent(mActivity, LandingActivity::class.java), VariableConstants.LANDING_REQ_CODE)
                }
            }
        }
    }

    private fun viewPagerSetup() {
        val vp_image_adapter = ProductDetailImagePagerAdapter(mActivity!!, aL_multipleImages!!)
        vP_productImage!!.setAdapter(vp_image_adapter)

        dots = arrayOfNulls(vp_image_adapter.count)
        sliderDotspanel!!.removeAllViews()
        for (i in 0 until vp_image_adapter.count) {
            dots!![i] = ImageView(mActivity)
            dots!![i]!!.setImageDrawable(ContextCompat.getDrawable(mActivity!!, R.drawable.non_active_dot))
            val params = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            params.setMargins(8, 0, 8, 0)
            sliderDotspanel!!.addView(dots!![i], params)
        }
        dots!![0]!!.setImageDrawable(ContextCompat.getDrawable(mActivity!!, R.drawable.active_dot))
        vP_productImage!!.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
           override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
            override fun onPageSelected(position: Int) {
                for (i in 0 until vp_image_adapter.count) {
                    dots!![i]!!.setImageDrawable(ContextCompat.getDrawable(getContext()!!, R.drawable.non_active_dot))
                }
                dots!![position]!!.setImageDrawable(ContextCompat.getDrawable(getContext()!!, R.drawable.active_dot))
            }

            override fun onPageScrollStateChanged(state: Int) {}
        })
    }

    /**
     * In this method we used to open Edit product screen & pass the all required values through bundle
     */
    private fun openEditProductScreen() {
        val intent = Intent(mActivity, EditProductActivity::class.java)
        val bundle = Bundle()
        bundle.putString("postId", postId)
        bundle.putString("membername", membername)
        bundle.putString("productImage", productImage)
        bundle.putString("productName", productName)
        bundle.putString("category", category)
        bundle.putString("subCategory", subCategory)
        bundle.putSerializable("information", postFilters)
        bundle.putString("description", description)
        bundle.putString("condition", condition)
        bundle.putString("price", price)
        bundle.putString("negotiable", negotiable)
        bundle.putString("place", place)
        bundle.putString("latitude", latitude)
        bundle.putString("longitude", longitude)
        bundle.putString("currency", currency)
        bundle.putInt("isSwap", isSwap)
        bundle.putInt("isSold", sold)
        bundle.putInt("isPromoted", isPromoted)
        bundle.putSerializable("swapPostList", swapPostList)
        bundle.putBoolean("isFromProductDetail", true)
        val aLProductImageDatases = ArrayList<ProductImageDatas>()

        // first image
        val mainUrl = productImage
        if (mainUrl != null && !mainUrl.isEmpty()) {
            val productImageDatas1 = ProductImageDatas()
            productImageDatas1.mainUrl = productImage
            productImageDatas1.thumbnailUrl = thumbnailImageUrl
            productImageDatas1.public_id = cloudinaryPublicId

            // set width
            if (containerWidth != null && !containerWidth!!.isEmpty()) productImageDatas1.width = containerWidth!!.toInt()

            // set height
            val height = containerHeight
            if (height != null && !height.isEmpty()) productImageDatas1.height = height.toInt()
            productImageDatas1.isImageUrl = true
            aLProductImageDatases.add(productImageDatas1)
        }

        // second image
        if (image1 != null && !image1!!.isEmpty()) {
            val productImageDatas2 = ProductImageDatas()
            productImageDatas2.mainUrl = image1
            productImageDatas2.thumbnailUrl = image1thumbnail
            productImageDatas2.isImageUrl = true
            productImageDatas2.public_id = cloudinaryPublicId1
            aLProductImageDatases.add(productImageDatas2)
        }

        // Third Image
        if (image2 != null && !image2!!.isEmpty()) {
            val productImageDatas3 = ProductImageDatas()
            productImageDatas3.mainUrl = image2
            productImageDatas3.thumbnailUrl = image2thumbnail
            productImageDatas3.public_id = cloudinaryPublicId2
            productImageDatas3.isImageUrl = true
            aLProductImageDatases.add(productImageDatas3)
        }

        // Fourth Image
        if (image3 != null && !image3!!.isEmpty()) {
            val productImageDatas4 = ProductImageDatas()
            productImageDatas4.mainUrl = image3
            productImageDatas4.thumbnailUrl = image3thumbnail
            productImageDatas4.isImageUrl = true
            productImageDatas4.public_id = cloudinaryPublicId3
            aLProductImageDatases.add(productImageDatas4)
        }

        // Fifth Image
        if (image4 != null && !image4!!.isEmpty()) {
            val productImageDatas5 = ProductImageDatas()
            productImageDatas5.mainUrl = image4
            productImageDatas5.thumbnailUrl = image4thumbnail
            productImageDatas5.public_id = cloudinaryPublicId4
            productImageDatas5.isImageUrl = true
            aLProductImageDatases.add(productImageDatas5)
        }
        bundle.putSerializable("imageDatas", aLProductImageDatases)
        intent.putExtras(bundle)
        startActivityForResult(intent, VariableConstants.SELLING_REQ_CODE)
    }

    /**
     * <h>CapitalizeString</h>
     *
     *
     * In this method we used to capitalize the initial character of
     * each word in a given sentence.
     *
     * @param string The given line consisting several words
     * @return The Sentence with initial words in uppercase.
     */
    private fun capitalizeString(string: String): String {
        val chars = string.toLowerCase().toCharArray()
        var found = false
        for (i in chars.indices) {
            if (!found && Character.isLetter(chars[i])) {
                chars[i] = Character.toUpperCase(chars[i])
                found = true
            } else if (Character.isWhitespace(chars[i]) || chars[i] == '.' || chars[i] == '\'') { // You can add other chars here
                found = false
            }
        }
        return String(chars)
    }

    /**
     *
     *
     * Here we set alpha value for action bar title(Product name)
     *
     * @param alpha set value
     */
    fun setTitleAlpha(alpha: Float) {
        var alpha = alpha
        if (alpha < 1) {
            alpha = 1f
        }
        if (alpha == 1f) {
            toolbar_shadow!!.visibility = View.GONE
            iV_back_icon!!.setImageResource(R.drawable.back_arrow_icon)
            iV_option_menu!!.setImageResource(R.drawable.option_menu_icon)
        } else {
            toolbar_shadow!!.visibility = View.GONE
            iV_back_icon!!.setImageResource(R.drawable.white_color_back_button_with_shadow)
            iV_option_menu!!.setImageResource(R.drawable.white_option_menu_icon_with_shadow)
        }
        println("$TAG alpha value=$alpha")
        mAlphaForegroundColorSpan!!.alpha = alpha
        mSpannableString!!.setSpan(mAlphaForegroundColorSpan, 0, mSpannableString!!.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        tV_ProductName!!.text = mSpannableString
    }

    /**
     * <h>InflateUserLikes</h>
     *
     *
     * In this method we used to inflate the user likes list into
     * LinearLayout horizontally.
     *
     */
    private fun inflateUserLikes() {
        var mLikeCount = 0
        if (likesCount != null && !likesCount!!.isEmpty()) mLikeCount = likesCount!!.toInt()
        if (mLikeCount > 0) {
            if (aL_likedByUsers != null && aL_likedByUsers!!.size > 0) {
                linear_followed_images!!.removeAllViews()
                for (likedCount in aL_likedByUsers!!.indices) {
                    val layoutInflater = mActivity!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                    val followedView = layoutInflater.inflate(R.layout.single_row_images, null)
                    val viewPagerItem_image = followedView.findViewById<View>(R.id.iV_image) as ImageView
                    viewPagerItem_image.setBackgroundColor(ContextCompat.getColor(mActivity!!, R.color.white))
                    viewPagerItem_image.layoutParams.width = CommonClass.getDeviceWidth(mActivity!!) / 10
                    viewPagerItem_image.layoutParams.height = CommonClass.getDeviceWidth(mActivity!!) / 10
                    viewPagerItem_image.setImageResource(R.drawable.default_circle_img)
                    val likedUserImg = aL_likedByUsers!![likedCount].profilePicUrl
                    //viewPagerItem_image.setX(mActivity.getResources().getDimension(R.dimen.dim));
                    if (likedUserImg != null && !likedUserImg.isEmpty()) {
                        Picasso.with(mActivity)
                                .load(likedUserImg)
                                .placeholder(R.drawable.default_circle_img)
                                .error(R.drawable.default_circle_img)
                                .transform(CircleTransform())
                                .into(viewPagerItem_image)
                    }

                    // view user profile
                    viewPagerItem_image.setOnClickListener {
                        val currentUserName = aL_likedByUsers!![likedCount].likedByUsers
                        if (currentUserName != null && !currentUserName.isEmpty()) {
                            /* Intent intent=new Intent(mActivity, UserProfileActivity.class);
                                            intent.putExtra("membername",currentUserName);
                                            startActivityForResult(intent,VariableConstants.USER_FOLLOW_REQ_CODE);*/
                            var intent: Intent? = null
                            if (mSessionManager!!.userName == currentUserName) {
                                if (mSessionManager!!.isUserLoggedIn) {
                                    intent = Intent(mActivity, SelfProfileActivity::class.java)
                                    intent.putExtra("membername", currentUserName)
                                } else startActivityForResult(Intent(mActivity, LandingActivity::class.java), VariableConstants.LANDING_REQ_CODE)
                            } else {
                                intent = Intent(mActivity, UserProfileActivity::class.java)
                                intent.putExtra("membername", currentUserName)
                            }
                            if (intent != null) mActivity!!.startActivityForResult(intent, VariableConstants.USER_FOLLOW_REQ_CODE)
                        }
                    }
                    linear_followed_images!!.addView(followedView)
                }
            }
        } else linear_followed_images!!.removeAllViews()
    }

    override fun onClick(v: View) {
        var intent: Intent?
        when (v.id) {
            R.id.rL_back_btn -> {
                intent = Intent()
                intent.putExtra("likesCount", likesCount)
                intent.putExtra("likeStatus", likeStatus)
                intent.putExtra("followRequestStatus", followRequestStatus)
                intent.putExtra("clickCount", clickCount)
                intent.putExtra("aL_likedByUsers", aL_likedByUsers)
                intent.putExtra("isToSellItAgain", isToSellItAgain)
                mActivity!!.setResult(VariableConstants.PRODUCT_DETAILS_REQ_CODE, intent)
                mActivity!!.onBackPressed()
            }
            R.id.linear_like_product -> if (CommonClass.isNetworkAvailable(mActivity!!)) {
                if (mSessionManager!!.isUserLoggedIn) {
                    var mLikeCount = 0
                    if (likesCount != null && !likesCount!!.isEmpty()) mLikeCount = likesCount!!.toInt()

                    // unlike
                    if (likeStatus != null && likeStatus == "1") {
                        // remove my self
                        if (aL_likedByUsers!!.size > 0) {
                            var likeCount = 0
                            while (likeCount < aL_likedByUsers!!.size) {
                                if (aL_likedByUsers!![likeCount].likedByUsers == mSessionManager!!.userName) {
                                    aL_likedByUsers!!.removeAt(likeCount)
                                }
                                likeCount++
                            }
                        }
                        mLikeCount -= 1
                        likesCount = mLikeCount.toString() + ""
                        inflateUserLikes()
                        tV_like_count!!.text = mLikeCount.toString()
                        likeStatus = "0"
                        like_item_icon!!.setImageResource(R.drawable.like_icon_off)
                        tV_like_count!!.setTextColor(ContextCompat.getColor(mActivity!!, R.color.hide_button_border_color))
                        linear_like_product!!.setBackgroundResource(R.drawable.rect_gray_color_with_with_stroke_shape)
                        mEventBusDatasHandler!!.setFavDatasFromProductDetails(productResponse!!, false)
                        apiCall!!.likeProductApi(ApiUrl.UNLIKE_PRODUCT, postId)
                    } else {
                        // add myself
                        val likedByUsersDatas = ExploreLikedByUsersDatas()
                        likedByUsersDatas.likedByUsers = mSessionManager!!.userName!!
                        likedByUsersDatas.profilePicUrl = mSessionManager!!.userImage!!
                        aL_likedByUsers!!.add(0, likedByUsersDatas)
                        mLikeCount += 1
                        likesCount = mLikeCount.toString() + ""
                        inflateUserLikes()
                        tV_like_count!!.text = mLikeCount.toString()
                        likeStatus = "1"
                        like_item_icon!!.setImageResource(R.drawable.like_icon_on)
                        tV_like_count!!.setTextColor(ContextCompat.getColor(mActivity!!, R.color.pink_color))
                        linear_like_product!!.setBackgroundResource(R.drawable.rect_pink_color_with_stroke_shape)
                        mEventBusDatasHandler!!.setFavDatasFromProductDetails(productResponse!!, true)
                        apiCall!!.likeProductApi(ApiUrl.LIKE_PRODUCT, postId)
                    }
                    println("$TAG mLike count=$mLikeCount")
                } else startActivityForResult(Intent(mActivity, LandingActivity::class.java), VariableConstants.LANDING_REQ_CODE)
            } else CommonClass.showSnackbarMessage(product_rootview, mActivity!!.resources.getString(R.string.NoInternetAccess))
            R.id.relative_follow -> if (mSessionManager!!.isUserLoggedIn) {
                if (CommonClass.isNetworkAvailable(mActivity!!)) {
                    val url: String
                    if (followRequestStatus != null && followRequestStatus == "1") {
                        url = ApiUrl.UNFOLLOW + membername
                        unfollowUserAlert(url)
                    } else {
                        url = ApiUrl.FOLLOW + membername
                        apiCall!!.followUserApi(url)
                        rL_follow!!.setBackgroundResource(R.drawable.rect_purple_color_with_solid_shape)
                        tV_follow!!.setTextColor(ContextCompat.getColor(mActivity!!, R.color.white))
                        tV_follow!!.text = mActivity!!.resources.getString(R.string.Following)
                        followRequestStatus = "1"
                        mEventBusDatasHandler!!.setSocialDatasFromProductDetails(productResponse, true)
                    }
                } else {
                    CommonClass.showSnackbarMessage(product_rootview, mActivity!!.resources.getString(R.string.NoInternetAccess))
                }
            } else startActivityForResult(Intent(mActivity, LandingActivity::class.java), VariableConstants.LANDING_REQ_CODE)
            R.id.rL_sold_by -> {
                intent = null
                if (mSessionManager!!.userName == membername) {
                    if (mSessionManager!!.isUserLoggedIn) {
                        intent = Intent(mActivity, SelfProfileActivity::class.java)
                        intent.putExtra("membername", membername)
                    } else startActivityForResult(Intent(mActivity, LandingActivity::class.java), VariableConstants.LANDING_REQ_CODE)
                } else {
                    intent = Intent(mActivity, UserProfileActivity::class.java)
                    intent.putExtra("membername", membername)
                }
                if (intent != null) mActivity!!.startActivity(intent)
            }
            R.id.rL_share -> if (mSessionManager!!.isUserLoggedIn) {
                openShareOptionDialog()
            } else {
                startActivityForResult(Intent(mActivity, LandingActivity::class.java), VariableConstants.LANDING_REQ_CODE)
            }
            R.id.rL_addToReview -> if (mSessionManager!!.isUserLoggedIn) {
                intent = Intent(mActivity, ProductReviewActivity::class.java)
                intent.putExtra("postId", postId)
                intent.putExtra("userName", membername)
                startActivity(intent)
            } else startActivityForResult(Intent(mActivity, LandingActivity::class.java), VariableConstants.LANDING_REQ_CODE)
            R.id.rL_report_item -> if (mSessionManager!!.isUserLoggedIn) {
                intent = Intent(mActivity, ReportProductActivity::class.java)
                intent.putExtra("postId", postId)
                intent.putExtra("product_image", productImage)
                intent.putExtra("product_name", productName)
                intent.putExtra("sold_by_name", membername)
                startActivity(intent)
            } else startActivityForResult(Intent(mActivity, LandingActivity::class.java), VariableConstants.LANDING_REQ_CODE)
            R.id.rL_chat_icon -> if (isToMakeOffer) {
                if (mSessionManager!!.isUserLoggedIn) {
                    initiateChat()
                } else startActivityForResult(Intent(mActivity, LandingActivity::class.java), VariableConstants.LANDING_REQ_CODE)
            }
            R.id.rL_swap -> if (mSessionManager!!.isUserLoggedIn) {
                if (membername != mSessionManager!!.userName) {
                    val intent1 = Intent(mActivity, MyListingActivity::class.java)
                    intent1.putExtra("productPicUrl", productImage)
                    intent1.putExtra("productName", productName)
                    intent1.putExtra("place", place)
                    intent1.putExtra("latitude", latitude)
                    intent1.putExtra("longitude", longitude)
                    intent1.putExtra("currency", tV_currency!!.text.toString())
                    intent1.putExtra("price", tV_productprice!!.text.toString())
                    intent1.putExtra("membername", membername)
                    intent1.putExtra("postId", postId)
                    intent1.putExtra("memberPicUrl", memberProfilePicUrl)
                    intent1.putExtra("receiverMqttId", receiverMqttId)
                    intent1.putExtra("negotiable", negotiable)
                    intent1.putExtra("fromChatScreen", fromChatScreen)
                    startActivity(intent1)
                } else {
                    Snackbar.make(product_rootview!!, "It's your product.", Snackbar.LENGTH_LONG).show()
                }
            } else {
                startActivityForResult(Intent(mActivity, LandingActivity::class.java), VariableConstants.LANDING_REQ_CODE)
            }
            R.id.rL_editProduct -> if (membername != null && membername == mSessionManager!!.userName) mDialogBox!!.openEditProductDialog(postId, product_rootview, productBundleDatas())
        }
    }

    /**
     * In this method we used to open Edit product dialog & pass the all required values through bundle
     */
    private fun productBundleDatas(): Bundle {
        val bundle = Bundle()
        bundle.putString("postId", postId)
        bundle.putString("postedByUserName", membername)
        bundle.putString("productImage", productImage)
        bundle.putString("productName", productName)
        bundle.putString("category", category)
        bundle.putString("subCategory", subCategory)
        bundle.putSerializable("information", postFilters)
        bundle.putString("description", description)
        bundle.putString("condition", condition)
        bundle.putString("price", price)
        bundle.putString("negotiable", negotiable)
        bundle.putString("place", place)
        bundle.putString("latitude", latitude)
        bundle.putString("longitude", longitude)
        bundle.putString("currency", currency)
        bundle.putInt("isSwap", isSwap)
        bundle.putInt("isSold", sold)
        bundle.putInt("isPromoted", isPromoted)
        bundle.putSerializable("swapPostList", swapPostList)
        val aLProductImageDatases = ArrayList<ProductImageDatas>()

        // first image
        val mainUrl = productImage
        if (mainUrl != null && !mainUrl.isEmpty()) {
            val productImageDatas1 = ProductImageDatas()
            productImageDatas1.mainUrl = productImage
            productImageDatas1.thumbnailUrl = thumbnailImageUrl
            productImageDatas1.public_id = cloudinaryPublicId

            // set width
            if (containerWidth != null && !containerWidth!!.isEmpty()) productImageDatas1.width = containerWidth!!.toInt()

            // set height
            val height = containerHeight
            if (height != null && !height.isEmpty()) productImageDatas1.height = height.toInt()
            productImageDatas1.isImageUrl = true
            aLProductImageDatases.add(productImageDatas1)
        }

        // second image
        if (image1 != null && !image1!!.isEmpty()) {
            val productImageDatas2 = ProductImageDatas()
            productImageDatas2.mainUrl = image1
            productImageDatas2.thumbnailUrl = image1thumbnail
            productImageDatas2.isImageUrl = true
            productImageDatas2.public_id = cloudinaryPublicId1
            aLProductImageDatases.add(productImageDatas2)
        }

        // Third Image
        if (image2 != null && !image2!!.isEmpty()) {
            val productImageDatas3 = ProductImageDatas()
            productImageDatas3.mainUrl = image2
            productImageDatas3.thumbnailUrl = image2thumbnail
            productImageDatas3.public_id = cloudinaryPublicId2
            productImageDatas3.isImageUrl = true
            aLProductImageDatases.add(productImageDatas3)
        }

        // Fourth Image
        if (image3 != null && !image3!!.isEmpty()) {
            val productImageDatas4 = ProductImageDatas()
            productImageDatas4.mainUrl = image3
            productImageDatas4.thumbnailUrl = image3thumbnail
            productImageDatas4.isImageUrl = true
            productImageDatas4.public_id = cloudinaryPublicId3
            aLProductImageDatases.add(productImageDatas4)
        }

        // Fifth Image
        if (image4 != null && !image4!!.isEmpty()) {
            val productImageDatas5 = ProductImageDatas()
            productImageDatas5.mainUrl = image4
            productImageDatas5.thumbnailUrl = image4thumbnail
            productImageDatas5.public_id = cloudinaryPublicId4
            productImageDatas5.isImageUrl = true
            aLProductImageDatases.add(productImageDatas5)
        }
        bundle.putSerializable("imageDatas", aLProductImageDatases)
        return bundle
    }

    /**
     * <h>SellItAgainDialog</h>
     *
     *
     * In this method we used to open a dialog to alert the user to sell the item again.
     *
     */
    fun sellItAgainDialog() {
        val errorMessageDialog = Dialog(mActivity!!)
        errorMessageDialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        errorMessageDialog.setContentView(R.layout.dialog_sell_it_again)
        errorMessageDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        errorMessageDialog.window!!.setLayout((CommonClass.getDeviceWidth(mActivity!!) * 0.8).toInt(), RelativeLayout.LayoutParams.WRAP_CONTENT)

        // dismiss
        val tV_no = errorMessageDialog.findViewById<View>(R.id.tV_no) as TextView
        tV_no.setOnClickListener { errorMessageDialog.dismiss() }

        // yes
        val tV_yes = errorMessageDialog.findViewById<View>(R.id.tV_yes) as TextView
        tV_yes.setOnClickListener {
            if (CommonClass.isNetworkAvailable(mActivity!!)) {
                isToSellItAgain = false
                tV_makeoffer!!.text = mActivity!!.resources.getString(R.string.mark_as_sold)
                apiCall!!.markSellingApi(product_rootview, postId)
                mEventBusDatasHandler!!.addSellingDatasFromProductDetails(productResponse!!)
                mEventBusDatasHandler!!.addHomePageDatasFromProductDetails(productResponse!!)
                mEventBusDatasHandler!!.setSocialDatasFromProductDetails(productResponse!!)
                errorMessageDialog.dismiss()
            } else {
                CommonClass.showSnackbarMessage((mActivity as HomePageActivity?)!!.rL_rootElement, mActivity!!.resources.getString(R.string.NoInternetAccess))
            }
        }
        errorMessageDialog.show()
    }

    /**
     * <h>openShareOptionDialog</h>
     *
     *
     * In this method we used to open a dialog to show option like sharing or copy item url.
     *
     */
    private fun openShareOptionDialog() {
        val shareDialog = Dialog(mActivity!!)
        shareDialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        shareDialog.setContentView(R.layout.dialog_share_option)
        shareDialog.window!!.setGravity(Gravity.BOTTOM)
        shareDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        shareDialog.window!!.attributes.windowAnimations = R.style.DialogAnimation
        shareDialog.window!!.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT)

        // Share on facebook
        val rL_share_on_fb = shareDialog.findViewById<View>(R.id.rL_share_on_fb) as RelativeLayout
        rL_share_on_fb.setOnClickListener {
            shareDialog.dismiss()
            val deepLink = mActivity!!.resources.getString(R.string.share_item_base_url) + postId
            val post_url = ""
            val shortLinkTask = FirebaseDynamicLinks.getInstance().createDynamicLink()
                    .setLink(Uri.parse(deepLink))
                    .setDynamicLinkDomain(mActivity!!.resources.getString(R.string.deep_link))
                    .setSocialMetaTagParameters(
                            SocialMetaTagParameters.Builder()
                                    .setTitle(productName)
                                    .setDescription(description)
                                    .setImageUrl(Uri.parse(productImage))
                                    .build())
                    .setAndroidParameters(AndroidParameters.Builder().build())
                    .buildShortDynamicLink()
                    .addOnCompleteListener(mActivity!!) { task ->
                        if (task.isSuccessful) {
                            // Short link created
                            val shortLink = task.result!!.shortLink
                            val shareDialog: ShareDialog
                            FacebookSdk.sdkInitialize(mActivity)
                            shareDialog = ShareDialog(mActivity)
                            val linkContent = ShareLinkContent.Builder()
                                    .setContentUrl(shortLink)
                                    .setImageUrl(Uri.parse(productImage))
                                    .setContentTitle(productName)
                                    .setContentDescription(description).build()
                            shareDialog.show(linkContent)
                        } else {
                            Toast.makeText(mActivity, "Sharing failed, Try again", Toast.LENGTH_SHORT).show()
                        }
                    }
        }

        // copy product url
        val rL_copy_url = shareDialog.findViewById<View>(R.id.rL_copy_url) as RelativeLayout
        rL_copy_url.setOnClickListener {
            shareDialog.dismiss()
            val deepLink = mActivity!!.resources.getString(R.string.share_item_base_url) + postId
            FirebaseDynamicLinks.getInstance().createDynamicLink()
                    .setLink(Uri.parse(deepLink))
                    .setDynamicLinkDomain("jm2gm.app.goo.gl")
                    .setSocialMetaTagParameters(
                            SocialMetaTagParameters.Builder()
                                    .setTitle(productName)
                                    .setDescription(description)
                                    .setImageUrl(Uri.parse(productImage))
                                    .build())
                    .setAndroidParameters(AndroidParameters.Builder().build())
                    .buildShortDynamicLink()
                    .addOnCompleteListener(mActivity!!) { task ->
                        if (task.isSuccessful) {
                            // Short link created
                            val shortLink = task.result!!.shortLink
                            myClip = ClipData.newPlainText("text", shortLink.toString())
                            myClipboard!!.setPrimaryClip(myClip!!)
                            CommonClass.showShortSuccessMsg(product_rootview, mActivity!!.resources.getString(R.string.url_copied))
                        }
                    }
        }

        // cancel
        val cancel_button = shareDialog.findViewById<View>(R.id.cancel_button) as TextView
        cancel_button.setOnClickListener { shareDialog.dismiss() }
        shareDialog.show()
    }

    /**
     * <h>unfollowUserAlert</h>
     *
     *
     * In this method we used to open a simple dialog pop-up to show
     * alert to unfollow
     *
     */
    fun unfollowUserAlert(url: String?) {
        val unfollowUserDialog = Dialog(mActivity!!)
        unfollowUserDialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        unfollowUserDialog.setContentView(R.layout.dialog_unfollow_user)
        unfollowUserDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        unfollowUserDialog.window!!.setLayout((CommonClass.getDeviceWidth(mActivity!!) * 0.9).toInt(), RelativeLayout.LayoutParams.WRAP_CONTENT)

        // set user pic
        val imageViewPic = unfollowUserDialog.findViewById<View>(R.id.iV_userPic) as ImageView
        imageViewPic.layoutParams.width = CommonClass.getDeviceWidth(mActivity!!) / 5
        imageViewPic.layoutParams.height = CommonClass.getDeviceWidth(mActivity!!) / 5

        // posted by pic
        if (memberProfilePicUrl != null && !memberProfilePicUrl!!.isEmpty()) Picasso.with(mActivity)
                .load(memberProfilePicUrl)
                .transform(CircleTransform())
                .placeholder(R.drawable.default_profile_image)
                .error(R.drawable.default_profile_image)
                .into(imageViewPic)

        // set user name
        val tV_userName = unfollowUserDialog.findViewById<View>(R.id.tV_userName) as TextView
        if (membername != null && !membername!!.isEmpty()) {
            val setUserName = mActivity!!.resources.getString(R.string.at_the_rate) + membername + mActivity!!.resources.getString(R.string.question_mark)
            tV_userName.text = setUserName
        }

        // set cancel button
        val tV_cancel = unfollowUserDialog.findViewById<View>(R.id.tV_cancel) as TextView
        tV_cancel.setOnClickListener { unfollowUserDialog.dismiss() }

        // set done button
        val tV_unfollow = unfollowUserDialog.findViewById<View>(R.id.tV_unfollow) as TextView
        tV_unfollow.setOnClickListener {
            apiCall!!.followUserApi(url!!)
            rL_follow!!.setBackgroundResource(R.drawable.rect_purple_color_with_stroke_shape)
            tV_follow!!.setTextColor(ContextCompat.getColor(mActivity!!, R.color.blue))
            tV_follow!!.text = mActivity!!.resources.getString(R.string.follow)
            followRequestStatus = "0"
            mEventBusDatasHandler!!.setSocialDatasFromProductDetails(productResponse, false)
            unfollowUserDialog.dismiss()
        }
        unfollowUserDialog.show()
    }

   override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        callbackManager!!.onActivityResult(requestCode, resultCode, data)
        if (data != null) {
            println("$TAG onactivity result res code=$resultCode req code=$requestCode data=$data")
            when (requestCode) {
                VariableConstants.USER_FOLLOW_REQ_CODE -> {
                    followRequestStatus = data.getStringExtra("followStatus")
                    if (followRequestStatus == "1") {
                        rL_follow!!.setBackgroundResource(R.drawable.rect_purple_color_with_solid_shape)
                        tV_follow!!.setTextColor(ContextCompat.getColor(mActivity!!, R.color.white))
                        tV_follow!!.text = mActivity!!.resources.getString(R.string.Following)
                    } else {
                        rL_follow!!.setBackgroundResource(R.drawable.rect_purple_color_with_stroke_shape)
                        tV_follow!!.setTextColor(ContextCompat.getColor(mActivity!!, R.color.blue))
                        tV_follow!!.text = mActivity!!.resources.getString(R.string.follow)
                    }
                }
                VariableConstants.REQUEST_CHECK_SETTINGS -> when (resultCode) {
                    Activity.RESULT_CANCELED ->                             //mDialogBox.showProgressDialog(mActivity.getResources().getString(R.string.Loading));
                        // call product details api call method
                        if (mSessionManager!!.isUserLoggedIn) getProductDetailsService(ApiUrl.GET_POST_BY_ID_USER) else getProductDetailsService(ApiUrl.GET_POST_BY_ID_GUEST)
                }
                VariableConstants.SELLING_REQ_CODE -> {
                    isToSellItAgain = data.getBooleanExtra("isToSellItAgain", false)
                    val isPostDeleted = data.getBooleanExtra("isPostDeleted", false)
                    println("$TAG isToSwitchItem=$isToSellItAgain")
                    if (isToSellItAgain) tV_makeoffer!!.text = mActivity!!.resources.getString(R.string.sell_it_again)
                    if (isPostDeleted) mActivity!!.finish()
                }
                VariableConstants.LANDING_REQ_CODE -> {
                    val isToRefreshHomePage = data.getBooleanExtra("isToRefreshHomePage", true)
                    println("$TAG isToRefreshHomePage=$isToRefreshHomePage")
                    val isFromSignup = data.getBooleanExtra("isFromSignup", false)

                    // open start browsering screen
                    if (isFromSignup) DialogBox(mActivity!!).startBrowsingDialog()
                    if (runTimePermission!!.checkPermissions(permissionsArray)) {
                        currentLocation
                    } else runTimePermission!!.requestPermission()
                }
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, @NonNull permissions: Array<String?>, @NonNull grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            VariableConstants.PERMISSION_REQUEST_CODE -> {
                println("grant result=" + grantResults.size)
                if (grantResults.size > 0) {
                    var count = 0
                    while (count < grantResults.size) {
                        if (grantResults[count] != PackageManager.PERMISSION_GRANTED) runTimePermission!!.allowPermissionAlert(permissions!![count]!!)
                        count++
                    }
                    println("isAllPermissionGranted=" + runTimePermission!!.checkPermissions(permissionsArray))
                    if (runTimePermission!!.checkPermissions(permissionsArray)) {
                        //mDialogBox.showProgressDialog(mActivity.getResources().getString(R.string.Loading));
                        currentLocation
                    }
                }
            }
        }
    }

    /*
     *initiate chat with out offer. */
    private fun initiateChat() {
        if (receiverMqttId == null || receiverMqttId!!.isEmpty()) {
            Toast.makeText(mActivity, R.string.not_register_task, Toast.LENGTH_SHORT).show()
            return
        }
        var doucumentId = AppController.instance?.findDocumentIdOfReceiver(receiverMqttId, postId)
        val isChatNotExist: Boolean
        if (doucumentId!!.isEmpty()) {
//            doucumentId = null
            isChatNotExist = true
        } else {
            isChatNotExist = false
            AppController.instance?.dbController!!.updateChatDetails(doucumentId, membername, memberProfilePicUrl)
        }
        val intent: Intent
        intent = Intent(mActivity, ChatMessageScreen::class.java)
        intent.putExtra("isChatNotExist", isChatNotExist)
        intent.putExtra("productId", postId)
        intent.putExtra("receiverUid", receiverMqttId)
        intent.putExtra("receiverName", membername)
        intent.putExtra("documentId", doucumentId)
        intent.putExtra("receiverIdentifier", AppController.instance?.userIdentifier)
        intent.putExtra("receiverImage", memberProfilePicUrl)
        intent.putExtra("colorCode", AppController.instance?.getColorCode(1 % 19))
        intent.putExtra("isFromOfferPage", false)
        if (fromChatScreen == "0") {
            startActivity(intent)
        } else {
            mActivity!!.finish()
        }
    }

    private var mMap: GoogleMap? = null
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap!!.uiSettings.setAllGesturesEnabled(false)
        if (isLocationFound(latitude, longitude)) {
            val latLng = LatLng(latitude.toDouble(), longitude.toDouble())
            mMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 14f))
            mMap!!.setOnMapClickListener {
                val intent = Intent(mActivity, ProductsMapActivity::class.java)
                intent.putExtra("place", place)
                intent.putExtra("latitude", latitude)
                intent.putExtra("longitude", longitude)
                startActivity(intent)
            }
        }
    }

    companion object {
        private val TAG = ProductDetailsActivity::class.java.simpleName
    }
}