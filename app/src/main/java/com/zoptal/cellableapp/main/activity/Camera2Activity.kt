package com.zoptal.cellableapp.main.activity

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.graphics.*
import android.hardware.camera2.*
import android.media.Image
import android.media.ImageReader
import android.os.*

import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.util.Size
import android.util.SparseIntArray
import android.view.Surface
import android.view.TextureView
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.adapter.ImagesHorizontalRvAdap
import com.zoptal.cellableapp.fcm_push_notification.Config
import com.zoptal.cellableapp.fcm_push_notification.NotificationMessageDialog
import com.zoptal.cellableapp.fcm_push_notification.NotificationUtils.Companion.clearNotifications
import com.zoptal.cellableapp.main.activity.Camera2Activity
import com.zoptal.cellableapp.utility.*
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.*
import java.util.concurrent.Semaphore
import java.util.concurrent.TimeUnit

/**
 * <h>CameraActivity</h>
 *
 *
 * In this class we used to do the custom camera funtionality on AutoFitTextureView. we
 * have a custom button to capture image. Once we capture image we save the path of image
 * into list from File. In this we have maximum 5 picture to take either by camera or gallery.
 *
 * @since 14-Sep-17
 * @author 3Embed
 * @version 1.0
 */
@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
class Camera2Activity : AppCompatActivity(), View.OnClickListener {
    private var mActivity: Activity? = null
    private var permissionsArray: Array<String> = arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
    private var runTimePermission: RunTimePermission? = null
    private var imagesHorizontalRvAdap: ImagesHorizontalRvAdap? = null
    private var arrayListImgPath: ArrayList<CapturedImage>? = null
    var isToCaptureImage = false
    var isFromUpdatePost = false
    var isToOpenCamera = false
    private var mBackgroundThread: HandlerThread? = null
    private var isToReleaseCamera = false

    /**
     * Xml Variables
     */
    var rL_rootview: RelativeLayout? = null
    var tV_upload: TextView? = null

    /**
     * Fop the oneplus and nexux6 issue
     */
    private var readyToCapture = true

    companion object {
        /**
         * Conversion from screen rotation to JPEG orientation.
         */
        private val ORIENTATIONS = SparseIntArray()

        /**
         * Tag for the [Log].
         */
        private val TAG = Camera2Activity::class.java.simpleName

        /**
         * Camera state: Showing camera preview.
         */
        private const val STATE_PREVIEW = 0

        /**
         * Camera state: Waiting for the focus to be locked.
         */
        private const val STATE_WAITING_LOCK = 1

        /**
         * Camera state: Waiting for the exposure to be precapture state.
         */
        private const val STATE_WAITING_PRECAPTURE = 2

        /**
         * Camera state: Waiting for the exposure state to be something other than precapture.
         */
        private const val STATE_WAITING_NON_PRECAPTURE = 3

        /**
         * Camera state: Picture was taken.
         */
        private const val STATE_PICTURE_TAKEN = 4

        /**
         * Max preview width that is guaranteed by Camera2 API
         */
        private const val MAX_PREVIEW_WIDTH = 1920

        /**
         * Max preview height that is guaranteed by Camera2 API
         */
        private const val MAX_PREVIEW_HEIGHT = 1080
        const val CAMERA_FRONT = "1"
        const val CAMERA_BACK = "0"

        init {
            ORIENTATIONS.append(Surface.ROTATION_0, 90)
            ORIENTATIONS.append(Surface.ROTATION_90, 0)
            ORIENTATIONS.append(Surface.ROTATION_180, 270)
            ORIENTATIONS.append(Surface.ROTATION_270, 180)
        }
    }

    private var mNotificationMessageDialog: NotificationMessageDialog? = null
    private val cameraId = CAMERA_BACK
    private val isFlashSupported = false
    private var isTorchOn = false
    private var iV_flashIcon: ImageView? = null
     override fun onCreate(@Nullable savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_camera2)
        initVariables()
    }

    private fun initVariables() {
        mActivity = this@Camera2Activity
        isToCaptureImage = true
        isToReleaseCamera = true
        mNotificationMessageDialog = NotificationMessageDialog(mActivity)
        isToOpenCamera = true

        // receive datas from last activity
        val intent: Intent = getIntent()
        isFromUpdatePost = intent.getBooleanExtra("isUpdatePost", false)

        // set status bar color
        CommonClass.statusBarColor(mActivity!!)
        arrayListImgPath = ArrayList()
        mTextureView = findViewById(R.id.texture) as AutoFitTextureView?
        rL_rootview = findViewById(R.id.rL_rootview) as RelativeLayout?

        runTimePermission = RunTimePermission(mActivity!!, permissionsArray, true)
        iV_flashIcon = findViewById(R.id.iV_flashIcon) as ImageView?
        iV_flashIcon!!.setOnClickListener(this)

        // Adapter
        tV_upload = findViewById(R.id.tV_upload) as TextView?
        imagesHorizontalRvAdap = ImagesHorizontalRvAdap(mActivity!!, arrayListImgPath!!, tV_upload, isToCaptureImage)
        val rV_cameraImages: RecyclerView = findViewById(R.id.rV_cameraImages) as RecyclerView
        val linearLayoutManager = LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false)
        rV_cameraImages.setLayoutManager(linearLayoutManager)
        rV_cameraImages.setAdapter(imagesHorizontalRvAdap)

        // Done
        val rL_done = findViewById(R.id.rL_done) as RelativeLayout
        rL_done.setOnClickListener(this)

        // gallery
        val iV_gallery = findViewById(R.id.iV_gallery) as ImageView
        iV_gallery.setOnClickListener(this)

        // Capture image
        val iV_captureImage = findViewById(R.id.iV_captureImage) as ImageView
        iV_captureImage.setOnClickListener {
            // check captured images are less than 5 or not
            if (arrayListImgPath!!.size < 5) {
                println("$TAG isToCaptureImage=$isToCaptureImage")
                //take the picture
                if (isToCaptureImage) {
                    lockFocus()
                    //isToCaptureImage=false;
                }
            } else CommonClass.showSnackbarMessage(rL_rootview, getResources().getString(R.string.you_can_select_only_upto))
        }

        // Cancel
        val rL_cancel_btn = findViewById(R.id.rL_cancel_btn) as RelativeLayout
        rL_cancel_btn.setOnClickListener(this)
    }

    /**
     * [TextureView.SurfaceTextureListener] handles several lifecycle events on a
     * [TextureView].
     */
    private val mSurfaceTextureListener: TextureView.SurfaceTextureListener = object : TextureView.SurfaceTextureListener {
        override fun onSurfaceTextureAvailable(texture: SurfaceTexture, width: Int, height: Int) {
            println("$TAG openCamera from onSurfaceTextureAvailable")
            if (isToOpenCamera) openCamera(width, height)
        }

        override fun onSurfaceTextureSizeChanged(texture: SurfaceTexture, width: Int, height: Int) {
            configureTransform(width, height)
        }

        override fun onSurfaceTextureDestroyed(texture: SurfaceTexture): Boolean {
            return true
        }

        override fun onSurfaceTextureUpdated(texture: SurfaceTexture) {}
    }

    /**
     * ID of the current [CameraDevice].
     */
    private var mCameraId: String? = null

    /**
     * An [AutoFitTextureView] for camera preview.
     */
    private var mTextureView: AutoFitTextureView? = null

    /**
     * A [CameraCaptureSession] for camera preview.
     */
    private var mCaptureSession: CameraCaptureSession? = null

    /**
     * A reference to the opened [CameraDevice].
     */
    private var mCameraDevice: CameraDevice? = null

    /**
     * The [android.util.Size] of camera preview.
     */
    private var mPreviewSize: Size? = null

    /**
     * [CameraDevice.StateCallback] is called when [CameraDevice] changes its state.
     */
    private val mStateCallback: CameraDevice.StateCallback = object : CameraDevice.StateCallback() {
        override fun onOpened(@NonNull cameraDevice: CameraDevice) {
            // This method is called when the camera is opened.  We start camera preview here.
            mCameraOpenCloseLock!!.release()
            mCameraDevice = cameraDevice
            createCameraPreviewSession()
        }

        override fun onDisconnected(@NonNull cameraDevice: CameraDevice) {
            mCameraOpenCloseLock!!.release()
            cameraDevice.close()
            mCameraDevice = null
        }

        override fun onError(@NonNull cameraDevice: CameraDevice, error: Int) {
            mCameraOpenCloseLock!!.release()
            cameraDevice.close()
            mCameraDevice = null
        }
    }

    /**
     * A [Handler] for running tasks in the background.
     */
    private var mBackgroundHandler: Handler? = null

    /**
     * An [ImageReader] that handles still image capture.
     */
    private var mImageReader: ImageReader? = null

    /**
     * This is the output file for our picture.
     */
    private var mFile: File? = null

    /**
     * This a callback object for the [ImageReader]. "onImageAvailable" will be called when a
     * still image is ready to be saved.
     */
    private val mOnImageAvailableListener = ImageReader.OnImageAvailableListener { reader ->
        //mFile = new File(getActivity().getExternalFilesDir(null), String.valueOf(System.nanoTime())+"pic.jpg");
        println("$TAG onImageAvailable...")
        val folderPath: String
        folderPath = if (Environment.getExternalStorageState() == Environment.MEDIA_MOUNTED) {
            Environment.getExternalStorageDirectory().toString() + "/" + getResources().getString(R.string.app_name)
        } else {
            mActivity!!.filesDir.toString() + "/" + getResources().getString(R.string.app_name)
        }
        val folder = File(folderPath)
        if (!folder.exists() && !folder.isDirectory) {
            folder.mkdirs()
        }
        if (mFile == null) {
            mFile = File(folderPath, System.currentTimeMillis().toString() + ".jpg")
        }
        try {
            if (!mFile!!.exists()) {
                mFile!!.createNewFile()
            }
        } catch (e: Exception) {
            Toast.makeText(mActivity, "Image Not saved", Toast.LENGTH_SHORT).show()
            return@OnImageAvailableListener
        }
        mBackgroundHandler!!.post(ImageSaver(reader.acquireNextImage(), mFile!!))
        runOnUiThread(Runnable {
            if (mFile != null) {
                val image = CapturedImage()
                image.imagePath = mFile!!.absolutePath
                image.rotateAngle = 0
                arrayListImgPath!!.add(image)
                imagesHorizontalRvAdap!!.notifyItemInserted(arrayListImgPath!!.size)
                //imagesHorizontalRvAdap.notifyDataSetChanged();
                isToCaptureImage = arrayListImgPath!!.size < 5

                // enable upload button
                if (arrayListImgPath!!.size > 0) tV_upload!!.setTextColor(ContextCompat.getColor(mActivity!!, R.color.colorAccent))
                mFile = null
                readyToCapture = true
            }
        })
    }

    /**
     * [CaptureRequest.Builder] for the camera preview
     */
    private var mPreviewRequestBuilder: CaptureRequest.Builder? = null

    /**
     * [CaptureRequest] generated by [.mPreviewRequestBuilder]
     */
    private var mPreviewRequest: CaptureRequest? = null

    /**
     * The current state of camera state for taking pictures.
     *
     * @see .mCaptureCallback
     */
    private var mState = STATE_PREVIEW

    /**
     * A [Semaphore] to prevent the app from exiting before closing the camera.
     */
    private val mCameraOpenCloseLock: Semaphore? = Semaphore(1)

    /**
     * Whether the current camera device supports Flash or not.
     */
    private var mFlashSupported = false

    /**
     * Orientation of the camera sensor
     */
    private var mSensorOrientation = 0

    /**
     * A [CameraCaptureSession.CaptureCallback] that handles events related to JPEG capture.
     */
    private val mCaptureCallback: CameraCaptureSession.CaptureCallback = object : CameraCaptureSession.CaptureCallback() {
        private fun process(result: CaptureResult) {
            when (mState) {
                STATE_PREVIEW -> {
                }
                STATE_WAITING_LOCK -> {
                    val afState = result.get(CaptureResult.CONTROL_AF_STATE)
                    Log.d("log11", "17")
                    if (afState == null) {
                        Log.d("log11", "18")
                        if (readyToCapture) {
                            readyToCapture = false
                            captureStillPicture()
                        }
                    } else if (CaptureResult.CONTROL_AF_STATE_FOCUSED_LOCKED == afState ||
                            CaptureResult.CONTROL_AF_STATE_NOT_FOCUSED_LOCKED == afState) {
                        Log.d("log11", "19")
                        // CONTROL_AE_STATE can be null on some devices
                        val aeState = result.get(CaptureResult.CONTROL_AE_STATE)
                        if (aeState == null ||
                                aeState == CaptureResult.CONTROL_AE_STATE_CONVERGED) {
                            if (readyToCapture) {
                                readyToCapture = false
                                Log.d("log11", "20")
                                mState = STATE_PICTURE_TAKEN
                                captureStillPicture()
                            }
                        } else {
                            Log.d("log11", "21")
                            runPrecaptureSequence()
                        }
                    }
                }
                STATE_WAITING_PRECAPTURE -> {

                    // CONTROL_AE_STATE can be null on some devices
                    val aeState = result.get(CaptureResult.CONTROL_AE_STATE)
                    if (aeState == null || aeState == CaptureResult.CONTROL_AE_STATE_PRECAPTURE || aeState == CaptureRequest.CONTROL_AE_STATE_FLASH_REQUIRED) {
                        mState = STATE_WAITING_NON_PRECAPTURE
                    }
                }
                STATE_WAITING_NON_PRECAPTURE -> {

                    // CONTROL_AE_STATE can be null on some devices
                    val aeState = result.get(CaptureResult.CONTROL_AE_STATE)
                    if (aeState == null || aeState != CaptureResult.CONTROL_AE_STATE_PRECAPTURE) {
                        mState = STATE_PICTURE_TAKEN
                        if (readyToCapture) {
                            readyToCapture = false
                            captureStillPicture()
                        }
                    }
                }
            }
        }

        override fun onCaptureProgressed(@NonNull session: CameraCaptureSession,
                                         @NonNull request: CaptureRequest,
                                         @NonNull partialResult: CaptureResult) {
            process(partialResult)
        }

        override fun onCaptureCompleted(@NonNull session: CameraCaptureSession,
                                        @NonNull request: CaptureRequest,
                                        @NonNull result: TotalCaptureResult) {
            process(result)
        }
    }

    /**
     * Given `choices` of `Size`s supported by a camera, choose the smallest one that
     * is at least as large as the respective texture view size, and that is at most as large as the
     * respective max size, and whose aspect ratio matches with the specified value. If such size
     * doesn't exist, choose the largest one that is at most as large as the respective max size,
     * and whose aspect ratio matches with the specified value.
     *
     * @param choices           The list of sizes that the camera supports for the intended output
     * class
     * @param textureViewWidth  The width of the texture view relative to sensor coordinate
     * @param textureViewHeight The height of the texture view relative to sensor coordinate
     * @param maxWidth          The maximum width that can be chosen
     * @param maxHeight         The maximum height that can be chosen
     * @param aspectRatio       The aspect ratio
     * @return The optimal `Size`, or an arbitrary one if none were big enough
     */
    private fun chooseOptimalSize(choices: Array<Size>, textureViewWidth: Int,
                                  textureViewHeight: Int, maxWidth: Int, maxHeight: Int, aspectRatio: Size): Size {

        // Collect the supported resolutions that are at least as big as the preview Surface
        val bigEnough: MutableList<Size> = ArrayList()
        // Collect the supported resolutions that are smaller than the preview Surface
        val notBigEnough: MutableList<Size> = ArrayList()
        Log.d("log73", "$maxHeight $maxWidth")
        val w = aspectRatio.width
        val h = aspectRatio.height
        for (option in choices) {
            Log.d("log92", option.height.toString() + " " + option.width)
            if (option.width <= maxWidth && option.height <= maxHeight && option.height == option.width * h / w) {
                if (option.width >= textureViewWidth &&
                        option.height >= textureViewHeight) {
                    bigEnough.add(option)
                } else {
                    notBigEnough.add(option)
                }
            }
        }

        // Pick the smallest of those big enough. If there is no one big enough, pick the
        // largest of those not big enough.
        return if (bigEnough.size > 0) {
            Collections.min(bigEnough, CompareSizesByArea())
        } else if (notBigEnough.size > 0) {
            Collections.max(notBigEnough, CompareSizesByArea())
        } else {
            Log.e(TAG, "Couldn't find any suitable preview size")
            choices[0]
        }
    }

    override fun onResume() {
        super.onResume()
        startBackgroundThread()

        // When the screen is turned off and turned back on, the SurfaceTexture is already
        // available, and "onSurfaceTextureAvailable" will not be called. In that case, we can open
        // a camera and start preview from here (otherwise, we wait until the surface is ready in
        // the SurfaceTextureListener).
        if (mTextureView!!.isAvailable && isToOpenCamera) {
            openCamera(mTextureView!!.width, mTextureView!!.height)
        } else {
            mTextureView!!.surfaceTextureListener = mSurfaceTextureListener
        }

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.REGISTRATION_COMPLETE))

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.PUSH_NOTIFICATION))

        // clear the notification area when the app is opened
        clearNotifications(getApplicationContext())
    }

   override fun onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!)
        closeCamera()
        stopBackgroundThread()
        println("$TAG on pause called..")
        super.onPause()
    }

    private fun closeCamera() {
        try {
            mCameraOpenCloseLock!!.acquire()
            if (null != mCaptureSession) {
                mCaptureSession!!.close()
                mCaptureSession = null
            }
            if (null != mCameraDevice) {
                mCameraDevice!!.close()
                mCameraDevice = null
            }
            if (null != mImageReader) {
                mImageReader!!.close()
                mImageReader = null
            }
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            mCameraOpenCloseLock!!.release()
        }
    }

    /**
     * Stops the background thread and its [Handler].
     */
    private fun stopBackgroundThread() {
        if (mBackgroundThread != null) {
            mBackgroundThread!!.quitSafely()
        }
        try {
            mBackgroundThread!!.join()
            mBackgroundThread = null
            mBackgroundHandler = null
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * Sets up member variables related to camera.
     *
     * @param width  The width of available size for camera preview
     * @param height The height of available size for camera preview
     */
    private fun setUpCameraOutputs(width: Int, height: Int) {
        Log.d("log123", "123$width $height")
        val manager = mActivity!!.getSystemService(Context.CAMERA_SERVICE) as CameraManager
        try {
            for (cameraId in manager.cameraIdList) {
                val characteristics = manager.getCameraCharacteristics(cameraId)

                // We don't use a front facing camera in this sample.
                val facing = characteristics.get(CameraCharacteristics.LENS_FACING)
                if (facing != null && facing == CameraCharacteristics.LENS_FACING_FRONT) {
                    continue
                }
                val map = characteristics.get(
                        CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP)
                        ?: continue

                // For still image captures, we use the largest available size.
                val largest = Collections.max(
                        Arrays.asList(*map.getOutputSizes(ImageFormat.JPEG)), CompareSizesByArea())
                mImageReader = ImageReader.newInstance(largest.width, largest.height,
                        ImageFormat.JPEG,  /*maxImages*/2)
                mImageReader!!.setOnImageAvailableListener(mOnImageAvailableListener, mBackgroundHandler)

                // Find out if we need to swap dimension to get the preview size relative to sensor
                // coordinate.
                val displayRotation = mActivity!!.windowManager.defaultDisplay.rotation
                mSensorOrientation = characteristics.get(CameraCharacteristics.SENSOR_ORIENTATION)!!
                var swappedDimensions = false
                when (displayRotation) {
                    Surface.ROTATION_0, Surface.ROTATION_180 -> if (mSensorOrientation == 90 || mSensorOrientation == 270) {
                        swappedDimensions = true
                    }
                    Surface.ROTATION_90, Surface.ROTATION_270 -> if (mSensorOrientation == 0 || mSensorOrientation == 180) {
                        swappedDimensions = true
                    }
                    else -> Log.e(TAG, "Display rotation is invalid: $displayRotation")
                }
                val displaySize = Point()
                mActivity!!.windowManager.defaultDisplay.getSize(displaySize)
                var rotatedPreviewWidth = width
                var rotatedPreviewHeight = height
                var maxPreviewWidth = displaySize.x
                var maxPreviewHeight = displaySize.y
                Log.d("log13", "$maxPreviewWidth $maxPreviewHeight")
                if (swappedDimensions) {
                    rotatedPreviewWidth = height
                    rotatedPreviewHeight = width
                    maxPreviewWidth = displaySize.y
                    maxPreviewHeight = displaySize.x
                }

                // Danger, W.R.! Attempting to use too large a preview size could  exceed the camera
                // bus' bandwidth limitation, resulting in gorgeous previews but the storage of
                // garbage capture data.
                mPreviewSize = chooseOptimalSize(map.getOutputSizes(SurfaceTexture::class.java),
                        rotatedPreviewWidth, rotatedPreviewHeight, maxPreviewWidth,
                        maxPreviewHeight, largest)

                // We fit the aspect ratio of TextureView to the size of preview we picked.
                val orientation: Int = getResources().getConfiguration().orientation
                if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
                    Log.d("log17", mPreviewSize!!.width.toString() + " " + mPreviewSize!!.height)
                    mTextureView!!.setAspectRatio(mPreviewSize!!.width, mPreviewSize!!.height)
                } else {
                    Log.d("log18", mPreviewSize!!.width.toString() + " " + mPreviewSize!!.height)
                    mTextureView!!.setAspectRatio(mPreviewSize!!.height, mPreviewSize!!.width)
                }

                // Check if the flash is supported.
                val available = characteristics.get(CameraCharacteristics.FLASH_INFO_AVAILABLE)
                mFlashSupported = available ?: false
                mCameraId = cameraId
                return
            }
        } catch (e: CameraAccessException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            // Currently an NPE is thrown when the Camera2API is used but not supported on the
            // device this code runs.
            CommonClass.showSnackbarMessage(rL_rootview, getString(R.string.camera_error))
        }
    }

    /**
     * Opens the camera specified by [Camera2Activity.mCameraId].
     */
    private fun openCamera(width: Int, height: Int) {
        println("$TAG openCamera called...")
        if (runTimePermission!!.checkPermissions(permissionsArray)) {
            setUpCameraOutputs(width, height)
            configureTransform(width, height)
            //   configureTransform(reqWidth, reqHeight);
            val manager = mActivity!!.getSystemService(Context.CAMERA_SERVICE) as CameraManager
            try {
                if (!mCameraOpenCloseLock!!.tryAcquire(2500, TimeUnit.MILLISECONDS)) {
                    throw RuntimeException("Time out waiting to lock camera opening.")
                }
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    return
                }
                manager.openCamera(mCameraId!!, mStateCallback, mBackgroundHandler)
            } catch (e: CameraAccessException) {
                e.printStackTrace()
            } catch (e: InterruptedException) {
                throw RuntimeException("Interrupted while trying to lock camera opening.", e)
            }
        } else {
            runTimePermission!!.requestPermission()
            isToOpenCamera = false
        }
    }

    /**
     * Starts a background thread and its [Handler].
     */
    private fun startBackgroundThread() {
        /*
      An additional thread for running tasks that shouldn't block the UI.
     */
        mBackgroundThread = HandlerThread("CameraBackground")
        mBackgroundThread!!.start()
        mBackgroundHandler = Handler(mBackgroundThread!!.looper)
    }

    /**
     * Creates a new [CameraCaptureSession] for camera preview.
     */
    private fun createCameraPreviewSession() {
        try {
            val texture = mTextureView!!.surfaceTexture!!

            // We configure the size of default buffer to be the size of camera preview we want.
            texture.setDefaultBufferSize(mPreviewSize!!.width, mPreviewSize!!.height)

            // This is the output Surface we need to start preview.
            val surface = Surface(texture)

            // We set up a CaptureRequest.Builder with the output Surface.
            mPreviewRequestBuilder = mCameraDevice!!.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW)
            mPreviewRequestBuilder!!.addTarget(surface)

            // Here, we create a CameraCaptureSession for camera preview.
            mCameraDevice!!.createCaptureSession(Arrays.asList(surface, mImageReader!!.surface),
                    object : CameraCaptureSession.StateCallback() {
                        override fun onConfigured(@NonNull cameraCaptureSession: CameraCaptureSession) {
                            // The camera is already closed
                            if (null == mCameraDevice) {
                                return
                            }
                            Log.d("log81", "85")
                            // When the session is ready, we start displaying the preview.
                            mCaptureSession = cameraCaptureSession
                            Log.d("log81", "88")
                            // Auto focus should be continuous for camera preview.
                            mPreviewRequestBuilder!!.set(CaptureRequest.CONTROL_AF_MODE,
                                    CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE)
                            Log.d("log81", "87")
                            // Flash is automatically enabled when necessary.
                            //setAutoFlash(mPreviewRequestBuilder);
                            Log.d("log81", "86")
                            // Finally, we start displaying the camera preview.
                            mPreviewRequest = mPreviewRequestBuilder!!.build()
                            Log.d("log81", "84")
                            /**
                             * Ashutosh
                             */
                            try {
                                Log.d("log81", "81")
                                mCaptureSession!!.setRepeatingRequest(mPreviewRequest!!,
                                        mCaptureCallback, mBackgroundHandler)
                            } catch (e: CameraAccessException) {
                                /**
                                 * cameraaccessexception
                                 */
                                e.printStackTrace()
                            } finally {
                                mCameraOpenCloseLock?.release()

                                /*if (mCameraDevice!=null)
                                    mCameraDevice.close();*/
                            }
                        }

                        override fun onConfigureFailed(
                                @NonNull cameraCaptureSession: CameraCaptureSession) {
                            println("$TAG failed")
                        }
                    }, null
            )
        } catch (e: CameraAccessException) {
            e.printStackTrace()
        }
    }

    /**
     * Configures the necessary [android.graphics.Matrix] transformation to `mTextureView`.
     * This method should be called after the camera preview size is determined in
     * setUpCameraOutputs and also the size of `mTextureView` is fixed.
     *
     * @param viewWidth  The width of `mTextureView`
     * @param viewHeight The height of `mTextureView`
     */
    private fun configureTransform(viewWidth: Int, viewHeight: Int) {
        if (null == mTextureView || null == mPreviewSize || null == mActivity) {
            return
        }
        val rotation = mActivity!!.windowManager.defaultDisplay.rotation
        val matrix = Matrix()
        val viewRect = RectF(0f, 0f, viewWidth.toFloat(), viewHeight.toFloat())
        val bufferRect = RectF(0f, 0f, mPreviewSize!!.height.toFloat(), mPreviewSize!!.width.toFloat())
        val centerX = viewRect.centerX()
        val centerY = viewRect.centerY()
        if (Surface.ROTATION_90 == rotation || Surface.ROTATION_270 == rotation) {
            bufferRect.offset(centerX - bufferRect.centerX(), centerY - bufferRect.centerY())
            matrix.setRectToRect(viewRect, bufferRect, Matrix.ScaleToFit.FILL)
            val scale = Math.max(
                    viewHeight.toFloat() / mPreviewSize!!.height,
                    viewWidth.toFloat() / mPreviewSize!!.width)
            matrix.postScale(scale, scale, centerX, centerY)
            matrix.postRotate(90 * (rotation - 2).toFloat(), centerX, centerY)
        } else if (Surface.ROTATION_180 == rotation) {
            matrix.postRotate(180f, centerX, centerY)
        }
        mTextureView!!.setTransform(matrix)
        Log.d("log12999", mPreviewSize!!.height.toString() + " " + mPreviewSize!!.width + " " + viewHeight + " " + viewWidth)
    }

    /**
     * Lock the focus as the first step for a still image capture.
     */
    private fun lockFocus() {
        try {
            // This is how to tell the camera to lock focus.
            mPreviewRequestBuilder!!.set(CaptureRequest.CONTROL_AF_TRIGGER, CameraMetadata.CONTROL_AF_TRIGGER_START)
            // Tell #mCaptureCallback to wait for the lock.
            mState = STATE_WAITING_LOCK
            mCaptureSession!!.capture(mPreviewRequestBuilder!!.build(), mCaptureCallback,
                    mBackgroundHandler)
        } catch (e: CameraAccessException) {
            e.printStackTrace()
        } finally {
            mCameraOpenCloseLock?.release()
        }
    }

    /**
     * Run the precapture sequence for capturing a still image. This method should be called when
     * we get a response in [.mCaptureCallback] from [.lockFocus].
     */
    private fun runPrecaptureSequence() {
        try {
            // This is how to tell the camera to trigger.
            mPreviewRequestBuilder!!.set(CaptureRequest.CONTROL_AE_PRECAPTURE_TRIGGER,
                    CaptureRequest.CONTROL_AE_PRECAPTURE_TRIGGER_START)
            // Tell #mCaptureCallback to wait for the precapture sequence to be set.
            mState = STATE_WAITING_PRECAPTURE
            mCaptureSession!!.capture(mPreviewRequestBuilder!!.build(), mCaptureCallback,
                    mBackgroundHandler)
        } catch (e: CameraAccessException) {
            e.printStackTrace()
        } finally {
            mCameraOpenCloseLock?.release()
        }
    }

    /**
     * Capture a still picture. This method should be called when we get a response in
     * [.mCaptureCallback] from both [.lockFocus].
     */
    private fun captureStillPicture() {
        try {
            if (null == mActivity || null == mCameraDevice) {
                return
            }
            // This is the CaptureRequest.Builder that we use to take a picture.
            val captureBuilder = mCameraDevice!!.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE)
            captureBuilder.addTarget(mImageReader!!.surface)
            // Use the same AE and AF modes as the preview.
            captureBuilder.set(CaptureRequest.CONTROL_AF_MODE,
                    CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE)
            //setAutoFlash(captureBuilder);
            // Orientation
            val rotation = mActivity!!.windowManager.defaultDisplay.rotation
            captureBuilder.set(CaptureRequest.JPEG_ORIENTATION, getOrientation(rotation))
            val CaptureCallback: CameraCaptureSession.CaptureCallback = object : CameraCaptureSession.CaptureCallback() {
                override fun onCaptureCompleted(@NonNull session: CameraCaptureSession,
                                                @NonNull request: CaptureRequest,
                                                @NonNull result: TotalCaptureResult) {
                    unlockFocus()
                }
            }
            mCaptureSession!!.stopRepeating()
            mCaptureSession!!.capture(captureBuilder.build(), CaptureCallback, null)
        } catch (e: CameraAccessException) {
            e.printStackTrace()
        }
    }

    /**
     * Retrieves the JPEG orientation from the specified screen rotation.
     *
     * @param rotation The screen rotation.
     * @return The JPEG orientation (one of 0, 90, 270, and 360)
     */
    private fun getOrientation(rotation: Int): Int {
        // Sensor orientation is 90 for most devices, or 270 for some devices (eg. Nexus 5X)
        // We have to take that into account and rotate JPEG properly.
        // For devices with orientation of 90, we simply return our mapping from ORIENTATIONS.
        // For devices with orientation of 270, we need to rotate the JPEG 180 degrees.
        return (ORIENTATIONS[rotation] + mSensorOrientation + 270) % 360
    }

    /**
     * Unlock the focus. This method should be called when still image capture sequence is
     * finished.
     */
    private fun unlockFocus() {
        try {
            // Reset the auto-focus trigger
            mPreviewRequestBuilder!!.set(CaptureRequest.CONTROL_AF_TRIGGER,
                    CameraMetadata.CONTROL_AF_TRIGGER_CANCEL)
            //setAutoFlash(mPreviewRequestBuilder);
            mCaptureSession!!.capture(mPreviewRequestBuilder!!.build(), mCaptureCallback,
                    mBackgroundHandler)
            // After this, the camera will go back to the normal state of preview.
            mState = STATE_PREVIEW
            Log.d("log81", "82")
            mCaptureSession!!.setRepeatingRequest(mPreviewRequest!!, mCaptureCallback,
                    mBackgroundHandler)
        } catch (e: CameraAccessException) {
            e.printStackTrace()
        } finally {
            mCameraOpenCloseLock?.release()
        }
    }

    private fun setAutoFlash(requestBuilder: CaptureRequest.Builder) {
        if (mFlashSupported) {
            requestBuilder.set(CaptureRequest.CONTROL_AE_MODE,
                    CaptureRequest.CONTROL_AE_MODE_ON_AUTO_FLASH)
        }
    }

    /**
     * Saves a JPEG [Image] into the specified [File].
     */
    private inner class ImageSaver internal constructor(
            /**
             * The JPEG image
             */
            private val mImage: Image,
            /**
             * The file we save the image into.
             */
            private val mFile: File) : Runnable {

        override fun run() {
            val buffer = mImage.planes[0].buffer
            val bytes = ByteArray(buffer.remaining())
            //saveCapturedImage(bytes);
            buffer[bytes]
            var output: FileOutputStream? = null
            try {
                output = FileOutputStream(mFile)
                output.write(bytes)
            } catch (e: IOException) {
                e.printStackTrace()
            } finally {
                mImage.close()
                if (null != output) {
                    try {
                        output.close()
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                }
            }
        }

    }

    /**
     * Compares two `Size`s based on their areas.
     */
    private inner class CompareSizesByArea : Comparator<Size> {
        override fun compare(lhs: Size, rhs: Size): Int {
            // We cast here to ensure the multiplications won't overflow
            return java.lang.Long.signum(lhs.width.toLong() * lhs.height -
                    rhs.width.toLong() * rhs.height)
        }
    }

    override fun onClick(v: View) {
        val intent: Intent
        when (v.id) {
            R.id.rL_cancel_btn -> onBackPressed()
            R.id.iV_gallery -> {
                println(TAG + " " + "captured image size=" + arrayListImgPath!!.size)
                intent = Intent(mActivity, GalleryActivity::class.java)
                val galleryImagePaths = ArrayList<String>()
                val rotationAngles = ArrayList<Int>()
                var i = 0
                while (i < arrayListImgPath!!.size) {
                    galleryImagePaths.add(arrayListImgPath!![i].imagePath!!)
                    rotationAngles.add(arrayListImgPath!![i].rotateAngle)
                    i++
                }
                intent.putExtra("arrayListImgPath", galleryImagePaths)
                intent.putExtra("rotationAngles", rotationAngles)
                startActivityForResult(intent, VariableConstants.SELECT_GALLERY_IMG_REQ_CODE)
            }
            R.id.rL_done -> {
                val galleryImagePaths = ArrayList<String>()
                val rotationAngles = ArrayList<Int>()
                var i = 0
                while (i < arrayListImgPath!!.size) {
                    galleryImagePaths.add(arrayListImgPath!![i].imagePath!!)
                    rotationAngles.add(arrayListImgPath!![i].rotateAngle)
                    i++
                }
                if (isFromUpdatePost) {
                    intent = Intent()
                    intent.putExtra("arrayListImgPath", galleryImagePaths)
                    intent.putExtra("rotationAngles", rotationAngles)
                    setResult(VariableConstants.UPDATE_IMAGE_REQ_CODE, intent)
                    onBackPressed()
                } else {
                    if (arrayListImgPath!!.size > 0) {
                        intent = Intent(mActivity, PostProductActivity::class.java)
                        intent.putExtra("arrayListImgPath", galleryImagePaths)
                        intent.putExtra("rotationAngles", rotationAngles)
                        startActivity(intent)
                    }
                }
            }
            R.id.iV_flashIcon -> switchFlash()
        }
    }

    fun switchFlash() {
        if (cameraId == CAMERA_BACK) {
            isTorchOn = if (isTorchOn) {
                mPreviewRequestBuilder!!.set(CaptureRequest.FLASH_MODE, CaptureRequest.FLASH_MODE_OFF)
                //mCaptureSession.setRepeatingRequest(mPreviewRequestBuilder.build(), null, null);
                iV_flashIcon!!.setImageResource(R.drawable.flash_light_off)
                false
            } else {
                mPreviewRequestBuilder!!.set(CaptureRequest.FLASH_MODE, CaptureRequest.FLASH_MODE_TORCH)
                //mCaptureSession.setRepeatingRequest(mPreviewRequestBuilder.build(), null, null);
                iV_flashIcon!!.setImageResource(R.drawable.flash_light_on)
                true
            }
        }
    }

    override fun onBackPressed() {
        finish()
        overridePendingTransition(R.anim.stay, R.anim.slide_down)
    }

    override fun onRequestPermissionsResult(requestCode: Int, @NonNull permissions: Array<String?>, @NonNull grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            VariableConstants.PERMISSION_REQUEST_CODE -> {
                println("grant result=" + grantResults.size)
                if (grantResults.size > 0) {
                    var count = 0
                    while (count < grantResults.size) {
                        if (grantResults[count] != PackageManager.PERMISSION_GRANTED) runTimePermission!!.allowPermissionAlert(permissions[count]!!)
                        count++
                    }
                    println("isAllPermissionGranted=" + runTimePermission!!.checkPermissions(permissionsArray))
                    if (runTimePermission!!.checkPermissions(permissionsArray)) {
                        isToOpenCamera = true
                        println("$TAG openCamera from onRequestPermissionsResult")
                        startBackgroundThread()

                        // When the screen is turned off and turned back on, the SurfaceTexture is already
                        // available, and "onSurfaceTextureAvailable" will not be called. In that case, we can open
                        // a camera and start preview from here (otherwise, we wait until the surface is ready in
                        // the SurfaceTextureListener).
                        if (mTextureView!!.isAvailable && isToOpenCamera) {
                            openCamera(mTextureView!!.width, mTextureView!!.height)
                        } else {
                            mTextureView!!.surfaceTextureListener = mSurfaceTextureListener
                        }
                        //openCamera(mTextureView.getWidth(),mTextureView.getHeight());
                    }
                }
            }
        }
    }

   override  protected fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        println("$TAG on activity result called...$requestCode")
        if (data != null) {
            when (requestCode) {
                VariableConstants.SELECT_GALLERY_IMG_REQ_CODE -> if (data.getStringArrayListExtra("arrayListImgPath") != null && data.getStringArrayListExtra("arrayListImgPath").size > 0) {
                    tV_upload!!.setTextColor(ContextCompat.getColor(mActivity!!, R.color.pink_color))
                    arrayListImgPath!!.clear()
                    val imagePathsFromGallery = data.getStringArrayListExtra("arrayListImgPath")
                    var image: CapturedImage
                    var i = 0
                    while (i < imagePathsFromGallery.size) {
                        image = CapturedImage()
                        image.rotateAngle = 0
                        image.imagePath = imagePathsFromGallery[i]
                        arrayListImgPath!!.add(image)
                        i++
                    }
                    //                        arrayListImgPath.addAll(data.getStringArrayListExtra("arrayListImgPath"));
                    imagesHorizontalRvAdap!!.notifyDataSetChanged()
                } else tV_upload!!.setTextColor(ContextCompat.getColor(mActivity!!, R.color.reset_button_bg))
            }
        }
    }
}