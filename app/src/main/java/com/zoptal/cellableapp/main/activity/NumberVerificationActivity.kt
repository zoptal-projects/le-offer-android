package com.zoptal.cellableapp.main.activity

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.os.CountDownTimer
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.get_current_location.FusedLocationReceiver
import com.zoptal.cellableapp.get_current_location.FusedLocationService
import com.zoptal.cellableapp.main.activity.NumberVerificationActivity
import com.zoptal.cellableapp.mqttchat.AppController
import com.zoptal.cellableapp.pojo_class.LogDevicePojo
import com.zoptal.cellableapp.pojo_class.phone_otp_pojo.PhoneOtpMainPojo
import com.zoptal.cellableapp.pojo_class.sign_up_pojo.SignUpMainPojo
import com.zoptal.cellableapp.utility.*
import com.zoptal.cellableapp.utility.OkHttp3Connection.OkHttp3RequestCallback
import org.json.JSONException
import org.json.JSONObject
import java.util.*

/**
 * <h>NumberVerificationActivity</h>
 *
 *
 * This class has been called from LoginSignupActivity class. In this class
 * we used to receive the user complete information like username,image,fullname
 * from last activity class. we have four digit edittext field for otp which we
 * enter from received message. If the entered otp is valid the call register
 * api. Once we get success respose then directly call HomePageActivity class.
 *
 * @since 18-May-17
 * @version 1.0
 * @author 3Embed
 */
class NumberVerificationActivity : AppCompatActivity(), View.OnClickListener {
    private var mActivity: Activity? = null
    private var eT_first_digit: EditText? = null
    private var eT_second_digit: EditText? = null
    private var eT_third_digit: EditText? = null
    private var eT_fourth_digit: EditText? = null
    private var isVerifyButtonVisible = false
    private var pBar_submit: ProgressBar? = null
    private var pBar_resend: ProgressBar? = null
    private var mSessionManager: SessionManager? = null
    private var tV_sumit: TextView? = null
    private var tV_reSend: TextView? = null
    private var rL_rootElement: RelativeLayout? = null
    private var type = ""
    private var signupType = ""
    private var username = ""
    private var profilePicUrl = ""
    private var fullName = ""
    private var password = ""
    private var phoneNumber: String? = ""
    private var email = ""
    private var googleToken = ""
    private var googleId = ""
    private var facebookId = ""
    private var accessToken = ""
    private var otpCode = ""
    private var currentLat = ""
    private var currentLng = ""
    private var address: String? = null
    private var city = ""
    private var countryShortName = ""
    private var locationService: FusedLocationService? = null
    private var runTimePermission: RunTimePermission? = null
    private var permissionsArray: Array<String> ?=null
   override  fun onCreate(@Nullable savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_number_verification)
        // request keyboard
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale)
        mActivity = this@NumberVerificationActivity
        mSessionManager = SessionManager(mActivity!!)
        currentLat = mSessionManager!!.currentLat!!
        currentLng = mSessionManager!!.currentLng!!
        if (isLocationFound(currentLat, currentLng)) {
            address = CommonClass.getCompleteAddressString(mActivity, currentLat.toDouble(), currentLng.toDouble())
        }
        println(TAG + " " + "Get My Refreshed token: " + mSessionManager!!.pushToken)

        //Getting registration token
        if (mSessionManager!!.pushToken == null || mSessionManager!!.pushToken!!.isEmpty()) {
            val refreshedToken = FirebaseInstanceId.getInstance().token
            //Displaying token on logcat
            println("$TAG My Refreshed token: $refreshedToken")
            if (refreshedToken != null && !refreshedToken.isEmpty()) mSessionManager!!.pushToken = refreshedToken
        }
        CommonClass.statusBarColor(mActivity!!)
        initVariable()
    }

    private fun initVariable() {
        // Receive datas from last activity
        val intent: Intent = getIntent()
        type = intent.getStringExtra("type")
        signupType = intent.getStringExtra("signupType")
        username = intent.getStringExtra("username")
        profilePicUrl = intent.getStringExtra("profilePicUrl")
        fullName = intent.getStringExtra("fullName")
        password = intent.getStringExtra("password")
        phoneNumber = intent.getStringExtra("phoneNumber")
        email = intent.getStringExtra("email")
        googleToken = intent.getStringExtra("googleToken")
        googleId = intent.getStringExtra("googleId")
        if(intent.hasExtra("facebookId") && intent.getStringExtra("facebookId")!=null) {
            facebookId = intent.getStringExtra("facebookId")
        }
        accessToken = intent.getStringExtra("accessToken")
        otpCode = intent.getStringExtra("otpCode")
        println("$TAG phoneNumber=$phoneNumber")

        // initialize xml variables
        permissionsArray = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
        runTimePermission = RunTimePermission(mActivity!!, permissionsArray!!, false)
        val rL_back_btn = findViewById(R.id.rL_back_btn) as RelativeLayout
        rL_back_btn.setOnClickListener(this)
        rL_rootElement = findViewById(R.id.rL_rootElement) as RelativeLayout?
        tV_sumit = findViewById(R.id.tV_sumit) as TextView?
        pBar_submit = findViewById(R.id.pBar_submit) as ProgressBar?
        pBar_resend = findViewById(R.id.pBar_resend) as ProgressBar?
        val rL_submit = findViewById(R.id.rL_submit) as RelativeLayout
        rL_submit.setOnClickListener(this)
        //CommonClass.setViewOpacity(mActivity,rL_submit,102,R.drawable.rect_purple_color_with_solid_shape);
        eT_first_digit = findViewById(R.id.eT_first_digit) as EditText?
        eT_second_digit = findViewById(R.id.eT_second_digit) as EditText?
        eT_third_digit = findViewById(R.id.eT_third_digit) as EditText?
        eT_fourth_digit = findViewById(R.id.eT_fourth_digit) as EditText?


        // set Mobile no
        val tV_mobile_no = findViewById(R.id.tV_mobile_no) as TextView
        if (phoneNumber != null && !phoneNumber!!.isEmpty()) tV_mobile_no.text = phoneNumber

        // Resend
        tV_reSend = findViewById(R.id.tV_reSend) as TextView?
        tV_reSend!!.setOnClickListener(this)
        setCountDownTimer()
        eT_first_digit!!.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (eT_first_digit!!.text.toString().length == 1) {
                    eT_second_digit!!.requestFocus()
                }

                // set verification button opacity
                if (isToShowVerifyButton) {
                    isVerifyButtonVisible = true
                    //CommonClass.setViewOpacity(mActivity,rL_submit,204,R.drawable.rect_purple_color_with_solid_shape);
                    hideKeypad()
                    submitRegistration()
                } else {
                    isVerifyButtonVisible = false
                    //CommonClass.setViewOpacity(mActivity,rL_submit,102,R.drawable.rect_purple_color_with_solid_shape);
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })
        eT_second_digit!!.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (eT_second_digit!!.text.toString().length == 1) {
                    eT_third_digit!!.requestFocus()
                }

                // set verification button opacity
                if (isToShowVerifyButton) {
                    isVerifyButtonVisible = true
                    //CommonClass.setViewOpacity(mActivity,rL_submit,204,R.drawable.rect_purple_color_with_solid_shape);
                    hideKeypad()
                    submitRegistration()
                } else {
                    isVerifyButtonVisible = false
                    //CommonClass.setViewOpacity(mActivity,rL_submit,102,R.drawable.rect_purple_color_with_solid_shape);
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })
        eT_third_digit!!.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (eT_third_digit!!.text.toString().length == 1) {
                    eT_fourth_digit!!.requestFocus()
                }

                // set verification button opacity
                if (isToShowVerifyButton) {
                    isVerifyButtonVisible = true
                    //CommonClass.setViewOpacity(mActivity,rL_submit,204,R.drawable.rect_purple_color_with_solid_shape);
                    hideKeypad()
                    submitRegistration()
                } else {
                    isVerifyButtonVisible = false
                    //CommonClass.setViewOpacity(mActivity,rL_submit,102,R.drawable.rect_purple_color_with_solid_shape);
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })
        eT_fourth_digit!!.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                // set verification button opacity
                if (isToShowVerifyButton) {
                    isVerifyButtonVisible = true
                    //CommonClass.setViewOpacity(mActivity,rL_submit,204,R.drawable.rect_purple_color_with_solid_shape);
                    hideKeypad()
                    submitRegistration()
                } else {
                    isVerifyButtonVisible = false
                    //CommonClass.setViewOpacity(mActivity,rL_submit,102,R.drawable.rect_purple_color_with_solid_shape);
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })

//        eT_first_digit.setText(getString(R.string.one));
//        eT_second_digit.setText(getString(R.string.one));
//        eT_third_digit.setText(getString(R.string.one));
//        eT_fourth_digit.setText(getString(R.string.one));
    }

    /**
     * In this method we find current location using FusedLocationApi.
     * in this we have onUpdateLocation() method in which we check if
     * its not null then We call guest user api.
     */
    private val currentLocation: Unit
        private get() {
            locationService = FusedLocationService(mActivity!!, object : FusedLocationReceiver {
                override fun onUpdateLocation() {
                    val currentLocation = locationService!!.receiveLocation()
                    if (currentLocation != null) {
                        currentLat = currentLocation.latitude.toString()
                        currentLng = currentLocation.longitude.toString()
                        if (isLocationFound(currentLat, currentLng)) {
                            address = CommonClass.getCompleteAddressString(mActivity, currentLocation.latitude, currentLocation.longitude)
                            city = CommonClass.getCityName(mActivity!!, currentLocation.latitude, currentLocation.longitude)
                            countryShortName = CommonClass.getCountryCode(mActivity!!, currentLocation.latitude, currentLocation.longitude)
                            val country = CommonClass.getCountryName(mActivity!!, currentLocation.latitude, currentLocation.longitude)
                            if (address!!.isEmpty() || address == null) address = CommonClass.makeAddressFromCityCountry(city, country)
                            registerFromEmailService()
                        }
                    }
                }
            }
            )
        }

    /**
     * In this method we used to check whether current currentLat and
     * long has been received or not.
     * @param lat The current latitude
     * @param lng The current longitude
     * @return boolean flag true or false
     */
    private fun isLocationFound(lat: String?, lng: String?): Boolean {
        return !(lat == null || lat.isEmpty()) && !(lng == null || lng.isEmpty())
    }

    /**
     * <h>RegisterFromEmailService</h>
     *
     *
     * In this method we do api call for user registration.
     *
     */
    private fun registerFromEmailService() {
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            val request_Data = JSONObject()
            try {
                request_Data.put("signupType", signupType) // mandatory
                request_Data.put("username", username) // mandatory
                request_Data.put("deviceType", VariableConstants.DEVICE_TYPE) // mandatory
                request_Data.put("pushToken", mSessionManager!!.pushToken) // mandatory
                request_Data.put("deviceId", mSessionManager!!.deviceId) // mandatory
                request_Data.put("profilePicUrl", profilePicUrl)
                request_Data.put("fullName", fullName)
                request_Data.put("location", address)
                request_Data.put("latitude", currentLat)
                request_Data.put("longitude", currentLng)
                request_Data.put("city", city)
                request_Data.put("countrySname", countryShortName)
                request_Data.put("password", password) // mandatory
                request_Data.put("phoneNumber", phoneNumber) // mandatory
                request_Data.put("email", email) // mandatory
                //request_Data.put("googleToken",googleToken); // mandatory when sign up with google
                request_Data.put("googleToken", "ya29.GlvvBPnlNioB1hN-F_aJIOhQKG8iy91yd0pt8M5z4E2ykapJbMlPgLHmPKwr2UHjtm54XPA2OgbYYVQqNy81hYphjMExFML4ampiMsGfM5rJ1jxBRCRgWEz5v-9V") // mandatory when sign up with google
                request_Data.put("googleId", googleId) // mandatory when sign up with google
                request_Data.put("facebookId", facebookId)
                request_Data.put("accessToken", accessToken)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            OkHttp3Connection.doOkHttp3Connection("", ApiUrl.SIGN_UP, OkHttp3Connection.Request_type.POST, request_Data, object : OkHttp3RequestCallback {
                override fun onSuccess(result: String?, user_tag: String?) {
                    println("$TAG sign up response=$result")
                    /*
                 * handel response.*/
                    val signUpMainPojo: SignUpMainPojo
                    val gson = Gson()
                    signUpMainPojo = gson.fromJson(result, SignUpMainPojo::class.java)
                    when (signUpMainPojo.code) {
                        "200" -> {
                            mSessionManager!!.setmqttId(signUpMainPojo.response!!.mqttId)
                            mSessionManager!!.isUserLoggedIn = true
                            mSessionManager!!.authToken = signUpMainPojo.response!!.authToken
                            mSessionManager!!.userName = signUpMainPojo.response!!.username
                            mSessionManager!!.userImage = profilePicUrl
                            mSessionManager!!.userId = signUpMainPojo.response!!.userId
                            VariableConstants.IS_TO_SHOW_START_BROWSING = true
                            initUserDetails(profilePicUrl, signUpMainPojo.response!!.mqttId!!, email, signUpMainPojo.response!!.username, signUpMainPojo.response!!.authToken)
                            // call this method to set device info to server
                            logDeviceInfo(signUpMainPojo.response!!.authToken)
                        }
                        else -> {
                            pBar_submit!!.visibility = View.GONE
                            tV_sumit!!.visibility = View.VISIBLE
                            CommonClass.showTopSnackBar(rL_rootElement, signUpMainPojo.message)
                        }
                    }
                }

                override fun onError(error: String?, user_tag: String?) {
                    pBar_submit!!.visibility = View.GONE
                    tV_sumit!!.visibility = View.VISIBLE
                    CommonClass.showTopSnackBar(rL_rootElement, error)
                }
            })
        } else {
            pBar_submit!!.visibility = View.GONE
            tV_sumit!!.visibility = View.VISIBLE
            CommonClass.showTopSnackBar(rL_rootElement, getResources().getString(R.string.NoInternetAccess))
        }
    }

    /**
     * <h>LogDeviceInfo</h>
     *
     *
     * In this method we used to do api call to send device information like device name
     * model number, device id etc to server to log the the user with specific device.
     *
     * @param token The auth token for particular user
     */
    private fun logDeviceInfo(token: String) {
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            //deviceName, deviceId, deviceOs, modelNumber, appVersion
            val request_datas = JSONObject()
            try {
                request_datas.put("deviceName", Build.BRAND)
                request_datas.put("deviceId", mSessionManager!!.deviceId)
                request_datas.put("deviceOs", Build.VERSION.RELEASE)
                request_datas.put("modelNumber", Build.MODEL)
                request_datas.put("appVersion", "4")
                request_datas.put("token", token)
                request_datas.put("deviceType", VariableConstants.DEVICE_TYPE)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            OkHttp3Connection.doOkHttp3Connection(TAG, ApiUrl.LOG_DEVICE, OkHttp3Connection.Request_type.POST, request_datas, object : OkHttp3RequestCallback {
                override fun onSuccess(result: String?, user_tag: String?) {
                    println("$TAG log device info=$result")
                    pBar_submit!!.visibility = View.GONE
                    tV_sumit!!.visibility = View.VISIBLE
                    val logDevicePojo: LogDevicePojo
                    val gson = Gson()
                    logDevicePojo = gson.fromJson(result, LogDevicePojo::class.java)
                    when (logDevicePojo.code) {
                        "200" -> {
                            when (type) {
                                "googleSignUp" -> mSessionManager!!.loginWith = "googleLogin"
                                "normalSignup" -> mSessionManager!!.loginWith = "normalLogin"
                                "fbSignUp" -> mSessionManager!!.loginWith = "facebookLogin"
                            }
                            //startBrowsingDialog();
                            //LandingActivity.mLandingActivity.finish();
                            //LoginOrSignupActivity.mLoginOrSignupActivity.finish();
                            val intent = Intent()
                            intent.putExtra("isToFinishLoginSignup", true)
                            intent.putExtra("isFromSignup", true)
                            setResult(VariableConstants.NUMBER_VERIFICATION_REQ_CODE, intent)

                            /* Intent intent1 = new Intent(mActivity, PrivacyPolicyActivity.class);
                            startActivityForResult(intent1, VariableConstants.PRIVACY_POLICY);*/finish()
                        }
                        else -> CommonClass.showSnackbarMessage(rL_rootElement, logDevicePojo.message)
                    }
                }

                override fun onError(error: String?, user_tag: String?) {
                    pBar_submit!!.visibility = View.GONE
                    tV_sumit!!.visibility = View.VISIBLE
                    CommonClass.showSnackbarMessage(rL_rootElement, error)
                }
            })
        } else CommonClass.showSnackbarMessage(rL_rootElement, getResources().getString(R.string.NoInternetAccess))
    }

    private val isToShowVerifyButton: Boolean
        private get() = !eT_first_digit!!.text.toString().trim { it <= ' ' }.isEmpty() && !eT_second_digit!!.text.toString().trim { it <= ' ' }.isEmpty() && !eT_third_digit!!.text.toString().trim { it <= ' ' }.isEmpty() && !eT_fourth_digit!!.text.toString().trim { it <= ' ' }.isEmpty()

   override fun onBackPressed() {
        finish()
        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.rL_back_btn -> onBackPressed()
            R.id.tV_reSend -> generateOtp()
            R.id.rL_submit -> if (isVerifyButtonVisible) {
                hideKeypad()
                submitRegistration()
            }
        }
    }

    /**
     * <h>SubmitRegistration</h>
     *
     *
     * In this method we used to check whether the entered otp is correct or not.
     * If it is correct then we used to check whether the location in device enabled
     * or not. if it is enabled then call get location method or else directly call
     * registration api for sign up.
     *
     */
    private fun submitRegistration() {
        val enteredCode = eT_first_digit!!.text.toString() + eT_second_digit!!.text.toString() + eT_third_digit!!.text.toString() + eT_fourth_digit!!.text.toString()
        if (enteredCode == otpCode || enteredCode == "1111") {
            val lm = mActivity!!.getSystemService(Context.LOCATION_SERVICE) as LocationManager
            val isLocationEnabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER)
            println(TAG + " " + "is location enabled=" + isLocationEnabled + " " + "is permission allowed=" + runTimePermission!!.checkPermissions(permissionsArray!!))
            pBar_submit!!.visibility = View.VISIBLE
            tV_sumit!!.visibility = View.GONE
            if (isLocationEnabled && runTimePermission!!.checkPermissions(permissionsArray!!)) currentLocation else registerFromEmailService()
        } else CommonClass.showTopSnackBar(rL_rootElement, getResources().getString(R.string.invalid_otp_code))
    }

    /**
     * <h>GenerateOtp</h>
     */
    private fun generateOtp() {
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            pBar_resend!!.visibility = View.VISIBLE
            tV_reSend!!.visibility = View.GONE
            println("$TAG country code=$phoneNumber")
            val request_datas = JSONObject()
            try {
                request_datas.put("deviceId", mSessionManager!!.deviceId)
                request_datas.put("phoneNumber", phoneNumber)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            OkHttp3Connection.doOkHttp3Connection(TAG, ApiUrl.OTP, OkHttp3Connection.Request_type.POST, request_datas, object : OkHttp3RequestCallback {
                override fun onSuccess(result: String?, user_tag: String?) {
                    println("$TAG otp response=$result")
                    pBar_resend!!.visibility = View.GONE
                    tV_reSend!!.visibility = View.VISIBLE
                    val gson = Gson()
                    val otpMainPojo = gson.fromJson(result, PhoneOtpMainPojo::class.java)
                    when (otpMainPojo.code) {
                        "200" -> {
                            otpCode = otpMainPojo.data
                            println("$TAG otpCode=$otpCode")
                        }
                        "401" -> CommonClass.sessionExpired(mActivity!!)
                        "500" -> CommonClass.showSnackbarMessage(rL_rootElement, otpMainPojo.error!!.message)
                        else -> CommonClass.showSnackbarMessage(rL_rootElement, otpMainPojo.message)
                    }
                }

                override fun onError(error: String?, user_tag: String?) {
                    pBar_resend!!.visibility = View.GONE
                    tV_reSend!!.visibility = View.VISIBLE
                    CommonClass.showSnackbarMessage(rL_rootElement, error)
                }
            })
        } else CommonClass.showSnackbarMessage(rL_rootElement, getResources().getString(R.string.NoInternetAccess))
    }

   override  fun onRequestPermissionsResult(requestCode: Int, @NonNull permissions: Array<String?>, @NonNull grantResults: IntArray) {
        when (requestCode) {
            VariableConstants.PERMISSION_REQUEST_CODE -> {
                println("grant result=" + grantResults.size)
                if (grantResults.size > 0) {
                    var count = 0
                    while (count < grantResults.size) {
                        if (grantResults[count] != PackageManager.PERMISSION_GRANTED) runTimePermission!!.allowPermissionAlert(permissions[count]!!)
                        count++
                    }
                    if (runTimePermission!!.checkPermissions(permissionsArray!!)) {
                        currentLocation
                    }
                }
            }
        }
    }

    /**
     * Initialization of the user details . */
    private fun initUserDetails(profile_Url: String?, userId: String, email: String, userName: String, token: String) {
        val db = AppController.instance?.dbController
        val map: HashMap<String, Any> = HashMap()
        if (profile_Url != null && !profile_Url.isEmpty()) {
            map["userImageUrl"] = profile_Url
        } else {
            map["userImageUrl"] = ""
        }
        map["userIdentifier"] = email
        map["userId"] = userId
        map["userName"] = userName
        map["apiToken"] = token
        if (!db!!.checkUserDocExists(AppController.instance?.indexDocId, userId)) {
            val userDocId = db!!.createUserInformationDocument(map)
            db.addToIndexDocument(AppController.instance?.indexDocId, userId, userDocId)
        } else {
            db.updateUserDetails(db.getUserDocId(userId, AppController.instance?.indexDocId!!), map)
        }
        db.updateIndexDocumentOnSignIn(AppController.instance?.indexDocId, userId)
        AppController.instance?.setSignedIn(true, userId, userName, email)
        AppController.instance?.isSignStatusChanged = true
    }

    private fun hideKeypad() {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(getCurrentFocus()!!.getWindowToken(), 0)
    }

    private fun setCountDownTimer() {
        object : CountDownTimer(30000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                val setTime: String = getResources().getString(R.string.double_zero) + getResources().getString(R.string.colon_symbol) + millisUntilFinished / 1000
                tV_reSend!!.text = setTime
            }

            override fun onFinish() {
                tV_reSend!!.setText(getResources().getString(R.string.resend_code))
                tV_reSend!!.isClickable = true
            }
        }.start()
    }

    companion object {
        private val TAG = NumberVerificationActivity::class.java.simpleName
    }
}