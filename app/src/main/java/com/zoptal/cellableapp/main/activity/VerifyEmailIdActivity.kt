package com.zoptal.cellableapp.main.activity

import android.app.Activity
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.*
import androidx.annotation.Nullable
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.gson.Gson
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.fcm_push_notification.Config
import com.zoptal.cellableapp.fcm_push_notification.NotificationMessageDialog
import com.zoptal.cellableapp.fcm_push_notification.NotificationUtils.Companion.clearNotifications
import com.zoptal.cellableapp.main.activity.VerifyEmailIdActivity
import com.zoptal.cellableapp.pojo_class.verify_email_pojo.VerifyEmailMain
import com.zoptal.cellableapp.utility.*
import com.zoptal.cellableapp.utility.OkHttp3Connection.OkHttp3RequestCallback
import org.json.JSONException
import org.json.JSONObject
import java.util.*

/**
 * <h>VerifyEmailIdActivity</h>
 *
 *
 * In this class we used to change email-id and send email to that given mail id to verify.
 *
 * @since 11-Jul-17
 */
class VerifyEmailIdActivity : AppCompatActivity(), View.OnClickListener {
    private var mActivity: Activity? = null
    private var eT_emailId: EditText? = null
    private var isSendButtonEnabled = false
    private var rL_send: RelativeLayout? = null
    private var mSessionManager: SessionManager? = null
    private var linear_rootElement: LinearLayout? = null
    private var progress_bar: ProgressBar? = null
    private var tV_send: TextView? = null
    private var mNotificationMessageDialog: NotificationMessageDialog? = null
     override fun onCreate(@Nullable savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_verify_email)
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale)
        initVariables()
    }

    /**
     * <h>InitVariables</h>
     *
     *
     * In this method we used to initialize all variables.
     *
     */
    private fun initVariables() {
        mActivity = this@VerifyEmailIdActivity
        mNotificationMessageDialog = NotificationMessageDialog(mActivity)
        mSessionManager = SessionManager(mActivity!!)
        progress_bar = findViewById(R.id.progress_bar) as ProgressBar?
        tV_send = findViewById(R.id.tV_send) as TextView?

        // root view
        linear_rootElement = findViewById(R.id.linear_rootElement) as LinearLayout?

        // back button
        val rL_back_btn = findViewById(R.id.rL_back_btn) as RelativeLayout
        rL_back_btn.setOnClickListener(this)

        // submit button
        rL_send = findViewById(R.id.rL_send) as RelativeLayout?
        rL_send!!.setOnClickListener(this)
        CommonClass.setViewOpacity(mActivity, rL_send!!, 102, R.drawable.rect_purple_color_with_solid_shape)

        // EditText Email Address
        eT_emailId = findViewById(R.id.eT_emailId) as EditText?
        eT_emailId!!.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                val email = eT_emailId!!.text.toString()
                if (!email.isEmpty() && CommonClass.isValidEmail(email)) {
                    isSendButtonEnabled = true
                    CommonClass.setViewOpacity(mActivity, rL_send!!, 204, R.drawable.rect_purple_color_with_solid_shape)
                } else {
                    isSendButtonEnabled = false
                    CommonClass.setViewOpacity(mActivity, rL_send!!, 102, R.drawable.rect_purple_color_with_solid_shape)
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })
    }

    override fun onResume() {
        super.onResume()

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.REGISTRATION_COMPLETE))

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!, IntentFilter(Config.PUSH_NOTIFICATION))

        // clear the notification area when the app is opened
        clearNotifications(getApplicationContext())
    }

   override fun onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationMessageDialog!!.mRegistrationBroadcastReceiver!!)
        super.onPause()
    }

    /**
     * <h>verifyEmailId</h>
     *
     *
     * In this method we used to do api call to for eamil verification.
     *
     */
    private fun verifyEmailId() {
        if (CommonClass.isNetworkAvailable(mActivity!!)) {
            progress_bar!!.visibility = View.VISIBLE
            tV_send!!.visibility = View.GONE
            val request_datas = JSONObject()
            try {
                request_datas.put("email", eT_emailId!!.text.toString())
                request_datas.put("token", mSessionManager!!.authToken)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            OkHttp3Connection.doOkHttp3Connection(TAG, ApiUrl.VERIFY_EMAIL, OkHttp3Connection.Request_type.POST, request_datas, object : OkHttp3RequestCallback {
                 override fun onSuccess(result: String?, user_tag: String?) {
                    progress_bar!!.visibility = View.GONE
                    tV_send!!.visibility = View.VISIBLE
                    val verifyEmailMain: VerifyEmailMain
                    val gson = Gson()
                    verifyEmailMain = gson.fromJson(result, VerifyEmailMain::class.java)
                    when (verifyEmailMain.code) {
                        "200" -> {
                            CommonClass.showSuccessSnackbarMsg(linear_rootElement, verifyEmailMain.message)
                            val t = Timer()
                            t.schedule(object : TimerTask() {
                                override fun run() {
                                    val intent = Intent()
                                    intent.putExtra("emailId", eT_emailId!!.text.toString())
                                    setResult(VariableConstants.VERIFY_EMAIL_REQ_CODE, intent)
                                    // when the task active then close the activity
                                    t.cancel()
                                    onBackPressed()
                                }
                            }, 3000)
                        }
                        "401" -> CommonClass.sessionExpired(mActivity!!)
                        else -> CommonClass.showSnackbarMessage(linear_rootElement, verifyEmailMain.message)
                    }
                }

                 override fun onError(error: String?, user_tag: String?) {
                    progress_bar!!.visibility = View.GONE
                    tV_send!!.visibility = View.VISIBLE
                }
            })
        } else CommonClass.showSnackbarMessage(linear_rootElement, getResources().getString(R.string.NoInternetAccess))
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.rL_back_btn -> onBackPressed()
            R.id.rL_send -> if (isSendButtonEnabled) verifyEmailId()
        }
    }

 override fun onBackPressed() {
        finish()
        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate)
    }

    companion object {
        private val TAG = VerifyEmailIdActivity::class.java.simpleName
    }
}