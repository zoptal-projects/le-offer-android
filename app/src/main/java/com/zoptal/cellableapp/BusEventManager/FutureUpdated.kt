package com.zoptal.cellableapp.BusEventManager

/**
 * @since /9/2017.
 * @version 1.0.
 */
class FutureUpdated(var postId: String, var isUpgraded: Boolean)