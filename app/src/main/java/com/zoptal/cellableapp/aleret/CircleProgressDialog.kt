package com.zoptal.cellableapp.aleret

import android.R
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.widget.TextView

/**
 * @since  16/4/16.
 * @author 3Embed.
 */
class CircleProgressDialog private constructor() {
    private var progress_bar: Dialog? = null
    private var progress_bar_title: TextView? = null
    var inflater: LayoutInflater? = null
    fun get_Circle_Progress_bar(mactivity: Activity): Dialog {
        inflater = mactivity.layoutInflater
        if (progress_bar != null) {
            if (progress_bar!!.isShowing) {
                progress_bar!!.dismiss()
            }
        }
        /*
         * Initializing the Circle progress dialog. */progress_bar = Dialog(mactivity, R.style.Theme_Translucent)
        progress_bar!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        @SuppressLint("InflateParams") val dialogView = inflater!!.inflate(com.zoptal.cellableapp.R.layout.circle_progress_dialog, null)
        progress_bar_title = dialogView.findViewById<View>(com.zoptal.cellableapp.R.id.progress_title) as TextView
        progress_bar!!.setContentView(dialogView)
        progress_bar!!.setCancelable(true)
        progress_bar!!.setOnCancelListener {
            //Canceled..
        }
        return progress_bar!!
    }

    fun set_Progress_title(text: String?) {
        if (progress_bar_title != null && text != null) {
            progress_bar_title!!.text = text
        }
    }

    fun set_Title_Color(color_code: Int) {
        if (progress_bar_title != null) {
            progress_bar_title!!.setTextColor(color_code)
        }
    }

    fun set_Title_Text_Style(title_text_style: Typeface?) {
        if (progress_bar_title != null) {
            progress_bar_title!!.typeface = title_text_style
        }
    }

    companion object {
        private var CIRCLE_ALERET: CircleProgressDialog? = CircleProgressDialog()
        @JvmStatic
        val instance: CircleProgressDialog?
            get() = if (CIRCLE_ALERET == null) {
                CIRCLE_ALERET = CircleProgressDialog()
                CIRCLE_ALERET
            } else {
                CIRCLE_ALERET
            }
    }
}