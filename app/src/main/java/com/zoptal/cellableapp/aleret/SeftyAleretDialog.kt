package com.zoptal.cellableapp.aleret

import android.app.Activity

import android.view.View
import androidx.appcompat.app.AlertDialog
import com.zoptal.cellableapp.R

/**
 * <h2>SeftyAleretDialog</h2>
 * <P>
 * To show tha app error aleret
</P> *
 * @since  10/4/2017.
 * @version 1.0.
 * @author 3Embed.
 */
class SeftyAleretDialog private constructor() {
    private var dialog: AlertDialog? = null
    private var dialogView: View? = null
    fun showSefty(activity: Activity) {
        if (dialog != null && dialog!!.isShowing()) {
            dialog!!.dismiss()
        }
        dialogView = activity.layoutInflater.inflate(R.layout.seft_aleret_layout, null)
        val dialogBuilder: AlertDialog.Builder = AlertDialog.Builder(activity, R.style.AppErrorTheme)
        dialogBuilder.setView(dialogView)
        dialogView!!.findViewById<View>(R.id.close_app).setOnClickListener {
            dialog!!.dismiss()
            dialog!!.cancel()
        }
        dialogBuilder.setCancelable(false)
        dialog = dialogBuilder.show()
    }

    companion object {
        val instance = SeftyAleretDialog()
    }
}