package com.zoptal.cellableapp.custom_scroll_view

import android.content.Context
import android.util.AttributeSet
import android.widget.ScrollView

/**
 * <h>AlphaForeGroundColorSpan</h>
 * @since 24.11.2017
 */
class ScrollViewHelper : ScrollView {
    private var mOnScrollViewListener: OnScrollViewListener? = null

    constructor(context: Context?) : super(context) {}
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {}
    constructor(context: Context?, attrs: AttributeSet?, defStyle: Int) : super(context, attrs, defStyle) {}

    interface OnScrollViewListener {
        fun onScrollChanged(v: ScrollViewHelper?, l: Int, t: Int, oldl: Int, oldt: Int)
    }

    fun setOnScrollViewListener(l: OnScrollViewListener?) {
        mOnScrollViewListener = l
    }

    override fun onScrollChanged(l: Int, t: Int, oldl: Int, oldt: Int) {
        mOnScrollViewListener!!.onScrollChanged(this, l, t, oldl, oldt)
        super.onScrollChanged(l, t, oldl, oldt)
    }
}