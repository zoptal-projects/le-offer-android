package com.zoptal.cellableapp.custom_scroll_view

import android.graphics.Color
import android.text.TextPaint
import android.text.style.ForegroundColorSpan

/**
 * <h>AlphaForeGroundColorSpan</h>
 * @since 24.11.2017
 */
class AlphaForeGroundColorSpan(color: Int) : ForegroundColorSpan(color) {
    var alpha = 0f
    override fun updateDrawState(ds: TextPaint) {
        ds.color = alphaColor
    }

    private val alphaColor: Int
        private get() {
            val foregroundColor = foregroundColor
            return Color.argb((alpha * 255).toInt(), Color.red(foregroundColor), Color.green(foregroundColor), Color.blue(foregroundColor))
        }
}