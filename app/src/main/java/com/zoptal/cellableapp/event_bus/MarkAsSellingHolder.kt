package com.zoptal.cellableapp.event_bus

import com.zoptal.cellableapp.pojo_class.home_explore_pojo.ExploreResponseDatas

/**
 * <h>MarkAsSellingHolder</h>
 *
 *
 * This is pojo class for passing events.
 *
 * @since 17-Jul-17
 */
class MarkAsSellingHolder(var details: ExploreResponseDatas)