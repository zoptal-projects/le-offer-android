package com.zoptal.cellableapp.event_bus

import com.squareup.otto.Bus
import com.squareup.otto.ThreadEnforcer

/**
 * @since 14-Jul-17.
 */
object BusProvider {
    val instance = Bus(ThreadEnforcer.ANY)
}