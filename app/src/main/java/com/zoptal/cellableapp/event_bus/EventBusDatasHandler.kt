package com.zoptal.cellableapp.event_bus

import android.app.Activity
import com.zoptal.cellableapp.pojo_class.UserPostDataPojo
import com.zoptal.cellableapp.pojo_class.facebook_friends_pojo.FacebookFriendsPosts
import com.zoptal.cellableapp.pojo_class.home_explore_pojo.ExploreResponseDatas
import com.zoptal.cellableapp.pojo_class.likedPosts.LikedPostResponseDatas
import com.zoptal.cellableapp.pojo_class.phone_contact_pojo.PhoneContactPostData
import com.zoptal.cellableapp.pojo_class.product_details_pojo.ProductResponseDatas
import com.zoptal.cellableapp.pojo_class.profile_selling_pojo.ProfileSellingData
import com.zoptal.cellableapp.pojo_class.profile_sold_pojo.ProfileSoldDatas
import com.zoptal.cellableapp.pojo_class.rate_user_pojo.RateUserDatas
import com.zoptal.cellableapp.pojo_class.social_frag_pojo.SocialDatas
import com.zoptal.cellableapp.pojo_class.sold_somewhere_else_pojo.SoldSomeWhereData
import com.zoptal.cellableapp.utility.SessionManager

/**
 * <h>EventBusDatasHandler</h>
 *
 *
 * This class acts as a mediator between the fragments while passing datas through Event Bus.
 *
 * @since 26-Oct-17.
 */
class EventBusDatasHandler(mActivity: Activity?) {
    private val mSessionManager: SessionManager

    /**
     * <h>SetSocialDatasFromProductDetails</h>
     *
     *
     * In this method we used to add or remove the item from Social frag class
     * when user follow or unfollow product from product details.
     *
     * @param mProductResponse the mProductResponse the pojo class which contains the complete datas of the product.
     * @param isToAdd The boolean flag if it is true then add or else remove.
     */
    fun setSocialDatasFromProductDetails(mProductResponse: ProductResponseDatas?, isToAdd: Boolean) {
        try {
            if (mProductResponse != null) {
                val socialDatas = SocialDatas()
                socialDatas.isToAddSocialData = isToAdd
                socialDatas.postedOn = mProductResponse.postedOn
                socialDatas.productName = mProductResponse.productName
                socialDatas.categoryData = mProductResponse.categoryData
                socialDatas.currency = mProductResponse.currency
                socialDatas.price = mProductResponse.price
                socialDatas.memberProfilePicUrl = mProductResponse.memberProfilePicUrl
                socialDatas.mainUrl = mProductResponse.mainUrl
                socialDatas.likes = mProductResponse.likes
                socialDatas.clickCount = mProductResponse.clickCount
                socialDatas.likeStatus = mProductResponse.likeStatus
                socialDatas.postId = mProductResponse.postId
                socialDatas.membername = mProductResponse.membername
                BusProvider.instance.post(socialDatas)
            }
        } catch (e: Exception) {
            println(TAG + " " + "social event bus error=" + e.message)
        }
    }

    /**
     * <h>SetFavDatasFromProductDetails</h>
     *
     *
     * In this method we used to add or remove the item from Social frag class
     * when user like or unlike product from product details.
     *
     * @param mProductResponseDatas the mProductResponse the pojo class which contains the complete datas of the product.
     * @param isToAdd The boolean flag if it is true then add or else remove.
     */
    fun setFavDatasFromProductDetails(mProductResponseDatas: ProductResponseDatas, isToAdd: Boolean) {
        val likedPostCategoryDatas = LikedPostResponseDatas()
        likedPostCategoryDatas.isToAddLikedItem = isToAdd
        likedPostCategoryDatas.mainUrl = mProductResponseDatas.mainUrl
        likedPostCategoryDatas.productName = mProductResponseDatas.productName
        likedPostCategoryDatas.likes = mProductResponseDatas.likes
        likedPostCategoryDatas.likeStatus = mProductResponseDatas.likeStatus
        likedPostCategoryDatas.currency = mProductResponseDatas.currency
        likedPostCategoryDatas.price = mProductResponseDatas.price
        likedPostCategoryDatas.postedOn = mProductResponseDatas.postedOn
        likedPostCategoryDatas.thumbnailImageUrl = mProductResponseDatas.thumbnailImageUrl
        likedPostCategoryDatas.likedByUsers = mProductResponseDatas.likedByUsers
        likedPostCategoryDatas.description = mProductResponseDatas.description
        likedPostCategoryDatas.condition = mProductResponseDatas.condition
        likedPostCategoryDatas.place = mProductResponseDatas.place
        likedPostCategoryDatas.latitude = mProductResponseDatas.latitude
        likedPostCategoryDatas.longitude = mProductResponseDatas.longitude
        likedPostCategoryDatas.postId = mProductResponseDatas.postId
        likedPostCategoryDatas.postsType = mProductResponseDatas.postType
        likedPostCategoryDatas.containerWidth = mProductResponseDatas.containerWidth
        likedPostCategoryDatas.containerHeight = mProductResponseDatas.containerHeight
        BusProvider.instance.post(likedPostCategoryDatas)
    }

    /**
     * <h>setSocialDatasFromDiscovery</h>
     *
     *
     * In this method we used to add or remove the item from Social frag class
     * when user follow or unfollow product from DiscoverPeople screen.
     *
     * @param mUserPostDataPojo the mProductResponse the pojo class which contains the complete datas of the product.
     * @param isToAdd The boolean flag if it is true then add or else remove.
     */
    fun setSocialDatasFromDiscovery(memberName: String?, memberPic: String?, mUserPostDataPojo: UserPostDataPojo?, isToAdd: Boolean) {
        try {
            if (mUserPostDataPojo != null) {
                val socialDatas = SocialDatas()
                socialDatas.isToAddSocialData = isToAdd
                socialDatas.postedOn = mUserPostDataPojo.postedOn
                socialDatas.productName = mUserPostDataPojo.productName
                socialDatas.categoryData = mUserPostDataPojo.category
                socialDatas.currency = mUserPostDataPojo.currency
                socialDatas.price = mUserPostDataPojo.price
                socialDatas.memberProfilePicUrl = memberPic!!
                socialDatas.mainUrl = mUserPostDataPojo.mainUrl
                socialDatas.likes = mUserPostDataPojo.likes
                socialDatas.clickCount = mUserPostDataPojo.clickCount
                socialDatas.likeStatus = mUserPostDataPojo.likeStatus
                socialDatas.postId = mUserPostDataPojo.postId
                socialDatas.membername = memberName!!
                BusProvider.instance.post(socialDatas)
            }
        } catch (e: Exception) {
            println(TAG + " " + "social event bus error=" + e.message)
        }
    }

    /**
     * <h>setSocialDatasFromFbFriends</h>
     *
     *
     * In this method we used to add or remove the item from Social frag class
     * when user follow or unfollow product from DiscoverPeople screen.
     *
     * @param mFacebookFriendsPosts the mProductResponse the pojo class which contains the complete datas of the product.
     * @param isToAdd The boolean flag if it is true then add or else remove.
     */
    fun setSocialDatasFromFbFriends(mFacebookFriendsPosts: FacebookFriendsPosts?, isToAdd: Boolean) {
        try {
            if (mFacebookFriendsPosts != null) {
                val socialDatas = SocialDatas()
                socialDatas.isToAddSocialData = isToAdd
                socialDatas.postedOn = mFacebookFriendsPosts.postedOn
                socialDatas.productName = ""
                socialDatas.categoryData = null
                socialDatas.currency = ""
                socialDatas.price = ""
                socialDatas.memberProfilePicUrl = ""
                socialDatas.mainUrl = mFacebookFriendsPosts.mainUrl
                socialDatas.likes = mFacebookFriendsPosts.likes
                socialDatas.clickCount = ""
                socialDatas.likeStatus = ""
                socialDatas.postId = mFacebookFriendsPosts.postId
                socialDatas.membername = ""
                BusProvider.instance.post(socialDatas)
            }
        } catch (e: Exception) {
            println(TAG + " " + "social event bus error=" + e.message)
        }
    }

    /**
     * <h>setSocialDatasFromFbFriends</h>
     *
     *
     * In this method we used to add or remove the item from Social frag class
     * when user follow or unfollow product from DiscoverPeople screen.
     *
     * @param phoneContactPostData the mProductResponse the pojo class which contains the complete datas of the product.
     * @param isToAdd The boolean flag if it is true then add or else remove.
     */
    fun setSocialDatasFromContactsFriends(phoneContactPostData: PhoneContactPostData?, isToAdd: Boolean) {
        try {
            if (phoneContactPostData != null) {
                val socialDatas = SocialDatas()
                socialDatas.isToAddSocialData = isToAdd
                socialDatas.postedOn = phoneContactPostData.postedOn
                socialDatas.productName = ""
                socialDatas.categoryData = null
                socialDatas.currency = ""
                socialDatas.price = ""
                socialDatas.memberProfilePicUrl = ""
                socialDatas.mainUrl = phoneContactPostData.mainUrl
                socialDatas.likes = phoneContactPostData.likes
                socialDatas.clickCount = ""
                socialDatas.likeStatus = ""
                socialDatas.postId = phoneContactPostData.postId
                socialDatas.membername = ""
                BusProvider.instance.post(socialDatas)
            }
        } catch (e: Exception) {
            println(TAG + " " + "social event bus error=" + e.message)
        }
    }

    /**
     * <h>AddSoldDatas</h>
     *
     *
     * In this method we used to make obj of Sold Frag datas set all values of that and
     * send that object to the SoldFrag via event bus.
     *
     * @param mSoldSomeWhereData The reference variables of SoldSomeWhereData.
     */
    fun addSoldDatasFromEditPost(mSoldSomeWhereData: SoldSomeWhereData) {
        val profileSoldDatas = ProfileSoldDatas()
        profileSoldDatas.postNodeId = mSoldSomeWhereData.postNodeId
        profileSoldDatas.isPromoted = ""
        profileSoldDatas.planId = ""
        profileSoldDatas.likes = ""
        profileSoldDatas.mainUrl = mSoldSomeWhereData.mainUrl
        profileSoldDatas.postLikedBy = ""
        profileSoldDatas.place = mSoldSomeWhereData.place
        profileSoldDatas.thumbnailImageUrl = mSoldSomeWhereData.thumbnailImageUrl
        profileSoldDatas.postId = mSoldSomeWhereData.postId
        profileSoldDatas.productsTagged = mSoldSomeWhereData.productsTagged
        profileSoldDatas.productsTaggedCoordinates = ""
        profileSoldDatas.hasAudio = mSoldSomeWhereData.hasAudio
        profileSoldDatas.containerHeight = mSoldSomeWhereData.containerHeight
        profileSoldDatas.containerWidth = mSoldSomeWhereData.containerWidth
        profileSoldDatas.hashTags = ""
        profileSoldDatas.postCaption = mSoldSomeWhereData.postCaption
        profileSoldDatas.latitude = mSoldSomeWhereData.latitude
        profileSoldDatas.longitude = mSoldSomeWhereData.longitude
        profileSoldDatas.thumbnailUrl1 = mSoldSomeWhereData.thumbnailUrl1
        profileSoldDatas.imageUrl1 = mSoldSomeWhereData.imageUrl1
        profileSoldDatas.thumbnailImageUrl = mSoldSomeWhereData.thumbnailImageUrl
        profileSoldDatas.containerWidth1 = mSoldSomeWhereData.containerWidth1
        profileSoldDatas.containerHeight1 = mSoldSomeWhereData.containerHeight1
        profileSoldDatas.imageUrl2 = mSoldSomeWhereData.imageUrl2
        profileSoldDatas.thumbnailUrl2 = mSoldSomeWhereData.thumbnailUrl2
        profileSoldDatas.containerHeight2 = mSoldSomeWhereData.containerHeight2
        profileSoldDatas.containerWidth2 = mSoldSomeWhereData.containerWidth2
        profileSoldDatas.thumbnailUrl3 = mSoldSomeWhereData.thumbnailUrl3
        profileSoldDatas.imageUrl3 = mSoldSomeWhereData.imageUrl3
        profileSoldDatas.containerHeight3 = mSoldSomeWhereData.containerHeight3
        profileSoldDatas.containerWidth3 = mSoldSomeWhereData.containerWidth3
        profileSoldDatas.thumbnailUrl4 = mSoldSomeWhereData.thumbnailUrl4
        profileSoldDatas.imageUrl4 = mSoldSomeWhereData.imageUrl4
        profileSoldDatas.containerHeight4 = mSoldSomeWhereData.containerHeight4
        profileSoldDatas.containerWidth4 = mSoldSomeWhereData.containerWidth4
        profileSoldDatas.postsType = ""
        profileSoldDatas.postedOn = mSoldSomeWhereData.postedOn
        profileSoldDatas.likeStatus = ""
        profileSoldDatas.sold = mSoldSomeWhereData.sold
        profileSoldDatas.productUrl = mSoldSomeWhereData.productUrl
        profileSoldDatas.description = mSoldSomeWhereData.description
        profileSoldDatas.negotiable = ""
        profileSoldDatas.condition = mSoldSomeWhereData.condition
        profileSoldDatas.price = mSoldSomeWhereData.price
        profileSoldDatas.currency = mSoldSomeWhereData.currency
        profileSoldDatas.productName = mSoldSomeWhereData.productName
        profileSoldDatas.totalComments = ""
        profileSoldDatas.categoryData = mSoldSomeWhereData.categoryData
        BusProvider.instance.post(profileSoldDatas)
    }

    /**
     * <h>AddSoldDatas</h>
     *
     *
     * In this method we used to make obj of Sold Frag datas set all values of that and
     * send that object to the SoldFrag via event bus.
     *
     * @param mRateUserDatas The reference variables of SoldSomeWhereData.
     */
    fun addSoldDatasFromRateUser(mRateUserDatas: RateUserDatas) {
        val profileSoldDatas = ProfileSoldDatas()
        profileSoldDatas.postNodeId = mRateUserDatas.postNodeId
        profileSoldDatas.isPromoted = ""
        profileSoldDatas.planId = ""
        profileSoldDatas.likes = ""
        profileSoldDatas.mainUrl = mRateUserDatas.mainUrl
        profileSoldDatas.postLikedBy = ""
        profileSoldDatas.place = mRateUserDatas.place
        profileSoldDatas.thumbnailImageUrl = mRateUserDatas.thumbnailImageUrl
        profileSoldDatas.postId = mRateUserDatas.postId
        profileSoldDatas.productsTagged = mRateUserDatas.productsTagged
        profileSoldDatas.productsTaggedCoordinates = ""
        profileSoldDatas.hasAudio = ""
        profileSoldDatas.containerHeight = mRateUserDatas.containerHeight
        profileSoldDatas.containerWidth = mRateUserDatas.containerWidth
        profileSoldDatas.hashTags = ""
        profileSoldDatas.postCaption = mRateUserDatas.postCaption
        profileSoldDatas.latitude = mRateUserDatas.latitude
        profileSoldDatas.longitude = mRateUserDatas.longitude
        profileSoldDatas.thumbnailUrl1 = mRateUserDatas.thumbnailUrl1
        profileSoldDatas.imageUrl1 = mRateUserDatas.imageUrl1
        profileSoldDatas.thumbnailImageUrl = mRateUserDatas.thumbnailImageUrl
        profileSoldDatas.containerWidth1 = mRateUserDatas.containerWidth1
        profileSoldDatas.containerHeight1 = mRateUserDatas.containerHeight1
        profileSoldDatas.imageUrl2 = mRateUserDatas.imageUrl2
        profileSoldDatas.thumbnailUrl2 = mRateUserDatas.thumbnailUrl2
        profileSoldDatas.containerHeight2 = mRateUserDatas.containerHeight2
        profileSoldDatas.containerWidth2 = mRateUserDatas.containerWidth2
        profileSoldDatas.thumbnailUrl3 = mRateUserDatas.thumbnailUrl3
        profileSoldDatas.imageUrl3 = mRateUserDatas.imageUrl3
        profileSoldDatas.containerHeight3 = mRateUserDatas.containerHeight3
        profileSoldDatas.containerWidth3 = mRateUserDatas.containerWidth3
        profileSoldDatas.thumbnailUrl4 = mRateUserDatas.thumbnailUrl4
        profileSoldDatas.imageUrl4 = mRateUserDatas.imageUrl4
        profileSoldDatas.containerHeight4 = mRateUserDatas.containerHeight4
        profileSoldDatas.containerWidth4 = mRateUserDatas.containerWidth4
        profileSoldDatas.postsType = ""
        profileSoldDatas.postedOn = mRateUserDatas.postedOn
        profileSoldDatas.likeStatus = ""
        profileSoldDatas.sold = mRateUserDatas.sold
        profileSoldDatas.productUrl = mRateUserDatas.productUrl
        profileSoldDatas.description = mRateUserDatas.description
        profileSoldDatas.negotiable = ""
        profileSoldDatas.condition = mRateUserDatas.condition
        profileSoldDatas.price = mRateUserDatas.price
        profileSoldDatas.currency = mRateUserDatas.currency
        profileSoldDatas.productName = mRateUserDatas.productName
        profileSoldDatas.totalComments = ""
        profileSoldDatas.categoryData = mRateUserDatas.categoryData
        BusProvider.instance.post(profileSoldDatas)
    }

    /**
     * <h>AddHomePageDatas</h>
     *
     *
     * In this method we used to make obj of Home Frag datas set all values of that and
     * send that object to the Selling via event bus.
     *
     */
    fun removeHomePageDatasFromEditPost(postId: String?) {
        val mExploreResponseDatas = ExploreResponseDatas()
        mExploreResponseDatas.isToRemoveHomeItem = true
        mExploreResponseDatas.postId = postId!!
        BusProvider.instance.post(mExploreResponseDatas)
    }

    /**
     * <h>SetSocialDatas</h>
     *
     *
     * In this method we used to send the one complete object of a followed product
     * to the socail screen through screen.
     *
     */
    fun removeSocialDatasFromEditPost(postId: String?) {
        try {
            val socialDatas = SocialDatas()
            socialDatas.isToAddSocialData = false
            socialDatas.postId = postId!!
            BusProvider.instance.post(socialDatas)
        } catch (e: Exception) {
            println(TAG + " " + "social event bus error=" + e.message)
        }
    }

    /**
     * <h>AddSellingDatas</h>
     *
     *
     * In this method we used to make obj of Selling Frag datas set all values of that and
     * send that object to the Selling via event bus.
     *
     */
    fun removeSellingDatasFromEditPost(postId: String?) {
        val profileSellingData = ProfileSellingData()
        profileSellingData.isToRemoveSellingItem = true
        profileSellingData.postId = postId!!
        BusProvider.instance.post(profileSellingData)
    }

    /**
     * <h>AddSellingDatas</h>
     *
     *
     * In this method we used to make obj of Selling Frag datas set all values of that and
     * send that object to the Selling via event bus.
     *
     * @param mProductResponseDatas The reference variables of ProductResponseDatas.
     */
    fun addSellingDatasFromProductDetails(mProductResponseDatas: ProductResponseDatas) {
        val profileSellingData = ProfileSellingData()
        profileSellingData.postNodeId = ""
        profileSellingData.isPromoted = ""
        profileSellingData.planId = ""
        profileSellingData.likes = mProductResponseDatas.likes
        profileSellingData.mainUrl = mProductResponseDatas.mainUrl
        profileSellingData.postLikedBy = ""
        profileSellingData.place = mProductResponseDatas.place
        profileSellingData.thumbnailImageUrl = mProductResponseDatas.thumbnailImageUrl
        profileSellingData.postId = mProductResponseDatas.postId
        profileSellingData.productsTagged = mProductResponseDatas.productsTagged
        profileSellingData.productsTaggedCoordinates = mProductResponseDatas.productsTaggedCoordinates
        profileSellingData.hasAudio = mProductResponseDatas.hasAudio
        profileSellingData.containerHeight = mProductResponseDatas.containerHeight
        profileSellingData.containerWidth = mProductResponseDatas.containerWidth
        profileSellingData.hashTags = mProductResponseDatas.hashTags
        profileSellingData.postCaption = ""
        profileSellingData.latitude = mProductResponseDatas.latitude
        profileSellingData.longitude = mProductResponseDatas.longitude
        profileSellingData.thumbnailUrl1 = mProductResponseDatas.thumbnailUrl1
        profileSellingData.imageUrl1 = mProductResponseDatas.imageUrl1
        profileSellingData.thumbnailImageUrl = mProductResponseDatas.thumbnailImageUrl
        profileSellingData.containerWidth1 = mProductResponseDatas.containerWidth1
        profileSellingData.containerHeight1 = mProductResponseDatas.containerHeight1
        profileSellingData.imageUrl2 = mProductResponseDatas.imageUrl2
        profileSellingData.thumbnailUrl2 = mProductResponseDatas.thumbnailUrl2
        profileSellingData.containerHeight2 = mProductResponseDatas.containerHeight2
        profileSellingData.containerWidth2 = mProductResponseDatas.containerWidth2
        profileSellingData.thumbnailUrl3 = mProductResponseDatas.thumbnailUrl3
        profileSellingData.imageUrl3 = mProductResponseDatas.imageUrl3
        profileSellingData.containerHeight3 = mProductResponseDatas.containerHeight3
        profileSellingData.containerWidth3 = mProductResponseDatas.containerWidth3
        profileSellingData.thumbnailUrl4 = mProductResponseDatas.thumbnailUrl4
        profileSellingData.imageUrl4 = mProductResponseDatas.imageUrl4
        profileSellingData.containerHeight4 = mProductResponseDatas.containerHeight4
        profileSellingData.containerWidth4 = mProductResponseDatas.containerWidth4
        profileSellingData.postsType = ""
        profileSellingData.postedOn = mProductResponseDatas.postedOn
        profileSellingData.likeStatus = mProductResponseDatas.likeStatus
        profileSellingData.sold = "" + mProductResponseDatas.getIsSold()
        profileSellingData.productUrl = mProductResponseDatas.productUrl
        profileSellingData.description = mProductResponseDatas.description
        profileSellingData.negotiable = mProductResponseDatas.negotiable
        profileSellingData.condition = mProductResponseDatas.condition
        profileSellingData.price = mProductResponseDatas.price
        profileSellingData.currency = mProductResponseDatas.currency
        profileSellingData.productName = mProductResponseDatas.productName
        profileSellingData.totalComments = ""
        profileSellingData.categoryData = mProductResponseDatas.categoryData
        BusProvider.instance.post(profileSellingData)
    }

    /**
     * <h>AddHomePageDatas</h>
     *
     *
     * In this method we used to make obj of Home Frag datas set all values of that and
     * send that object to the Selling via event bus.
     *
     * @param mProductResponseDatas The reference variables of ProductResponseDatas.
     */
    fun addHomePageDatasFromProductDetails(mProductResponseDatas: ProductResponseDatas) {
        val mExploreResponseDatas = ExploreResponseDatas()
        mExploreResponseDatas.postedByUserName = mSessionManager.userName!!
        mExploreResponseDatas.postNodeId = ""
        mExploreResponseDatas.isPromoted = ""
        mExploreResponseDatas.likes = mProductResponseDatas.likes
        mExploreResponseDatas.mainUrl = mProductResponseDatas.mainUrl
        mExploreResponseDatas.place = mProductResponseDatas.place
        mExploreResponseDatas.thumbnailImageUrl = mProductResponseDatas.thumbnailImageUrl
        mExploreResponseDatas.postId = mProductResponseDatas.postId
        mExploreResponseDatas.hasAudio = mProductResponseDatas.hasAudio
        mExploreResponseDatas.containerHeight = mProductResponseDatas.containerHeight
        mExploreResponseDatas.containerWidth = mProductResponseDatas.containerWidth
        mExploreResponseDatas.hashTags = mProductResponseDatas.hashTags
        mExploreResponseDatas.postCaption = ""
        mExploreResponseDatas.latitude = mProductResponseDatas.latitude
        mExploreResponseDatas.longitude = mProductResponseDatas.longitude
        mExploreResponseDatas.thumbnailUrl1 = mProductResponseDatas.thumbnailUrl1
        mExploreResponseDatas.imageUrl1 = mProductResponseDatas.imageUrl1
        mExploreResponseDatas.thumbnailImageUrl = mProductResponseDatas.thumbnailImageUrl
        mExploreResponseDatas.containerWidth1 = mProductResponseDatas.containerWidth1
        mExploreResponseDatas.containerHeight1 = mProductResponseDatas.containerHeight1
        mExploreResponseDatas.imageUrl2 = mProductResponseDatas.imageUrl2
        mExploreResponseDatas.thumbnailUrl2 = mProductResponseDatas.thumbnailUrl2
        mExploreResponseDatas.containerHeight2 = mProductResponseDatas.containerHeight2
        mExploreResponseDatas.containerWidth2 = mProductResponseDatas.containerWidth2
        mExploreResponseDatas.thumbnailUrl3 = mProductResponseDatas.thumbnailUrl3
        mExploreResponseDatas.imageUrl3 = mProductResponseDatas.imageUrl3
        mExploreResponseDatas.containerHeight3 = mProductResponseDatas.containerHeight3
        mExploreResponseDatas.containerWidth3 = mProductResponseDatas.containerWidth3
        mExploreResponseDatas.thumbnailUrl4 = mProductResponseDatas.thumbnailUrl4
        mExploreResponseDatas.imageUrl4 = mProductResponseDatas.imageUrl4
        mExploreResponseDatas.containerHeight4 = mProductResponseDatas.containerHeight4
        mExploreResponseDatas.containerWidth4 = mProductResponseDatas.containerWidth4
        mExploreResponseDatas.postsType = ""
        mExploreResponseDatas.postedOn = mProductResponseDatas.postedOn
        mExploreResponseDatas.likeStatus = mProductResponseDatas.likeStatus
        mExploreResponseDatas.productUrl = mProductResponseDatas.productUrl
        mExploreResponseDatas.description = mProductResponseDatas.description
        mExploreResponseDatas.negotiable = mProductResponseDatas.negotiable
        mExploreResponseDatas.condition = mProductResponseDatas.condition
        mExploreResponseDatas.price = mProductResponseDatas.price
        mExploreResponseDatas.currency = mProductResponseDatas.currency
        mExploreResponseDatas.productName = mProductResponseDatas.productName
        mExploreResponseDatas.totalComments = ""
        mExploreResponseDatas.categoryData = mProductResponseDatas.categoryData
        BusProvider.instance.post(mExploreResponseDatas)
    }

    /**
     * <h>SetSocialDatas</h>
     *
     *
     * In this method we used to send the one complete object of a followed product
     * to the socail screen through screen.
     *
     */
    fun setSocialDatasFromProductDetails(mProductResponseDatas: ProductResponseDatas) {
        try {
            val socialDatas = SocialDatas()
            socialDatas.isToAddSocialData = true
            socialDatas.postedOn = mProductResponseDatas.postedOn
            socialDatas.productName = mProductResponseDatas.productName
            socialDatas.categoryData = mProductResponseDatas.categoryData
            socialDatas.currency = mProductResponseDatas.currency
            socialDatas.price = mProductResponseDatas.price
            socialDatas.memberProfilePicUrl = mProductResponseDatas.memberProfilePicUrl
            socialDatas.mainUrl = mProductResponseDatas.mainUrl
            socialDatas.likes = mProductResponseDatas.likes
            socialDatas.clickCount = mProductResponseDatas.clickCount
            socialDatas.likeStatus = mProductResponseDatas.likeStatus
            socialDatas.postId = mProductResponseDatas.postId
            socialDatas.membername = mSessionManager.userName!!
            BusProvider.instance.post(socialDatas)
        } catch (e: Exception) {
            println(TAG + " " + "social event bus error=" + e.message)
        }
    }

    companion object {
        private val TAG = EventBusDatasHandler::class.java.simpleName
    }

    init {
        mSessionManager = SessionManager(mActivity!!)
    }
}