package com.zoptal.cellableapp.Uploader

import android.content.Context
import android.os.AsyncTask
import android.util.Log
import com.google.gson.Gson
import com.zoptal.cellableapp.utility.ApiUrl
import com.zoptal.cellableapp.utility.OkHttp3Connection
import com.zoptal.cellableapp.utility.OkHttp3Connection.OkHttp3RequestCallback
import com.zoptal.cellableapp.utility.OkHttp3Connection.doOkHttp3Connection
import com.zoptal.cellableapp.utility.SessionManager
import org.json.JSONException
import org.json.JSONObject
import java.util.*

/**
 * @since  8/19/2017.
 */
class FileUploader private constructor(context: Context) {
    private val success_list: MutableList<ProductImageDatas>
    private val failed_list: MutableList<ProductImageDatas>
    private val list: MutableList<String?>
    private var uploaderCallback: UploaderCallback? = null
    private var callback: UploadedCallback? = null
    private val sessionManager: SessionManager

    //..To store the imagelist in capture sequence before uploading, same as "list"
    private val imageOrderList: MutableList<String?>
    fun UploadMultiple(fileList: List<String?>?, tempcallback: UploadedCallback?) {
        callback = tempcallback
        list.clear()
        success_list.clear()
        failed_list.clear()
        list.addAll(fileList!!)
        imageOrderList.addAll(fileList)
        if (list.size > 0) {
            collectClodudDetails()
        } else {
            if (callback != null) callback!!.onError("No file to upload!")
        }
    }

    /*
     *getting all the cloudinary details.*/
    private fun collectClodudDetails() {
        val request_data = JSONObject()
        try {
            request_data.put("deviceId", sessionManager.deviceId)
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        doOkHttp3Connection("", ApiUrl.GET_CLOUDINARY_DETAILS, OkHttp3Connection.Request_type.POST, request_data, object : OkHttp3RequestCallback {
            override fun onSuccess(result: String?, user_tag: String?) {
                try {
                    val response = Gson().fromJson(result, Cloudinary_Details_reponse::class.java)
                    if (response.code == "200") {
                        createMultiUploader(response.response!!.cloudName, response.response!!.apiKey, response.response!!.signature, response.response!!.timestamp)
                    } else {
                        if (callback != null) callback!!.onError(response.message)
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                    if (callback != null) callback!!.onError("Error message!")
                }
            }

            override fun onError(error: String?, user_tag: String?) {
                if (callback != null) callback!!.onError(error)
            }
        })
    }

    private fun createMultiUploader(name: String?, key: String?, signeture: String?, timestamp: String?) {
        for (index in list.indices) {
            val uploadDetail = createCloudData(name, key, signeture, timestamp)
            uploadDetail.file_path = list[index]
            Uploader().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, uploadDetail)
        }
    }

    private fun initUploaderCallback() {
        uploaderCallback = object : UploaderCallback {
            override fun onSuccess(resourceId: String?, mainUrl: String?, width: Int, height: Int, publicId: String?) {
                val temp_data = ProductImageDatas()
                temp_data.mainUrl = mainUrl
                temp_data.thumbnailUrl = create_Image_thumbnail(mainUrl)
                temp_data.width = width
                temp_data.height = height
                temp_data.public_id = publicId
                temp_data.message = "Sucess"

                //..catch up index by compare with resource_id of uploaded.
                // resouceid is same before uploading and after uploading
                val index = imageOrderList.indexOf(resourceId)
                Log.d("index=", index.toString() + "")
                temp_data.index = index
                //////////////////////////////////////////////////////////
                success_list.add(temp_data)
                list.remove(resourceId)
                if (list.size < 1) {
                    if (callback != null) callback!!.onSuccess(success_list, failed_list)
                }
            }

            override fun onError(resourceId: String?, msg: String?) {
                val temp_data = ProductImageDatas()
                temp_data.mainUrl = resourceId
                temp_data.mainUrl = null
                temp_data.message = "Failed"
                failed_list.add(temp_data)
                list.remove(resourceId)
                if (list.size < 1) {
                    if (callback != null) callback!!.onSuccess(success_list, failed_list)
                }
            }
        }
    }

    /*
     * Parsing the success response*/
    private fun createCloudData(name: String?, key: String?, signeture: String?, timestamp: String?): UploadDetail {
        val uploadDetail = UploadDetail()
        uploadDetail.apiKey = key
        uploadDetail.cloudName = name
        uploadDetail.timestamp = timestamp
        uploadDetail.signature = signeture
        uploadDetail.timestamp = timestamp
        uploadDetail.callback = uploaderCallback
        return uploadDetail
    }

    private fun create_Image_thumbnail(image_url: String?): String? {
        val key_word = "upload"
        val length_key = key_word.length
        val index = image_url!!.indexOf("upload")
        return if (index > 0) {
            val firs_sub_String = image_url.substring(0, index + length_key)
            val last_sub_String = image_url.substring(index + length_key)
            firs_sub_String + ConfigFile.IMAGE_THUMBNAIL_SIZE + last_sub_String
        } else {
            image_url
        }
    }

    companion object {
        private var fileUploader: FileUploader? = null
        fun getFileUploader(context: Context): FileUploader? {
            if (fileUploader == null) {
                fileUploader = FileUploader(context)
            }
            return fileUploader
        }
    }

    init {
        sessionManager = SessionManager(context)
        list = ArrayList()
        imageOrderList = ArrayList()
        success_list = ArrayList()
        failed_list = ArrayList()
        initUploaderCallback()
    }
}