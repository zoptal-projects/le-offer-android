package com.zoptal.cellableapp.Uploader

/**
 * @since  8/19/2017.
 */
object ConfigFile {
    /*
  * Image thumb nail size*/
    const val IMAGE_THUMBNAIL_SIZE = "/q_60,w_150,h_150,c_thumb"
}