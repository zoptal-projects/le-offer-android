package com.zoptal.cellableapp.Uploader

import java.io.Serializable

/**
 * @since 8/19/2017.
 */
class ProductImageDatas : Serializable {
    var message: String? = null
    var mainUrl: String? = null
    var thumbnailUrl: String? = null
    var public_id: String? = null
    var width = 0
    var height = 0
    var isImageUrl = false
    var rotationAngle = 0
    var index = 0

}