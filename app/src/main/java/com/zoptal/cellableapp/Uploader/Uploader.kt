package com.zoptal.cellableapp.Uploader

import android.os.AsyncTask
import com.cloudinary.Cloudinary
import com.cloudinary.utils.ObjectUtils
import java.io.File
import java.util.*

/**
 * <h2>Uploader</h2>
 * Uploading a file to cloudinary in android.
 * @since 8/19/2017.
 */
internal class Uploader : AsyncTask<UploadDetail?, Void?, Map<*, *>?>() {
    private var isError = false
    private var callback: UploaderCallback? = null
    private var resource_id: String? = null
    protected override fun doInBackground(vararg params: UploadDetail?): Map<*, *>? {
        var uploaded_details: Map<*, *>? = null
        val config: HashMap<String, String> = HashMap<String, String>()
        val uploadDetail = params[0]
        callback = uploadDetail!!.callback
        resource_id = uploadDetail.file_path
        config.put("cloud_name",uploadDetail.cloudName!!)
        val cloudinary = Cloudinary(config)
        try {
            val data = ObjectUtils.asMap("signature", uploadDetail.signature, "timestamp", uploadDetail.timestamp, "api_key", uploadDetail.apiKey)
            uploaded_details = cloudinary.uploader().uploadLarge(File(uploadDetail.file_path), data)
            isError = false
        } catch (e: Exception) {
            isError = true
            e.printStackTrace()
        }
        return uploaded_details
    }

    override fun onPostExecute(data: Map<*, *>?) {
        super.onPostExecute(data)
        if (!isError && data != null) {
            //String main_url=(String)data.get("url");
            val main_url: String?
            main_url = if (data.containsKey("secure_url")) {
                data["secure_url"] as String?
            } else {
                data["url"] as String?
            }
            val width = data["width"] as Int
            val height = data["height"] as Int
            val publicId = data["public_id"] as String?
            callback!!.onSuccess(resource_id, main_url, width, height, publicId)
        } else {
            if (callback != null) callback!!.onError(resource_id, "Upload error")
        }
    }
}