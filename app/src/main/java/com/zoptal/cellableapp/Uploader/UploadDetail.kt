package com.zoptal.cellableapp.Uploader

/**
 * @since  8/19/2017.
 */
class UploadDetail {
    var signature: String? = null
    var timestamp: String? = null
    var apiKey: String? = null
    var cloudName: String? = null
    var callback: UploaderCallback? = null
    var file_path: String? = null
}