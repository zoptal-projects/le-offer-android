package com.zoptal.cellableapp.Uploader

/**
 * @since  8/19/2017.
 */
interface UploaderCallback {
    fun onSuccess(resourseId: String?, mainUrl: String?, width: Int, height: Int, publicId: String?)
    fun onError(resourseId: String?, msg: String?)
}