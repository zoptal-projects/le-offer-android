package com.zoptal.cellableapp.Uploader

/**
 * @since  8/19/2017.
 */
interface UploadedCallback {
    fun onSuccess(data_list: List<ProductImageDatas>?, failed_list: List<ProductImageDatas>?)
    fun onError(error: String?)
}