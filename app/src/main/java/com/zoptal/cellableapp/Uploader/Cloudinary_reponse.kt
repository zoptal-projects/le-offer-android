package com.zoptal.cellableapp.Uploader

/**
 * @since  8/19/2017.
 */
class Cloudinary_reponse {
    var timestamp: String? = null
    var signature: String? = null
    var apiKey: String? = null
    var cloudName: String? = null

}