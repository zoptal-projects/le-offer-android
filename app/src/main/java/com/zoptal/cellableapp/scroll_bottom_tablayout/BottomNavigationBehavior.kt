package com.zoptal.cellableapp.scroll_bottom_tablayout

import android.content.Context

import android.util.AttributeSet
import android.view.View
import android.view.animation.Interpolator
import androidx.annotation.IdRes
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout.Behavior
import androidx.core.view.ViewCompat
import androidx.core.view.ViewPropertyAnimatorCompat
import androidx.interpolator.view.animation.LinearOutSlowInInterpolator
import com.google.android.material.tabs.TabLayout
import com.zoptal.cellableapp.R

/**
 * @author Waleed Sarwar
 * @since September 28, 2016 11:50 AM
 */
class BottomNavigationBehavior<V : View?> : VerticalScrollingBehavior<V> {
    private var mTabLayoutId = 0
    private var hidden = false
    private var mTranslationAnimator: ViewPropertyAnimatorCompat? = null
    private var mTabLayout: TabLayout? = null
    private var mTabsHolder: View? = null

    constructor() : super() {}
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        val a = context.obtainStyledAttributes(attrs,
                R.styleable.BottomNavigationBehavior_Params)
        mTabLayoutId = a.getResourceId(R.styleable.BottomNavigationBehavior_Params_tabLayoutId, View.NO_ID)
        a.recycle()
    }

    override fun onLayoutChild(parent: CoordinatorLayout, child: V, layoutDirection: Int): Boolean {
        val layoutChild: Boolean = super.onLayoutChild(parent, child, layoutDirection)
        if (mTabLayout == null && mTabLayoutId != View.NO_ID) {
            mTabLayout = findTabLayout(child!!)
            tabsHolder
        }
        return layoutChild
    }

    private fun findTabLayout(child: View): TabLayout? {
        return if (mTabLayoutId == 0) null else child.findViewById<View>(mTabLayoutId) as TabLayout
    }

    override fun onNestedVerticalOverScroll(coordinatorLayout: CoordinatorLayout?, child: V, @ScrollDirection direction: Int, currentOverScroll: Int, totalOverScroll: Int) {}
    override fun onDirectionNestedPreScroll(coordinatorLayout: CoordinatorLayout?, child: V, target: View?, dx: Int, dy: Int, consumed: IntArray?, @ScrollDirection scrollDirection: Int) {
        handleDirection(child, scrollDirection)
    }

    private fun handleDirection(child: V, scrollDirection: Int) {
        if (scrollDirection == ScrollDirection.Companion.SCROLL_DIRECTION_DOWN && hidden) {
            hidden = false
            animateOffset(child, 0)
        } else if (scrollDirection == ScrollDirection.Companion.SCROLL_DIRECTION_UP && !hidden) {
            hidden = true
            animateOffset(child, child!!.height)
        }
    }

    override fun onNestedDirectionFling(coordinatorLayout: CoordinatorLayout?, child: V, target: View?, velocityX: Float, velocityY: Float, @ScrollDirection scrollDirection: Int): Boolean {
        handleDirection(child, scrollDirection)
        return true
    }

    private fun animateOffset(child: V, offset: Int) {
        ensureOrCancelAnimator(child)
        mTranslationAnimator!!.translationY(offset.toFloat()).start()
        animateTabsHolder(offset)
    }

    private fun animateTabsHolder(offset: Int) {
        var offset = offset
        if (mTabsHolder != null) {
            offset = if (offset > 0) 0 else 1
            ViewCompat.animate(mTabsHolder!!).alpha(offset.toFloat()).setDuration(200).start()
        }
    }

    private fun ensureOrCancelAnimator(child: V) {
        if (mTranslationAnimator == null) {
            mTranslationAnimator = ViewCompat.animate(child!!)
            mTranslationAnimator!!.duration = 100
            mTranslationAnimator!!.interpolator = INTERPOLATOR
        } else {
            mTranslationAnimator!!.cancel()
        }
    }

    private val tabsHolder: Unit
        private get() {
            if (mTabLayout != null) {
                mTabsHolder = mTabLayout!!.getChildAt(0)
            }
        }

    fun setTabLayoutId(@IdRes tabId: Int) {
        mTabLayoutId = tabId
    }

    companion object {
        private val INTERPOLATOR: Interpolator = LinearOutSlowInInterpolator()
        fun <V : View?> from(view: V): BottomNavigationBehavior<V> {
            val params = view!!.layoutParams
            require(params is CoordinatorLayout.LayoutParams) { "The view is not a child of CoordinatorLayout" }
            val behavior = (params as CoordinatorLayout.LayoutParams)
                    .getBehavior()
            require(behavior is BottomNavigationBehavior<*>) { "The view is not associated with ottomNavigationBehavior" }
            return behavior as BottomNavigationBehavior<V>
        }
    }
}