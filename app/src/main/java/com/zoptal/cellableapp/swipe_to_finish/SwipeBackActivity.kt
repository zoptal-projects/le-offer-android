package com.zoptal.cellableapp.swipe_to_finish

import androidx.appcompat.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import com.zoptal.cellableapp.swipe_to_finish.SwipeBackLayout.*

/**
 * Created by Eric on 15/3/3.
 */
open class SwipeBackActivity : AppCompatActivity(), SwipeBackListener {
    var swipeBackLayout: SwipeBackLayout? = null
        private set
    private var ivShadow: ImageView? = null
   override fun setContentView(layoutResID: Int) {
        super.setContentView(container)
        val view = LayoutInflater.from(this).inflate(layoutResID, null)
        swipeBackLayout!!.addView(view)
    }

    //ivShadow.setBackgroundColor(ContextCompat.getColor(this,R.color.black_p50));
    private val container: View
        private get() {
            val container = RelativeLayout(this)
            swipeBackLayout = SwipeBackLayout(this)
            swipeBackLayout!!.setDragEdge(DEFAULT_DRAG_EDGE)
            swipeBackLayout!!.setOnSwipeBackListener(this)
            ivShadow = ImageView(this)
            //ivShadow.setBackgroundColor(ContextCompat.getColor(this,R.color.black_p50));
            val params = RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT)
            container.addView(ivShadow, params)
            container.addView(swipeBackLayout)
            return container
        }

    fun setEnableSwipe(enableSwipe: Boolean) {
        swipeBackLayout!!.setEnablePullToBack(enableSwipe)
    }

    fun setDragEdge(dragEdge: DragEdge) {
        swipeBackLayout!!.setDragEdge(dragEdge)
    }

    fun setDragDirectMode(dragDirectMode: DragDirectMode) {
        swipeBackLayout!!.setDragDirectMode(dragDirectMode)
    }

    override fun onViewPositionChanged(fractionAnchor: Float, fractionScreen: Float) {
        ivShadow!!.alpha = 1 - fractionScreen
    }

    companion object {
        private val DEFAULT_DRAG_EDGE = DragEdge.LEFT
    }
}