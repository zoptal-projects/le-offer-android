package com.zoptal.cellableapp.custom_fonts

import android.content.Context
import android.graphics.Typeface

import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatCheckBox

/**
 * <h>CheckBoxRobotoRegular</h>
 *
 *
 * In this class we used to make my custom CheckBox with roboto-regular font.
 *
 * @since 4/17/2017
 */
class CheckBoxRobotoRegular : AppCompatCheckBox {
    constructor(context: Context) : super(context) {
        if (FONT_NAME == null) FONT_NAME = Typeface.createFromAsset(context.assets, "fonts/roboto-regular.ttf")
        this.setTypeface(FONT_NAME)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        if (FONT_NAME == null) FONT_NAME = Typeface.createFromAsset(context.assets, "fonts/roboto-regular.ttf")
        this.setTypeface(FONT_NAME)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        if (FONT_NAME == null) FONT_NAME = Typeface.createFromAsset(context.assets, "fonts/roboto-regular.ttf")
        this.setTypeface(FONT_NAME)
    }

    companion object {
        private var FONT_NAME: Typeface? = null
    }
}