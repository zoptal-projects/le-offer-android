package com.zoptal.cellableapp.custom_fonts

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatTextView

/**
 * <h>TextViewRobotoRegular</h>
 *
 *
 * In this class we used to make my custom TextView with roboto-regular font.
 *
 * @since 3/29/2017
 */
class TextViewRobotoRegular : AppCompatTextView {
    constructor(context: Context) : super(context) {
        if (FONT_NAME == null) FONT_NAME = Typeface.createFromAsset(context.assets, "fonts/roboto-regular.ttf")
        this.typeface = FONT_NAME
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        if (FONT_NAME == null) FONT_NAME = Typeface.createFromAsset(context.assets, "fonts/roboto-regular.ttf")
        this.typeface = FONT_NAME
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        if (FONT_NAME == null) FONT_NAME = Typeface.createFromAsset(context.assets, "fonts/roboto-regular.ttf")
        this.typeface = FONT_NAME
    }

    companion object {
        private var FONT_NAME: Typeface? = null
    }
}