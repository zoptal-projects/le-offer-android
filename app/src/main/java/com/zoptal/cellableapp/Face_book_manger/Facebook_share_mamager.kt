package com.zoptal.cellableapp.Face_book_manger

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.AsyncTask
import android.os.Build
import android.util.Log
import androidx.core.content.FileProvider
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.share.ShareApi
import com.facebook.share.Sharer
import com.facebook.share.model.*
import com.zoptal.cellableapp.mqttchat.AppController
import java.io.*
import java.net.URL

/**
 * <h2>Facebook_share_mamager</h2>
 * <P>
 *
</P> *
 * @since 6/3/17.
 */
class Facebook_share_mamager private constructor() {
    private var share_callback: Share_callback? = null
    fun fb_share_image(image_local_path: String?, caption: String?, callback: Share_callback?) {
        val data = Image_data_holder()
        data.image_local_path = image_local_path
        data.caption = caption
        data.callback = callback
        Image_upload().execute(data)
    }

    /**
     * Creating the async task to upload the image in facebook. */
    private inner class Image_upload : AsyncTask<Image_data_holder?, Void?, Void?>() {

        protected override fun doInBackground(vararg params: Image_data_holder?): Void? {
            val item_data = params[0]
            share_Image(item_data!!.image_local_path, item_data.caption, item_data.callback)
            return null
        }
    }

    private fun share_Image(image_local_path: String?, caption: String?, callback: Share_callback?) {
        share_callback = callback
        val photo = SharePhoto.Builder()
                .setBitmap(BitmapFactory.decodeFile(image_local_path))
                .setCaption(caption)
                .build()
        val content = SharePhotoContent.Builder()
                .addPhoto(photo)
                .build()
        /*
          * Finally sharing the data.*/share_Data(content)
    }

    fun fb_share_video(local_video_path: String?, thumbnail_url: String?, caption: String?, callback: Share_callback?) {
        val data = Video_data_holder()
        data.local_video_path = local_video_path
        data.thumbnail_url = thumbnail_url
        data.caption = caption
        data.callback = callback
        Video_upload().execute(data)
    }

    /**
     * Creating the async task to upload the image in facebook. */
    private inner class Video_upload : AsyncTask<Video_data_holder?, Void?, Void?>() {
        protected override fun doInBackground(vararg params: Video_data_holder?): Void? {
            val item_data = params[0]
            share_Video(item_data!!.local_video_path, item_data.caption, item_data.callback)
            return null
        }
    }

    private fun share_Video(local_video_path: String?, caption: String?, callback: Share_callback?) {
        share_callback = callback
        val video = ShareVideo.Builder()
                .setLocalUrl(getUri_Path(File(local_video_path)))
                .build()
        val content = ShareVideoContent.Builder()
                .setVideo(video)
                .setContentTitle(caption)
                .build()
        /*
         * Finally sharing the data.*/share_Data(content)
    }

    fun shareImage_Link(link: String?, image_url: String?, caption: String?, callback: Share_callback?) {
        share_callback = callback
        val data_holder = Data_holder()
        data_holder.caption = caption
        data_holder.image_path = image_url
        data_holder.link = link
        DownloadImgTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, data_holder)
    }

    private inner class DownloadImgTask : AsyncTask<Data_holder?, Void?, Bitmap?>() {
        var holder: Data_holder? = null
        protected override fun doInBackground(vararg data: Data_holder?): Bitmap? {
            holder = data[0]
            val urldisplay = holder!!.image_path
            var bm: Bitmap? = null
            try {
                val `in` = URL(urldisplay).openStream()
                bm = BitmapFactory.decodeStream(`in`)
            } catch (e: Exception) {
                Log.e("Error", e.message)
                e.printStackTrace()
            }
            return bm
        }

        override fun onPostExecute(result: Bitmap?) {
            post_In_Fb(result, holder!!.caption, holder!!.link)
        }
    }

    /*
     *Sharing the bitmap */
    private fun post_In_Fb(result: Bitmap?, caption: String?, link: String?) {
        Log.d("dgta2", "" + link)
        val photo = SharePhoto.Builder()
                .setBitmap(result)
                .setCaption(caption)
                .setImageUrl(Uri.parse(link))
                .build()
        val content = SharePhotoContent.Builder()
                .addPhoto(photo)
                .setContentUrl(Uri.parse(link))
                .build()
        /*
         * Finally -sharing the data.*/share_Data(content)
    }

    private fun postData(link: String, imageUrl: String, title: String, decryption: String) {
        val `object` = ShareOpenGraphObject.Builder()
                .putString("og:type", "enguru_app:badge")
                .putString("og:title", title)
                .putString("og:url", link)
                .putString("og:image", imageUrl)
                .putString("og:description", decryption).build()
        // Create an action
        val action = ShareOpenGraphAction.Builder()
                .setActionType("enguru_app:unlocked")
                .putObject("badge", `object`).build()
        // Create the content
        val content = ShareOpenGraphContent.Builder()
                .setPreviewPropertyName("badge").setAction(action)
                .build()
    }

    /*
     *Downloading the video and sharing the video to facebook */
    fun shareVideo_link(video_url: String?, thumbnail_url: String?, caption: String?, callback: Share_callback?) {
        share_callback = callback
        val data_holder = Data_holder()
        data_holder.caption = caption
        data_holder.image_path = video_url
        DownloadVidTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, data_holder)
    }

    /*
     *Downloading the video file */
    private inner class DownloadVidTask : AsyncTask<Data_holder?, Void?, String>() {
        var holder: Data_holder? = null
        private var isError = false
        private var caption: String? = ""
        protected override fun doInBackground(vararg param: Data_holder?): String {
            isError = false
            var inputStream: InputStream? = null
            var outputStream: OutputStream? = null
            var file: File? = null
            holder = param[0]
            val url = holder!!.image_path
            caption = holder!!.caption
            var details = ""
            try {
                file = getFile("1")
                val url_path = URL(url)
                val connection = url_path.openConnection()
                connection.connect()
                connection.contentType
                inputStream = BufferedInputStream(url_path.openStream())
                outputStream = FileOutputStream(file)
                val data = ByteArray(1024)
                var count: Int
                while (inputStream.read(data).also { count = it } != -1) {
                    outputStream.write(data, 0, count)
                }
            } catch (e: Exception) {
                e.printStackTrace()
                isError = true
                details = "Error on loading file."
            } finally {
                try {
                    assert(outputStream != null)
                    outputStream!!.flush()
                    outputStream.close()
                } catch (e: Exception) {
                    e.printStackTrace()
                    isError = true
                    details = "Error on loading file."
                }
                try {
                    inputStream!!.close()
                } catch (e: Exception) {
                    e.printStackTrace()
                    isError = true
                    details = "Error on loading file."
                }
            }
            if (file!!.length() > 12582912) {
                isError = true
                details = "File size is more then 12MB."
            }
            return if (isError) details else file.path
        }

        override fun onPostExecute(result: String) {
            if (isError) {
                if (share_callback != null) share_callback!!.onError(result)
            } else {
                share_Video(result, caption, share_callback)
            }
        }
    }

    /*
     *Finally posting the data to the facebook. */
    private fun share_Data(shareContent: ShareContent<*, *>) {
        ShareApi(shareContent)
                .share(object : FacebookCallback<Sharer.Result?> {
                    override fun onSuccess(result: Sharer.Result?) {
                        if (share_callback != null) {
                            share_callback!!.onSucess_share()
                        }
                    }

                    override fun onCancel() {
                        if (share_callback != null) {
                            share_callback!!.onError("User canceled.")
                        }
                    }

                    override fun onError(error: FacebookException) {
                        if (share_callback != null) {
                            share_callback!!.onError("" + error)
                        }
                    }
                })
    }

    private inner class Data_holder {
        var image_path: String? = null
        var caption: String? = null
        var link: String? = null
    }

    private inner class Image_data_holder {
        var image_local_path: String? = null
        var caption: String? = null
        var callback: Share_callback? = null
    }

    private inner class Video_data_holder {
        var local_video_path: String? = null
        var thumbnail_url: String? = null
        var caption: String? = null
        var callback: Share_callback? = null
    }

    interface Share_callback {
        fun onSucess_share()
        fun onError(error: String?)
    }

    /*
     * Get the file of the required type ie. Image or video.*/
    @Throws(IOException::class)
    private fun getFile(isVideo: String): File {
        val file: File
        val time = System.currentTimeMillis()
        val temp_video = "temp$time"
        val suffix: String
        suffix = if (isVideo.equals("1", ignoreCase = true)) {
            ".mp4"
        } else {
            ".jpeg"
        }
        file = File.createTempFile(temp_video, suffix, AppController.instance?.getCacheDir())
        return file
    }

    fun shareLinkOnFacebook(link: String?, thumbnail: String?, name: String?, description: String?, callback: Share_callback?) {
        val linkShareDataHolder = LinkShareDataHolder()
        linkShareDataHolder.link = link
        linkShareDataHolder.thumbnail = thumbnail
        linkShareDataHolder.name = name
        linkShareDataHolder.description = description
        linkShareDataHolder.callback = callback
        ShareLink().execute(linkShareDataHolder)
    }

    private inner class ShareLink : AsyncTask<LinkShareDataHolder?, Void?, Void?>() {
        protected override fun doInBackground(vararg params: LinkShareDataHolder?): Void? {
            val linkShareDataHolder = params[0]
            share_Link(linkShareDataHolder!!.link, linkShareDataHolder.thumbnail, linkShareDataHolder.name, linkShareDataHolder.description, linkShareDataHolder.callback)
            return null
        }
    }

    private inner class LinkShareDataHolder {
        var link: String? = null
        var thumbnail: String? = null
        var name: String? = null
        var description: String? = null
        var callback: Share_callback? = null
    }

    private fun share_Link(link: String?, thumbnail: String?, name: String?, description: String?, callback: Share_callback?) {
        share_callback = callback
        val content = ShareLinkContent.Builder()
                .setContentUrl(Uri.parse(link))
                .setContentTitle(name)
                .setContentDescription(description)
                .build()
        /*
         * Finally sharing the data.*/share_Data(content)
    }

    companion object {
        private var FACEOOK_MANAGER: Facebook_share_mamager? = null
        @JvmStatic
        val instance: Facebook_share_mamager?
            get() {
                if (FACEOOK_MANAGER == null) {
                    FACEOOK_MANAGER = Facebook_share_mamager()
                }
                return FACEOOK_MANAGER
            }

        /**
         * <h2>getUri_Path</h2>
         * <P>
         *
        </P> */
        fun getUri_Path(file: File?): Uri {
            val context: Context = AppController.instance!!
            val uri: Uri
            /*
         * Checking if the build version is greater then 25 then no need ask for runtime permission.*/uri = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                FileProvider.getUriForFile(context, context.applicationContext.packageName + ".provider", file!!)
            } else {
                Uri.fromFile(file)
            }
            return uri
        }
    }
}