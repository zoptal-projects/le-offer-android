package com.zoptal.cellableapp.Face_book_manger

import android.annotation.SuppressLint
import android.app.Activity
import android.util.Log
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

/**
 * <h>Facebook_login</h>
 * <P>
 * Class contain a async task to get the data from facebook .
 * Contains a method to do facebook login .
 * Here doing facebook login and taking data as user_friends .
</P> *
 * @author 3Embed
 * @since  4/02/2016
 */
class Facebook_login(activity: Activity) {
    private val mactivity: Activity
    private var isReady = false
    private val facebook_session_manager: Facebook_session_manager
    private var callback: Facebook_callback? = null

    /**
     * <h2>faceBook_Login</h2>
     * <P>
     * Facebook login data from user.
    </P> */
    fun faceBook_Login(callbackmanager: CallbackManager?, facebook_callback: Facebook_callback?) {
        callback = facebook_callback
        Refresh_Token()
        LoginManager.getInstance().logInWithReadPermissions(mactivity, listOf("email"))
        LoginManager.getInstance().registerCallback(callbackmanager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(loginResult: LoginResult) {
                /*
                 * Storing all the data in session manger .*/
                val accessToken = loginResult.accessToken
                store_Acess_token_details(accessToken)
                /*
                 * Giving the call back for success logged in.*/if (callback != null) callback!!.success(facebook_session_manager.userId)
            }

            override fun onCancel() {
                if (callback != null) callback!!.cancel("User canceled !")
            }

            override fun onError(error: FacebookException) {
                if (callback != null) callback!!.error("Got some error!")
            }
        })
    }

    /**
     * <h2>get_FB_Friends</h2>
     * <P>
     * Fb login status.
    </P> *
     * @param callbackmanager call back managers.
     * @param facebook_callback user interface manager.
     */
    fun get_FB_Friends(callbackmanager: CallbackManager, facebook_callback: Facebook_callback?) {
        callback = facebook_callback
        if (isLoggedIn) {
            fb_ReadPermission(callbackmanager, arrayOf("user_friends"))
        } else {
            first_d_login(callbackmanager, arrayOf("user_friends"), true)
        }
    }

    /**
     * <h2>fbLogin_status_for_permission</h2>
     * <P>
     * Fb login status.
    </P> *
     * @param callbackmanager call back managers.
     * @param facebook_callback user interface manager.
     * @param permission_array checking for the  permission result.
     */
    fun ask_PublishPermission(callbackmanager: CallbackManager, permission_array: Array<String>, facebook_callback: Facebook_callback?) {
        callback = facebook_callback
        if (isLoggedIn) {
            fb_PublishPermission(callbackmanager, permission_array)
        } else {
            first_d_login(callbackmanager, permission_array, false)
        }
    }

    /**
     * <h2>first_d_login</h2>
     * <P>
     * Fb login status.
    </P> *
     * @param callbackmanager call back managers.
     * @param permission_array checking for the  permission result.
     */
    private fun first_d_login(callbackmanager: CallbackManager, permission_array: Array<String>, isRedPermission: Boolean) {
        Refresh_Token()
        LoginManager.getInstance().logInWithReadPermissions(mactivity, listOf("email"))
        LoginManager.getInstance().registerCallback(callbackmanager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(loginResult: LoginResult) {
                val accessToken = loginResult.accessToken
                store_Acess_token_details(accessToken)
                if (isRedPermission) {
                    fb_ReadPermission(callbackmanager, permission_array)
                } else {
                    fb_PublishPermission(callbackmanager, permission_array)
                }
            }

            override fun onCancel() {
                if (callback != null) callback!!.error("User canceled!")
            }

            override fun onError(error: FacebookException) {
                Log.d("user2", "Canceled data details")
                if (callback != null) callback!!.error("Got some error!")
            }
        })
    }

    /**
     * <h2>fb_PublishPermission</h2>
     * <P>
     *
    </P> */
    private fun fb_ReadPermission(callbackmanager: CallbackManager, permission_array: Array<String>) {
        if (verify_Permission(Arrays.asList(*permission_array)).size == 0) {
            if (callback != null) callback!!.success(facebook_session_manager.userId)
            return
        }
        LoginManager.getInstance().logInWithReadPermissions(mactivity, Arrays.asList(*permission_array))
        LoginManager.getInstance().registerCallback(callbackmanager, object : FacebookCallback<LoginResult?> {
            override fun onSuccess(loginResult: LoginResult?) {
                /*
                 * Giving the call back for sucess logged in.*/
                if (callback != null) callback!!.success(facebook_session_manager.userId)
            }

            override fun onCancel() {
                if (callback != null) callback!!.cancel("User canceled !")
            }

            override fun onError(error: FacebookException) {
                if (callback != null) callback!!.error("Got some error!")
            }
        })
    }

    /**
     * <h2>fb_PublishPermission</h2>
     * <P>
     *
    </P> */
    private fun fb_PublishPermission(callbackmanager: CallbackManager, permission_array: Array<String>) {
        if (verify_Permission(Arrays.asList(*permission_array)).size == 0) {
            if (callback != null) callback!!.success(facebook_session_manager.userId)
            return
        }
        LoginManager.getInstance().logInWithPublishPermissions(mactivity, Arrays.asList(*permission_array))
        LoginManager.getInstance().registerCallback(callbackmanager, object : FacebookCallback<LoginResult?> {
            override fun onSuccess(loginResult: LoginResult?) {
                /*
                 * Giving the call back for sucess logged in.*/
                if (callback != null) callback!!.success(facebook_session_manager.userId)
            }

            override fun onCancel() {
                if (callback != null) callback!!.cancel("User canceled !")
            }

            override fun onError(error: FacebookException) {
                if (callback != null) callback!!.error("Got some error!")
            }
        })
    }

    /**
     * <h2>verify_Permission</h2>
     * <P>
     * Checking the permission of the data.
    </P> *
     * @param permissions contains the token error.
     */
    private fun verify_Permission(permissions: List<String>): ArrayList<String> {
        val temp = ArrayList<String>()
        val granted_list = AccessToken.getCurrentAccessToken().permissions
        for (check_test in permissions) {
            var isFound = false
            for (permissioon in granted_list) {
                if (check_test == permissioon) {
                    isFound = true
                    break
                }
            }
            if (!isFound) {
                temp.add(check_test)
            }
        }
        return temp
    }

    /*
    * Storing the facebook token and other details.*/
    private fun store_Acess_token_details(accessToken: AccessToken) {
        facebook_session_manager.facebookToken = accessToken.token
        facebook_session_manager.acessToken = accessToken.token
        facebook_session_manager.applicationId = accessToken.applicationId
        facebook_session_manager.userId = accessToken.userId
        facebook_session_manager.permission = accessToken.permissions
        facebook_session_manager.declinePermission = accessToken.declinedPermissions
        when (accessToken.source) {
            AccessTokenSource.NONE -> {
                facebook_session_manager.acessTokenSource = Facebook_common_constance.None
            }
            AccessTokenSource.FACEBOOK_APPLICATION_WEB -> {
                facebook_session_manager.acessTokenSource = Facebook_common_constance.Facebook_app_web
            }
            AccessTokenSource.FACEBOOK_APPLICATION_NATIVE -> {
                facebook_session_manager.acessTokenSource = Facebook_common_constance.Facebook_app_native
            }
            AccessTokenSource.FACEBOOK_APPLICATION_SERVICE -> {
                facebook_session_manager.acessTokenSource = Facebook_common_constance.Facebook_app_server
            }
            AccessTokenSource.WEB_VIEW -> {
                facebook_session_manager.acessTokenSource = Facebook_common_constance.web_view
            }
            AccessTokenSource.TEST_USER -> {
                facebook_session_manager.acessTokenSource = Facebook_common_constance.test_user
            }
            AccessTokenSource.CLIENT_TOKEN -> {
                facebook_session_manager.acessTokenSource = Facebook_common_constance.client_token
            }
        }
        @SuppressLint("SimpleDateFormat") val formatter: DateFormat = SimpleDateFormat("MM/dd/yyyy HH:mm:ss")
        facebook_session_manager.expTime = formatter.format(accessToken.expires)
        facebook_session_manager.lastRefTime = formatter.format(accessToken.lastRefresh)
    }

    /**
     * <h>Facebook_callback</h>
     * <P>
     * Calback interface of facebook.
    </P> *
     */
    interface Facebook_callback {
        fun success(id: String?)
        fun error(error: String?)
        fun cancel(cancel: String?)
    }

    /**
     * <h>Refresh_Token</h>
     * <P>
     * Calback interface of facebook.
    </P> *
     */
    private fun Refresh_Token() {
        if (isReady) {
            LoginManager.getInstance().logOut()
            /*
             * Refreshing the acess token of the Facebook*/AccessToken.refreshCurrentAccessTokenAsync()
        }
    }

    val isLoggedIn: Boolean
        get() {
            val accessToken = AccessToken.getCurrentAccessToken()
            return accessToken != null
        }

    init {
        FacebookSdk.sdkInitialize(activity.applicationContext) { isReady = true }
        mactivity = activity
        facebook_session_manager = Facebook_session_manager(mactivity)
    }
}