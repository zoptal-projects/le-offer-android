package com.zoptal.cellableapp.Face_book_manger

import android.content.Context
import android.content.SharedPreferences

/**
 * <h1>Facebook_session_manager</h1>
 * <P>
 * Managing the app facebook login.
</P> *
 * @since  18/11/16.
 */
class Facebook_session_manager(context: Context) {
    private val sharedPreferences: SharedPreferences
    fun clear_session() {
        sharedPreferences.edit().clear().apply()
    }

    var acessToken: String?
        get() = sharedPreferences.getString(ACESS_TOKEN, "")
        set(acess_token) {
            sharedPreferences.edit().putString(ACESS_TOKEN, acess_token).apply()
        }

    var facebookToken: String?
        get() = sharedPreferences.getString(FACEBOOK_TOKEN, "")
        set(token) {
            sharedPreferences.edit().putString(FACEBOOK_TOKEN, token).apply()
        }

    var applicationId: String?
        get() = sharedPreferences.getString(APPLICATION_ID, "")
        set(applicationId) {
            sharedPreferences.edit().putString(APPLICATION_ID, applicationId).apply()
        }

    var userId: String?
        get() = sharedPreferences.getString(USER_ID, "")
        set(user_id) {
            sharedPreferences.edit().putString(USER_ID, user_id).apply()
        }

    var permission: Set<String>?
        get() = sharedPreferences.getStringSet(PERMISSION, null)
        set(permission_list) {
            sharedPreferences.edit().putStringSet(PERMISSION, permission_list).apply()
        }

    var declinePermission: Set<String>?
        get() = sharedPreferences.getStringSet(DECLINE_PERMISSION, null)
        set(dec_permission_list) {
            sharedPreferences.edit().putStringSet(DECLINE_PERMISSION, dec_permission_list).apply()
        }

    var acessTokenSource: String?
        get() = sharedPreferences.getString(ACESS_TOKEN_SOURCE, "")
        set(acess_token_tag) {
            sharedPreferences.edit().putString(ACESS_TOKEN_SOURCE, acess_token_tag).apply()
        }

    var expTime: String?
        get() = sharedPreferences.getString(EXP_TIME, "")
        set(time_data) {
            sharedPreferences.edit().putString(EXP_TIME, time_data).apply()
        }

    var lastRefTime: String?
        get() = sharedPreferences.getString(LAST_REF_TIME, "")
        set(last_time) {
            sharedPreferences.edit().putString(LAST_REF_TIME, last_time).apply()
        }

    companion object {
        private const val PREF_NAME = "Facebook_data"
        private const val ACESS_TOKEN = "accessToken"
        private const val APPLICATION_ID = "applicationId"
        private const val USER_ID = "userId"
        private const val PERMISSION = "permissions"
        private const val DECLINE_PERMISSION = "declinedPermissions"
        private const val ACESS_TOKEN_SOURCE = "accessTokenSource"
        private const val EXP_TIME = "expirationTime"
        private const val LAST_REF_TIME = "lastRefreshTime"
        private const val FACEBOOK_TOKEN = "facebook_token"
    }

    init {
        sharedPreferences = context.getSharedPreferences(PREF_NAME, 0)
    }
}