package com.zoptal.cellableapp.Face_book_manger

/**
 * @since 18/11/16.
 */
object Facebook_common_constance {
    const val None = "NONE"
    const val Facebook_app_web = "FACEBOOK_APPLICATION_WEB"
    const val Facebook_app_native = "FACEBOOK_APPLICATION_NATIVE"
    const val Facebook_app_server = "FACEBOOK_APPLICATION_SERVICE"
    const val web_view = "WEB_VIEW"
    const val test_user = "TEST_USER"
    const val client_token = "CLIENT_TOKEN"
}