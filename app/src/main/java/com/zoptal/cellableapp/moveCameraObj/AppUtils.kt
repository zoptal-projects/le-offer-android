package com.zoptal.cellableapp.moveCameraObj

import android.content.Context
import android.os.Build
import android.provider.Settings
import android.text.TextUtils

/**
 * <h>AppUtils</h>
 *
 *
 * In this class we used to decalare the all constant variables which being used in moving map.
 *
 */
object AppUtils {
    fun isLocationEnabled(context: Context): Boolean {
        var locationMode = 0
        val locationProviders: String
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locationMode = Settings.Secure.getInt(context.contentResolver, Settings.Secure.LOCATION_MODE)
            } catch (e: Settings.SettingNotFoundException) {
                e.printStackTrace()
            }
            locationMode != Settings.Secure.LOCATION_MODE_OFF
        } else {
            locationProviders = Settings.Secure.getString(context.contentResolver, Settings.Secure.LOCATION_PROVIDERS_ALLOWED)
            !TextUtils.isEmpty(locationProviders)
        }
    }

    object LocationConstants {
        const val SUCCESS_RESULT = 0
        const val FAILURE_RESULT = 1
        const val package_NAME = "com.yelo.com"
        const val RECEIVER = "$package_NAME.RECEIVER"
        const val RESULT_DATA_KEY = "$package_NAME.RESULT_DATA_KEY"
        const val LOCATION_DATA_EXTRA = "$package_NAME.LOCATION_DATA_EXTRA"
        const val LOCATION_DATA_AREA = "$package_NAME.LOCATION_DATA_AREA"
        const val LOCATION_DATA_CITY = "$package_NAME.LOCATION_DATA_CITY"
        const val LOCATION_DATA_STREET = "$package_NAME.LOCATION_DATA_STREET"
    }
}