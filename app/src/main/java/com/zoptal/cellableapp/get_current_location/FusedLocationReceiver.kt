package com.zoptal.cellableapp.get_current_location

/**
 * <h>FusedLocationReceiver</h>
 *
 *
 * We used this interface for callback once we receive the current lat,lng
 *
 * @since 25/7/16
 */
interface FusedLocationReceiver {
    fun onUpdateLocation()
}