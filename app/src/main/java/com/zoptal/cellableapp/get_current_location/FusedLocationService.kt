package com.zoptal.cellableapp.get_current_location

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.util.Log
import androidx.core.app.ActivityCompat
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.*
import com.zoptal.cellableapp.utility.ProgressBarHandler
import com.zoptal.cellableapp.utility.VariableConstants

/**
 * <h>FusedLocationService</h>
 *
 *
 * In This class we used to do Google fused Location api call to
 * receive the current latitude and longitude.
 *
 * @since 25/7/16
 */
class FusedLocationService(private val activity: Activity, private val locationReceiver: FusedLocationReceiver) : GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {
    private var mGoogleApiClient: GoogleApiClient? = null
    private val mProgressDialog: ProgressBarHandler?
    private var getLocation: Location? = null
    private val fusedLocationProviderApi = LocationServices.FusedLocationApi
    private var onlocationChanged = false

    ////////// Overriden methods of ConnectionCallbacks ///////////////
    override fun onConnected(bundle: Bundle?) {
        println("Callback=" + "onConnected")
        val mLocationRequest: LocationRequest
        mLocationRequest = LocationRequest.create()
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.interval = 100 // Update location every second
        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this)
        val mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient)
        if (mLastLocation != null) {
            println(TAG + " " + "location onConnected=" + mLastLocation.latitude + " " + mLastLocation.longitude)
            getLocation = mLastLocation
        }
    }

    override fun onConnectionSuspended(i: Int) {}

    /////////////////// Overriden method of OnConnectionFailedListener /////////
    override fun onConnectionFailed(connectionResult: ConnectionResult) {
        buildGoogleApiClient()
    }

    ////////////// Overriden method of LocationListener ///////////////////////
    override fun onLocationChanged(location: Location) {
        if (!onlocationChanged) {
            activity.runOnUiThread { mProgressDialog?.hide() }
            Log.d(TAG, "onLocationChanged: refresh repeataly ")
            if (location != null) {
                getLocation = location
                locationReceiver.onUpdateLocation()
            }
            if (getLocation!!.accuracy < MINIMUM_ACCURACY) fusedLocationProviderApi.removeLocationUpdates(mGoogleApiClient, this)
            onlocationChanged = true
        }
    }

    @Synchronized
    private fun buildGoogleApiClient() {
        mGoogleApiClient = GoogleApiClient.Builder(activity)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build()
    }

    private fun settingRequest() {
        val locationRequest: LocationRequest
        locationRequest = LocationRequest.create()
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest.interval = 30 * 1000.toLong()
        locationRequest.fastestInterval = 5 * 1000.toLong()
        val builder = LocationSettingsRequest.Builder().addLocationRequest(locationRequest)

        // **************************
        builder.setAlwaysShow(true) // this is the key ingredient
        // **************************
        val result = LocationServices.SettingsApi
                .checkLocationSettings(mGoogleApiClient, builder.build())
        result.setResultCallback { result ->
            val status = result.status
            when (status.statusCode) {
                LocationSettingsStatusCodes.SUCCESS ->
                    // Log.i(TAG,"LocationSettingsStatusCodes SUCCESS");
                    println("Pending result=" + "SUCCESS")
                LocationSettingsStatusCodes.RESOLUTION_REQUIRED ->                         // Location settings are not satisfied. But could be
                    // fixed by showing the user
                    // a dialog.
                    try {
                        // Log.i(TAG,"LocationSettingsStatusCodes RESOLUTION_REQUIRED");
                        println("Pending result=" + "RESOLUTION_REQUIRED")

                        // Show the dialog by calling
                        // startResolutionForResult(),
                        // and check the result in onActivityResult().
                        status.startResolutionForResult(activity, VariableConstants.REQUEST_CHECK_SETTINGS)
                    } catch (e: IntentSender.SendIntentException) {
                        // Ignore the error.
                    }
                LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> println("Pending result=" + "SETTINGS_CHANGE_UNAVAILABLE")
            }
        }
    }

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (data != null) {
            when (requestCode) {
                VariableConstants.REQUEST_CHECK_SETTINGS -> when (resultCode) {
                    Activity.RESULT_OK -> {
                        println("result ok..")
                        mProgressDialog!!.show()
                    }
                    Activity.RESULT_CANCELED -> {
                        println("result cancel..")
                        activity.finish()
                    }
                }
            }
        }
    }

    ////////////// Get location ///////////////////
    fun receiveLocation(): Location? {
        return getLocation
    }

    companion object {
        private val TAG = FusedLocationService::class.java.simpleName
        private const val MINIMUM_ACCURACY = 50.0f
    }

    //private TextView txtOutputLat,txtOutputLon;
    init {
        mProgressDialog = ProgressBarHandler(activity)
        buildGoogleApiClient()
        if (mGoogleApiClient != null && !mGoogleApiClient!!.isConnected) mGoogleApiClient!!.connect()
        settingRequest()
    }
}