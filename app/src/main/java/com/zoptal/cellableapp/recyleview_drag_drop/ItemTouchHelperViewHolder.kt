package com.zoptal.cellableapp.recyleview_drag_drop

/**
 * Created by embed on 25/6/18.
 */
interface ItemTouchHelperViewHolder {
    /**
     * Implementations should update the item view to indicate it's active state.
     */
    fun onItemSelected()

    /**
     * state should be cleared.
     */
    fun onItemClear()
}