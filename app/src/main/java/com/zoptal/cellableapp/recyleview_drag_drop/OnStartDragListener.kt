package com.zoptal.cellableapp.recyleview_drag_drop

import androidx.recyclerview.widget.RecyclerView

/**
 * Created by embed on 25/6/18.
 */
interface OnStartDragListener {
    /**
     * Called when a view is requesting a start of a drag.
     *
     * @param viewHolder The holder of the view to drag.
     */
    fun onStartDrag(viewHolder: RecyclerView.ViewHolder?)
}