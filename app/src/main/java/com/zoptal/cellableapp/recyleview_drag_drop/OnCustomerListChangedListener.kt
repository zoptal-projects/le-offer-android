package com.zoptal.cellableapp.recyleview_drag_drop

import com.zoptal.cellableapp.Uploader.ProductImageDatas
import com.zoptal.cellableapp.utility.CapturedImage

/**
 * Created by embed on 25/6/18.
 */
interface OnCustomerListChangedListener {
    // use while post product
    fun onNoteListChanged(capturedImageList: List<CapturedImage?>?)

    // use while update product
    fun onUpdateImagesListChanged(productImageDatasList: List<ProductImageDatas?>?)
}