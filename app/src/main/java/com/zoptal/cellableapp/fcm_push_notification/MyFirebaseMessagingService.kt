package com.zoptal.cellableapp.fcm_push_notification

import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.text.TextUtils
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson
import com.zoptal.cellableapp.R
import com.zoptal.cellableapp.main.activity.HomePageActivity
import com.zoptal.cellableapp.main.activity.NotificationActivity
import com.zoptal.cellableapp.pojo_class.fcm_notification_pojo.FcmNotificationMain
import com.zoptal.cellableapp.utility.SessionManager
import org.json.JSONObject

/**
 * <h>MyFirebaseMessagingService</h>
 *
 *
 * In this class we used to receive the Fcm Push notification in onMessageReceived().
 *
 */
class MyFirebaseMessagingService : FirebaseMessagingService() {
    private var notificationUtils: NotificationUtils? = null
    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        Log.e(TAG, "From: " + remoteMessage.from)

        // Check if message contains a notification payload.
        if (remoteMessage.notification != null) {
            Log.e(TAG, "Notification Body: " + remoteMessage.notification!!.body)
            //handleNotification(remoteMessage.getNotification().getBody());
            //sendNotification(remoteMessage.getNotification().getBody());
            val resultIntent = Intent(applicationContext, NotificationActivity::class.java) //MainActivity
            resultIntent.putExtra("message", "message")
            resultIntent.putExtra("isFromNotification", true)
            tempShowNotificationMessage(applicationContext, resources.getString(R.string.app_name), remoteMessage.notification!!.body, "", resultIntent)

            // app is in foreground, broadcast the push message
            val pushNotification = Intent(Config.PUSH_NOTIFICATION)
            pushNotification.putExtra("message", remoteMessage.notification!!.body)
            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification)
        }

        // Check if message contains a data payload.
        if (remoteMessage.data.size > 0) {
            Log.e(TAG, "Data Payload: " + remoteMessage.data.toString())
            try {
                val json = JSONObject(remoteMessage.data.toString())
                handleDataMessage(json)
            } catch (e: Exception) {
                Log.e(TAG, "Exception: " + e.message)
            }
        }
    }

    private fun handleNotification(message: String) {
        println("$TAG firebase message=$message")
        if (!NotificationUtils.Companion.isAppIsInBackground(applicationContext)) {
            // app is in foreground, broadcast the push message
            val pushNotification = Intent(Config.PUSH_NOTIFICATION)
            pushNotification.putExtra("message", message)
            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification)

            // play notification sound
            val notificationUtils = NotificationUtils(applicationContext)
            notificationUtils.playNotificationSound()
        } else {
            println("$TAG push in background")
            SessionManager(this).isBackgroundNotification = true
            // If the app is in background, firebase itself handles the notification
        }
    }

    private fun sendNotification(messageBody: String) {
        val intent = Intent(this, NotificationActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        intent.putExtra("isFromNotification", true)
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(resources.getString(R.string.app_name))
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)
        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.notify(0, notificationBuilder.build())
    }

    private fun handleDataMessage(json: JSONObject) {
        println("$TAG firebase json=$json")
        Log.e(TAG, "push json: $json")

        // send the complete json message to HomePageActivity through broadcast receiver
        if (json.toString() != null && !json.toString().isEmpty()) {
            val pushNotification = Intent(Config.PUSH_NOTIFICATION)
            pushNotification.putExtra("jsonMessage", json.toString())
            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification)
        }
        try {
            val gson = Gson()
            val fcmNotificationMain = gson.fromJson(json.toString(), FcmNotificationMain::class.java)
            val campaignId: String
            val imageUrl: String
            val title: String
            val message: String
            val type: String
            val userId: String
            val username: String
            val timestamp = ""
            campaignId = fcmNotificationMain.body!!.campaignId
            imageUrl = fcmNotificationMain.body!!.imageUrl
            title = fcmNotificationMain.body!!.title
            message = fcmNotificationMain.body!!.message
            type = fcmNotificationMain.body!!.type
            userId = fcmNotificationMain.body!!.userId
            username = fcmNotificationMain.body!!.username
            println("$TAG campaignId=$campaignId imageUrl=$imageUrl title=$title message=$message type=$type userId=$userId username=$username")
            if (!NotificationUtils.Companion.isAppIsInBackground(applicationContext)) {
                // play notification sound
                val notificationUtils = NotificationUtils(applicationContext)
                notificationUtils.playNotificationSound()
            } else {
                // app is in background, show the notification in notification tray
                val resultIntent = Intent(applicationContext, HomePageActivity::class.java)
                resultIntent.putExtra("message", message)

                // check for image attachment
                if (TextUtils.isEmpty(imageUrl)) {
                    tempShowNotificationMessage(applicationContext, title, message, timestamp, resultIntent)
                } else {
                    // image is present, show notification with image
                    showNotificationMessageWithBigImage(applicationContext, title, message, timestamp, resultIntent, imageUrl)
                }
            }
        } catch (e: Exception) {
            Log.e(TAG, "Exception: " + e.message)
        }
    }

    /**
     * Showing notification with text only
     */
    private fun showNotificationMessage(context: Context, title: String, message: String, timeStamp: String, intent: Intent) {
        notificationUtils = NotificationUtils(context)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        notificationUtils!!.showNotificationMessage(title, message, timeStamp, intent)
    }

    /**
     * Showing notification with text and image
     */
    private fun showNotificationMessageWithBigImage(context: Context, title: String, message: String, timeStamp: String, intent: Intent, imageUrl: String) {
        notificationUtils = NotificationUtils(context)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        notificationUtils!!.showNotificationMessage(title, message, timeStamp, intent, imageUrl)
    }

    /**
     * Showing notification with text only
     */
    private fun tempShowNotificationMessage(context: Context, title: String, message: String?, timeStamp: String, intent: Intent) {
        val notificationUtils = TempNotificationUtils(context)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent)
    }

    companion object {
        private val TAG = MyFirebaseMessagingService::class.java.simpleName
    }
}