package com.zoptal.cellableapp.fcm_push_notification

import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.google.firebase.messaging.FirebaseMessaging
import com.google.gson.Gson
import com.zoptal.cellableapp.pojo_class.fcm_notification_pojo.FcmNotificationMain
import com.zoptal.cellableapp.utility.DialogBox
import com.zoptal.cellableapp.utility.SessionManager

/**
 * <h>NotificationMessageDialog</h>
 *
 *
 * In this class We used to initialize broadcast variable. Once we get the value
 * which has been broadcasted from Notification screen. Then we used to open a dialog
 * to show local campaign message.
 *
 * @since 25-Aug-17
 */
class NotificationMessageDialog(mActivity: Activity?) {
    @JvmField
    var mRegistrationBroadcastReceiver: BroadcastReceiver? = null
    private val dialogBox: DialogBox
    private val mSessionManager: SessionManager

    /**
     * <h>InitFcmBroadCastVar</h>
     *
     *
     * In this method we used to initialize Broadcast receiver value. Once we get the
     * notification then check whether it is json response then show the local campaign
     * dialog.
     *
     */
    private fun initFcmBroadCastVar() {
        mRegistrationBroadcastReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                // checking for type intent filter
                if (intent.action == Config.REGISTRATION_COMPLETE) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL)
                } else if (intent.action == Config.PUSH_NOTIFICATION) {
                    // new push notification is received
                    val message = intent.getStringExtra("message")
                    val jsonMessageResponse = intent.getStringExtra("jsonMessage")
                    if (jsonMessageResponse != null && !jsonMessageResponse.isEmpty()) {
                        println("$TAG json message=$jsonMessageResponse")
                        val gson = Gson()
                        val fcmNotificationMain = gson.fromJson(jsonMessageResponse, FcmNotificationMain::class.java)
                        val campaignId: String
                        val imageUrl: String
                        val title: String
                        val jsonMessage: String
                        val type: String?
                        val userId: String
                        val url: String
                        val username: String
                        val timestamp = ""
                        try {
                            campaignId = fcmNotificationMain.body!!.campaignId
                            imageUrl = fcmNotificationMain.body!!.imageUrl
                            title = fcmNotificationMain.body!!.title
                            jsonMessage = fcmNotificationMain.body!!.message
                            type = fcmNotificationMain.body!!.type
                            userId = fcmNotificationMain.body!!.userId
                            url = fcmNotificationMain.body!!.url
                            username = fcmNotificationMain.body!!.username
                            if (type != null && type == "73") {
                                //final String username, final String userId, final String campaignId, String imageUrl, String title, String message, final String url
                                if (mSessionManager.isUserLoggedIn) dialogBox.localCampaignDialog(username, userId, campaignId, imageUrl, title, jsonMessage, url)
                            }
                        } catch (e: NullPointerException) {
                            e.printStackTrace()
                        }
                    }
                }
            }
        }
    }

    companion object {
        private val TAG = NotificationMessageDialog::class.java.simpleName
    }

    init {
        mSessionManager = SessionManager(mActivity!!)
        dialogBox = DialogBox(mActivity)
        initFcmBroadCastVar()
    }
}