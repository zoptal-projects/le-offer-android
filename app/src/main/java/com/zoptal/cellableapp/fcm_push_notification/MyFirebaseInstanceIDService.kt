package com.zoptal.cellableapp.fcm_push_notification

import android.content.Intent
import android.util.Log

import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.FirebaseInstanceIdService
import com.zoptal.cellableapp.fcm_push_notification.MyFirebaseInstanceIDService
import com.zoptal.cellableapp.utility.SessionManager

/**
 * <h>MyFirebaseInstanceIDService</h>
 *
 *
 * In this class we used to get the Push Registration Id.
 *
 */
class MyFirebaseInstanceIDService : FirebaseInstanceIdService() {
    override fun onTokenRefresh() {
        super.onTokenRefresh()
        val refreshedToken = FirebaseInstanceId.getInstance().token
        Log.e(TAG, "sendRegistrationToServer: $refreshedToken")

        // Saving reg id to shared preferences
        val mSessionManager = SessionManager(this@MyFirebaseInstanceIDService)
        if (refreshedToken != null && !refreshedToken.isEmpty()) mSessionManager.pushToken = refreshedToken

        // Notify UI that registration has completed, so the progress indicator can be hidden.
        val registrationComplete = Intent(Config.REGISTRATION_COMPLETE)
        registrationComplete.putExtra("token", refreshedToken)
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete)
    }

    companion object {
        private val TAG = MyFirebaseInstanceIDService::class.java.simpleName
    }
}