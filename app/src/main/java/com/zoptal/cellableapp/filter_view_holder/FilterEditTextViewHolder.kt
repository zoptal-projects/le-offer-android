package com.zoptal.cellableapp.filter_view_holder

import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.widget.EditText
import android.widget.TextView
import com.zoptal.cellableapp.R

/**
 * Created by embed on 22/5/18.
 */
class FilterEditTextViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var tV_fieldName: TextView
    var eT_fieldValue: EditText

    init {
        tV_fieldName = itemView.findViewById<View>(R.id.tV_fieldName) as TextView
        eT_fieldValue = itemView.findViewById<View>(R.id.eT_fieldValue) as EditText
    }
}