package com.zoptal.cellableapp.filter_view_holder

import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.zoptal.cellableapp.R

/**
 * Created by embed on 22/5/18.
 */
class MultipleSelectionViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var rV_multiple: RecyclerView
    var tV_fieldName: TextView

    init {
        tV_fieldName = itemView.findViewById<View>(R.id.tV_fieldName) as TextView
        rV_multiple = itemView.findViewById<View>(R.id.rV_multiple) as RecyclerView
    }
}