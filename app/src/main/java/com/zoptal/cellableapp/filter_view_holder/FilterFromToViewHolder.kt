package com.zoptal.cellableapp.filter_view_holder

import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.widget.EditText
import android.widget.TextView
import com.zoptal.cellableapp.R

/**
 * Created by embed on 22/5/18.
 */
class FilterFromToViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var eT_fromValue: EditText
    var eT_toValue: EditText
    var tV_fieldName: TextView

    init {
        tV_fieldName = itemView.findViewById<View>(R.id.tV_fieldName) as TextView
        eT_toValue = itemView.findViewById<View>(R.id.eT_toValue) as EditText
        eT_fromValue = itemView.findViewById<View>(R.id.eT_fromValue) as EditText
    }
}