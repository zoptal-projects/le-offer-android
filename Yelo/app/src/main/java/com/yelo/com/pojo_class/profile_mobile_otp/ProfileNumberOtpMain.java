package com.zoptal.cellableapp.pojo_class.profile_mobile_otp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by hello on 11-Jul-17.
 */

public class ProfileNumberOtpMain
{
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private ProfileNumberOtpData data;
    @SerializedName("otp")
    @Expose
    private String otp;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ProfileNumberOtpData getData() {
        return data;
    }

    public void setData(ProfileNumberOtpData data) {
        this.data = data;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }
}
