package com.zoptal.cellableapp.main.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.zoptal.cellableapp.R;
import com.zoptal.cellableapp.utility.SessionManager;
import com.zoptal.cellableapp.utility.VariableConstants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * <h>WebActivity class</h>
 * <p>
 * This activity mainly load the url to webView, url passed via intent form other activity.
 *
 * @author 3Embed
 * @since 30/01/18.
 */
public class PrivacyPolicyActivity extends AppCompatActivity {
   /* @Inject
    TypeFaceManager typeFaceManager;*/

    @BindView(R.id.webView)
    WebView webView;

    @BindView(R.id.pbLoadProgress)
    ProgressBar pbLoadProgress;

    @BindView(R.id.tvError)
    TextView tvError;
    @BindView(R.id.page_title_tv)
    TextView tvPageTitle;

    @BindView(R.id.rL_accept)
    RelativeLayout rL_accept;

    private Unbinder unbinder;
    final Activity activity = this;
    private String url = null;
    private String pageTitle = "";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        unbinder = ButterKnife.bind(this);
        url = getResources().getString(R.string.privacyPolicyUrl);
        //pageTitle = getIntent().getStringExtra("title");
        initView();
        initWebview();
    }

    /**
     * set the page title and typeface.
     */
    private void initView() {
        if(pageTitle != null)
            tvPageTitle.setText(getResources().getString(R.string.privacyPolicy));
        //rL_accept.setVisibility(View.VISIBLE);
        //tvPageTitle.setTypeface(typeFaceManager.getCircularAirBold());
    }

    /**
     * <P>
     *  Enable javaScript and set WebChromeClient and webViewClient
     *  to webView.
     * </P>
     *
     */
    @SuppressLint("SetJavaScriptEnabled")
    private void initWebview() {
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.setWebChromeClient(new WebChromeClient(){
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);
                activity.setProgress(newProgress);
                if(newProgress >= 50) {
                    activity.setTitle(R.string.app_name);
                    //if(pbLoadProgress != null)
                        //pbLoadProgress.setVisibility(View.INVISIBLE);
                }
            }
        });

        webView.setWebViewClient(new WebViewClient(){

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
                showError("loading error");
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                return super.shouldOverrideUrlLoading(view, request);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                pbLoadProgress.setVisibility(View.GONE);
                rL_accept.setVisibility(View.VISIBLE);
            }
        });

        pbLoadProgress.setVisibility(View.VISIBLE);
        if(url != null)
            webView.loadUrl(url);

    }

    /**
     * set the error msg to TextView tvError.
     * @param errorMsg
     */
    private void showError(String errorMsg)
    {
        if(tvError != null) {
            tvError.setText(errorMsg);
            tvError.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onBackPressed()
    {
        if(webView.canGoBack())
            webView.goBack();
        else {
            onBack();
        }
    }

    @OnClick(R.id.close_button)
    public void onBack(){
        super.onBackPressed();
        activity.overridePendingTransition(R.anim.slide_up, R.anim.slide_down);
        Intent   intent = new Intent(this, HomePageActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        webView.destroy();
        unbinder.unbind();
    }

    @OnClick(R.id.rL_accept)
    public void accept() {
        Intent intent = new Intent();
        intent.putExtra("accept",true);
        setResult(VariableConstants.PRIVACY_POLICY,intent);
        SessionManager sessionManager = new SessionManager(activity);
        sessionManager.setSignUpPrivacyAccept(true);
        finish();
    }
}

