package com.zoptal.cellableapp.main.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.zoptal.cellableapp.R;
import com.zoptal.cellableapp.utility.SessionManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * <h>WebActivity class</h>
 * <p>
 * This activity mainly load the url to webView, url passed via intent form other activity.
 *
 * @author 3Embed
 * @since 30/01/18.
 */
public class PermissionAcceptActivity extends AppCompatActivity {
   /* @Inject
    TypeFaceManager typeFaceManager;*/


    @BindView(R.id.page_title_tv)
    TextView tvPageTitle;

    @BindView(R.id.rL_allow)
    RelativeLayout rL_allow;

    @BindView(R.id.tV_deny)
    TextView tV_deny;

    private Unbinder unbinder;
    final Activity mActivity = this;
    private String url = null;
    private String pageTitle = "",postResultData="";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_show_privacy);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        unbinder = ButterKnife.bind(this);
        //url = getResources().getString(R.string.privacyPolicyUrl);
        //pageTitle = getIntent().getStringExtra("title");
        initView();
        //initWebview();
    }

    /**
     * set the page title and typeface.
     */
    private void initView() {
        if(pageTitle != null)
            tvPageTitle.setText(getResources().getString(R.string.privacyPolicy));

        postResultData = getIntent().getStringExtra("postResultData");

        //tvPageTitle.setTypeface(typeFaceManager.getCircularAirBold());

        TextView mPrivacyLink = findViewById(R.id.tV_privacy);
        String s1 = mActivity.getResources().getString(R.string.privacy_link_text);
        String s2 = mActivity.getResources().getString(R.string.privacyPolicy);
        String privacy = getResources().getString(R.string.privacyPolicyUrl);
        String privacyTitle = getResources().getString(R.string.privacyPolicy);
        String message = s1 + " " + s2;
        SpannableString spannableString = new SpannableString(message);
        //spannableString.setSpan(new MyClickableSpan(true,terms,termsConditions),s1.length()+1,s1.length()+s2.length()+1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new MyClickableSpan(true, privacy, privacyTitle), s1.length() + 1, s1.length() + s2.length() + 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        mPrivacyLink.setText(spannableString);
        mPrivacyLink.setMovementMethod(LinkMovementMethod.getInstance());


        mPrivacyLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mActivity, PrivacyAndTermsActivity.class);
                intent.putExtra("url", mActivity.getResources().getString(R.string.privacyPolicyUrl));
                intent.putExtra("title", mActivity.getResources().getString(R.string.privacyPolicy));
                mActivity.startActivity(intent);
            }
        });
    }




    @Override
    public void onBackPressed()
    {

            onBack();

    }

    @OnClick(R.id.close_button)
    public void onBack(){
        super.onBackPressed();
        mActivity.overridePendingTransition(R.anim.slide_up,R.anim.slide_down);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @OnClick(R.id.rL_allow)
    public void accept() {
        SessionManager sessionManager = new SessionManager(mActivity);
        sessionManager.setPrivacyAccept(true);
        Intent intent = new Intent(mActivity, HomePageActivity.class);
        intent.putExtra("postResultData", postResultData);
        startActivity(intent);
        finish();
    }

    @OnClick(R.id.tV_deny)
    public void deny() {
       onBack();
    }

    /**
     * <h>MyClickableSpan</h>
     * <p>
     * In this method we used to set the text clickable part by part seperately
     * like "Terms and Conditions" seperate and "Privacy Policy" seperate.
     * </p>
     */
    private class MyClickableSpan extends ClickableSpan {
        private boolean isUnderLine;
        private String setUrl, title;

        MyClickableSpan(boolean isUnderLine, String setUrl, String title) {
            this.isUnderLine = isUnderLine;
            this.setUrl = setUrl;
            this.title = title;
        }

        @Override
        public void onClick(View widget) {
            Intent privacyIntent = new Intent(mActivity, PrivacyAndTermsActivity.class);
            privacyIntent.putExtra("url", setUrl);
            privacyIntent.putExtra("title", title);
            startActivity(privacyIntent);
        }

        @Override
        public void updateDrawState(TextPaint ds) {
            super.updateDrawState(ds);
            ds.setUnderlineText(isUnderLine);
            ds.setColor(ContextCompat.getColor(mActivity, R.color.color_overlay));
            ds.setFakeBoldText(false);
        }
    }
}

