package com.zoptal.cellableapp.mqttchat.Utilities;

import android.content.Context;
import android.content.Intent;
import androidx.core.content.WakefulBroadcastReceiver;
import com.zoptal.cellableapp.mqttchat.AppController;
/*
 * To keep the device awake when it is booting
 */
public class BootCompletedIntentReceiver extends WakefulBroadcastReceiver
{
    @Override
    public void onReceive(Context context, Intent intent)
    {
        AppController.getInstance().createMQttConnection(AppController.getInstance().getUserId(),true);

    }
}