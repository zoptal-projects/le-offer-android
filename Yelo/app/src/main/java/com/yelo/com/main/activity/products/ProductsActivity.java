package com.zoptal.cellableapp.main.activity.products;

import android.app.Activity;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.core.content.LocalBroadcastManager;
import androidx.core.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.zoptal.cellableapp.R;
import com.zoptal.cellableapp.adapter.ProductDetailPagerAdap;
import com.zoptal.cellableapp.fcm_push_notification.Config;
import com.zoptal.cellableapp.fcm_push_notification.NotificationMessageDialog;
import com.zoptal.cellableapp.fcm_push_notification.NotificationUtils;
import com.zoptal.cellableapp.pojo_class.home_explore_pojo.ExploreResponseDatas;

import java.util.ArrayList;

/*
* In this activity product detail page showing
* Using View Pager For swipe to go next product
* */

public class ProductsActivity extends AppCompatActivity {

    private ArrayList<ExploreResponseDatas> arrayListExploreDatas;
    private ViewPager viewPagerDetail;
    public ProductDetailPagerAdap productDetailPagerAdap;
    private int position;
    private Activity mActivity;
    private NotificationMessageDialog mNotificationMessageDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products);

        if(getIntent()!=null) {
            arrayListExploreDatas = (ArrayList<ExploreResponseDatas>) getIntent().getSerializableExtra("arrayListExploreDatas");
            position = getIntent().getIntExtra("position",0);
        }
        mActivity = ProductsActivity.this;
        mNotificationMessageDialog=new NotificationMessageDialog(mActivity);

        viewPagerDetail = (ViewPager)findViewById(R.id.viewPager_detail);
        productDetailPagerAdap = new ProductDetailPagerAdap(getSupportFragmentManager(),arrayListExploreDatas,this);
        viewPagerDetail.setAdapter(productDetailPagerAdap);
        viewPagerDetail.setCurrentItem(position,true);


    }

    @Override
    protected void onResume()
    {
        super.onResume();

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog.mRegistrationBroadcastReceiver, new IntentFilter(Config.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog.mRegistrationBroadcastReceiver, new IntentFilter(Config.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getApplicationContext());
    }
}
